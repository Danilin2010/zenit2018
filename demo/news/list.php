<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список новостей");
?>


    <div class="div_mini_filter show_min">
        <div class="div_mini_filter_title">
            Фильтр
            <div class="arr"></div>
        </div>
        <div class="div_mini_filter_body">
            <!--filter_block-->
            <div class="filter_block">
                <div class="filter_block_title">
                    Категории
                </div>
                <div class="filter_block_body">
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="checkbox"
                                name="CATALOG"
                                checked
                                value="1"/>Банковские карты</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="checkbox"
                                name="CATALOG"
                                value="2"/>Интернет–банк</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="checkbox"
                                name="CATALOG"
                                value="3"/>Изменение тарифов</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="checkbox"
                                name="CATALOG"
                                value="4"/>Кредитование</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="checkbox"
                                name="CATALOG"
                                value="5"/>Для бизнеса</label>
                    </div>
                </div>
            </div>
            <!--filter_block-->
            <!--filter_block-->
            <div class="filter_block">
                <div class="filter_block_title">
                    По годам
                </div>
                <div class="filter_block_body">
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="radio"
                                name="year"
                                checked
                                value="1"/>Все</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="radio"
                                name="year"
                                value="2"/>2017</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="radio"
                                name="year"
                                value="3"/>2016</label>
                    </div>
                    <div class="filter_block_body_item">
                        <label><input
                                class="formstyle"
                                type="radio"
                                name="year"
                                value="4"/>2015</label>
                    </div>
                </div>
            </div>
            <!--filter_block-->
            <div class="form_application_line">
                <input class="button bigmaxwidth" value="Применить фильтр" type="submit">
            </div>
        </div>
    </div>


    <div class="wr_block_type gray">
        <div class="block_type to_column c-container wr_news_list">
            <div class="block_type_right">
                <div class="wr_form_block">
                    <div class="right_top_line">
                        <div class="block_top_line hide_min"></div>
                        <!--filter_block-->
                        <div class="filter_block hide_min">
                            <div class="filter_block_title">
                                Категории
                            </div>
                            <div class="filter_block_body">
                                <div class="filter_block_body_item">
                                    <label><input
                                            class="formstyle"
                                            type="checkbox"
                                            name="CATALOG"
                                            checked
                                            value="1"/>Банковские карты</label>
                                </div>
                                <div class="filter_block_body_item">
                                    <label><input
                                            class="formstyle"
                                            type="checkbox"
                                            name="CATALOG"
                                            value="2"/>Интернет–банк</label>
                                </div>
                                <div class="filter_block_body_item">
                                    <label><input
                                            class="formstyle"
                                            type="checkbox"
                                            name="CATALOG"
                                            value="3"/>Изменение тарифов</label>
                                </div>
                                <div class="filter_block_body_item">
                                    <label><input
                                            class="formstyle"
                                            type="checkbox"
                                            name="CATALOG"
                                            value="4"/>Кредитование</label>
                                </div>
                                <div class="filter_block_body_item">
                                    <label><input
                                            class="formstyle"
                                            type="checkbox"
                                            name="CATALOG"
                                            value="5"/>Для бизнеса</label>
                                </div>
                            </div>
                        </div>
                        <!--filter_block-->
                        <br class="hide_min">
                        <?require($_SERVER["DOCUMENT_ROOT"]."/demo/news/form.php");?>
                    </div>
                </div>
            </div>
            <div class="block_type_center">



                <div class="filtr_news_top hide_min">
                    Показать за
                    <select class="formstyle min" name="year">
                        <option value="all">Весь период</option>
                        <option value="2017" selected>2017</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                    </select>
                </div>

                <div class="news_list">
                    <div class="news_list_item">
                        <div class="body">
                            <div class="news_list_item_data">
                                15 февраля 2017
                            </div>
                            <a href="#" class="news_list_item_title">
                                Проведите волшебные выходные в Диснейленд в Париже
                            </a>
                            <div class="news_list_item_text">
                                Уважаемые клиенты! В связи празднованием Дня защитника Отечества изменяется график работы отделений Банка ЗЕНИТ.
                            </div>
                            <div class="news_list_item_source">
                                Сюзанна Камара «Ведомости»
                            </div>
                        </div>
                    </div>
                    <div class="news_list_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/news/001.png')"></div>
                        <div class="body right">
                            <div class="news_list_item_data">
                                15 февраля 2017
                            </div>
                            <a href="#" class="news_list_item_title">
                                Информация для клиентов
                            </a>
                            <div class="news_list_item_text">
                                Уважаемые клиенты! В связи празднованием Дня защитника Отечества изменяется график работы отделений Банка ЗЕНИТ.
                            </div>
                        </div>
                    </div>
                    <div class="news_list_item">
                        <div class="body">
                            <div class="news_list_item_data">
                                14 февраля 2017
                            </div>
                            <a href="#" class="news_list_item_title">
                                Проведите волшебные выходные в Диснейленд в Париже
                            </a>
                            <div class="news_list_item_text">
                                Уважаемые клиенты! В связи празднованием Дня защитника Отечества изменяется график работы отделений Банка ЗЕНИТ.
                            </div>
                        </div>
                    </div>
                    <div class="news_list_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/news/002.png')"></div>
                        <div class="body right">
                            <div class="news_list_item_data">
                                10 января 2017
                            </div>
                            <a href="#" class="news_list_item_title">
                                Информация для клиентов
                            </a>
                            <div class="news_list_item_text">
                                Уважаемые клиенты! В связи празднованием Дня защитника Отечества изменяется график работы отделений Банка ЗЕНИТ.
                            </div>
                        </div>
                    </div>
                    <div class="news_list_item">
                        <div class="body">
                            <div class="news_list_item_data">
                                10 января 2017
                            </div>
                            <a href="#" class="news_list_item_title">
                                Новые направления переводов в системе "Золотая Корона
                                Денежные переводы"
                            </a>
                            <div class="news_list_item_text">
                                Банк ЗЕНИТ поднялся на 2 позиции по сравнению с результатами 2014 года и занял 16-е место по
                                объему ипотечного портфеля в рейтинге российских ипотечных банков.
                            </div>
                            <div class="news_list_item_source">
                                Сюзанна Камара «Ведомости»
                            </div>
                        </div>
                    </div>
                </div>

                <div class="button_line">
                    <a href="#" class="button transparent maxwidth" >показать еще новости</a>
                </div>

            </div>

            <div class="contant_block_fixwd">
                <div class="block_top_line"></div>
                <h2>Контакты для прессы</h2>
                <div class="contacts_block">
                    8 (800) 500-66-77
                    <br>
                    <a href="mailto:info@zenit.ru">info@zenit.ru</a>
                </div>
            </div>

        </div>
    </div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>