<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новость");
?>





    <div class="wr_block_type gray">
        <div class="block_type to_column c-container">

            <a href="#" class="top_arr_prew show_min">
                <div class="arr"></div>Назад
            </a>

            <h1 class="hide_min">Новости<a href="#" class="rss"></a></h1>

            <div class="block_type_right">
                <div class="wr_form_block">
                    <div class="right_top_line">
                        <?require($_SERVER["DOCUMENT_ROOT"]."/demo/news/form.php");?>


                        <br>          <br class=" hide_min">

                        <div class="block_top_line hide_min"></div>
                        <h2>Новости банка в соцсетях</h2>
                        <?$APPLICATION->IncludeComponent("bitrix:news.list", "fuuter_net", Array(
                            "COMPONENT_TEMPLATE" => "main_news",
                            "IBLOCK_TYPE" => "icon",	// Тип информационного блока (используется только для проверки)
                            "IBLOCK_ID" => "5",	// Код информационного блока
                            "NEWS_COUNT" => "10",	// Количество новостей на странице
                            "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                            "SORT_BY2" => "NAME",	// Поле для второй сортировки новостей
                            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                            "FILTER_NAME" => "",	// Фильтр
                            "FIELD_CODE" => array(	// Поля
                                0 => "DETAIL_TEXT",
                                1 => "",
                            ),
                            "PROPERTY_CODE" => array(	// Свойства
                                0 => "CLASS",
                                1 => "LINK",
                                2 => "",
                            ),
                            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                            "CACHE_TYPE" => "A",	// Тип кеширования
                            "CACHE_TIME" => "36000",	// Время кеширования (сек.)
                            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                            "ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
                            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                            "PARENT_SECTION" => "",	// ID раздела
                            "PARENT_SECTION_CODE" => "",	// Код раздела
                            "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
                            "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                            "DISPLAY_NAME" => "Y",	// Выводить название элемента
                            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                            "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                            "PAGER_TITLE" => "Новости",	// Название категорий
                            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                            "SET_STATUS_404" => "N",	// Устанавливать статус 404
                            "SHOW_404" => "N",	// Показ специальной страницы
                            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                        ),
                            false
                        );?>

                    </div>
                </div>
            </div>
            <div class="block_type_center">

                <div class="news_detail">
                    <div class="news_detail_body">
                        <div class="news_detail_data">15 февраля 2017</div>
                        <div class="news_detail_title">
                            Проведите волшебные выходные в Диснейленд в Париже
                        </div>
                        <div class="news_detail_source">Сюзанна Камара «Ведомости»</div>
                        <div class="news_detail_body_wr">
                            <p>
                                Зарегистрируйтесь в программе MasterCard Бесценные Города® до 21 августа 2016 года и получите шанс отправиться всей семьей в Disneyland®Paris.
                                Проведите волшебные выходные в самом главном парке развлечений Европы и подарите вашему малышу незабываемую возможность открыть
                                парад – театрализованное шествие любимых героев Disney. Воспоминания об этих бесценных моментах останутся с вашим ребенком на всю жизнь.
                            </p>
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/news/003.png" />
                            <p>
                                Программа MasterCard Бесценные Города® – это незабываемые впечатления и привилегии для держателей карт MasterCard® по всему миру.
                                Участникам программы MasterCard Бесценные Города® доступны различные уровни привилегий: от скидок и комплиментов до приоритетного
                                доступа и уникальных впечатлений, которые MasterCard создает в партнерстве со всемирно известными театрами, музеями, отелями, ресторанами, магазинами и брендами.
                            </p>
                            <p>
                                Кирилл Олегович Шпигун, Председатель Правления Банка:
                                <div class="note">
                                Программа MasterCard Бесценные Города® – это незабываемые
                                впечатления и привилегии для держателей карт MasterCard® по всему миру.
                                Участникам программы MasterCard Бесценные Города® доступны различные уровни привилегий.
                                </div>
                            </p>
                            <p>
                                Благодаря возможностям, которые дарит MasterCard, весь мир откроется Вам с новой, неожиданной стороны.<br/>
                                Чтобы стать участником акции, необходимо пройти регистрацию на<br/>
                                <a href="https://ru.priceless.com/disney">https://ru.priceless.com/disney</a>.
                            </p>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="left">
                            Спасибо за отзыв!
                        </div>
                        <div class="right">
                            Поделиться:
                            <ul class="share_net">
                                <li><a href="#" class="vk"></a></li>
                                <li><a href="#" class="f"></a></li>
                                <li><a href="#" class="tw" ></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="after_news">
                    <div class="after_news_title">
                        <a href="#" class="after_news_title_link">Все новости</a>
                        <h2>Другие новости</h2>
                    </div>
                    <div class="after_news_list">
                        <div class="after_news_item">
                            <div class="after_news_item_data">
                                15 января 2017
                            </div>
                            <a href="#" class="after_news_item_title">Информация для клиентов</a>
                            <div class="after_news_item_text">
                                Благодаря возможностям, которые дарит MasterCard, весь мир откроется Вам с новой,
                                неожиданной стороны. Чтобы стать участником акции, необходимо пройти регистрац...
                            </div>
                        </div>
                        <div class="after_news_item">
                            <div class="after_news_item_data">
                                15 января 2017
                            </div>
                            <a href="#" class="after_news_item_title">Проведите волшебные выходные в Диснейленд в Париже</a>
                            <div class="after_news_item_text">
                                MasterCard создает в партнерстве со всемирно известными театрами, музеями, отелями, ресторанами,
                                магазинами и брендами. Чтобы стать участником акции, необходимо... пройти
                            </div>
                        </div>
                        <div class="after_news_item">
                            <div class="after_news_item_data">
                                15 января 2017
                            </div>
                            <a href="#" class="after_news_item_title">Специальные ипотечные программы для военнослужащих</a>
                            <div class="after_news_item_text">
                                Программа MasterCard Бесценные Города® – это незабываемые впечатления и приви- легии для держателей
                                карт MasterCard® по всему миру. Участникам программы по...
                            </div>
                        </div>
                    </div>
                </div>

            </div>




        </div>
    </div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>