<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Калькулятор на сайт Ипотека");
?>

<?
use \Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/function.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/conversion.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/fly.js');
?>

    <div class="wr_block_type gray calculator calculator_miles">
        <div class="block_type to_column c-container">
            <div class="block_type_center">
                <form action="" class="calculator_mortgage">
                    <div class="form_application">
                        <div class="form_application_line">
                            <div class="form_application_line_title_first">
                                Куда я смогу полететь бесплатно?
                            </div>
                            <div class="form_application_line_title_to">
                                Рассчитайте колличество миль, которые вы можете тратить
                            </div>
                            <div class="wr_card_to_card">
                                <div class="wr_complex_input use">
                                    <div class="complex_input_body">
                                        <div class="text">Покупки по карте в месяц</div>
                                        <input data-summ type="text" class="simple_input big" id="summ" value="10000">
                                    </div>
                                    <div class="complex_input_block summ"></div>
                                </div>
                                <div class="card_to_card_select"
                                     data-summ-select
                                     data-target="summ"
                                     data-max="60000"
                                     data-min="5000"
                                     data-value="10000"
                                     data-suffix=" тыс"
                                     data-del="1000"
                                    ></div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="block_type_right">
                <div class="right_top_line">
                    <div class="wr_shares wr_shares_big">
                        <div class="shares">
                            <div class="f_shares"></div>
                            <div class="t_shares"></div>
                            <ul class="shares_container baraja-container">
                                <li data-summ="5000">
                                    <div class="wr_snippet" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/001.png');">
                                        <div class="wr_snippet_title">5000</div>
                                        <div class="wr_snippet_bottom">
                                            <div class="flag" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/flag.png');"></div>
                                            Киото, Япония
                                        </div>
                                    </div>
                                </li>
                                <li data-summ="10000">
                                    <div class="wr_snippet" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/001.png');">
                                        <div class="wr_snippet_title">10000</div>
                                        <div class="wr_snippet_bottom">
                                            <div class="flag" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/flag.png');"></div>
                                            Бомбей, Индия
                                        </div>
                                    </div>
                                </li>
                                <li data-summ="15000">
                                    <div class="wr_snippet" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/001.png');">
                                        <div class="wr_snippet_title">15000</div>
                                        <div class="wr_snippet_bottom">
                                            <div class="flag" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/flag.png');"></div>
                                            Сидней, Австралия
                                        </div>
                                    </div>
                                </li>
                                <li data-summ="20000">
                                    <div class="wr_snippet" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/001.png');">
                                        <div class="wr_snippet_title">20000</div>
                                        <div class="wr_snippet_bottom">
                                            <div class="flag" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/flag.png');"></div>
                                            Берлин, Германия
                                        </div>
                                    </div>
                                </li>
                                <li data-summ="25000">
                                    <div class="wr_snippet" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/001.png');">
                                        <div class="wr_snippet_title">25000</div>
                                        <div class="wr_snippet_bottom">
                                            <div class="flag" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/calk/flag.png');"></div>
                                            Москва, Россия
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>