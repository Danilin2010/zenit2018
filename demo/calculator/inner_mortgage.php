<div class="block_type to_column c-container">
    <h1>Рассчитайте ваше ипотечное предложение</h1>
    <div class="block_type_center">
        <form action="" class="calculator_mortgage">
            <div class="form_application">
                <div class="form_application_line">
                    <select data-programm data-plasholder="Выберите программу" data-title="Программа кредитования" class="formstyle" name="programm">
                        <option value="new" selected>Новостройка</option>
                        <option value="apartment">Квартира</option>
                        <option value="room">Комната</option>
                        <option value="house">Земля + Дом</option>
                        <option value="special">Специальные программы</option>
                        <option value="ahml">АИЖК</option>
                        <option value="pledge">Кредит под залог имеющейся недв</option>
                    </select>
                </div>
                <div class="form_application_line">
                    <div class="form_application_line_title">
                        Регион
                    </div>
                    <div class="form_application_item_label">
                        <label><input data-region class="formstyle" type="radio" name="region" value="moscow" checked/>Московский регион</label>
                    </div>
                    <div class="form_application_tree">
                        <label><input data-region class="formstyle" type="radio" name="region" value="region"/>Другие регионы</label>
                    </div>
                </div>
                <div class="form_application_line">
                    <label><input data-client class="formstyle" type="checkbox" name="client" value="Y" />Я зарплатный клиент Банка ЗЕНИТ / клиент от Партнера Банка</label>
                </div>

                <div class="form_application_line">
                    <div class="form_application_line_title">
                        Сумма кредита
                    </div>
                    <div class="wr_card_to_card">
                        <div class="wr_complex_input big">
                            <div class="complex_input_body">
                                <input data-amount-credit type="text" class="simple_input big" id="amount-credit" value="22460000">
                            </div>
                            <div class="complex_input_block summ"></div>
                        </div>
                        <div class="card_to_card_select"
                             data-target="amount-credit"
                             data-max="32000000"
                             data-min="1000000"
                             data-value="22460000"
                             data-suffix=" МЛН"
                             data-del="1000000"
                             data-useamount="Y"
                             data-useconversion="cost_real_estate"
                            ></div>
                    </div>
                </div>

                <div class="form_application_line" data-hidepledge>
                    <div class="form_application_line_title">
                        <div class="form_application_line_title_right" data-first-rub><span data-print-first-rub>50</span> %</div><div class="form_application_line_title_right" data-first-precent><span data-print-first>5 000 000</span> <span class="rub">₽</span></div>
                        Первоначальный взнос
                        <div class="wr-toggle-light-text line_initial">
                            <div class="toggle-light-text on">В процентах</div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N" data-name="type"></div>
                            </div>
                            <div class="toggle-light-text off">В рублях</div>
                        </div>
                    </div>
                    <div class="wr_card_to_card" data-first-precent>
                        <div class="wr_complex_input big">
                            <div class="complex_input_body">
                                <input data-contribution type="text" class="simple_input big" id="first_calk" value="35">
                            </div>
                            <div class="complex_input_block">%</div>
                        </div>
                        <div class="card_to_card_select"
                             data-target="first_calk"
                             data-max="80"
                             data-min="10"
                             data-value="35"
                             data-suffix="%"
                        ></div>
                    </div>
                    <div class="wr_card_to_card" data-first-rub>
                        <div class="wr_complex_input big">
                            <div class="complex_input_body">
                                <input data-contributionrub type="text" class="simple_input big" id="first_calk_rub" value="7980000">
                            </div>
                            <div class="complex_input_block summ"></div>
                        </div>
                        <div class="card_to_card_select"
                             data-target="first_calk_rub"
                             data-setfirstfumm="Y"
                             data-max="32000000"
                             data-min="1000000"
                             data-value="7980000"
                             data-del="1000000"
                             data-suffix=" МЛН"
                        ></div>
                    </div>
                </div>

                <div class="form_application_line">
                    <div class="form_application_line_title">
                        Срок ипотеки
                    </div>
                    <div class="wr_card_to_card">
                        <div class="wr_complex_input big">
                            <div class="complex_input_body">
                                <input data-year type="text" class="simple_input big" id="year_calk" value="10">
                            </div>
                            <div class="complex_input_block" data-yearsklonenie>лет</div>
                        </div>
                        <div class="card_to_card_select"
                             data-target="year_calk"
                             data-max="30"
                             data-min="0"
                             data-value="10"
                             data-stepdelh=6
                             data-stepdelhptir1=25
                             data-stepdelhtir1=5
                             data-stepdelhptir2=15
                             data-stepdelhtir2=3
                             data-useconversion="year"
                             data-stepdelmin="20"
                             data-maxmin="1"
                             data-useconversionmin="yearmin"
                        ></div>
                    </div>
                </div>

                <div class="form_application_line">
                    <div class="form_application_line_title" data-pledgetext>
                        Стоимость недвижимости
                    </div>
                    <div class="wr_card_to_card">
                        <div class="wr_complex_input big">
                            <div class="complex_input_body">
                                <input data-summ type="text" class="simple_input big" id="main_ptop" value="22460000">
                            </div>
                            <div class="complex_input_block summ"></div>
                        </div>
                        <div class="card_to_card_select"
                             data-target="main_ptop"
                             data-max="32000000"
                             data-min="1000000"
                             data-value="22460000"
                             data-suffix=" МЛН"
                             data-del="1000000"
                             data-useconversion="amount_credit"
                        ></div>
                    </div>
                </div>


                <div class="form_application_line">
                    <div class="form_application_line_title">
                        Eжемесячный платеж
                    </div>
                    <div class="wr_card_to_card">
                        <div class="wr_complex_input big">
                            <div class="complex_input_body">
                                <input data-monthly-payment type="text" class="simple_input big" id="monthly-payment" value="22460000">
                            </div>
                            <div class="complex_input_block summ"></div>
                        </div>
                    </div>
                </div>



            </div>
    </div>
    <div class="block_type_right">
        <div class="right_top_line">
            <div class="text_block right_bank_block right_bank_summ">
                <div class="right_bank_text">
                    Размер кредита
                </div>
                <div class="right_bank_summ_summ big">
                    <span data-print-summ>17 560 000</span> <span class="rub">₽</span>
                </div>
                <div class="wr_right_bank_summ_block">
                    <div class="right_bank_summ_block">
                        <div class="right_bank_text">
                            Ежемесячный платеж
                        </div>
                        <div class="right_bank_summ_summ">
                            <span data-print-monf>44 433</span> <span class="rub">₽</span>
                        </div>
                    </div><div class="right_bank_summ_block">
                        <div class="right_bank_text">
                            ставка
                        </div>
                        <div class="right_bank_summ_summ">
                            <span data-print-rate>10,5</span>%
                        </div>
                    </div>
                </div>
                <a href="#" class="button" >Оставить заявку</a>
            </div>
            <div class="right_bank_title" style="text-align: center;">
                Данный расчет носит справочный характер и не является офертой
            </div>
        </div>
    </div>
</div>
