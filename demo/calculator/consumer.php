<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Калькулятор на сайт Кредит");
?>
<?
use \Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/function.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/varconsumer.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/mortgage.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/conversion.js');
?>
<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/calculator/consumer.php",
    Array(),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>