var OfficeMap = (function (ymaps) {
    var myMap = null;
    var hasInit = false;
    var data = [];
    var placeholderCollection, placeholderCollectionZenit;
    var userLocation = null;
    var route = false;

    var modalMapOffice = null;
    var modalMapAtm = null;



    function init() {
        return this;
    }

    function initMap(callback) {
        ymaps.geocode('Россия, ' + city.NAME + '', {results: 1}).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

          myMap = new ymaps.Map("map-bank", {
                center: firstGeoObject.geometry.getCoordinates(),
                zoom: 12,
                controls: []
            }, {
                searchControlProvider: 'yandex#search'
            });

            myMap.behaviors.disable('scrollZoom');
            // myMap.behaviors.disable('drag');
            myMap.controls.add("zoomControl", {
                position: {top: 200, left: 30}
            });

            hasInit = true;

            if (callback) {
                callback();
            }

            modalMapAtm = new ymaps.Map("map-modal-atm", {
                center: firstGeoObject.geometry.getCoordinates(),
                zoom: 12,
                controls: [],
                scroll: false
            }, {
                searchControlProvider: 'yandex#search'
            });
            modalMapAtm.behaviors.disable('drag');

            modalMapOffice = new ymaps.Map("map-modal-office", {
                center: firstGeoObject.geometry.getCoordinates(),
                zoom: 12,
                controls: [],
                scroll: false
            }, {
                searchControlProvider: 'yandex#search'
            });
            modalMapOffice.behaviors.disable('drag');


            StyleZenit = new ymaps.Style();
            StyleZenit.iconStyle.href = "/local/templates/bz/img/placeholder.png";
            StyleZenit.iconStyle.size = new YMaps.Point(31, 41);

            StyleDefault = new ymaps.Style();
            StyleDefault.iconStyle.href = "/local/templates/bz/img/placeholder-empty.png";
            StyleDefault.iconStyle.size = new YMaps.Point(31, 41);

        });
    };

    function setMobailPointerById(id){
        var el=getElement(id);
        if(el)
            setMobailPointer(el.id,el.coord,el.type,el.prop.bankId.uid);
    }

    function setMobailPointer(id,coord,type,baukUid) {
        var param={
            id: id,
            type: type
        };

        if(baukUid=='b00005')
            param.style={
                iconLayout: 'default#image',
                iconImageHref: '/local/templates/bz/img/placeholder.png',
                iconImageSize: [31, 41]
            };
        else
            param.style={
                iconLayout: 'default#image',
                iconImageHref: '/local/templates/bz/img/placeholder-empty.png',
                iconImageSize: [31, 41]
            };

        var placemark = new ymaps.Placemark(coord,param);

        if(type=='office')
        {
            modalMapOffice.geoObjects.removeAll();
            modalMapOffice.geoObjects.add(placemark);
            modalMapOffice.panTo([placemark.geometry.getCoordinates()]);
        }else if(type=='atm'){
            modalMapAtm.geoObjects.removeAll();
            modalMapAtm.geoObjects.add(placemark);
            modalMapAtm.panTo([placemark.geometry.getCoordinates()]);
        }

    }

    function addPoints(points) {
        data = points;
        placeholderCollectionZenit = new ymaps.GeoObjectCollection(null, {
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/bz/img/placeholder.png',
            iconImageSize: [31, 41],
        });

        placeholderCollection = new ymaps.GeoObjectCollection(null, {
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/bz/img/placeholder-empty.png',
            iconImageSize: [31, 41],
        });


        for (var i = 0; i < points.length; i++) {
            //Проверяем какую метку поставить для банков зенит и остальных
            if (points[i].prop.bankId.uid == 'b00005') {
                placeholderCollectionZenit.add(new ymaps.Placemark(points[i].coord, {
                    id: points[i].id,
                    type: points[i].type
                }));
            } else {
                placeholderCollection.add(new ymaps.Placemark(points[i].coord, {
                    id: points[i].id,
                    type: points[i].type
                }));
            }
        }

        myMap.geoObjects
            .add(placeholderCollection)
            .add(placeholderCollectionZenit);

        // Через коллекции можно подписываться на события дочерних элементов.
        placeholderCollectionZenit.events.add('click', openDialog);
        placeholderCollection.events.add('click', openDialog);

        // Через коллекции можно задавать опции дочерним элементам.
        placeholderCollectionZenit.options.set('preset', 'islands#PlaceholderDotIcon');
        placeholderCollection.options.set('preset', 'islands#PlaceholderDotIcon');
    }


    function openDialog(placemark) {
        var id = placemark.get("target").properties.get('id');
        var type = placemark.get("target").properties.get('type');
        var selector = (type == 'atm' ? '#modal_form-atm' : '#modal_form-office');
        Office.openDialog(id);
        ShowModal(selector, function () {
          OfficeMap.setMobailPointerById(id);
        });
    }


    function goToCity(cityName) {
        ymaps.geocode('Россия, ' + cityName + '', {results: 1}).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);
            myMap.panTo(firstGeoObject.geometry.getCoordinates());
        });
    }

    function addMultiRoute(id) {

        getUserLocation(function (userCoord) {
            if (userCoord) {
                var coord = getCoord(id);

                if (coord) {

                    if (route) {
                        myMap.geoObjects.remove(route);
                    }

                    ymaps.route([
                        userCoord, // точка отправления (Место положение пользователя)
                        coord // точка прибытия (Куда должен добраться пользователь)
                    ]).then(function (newRoute) {
                        route = newRoute;
                        myMap.geoObjects.add(newRoute);
                        var routePoints = route.getWayPoints(),
                            lastPoint = routePoints.getLength() - 1;
                        routePoints.options.set('preset', 'islands#redStretchyIcon');
                        routePoints.get(0).properties.set('islands#redStretchyIcon');
                        routePoints.get(lastPoint).properties.set('islands#redStretchyIcon');
                    });
                }
            } else {
                console.log("Нет местоположения пользователя");
            }
        });
    }

    /**
     * поиск координаты
     * @param id
     * @returns {boolean}
     */
    function getCoord(id) {
        for(var i = 0; i < data.length; i++) {
            if(data[i].id == id) {
                return data[i].coord;
            }
        }
        return false;
    }

    /**
     * поиск элемента
     * @param id
     * @returns {boolean}
     */
    function getElement(id) {
        for(var i = 0; i < data.length; i++) {
            if(data[i].id == id) {
                return data[i];
            }
        }
        return false;
    }


    /**
     * Определение координат пользователя
     * @param callback
     * @returns {*}
     */
    function getUserLocation(callback) {

        var geolocation = ymaps.geolocation;

        if (userLocation === null) {
            //для более точного получим из браузера если разрешено
            geolocation.get({
                provider: 'browser',
                mapStateAutoApply: true
            }).then(function (result) {
                userLocation = result.geoObjects.position;
                if (callback) {
                    callback(result.geoObjects.position);
                }
            }).fail(function () {
                //Если неудача то определим по примерно IP
                ymaps.geolocation.get({
                    provider: 'yandex',
                }).then(function (result) {
                    if (callback) {
                        callback(result.geoObjects.position);
                    }
                });
            });
        } else {

            callback(userLocation);
        }
    }


    return {
        init: init,
        initMap: initMap,
        addPoints: addPoints,
        addMultiRoute: addMultiRoute,
        getUserLocation: getUserLocation,
        goToCity: goToCity,
        setMobailPointer: setMobailPointer,
        setMobailPointerById: setMobailPointerById,
        myMap: function () {
            return myMap
        },
        removePoint: function () {
            if (placeholderCollection != null) {
                placeholderCollection.removeAll();
            }

            if (placeholderCollectionZenit != null) {
                placeholderCollectionZenit.removeAll();
            }
        },
        hasInit: function () {
            return hasInit;
        },

    }
})(ymaps);

