ymaps.ready(function(){
    var myMap=new ymaps.Map("map-bank", {
        center: [45.026137, 38.959094],
        zoom: 12,
        controls: [],
    }, {
        searchControlProvider: 'yandex#search'
    });
    mapObject.init(myMap);
});

var mapObject = {
    myMap:[],
    myСoords:[45.057104, 39.037046],
    route:false,
    PlaceholderCollection:false,
    PlaceholderCoords:[],
    getCoord:function(id){
        var res=false;
        $.each(_this.PlaceholderCoords,function(key,value){
            if(value.id==id)
                res=value.coord;
        });
        return res;
    },
    reInit:function(){
        var _this = this;
        //_this.PlaceholderCollection.removeAll(_this.route);

        console.log(_this.PlaceholderCollection.length);

        $.each(_this.PlaceholderCoords,function(key,value){
            _this.PlaceholderCollection.add(new ymaps.Placemark(value.coord,{
                id:value.id
            }));
        });
        if (_this.PlaceholderCollection.removeAll()) {
            console.log('test');
        }
        console.log(_this.PlaceholderCollection);
    },

    addMultiRoute:function(id){
        var _this = this;
        if(this.myСoords)
        {
            var coord =_this.getCoord(id);
            if(coord)
            {
                //console.log(id);
                if(_this.route)
                    _this.myMap.geoObjects.remove(_this.route);
                ymaps.route([
                    _this.myСoords, // точка отправления (Место положение пользователя)
                    coord // точка прибытия (Куда должен добраться пользователь)
                ]).then(function (route) {
                    _this.route=route;
                    _this.myMap.geoObjects.add(route);
                    var points = route.getWayPoints(),
                        lastPoint = points.getLength() - 1;
                    points.options.set('preset', 'islands#redStretchyIcon');
                    points.get(0).properties.set('islands#redStretchyIcon');
                    points.get(lastPoint).properties.set('islands#redStretchyIcon');
                });
            }
        } else {
            console.log("Нет местоположения пользователя");
        }

    },
    init:function(myMap) {
        _this = this;
        _this.myMap = myMap;
        _this.PlaceholderCollection = new ymaps.GeoObjectCollection(null, {
            iconLayout: 'default#image',
            iconImageHref: '../images/placeholder.png',
            iconImageSize: [31, 41],
        });


        ymaps.geolocation.get({
            provider: 'auto'
        }).then(function (result) {
            _this.coords = result.geoObjects.get(0).geometry.getCoordinates();
        });

        $.each(_this.PlaceholderCoords,function(key,value){
            _this.PlaceholderCollection.add(new ymaps.Placemark(value.coord,{
                id:value.id
            }));
        });

        _this.myMap.behaviors.disable('scrollZoom');
        _this.myMap.controls.add("zoomControl", {
            position: {top: 200, left: 30}
        });

        _this.myMap.geoObjects.add(_this.PlaceholderCollection);

        // Через коллекции можно подписываться на события дочерних элементов.
        _this.PlaceholderCollection.events.add('click', function (placemark) {
            var id=placemark.get("target").properties.get('id');
            //_this.addMultiRoute(id);
            ShowModal();
        });

        // Через коллекции можно задавать опции дочерним элементам.
        _this.PlaceholderCollection.options.set('preset', 'islands#PlaceholderDotIcon');
    }
}