<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Типовая страница");
?>
<?
use \Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/function.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/varmortgage.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/mortgage.js');
?>


    <div class="wr_block_type">
        <!--content_tab-->
        <div class="content_rates_tabs">
            <ul class="content_tab c-container">
                <li><a href="#content-tabs-1">Преимущества</a></li>
                <li><a href="#content-tabs-2">Информация и тарифы</a></li>
                <li><a href="#content-tabs-3">Как пользоваться</a></li>
            </ul>
            <div class="content_body" id="content-tabs-1">

                <div class="wr_block_type gray calculator">

                    <?require($_SERVER["DOCUMENT_ROOT"]."/demo/calculator/inner_mortgage.php");?>
                </div>
            </div>
            <div class="content_body" id="content-tabs-2">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="wr-toggle-light-text"><div class="toggle-light-text off">Кредитные</div><div class="toggle-light-wr"><div class="toggle toggle-light" data-toggle data-checked="Y" data-name="type"></div></div><div class="toggle-light-text on">Дебетовые</div></div>
                        <div class="content_body_left">
                            <div class="content_body_item">
                                <div class="content_body_pict site"></div>
                                <div class="content_body_body">
                                    <a href="#">Оставить заявку</a> онлайн прямо сайте
                                </div>
                            </div>
                        </div><div class="content_body_right">
                            <div class="content_body_item">
                                <div class="content_body_pict bank"></div>
                                <div class="content_body_body">
                                    <a href="#">В любом отделении</a> банка Зенит
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_body" id="content-tabs-3">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="content_body_left">
                            <div class="content_body_item title">
                                Безналичным переводом на текущий<br/>
                                или накопительный счет:
                            </div>
                            <div class="content_body_item">
                                <div class="content_body_pict site"></div>
                                <div class="content_body_body">
                                    <a href="#">Оставить заявку</a> онлайн прямо сайте
                                </div>
                            </div>
                            <div class="content_body_item">
                                <div class="content_body_pict mobile_bank"></div>
                                <div class="content_body_body">
                                    По телефонам:<br/>
                                    <a href="tel:+7 (495) 967-11-11">+7 (495) 967-11-11</a>; <a href="tel:8 (800) 500-66-77">8 (800) 500-66-77</a>
                                </div>
                            </div>
                        </div><div class="content_body_right">
                            <div class="content_body_item title">
                                Наличными:
                            </div>
                            <div class="content_body_item">
                                <div class="content_body_pict bank"></div>
                                <div class="content_body_body">
                                    <a href="#">В любом отделении</a> банка Зенит
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--content_tab-->
    </div>


    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Таблица горизонтальная</h1>
                <!--table-->
                <table class="tb min_table">
                    <tr>
                        <td>
                            Цель
                        </td>
                        <td>
                            Приобретение объектов недвижимости в Жилом комплексе<br/>
                            «SAMPO», возводимых ООО «Микрорайон «Кантри» по договору<br/>
                            участия в долевом строительстве<br/>
                            и договору купли-продажи
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Валюта кредита
                        </td>
                        <td>
                            Рубли РФ
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Срок кредита
                        </td>
                        <td>
                            От 3 лет и до достижения военнослужащим<br/>
                            возраста 45 лет
                        </td>
                    </tr>
                </table>
                <!--table-->
            </div>
        </div>
    </div>

    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Текстовый блок с заголовком, списком и ссылкой</h1>
                <div class="text_block">
                    <h2>Заголовок</h2>
                    <p>Одним из важных направлений деятельности является комплексное обслуживание корпоративных клиентов.</p>
                    <p>
                        Банк ЗЕНИТ активно занимается разработкой и продвижением розничных услуг
                        и предлагает своим клиентам - физическим лицам - полный спектр банковских программ:
                    </p>
                    <ul>
                        <li>вклады</li>
                        <li>ипотечные кредиты</li>
                        <li>потребительские кредиты</li>
                        <li>автокредиты</li>
                        <li>банковские карты</li>
                        <li>сейфовые ячейки</li>
                    </ul>
                    <p>Банк также занимает сильные позиции на рынке инвестиционных услуг и частных инвестиций (Private Banking).</p>
                </div>
            </div>
        </div>
    </div>

    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <h1>Таблица вертикальная</h1>
            <!--table-->
            <table class="responsive-stacked-table tb">
                <thead>
                <tr>
                    <td></td>
                    <td>Ставка</td>
                    <td>Срок</td>
                    <td>Валюта</td>
                    <td>Неснижаемый остаток</td>
                    <td>Дополнительно</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Максимальная выгода</td>
                    <td>7%</td>
                    <td>736 дней</td>
                    <td>Рубли РФ</td>
                    <td>30 000 Р</td>
                    <td>Пополнение</td>
                </tr>
                <tr>
                    <td>Дальновидный выбор</td>
                    <td>8%</td>
                    <td>30 — 90 дней</td>
                    <td>Рубли РФ</td>
                    <td>10 000 Р</td>
                    <td>
                        Снятие<br/>
                        Пополнение
                    </td>
                </tr>
                <tr>
                    <td>Большие возможности</td>
                    <td>9%</td>
                    <td>90 дней</td>
                    <td>Рубли РФ</td>
                    <td>1000 Р</td>
                    <td>Снятие</td>
                </tr>
                </tbody>
            </table>
            <!--table-->
        </div>
    </div>



    <div class="wr_block_type">
        <div class="block_type to_column">
            <!--banners-->
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/block/banners.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
            <!--banners-->
        </div>
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Преимущества простой стиль</h1>
                <!--simple_benefits-->
                <div class="simple_benefits">
                    <div class="simple_benefits_item">
                        <div class="pict sale"></div>
                        <div class="body">
                            <div class="title">
                                Скидки и привилегии
                            </div>
                            <div class="text">
                                Расплачиваясь Вашей картой VISA
                                Platinum «Мир путешествий», Вы
                                можете получать скидки до 50%
                                на предприятиях розничных сетей
                                наших партнёров.
                            </div>
                        </div>
                    </div><div class="simple_benefits_item">
                        <div class="pict men"></div>
                        <div class="body">
                            <div class="title">
                                Консьерж-сервис
                            </div>
                            <div class="text">
                                Консьержи осуществляют поиск
                                информации, находят нужного
                                поставщика товара/услуги,
                                предоставляя клиенту
                                консультацию. Услуга бесплатная и
                                доступна 24/7.
                            </div>
                        </div>
                    </div><div class="simple_benefits_item">
                        <div class="pict money"></div>
                        <div class="body">
                            <div class="title">
                                Экстренная выдача
                                наличных
                            </div>
                            <div class="text">
                                Находясь в поездке, практически
                                в любой стране мира, Вы можете
                                воспользоваться услугой
                                экстренной выдачи наличных
                            </div>
                        </div>
                    </div><div class="simple_benefits_item">
                        <div class="pict cart"></div>
                        <div class="body">
                            <div class="title">
                                Карта Priority Pass
                            </div>
                            <div class="text">
                                В дополнение к карте VISA Platinum
                                «Мир путешествий» Вы можете
                                совершенно бесплатно выпустить
                                карту Priority Pass.
                            </div>
                        </div>
                    </div>
                </div>
                <!--simple_benefits-->
            </div>
        </div>
    </div>


    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Ненумерованный список</h1>
                <ul class="big_list">
                    <li>
                        по картам VISA CLASSIC «Подари детям улыбку» - 0,3% от суммы операций, совершенных по карте за истекший календарный день, но не более 5 рублей,
                        либо 50 рублей, либо 200 рублей по каждой операции (по выбору держателя карты)
                    </li>
                    <li>
                        о картам VISA GOLD «Подари детям улыбку» - 0,3 % от суммы операций, совершенных по карте за истекший календарный день, но не более 50 рублей либо 200 рублей
                        по каждой операции (по выбору держателя карты) - программа «Стандарт»
                    </li>
                    <li>
                        по картам VISA GOLD «Подари детям улыбку» - 1 % от суммы операций, совершенных по карте за истекший календарный день, но не менее 50 рублей по каждой операции
                        и не более 3 000 рублей по каждой операции - программа «VIP»
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <h1>Преимущества промо стиль</h1>
            <!--promo_benefits-->
            <div class="promo_benefits">
                <div class="promo_benefits_item">
                    <div class="pict sale"></div>
                    <div class="body">
                        <div class="title">
                            Скидки и привилегии
                        </div>
                        <div class="text">
                            Расплачиваясь Вашей картой VISA
                            Platinum «Мир путешествий», Вы
                            можете получать скидки до 50%
                            на предприятиях розничных сетей
                            наших партнёров.
                        </div>
                    </div>
                </div><div class="promo_benefits_item">
                    <div class="pict men"></div>
                    <div class="body">
                        <div class="title">
                            Консьерж-сервис
                        </div>
                        <div class="text">
                            Консьержи осуществляют поиск
                            информации, находят нужного
                            поставщика товара/услуги,
                            предоставляя клиенту
                            консультацию. Услуга бесплатная и
                            доступна 24/7.
                        </div>
                    </div>
                </div><div class="promo_benefits_item">
                    <div class="pict money"></div>
                    <div class="body">
                        <div class="title">
                            Экстренная выдача
                            наличных
                        </div>
                        <div class="text">
                            Находясь в поездке, практически
                            в любой стране мира, Вы можете
                            воспользоваться услугой
                            экстренной выдачи наличных
                        </div>
                    </div>
                </div><div class="promo_benefits_item">
                    <div class="pict cart"></div>
                    <div class="body">
                        <div class="title">
                            Карта Priority Pass
                        </div>
                        <div class="text">
                            В дополнение к карте VISA Platinum
                            «Мир путешествий» Вы можете
                            совершенно бесплатно выпустить
                            карту Priority Pass.
                        </div>
                    </div>
                </div>
            </div>
            <!--promo_benefits-->
        </div>
    </div>


    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">
                <div class="wr_max_block">
                    <h1>
                        Доп. информация
                        <div class="note_text">
                            Размещается всегда в крайнем правом блоке
                        </div>
                    </h1>
                    <h2>Связаться с банком</h2>
                    <div class="form_application_line">
                        <div class="contacts_block">
                            <a href="mailto:email@zenit.ru">email@zenit.ru</a><br/>
                            +7 (495) 967-11-11<br/>
                            8 (800) 500-66-77
                        </div>
                        <div class="note_text">
                            звонок по России бесплатный
                        </div>
                    </div>
                    <a href="#" class="button bigmaxwidth">задать вопрос</a>
                </div>
                <div class="wr_max_block">
                    <h2>Подписка на новости</h2>
                    <form action="">
                        <div class="form_application_line">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div class="text">Имя</div>
                                    <input type="text" name="name" class="simple_input" placeholder="" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div class="text">e-mail</div>
                                    <input type="text" name="email" class="simple_input" placeholder="" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line">
                            <input class="button bigmaxwidth" value="подписаться" type="submit">
                        </div>
                    </form>
                </div>
                <div class="wr_max_block">
                    <div class="right_top_line">
                        <div class="block_top_line"></div>
                        <ul class="right_menu">
                            <li><a href="#">Устав Банка</a></li>
                            <li><a href="#">Реквизиты</a></li>
                            <li><a href="#">Вакансии</a></li>
                            <li><a href="#">Акционерный капитал</a></li>
                            <li><a href="#">Для инвесторов</a></li>
                            <li><a href="#">Социальные проекты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="block_type_center">
                <!--photo-->
                <h1>Фотогалерея</h1>
                <div class="photo_blocks">
                    <div class="photo_blocks_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/photo.png');"></div>
                        <div class="text">
                            Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                            за 2016 год,  выросло на 32%
                        </div>
                    </div>
                    <div class="photo_blocks_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/photo.png');"></div>
                        <div class="text">
                            Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                            за 2016 год,  выросло на 32%
                        </div>
                    </div>
                    <div class="photo_blocks_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/photo.png');"></div>
                        <div class="text">
                            Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                            за 2016 год,  выросло на 32%
                        </div>
                    </div>
                    <div class="photo_blocks_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/photo.png');"></div>
                        <div class="text">
                            Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                            за 2016 год,  выросло на 32%
                        </div>
                    </div>
                    <div class="photo_blocks_item">
                        <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/photo.png');"></div>
                        <div class="text">
                            Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                            за 2016 год,  выросло на 32%
                        </div>
                    </div>
                    <div class="wr_caption">
                        <div class="caption">
                            <div class="caption_num">1</div>/<div class="caption_all">5</div>
                        </div>
                        <div class="caption_text">
                            Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                            за 2016 год,  выросло на 32%
                        </div>
                    </div>
                </div>
                <!--photo-->
                <!--video-->
                <h1>Видео</h1>
                <div class="video">
                    <iframe src="https://www.youtube.com/embed/N8XCFigfeKY"
                            frameborder="0" allowfullscreen></iframe>
                    <div class="video_text">
                        Количество транзакций в торговых точках, обслуживаемых Банком ЗЕНИТ,
                        за 2016 год,  выросло на 32%
                    </div>
                </div>
                <!--video-->
            </div>
        </div>
    </div>










    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">
                <div class="right_top_line">
                    <div class="block_top_line"></div>
                    <div class="right_bank_block">
                        <div class="right_bank_title">
                            5 банков
                        </div>
                        <div class="right_bank_text">
                            Входят в банковскую<br/>
                            группу зенит
                        </div>
                    </div>
                    <div class="right_bank_block">
                        <div class="right_bank_title">
                            А уровень
                        </div>
                        <div class="right_bank_text">
                            Надежности<br/>
                            и стабильности Банка
                        </div>
                    </div>
                    <div class="right_bank_block">
                        <div class="right_bank_title">
                            23 года
                        </div>
                        <div class="right_bank_text">
                            На рынке банковских<br/>
                            услуг
                        </div>
                    </div>
                </div>
            </div>
            <div class="block_type_center">
                <h1>Документы для скачивания</h1>
                <div class="text_block">
                    <!--doc-->
                    <div class="doc_list">
                        <a href="#" class="doc_item">
                            <div class="doc_pict pdf"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Бонусные программы
                                </div>
                                <div class="doc_note">
                                    32 Мб
                                </div>
                            </div>
                        </a><a href="#" class="doc_item">
                            <div class="doc_pict xls"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Заявление о предоставлении
                                    Услуги «Торговый эквайринг»
                                </div>
                                <div class="doc_note">
                                    14 Кб
                                </div>
                            </div>
                        </a><a href="#" class="doc_item">
                            <div class="doc_pict pdf"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Тарифы за услуги для юридических
                                    лиц и индивидуальных
                                    предпринимателей за
                                    расчетно-кассовое
                                    обслуживание
                                </div>
                                <div class="doc_note">
                                    2 Мб
                                </div>
                            </div>
                        </a><a href="#" class="doc_item">
                            <div class="doc_pict doc"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Правила оказания ПАО Банк
                                    ЗЕНИТ юридическим лицам
                                    и индивидуальным предпри-
                                    нимателям Услуги «Торговый
                                    эквайринг»
                                </div>
                                <div class="doc_note">
                                    12 Кб
                                </div>
                            </div>
                        </a>
                    </div>
                    <!--doc-->
                </div>
            </div>
        </div>
    </div>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Предупреждения</h1>
                <div class="warning">
                    <div class="pict circle"></div>
                    <div class="text">
                        Наличие текущего (со сроком обслуживания не менее 6 мес.) или погашенный кредит в Банке
                        ЗЕНИТ, отсутствие просроченных платежей длительностью более 30 дней и не более 1
                        случая просроченных платежей в полугодии
                    </div>
                </div>
                <div class="warning">
                    <div class="pict triangle"></div>
                    <div class="text">
                        Наличие текущего (со сроком обслуживания не менее 6 мес.) или погашенный кредит в Банке
                        ЗЕНИТ, отсутствие просроченных платежей длительностью более 30 дней и не более 1
                        случая просроченных платежей в полугодии
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>
                    Разводящий блок по разделам
                    <div class="note_text">
                        Изображение ховера зависит от имиджа страницы
                    </div>
                </h1>
                <!--ipoteka_list-->
                <div class="ipoteka_list">
                    <div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="#" class="ipoteka_item_title">
                                Переводы с карты на карту
                            </a>
                            <div class="ipoteka_item_text">
                                Быстрый перевод денежных средств
                                на карту любого банка
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="#" class="ipoteka_item_title">
                                Переводы Western Union
                            </a>
                            <div class="ipoteka_item_text">
                                Возможность совершать переводы
                                в рублях и иностранной валюте
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a  href="#" class="ipoteka_item_title">
                                Переводы в системе "Город"
                            </a>
                            <div class="ipoteka_item_text">
                                Денежные переводы без открытия счета
                                для оплаты более 3 400 услуг
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="#" class="ipoteka_item_title">
                                Переводы “Золотая корона”
                            </a>
                            <div class="ipoteka_item_text">
                                Денежные переводы по России,
                                странам ближнего и дальнего зарубежья
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a  href="#" class="ipoteka_item_title">
                                Переводы без открытия
                                банковских счетов
                            </a>
                            <div class="ipoteka_item_text">
                                Переводы в рублях и иностранной валюте
                                без открытия банковского счета
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="#" class="ipoteka_item_title">
                                Реквизиты для переводов
                                на счета частных лиц
                            </a>
                            <div class="ipoteka_item_text">
                                Платежные реквизиты для перевода
                                на счет в долларах, евро или рублях
                            </div>
                        </div>
                    </div>
                </div>
                <!--ipoteka_list-->
            </div>
        </div>
    </div>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Список шагов (Как оформить и пр.)</h1>
                <!--step_block-->
                <div class="step_block">
                    <div class="step_item">
                        <div class="step_num">1</div>
                        <div class="step_body">
                            <div class="step_title">
                                Оформить заявку на сайте
                            </div>
                            <div class="step_text">
                                Подать заявку на предоставление кредитного продукта и получить
                                положительное решение Банка.
                            </div>
                        </div>
                    </div>
                    <div class="step_item">
                        <div class="step_num">2</div>
                        <div class="step_body">
                            <div class="step_title">
                                Подтвердить информацию по e–mail
                            </div>
                            <div class="step_text">
                                Подать заявку на предоставление кредитного продукта и получить
                                положительное решение Банка.
                            </div>
                        </div>
                    </div>
                    <div class="step_item">
                        <div class="step_num">3</div>
                        <div class="step_body">
                            <div class="step_title">
                                Принести документы в банк
                            </div>
                            <div class="step_text">
                                После подтверждения информации, необходимо предоставить оригиналы
                                всех документов в удобный <a href="#">офис банка ЗЕНИТ.</a>
                            </div>

                        </div>
                    </div>
                </div>
                <a href="#" class="button bigmaxwidth">подписаться</a>
                <!--step_block-->
            </div>
        </div>
    </div>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>Часто задаваемые вопросы</h1>
                <div class="text_block">
                    <!--faq-->
                    <div class="faq">
                        <div class="faq_item">
                            <div class="faq_top">
                                <div class="faq_pict"><div class="faq_arr"></div></div>
                                <div class="faq_top_text">Как узнать, сколько процентов начислено по вкладу?</div>
                            </div>
                            <div class="faq_text">
                                Вкладчик вправе открыть любое количество счетов по вкладам.
                                Единственно исключение составляют граждане стран СНГ.
                            </div>
                        </div>
                        <div class="faq_item open">
                            <div class="faq_top">
                                <div class="faq_pict"><div class="faq_arr"></div></div>
                                <div class="faq_top_text">Могу ли я открыть на свое имя сразу несколько вкладов?</div>
                            </div>
                            <div class="faq_text" style="display: block">
                                Вкладчик вправе открыть любое количество счетов по вкладам.
                                Единственно исключение составляют граждане стран СНГ.
                            </div>
                        </div>
                        <div class="faq_item">
                            <div class="faq_top">
                                <div class="faq_pict"><div class="faq_arr"></div></div>
                                <div class="faq_top_text">Как осуществляется выплата процентов?</div>
                            </div>
                            <div class="faq_text">
                                Вкладчик вправе открыть любое количество счетов по вкладам.
                                Единственно исключение составляют граждане стран СНГ.
                            </div>
                        </div>
                        <div class="faq_item">
                            <div class="faq_top">
                                <div class="faq_pict"><div class="faq_arr"></div></div>
                                <div class="faq_top_text">На какой срок можно открыть вклад в подразделении банка?</div>
                            </div>
                            <div class="faq_text">
                                Вкладчик вправе открыть любое количество счетов по вкладам.
                                Единственно исключение составляют граждане стран СНГ.
                            </div>
                        </div>
                    </div>
                    <!--faq-->
                </div>
            </div>
        </div>
        <div class="block_type to_column c-container">
            <div class="block_type_right">
                <div class="form_application_line">
                    <div class="right_top_line">
                        <div class="block_top_line"></div>
                        <div class="right_bank_block">
                            <div class="right_bank_title">
                                от 22 до 65
                            </div>
                            <div class="right_bank_text">
                                Возраст заемщика
                            </div>
                        </div>
                        <div class="right_bank_block">
                            <div class="right_bank_title">
                                12 мес.
                            </div>
                            <div class="right_bank_text">
                                Общий трудовой стаж
                            </div>
                        </div>
                        <div class="right_bank_block">
                            <div class="right_bank_title">
                                не менее 3 мес.
                            </div>
                            <div class="right_bank_text">
                                Стаж на последнем<br/>
                                или текущем месте работы
                            </div>
                        </div>
                        <div class="right_bank_block">
                            <div class="right_bank_title">
                                не менее 12 мес.
                            </div>
                            <div class="right_bank_text">
                                Стаж на дополнительном<br/>
                                месте работы
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_application_line">
                    <!--doc-->
                    <div class="doc_list">
                        <a href="#" class="doc_item">
                            <div class="doc_pict pdf"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Все требования
                                </div>
                                <div class="doc_note">
                                    32 Мб
                                </div>
                            </div>
                        </a>
                    </div>
                    <!--doc-->
                </div>
                <div class="form_application_line">
                    <a href="#" class="button bigmaxwidth">Оставить заявку</a>
                </div>
            </div>
            <div class="block_type_center">
                <h1>Условия и требования</h1>
                <!--conditions-->
                <div class="conditions">
                    <div class="conditions_item">
                        <div class="conditions_item_title">
                            600 <span>тыс</span> — 60 <span>млн.</span>
                        </div>
                        <div class="conditions_item_text">
                            Размер кредита
                        </div>
                    </div><div class="conditions_item">
                        <div class="conditions_item_title">
                            <span>от</span> 10 %
                        </div>
                        <div class="conditions_item_text">
                            первый взнос
                        </div>
                    </div><div class="conditions_item">
                        <div class="conditions_item_title">
                            10,5%
                        </div>
                        <div class="conditions_item_text">
                            ставка
                        </div>
                    </div>
                </div>
                <!--conditions-->
                <div class="text_block">
                    <h2>Заголовок</h2>
                    <p>
                        Приобретение физическими лицами объектов недвижимости (квартира,
                        комната, жилой дом с земельным участком, земельный участок),
                        находящейся в собственности Банка
                    </p>
                    <h2>Обеспечение кредита</h2>
                    <ul>
                        <li>
                            Залог (ипотека) приобретаемой недвижимости
                            <div class="note_text">и/или</div>
                        </li>
                        <li>
                            Поручительство юридического лица**
                            <div class="note_text">и/или</div>
                        </li>
                        <li>
                            Поручительство физических лиц**
                        </li>
                    </ul>
                    <div class="note_text">
                        В том случае, если кредит предоставляется клиенту, состоящему в браке,
                        оформляется поручительство супруга/супруги на весь срок кредитования (за
                        исключением случаев, когда супруги рассматриваются в качестве солидарных
                        заемщиков или заключен брачный договор, подтверждающий раздельный режим
                        собственности на предмет залога)
                    </div>
                </div>
            </div>
        </div>

        <?$APPLICATION->IncludeComponent(
            "bitrix:form.result.new",
            "universal",
            array(
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "N",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "EDIT_URL" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "LIST_URL" => "",
                "SEF_MODE" => "N",
                "SUCCESS_URL" => "",
                "USE_EXTENDED_ERRORS" => "Y",
                "WEB_FORM_ID" => "1",
                "COMPONENT_TEMPLATE" => "universal",
                "SOURCE_TREATMENT" => "Заявка на Ипотечный кредит: ".$arResult["NAME"],
                "RIGHT_TEXT" => "Заполните заявку и получите кредит<br/>в течение 2 дней. Это займет<br/>не более  10 минут.",
                "VARIABLE_ALIASES" => array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID",
                )
            ),
            false
        );?>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>