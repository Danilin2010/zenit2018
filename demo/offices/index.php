<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Банкоматы и офисы");
?>
<?
use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
Asset::getInstance()->addCss('/demo/offices/px/px.css');
Asset::getInstance()->addCss('/demo/css/magnific-popup.css');
//Asset::getInstance()->addJs('');
Asset::getInstance()->addJs('/demo/js/jquery.magnific-popup.js');
Asset::getInstance()->addJs('/demo/js/custom-ya.js');
Asset::getInstance()->addJs('http://api-maps.yandex.ru/2.1/?lang=ru_RU');
?>

<div id="filtr-atm-ofice-all" class="main_content">
    <div class="container basic-filter">
        <!-- Фильтр в шапке -->
        <div id="atm-ofice-filtr" class="atm-ofice-full-filtr">
            <div id="filtr-header">
                <div class="row">
                    <div class="col-sm-12 col-md-6 switching-offices-atm">
                        <div class="wr-toggle-light-text text-fixed mobile-offices-cash">
                            <div id="offices-linck" class="toggle-light-text on">
                                <span class="h-1">Офисы</span>
                            </div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N" data-name="type_cash_list"></div>
                            </div>
                            <div id="cash-dispenser-linck" class="toggle-light-text off">
                                <span class="h-1">Банкоматы</span>
                            </div>
                        </div>
                    </div>
                    <div id="mb-search-adres" class="col-sm-12 col-mb-10 col-mt-11 col-md-3 hidden-lg">
                        <div class="block-filtr-list select">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <input type="text" name="" class="simple_input" placeholder="" value="">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                                <div class="text search">Введите адрес</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-2 col-mt-1 col-md-3 hidden-lg">
                        <a href="#modal_form_filtr" class="open_modal open-mobile-filtr">
                            <i class="fa fa-sliders" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-12 col-mb-12 col-mt-12 col-md-3 text-right switch-list-card">
                        <div class="wr-toggle-light-text text-fixed">
                            <div id="tab-1" class="toggle-light-text on">
                                <span class="h-1 hidden-xs hidden-sm">списком</span>
                                <i class="fa fa-list hidden-lg" aria-hidden="true"></i>
                            </div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N" data-name="type_visual"></div>
                            </div>
                            <div id="tab-2" class="toggle-light-text off">
                                <span class="h-1 hidden-xs hidden-sm">на карте</span>
                                <i class="fa fa-map-o hidden-lg" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 hidden-xs hidden-sm pl-0">
                        <div id="filtr-bank" class="jq-selectbox jqselect formstyle bank">
                            <select data-programm="" data-plasholder="Банк группы ЗЕНИТ" data-title="Банк группы ЗЕНИТ" class="formstyle" name="programm">
                                <option value="all" selected="">Все банки</option>
                                <option value="bank-1">Банк 1</option>
                                <option value="bank-2">Банк 2</option>
                                <option value="bank-3">Банк 3</option>
                                <option value="bank-4">Банк 4</option>
                                <option value="bank-5">Банк 5</option>
                                <option value="bank-6">Банк 6</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="atm-ofice" class="atm-ofice-full">
        <!-- Отображение на карте -->
        <div id="map-bank" class="offices hidden">
            <!-- Отображение офисов на карте -->
            <div id="cash-dispenser-map" class="container">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12 col-md-4 col-left">
                        <div class="block-filtr filtr-block-1">
                            <div id="offices-map-filtr">
                                <a type="atm" href="#modal_form-office" id="placeholder-1" class="open_modal marshrut hover">
                                    <div class="card-block data">
                                        <h3 class="title-ofice">Главный офис</h3>
                                        <p>м. Проспект мира<br>
                                            Банный переулок, дом 9</p>
                                    </div>
                                </a>
                                <a type="atm" href="#modal_form-office" id="placeholder-2" class="open_modal marshrut hover">
                                    <div class="card-block data">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                        <p>м. Савеловская, Динамо<br>
                                            ул. Башиловская, д. 2, стр. 3<br>
                                            Автосалон "Ниссан" </p>
                                    </div>
                                </a>
                                <a type="atm" href="#modal_form-office" id="placeholder-3" class="open_modal marshrut hover">
                                    <div class="card-block data">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                        <p>м. Проспект мира<br>
                                            Банный переулок, дом 9</p>
                                    </div>
                                </a>
                                <a type="atm" href="#modal_form-office" id="placeholder-4" class="open_modal marshrut hover">
                                    <div class="card-block data">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                        <p>м. Савеловская, Динамо<br>
                                            ул. Башиловская, д. 2, стр. 3</p>
                                    </div>
                                </a>
                                <a type="atm" href="#modal_form-office" id="placeholder-5" class="open_modal marshrut hover">
                                    <div class="card-block data">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                        <p>м. Проспект мира<br>
                                            Банный переулок, дом 9</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="block-filtr filtr-block-2">
                            <div class="placeholder">
                                <span class="placeholder-icon"></span>
                                <span class="text">Банк Зенит</span>
                            </div>
                            <div class="placeholder">
                                <span class="placeholder-empty-icon"></span>
                                <span class="text">Банковская группа ЗЕНИТ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-right">
                        <div class="block-filtr filtr-block-3">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div id="map-search-adres"  class="complex_input_body">
                                        <input type="text" name="" class="simple_input" placeholder="" value="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text search">поиск по адресу или метро</div>
                                </div>
                            </div>
                        </div>
                        <div class="block-filtr filtr-block-5">
                            <div id="map-services" class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Услуги" data-title="Услуги" class="formstyle" name="programm">
                                    <option value="individuals">Частным лицам</option>
                                    <option value="jur-individuals">Юр. лицам</option>
                                </select>
                            </div>
                        </div>
                        <div id="maps-local-arm" class="block-filtr filtr-block-6">
                            <label><input type="checkbox" class="formstyle" name="of-11" value="of-11">Работает сейчас</label>
                            <label><input type="checkbox" class="formstyle" name="of-22" value="of-22">Ближайший</label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Отображение банкоматов на карте -->
            <div id="offices-map" class="container hidden">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12 col-md-4 col-left">
                        <div class="block-filtr filtr-block-1">
                            <a type="office" href="#modal_form-atm" id="placeholder-6" class="open_modal marshrut hover">
                                <div class="card-block data">
                                    <h3>Круглосуточный банкомат</h3>
                                    <p>м. Проспект мира<br>
                                        Банный переулок, дом 9</p>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-atm" id="placeholder-7" class="open_modal marshrut hover">
                                <div class="card-block data">
                                    <h3>Круглосуточный банкомат</h3>
                                    <p>м. Парк Победы<br>
                                        Московская область,<br>
                                        Одинцовский район,<br>
                                        рабочий поселок Новоивановское,<br>
                                        ул. Луговая, д. 1<br>
                                        Торговый комплекс "ТРИ КИТА", 2 этаж </p>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-atm" id="placeholder-8" class="open_modal marshrut hover">
                                <div class="card-block data">
                                    <h3>Круглосуточный банкомат</h3>
                                    <p>м. Проспект мира<br>
                                        Банный переулок, дом 9</p>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-atm" id="placeholder-9" class="open_modal marshrut hover">
                                <div class="card-block data">
                                    <h3>Круглосуточный банкомат</h3>
                                    <p>м. Савеловская, Динамо<br>
                                        ул. Башиловская, д. 2, стр. 3</p>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-atm" id="placeholder-10" class="open_modal marshrut hover">
                                <div class="card-block data">
                                    <h3>Круглосуточный банкомат</h3>
                                    <p>м. Парк Победы<br>
                                        Московская область,<br>
                                        Одинцовский район,<br>
                                        рабочий поселок Новоивановское,<br>
                                        ул. Луговая, д. 1<br>
                                        Торговый комплекс "ТРИ КИТА", 2 этаж </p>
                                </div>
                            </a>
                        </div>
                        <div class="block-filtr filtr-block-2">
                            <div class="placeholder">
                                <span class="placeholder-icon"></span>
                                <span class="text">Банк Зенит</span>
                            </div>
                            <div class="placeholder">
                                <span class="placeholder-empty-icon"></span>
                                <span class="text">Банковская группа ЗЕНИТ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-right">
                        <div class="block-filtr filtr-block-3">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div id="maps-search-adres" class="complex_input_body">
                                        <input type="text" name="" class="simple_input" placeholder="" value="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text search">поиск по адресу или метро</div>
                                </div>
                            </div>
                        </div>
                        <div id="maps-atmstatus" class="block-filtr filtr-block-4">
                            <label><input type="checkbox" class="formstyle" name="b-11" value="b-11">Работает сейчас</label>
                            <label><input type="checkbox" class="formstyle" name="b-22" value="b-22">Круглосуточно</label>
                            <label><input type="checkbox" class="formstyle" name="b-33" value="b-33">Ближайший</label>
                            <label><input type="checkbox" class="formstyle" name="b-44" value="b-44">Прием наличных</label>
                            <label><input type="checkbox" class="formstyle" name="b-55" value="b-55">Обмен валюты</label>
                            <label><input type="checkbox" class="formstyle" name="b-66" value="b-66">Снятие в валюте</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Отображение списком -->
        <div id="list-bank" class="offices">
            <!-- Отображение офисов -->
            <div id="offices" class="container">
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-9 list-bank-mobile ">
                        <div id="offices-list-filtr">
                            <a type="office" href="#modal_form-office" id="placeholder-1" class="open_modal open_modal_mobile hover">
                                <div class="card-block-list views-row-2">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one">8 (495) 937-07-37,</div>
                                            <div class="phone-two">8 (495) 777-57-07</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-office" id="placeholder-2" class="open_modal open_modal_mobile hover">
                                <div class="card-block-list views-row-3">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one">8 (495) 937-07-37,</div>
                                            <div class="phone-two">8 (495) 777-57-07</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-office" id="placeholder-3" class="open_modal open_modal_mobile hover">
                                <div class="card-block-list views-row-4">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice">Головной офис</h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one">8 (495) 937-07-37,</div>
                                            <div class="phone-two">8 (495) 777-57-07</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-office" id="placeholder-4" class="open_modal open_modal_mobile hover">
                                <div class="card-block-list views-row-5">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one">8 (495) 937-07-37,</div>
                                            <div class="phone-two">8 (495) 777-57-07</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a type="office" href="#modal_form-office" id="placeholder-5" class="open_modal open_modal_mobile hover">
                                <div class="card-block-list views-row-6">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one">8 (495) 937-07-37,</div>
                                            <div class="phone-two">8 (495) 777-57-07</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="row full-linck mb-5">
                            <div class="col-sm-12 col-md-12">
                                <a href="#" class="button bigmaxwidth">показать все офисы</a>
                            </div>
                        </div>
                    </div>
                    <!-- Фильтр офисов -->
                    <div id="form-list-ofis" class="col-sm-12 col-md-3 hidden-xs hidden-sm">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div class="wr_complex_input complex_input_noicon">
                                        <div class="complex_input_body">
                                            <div id="search-adres" class="complex_input_body">
                                                <input type="text" name="search-list-ofis" class="simple_input" placeholder="" value="">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </div>
                                            <div class="text search">поиск по адресу или метро</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div id="user-type" class="jq-selectbox jqselect formstyle">
                                        <select data-programm="" data-plasholder="Услуги" data-title="Услуги" class="formstyle" name="programm">
                                            <option value="individuals">Частным лицам</option>
                                            <option value="jur-individuals">Юр. лицам</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div id="ofisstatus" class="block-filtr-list">
                                    <label><input type="checkbox" class="formstyle" name="of-11" value="of-11">Работает сейчас</label>
                                    <label><input type="checkbox" class="formstyle" name="of-22" value="of-22">Ближайший</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Отображение банкоматов -->
            <div id="cash-dispenser" class="container hidden">
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-9 list-bank-mobile ">
                        <a type="atm" href="#modal_form-atm" id="placeholder-6" class="open_modal open_modal_mobile hover">
                            <div class="card-block-list views-row-1">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="round-clock">Круглосуточно</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="currency">Рубли РФ</div>
                                        <div class="cash-acceptance">Cash-in (прием наличных)</div>
                                        <div class="free"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a type="atm" href="#modal_form-atm" id="placeholder-7" class="open_modal open_modal_mobile hover">
                            <div class="card-block-list views-row-2">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice">Банкомат</h3>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                        <div class="weekend">сб. 10:00 – 17:00</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="currency">Рубли РФ</div>
                                        <div class="free"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a type="atm" href="#modal_form-atm" id="placeholder-8" class="open_modal open_modal_mobile hover">
                            <div class="card-block-list views-row-3">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice">Банкомат</h3>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                        <div class="weekend">сб. 10:00 – 17:00</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="currency">Рубли РФ</div>
                                        <div class="cash-acceptance">Cash-in (прием наличных)</div>
                                        <div class="currency-exchange">Обмен валюты</div>
                                        <div class="free"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a type="atm" href="#modal_form-atm" id="placeholder-9" class="open_modal open_modal_mobile hover">
                            <div class="card-block-list views-row-4">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="round-clock">Круглосуточно</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="currency">Рубли РФ</div>
                                        <div class="cash-acceptance">Cash-in (прием наличных)</div>
                                        <div class="free"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a type="atm" href="#modal_form-atm" id="placeholder-10" class="open_modal open_modal_mobile hover">
                            <div class="card-block-list views-row-5">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                    </div>
                                    <div class="ccol-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                        <div class="weekend">сб. 10:00 – 17:00</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="currency">Рубли РФ</div>
                                        <div class="free"></div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <div class="row full-linck mb-5">
                            <div class="col-sm-12 col-md-12">
                                <a href="#" class="button bigmaxwidth">показать все банкоматы</a>
                            </div>
                        </div>
                    </div>
                    <!-- Фильтр банкоматов -->
                    <div id="form-list-arm" class="col-sm-12 col-md-3 hidden-xs hidden-sm">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div class="wr_complex_input complex_input_noicon">
                                        <div class="complex_input_body">
                                            <div id="search-adres" class="complex_input_body">
                                                <input type="text" name="search-adres-arm" class="simple_input" placeholder="" value="">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </div>
                                            <div class="text search">поиск по адресу или метро</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div id="atmstatus" class="block-filtr-list">
                                    <label><input type="checkbox" class="formstyle" name="b-11" value="b-11">Работает сейчас</label>
                                    <label><input type="checkbox" class="formstyle" name="b-22" value="b-22">Круглосуточно</label>
                                    <label><input type="checkbox" class="formstyle" name="b-33" value="b-33">Ближайший</label>
                                    <label><input type="checkbox" class="formstyle" name="b-44" value="b-44">Прием наличных</label>
                                    <label><input type="checkbox" class="formstyle" name="b-55" value="b-55">Обмен валюты</label>
                                    <label><input type="checkbox" class="formstyle" name="b-66" value="b-66">Снятие в валюте</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <!-- Модальное окно для отображения информации на банкомата -->
    <div class="modal-open modal">
        <div id="modal_form-atm" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header hidden-xs hidden-sm">
                        <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h4 class="modal-title" id="exampleModalLabel">Круглосуточный банкомат</h4>
                    </div>
                    <div class="modal-header hidden-lg">
                        <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h4 class="modal-title" id="exampleModalLabel">Выбранный банкомат</h4>
                    </div>
                    <div class="mobile-modal-content">
                        <div class="modal-body">
                            <div class="modal-content-top">
                                <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel">Круглосуточный банкомат</h4>
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close close_marshrut">Построение маршрута</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Модальное окно для отображения информации офиса -->
    <div class="modal-open modal">
        <div id="modal_form-office" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header hidden-xs hidden-sm">
                        <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h4 class="modal-title" id="exampleModalLabel">Торгово-развлекательный центр "Европейский"</h4>
                    </div>
                    <div class="modal-header hidden-lg">
                        <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h4 class="modal-title" id="exampleModalLabel">Выбранное отделение</h4>
                    </div>
                    <div class="mobile-modal-content">
                        <div class="modal-body">
                            <div class="modal-content-top">
                                <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel">Торгово-развлекательный центр "Европейский"</h4>
                                <div class="adres">
                                    м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                    Торгово-развлекательный центр "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close close_marshrut">Построение маршрута</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Мобильный фильтр офисов и бпнкоматов -->
    <div id="modal_form_filtr" class="modal_div mobile">
        <div class="row">
            <div class="col-sm-12 col-md-12 modal_fon pb-0">
                <div class="row py-3 top-modal">
                    <div class="col-sm-12 col-md-12 px-5">
                        <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h4 class="modal-title">Фильтр</h4>
                    </div>
                </div>
                <div class="row content-modal">
                    <div class="col-sm-12 col-md-12 px-5">
                        <div id="bank-filtr" class="jq-selectbox jqselect formstyle bank">
                            <select data-programm="" data-plasholder="Банк группы ЗЕНИТ" data-title="Банк группы ЗЕНИТ" class="formstyle" name="programm">
                                <option value="all" selected="">Все банки</option>
                                <option value="bank-1">Банк 1</option>
                                <option value="bank-2">Банк 2</option>
                                <option value="bank-3">Банк 3</option>
                                <option value="bank-4">Банк 4</option>
                                <option value="bank-5">Банк 5</option>
                                <option value="bank-6">Банк 6</option>
                            </select>
                        </div>
                        <div id="works" class="block-filtr-list row">
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="works-now" value="works-now">Работает сейчас</label>
                            </div>
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="round-clock" value="round-clock">Круглосуточно</label>
                            </div>
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="nearest" value="nearest">Ближайший</label>
                            </div>
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="cceptance-cash" value="acceptance-cash">Прием наличных</label>
                            </div>
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="currency-exchange" value="currency-exchange">Обмен валюты</label>
                            </div>
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="withdrawal-currency" value="withdrawal-currency">Снятие в валюте</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row footer-madal">
                    <div class="col-sm-12 col-md-12 px-5">
                        <div class="form_application_line">
                            <input class="button bigmaxwidth" value="Применить фильтр" type="submit">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "contact",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "Y",
        "WEB_FORM_ID" => "2",
        "COMPONENT_TEMPLATE" => "universal",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
);
?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "contact",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "Y",
        "WEB_FORM_ID" => "2",
        "COMPONENT_TEMPLATE" => "universal",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
);
?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "contact",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "Y",
        "WEB_FORM_ID" => "2",
        "COMPONENT_TEMPLATE" => "universal",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>