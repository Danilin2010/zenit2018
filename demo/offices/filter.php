<?php

$arr=array(
    array(
        'id'=>'placeholder-1',
        'coord'=>array(45.026137, 38.959094),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="round-clock">Круглосуточно</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>м. Проспект мира<br>
                           Банный переулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                    Торгово-развлекательный центр "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-1" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-2',
        'coord'=>array(45.034019, 38.989178),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-2" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-2" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Банкомат</h3>
                        <p>м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-2" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-3',
        'coord'=>array(45.020415, 38.979779),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-3" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="currency-exchange">Обмен валюты</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-3" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Банкомат</h3>
                        <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-3" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-4',
        'coord'=>array(45.041596, 38.959523),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-4" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-4" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-4" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-5',
        'coord'=>array(45.037944, 38.934632),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-5" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-5" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                    <p>м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Европейский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-6',
        'coord'=>array(45.055590, 38.958321),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-6" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-6" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                    <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Покровский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-6" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-7',
        'coord'=>array(45.051939, 38.935662),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-7" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Головной офис</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-7" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Головной офис</h3>
                    <p>м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Головной офис</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-7" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);
$arr=array(
    array(
        'id'=>'placeholder-8',
        'coord'=>array(45.055955, 39.006043),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-8" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-8" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                    <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Покровский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-8" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    )
);

if($_REQUEST['programm']=='individuals')
{
    $arr[]=array(
        'id'=>'placeholder-1',
        'coord'=>array(45.026137, 38.959094),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="round-clock">Круглосуточно</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>м. Проспект мира<br>
                           Банный переулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                    Торгово-развлекательный центр "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-1" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['programm']=='jur-individuals')
{
    $arr[]=array(
        'id'=>'placeholder-1',
        'coord'=>array(45.026137, 38.959094),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="round-clock">Круглосуточно</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>м. Проспект мира<br>
                           Банный переулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                    Торгово-развлекательный центр "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-1" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['of-11']=='of-11')
{
    $arr[]=array(
        'id'=>'placeholder-1',
        'coord'=>array(45.026137, 38.959094),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="round-clock">Круглосуточно</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>м. Проспект мира<br>
                           Банный переулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                    Торгово-развлекательный центр "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-1" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-4',
        'coord'=>array(45.041596, 38.959523),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-4" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-4" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-4" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-8',
        'coord'=>array(45.055955, 39.006043),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-8" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-8" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                    <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Покровский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-8" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['of-22']=='of-22')
{
    $arr[]=array(
        'id'=>'placeholder-2',
        'coord'=>array(45.034019, 38.989178),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-2" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-2" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Банкомат</h3>
                        <p>м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-2" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-3',
        'coord'=>array(45.020415, 38.979779),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-3" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="currency-exchange">Обмен валюты</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-3" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Банкомат</h3>
                        <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-3" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-5',
        'coord'=>array(45.037944, 38.934632),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-5" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-5" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                    <p>м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Европейский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-7',
        'coord'=>array(45.051939, 38.935662),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-7" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Головной офис</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-7" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Головной офис</h3>
                    <p>м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Головной офис</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-7" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['b-11']=='b-11')
{
    $arr[]=array(
        'id'=>'placeholder-1',
        'coord'=>array(45.026137, 38.959094),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="round-clock">Круглосуточно</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-1" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>м. Проспект мира<br>
                           Банный переулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                    Торгово-развлекательный центр "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-1" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['b-22']=='b-22')
{
    $arr[]=array(
        'id'=>'placeholder-2',
        'coord'=>array(45.034019, 38.989178),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-2" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-2" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Банкомат</h3>
                        <p>м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы площадь Киевского вокзала, дом 2 ТРЦ "Европейский", первый этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-2" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['b-33']=='b-33')
{
    $arr[]=array(
        'id'=>'placeholder-3',
        'coord'=>array(45.020415, 38.979779),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-3" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="cash-acceptance">Cash-in (прием наличных)</div>
                            <div class="currency-exchange">Обмен валюты</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-3" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Банкомат</h3>
                        <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-3" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['b-44']=='b-44')
{
    $arr[]=array(
        'id'=>'placeholder-4',
        'coord'=>array(45.041596, 38.959523),
        'type'=>'atm',
        'html_list'=>'
            <a type="atm" href="#modal_form-atm" id="placeholder-4" class="open_modal hover">
                <div class="card-block-list views-row-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="title-ofice">Круглосуточный банкомат</h3>
                        </div>
                        <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="adres">Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                            <div class="weekend">сб. 10:00 – 17:00</div>
                            <div class="free"></div>
                        </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                            <div class="currency">Рубли РФ</div>
                            <div class="free"></div>
                        </div>
                    </div>
                </div>
            </a>
        ',
        'html_map'=>'
             <a type="atm" href="#modal_form-atm" id="placeholder-4" class="open_modal marshrut hover">
                  <div class="card-block data">
                        <h3>Круглосуточный банкомат</h3>
                        <p>Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж</p>
                  </div>
             </a>
        ',
        'html_modal'=>'
            <div id="modal_form-atm" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Круглосуточный банкомат</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    Московская область, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-4" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-5',
        'coord'=>array(45.037944, 38.934632),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-5" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-5" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                    <p>м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Европейский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['b-55']=='b-55')
{
    $arr[]=array(
        'id'=>'placeholder-6',
        'coord'=>array(45.055590, 38.958321),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-6" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-6" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                    <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Покровский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-6" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
if($_REQUEST['b-66']=='b-66')
{
    $arr[]=array(
        'id'=>'placeholder-7',
        'coord'=>array(45.051939, 38.935662),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-7" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Головной офис</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-7" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Головной офис</h3>
                    <p>м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Головной офис</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-7" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
    $arr[]=array(
        'id'=>'placeholder-8',
        'coord'=>array(45.055955, 39.006043),
        'type'=>'office',
        'html_list'=>'
                <a type="office" href="#modal_form-office" id="placeholder-8" class="open_modal hover">
                    <div class="card-block-list views-row-2">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="phone-one">8 (495) 937-07-37,</div>
                                <div class="phone-two">8 (495) 777-57-07</div>
                                <div class="free"></div>
                            </div>
                            <div class="col-sm-12 col-mt-4 col-md-4">
                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                <div class="weekend">сб. 10:00 – 17:00</div>
                                <div class="free"></div>
                            </div>
                        </div>
                    </div>
                </a>
        ',
        'html_map'=>'
            <a type="atm" href="#modal_form-office" id="placeholder-8" class="open_modal marshrut hover">
                <div class="card-block data">
                    <h3 class="title-ofice">Дополнительный офис “Покровский”</h3>
                    <p>м. Красносельская Москва, Банный перулок, дом 9</p>
                </div>
            </a>
        ',
        'html_modal'=>'
            <div id="modal_form-office" class="modal_div container">
                <div class="col-sm-12 col-md-12 modal_fon pb-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-5 mobile-title">
                            <h4 class="modal-title">Дополнительный офис “Покровский”</h4>
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12 mobile-content">
                            <div class="modal-content-top px-4">
                                <div class="adres">
                                    м. Красносельская Москва, Банный перулок, дом 9
                                </div>
                                <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                                <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                            </div>
                            <div class="modal-content-bottom">
                                <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                    <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                    </ul>
                                    <div class="info-content">
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Физ. лицам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Пенсионерам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                            <div class="row px-4 block-line">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>режим работы</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">сб. 10:00 – 17:00</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>касса</h5>
                                                    <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                    <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                                </div>
                                            </div>
                                            <div class="row px-4 block-line-bottom">
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Корпоративным клиентам</h5>
                                                    <div class="services">Денежные переводы</div>
                                                    <div class="services">Прием и выдача денежных средств</div>
                                                    <h5>услуги Инвестиционным банкам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                                <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                                    <h5>услуги Малому и среднему бизнесу </h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                    <h5>услуги Финансовым институтам</h5>
                                                    <div class="services">Автокредитование</div>
                                                    <div class="services">Ипотечное кредитование</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row background-linck">
                                <div class="col-sm-12 col-mt-6 col-md-6">
                                    <div class="start-marshrut">
                                        <a href="#" data-placeholder="placeholder-8" id="marshrut" class="modal_close">Построение маршрута</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ',
    );
}
echo json_encode($arr);
?>
