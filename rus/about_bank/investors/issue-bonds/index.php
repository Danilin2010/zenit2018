<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Эмиссии облигаций");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
use \Bitrix\Main\Page\Asset;
?>

    <div class="container about-page">
        <div class="row">
            <!-- Правый блок мобилка и планшет -->
            <div class="col-sm-12 col-md-4 hidden-lg">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text pt-0">
                            <div class="content_rates_tabs">
                                <ul class="content_tab c-container">
                                    <li><a href="#content-tabs-1">Российские облигации</a></li>
                                    <li><a href="#content-tabs-2">Еврооблигации</a></li>
                                </ul>
								<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "issue-bonds-rus-sec", Array(
									"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
									"CACHE_GROUPS" => "Y",	// Учитывать права доступа
									"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
									"CACHE_TYPE" => "A",	// Тип кеширования
									"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
									"IBLOCK_ID" => "141",	// Инфоблок
									"IBLOCK_TYPE" => "about_bank",	// Тип инфоблока
									"SECTION_CODE" => "",	// Код раздела
									"SECTION_FIELDS" => array(	// Поля разделов
										"NAME",
										"DESCRIPTION"
									),
									"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
									"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
									"SECTION_USER_FIELDS" => array(),	// Свойства разделов
									"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
									"TOP_DEPTH" => "1000",	// Максимальная отображаемая глубина разделов
									"VIEW_MODE" => "LINE",	// Вид списка подразделов
								),
									false
								);?>
								<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "issue-bonds-euro-sec", Array(
									"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
									"CACHE_GROUPS" => "Y",	// Учитывать права доступа
									"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
									"CACHE_TYPE" => "A",	// Тип кеширования
									"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
									"IBLOCK_ID" => "140",	// Инфоблок
									"IBLOCK_TYPE" => "about_bank",	// Тип инфоблока
									"SECTION_CODE" => "",	// Код раздела
									"SECTION_FIELDS" => array(	// Поля разделов
										"NAME",
										"DESCRIPTION"
									),
									"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
									"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
									"SECTION_USER_FIELDS" => array(),	// Свойства разделов
									"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
									"TOP_DEPTH" => "1000",	// Максимальная отображаемая глубина разделов
									"VIEW_MODE" => "LINE",	// Вид списка подразделов
								),
									false
								);?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Правый блок для ПС -->
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                            Array(),
                            Array("MODE"=>"txt","SHOW_BORDER"=>false)
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>