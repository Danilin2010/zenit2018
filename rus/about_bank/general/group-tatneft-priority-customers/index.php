<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Компании Группы «Татнефть» - приоритетные клиенты Банка ЗЕНИТ");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
?>
<div class="container about-page">
    <div class="row">
        <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <!-- Содержимое -->
        <div class="col-sm-12 col-mt-0 col-md-8">
            <div class="row">
				<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
    				<div class="block-card note_text">
						<h2>Специальные предложения для компаний Группы «Татнефть»</h2>
						<p>Компании Группы «Татнефть» - приоритетные клиенты Банка ЗЕНИТ. Банк предоставляет индивидуальные условия обслуживания, гибкие тарифные планы и дополнительные преимущества по всем категориям продуктов и сервисов как компаниям, так и их сотрудникам.</p>
						<p>Благодаря сотрудничеству с нами компании Группы «Татнефть» получают поддержку в реализации масштабных проектов, а их сотрудники – банковские продукты и услуги на максимально комфортных условиях.</p>
						<h2>Держателям зарплатных карт</h2>
						<p>Сотрудники компаний Группы «Татнефть», являющихся участниками зарплатного проекта Банка ЗЕНИТ, имеют возможность пользоваться продуктами Банка на специальных условиях: оформлять кредитные карты со льготным периодом, получать потребительские кредиты, рефинансировать кредиты других банков и др.</p>
						<p>Банк постоянно улучшает условия обслуживания для зарплатных клиентов. Ведётся работа над новой линейкой зарплатных карт, сочетающих в себе надёжность и выгоду при использовании.</p>
						<h2>Дистанционное банковское обслуживание</h2>
						<p>Уровень дистанционного обслуживания Банка ЗЕНИТ полностью соответствует рыночным стандартам, что подтверждается высокими рейтингами агентств, специализирующихся на анализе банковских цифровых сервисов.</p>
						<p>Интернет-банк и мобильное приложение «ЗЕНИТ Онлайн» позволяет получать полноценное банковское обслуживание без посещения офиса: контролировать состояние счетов и совершать операции по ним, открывать вклады с повышенными процентными ставками и др.</p>
						<h2>Private banking</h2>
						<p>В рамках системы Private banking Банк ЗЕНИТ предлагает топ-клиентам максимально персонализированные продукты и высококлассное индивидуальное обслуживание.</p>
					</div>
				</div>
            </div>
        </div>
        <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>