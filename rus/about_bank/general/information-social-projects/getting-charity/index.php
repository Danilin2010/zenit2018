<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Получение благотворительной и спонсорской помощи");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-12">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card pt-0 note_text">
                            <!--content_tab-->
                            <div class="content_rates_tabs">
                                <ul class="content_tab c-container">
                                    <li><a href="#content-tabs-1">Для физического лица</a></li>
                                    <li><a href="#content-tabs-2">Для юридического лица</a></li>
                                    <li><a href="#content-tabs-3">Спонсорская поддержка</a></li>
                                </ul>
                                <div class="content_body" id="content-tabs-1">
                                    <h3>Заявка на получение благотворительной помощи для физического лица</h3>
                                    <p>Поля, отмеченные *, обязательны для заполнения.<br>
                                        Пожалуйста, соблюдайте формат ввода данных.</p>

                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:form.result.new",
                                        "application-charitable-assistance-individual",
                                        array(
                                            "CACHE_TIME" => "3600",
                                            "CACHE_TYPE" => "N",
                                            "CHAIN_ITEM_LINK" => "",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "EDIT_URL" => "",
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "LIST_URL" => "",
                                            "SEF_MODE" => "N",
                                            "SUCCESS_URL" => "",
                                            "USE_EXTENDED_ERRORS" => "Y",
                                            "WEB_FORM_ID" => "4",
                                            "COMPONENT_TEMPLATE" => "application-charitable-assistance-individual",
                                            "SOURCE_TREATMENT" => "",
                                            "RIGHT_TEXT" => "",
                                            "VARIABLE_ALIASES" => array(
                                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                                "RESULT_ID" => "RESULT_ID",
                                            )
                                        ),
                                        false
                                    );?>

                                </div>
                                <div class="content_body" id="content-tabs-2">
                                    <h3>Заявка на получение благотворительной помощи для юридического лица</h3>
                                    <p>Поля, отмеченные *, обязательны для заполнения.<br>
                                        Пожалуйста, соблюдайте формат ввода данных.</p>

                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:form.result.new",
                                        "application-charitable-assistance-legalentity",
                                        array(
                                            "CACHE_TIME" => "3600",
                                            "CACHE_TYPE" => "N",
                                            "CHAIN_ITEM_LINK" => "",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "EDIT_URL" => "",
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "LIST_URL" => "",
                                            "SEF_MODE" => "N",
                                            "SUCCESS_URL" => "",
                                            "USE_EXTENDED_ERRORS" => "Y",
                                            "WEB_FORM_ID" => "5",
                                            "COMPONENT_TEMPLATE" => "application-charitable-assistance-legalentity",
                                            "SOURCE_TREATMENT" => "",
                                            "RIGHT_TEXT" => "",
                                            "VARIABLE_ALIASES" => array(
                                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                                "RESULT_ID" => "RESULT_ID",
                                            )
                                        ),
                                        false
                                    );?>

                                </div>
                                <div class="content_body" id="content-tabs-3">

                                    <h3>Получение благотворительной и спонсорской помощи</h3>
                                    <p>Поля, отмеченные *, обязательны для заполнения.<br>
                                        Пожалуйста, соблюдайте формат ввода данных.</p>

                                    <?
                                    if($USER->isAdmin()): ?>

                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:form.result.new",
                                        "sponsorship",
                                        array(
                                            "CACHE_TIME" => "0",
                                            "CACHE_TYPE" => "N",
                                            "CHAIN_ITEM_LINK" => "",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "EDIT_URL" => "",
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "LIST_URL" => "",
                                            "SEF_MODE" => "N",
                                            "SUCCESS_URL" => "",
                                            "USE_EXTENDED_ERRORS" => "Y",
                                            "WEB_FORM_ID" => "14",
                                            "COMPONENT_TEMPLATE" => "",
                                            "SOURCE_TREATMENT" => "",
                                            "RIGHT_TEXT" => "",
                                            "VARIABLE_ALIASES" => array(
                                                "WEB_FORM_ID" => "WEB_FORM_ID",
                                                "RESULT_ID" => "RESULT_ID",
                                            )
                                        ),
                                        false
                                    );
                                    ?>
                                        
                                    <?
                                    endif;
                                    ?>

                                </div>
                            </div>
                            <!--content_tab-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>