<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корпоративный секретарь");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
use \Bitrix\Main\Page\Asset;
?>

<div class="container about-page">
	<div class="row">
		<div class="col-sm-12 col-md-4 hidden-lg">
			<div class="row note_text manager-top">
				<div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/corporate-secretary-file.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-mt-0 col-md-8">
			<div class="row">
				<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
					<div class="block-card note_text">
						<p>
							Кодекс корпоративного управления, одобренный Центральным Банком РФ письмом от 10 апреля 2014 г. N 06-52/2463, рекомендует Совету директоров общества избирать Корпоративного секретаря.
						</p>
						<p>
							Корпоративный секретарь Банка – должностное лицо, в задачи которого входит обеспечение соблюдения органами и должностными лицами Банка правил и процедур корпоративного управления, гарантирующих реализацию прав и интересов акционеров, организация взаимодействия между Банком и его акционерами, а также обеспечение поддержки эффективной работы Совета директоров Банка и его комитетов.
						</p>
						<p>
							Корпоративный секретарь осуществляет свою деятельность в соответствии с нормами действующего законодательства, Устава Банка, Положения о Корпоративном секретаре, внутренних документов Банка, а также решениями Общего собрания акционеров, Совета директоров, Правления и Председателя Правления Банка.
						</p>
					</div>
				</div>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "corporate-secretary",
                    array(
                        "COMPONENT_TEMPLATE" => "corporate-secretary",
                        "IBLOCK_TYPE" => "about_bank",
                        "IBLOCK_ID" => "114",
                        "NEWS_COUNT" => "20",
                        "SORT_BY1" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "DESC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "NAME",
                            1 => "DETAIL_TEXT",
                            2 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "FIO",
                            1 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "Y",
                        "SET_BROWSER_TITLE" => "Y",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_META_DESCRIPTION" => "Y",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                        "ADD_SECTIONS_CHAIN" => "Y",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "STRICT_SECTION_CHECK" => "N",
                        "DISPLAY_DATE" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "DISPLAY_PICTURE" => "Y",
                        "DISPLAY_PREVIEW_TEXT" => "Y",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "О банке",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => ""
                    ),
                    false
                );?>

			</div>
		</div>
		<div class="col-md-4 hidden-xs hidden-sm">
			<div class="row">
				<div class="col-sm-12">
					<div class="block-card right note_text">
                        <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/inc/template/corporate-secretary-file.php",
                            Array(),
                            Array("MODE"=>"txt","SHOW_BORDER"=>false)
                        );?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>