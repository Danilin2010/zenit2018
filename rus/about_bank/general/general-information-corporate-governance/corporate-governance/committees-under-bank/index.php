<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Комитеты при Совете директоров Банка");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12 col-mt-0 col-md-8">
                    <div class="row">
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                            <div class="block-card note_text">
                                <p>Совет директоров Банка вправе создавать временные и постоянные комитеты для предварительного изучения и рассмотрения наиболее важных вопросов, относящихся к компетенции Совета директоров Банка. Комитеты при Совете директоров Банка действуют на основании положений, утвержденных Советом директоров Банка. Персональные составы комитетов формируются Советом директоров Банка. Комитеты являются консультативными органами Совета директоров Банка, которые возглавляются одним из членов Совета директоров Банка.</p>
                                <p>При Совете директоров ПАО Банк ЗЕНИТ действуют следующие постоянные комитеты: <strong>Комитет по стратегическому планированию, Комитет по аудиту, Комитет по кадрам и вознаграждениям</strong>. </p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
                            <div class="block-card-no block-card note_text pt-3">
                                <h3>Комитет по стратегическому планированию</h3>
                                <p>Количественный и персональный состав Комитета по стратегическому планированию, в том числе Председатель Комитета и его заместитель, утверждается Советом директоров Банка по представлению Председателя Совета директоров Банка либо иных членов Совета директоров Банка.</p>
                                <div class="doc_list">
                                    <a class="doc_item" href="/upload/about/pdf/committee_strategy.pdf" target="_blank">
                                        <div class="doc_pict pdf"></div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                Положение о Комитете по стратегическому планированию
                                            </div>
                                            <div class="doc_note">
                                                131 kб
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
                            <div class="block-card-no block-card note_text pt-3">
                                <h3>Комитет по аудиту</h3>
                                <p>Количественный и персональный состав Комитета, в том числе Председатель Комитета и его заместитель, утверждается Советом директоров Банка по представлению Председателя Совета директоров Банка либо иных членов Совета директоров Банка. В любом случае в состав Комитета не может входить менее трех человек.</p>
                                <p>Комитет по аудиту может состоять как из членов Совета директоров Банка, не являющихся единоличным исполнительным органом и (или) членами коллегиального исполнительного органа Банка, так и иных лиц, уполномоченных Советом директоров Банка. Председателем Комитета по аудиту  может быть только независимый директор.</p>
                                <div class="doc_list">
                                    <a class="doc_item" href="/upload/about/pdf/committee_audit_2012.pdf" target="_blank">
                                        <div class="doc_pict pdf"></div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                Положение о Комитете по аудиту
                                            </div>
                                            <div class="doc_note">
                                                128 kб
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
                            <div class="block-card-no block-card note_text pt-3">
                                <h3>Комитет по кадрам и вознаграждениям </h3>
                                <p>Количественный и персональный состав Комитета, в том числе Председатель Комитета и его заместитель, утверждается Советом директоров Банка по представлению Председателя Совета директоров Банка либо иных членов Совета директоров Банка.</p>
                                <p>Комитет возглавляется Председателем Комитета, и может состоять как из членов Совета директоров Банка, не являющихся единоличным исполнительным органом и (или) членами коллегиального исполнительного органа Банка, так и иных лиц, уполномоченных Советом директоров Банка.</p>
                                <div class="doc_list">
                                    <a class="doc_item" href="/upload/about/pdf/committee_staff_2012.pdf" target="_blank">
                                        <div class="doc_pict pdf"></div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                Положение о Комитете по кадрам и вознаграждениям
                                            </div>
                                            <div class="doc_note">
                                                122 kб
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>