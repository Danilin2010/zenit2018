<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корпоративный секретарь");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">
                    <div class="col-sm-12">
                        <div class="doc_list">
                            <a class="doc_item" href="/upload/about/pdf/corp_secretary.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Положение о Корпоративном секретаре Банка ПАО Банк ЗЕНИТ
                                    </div>
                                    <div class="doc_note">
                                        113 kб
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">
                            <p>Кодекс корпоративного управления, одобренный Центральным Банком РФ письмом от 10 апреля 2014 г. N 06-52/2463, рекомендует Совету директоров общества избирать Корпоративного секретаря.</p>
                            <p>Корпоративный секретарь Банка – должностное лицо, в задачи которого входит обеспечение соблюдения органами и должностными лицами Банка правил и процедур корпоративного управления, гарантирующих реализацию прав и интересов акционеров, организация взаимодействия между Банком и его акционерами, а также обеспечение поддержки эффективной работы Совета директоров Банка и его комитетов.</p>
                            <p>Корпоративный секретарь осуществляет свою деятельность в соответствии с нормами действующего законодательства, Устава Банка, Положения о Корпоративном секретаре, внутренних документов Банка, а также решениями Общего собрания акционеров, Совета директоров, Правления и Председателя Правления Банка.</p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
                        <div class="block-card-no block-card note_text">
                            <h2>Корпоративный секретарь Банка</h2>
                            <h3>Анистратова Елена Михайловна</h3>
                            <p>Окончила Московский институт народного хозяйства им. Г.В. Плеханова (в наст. время Российский Экономический Университет им. Г.В. Плеханова). С 1996 г. по 2000 г. являлась Ответственным секретарем Правления, секретарем Совета директоров Российского Экспортно-Импортного Банка (РОСЭКСИМБАНК).</p>
                            <p>С 2000 г. работает в ПАО Банк ЗЕНИТ (ранее ОАО Банк ЗЕНИТ), занимала должности Советника Правления, начальника Корпоративного секретариата, начальника Управления организации корпоративной деятельности – заместителя начальника Аппарата Правления. В настоящее время является Корпоративным секретарем ПАО Банк ЗЕНИТ.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="block-card right note_text">
                            <div class="doc_list">
                                <a class="doc_item" href="/upload/about/pdf/corp_secretary.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Положение о Корпоративном секретаре Банка ПАО Банк ЗЕНИТ
                                        </div>
                                        <div class="doc_note">
                                            113 kб
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>