<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ревизионная комиссия");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12 col-md-4 hidden-lg">
                    <div class="row note_text manager-top">
                        <div class="col-sm-12">
                            <div class="doc_list">
                                <a class="doc_item full-contener" href="/upload/about/pdf/PolRev.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Положение о Ревизионной комиссии Банка
                                        </div>
                                        <div class="doc_note">
                                            132 Кб
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-mt-0 col-md-8">
                    <div class="row">
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                            <div class="block-card note_text">
                                <p>
                                    Ревизионная комиссия является постоянно действующим выборным органом Банка, избираемым Общим собранием акционеров для осуществления контроля за его финансово-хозяйственной деятельностью.
                                </p>
                                <p>
                                    Ревизионная комиссия избирается на годовом Общем собрании акционеров. Кандидатов в члены Ревизионной комиссии имеют право выдвигать акционеры, являющиеся в совокупности владельцами не менее чем 2 процентами голосующих акций Банка в срок не позднее 30 дней после окончания финансового года. Акции, принадлежащие членам Совета директоров Банка или лицам, занимающим должности в органах управления Банка, при избрании членов Ревизионной комиссии не могут участвовать в голосовании.
                                </p>
                                <p>
                                    В соответствии со своей компетенцией Ревизионная комиссия проводит ревизии (проверки) финансово-хозяйственной деятельности Банка и осуществляет:
                                </p>
                                <ul class="big_list">
                                    <li>
                                        проверку законности заключенных от имени Банка сделок, проведенных расчетов с контрагентами;
                                    </li>
                                    <li>
                                        анализ соответствия ведения бухгалтерского и статистического учета соответствующим нормативным документам;
                                    </li>
                                    <li>
                                        анализ финансового положения Банка и выработка рекомендаций для органов управления Банка;
                                    </li>
                                    <li>
                                        проверку своевременности и правильности платежей в бюджет, начислений и выплат дивидендов, исполнения прочих обязательств;
                                    </li>
                                    <li>
                                        проверку правильности составления балансов Банка, годового отчета, годовой бухгалтерской отчетности, отчета о финансовых результатах, отчетной документации для налоговой инспекции, органов государственного управления;
                                    </li>
                                    <li>
                                        иные виды работ.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden-xs hidden-sm">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="block-card right note_text">
                                <div class="doc_list">
                                    <a class="doc_item full-contener" href="/upload/about/pdf/PolRev.pdf" target="_blank">
                                        <div class="doc_pict pdf"></div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                Положение о Ревизионной комиссии Банка
                                            </div>
                                            <div class="doc_note">
                                                132 Кб
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>