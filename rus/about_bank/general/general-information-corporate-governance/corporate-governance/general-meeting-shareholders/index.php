<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Общее собрание акционеров");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12 col-mt-0 col-md-8">
                    <div class="row">
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                            <div class="block-card note_text">
                                <p>
                                    Общее собрание акционеров является высшим органом управления Банка. Общие собрания акционеров могут быть годовыми и внеочередными.
                                </p>
                                <p>
                                    На годовом Общем собрании акционеров должны решаться вопросы об избрании Совета директоров Банка, Ревизионной комиссии Банка, утверждении аудитора Банка, годовых отчетов, годовой бухгалтерской отчетности, в том числе отчетов о прибылях и убытках Банка, о распределении прибыли, и порядке погашения убытков Банка по результатам финансового года
                                </p>
                                <p>
                                    Сообщение о проведении внеочередного Общего собрания акционеров должно быть сделано не позднее чем за 20 дней, а сообщение о проведении Годового Общего собрания акционеров или Общего собрания акционеров, повестка дня которого содержит вопрос о реорганизации Банка, - не позднее чем за 30 дней до даты его проведения.
                                </p>
                                <p>
                                    В указанные сроки Сообщение о проведении Общего собрания акционеров должно быть опубликовано на сайте Банка в информационно-телекоммуникационной сети «Интернет» - <a href="http://www.zenit.ru" target="_blank">www.zenit.ru</a>.
                                </p>
                                <div class="doc_list">
                                    <a class="doc_item full-contener" href="/upload/about/pdf/Pol_OSA_news.pdf" target="_blank">
                                        <div class="doc_pict pdf"></div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                Положение об Общем собрании акционеров Банка ЗЕНИТ
                                            </div>
                                            <div class="doc_note">
                                                213.8 Кб
                                            </div>
                                        </div>
                                    </a>
                                    <a class="doc_item full-contener" href="/upload/about/pdf/Pol_OSA_ch1.pdf" target="_blank">
                                        <div class="doc_pict pdf"></div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                Изменения № 1 в Положение об Общем собрании акционеров Банка ЗЕНИТ (открытое акционерное общество)
                                            </div>
                                            <div class="doc_note">
                                                53 Кб
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>