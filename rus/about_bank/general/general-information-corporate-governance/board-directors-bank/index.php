<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Совет директоров Банка");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
use \Bitrix\Main\Page\Asset;
?><div class="container about-page">
	<div class="row">
		<div class="col-sm-12 col-md-4 hidden-lg">
			<div class="row note_text manager-top">
				<div class="col-sm-12">
					 <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/board-directors-bank-file.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-mt-0 col-md-8">
			<div class="row">
				<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
					<div class="block-card note_text">
						<p>
							 Совет директоров ПАО Банк ЗЕНИТ осуществляет общее руководство деятельностью Банка, за исключением решения вопросов, отнесенных к компетенции Общего собрания акционеров. К компетенции Совета директоров Банка относятся вопросы, предусмотренные Федеральным законом «Об акционерных обществах» и Уставом Банка.
						</p>
						<p>
							 Председатель Совета директоров - Генеральный директор ПАО "Татнефть" им. В.Д. Шашина Маганов Наиль Ульфатович.
						</p>
						<p>
							 В состав Совета директоров Банка ЗЕНИТ, исходя из требований, выполнение которых является условием включения акций акционерных обществ в котировальные списки фондовых бирж РФ (п.13.3 Устава), входят независимые директора г-н Боруцкий В.В., г-н Панферов А.В., г-н Шибаев С.В.
						</p>
					</div>
				</div>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"board-directors-bank",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "board-directors-bank",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"NAME",1=>"DETAIL_TEXT",2=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "110",
		"IBLOCK_TYPE" => "about_bank",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "1000",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"YEAR_BIRTH",1=>"POSITION",2=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "external_id",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
			</div>
		</div>
		<div class="col-md-4 hidden-xs hidden-sm">
			<div class="row">
				<div class="col-sm-12">
					<div class="block-card right note_text">
						 <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/inc/template/board-directors-bank-file.php",
                            Array(),
                            Array("MODE"=>"txt","SHOW_BORDER"=>false)
                        );?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>