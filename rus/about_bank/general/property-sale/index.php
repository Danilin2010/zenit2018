<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Имущество для реализации");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <!-- Правый блок мобилка и планшет -->
           <!-- <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">

                </div>
            </div>-->
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">

<p>ПАО Банк ЗЕНИТ предлагает к реализации следующее имущество:</p>
<p><a href="https://www.zenit.ru/media/doc/property/realty.xls" target="_blank">Объекты недвижимости</a></p>
<p><a href="https://old.zenit.ru/media/rus/content/docs/property/equip.xls" target="_blank">Производственное оборудование</a></p>
<p><a href="https://old.zenit.ru/media/rus/content/docs/property/transport.xls" target="_blank">Транспортные средства</a></p>
<p><!--Товарно-материальные ценности <a onclick="Hider('tmc'); return false" href="#">показать/скрыть</a>--></p>
<!--<div id="tmc" style="display: none;">-->
<p>Предлагаются к продаже кожгалантерейные товары, а также материалы и бижутерия для текстильного/кожгалантерейного производств:</p>
<p>1. Кожгалантерейные изделия:</p>
<ul style="list-style-type: square;">
<li>Бумажники</li>
<li>Портмоне</li>
<li>Визитницы</li>
<li>Обложки для документов и удостоверений</li>
<li>Футляры для кредитных карт</li>
<li>Сумки</li>
</ul>
<p>2. Материалы для изготовления кожгалантерейных и текстильных изделий:</p>
<ul style="list-style-type: square;">
<li>Кожа</li>
<li>Заменитель кожи</li>
<li>Стразы</li>
<li>Камень пришивной</li>
<li>Нитки, застежки и прочее</li>
</ul>
<p><a href="https://old.zenit.ru/media/rus/content/docs/property/tmc.xls" target="_blank">Полный перечень</a></p>
<p>Контактные данные: Дмитрий Токарев, +7 (812) 448-22-48, доб. 102.</p>
<!--<hr />-->

<p>3. Эксклюзивная сувенирная продукция премиум класса (кортики, кинжалы, сабли, ножи, кухонные столовые приборы, сувенирные наборы и предметы интерьера) в количестве 2.656 ед.</p>
<p><a href="https://old.zenit.ru/media/rus/content/docs/property/souvenirs.xls" target="_blank">Полный перечень</a></p>
<p>Контактные данные: Дмитрий Романов, +7 (351) 247-91-94</p>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Правый блок для ПС -->
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
                 <!--   <div class="col-sm-12">
                        <div class="block-card right note_text">

                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>