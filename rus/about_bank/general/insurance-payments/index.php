<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страховые выплаты по решениям АСВ");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>
                                <p>
                                    Банк ЗЕНИТ, выступая в качестве банка-агента Государственной корпорации «Агентство по страхованию вкладов» (АСВ), на основании решений АСВ участвует в выплате страховых возмещений в соответсвии с Федеральным законом № 177-ФЗ «О страховании вкладов физических лиц в банках Российской Федерации».
                                </p>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>