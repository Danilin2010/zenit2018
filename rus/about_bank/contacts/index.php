<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
<h2>Телефон для консультаций по услугам для Юридических лиц</h2>
<p>+7 (495) 777-57-04; 8 (800) 500-40-82</p>
<h2>Телефоны для консультаций по услугам для Частных лиц</h2>
<p>+7 (495) 967-11-11; 8 (800) 500-66-77</p>
<!--
<h2><strong>Телефоны для справок:</strong></h2>
<p>+7 (495) 777-57-07, 937-07-37</p>
-->
<p><strong>Адрес Головного офиса ПАО Банка ЗЕНИТ:</strong></p>
<p>Россия, 129110, г. Москва, Банный пер., д. 9.<br />Станция метро "Проспект мира".</p>
<p><strong>Факс:</strong></p>
<p>+7 (495) 937-07-36, 777-57-06</p>
<p><strong>Электронная почта:</strong></p>
<p><a href="mailto:info@zenit.ru">info@zenit.ru</a></p>
					</div>
				</div>
			</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>