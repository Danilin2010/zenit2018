<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Раскрытие информации");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
?>
<?
use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <!-- Правый блок мобилка и планшет -->
            <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">
                    <div class="block-card right note_text">
                        <h2>Связаться с банком</h2>
                        <div class="form_application_line">
                            <div class="contacts_block">
                                <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
                                +7 (495) 967-11-11<br>
                                8 (800) 500-66-77
                            </div>
                            <div class="note_text">
                                звонок по России бесплатный
                            </div>
                        </div>
                        <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
                    </div>
                </div>
            </div>
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mt-3 main-content-top">
                        <div class="ipoteka_list mb-5">
                            <div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="http://www.e-disclosure.ru/portal/company.aspx?id=538" class="ipoteka_item_title">
                                        Интерфакс-ЦРКИ
                                    </a>
                                    <div class="ipoteka_item_text">
										<!--Центр раскрытия информации-->
                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/posts/" class="ipoteka_item_title">
                                        Сообщения о существенных фактах
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/quarterly-reports/" class="ipoteka_item_title">
                                        Ежеквартальные отчеты по ценным бумагам
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/affiliated/" class="ipoteka_item_title">
                                        Список аффилированных лиц
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/annual-reports/" class="ipoteka_item_title">
                                        Годовые отчеты ПАО Банк ЗЕНИТ
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/list-insider/" class="ipoteka_item_title">
                                        Перечень инсайдерской информации
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/financial-statements/" class="ipoteka_item_title">
                                        Финансовая отчетность
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/equity-structure/" class="ipoteka_item_title">
                                        Структура акционерного капитала
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/information-qualifications/" class="ipoteka_item_title">
                                        Информация о квалификации и опыте работы руководителей Банка и его филиалов
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/additional-issue/" class="ipoteka_item_title">
                                        Дополнительный выпуск акций
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/disclosure-information/" class="ipoteka_item_title">
                                        Раскрытие информации для регулятивных целей
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div><div class="ipoteka_item">
                                <div class="wr_ipoteka_item">
                                    <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                    <a href="/rus/about_bank/disclosure/disclosure-information-professional/" class="ipoteka_item_title">
                                        Раскрытие информации профессиональным участником рынка ценных бумаг
                                    </a>
                                    <div class="ipoteka_item_text">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Правый блок для ПС -->
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="block-card right note_text">
                            <h2>Связаться с банком</h2>
                            <div class="form_application_line">
                                <div class="contacts_block">
                                    <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
                                    +7 (495) 967-11-11<br>
                                    8 (800) 500-66-77
                                </div>
                                <div class="note_text">
                                    звонок по России бесплатный
                                </div>
                            </div>
                            <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>