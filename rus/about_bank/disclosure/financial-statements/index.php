<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Финансовая отчетность");
$APPLICATION->SetPageProperty("title", "Финансовая отчетность");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$reporting = (int)($request->getQuery("reporting"));
if($reporting>0){
    global $arrFilter;
    $arrFilter["PROPERTY_REPORTING_TYPE"]=$reporting;
}
?>

<div class="container about-page">
	<div class="row">
		 <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
		 <!-- Содержимое -->
		<div class="col-sm-12 col-mt-0 col-md-8">
			<div class="row">
				<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
					<div class="preview-card note_text min-linck">
						<div class="content_rates_tabs">
							<ul class="content_tab c-container">
								<li><a href="#content-tabs-1">Отчетность по РСБУ</a></li>
								<li><a href="#content-tabs-2">Отчетность по МСФО</a></li>
							</ul>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "financial-statements-rsbu",
                                array(
                                    "COMPONENT_TEMPLATE" => "financial-statements-rsbu",
                                    "IBLOCK_TYPE" => "reporting",
                                    "IBLOCK_ID" => "82",
                                    "NEWS_COUNT" => "1000",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "arrFilter",
                                    "FIELD_CODE" => array(
                                        0 => "NAME",
                                        1 => "",
                                    ),
                                    "PROPERTY_CODE" => array(
                                        0 => "DATA",
                                        1 => "DATA_PUBLICATION",
                                        2 => "SHORT_DESCRIPTION",
                                        3 => "REPORTING_TYPE",
                                        4 => "FILE",
                                        5 => "",
                                    ),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_META_KEYWORDS" => "Y",
                                    "SET_META_DESCRIPTION" => "Y",
                                    "SET_LAST_MODIFIED" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "STRICT_SECTION_CHECK" => "N",
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "PAGER_TEMPLATE" => ".default",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "Y",
                                    "PAGER_TITLE" => "Новости",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SHOW_404" => "N",
                                    "MESSAGE_404" => "",
                                    "REPORTING_TYPE" => $reporting
                                ),
                                false
                            );?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "financial-statements-msfo",
                                array(
                                    "COMPONENT_TEMPLATE" => "financial-statements-msfo",
                                    "IBLOCK_TYPE" => "reporting",
                                    "IBLOCK_ID" => "95",
                                    "NEWS_COUNT" => "1000",
                                    "SORT_BY1" => "ACTIVE_FROM",
                                    "SORT_ORDER1" => "DESC",
                                    "SORT_BY2" => "SORT",
                                    "SORT_ORDER2" => "ASC",
                                    "FILTER_NAME" => "",
                                    "FIELD_CODE" => array(
                                        0 => "NAME",
                                        1 => "",
                                    ),
                                    "PROPERTY_CODE" => array(
                                        0 => "DATA",
                                        1 => "DATA_PUBLICATION",
                                        2 => "SHORT_DESCRIPTION",
                                        3 => "FILE",
                                    ),
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "CACHE_TYPE" => "A",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "SET_TITLE" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "PARENT_SECTION" => "",
                                    "PARENT_SECTION_CODE" => "",
                                    "INCLUDE_SUBSECTIONS" => "N",
                                    "STRICT_SECTION_CHECK" => "N",
                                    "DISPLAY_DATE" => "N",
                                    "DISPLAY_NAME" => "N",
                                    "DISPLAY_PICTURE" => "N",
                                    "DISPLAY_PREVIEW_TEXT" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "Отчетность",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SHOW_404" => "N",
                                    "MESSAGE_404" => ""
                                ),
                                false
                            );?>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>