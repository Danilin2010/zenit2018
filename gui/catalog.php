<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Банк ЗЕНИТ — GUI");
?>
<!-- Блок заголовок -->

<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	".default",
	Array(
		"PRODUCTHEADER_TITLE" => "Заголовок",
		"PRODUCTHEADER_IMG" => "/personal/cards/img/cardsCatalogIconBig.png"
	)
);?>

<!-- /Блок заголовок -->


<!-- Блок таббар -->

<div class="tabs-controls" id="tabs-controls-app">
	<div class="tabs-controls__wrapper">
		<nav class="tabs-controls__inner">

			<!-- Кнопки табов -->

			<?$APPLICATION->IncludeComponent(
				"aic.bz:snippets.tabbutton",
				"",
				Array(
					"PRODUCTHEADER_ACTIVE" => "Y",
					"PRODUCTHEADER_SRC" => "#tab1",
					"PRODUCTHEADER_STAR" => "N",
					"PRODUCTHEADER_TARGET" => "N",
					"PRODUCTHEADER_TITLE" => "Таб 1"
				)
			);?>

			<?$APPLICATION->IncludeComponent(
				"aic.bz:snippets.tabbutton",
				"",
				Array(
					"PRODUCTHEADER_ACTIVE" => "N",
					"PRODUCTHEADER_SRC" => "#tab2",
					"PRODUCTHEADER_STAR" => "N",
					"PRODUCTHEADER_TARGET" => "N",
					"PRODUCTHEADER_TITLE" => "Таб 2"
				)
			);?>

			<?$APPLICATION->IncludeComponent(
				"aic.bz:snippets.tabbutton",
				"",
				Array(
					"PRODUCTHEADER_ACTIVE" => "N",
					"PRODUCTHEADER_SRC" => "#tab3",
					"PRODUCTHEADER_STAR" => "N",
					"PRODUCTHEADER_TARGET" => "N",
					"PRODUCTHEADER_TITLE" => "Таб 3"
				)
			);?>

			<?$APPLICATION->IncludeComponent(
				"aic.bz:snippets.tabbutton",
				"",
				Array(
					"PRODUCTHEADER_ACTIVE" => "N",
					"PRODUCTHEADER_SRC" => "#tab4",
					"PRODUCTHEADER_STAR" => "N",
					"PRODUCTHEADER_TARGET" => "N",
					"PRODUCTHEADER_TITLE" => "Таб 4"
				)
			);?>

			<!-- /Кнопки табов -->

		</nav>
	</div>
</div>

<!-- /Блок таббар -->


<!-- Блок табов -->
<!-- Активный таб tab-content_state-active Поставить id из href кнопки -->

<div class="tab-content tab-content_state-active" id="tab1">

	<!-- Фон подложки таба -->

	<div class="tab-content__theme tab-content__theme_grey">
		<div class="tab-content__wrapper">
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-12">
					<div class="product-cards">

						<!-- Карты -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array(""),
								"SIDECOLUMN_DATA_FILTER" => array(""),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "12,9",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "0",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
								"SIDECOLUMN_FEATURE3_TITLE" => "100",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "тыс. ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет купить иномарку или&nbsp;отечественный автомобиль у&nbsp;официального диллера",
								"SIDECOLUMN_TITLE" => "Новый автомобиль",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array("если ожидаете получение крупной суммы в перспективе","например &#151; от продажи недвижимости или с премии",""),
								"SIDECOLUMN_DATA_FILTER" => array(""),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "14",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "15",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
								"SIDECOLUMN_FEATURE3_TITLE" => "6,5",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет уменьшить платеж по&nbsp;кредиту за&nbsp;счет переноса части кредита на&nbsp;последний платеж",
								"SIDECOLUMN_TITLE" => "Новый автомобиль с&nbsp;остаточным платежом",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<!-- /Карты -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>

			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-12">
					<div class="recommended-cards">
						<div class="recommended-cards__title">Обратите внимание</div>

						<!-- Рекомендации -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.recommended",
							"",
							Array(
								"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
								"RECOMMENDED_TEXT" => "Зарплатная карта с&nbsp;начислением кэшбэка и&nbsp;процентов на&nbsp;остаток средств",
								"RECOMMENDED_LINK" => "/personal/tatneft/#сards",
								"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
							)
						);?>

						<!-- /Рекомендации -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>
		</div>
	</div>
</div>

<!-- /Блок табов -->


<!-- Блок табов -->

<div class="tab-content" id="tab2">

	<!-- Фон подложки таба -->

	<div class="tab-content__theme tab-content__theme_grey">
		<div class="tab-content__wrapper">
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-12">
					<div class="product-cards">

						<!-- Подборщик сверху -->

						<div class="products-filter-selection" :class="{&quot;products-filter-selection_state-open&quot;: filterStateOpen}">
							<button class="button products-filter-selection__control" @click="toggleFilterState()">
								<svg class="icon icon__filter-symbol">
									<use xlink:href="#icon__filter-symbol"></use>
								</svg>
								<span class="text">Подборщик сверху</span>
							</button>
							<form class="products-filter-selection__wrapper" @change="onFilterChange($event)">
								<div class="products-filter-selection__inner">
									<div class="products-filter-selection__header">
										<div class="products-filter-selection__title">
											Подборщик сверху
											<button class="products-filter-selection__close" type="button" @click.prevent="toggleFilterState()">
												<svg class="icon icon__close-symbol">
													<use xlink:href="#icon__close-symbol"></use>
												</svg>
											</button>
										</div>
									</div>
									<div class="products-filter-selection__part products-filter-selection__part_tags">
										<div class="products-filter-selection__part-wrapper">
											<div class="products-filter-selection__tags-list">

												<!-- Кнопки подборщика -->

												<?$APPLICATION->IncludeComponent(
													"aic.bz:snippets.checkbox",
													"card-filter",
													Array(
														"CHECKBOX_ID" => "tab2item1",
														"CHECKBOX_CODE" => "tab2item1",
														"CHECKBOX_NAME" => "Кэшбэк за топливо 1"
													)
												);?>

												<?$APPLICATION->IncludeComponent(
													"aic.bz:snippets.checkbox",
													"card-filter",
													Array(
														"CHECKBOX_ID" => "tab2item2",
														"CHECKBOX_CODE" => "tab2item2",
														"CHECKBOX_NAME" => "Кэшбэк за топливо 2"
													)
												);?>

												<!-- Кнопки подборщика -->

											</div>
										</div>
									</div>
									<div class="products-filter-selection__controls">
										<div class="button button_primary products-filter-selection__apply-params" @click.prevent="onFilterApply()">Применить фильтр</div>
										<div class="button button_transparent button_dark-transparent products-filter-selection__reset-params" @click.prevent="showAllCards()">Сбросить</div>
									</div>
								</div>
							</form>
						</div>

						<!-- /Подборщик сверху -->

						<!-- Карты -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array(""),
								"SIDECOLUMN_DATA_FILTER" => array("tab2item1"),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "12,9",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "0",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
								"SIDECOLUMN_FEATURE3_TITLE" => "100",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "тыс. ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет купить иномарку или&nbsp;отечественный автомобиль у&nbsp;официального диллера",
								"SIDECOLUMN_TITLE" => "Новый автомобиль",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array("если ожидаете получение крупной суммы в перспективе","например &#151; от продажи недвижимости или с премии",""),
								"SIDECOLUMN_DATA_FILTER" => array("tab2item2"),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "14",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "15",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
								"SIDECOLUMN_FEATURE3_TITLE" => "6,5",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет уменьшить платеж по&nbsp;кредиту за&nbsp;счет переноса части кредита на&nbsp;последний платеж",
								"SIDECOLUMN_TITLE" => "Новый автомобиль с&nbsp;остаточным платежом",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<!-- /Карты -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-12">
					<div class="recommended-cards">
						<div class="recommended-cards__title">Обратите внимание</div>

						<!-- Рекомендации -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.recommended",
							"",
							Array(
								"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
								"RECOMMENDED_TEXT" => "Зарплатная карта с&nbsp;начислением кэшбэка и&nbsp;процентов на&nbsp;остаток средств",
								"RECOMMENDED_LINK" => "/personal/tatneft/#сards",
								"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
							)
						);?>

						<!-- /Рекомендации -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>
		</div>
	</div>
</div>

<!-- /Блок табов -->


<!-- Блок табов -->

<div class="tab-content" id="tab3">

	<!-- Фон подложки таба -->

	<div class="tab-content__theme tab-content__theme_grey">
		<div class="tab-content__wrapper">
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-8">
					<div class="product-cards">

						<!-- Карты -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"big",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array(""),
								"SIDECOLUMN_DATA_FILTER" => array(""),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "12,9",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "0",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
								"SIDECOLUMN_FEATURE3_TITLE" => "100",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "тыс. ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет купить иномарку или&nbsp;отечественный автомобиль у&nbsp;официального диллера",
								"SIDECOLUMN_TITLE" => "Новый автомобиль",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"big",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array("если ожидаете получение крупной суммы в перспективе","например &#151; от продажи недвижимости или с премии",""),
								"SIDECOLUMN_DATA_FILTER" => array(""),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "14",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "15",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
								"SIDECOLUMN_FEATURE3_TITLE" => "6,5",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет уменьшить платеж по&nbsp;кредиту за&nbsp;счет переноса части кредита на&nbsp;последний платеж",
								"SIDECOLUMN_TITLE" => "Новый автомобиль с&nbsp;остаточным платежом",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<!-- /Карты -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-12">
					<div class="recommended-cards">
						<div class="recommended-cards__title">Обратите внимание</div>

						<!-- Рекомендации -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.recommended",
							"",
							Array(
								"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
								"RECOMMENDED_TEXT" => "Зарплатная карта с&nbsp;начислением кэшбэка и&nbsp;процентов на&nbsp;остаток средств",
								"RECOMMENDED_LINK" => "/personal/tatneft/#сards",
								"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
							)
						);?>

						<!-- /Рекомендации -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>
		</div>
	</div>
</div>

<!-- /Блок табов -->


<!-- Блок табов -->

<div class="tab-content" id="tab4">

	<!-- Фон подложки таба -->

	<div class="tab-content__theme tab-content__theme_grey">
		<div class="tab-content__wrapper">
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-8">
					<div class="product-cards">

						<!-- Карты -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"big",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array(""),
								"SIDECOLUMN_DATA_FILTER" => array("tab4item1"),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "12,9",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "0",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
								"SIDECOLUMN_FEATURE3_TITLE" => "100",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "тыс. ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/new-car/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет купить иномарку или&nbsp;отечественный автомобиль у&nbsp;официального диллера",
								"SIDECOLUMN_TITLE" => "Новый автомобиль",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.sidecolumn",
							"big",
							Array(
								"SIDECOLUMN_BRAND_LIST" => array("если ожидаете получение крупной суммы в перспективе","например &#151; от продажи недвижимости или с премии",""),
								"SIDECOLUMN_DATA_FILTER" => array("tab4item2"),
								"SIDECOLUMN_FEATURE1_BEFORE" => "",
								"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
								"SIDECOLUMN_FEATURE1_TITLE" => "14",
								"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE2_BEFORE" => "",
								"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
								"SIDECOLUMN_FEATURE2_TITLE" => "15",
								"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
								"SIDECOLUMN_FEATURE3_BEFORE" => "",
								"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
								"SIDECOLUMN_FEATURE3_TITLE" => "6,5",
								"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
								"SIDECOLUMN_IMG" => "",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
								"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
								"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
								"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет уменьшить платеж по&nbsp;кредиту за&nbsp;счет переноса части кредита на&nbsp;последний платеж",
								"SIDECOLUMN_TITLE" => "Новый автомобиль с&nbsp;остаточным платежом",
								"SIDECOLUMN_TYPE" => "product-card_theme-normal"
							)
						);?>

						<!-- /Карты -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

				<!-- Боковая колонка-->

				<div class="col col_size-4">
					<asside class="products-asside">

						<!-- Подборщик сбоку-->

						<div class="products-filter" :class="{&quot;products-filter_state-open&quot;: filterStateOpen}">
							<button class="button products-filter__control" @click="toggleFilterState()">
								<svg class="icon icon__filter-symbol">
									<use xlink:href="#icon__filter-symbol"></use>
								</svg>
								<span class="text">Подобрать карту</span>
							</button>
							<form class="products-filter__wrapper" @change="onFilterChange($event)">
								<div class="products-filter__inner">
									<div class="products-filter__title">Подбор карты</div>
									<button class="products-filter__close" type="button" @click.prevent="toggleFilterState()">
										<svg class="icon icon__close-symbol">
											<use xlink:href="#icon__close-symbol"></use>
										</svg>
									</button>
									<div class="products-filter__list">

										<!-- Кнопки подборщика -->

										<?$APPLICATION->IncludeComponent(
											"aic.bz:snippets.checkbox",
											"",
											Array(
												"CHECKBOX_ID" => "tab4item1",
												"CHECKBOX_CODE" => "tab4item1",
												"CHECKBOX_NAME" => "Проценты на остаток"
											)
										);?>

										<?$APPLICATION->IncludeComponent(
											"aic.bz:snippets.checkbox",
											"",
											Array(
												"CHECKBOX_ID" => "tab4item2",
												"CHECKBOX_CODE" => "tab4item2",
												"CHECKBOX_NAME" => "Кэшбэк за топливо"
											)
										);?>

										<!-- /Кнопки подборщика -->

									</div>

									<div class="products-filter__controls">
										<div class="button button_primary products-filter__apply-params" @click.prevent="onFilterApply()">Применить фильтр</div>
										<div class="button button_transparent button_dark-transparent products-filter__reset-params" @click.prevent="showAllCards()">Сбросить</div>
									</div>
								</div>
							</form>
						</div>

						<!-- /Подборщик сбоку-->

					</asside>
				</div>
			</div>
			<div class="tab-content__inner">

				<!-- Колонка с контентом -->

				<div class="col col_size-12">
					<div class="recommended-cards">
						<div class="recommended-cards__title">Обратите внимание</div>

						<!-- Рекомендации -->

						<?$APPLICATION->IncludeComponent(
							"aic.bz:snippets.recommended",
							"",
							Array(
								"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
								"RECOMMENDED_TEXT" => "Зарплатная карта с&nbsp;начислением кэшбэка и&nbsp;процентов на&nbsp;остаток средств",
								"RECOMMENDED_LINK" => "/personal/tatneft/#сards",
								"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
							)
						);?>

						<!-- /Рекомендации -->

					</div>
				</div>

				<!-- /Колонка с контентом -->

			</div>
		</div>
	</div>
</div>

<!-- /Блок табов -->

<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>