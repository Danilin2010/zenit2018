<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

if (!function_exists("GetTreeRecursive")) //Include from main.map component
{
	$aMenuLinksExt=$APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
			"IBLOCK_TYPE_ID" => "mortgage",
			"IBLOCK_ID" => "6",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000"
	),
			false,
			Array('HIDE_ICONS' => 'Y')
	);
	$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
}

?>