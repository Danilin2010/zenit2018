<?php
$step = $_REQUEST['id'];
$data_step = array();
use \Bitrix\Main\Application;
switch ($step) {
    default:
        http_response_code(501);
    case 'start':
        $data_step = ([
            'STEPS' => '3', // Кол-во шагов
            'NEXT_STEP' => 'step_0_0', //Кол-во шагов до результата (с заголовком "Шаг № из №"). Нужно только для первого шага.
        ]);
        echo json_encode($data_step);
        break;
    case 'step_0_0':
        $data_step = ([
            'STEP_ID' => 'step_0_0', //ID элемента. К нему привязываются шаги
            'TITLE' => 'Выберите тип кредита', //Заголовок шага
            'STEP_NUMBER' => '1', //Номер шага
            'BUTTONS' => [ //Кнопки для следующих шагов
                [
                    "TITLE" => "Ипотека", //Текст кнопки
                    "NEXT_STEP" => "step_1_0" //ID следующего шага. В последнем шаге - ссылка на результат
                ],
                [
                    "TITLE" => "Кредит на неотложные нужды",
                    "NEXT_STEP" => "step_1_1"
                ],
                [
                    "TITLE" => "Кредит на покупку авто",
                    "NEXT_STEP" => "step_1_2"
                ],
            ]
        ]);
        echo json_encode($data_step);
        break;
    case 'step_1_0':
        $data_step = ([
            'STEP_ID' => 'step_1_0',
            'TITLE' => 'Выберите тип жилья',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Жилье на первичном рынке",
                    "NEXT_STEP" => "step_2_0"
                ],
                [
                    "TITLE" => "Жилье на вторичном рынке",
                    "NEXT_STEP" => "step_2_1"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    case 'step_1_1':
        $data_step = ([
            'STEP_ID' => 'step_1_1',
            'TITLE' => 'Выберите тип кредита',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Потребительский кредит",
                    "NEXT_STEP" => "step_2_0"
                ],
                [
                    "TITLE" => "Кредит на отпуск",
                    "NEXT_STEP" => "step_2_1"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    case 'step_1_2':
        $data_step = ([
            'STEP_ID' => 'step_1_2',
            'TITLE' => 'Выберите тип автомобиля',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Новый автомобиль",
                    "NEXT_STEP" => "step_2_0"
                ],
                [
                    "TITLE" => "Подержанный автомобиль",
                    "NEXT_STEP" => "step_2_1"
                ],
                [
                    "TITLE" => "Автомобиль бизнес класса",
                    "NEXT_STEP" => "step_2_0"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    case 'step_2_0':
        $data_step = ([
            'STEP_ID' => 'step_2_0',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'BUTTONS' => [
                [
                    "TITLE" => "Квартира на вторичном рынке",
                    "NEXT_STEP" => "step_result"
                ],
                [
                    "TITLE" => "Квартира в ЖК САМПО",
                    "NEXT_STEP" => "step_result"
                ],
                [
                    "TITLE" => "Квартира для военнослужащих",
                    "NEXT_STEP" => "step_result"
                ],
                [
                    "TITLE" => "Дом с землей",
                    "NEXT_STEP" => "step_result"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    case 'step_2_1':
        $data_step = ([
            'STEP_ID' => 'step_2_1',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'BUTTONS' => [
                [
                    "TITLE" => "Квартира на вторичном рынке 2",
                    "NEXT_STEP" => "step_result"
                ],
                [
                    "TITLE" => "Квартира в ЖК САМПО 2",
                    "NEXT_STEP" => "step_result"
                ],
                [
                    "TITLE" => "Квартира для военнослужащих 2",
                    "NEXT_STEP" => "step_result"
                ],
                [
                    "TITLE" => "Дом с землей 2",
                    "NEXT_STEP" => "step_result"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    case 'step_result':
        require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
        $APPLICATION->IncludeComponent(
            "bitrix:news.detail",
            "product_selection",
            Array(
                'AJAX_MODE' => 'N',
                "DISPLAY_DATE" => 'N',
                "DISPLAY_NAME" => '',
                "DISPLAY_PICTURE" => '',
                "DISPLAY_PREVIEW_TEXT" => '',
                    "IBLOCK_TYPE" => 'mortgage',
                    "IBLOCK_ID" => '6',
                    "FIELD_CODE" => array('', ''),
                    "PROPERTY_CODE" => Array
                        (
                            'REALTY_TYPE', 'ADDITIONALLY', ''
                        ),
                "DETAIL_URL" => '',
                "SECTION_URL" => '',
                "META_KEYWORDS" => '',
                "META_DESCRIPTION" => '',
                "BROWSER_TITLE" => '',
                "SET_CANONICAL_URL" => '',
                "DISPLAY_PANEL" => '',
                "SET_LAST_MODIFIED" => '',
                "SET_TITLE" => '',
                "MESSAGE_404" => '',
                "SET_STATUS_404" => '',
                "SHOW_404" => '',
                "FILE_404" => '',
                "INCLUDE_IBLOCK_INTO_CHAIN" => 'N',
                "ADD_SECTIONS_CHAIN" => 'N',
                "ACTIVE_DATE_FORMAT" => 'd.m.Y',
                "CACHE_TYPE" => 'N',
                "CACHE_TIME" => '36000',
                "CACHE_GROUPS" => 'N',
                "USE_PERMISSIONS" => 'N',
                "GROUP_PERMISSIONS" => '',
                "DISPLAY_TOP_PAGER" => '',
                "DISPLAY_BOTTOM_PAGER" => '',
                "PAGER_TITLE" => '',
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => '',
                "PAGER_SHOW_ALL" => '',
                "CHECK_DATES" => 'Y',
                    "ELEMENT_ID" => '',
                    "ELEMENT_CODE" => 'new',
                    "SECTION_ID" => '',
                    "SECTION_CODE" => 'new',
                "IBLOCK_URL" => '',
                "USE_SHARE" => '',
                "SHARE_HIDE" => '',
                "SHARE_TEMPLATE" => '',
                "SHARE_HANDLERS" => '',
                "SHARE_SHORTEN_URL_LOGIN" => '',
                "SHARE_SHORTEN_URL_KEY" => '',
                "ADD_ELEMENT_CHAIN" => '',
                'STRICT_SECTION_CHECK' => '',
            )
        );
        break;
}
