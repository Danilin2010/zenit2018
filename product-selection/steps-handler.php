<?php
$step = $_REQUEST['id'];
$data_step = array();
use \Bitrix\Main\Application;
switch ($step) {
    default:
        http_response_code(501);
    // Ипотека
    case 'ipoteka':
        $data_step = ([
            'STEP_ID' => 'ipoteka', //ID элемента. К нему привязываются шаги
            'TITLE' => 'Выберите тип кредита', //Заголовок шага
            'STEP_NUMBER' => '1', //Номер шага
            'BUTTONS' => [ //Кнопки для следующих шагов
                [
                    "TITLE" => "Новостройки", //Текст кнопки
                    "NEXT_STEP" => "new_buildings" //ID следующего шага. В последнем шаге - ссылка на результат
                ],
                [
                    "TITLE" => "Вторичное жилье",
                    "NEXT_STEP" => "old_buildings"
                ],
                [
                    "TITLE" => "Дом",
                    "NEXT_STEP" => "house"
                ],
                [
                    "TITLE" => "Для военнослужащих",
                    "NEXT_STEP" => "military"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Ипотека -> Новостройки
    case 'new_buildings':
        $data_step = ([
            'STEP_ID' => 'new_buildings',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Ипотека на новостройку",
                    "NEXT_STEP" => "/personal/mortgage/new/new/"
                ],
                [
                    "TITLE" => "Ипотека в объектах, финансируемых Банком",
                    "NEXT_STEP" => "/personal/mortgage/new/objects/"
                ],
                [
                    "TITLE" => "Ипотека в ЖК «SAMPO»",
                    "NEXT_STEP" => "/personal/mortgage/new/sampo/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Ипотека -> Вторичное жилье
    case 'old_buildings':
        $data_step = ([
            'STEP_ID' => 'old_buildings',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Ипотека на вторичное жилье",
                    "NEXT_STEP" => "/personal/mortgage/the-apartment-on-the-secondary-market/the-apartment-on-the-secondary-market/"
                ],
                [
                    "TITLE" => "Ипотека в ЖК «SAMPO»",
                    "NEXT_STEP" => "/personal/mortgage/the-apartment-on-the-secondary-market/sampo_secondary/"
                ],
                [
                    "TITLE" => "Ипотека на объекты с баланса Банка",
                    "NEXT_STEP" => "/personal/mortgage/the-apartment-on-the-secondary-market/property-from-the-bank-s-balance-sheet/"
                ],
                [
                    "TITLE" => "Ипотека на комнату",
                    "NEXT_STEP" => "/personal/mortgage/the-apartment-on-the-secondary-market/room-in-the-secondary-market/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Ипотека -> Дом
    case 'house':
        $data_step = ([
            'STEP_ID' => 'house',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Ипотека на дом",
                    "NEXT_STEP" => "/personal/mortgage/house-with-land/house-with-land/"
                ],
                [
                    "TITLE" => "Ипотека на объекты с баланса Банка",
                    "NEXT_STEP" => "/personal/mortgage/house-with-land/balance/"
                ],
                [
                    "TITLE" => "Ипотека на дом в КП «Примавера»",
                    "NEXT_STEP" => "/personal/mortgage/house-with-land/property-in-kp-primavera/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Ипотека -> Для военнослужащих
    case 'military':
        $data_step = ([
            'STEP_ID' => 'military',
            'TITLE' => 'Выберите тип жилья',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Новостройки",
                    "NEXT_STEP" => "military_new_buildings"
                ],
                [
                    "TITLE" => "Вторичное жилье",
                    "NEXT_STEP" => "military_old_buildings"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Ипотека -> Для военнослужащих -> Новостройки
    case 'military_new_buildings':
        $data_step = ([
            'STEP_ID' => 'military_new_buildings',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Ипотека на новостройку",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-new8079/"
                ],
                [
                    "TITLE" => "Ипотека «Семейная»",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-new-family/"
                ],
                [
                    "TITLE" => "Ипотека в ЖК «SAMPO»",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-sampo/"
                ],
                [
                    "TITLE" => "Ипотека по стандартам АИЖК",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-aijk/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Ипотека -> Для военнослужащих -> Вторичное жилье
    case 'military_old_buildings':
        $data_step = ([
            'STEP_ID' => 'military_old_buildings',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Ипотека на вторичное жилье",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-secondary/"
                ],
                [
                    "TITLE" => "Ипотека «Семейная»",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-secondary-family/"
                ],
                [
                    "TITLE" => "Ипотека в ЖК «SAMPO»",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-sampo/"
                ],
                [
                    "TITLE" => "Ипотека по стандартам АИЖК",
                    "NEXT_STEP" => "/personal/mortgage/military-mortgage/military-mortgage-aijk/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;

    // Кредиты
    case 'credits':
        $data_step = ([
            'STEP_ID' => 'credits',
            'TITLE' => 'Выберите тип кредита',
            'STEP_NUMBER' => '1',
            'BUTTONS' => [
                [
                    "TITLE" => "Потребительские кредиты",
                    "NEXT_STEP" => "consumer_credits"
                ],
                [
                    "TITLE" => "Автокредиты",
                    "NEXT_STEP" => "autocredits"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Потребительские кредиты
    case 'consumer_credits':
        $data_step = ([
            'STEP_ID' => 'consumer_credits',
            'TITLE' => 'Выберите тип кредита',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Без залога и поручительства",
                    "NEXT_STEP" => "consumer_credits_no_collateral"
                ],
                [
                    "TITLE" => "Под поручительство или залог",
                    "NEXT_STEP" => "consumer_credits_collateral"
                ],
                [
                    "TITLE" => "Для военнослужащих",
                    "NEXT_STEP" => "consumer_credits_military"
                ],
                [
                    "TITLE" => "Рефинансирование",
                    "NEXT_STEP" => "consumer_credits_refinancing"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Потребительские кредиты -> Без залога и поручительства
    case 'consumer_credits_no_collateral':
        $data_step = ([
            'STEP_ID' => 'consumer_credits_no_collateral',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредит без залога и поручительства",
                    "NEXT_STEP" => "/personal/credits/personal-loans/na-lyubye-tseli-bez-zaloga-i-poruchiteley/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Потребительские кредиты -> Под поручительство или залог
    case 'consumer_credits_collateral':
        $data_step = ([
            'STEP_ID' => 'consumer_credits_collateral',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредит под поручительство или залог",
                    "NEXT_STEP" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/"
                ],
                [
                    "TITLE" => "Кредит под залог недвижимости",
                    "NEXT_STEP" => "/personal/credits/personal-loans/pod-zalog-nedvizhimosti/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Потребительские кредиты -> Для военнослужащих
    case 'consumer_credits_military':
        $data_step = ([
            'STEP_ID' => 'consumer_credits_military',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредит для военнослужащих",
                    "NEXT_STEP" => "/personal/credits/personal-loans/dlya-voennosluzhashchikh/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Потребительские кредиты -> Рефинансирование
    case 'consumer_credits_refinancing':
        $data_step = ([
            'STEP_ID' => 'consumer_credits_refinancing',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредит на рефинансирование",
                    "NEXT_STEP" => "/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Автокредиты
    case 'autocredits':
        $data_step = ([
            'STEP_ID' => 'autocredits',
            'TITLE' => 'Выберите тип кредита',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Новый автомобиль",
                    "NEXT_STEP" => "autocredits_new_auto"
                ],
                [
                    "TITLE" => "Автомобиль с пробегом",
                    "NEXT_STEP" => "autocredits_old_auto"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Автокредиты -> Новый автомобиль
    case 'autocredits_new_auto':
        $data_step = ([
            'STEP_ID' => 'autocredits_new_auto',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредит на новое авто",
                    "NEXT_STEP" => "/personal/credits/carloans/new-car/"
                ],
                [
                    "TITLE" => "Кредит на новое авто без первоначального взноса",
                    "NEXT_STEP" => "/personal/credits/carloans/buying-a-new-car-without-a-down-payment/"
                ],
                [
                    "TITLE" => "Кредит на авто с остаточным платежом",
                    "NEXT_STEP" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/"
                ],
                [
                    "TITLE" => "Кредит на покупку нового коммерческого транспорта",
                    "NEXT_STEP" => "/personal/credits/carloans/commercial-vehicles/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Кредиты -> Автокредиты -> Автомобиль с пробегом
    case 'autocredits_old_auto':
        $data_step = ([
            'STEP_ID' => 'autocredits_old_auto',
            'TITLE' => 'Выберите программу кредитования',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредит на авто с пробегом",
                    "NEXT_STEP" => "/personal/credits/carloans/car-old/"
                ],
                [
                    "TITLE" => "Кредит на авто с пробегом без первоначального взноса",
                    "NEXT_STEP" => "/personal/credits/carloans/buying-a-used-car-without-a-down-payment/"
                ],
                [
                    "TITLE" => "Кредит на покупку коммерческого транспорта с пробегом",
                    "NEXT_STEP" => "/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;

    // Карты
    case 'cards':
        $data_step = ([
            'STEP_ID' => 'cards',
            'TITLE' => 'Выберите тип карты',
            'STEP_NUMBER' => '1',
            'BUTTONS' => [
                [
                    "TITLE" => "Кредитные",
                    "NEXT_STEP" => "cards_credit"
                ],
                [
                    "TITLE" => "Дебетовые",
                    "NEXT_STEP" => "cards_debit"
                ],
                [
                    "TITLE" => "Предоплаченные",
                    "NEXT_STEP" => "cards_prepaid"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Кредитные
    case 'cards_credit':
        $data_step = ([
            'STEP_ID' => 'cards_credit',
            'TITLE' => 'Выберите категорию',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Путешествия",
                    "NEXT_STEP" => "cards_credit_travel"
                ],
                [
                    "TITLE" => "Ежедневная выгода",
                    "NEXT_STEP" => "cards_credit_benefit"
                ],
                [
                    "TITLE" => "Премиальные",
                    "NEXT_STEP" => "cards_credit_premium"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Кредитные -> Путешествия
    case 'cards_credit_travel':
        $data_step = ([
            'STEP_ID' => 'cards_credit_travel',
            'TITLE' => 'Выберите карту',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредитная карта iGlobe",
                    "NEXT_STEP" => "/personal/cards/credit-card/credit-card-world-of-travel/"
                ],
                [
                    "TITLE" => "Кредитная карта Finnair",
                    "NEXT_STEP" => "/personal/cards/credit-card/credit-card-finnair/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Кредитные -> Ежедневная выгода
    case 'cards_credit_benefit':
        $data_step = ([
            'STEP_ID' => 'cards_credit_benefit',
            'TITLE' => 'Выберите карту',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредитная карта с кэшбэком",
                    "NEXT_STEP" => "/personal/cards/credit-card/credit-card-with-cash-back/"
                ],
                [
                    "TITLE" => "Кредитная карта «Мир»",
                    "NEXT_STEP" => "/personal/cards/credit-card/classic-credit-card-the-world/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Кредитные -> Премиальные
    case 'cards_credit_premium':
        $data_step = ([
            'STEP_ID' => 'cards_credit_premium',
            'TITLE' => 'Выберите карту',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Кредитная карта Mastercard Platinum",
                    "NEXT_STEP" => "/personal/cards/credit-card/credit-card-mastercard-platinum/"
                ],
                [
                    "TITLE" => "Кредитная карта Visa Platinum",
                    "NEXT_STEP" => "/personal/cards/credit-card/visa-platinum-credit-card/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Дебетовые
    case 'cards_debit':
        $data_step = ([
            'STEP_ID' => 'cards_debit',
            'TITLE' => 'Выберите тип карты',
            'STEP_NUMBER' => '2',
            'BUTTONS' => [
                [
                    "TITLE" => "Путешествия",
                    "NEXT_STEP" => "cards_debit_travel"
                ],
                [
                    "TITLE" => "Накопления",
                    "NEXT_STEP" => "cards_debit_collection"
                ],
                [
                    "TITLE" => "Ежедневная выгода",
                    "NEXT_STEP" => "cards_debit_benefit"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Дебетовые -> Путешествия
    case 'cards_debit_travel':
        $data_step = ([
            'STEP_ID' => 'cards_debit_travel',
            'TITLE' => 'Выберите карту',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Дебетовая карта Finnair ",
                    "NEXT_STEP" => "/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Дебетовые -> Накопления
    case 'cards_debit_collection':
        $data_step = ([
            'STEP_ID' => 'cards_debit_collection',
            'TITLE' => 'Выберите карту',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Дебетовая карта «Доходный остаток»",
                    "NEXT_STEP" => "/personal/cards/debit-card/card-profitable-balance/"
                ],
                [
                    "TITLE" => "Дебетовая карта «Инициатива»",
                    "NEXT_STEP" => "/personal/cards/debit-card/map-initiative/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Дебетовые -> Ежедневная выгода
    case 'cards_debit_benefit':
        $data_step = ([
            'STEP_ID' => 'cards_debit_benefit',
            'TITLE' => 'Ежедневная выгода',
            'STEP_NUMBER' => '3',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Дебетовая карта с кэшбэком",
                    "NEXT_STEP" => "/personal/cards/debit-card/card-with-cash-back/"
                ],
                [
                    "TITLE" => "Дебетовая карта с «Мир»",
                    "NEXT_STEP" => "/personal/cards/debit-card/classical-map-the-world/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Карты -> Предоплаченные
    case 'cards_prepaid':
        $data_step = ([
            'STEP_ID' => 'cards_prepaid',
            'TITLE' => 'Выберите карту',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Предоплаченная карта Finnair",
                    "NEXT_STEP" => "/"
                ],
            ]
        ]);
        echo json_encode($data_step);
        break;

    // Вклады
    case 'holdings':
        $data_step = ([
            'STEP_ID' => 'holdings',
            'TITLE' => 'Выберите цель',
            'STEP_NUMBER' => '1',
            'BUTTONS' => [
                [
                    "TITLE" => "Максимальный доход",
                    "NEXT_STEP" => "holdings_max_revenue"
                ],
                [
                    "TITLE" => "Накопление",
                    "NEXT_STEP" => "holdings_benefit"
                ],
                [
                    "TITLE" => "Максимальная гибкость",
                    "NEXT_STEP" => "holdings_max_flexibility"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Вклады -> Максимальный доход
    case 'holdings_max_revenue':
        $data_step = ([
            'STEP_ID' => 'holdings_max_revenue',
            'TITLE' => 'Выберите вклад',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Вклад «ЗЕНИТ Плюс»",
                    "NEXT_STEP" => "/personal/deposits/the-contribution-of-zenit-plyus/"
                ],
                [
                    "TITLE" => "Вклад «Высокий доход»",
                    "NEXT_STEP" => "/personal/deposits/high-profit/"
                ],
                [
                    "TITLE" => "Вклад «Стратегия лидерства»",
                    "NEXT_STEP" => "/personal/deposits/leadership-strategy/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Вклады -> Накопление
    case 'holdings_benefit':
        $data_step = ([
            'STEP_ID' => 'holdings_benefit',
            'TITLE' => 'Выберите вклад',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Вклад «Пополняемый доход»",
                    "NEXT_STEP" => "/personal/deposits/the-contribution-of-the-growing-income/"
                ],
                [
                    "TITLE" => "Вклад «Инициатива»",
                    "NEXT_STEP" => "/personal/deposits/initiative/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
    // Вклады -> Максимальная гибкость
    case 'holdings_max_flexibility':
        $data_step = ([
            'STEP_ID' => 'holdings_max_flexibility',
            'TITLE' => 'Выберите вклад',
            'STEP_NUMBER' => '2',
            'FINAL_STEP' => true,
            'BUTTONS' => [
                [
                    "TITLE" => "Вклад «Большие возможности»",
                    "NEXT_STEP" => "/personal/deposits/great-opportunity/"
                ],
                [
                    "TITLE" => "Счёт «Накопительный Онлайн»",
                    "NEXT_STEP" => "/personal/deposits/the-account-savings-online/"
                ]
            ]
        ]);
        echo json_encode($data_step);
        break;
}
