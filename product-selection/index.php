<?
use \Bitrix\Main\Application;
if($_REQUEST['get_header'] == 'Y') {?>
    <div class="top_breadcrumbs" style="background-image: url('http://usbgo.ru/local/templates/bz/img/top_banner/bg_inner_02.jpg'); height: calc(100vh - 147px);">
        <table class="table_top_breadcrumbs">
            <tbody>
            <tr>
                <td>
                    <!--breadcrumbs-->
                    <div class="wr_breadcrumbs">

                    </div>
                    <!--breadcrumbs-->
                    <div class="wr_title_header_block c-container">
                        <div class="title_header_block">
                            <div class="title">
                                Квартиры в новостройках
                            </div>
                            <div class="title_to">
                                <p>Кредит на осуществление мечты о квартире в новостройке</p>
                                <ul class="big_list">
                                    <li>До 16 млн рублей на срок до 30 лет</li>
                                    <li>Первоначальный взнос – от 20%</li>
                                    <li>От 11,9% годовых</li>
                                </ul>
                                <br>
                                <p><a href="#" class="button" data-yourapplication=""
                                      data-classapplication=".wr_form_application"
                                      data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить
                                        заявку</a></p>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
`
<?} else {

    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    $APPLICATION->IncludeComponent(
        "bitrix:news.detail",
        "product_selection",
        Array(
            'AJAX_MODE' => 'N',
            "DISPLAY_DATE" => 'N',
            "DISPLAY_NAME" => '',
            "DISPLAY_PICTURE" => '',
            "DISPLAY_PREVIEW_TEXT" => '',
            "IBLOCK_TYPE" => 'mortgage',
            "IBLOCK_ID" => '6',
            "FIELD_CODE" => array('', ''),
            "PROPERTY_CODE" => Array
            (
                'REALTY_TYPE', 'ADDITIONALLY', ''
            ),
            "DETAIL_URL" => '',
            "SECTION_URL" => '',
            "META_KEYWORDS" => '',
            "META_DESCRIPTION" => '',
            "BROWSER_TITLE" => '',
            "SET_CANONICAL_URL" => '',
            "DISPLAY_PANEL" => '',
            "SET_LAST_MODIFIED" => '',
            "SET_TITLE" => '',
            "MESSAGE_404" => '',
            "SET_STATUS_404" => '',
            "SHOW_404" => '',
            "FILE_404" => '',
            "INCLUDE_IBLOCK_INTO_CHAIN" => 'N',
            "ADD_SECTIONS_CHAIN" => 'N',
            "ACTIVE_DATE_FORMAT" => 'd.m.Y',
            "CACHE_TYPE" => 'N',
            "CACHE_TIME" => '36000',
            "CACHE_GROUPS" => 'N',
            "USE_PERMISSIONS" => 'N',
            "GROUP_PERMISSIONS" => '',
            "DISPLAY_TOP_PAGER" => '',
            "DISPLAY_BOTTOM_PAGER" => '',
            "PAGER_TITLE" => '',
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => '',
            "PAGER_SHOW_ALL" => '',
            "CHECK_DATES" => 'Y',
            "ELEMENT_ID" => '',
            "ELEMENT_CODE" => 'new',
            "SECTION_ID" => '',
            "SECTION_CODE" => 'new',
            "IBLOCK_URL" => '',
            "USE_SHARE" => '',
            "SHARE_HIDE" => '',
            "SHARE_TEMPLATE" => '',
            "SHARE_HANDLERS" => '',
            "SHARE_SHORTEN_URL_LOGIN" => '',
            "SHARE_SHORTEN_URL_KEY" => '',
            "ADD_ELEMENT_CHAIN" => '',
            'STRICT_SECTION_CHECK' => '',
        )
    );
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>