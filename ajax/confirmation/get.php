<?php

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$email=$request->getPost("email");

if (Bitrix\Main\Loader::includeModule('aic.bz') && strlen($email)>0)
{
    $Confirmation=new \Aic\Bz\cEmailConfirmation();
    $Confirmation->GetConfirmation($email);
}