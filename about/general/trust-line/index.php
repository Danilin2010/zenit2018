<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Линия доверия");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <!-- Правый блок мобилка и планшет -->
            <div class="col-sm-12 col-md-4 hidden-lg">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">
                            <p>
                                В данном разделе Вы имеете возможность воспользоваться предоставленными каналами коммуникации, чтобы сообщить информацию, вызвавшую Вашу обеспокоенность в деятельности ПАО Банк ЗЕНИТ (далее – Банк) и его работников.
                            </p>
                            <p>
                                Вся поступающая от Вас информация, независимо от выбранного канала связи, попадает к уполномоченным представителям Банка для ее последующего анализа и проверки*.
                            </p>
                            <p>
                                * Вы можете направить конфиденциальные сообщения:
                            </p>
                            <ul>
                                <li>
                                    о фактах нарушений законодательства, внутренних процедур, Кодекса корпоративной этики Банка любым работником и (или) любым членом органов управления или органов контроля за финансово-хозяйственной деятельностью Банка,
                                </li>
                                <li>
                                    c предложениями по улучшению антикоррупционных процедур и иных процедур внутреннего контроля,
                                </li>
                                <li>
                                    с указанием иной информации по направлению внутреннего контроля и безопасности.
                                </li>
                            </ul>
                            <p>
                                Направление официальных ответов на сообщения, направленные указанными в настоящем разделе способами, не подразумевается. При необходимости получения официального ответа на обращение, а также в целях обращения по вопросам, отличающихся от вышеперечисленных, просим Вас направлять сообщение на e-mail Банка info@zenit.ru, либо на почтовый адрес Россия, 129110, Москва, Банный пер.,9 (без пометки «Линия доверия» на конверте).
                            </p>
                            <h3>Контакты «Линии доверия»</h3>
                            <p><strong>E-mail</strong>: glb@zenit.ru</p>
                            <p><strong>Почтовый адрес</strong>: Россия, 129110, Москва, Банный пер.,9, с пометкой: «Линия доверия».</p>

                            <h3>Форма online-сообщения</h3>

                            <?$APPLICATION->IncludeComponent(
                                "bitrix:form.result.new",
                                "trust-line",
                                array(
                                    "CACHE_TIME" => "3600",
                                    "CACHE_TYPE" => "N",
                                    "CHAIN_ITEM_LINK" => "",
                                    "CHAIN_ITEM_TEXT" => "",
                                    "EDIT_URL" => "",
                                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                                    "LIST_URL" => "",
                                    "SEF_MODE" => "N",
                                    "SUCCESS_URL" => "",
                                    "USE_EXTENDED_ERRORS" => "Y",
                                    "WEB_FORM_ID" => "6",
                                    "COMPONENT_TEMPLATE" => "trust-line",
                                    "SOURCE_TREATMENT" => "",
                                    "RIGHT_TEXT" => "",
                                    "VARIABLE_ALIASES" => array(
                                        "WEB_FORM_ID" => "WEB_FORM_ID",
                                        "RESULT_ID" => "RESULT_ID",
                                    )
                                ),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Правый блок для ПС -->
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                            Array(),
                            Array("MODE"=>"txt","SHOW_BORDER"=>false)
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>