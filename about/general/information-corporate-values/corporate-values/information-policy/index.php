<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информационная политика");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">
                    <div class="col-sm-12">
                        <div class="doc_list">
                            <a class="doc_item full-contener" href="/upload/about/pdf/mesures.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Перечень мер, направленных на предотвращение неправомерного использования служебной информации при осуществлении профессиональной деятельности на рынке ценных бумаг
                                    </div>
                                    <div class="doc_note">
                                        105 Кб
                                    </div>
                                </div>
                            </a>
                            <a class="doc_item full-contener" href="/upload/about/pdf/ins_information.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Положение об инсайдерской информации Банка ЗЕНИТ
                                    </div>
                                    <div class="doc_note">
                                        131 Кб
                                    </div>
                                </div>
                            </a>
                            <a class="doc_item full-contener" href="/upload/about/pdf/poriadok_inside.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Порядок доступа к инсайдерской информации Банка ЗЕНИТ
                                    </div>
                                    <div class="doc_note">
                                        210 Кб
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">
                            <p>
                                Информационная политика ПАО Банк ЗЕНИТ направлена на обеспечение акционеров, инвесторов и иных заинтересованных лиц наиболее полной и достоверной информацией о Банке и осуществляемой им деятельности.
                            </p>
                            <p>
                                В случае если Банк предоставляет информацию по собственной инициативе, то способ предоставления информации определяется Банком самостоятельно с учетом интересов акционеров, инвесторов и иных заинтересованных лиц.
                            </p>
                            <p>
                                Координацию деятельности работников Банка в сфере реализации информационной политики Банка осуществляет Председатель Правления Банка и член Правления Банка, определенный Председателем Правления.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <p>
                                Основными принципами информационной политики Банка являются:
                            </p>
                            <ul class="big_list">
                                <li>
                                    соблюдение требований Федерального закона «О банках и банковской деятельности» № 395-1 от 2 декабря 1990 г., Федерального закона «Об акционерных обществах» № 208-ФЗ от 26 декабря 1995 г., Федерального закона «О рынке ценных бумаг» № 39-ФЗ от 22 апреля 1996 г., Федерального закона «О коммерческой тайне» № 98-ФЗ от 29 июля 2004 г., нормативных актов Центрального Банка Российской Федерации и федерального органа исполнительной власти по рынку ценных бумаг, иных нормативных актов в сфере раскрытия информации;
                                </li>
                                <li>
                                    регулярность предоставления информации;
                                </li>
                                <li>
                                    оперативность предоставления информации;
                                </li>
                                <li>
                                    доступность информации для акционеров, инвесторов и иных заинтересованных лиц;
                                </li>
                                <li>
                                    достоверность и полнота раскрываемой информации;
                                </li>
                                <li>
                                    соблюдение разумного баланса между открытостью Банка и обеспечением его коммерческих интересов.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <p>
                                Банк в обязательном порядке раскрывает информацию, раскрытие которой предусмотрено Федеральным законом «Об акционерных обществах» № 208-ФЗ от 26 декабря 1995 г., Федеральным законом «О рынке ценных бумаг» № 39-ФЗ от 22 апреля 1996 г., Федеральным законом «О банках и банковской деятельности» № 395-1 от 2 декабря 1990 г., нормативными актами Центрального Банка Российской Федерации и федерального органа исполнительной власти по рынку ценных бумаг, иными нормативными актами в сфере раскрытия информации, а также требованиями организаторов торговли на рынке ценных бумаг, к торгам у которых допущены ценные бумаги Банка. Банк вправе также предоставлять иную информацию, которая может иметь значение для акционеров, инвесторов и иных заинтересованных лиц в связи с принятием ими управленческих и инвестиционных решений в отношении Банка.
                            </p>
                            <p>
                                В порядке и случаях, установленных действующим законодательством РФ Банк представляет информацию о своих акционерах, об акционерах акционеров, а члены Совета директоров Банка, Председатель Правления Банка, члены Правления Банка о продаже и (или) покупке ценных бумаг Банка в Центральный Банк РФ, Федеральную службу по финансовым рынкам, Федеральную антимонопольную службу.
                            </p>
                            <p>
                                Банк ежегодно представляет акционерам годовой отчет о своей деятельности. Состав информации, содержащейся в отчете, должен соответствовать требованиям действующего законодательства РФ и позволять акционерам оценить итоги деятельности Банка за год.
                            </p>
                            <p>
                                Банк стремится к ограничению возможности возникновения конфликта интересов и недопущению злоупотребления инсайдерской информацией. Использование инсайдерской информации о деятельности Банка и ее защита регулируются Положением об инсайдерской информации Банка.
                            </p>
                            <p>
                                Информационная политика Банковской группы ЗЕНИТ определяет общие принципы информационного взаимодействия между ПАО Банк ЗЕНИТ, АБ «Девон-Кредит» (ОАО), ОАО «Липецкомбанк», ОАО «Спиритбанк» и ЗАО Банк ЗЕНИТ Сочи в рамках Банковской группы ЗЕНИТ.
                            </p>
                            <p>
                                Информационная политика Банковской группы ЗЕНИТ разработана в соответствии с действующим законодательством Российской Федерации, Уставами ПАО Банк ЗЕНИТ, АБ «Девон-Кредит» (ОАО), ОАО «Липецкомбанк», ОАО «Спиритбанк» и ЗАО Банк ЗЕНИТ Сочи.
                            </p>
                            <p>
                                Целью совместной информационной политики является наиболее полное удовлетворение информационных потребностей ПАО Банк ЗЕНИТ, как Головной организации, и АБ «Девон-Кредит» (ОАО), ОАО «Липецкомбанк», ОАО «Спиритбанк» и ЗАО Банк ЗЕНИТ Сочи как кредитных организаций, входящих в Банковскую группу, в достоверной информации о Банковской группе и ее деятельности.
                            </p>
                            <p>
                                Информационная политика Банковской группы ЗЕНИТ не регулирует раскрытие информации ПАО Банк ЗЕНИТ, АБ «Девон-Кредит» (ОАО), ОАО «Липецккомбанк», ОАО «Спиритбанк» и ЗАО Банк ЗЕНИТ Сочи как участниками рынка ценных бумаг. Раскрытие данной информации регулируется внутренними нормативными документами ПАО Банк ЗЕНИТ АБ «Девон-Кредит» (ОАО), ОАО «Липецкомбанк», ОАО «Спиритбанк» и ЗАО Банк ЗЕНИТ Сочи.
                            </p>
                            <p>
                                Порядок и основания предоставления служебной информации и коммерческой тайны участников Банковской группы ЗЕНИТ устанавливаются законодательством Российской Федерации, а также внутренними нормативными документами участников группы. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="block-card right note_text">
                            <div class="doc_list">
                                <a class="doc_item full-contener" href="/upload/about/pdf/mesures.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Перечень мер, направленных на предотвращение неправомерного использования служебной информации при осуществлении профессиональной деятельности на рынке ценных бумаг
                                        </div>
                                        <div class="doc_note">
                                            105 Кб
                                        </div>
                                    </div>
                                </a>
                                <a class="doc_item full-contener" href="/upload/about/pdf/ins_information.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Положение об инсайдерской информации Банка ЗЕНИТ
                                        </div>
                                        <div class="doc_note">
                                            131 Кб
                                        </div>
                                    </div>
                                </a>
                                <a class="doc_item full-contener" href="/upload/about/pdf/poriadok_inside.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Порядок доступа к инсайдерской информации Банка ЗЕНИТ
                                        </div>
                                        <div class="doc_note">
                                            210 Кб
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>