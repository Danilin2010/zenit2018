<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Совет директоров Банка");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">
                    <div class="col-sm-12">
                        <div class="doc_list">
                            <a class="doc_item full-contener" href="/upload/about/pdf/PolSD_news.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Положение о Совете директоров ОАО Банк ЗЕНИТ
                                    </div>
                                    <div class="doc_note">
                                        193.8 Кб
                                    </div>
                                </div>
                            </a>
                            <a class="doc_item full-contener" href="/upload/about/pdf/PolSD_ch1.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Изменения №1 в Положение о Совете директоров ОАО Банк ЗЕНИТ
                                    </div>
                                    <div class="doc_note">
                                        51 Кб
                                    </div>
                                </div>
                            </a>
                            <a class="doc_item full-contener" href="/upload/about/pdf/PolSD_ch2.pdf" target="_blank">
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Изменения №2 в Положение о Совете директоров ОАО Банк ЗЕНИТ
                                    </div>
                                    <div class="doc_note">
                                        53 Кб
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">
                            <p>
                                Совет директоров ПАО Банк ЗЕНИТ осуществляет общее руководство деятельностью Банка, за исключением решения вопросов, отнесенных к компетенции Общего собрания акционеров. К компетенции Совета директоров Банка относятся вопросы, предусмотренные Федеральным законом «Об акционерных обществах» и Уставом Банка.
                            </p>
                            <p>
                                Председатель Совета директоров - Генеральный директор ПАО "Татнефть" им. В.Д. Шашина Маганов Наиль Ульфатович.
                            </p>
                            <p>
                                В состав Совета директоров Банка ЗЕНИТ, исходя из требований, выполнение которых является условием включения акций акционерных обществ в котировальные списки фондовых бирж РФ (п.13.3 Устава), входят независимые директора г-н Дертниг Шт., г-н Панферов А.В., г-н Шибаев С.В.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Батырев Антон Искандерович</h3>
                            <p>
                                 Родился в 1977 г. В 1998 году<br>
                                окончил Юридический колледж МГУ им. М.В. Ломоносова, в 1999 году University of Michigan Law School. <br>
                                Советник Президента (Председателя Правления) по международному развитию ПАО "НЛМК".
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Воробьев Алексей Сергеевич</h3>
                            <p>
                                Родился в 1979 г. В 2001 году<br>
                                окончил Государственный Университет Управления. Кандидат экономических наук. Заместитель директора Департамента финансовых институтов Внешэкономбанка.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Дертниг Штефан Франц</h3>
                            <p>
                                Родился в 1961 г.<br>
                                 Образование: Венский технический университет, Австрия; INSEAD, Франция. В период с 1990 г. по 2012 г. занимал различные руководящие должности в The Boston Consulting Group, руководил программой MBA и преподавал стратегию в бизнес-школе Сколково. <br>
                                Директор DD-Management & Advisory.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Маганов Наиль Ульфатович</h3>
                            <p>
                                Родился в 1958 г.<br>
                                 В 1983 году окончил Московский институт нефтехимической и газовой промышленности имени И.М.Губкина. Председатель Правления, Генеральный директор ПАО «Татнефть» им. В.Д.Шашина. <br>
                                Председатель Совета директоров ПАО Банк ЗЕНИТ.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Панферов Алексей Валерьевич </h3>
                            <p>
                                Родился в 1970 году. <br>
                                В 1993 году окончил МИРЭА, в 1995 г. окончил МИМБ при ВАВТ МВЭС РФ. Член Наблюдательного совета, советник Председателя Правления ПАО «Совкомбанк».
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Соколова Мария Александровна</h3>
                            <p>
                                Родилась в 1959 году.<br>
                                 В 1981 году окончила Московский Финансовый институт. Кандидат экономических наук. Директор постоянного Представительства КОО «Бренткросс Холдингс Лимитед».
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Сюбаев Нурислам Зинатулович</h3>
                            <p>
                                Родился в 1960 году.<br>
                                 В 1982 году окончил Московский ордена Трудового Красного Знамени институт народного хозяйства им. Г.В. Плеханова, в 1995 году окончил Институт переподготовки и повышения квалификации кадров по финансово-банковским специальностям Финансовой академии при Правительстве Российской Федерации. Член Правления, Заместитель генерального директора по стратегическому развитию ПАО «Татнефть» им. В.Д. Шашина.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Тахаутдинов Шафагат Фахразович</h3>
                            <p>
                                Родился в 1946 г.<br>
                                 В 1971 году окончил Московский институт нефтехимической и газовой промышленности И.М.Губкина. Доктор экономических наук. Член Совета директоров ПАО «Татнефть» им. В.Д.Шашина. Советник Председателя Совета директоров ПАО «Татнефть» им. В.Д. Шашина.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Тихтуров Евгений Александрович</h3>
                            <p>
                                Родился в 1960 г.<br>
                                 В 1982 году окончил Московский институт управления. Член Правления, Начальник управления финансов ПАО «Татнефть» им. В.Д.Шашина.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Шибаев Сергей Викторович</h3>
                            <p>
                                Родился в 1959 г.<br>
                                 В 1981 году окончил МГИМО МИД СССР, в 1995 году - Ассоциацию Дипломированных Сертифицированных Бухгалтеров (АССА), в 1997 году - Хенли Менеджмент Колледж, Великобритания. Кандидат экономических наук.<br>
                                 Директор компании «Kafa Finance Inc.», Канада.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
                        <div class="block-card note_text pt-4">
                            <h3>Шпигун Кирилл Олегович</h3>
                            <p>
                                Родился в 1970 г.<br>
                                 В 1992 году окончил Московский государственный университет им. М.В. Ломоносова, в 1997 году - Институт переподготовки и повышения квалификации кадров по финансово-банковским специальностям Финансовой академии при Правительстве РФ по направлению «Банковское дело».<br>
                                 Председатель Правления ПАО Банк ЗЕНИТ.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="block-card right note_text">
                            <div class="doc_list">
                                <a class="doc_item full-contener" href="/upload/about/pdf/PolSD_news.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Положение о Совете директоров ОАО Банк ЗЕНИТ
                                        </div>
                                        <div class="doc_note">
                                            193.8 Кб
                                        </div>
                                    </div>
                                </a>
                                <a class="doc_item full-contener" href="/upload/about/pdf/PolSD_ch1.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Изменения №1 в Положение о Совете директоров ОАО Банк ЗЕНИТ
                                        </div>
                                        <div class="doc_note">
                                            51 Кб
                                        </div>
                                    </div>
                                </a>
                                <a class="doc_item full-contener" href="/upload/about/pdf/PolSD_ch2.pdf" target="_blank">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Изменения №2 в Положение о Совете директоров ОАО Банк ЗЕНИТ
                                        </div>
                                        <div class="doc_note">
                                            53 Кб
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>