<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корпоративное управление");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
use \Bitrix\Main\Page\Asset;
?>


                                <p>Корпоративное управление — это система принципов, норм и правил, в соответствии с которыми осуществляется регулирование взаимоотношений между акционерами, членами Совета директоров, исполнительными органами Банка, а также другими заинтересованными лицами.</p>
                                <p>Банковская группа ЗЕНИТ развивает систему корпоративного управления, приводя ее в соответствие с лучшими мировыми стандартами и практиками публичных международных корпораций.</p>
                                <p>Мы считаем, что качество корпоративного управления способствует успешному развитию Банковской группы и повышению ее инвестиционной привлекательности, дает дополнительные гарантии партнерам.</p>
                                <p>Высокие стандарты корпоративного управления позволяют в полной мере учитывать интересы акционеров и инвесторов, клиентов, бизнес-партнеров, широкого круга некоммерческих организаций и населения регионов нашего присутствия, положительно влияя на устойчивое развитие бизнеса.</p>
                                <p>Банковская группа ЗЕНИТ развивает стандарты корпоративного управления, основываясь на принципах прозрачного и ответственного ведения бизнеса.</p>
<p>Ключевыми аспектами этой деятельности являются:.</p>
                                <ul class="big_list">
                                    <li>
                                        выполнение требований Кодекса корпоративного управления и связанных с ним регламентирующих документов;
                                    </li>
                                    <li>
                                        назначение независимых директоров;
                                    </li>
                                    <li>
                                        эффективная деятельность комитетов Совета директоров, включая Комитет по аудиту, Комитет по стратегическому планированию, Комитет по кадрам и вознаграждениям;
                                    </li>
                                    <li>
                                        принятие Кодекса корпоративной этики.
                                    </li>
                                </ul>
                                <p>Лидерство на финансовых рынках — это не только высокое качество банковских услуг, но и высокие стандарты ведения бизнеса, социальной ответственности и корпоративного управления.</p>

<?$APPLICATION->IncludeComponent(
    "bitrix:news.list", 
    "corporate-governance", 
    array(
        "COMPONENT_TEMPLATE" => "corporate-governance",
        "IBLOCK_TYPE" => "reporting",
        "IBLOCK_ID" => "147",
        "NEWS_COUNT" => "1000",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(
            0 => "NAME",
            1 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "TITLE_AMENDMENT_CHARTER",
            1 => "FILE",
            2 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_LAST_MODIFIED" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "N",
        "STRICT_SECTION_CHECK" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "PAGER_TEMPLATE" => "",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => ""
    ),
    false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>