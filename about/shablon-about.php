<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Инсайдерам");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');

use \Bitrix\Main\Page\Asset;
//Asset::getInstance()->addCss('');
//Asset::getInstance()->addCss('/about/px/px.css');
//Asset::getInstance()->addJs('');
?>

    <div class="container about-page">
        <div class="row">
            <!-- Правый блок мобилка и планшет -->
            <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">

                </div>
            </div>
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">
                            <!--content_tab-->
                            <div class="content_rates_tabs">
                                <ul class="content_tab c-container">
                                    <li><a href="#content-tabs-1">Преимущества</a></li>
                                    <li><a href="#content-tabs-2">Информация и тарифы</a></li>
                                    <li><a href="#content-tabs-3">Как пользоваться</a></li>
                                </ul>
                                <div class="content_body" id="content-tabs-1">

                                </div>
                                <div class="content_body" id="content-tabs-2">

                                </div>
                                <div class="content_body" id="content-tabs-3">

                                </div>
                            </div>
                            <!--content_tab-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Правый блок для ПС -->
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="block-card right note_text">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>