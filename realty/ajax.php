<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
/*header('Cache-Control: no-cache, must-revalidate');
header('Expires: '.date('r',time()-86400));
header('Content-type: application/json');
header('content-encoding: gzip');*/

\Bitrix\Main\Loader::includeModule('aic.bz');

$APPLICATION->IncludeComponent(
    "aic.bz:realty",
    "ajax",
    array(
        'IBLOCK_ID' => '149'
    ),
    false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");



