<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Безналичные конверсионные  операции для юридических лиц");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_rko.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
				<div class="wr_max_block">
                    <div class="right_bank_title">
                        Если у Вас есть вопросы, Вы всегда можете получить информацию по телефонам
                    </div>
                    <br>
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/block/contact_phone.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
				</div>
			</div>
			<div class="block_type_center">
				<h1>Безналичные конверсионные  операции для юридических лиц</h1>
				<ul class="big_list">
					<li>
						по картам VISA CLASSIC «Подари детям улыбку» - 0,3% от суммы операций, совершенных по карте за истекший календарный день, но не более 5 рублей,
						либо 50 рублей, либо 200 рублей по каждой операции (по выбору держателя карты)
					</li>
					<li>
						о картам VISA GOLD «Подари детям улыбку» - 0,3 % от суммы операций, совершенных по карте за истекший календарный день, но не более 50 рублей либо 200 рублей
						по каждой операции (по выбору держателя карты) - программа «Стандарт»
					</li>
					<li>
						по картам VISA GOLD «Подари детям улыбку» - 1 % от суммы операций, совершенных по карте за истекший календарный день, но не менее 50 рублей по каждой операции
						и не более 3 000 рублей по каждой операции - программа «VIP»
					</li>
				</ul>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>