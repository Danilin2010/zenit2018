<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Льготные тарифы для клиентов проблемных банков");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_rko.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_center">
				<div class="text_block">
					<h2>Условия предложения</h2>
					<p>
						ПАО Банк ЗЕНИТ предлагает юридическим лицам и индивидуальным предпринимателям, ранее
						являвшимися клиентами банков с отозванной лицензией, либо банков, в отношении которых
						введена любая из мер по предупреждению банкротства, специальные условия открытия и обслуживания счета
					</p>
					<h2>Оказываемые бесплатно услуги</h2>
					<ul>
						<li>
							открытие первого банковского счета
						</li>
						<li>
							ведение счета в первые три месяца обслуживания
						</li>
						<li>
							подключение системы Клиент-Банк
						</li>
						<li>
							заверение карточки с образцами подписей и оттиска печати
						</li>
						<li>
							изготовление и заверение копий учредительных и иных документов при открытии счета
						</li>
					</ul>
					<h2>Как воспользоваться</h2>
					<p>
						Вам необходимо предоставить документ, подтверждающий наличие расчетного счета в банке, лицензия
						которого отозвана, либо в отношении которого введена любая из мер по предупреждению банкротства
					</p>
					<div class="note_text">
						С полным пакетом документов, необходимых для открытия счета, можно ознакомиться <a href="/big-business/rko/account/opening-of-accounts/#content-tabs-2" target="_blank">здесь</a>

					</div>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/block/rates.php",
    Array(
        "template"=>"rates",
        "all_phone" => "8 (800) 500-66-77",
        "use_clientbank" => "Y",
        "show_blanks" => "N",
    ),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>