<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Тарифные планы");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_rko.png');
?>	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Описание</a></li>
				<li><a href="#content-tabs-2">Правила использования</a></li>
				<li><a href="#content-tabs-3">Как подключить?</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type  c-container"><h1>Бесплатно:</h1>
						<? $APPLICATION->IncludeComponent("bitrix:news.list", "simple_benefits", Array(
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							// Формат показа даты
							"ADD_SECTIONS_CHAIN" => "N",
							// Включать раздел в цепочку навигации
							"AJAX_MODE" => "N",
							// Включить режим AJAX
							"AJAX_OPTION_ADDITIONAL" => "",
							// Дополнительный идентификатор
							"AJAX_OPTION_HISTORY" => "N",
							// Включить эмуляцию навигации браузера
							"AJAX_OPTION_JUMP" => "N",
							// Включить прокрутку к началу компонента
							"AJAX_OPTION_STYLE" => "Y",
							// Включить подгрузку стилей
							"CACHE_FILTER" => "N",
							// Кешировать при установленном фильтре
							"CACHE_GROUPS" => "Y",
							// Учитывать права доступа
							"CACHE_TIME" => "36000",
							// Время кеширования (сек.)
							"CACHE_TYPE" => "A",
							// Тип кеширования
							"CHECK_DATES" => "Y",
							// Показывать только активные на данный момент элементы
							"DETAIL_URL" => "",
							// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
							"DISPLAY_BOTTOM_PAGER" => "N",
							// Выводить под списком
							"DISPLAY_DATE" => "Y",
							// Выводить дату элемента
							"DISPLAY_NAME" => "Y",
							// Выводить название элемента
							"DISPLAY_PICTURE" => "Y",
							// Выводить изображение для анонса
							"DISPLAY_PREVIEW_TEXT" => "Y",
							// Выводить текст анонса
							"DISPLAY_TOP_PAGER" => "N",
							// Выводить над списком
							"FIELD_CODE" => array(    // Поля
								0 => "",
								1 => "",
							),
							"FILTER_NAME" => "",
							// Фильтр
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							// Скрывать ссылку, если нет детального описания
							"IBLOCK_ID" => "64",
							// Код информационного блока
							"IBLOCK_TYPE" => "rko",
							// Тип информационного блока (используется только для проверки)
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							// Включать инфоблок в цепочку навигации
							"INCLUDE_SUBSECTIONS" => "Y",
							// Показывать элементы подразделов раздела
							"MESSAGE_404" => "",
							// Сообщение для показа (по умолчанию из компонента)
							"NEWS_COUNT" => "100",
							// Количество новостей на странице
							"PAGER_BASE_LINK_ENABLE" => "N",
							// Включить обработку ссылок
							"PAGER_DESC_NUMBERING" => "N",
							// Использовать обратную навигацию
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							// Время кеширования страниц для обратной навигации
							"PAGER_SHOW_ALL" => "N",
							// Показывать ссылку "Все"
							"PAGER_SHOW_ALWAYS" => "N",
							// Выводить всегда
							"PAGER_TEMPLATE" => ".default",
							// Шаблон постраничной навигации
							"PAGER_TITLE" => "Новости",
							// Название категорий
							"PARENT_SECTION" => "142",
							// ID раздела
							"PARENT_SECTION_CODE" => " ",
							// Код раздела
							"PREVIEW_TRUNCATE_LEN" => "",
							// Максимальная длина анонса для вывода (только для типа текст)
							"PROPERTY_CODE" => array(    // Свойства
								0 => "",
								1 => "",
								2 => "",
							),
							"SET_BROWSER_TITLE" => "N",
							// Устанавливать заголовок окна браузера
							"SET_LAST_MODIFIED" => "N",
							// Устанавливать в заголовках ответа время модификации страницы
							"SET_META_DESCRIPTION" => "N",
							// Устанавливать описание страницы
							"SET_META_KEYWORDS" => "N",
							// Устанавливать ключевые слова страницы
							"SET_STATUS_404" => "N",
							// Устанавливать статус 404
							"SET_TITLE" => "N",
							// Устанавливать заголовок страницы
							"SHOW_404" => "N",
							// Показ специальной страницы
							"SORT_BY1" => "SORT",
							// Поле для первой сортировки новостей
							"SORT_BY2" => "NAME",
							// Поле для второй сортировки новостей
							"SORT_ORDER1" => "ASC",
							// Направление для первой сортировки новостей
							"SORT_ORDER2" => "ASC",
							// Направление для второй сортировки новостей
							"STRICT_SECTION_CHECK" => "N",
							// Строгая проверка раздела для показа списка
							"COMPONENT_TEMPLATE" => "warning"
						), false); ?>
					</div>
				</div>
				<?$GLOBALS["arrFilter"] = array(
					"PROPERTY_region" => GetSityID(),
				);?>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "regional_tariff_plan", Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_DATE" => "Y",	// Выводить дату элемента
						"DISPLAY_NAME" => "N",	// Выводить название элемента
						"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
						"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"FIELD_CODE" => array(	// Поля
							0 => "",
							1 => "",
						),
						"FILTER_NAME" => "arrFilter",	// Фильтр
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
						"IBLOCK_ID" => "76",	// Код информационного блока
						"IBLOCK_TYPE" => "rko_main",	// Тип информационного блока (используется только для проверки)
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"NEWS_COUNT" => "1",	// Количество новостей на странице
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Новости",	// Название категорий
						"PARENT_SECTION" => "",	// ID раздела
						"PARENT_SECTION_CODE" => "",	// Код раздела
						"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
						"PROPERTY_CODE" => array(	// Свойства
							0 => "blank_verification",
							1 => "copy_verification",
							2 => "tariffs_table",
							3 => "currency_account",
							4 => "external_transfer",
							5 => "currency_control_discount",
							6 => "region",
							7 => "",
						),
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
						"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
						"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
						"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
						"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
					),
					false
				);?>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="block_type to_column c-container">
					<div class="block_type_right">

					</div>
					<div class="block_type_center">
						<? $APPLICATION->IncludeComponent("bitrix:news.list", "warning", array(
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"ADD_SECTIONS_CHAIN" => "N",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000",
							"CACHE_TYPE" => "A",
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"FILTER_NAME" => "",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"IBLOCK_ID" => "62",
							"IBLOCK_TYPE" => "rko",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"INCLUDE_SUBSECTIONS" => "Y",
							"MESSAGE_404" => "",
							"NEWS_COUNT" => "100",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_TITLE" => "Новости",
							"PARENT_SECTION" => "144",
							"PARENT_SECTION_CODE" => "",
							"PREVIEW_TRUNCATE_LEN" => "",
							"PROPERTY_CODE" => array(
								0 => "TYPE_ELEMENT",
								1 => "",
							),
							"SET_BROWSER_TITLE" => "N",
							"SET_LAST_MODIFIED" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_STATUS_404" => "N",
							"SET_TITLE" => "N",
							"SHOW_404" => "N",
							"SORT_BY1" => "SORT",
							"SORT_BY2" => "NAME",
							"SORT_ORDER1" => "ASC",
							"SORT_ORDER2" => "ASC",
							"STRICT_SECTION_CHECK" => "N",
							"COMPONENT_TEMPLATE" => "warning",
							"HIDE_TITLE" => "Y"
						), false); ?>
					</div>
				</div>

			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<? $APPLICATION->IncludeComponent("bitrix:news.list", "step", Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								// Формат показа даты
								"ADD_SECTIONS_CHAIN" => "N",
								// Включать раздел в цепочку навигации
								"AJAX_MODE" => "N",
								// Включить режим AJAX
								"AJAX_OPTION_ADDITIONAL" => "",
								// Дополнительный идентификатор
								"AJAX_OPTION_HISTORY" => "N",
								// Включить эмуляцию навигации браузера
								"AJAX_OPTION_JUMP" => "N",
								// Включить прокрутку к началу компонента
								"AJAX_OPTION_STYLE" => "Y",
								// Включить подгрузку стилей
								"CACHE_FILTER" => "N",
								// Кешировать при установленном фильтре
								"CACHE_GROUPS" => "Y",
								// Учитывать права доступа
								"CACHE_TIME" => "36000",
								// Время кеширования (сек.)
								"CACHE_TYPE" => "A",
								// Тип кеширования
								"CHECK_DATES" => "Y",
								// Показывать только активные на данный момент элементы
								"DETAIL_URL" => "",
								// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
								"DISPLAY_BOTTOM_PAGER" => "N",
								// Выводить под списком
								"DISPLAY_DATE" => "Y",
								// Выводить дату элемента
								"DISPLAY_NAME" => "Y",
								// Выводить название элемента
								"DISPLAY_PICTURE" => "Y",
								// Выводить изображение для анонса
								"DISPLAY_PREVIEW_TEXT" => "Y",
								// Выводить текст анонса
								"DISPLAY_TOP_PAGER" => "N",
								// Выводить над списком
								"FIELD_CODE" => array(    // Поля
									0 => "",
									1 => "",
								),
								"FILTER_NAME" => "",
								// Фильтр
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								// Скрывать ссылку, если нет детального описания
								"IBLOCK_ID" => "62",
								// Код информационного блока
								"IBLOCK_TYPE" => "rko",
								// Тип информационного блока (используется только для проверки)
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								// Включать инфоблок в цепочку навигации
								"INCLUDE_SUBSECTIONS" => "Y",
								// Показывать элементы подразделов раздела
								"MESSAGE_404" => "",
								// Сообщение для показа (по умолчанию из компонента)
								"NEWS_COUNT" => "100",
								// Количество новостей на странице
								"PAGER_BASE_LINK_ENABLE" => "N",
								// Включить обработку ссылок
								"PAGER_DESC_NUMBERING" => "N",
								// Использовать обратную навигацию
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								// Время кеширования страниц для обратной навигации
								"PAGER_SHOW_ALL" => "N",
								// Показывать ссылку "Все"
								"PAGER_SHOW_ALWAYS" => "N",
								// Выводить всегда
								"PAGER_TEMPLATE" => ".default",
								// Шаблон постраничной навигации
								"PAGER_TITLE" => "Новости",
								// Название категорий
								"PARENT_SECTION" => "145",
								// ID раздела
								"PARENT_SECTION_CODE" => "",
								// Код раздела
								"PREVIEW_TRUNCATE_LEN" => "",
								// Максимальная длина анонса для вывода (только для типа текст)
								"PROPERTY_CODE" => array(    // Свойства
									0 => "TYPE_ELEMENT",
									1 => "",
								),
								"SET_BROWSER_TITLE" => "N",
								// Устанавливать заголовок окна браузера
								"SET_LAST_MODIFIED" => "N",
								// Устанавливать в заголовках ответа время модификации страницы
								"SET_META_DESCRIPTION" => "N",
								// Устанавливать описание страницы
								"SET_META_KEYWORDS" => "N",
								// Устанавливать ключевые слова страницы
								"SET_STATUS_404" => "N",
								// Устанавливать статус 404
								"SET_TITLE" => "N",
								// Устанавливать заголовок страницы
								"SHOW_404" => "N",
								// Показ специальной страницы
								"SORT_BY1" => "SORT",
								// Поле для первой сортировки новостей
								"SORT_BY2" => "NAME",
								// Поле для второй сортировки новостей
								"SORT_ORDER1" => "ASC",
								// Направление для первой сортировки новостей
								"SORT_ORDER2" => "ASC",
								// Направление для второй сортировки новостей
								"STRICT_SECTION_CHECK" => "N",
								// Строгая проверка раздела для показа списка
								"COMPONENT_TEMPLATE" => ".default"
							), false); ?>
						</div>
					</div>
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<? $APPLICATION->IncludeComponent("bitrix:news.list", "warning", array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"ADD_SECTIONS_CHAIN" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"CACHE_TIME" => "36000",
								"CACHE_TYPE" => "A",
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_DATE" => "Y",
								"DISPLAY_NAME" => "Y",
								"DISPLAY_PICTURE" => "Y",
								"DISPLAY_PREVIEW_TEXT" => "Y",
								"DISPLAY_TOP_PAGER" => "N",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"FILTER_NAME" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"IBLOCK_ID" => "62",
								"IBLOCK_TYPE" => "rko",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"INCLUDE_SUBSECTIONS" => "Y",
								"MESSAGE_404" => "",
								"NEWS_COUNT" => "100",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Новости",
								"PARENT_SECTION" => "146",
								"PARENT_SECTION_CODE" => "Перейти на базовые тарифы",
								"PREVIEW_TRUNCATE_LEN" => "",
								"PROPERTY_CODE" => array(
									0 => "TYPE_ELEMENT",
									1 => "",
								),
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SORT_BY1" => "SORT",
								"SORT_BY2" => "NAME",
								"SORT_ORDER1" => "ASC",
								"SORT_ORDER2" => "ASC",
								"STRICT_SECTION_CHECK" => "N",
								"COMPONENT_TEMPLATE" => "warning",
								"HIDE_TITLE" => "Y"
							), false); ?>
						</div>
					</div>

				</div>
			</div>
		</div>

                <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/block/rates.php",
                        Array(
                            "template"=>"rates",
                            "all_phone" => "8 (800) 500-66-77",
                            "use_clientbank" => "Y",
                            "show_blanks" => "N",
                        ),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
<div class="wr_block_type">
<div class="block_type to_column c-container">
 <div class="block_type_center">
  <div class="doc_list"> 
    <a href="/media/doc/business/rko/tariffs/packs_conditions_20171225.pdf.pdf" target="_blank" class="doc_item" >
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Условия обслуживания Клиентов в ПАО Банк ЗЕНИТ по Тарифным планам.                                    </div>
                                    <div class="doc_note">
                                        409.15 Кб                                    </div>
                                </div>
    </a>
    <a href="/media/doc/business/rko/tariffs/app_packs_20171225.pdf" target="_blank" class="doc_item" >
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Заявление на обслуживание счета по Тарифному плану.                                     </div>
                                    <div class="doc_note">
                                        310.73 Кб                                    </div>
                                </div>
    </a>
    </div>
   </div>
  </div>
  </div>

	</div>
<?$APPLICATION->IncludeFile("/local/templates/bz/inc/page-clone/online-reservations-account/index.php", Array(), Array(
    "MODE"      => "html",                                           // будет редактировать в веб-редакторе
    "NAME"      => "Редактирование включаемой области раздела",      // текст всплывающей подсказки на иконке
    "TEMPLATE"  => "section_include_template.php"                    // имя шаблона для нового файла
    ));
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>