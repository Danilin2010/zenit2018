<table class="tb">
    <tbody>
    <tr>
        <td rowspan="2" style="vertical-align: middle;">
            Тип операции
        </td>
        <td align="center" colspan="2">
            Время приема документов
        </td>
    </tr>
    <tr style="background: transparent;">
        <td style="font-weight: normal;" align="center">
            по системе Клиент-Банк
        </td>
        <td align="center">
            На бумажных носителях
        </td>
    </tr>
    <tr>
        <td>
            Прием к исполнению текущим днем переводов внутри банка
        </td>
        <td align="center" style="font-weight: normal;">
            до 15:00
        </td>
        <td align="center" style="font-weight: normal;">
            до 13:00
        </td>
    </tr>
    <tr>
        <td>
            Прием к исполнению текущим днем переводов на счета в других кредитных организациях
        </td>
        <td align="center" style="font-weight: normal;">
            до 15:00
        </td>
        <td align="center" style="font-weight: normal;">
            до 13:00
        </td>
    </tr>
    <tr>
        <td>
            Прием к исполнению текущим днем по системе банковских электронных срочных платежей (БЭСП)
        </td>
        <td colspan="2" align="center" style="font-weight: normal;">
            до 15:00
        </td>
    </tr>
    </tbody>
</table>

<?=$arItem["PROPERTIES"]["operation_time"]["~VALUE"]["TEXT"]?>