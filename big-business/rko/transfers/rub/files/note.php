<h2>Примечание</h2>
<ul>
    <li>
        Продолжительность операционного и послеоперационного времени в рабочем дне,
        предшествующем праздничному и/или выходному дню для расчетных документов в Российских рублях, уменьшается на один час
    </li>
    <li>
        К срочному исполнению принимаются расчетные документы с пометкой "срочно"
    </li>
    <li>
        Срочный перевод осуществляется при наличии возможности у банка, достаточности собственных
        денежных средств на счете клиента, а также отсутствии необходимости проведения работниками банка контрольных процедур
        согласно действующему законодательству
    </li>
    <li>
        За срочное исполнение перевода взимается комиссия в соответствии с Тарифами банка
    </li>
</ul>