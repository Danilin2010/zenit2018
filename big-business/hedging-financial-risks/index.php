<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("sky_show", "N");
$APPLICATION->SetTitle("Хеджирование финансовых рисков");
?>

<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
			<h2>Контакты</h2>
			<div class="contacts_block">
				Александр Карпов <a href="mailto:a.karpov@zenit.ru">a.karpov@zenit.ru</a><br>
				+7 (495) 933-07-36 (прямой)
				<div class="note_text">

				</div>
			</div><br>
			<div class="contacts_block">
				Алексей Воробьёв <a href="mailto:a.vorobiev@zenit.ru">a.vorobiev@zenit.ru</a><br>
					+7 (495) 933-07-36 (прямой)
				<div class="note_text">

				</div>	
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p>Банк ЗЕНИТ предлагает застраховать риски финансовых потерь, связанные с неблагоприятным изменением валютного курса для компаний, занимающихся внешнеэкономической деятельностью.</p>
				<p>Банк активно работает с компаниями нефтехимической и металлургической отраслей, компаниями потребительского сектора, сектора торговли, предприятиями легкой промышленности.</p>
				<p>Работа по страхованию рисков финансовых потерь от изменения валютных курсов происходит следующим образом:</p>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 1
									</div>
									<div class="step_body">
										<div class="step_title">Выявление слабых сторон компании.
										</div>
										<div class="step_text">
											 На этом этапе происходит анализ финансовых потоков компании с целью выявления рисков, которые несут опасность финансовых потерь в связи с колебаниями валютных курсов.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 2
									</div>
									<div class="step_body">
										<div class="step_title">Разработка стратегии.
										</div>
										<div class="step_text">
											 Банк разрабатывает для компании оптимальный вариант страхования валютных рисков.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 3
									</div>
									<div class="step_body">
										<div class="step_title">Представление стратегии и консультирование.
										</div>
										<div class="step_text">
											 Банк представляет для компании стратегию и даёт консультации, связанные с налогообложением и учетом.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 4
									</div>
									<div class="step_body">
										<div class="step_title">Оформление сделки и её сопровождение до момента истечения.
										</div>
										<div class="step_text">

										</div>
									</div>
								</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>