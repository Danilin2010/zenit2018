<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Аналитическое сопровождение");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/common-image.jpg'); ?>

	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Ежедневные обзоры</a></li>
				<li><a href="#content-tabs-2">Кредитные комментарии</a></li>
				<li><a href="#content-tabs-3">Актуальные комментарии и публикации в СМИ</a></li>
				<li><a href="#content-tabs-4">Архив аналитики</a></li>
				<li><a href="#content-tabs-5">Контакты</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">

			</div>
			<div class="content_body" id="content-tabs-2">

			</div>
			<div class="content_body" id="content-tabs-3">

			</div>
			<div class="content_body" id="content-tabs-4">

			</div>
			<div class="content_body" id="content-tabs-5">

			</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>