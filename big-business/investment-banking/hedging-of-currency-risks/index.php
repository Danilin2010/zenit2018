<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Хеджирование");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/common-image.jpg'); ?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
				<div class="wr_max_block">
					<h1>
						Контакты
					</h1>
					<div class="form_application_line">
						<div class="contacts_block">
							Басов Алексей<br>
							+7 (495) 937-07-37, доб. 2068
						</div>
						<div class="contacts_block">
							Карпов Александр<br>
							+7(495) 933-07-36 (прямой)
						</div>
						<div class="contacts_block">
							Алексей Воробьев<br>
							Тел. +7(495) 937-09-96 (прямой)
						</div>
						<div class="contacts_block">
							Шабалов Владимир<br>
							Тел. +7(495)937-07-37, доб. 2036
						</div>
					</div>
				</div>
			</div>
			<div class="block_type_center">
				<h1>ПРОФИЛЬ КЛИЕНТА</h1>
				<div class="text_block">
					<p>Риску финансовых потерь от колебаний валютных курсов, процентных ставок и стоимости сырья подвержены:</p>
					<ul class="big_list">
						<li>
							Компании, занимающиеся внешнеэкономической деятельностью:
							<ul>
								<li>Компании с валютной кредиторской задолженностью</li>
								<li>Компании с валютной дебиторской задолженностью</li>
							</ul>
						</li>
						<li>Компании с обязательствами по валютным займам и кредитам</li>
					</ul>
					<h2>СТОИМОСТЬ ХЕДЖИРОВАНИЯ</h2>
					<ul>
						<li>
							Расходы на хеджирование в зависимости от рыночной конъюнктуры могут составлять от 3 до 12%
							в год от величины бюджета покупок/продаж валюты, сырья; в случае со страхованием риска изменения
							процентных ставок – до 0,5% годовых.
						</li>
						<li>Снижение расходов до нуля возможно в случае принятия компанией определенных обязательств;</li>
						<li>Расходы по операциям хеджирования можно относить на себестоимость.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

