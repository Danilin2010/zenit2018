<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Описание и документы");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/mejraschet.png');
?>
	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Описание</a></li>
				<li><a href="#content-tabs-2">Нормативные документы</a></li>
				<li><a href="#content-tabs-3">Оформление сделки</a></li>
				<li><a href="#content-tabs-4">«Клиент-банк» и валютный контроль</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<ul class="big_list">
								<li>
									Осуществляем валютный контроль операций в иностранной валюте и валюте РФ.
								</li>
								<li>
									Контролируем валютные операции резидентов и нерезидентов РФ.
								</li>
								<li>
									Предоставляем профессиональную консультацию специалистов валютного контроля.
								</li>
								<li>
									Оперативно информируем клиентов об изменениях в нормативных правовых актах в области
									валютного контроля, что позволяет избежать нарушений клиентами законодательства РФ.
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<h1>ПРОКОНСУЛЬТИРУЕМ И ОКАЖЕМ ПОМОЩЬ</h1>
							<ul class="big_list">
								<li>
									По вопросам соблюдения требований валютного законодательства и таможенного законодательства.
								</li>
								<li>
									Помощь в подготовке внешнеторговых контрактов в соответствии с требованиями валютного законодательства РФ.
								</li>
								<li>
									Консультирование в области внешнеэкономической деятельности предприятий и организаций.
								</li>
								<li>
									Обеспечение инструктивными материалами по валютному законодательству, в том числе
									с учетом разъяснений органов и агентов валютного контроля (Банк России, ФТС России,
									ФНС России, Росфиннадзор).
								</li>
								<li>
									Помощь при оформлении документов, предусмотренных валютным законодательством РФ.
								</li>
								<li>
									Консультирование по организации оптимального документооборота между Клиентом и Банком.
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
                            <ul class="big_list">
							<li>
								<a href="/upload/docs/currency_control/173_fz.pdf" target="_blank">Федеральный закон </a>
								№ 173-ФЗ от 10.12.2003 «О валютном регулировании и валютном контроле».</li>
							<li>
								<a href="/upload/docs/currency_control/1525_codex.pdf" target="_blank"> Кодекс</a> 
								Российской Федерации об административных правонарушениях от 30.12.2001№ 195-ФЗ (Статья 15.25).
							</li>
							<li>
								Федеральный закон № 164-ФЗ от 08.12.2003 г. «Об основах государственного регулирования внешнеторговой деятельности».
							</li>
							<li>
								<a href="/upload/docs/currency_control/CB_138_i.pdf" target="_blank"> Инструкция Банка России</a> 
								от 04.06.2012 № 138-И «О порядке представления резидентами и нерезидентами уполномоченным
								банкам документов и информации, связанных с проведением валютных операций, порядке
								оформления паспортов сделок, а также порядке учета уполномоченными банками валютных
								операций и контроля за их проведением».
							</li>
							<li>
								<a href="/upload/docs/currency_control/BR_111_i.pdf" target="_blank"> Инструкция Банка России</a>
								 от 30.03.2004 № 111-И «Об обязательной продаже части валютной выручки на внутреннем
								валютном рынке Российской Федерации».
							</li>
							<li>
								<a href="/upload/docs/currency_control/mmvb_457.pdf" target="_blank">Приказ Федеральной
									налоговой службы </a>от 21.09.2010 № ММВ-7-6/457@ «Об утверждении форм уведомлений
								об открытии (закрытии), об изменении реквизитов счета (вклада), расположенном за пределами
								территории Российской Федерации, и о наличии счета в Банке за пределами территории Российской
								Федерации».
							</li>
                            </ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<h2>Для оформления Паспорта сделки резидент представляет в Банк:</h2>
							<ul class="big_list">
								<li>
									Один экземпляр ПС (по форме 1 и (или) по форме 2), заполненный в порядке, приведенном
									в приложении 4 к Инструкции № 138-И;
								</li>
								<li>
									Контракт (кредитный договор), исполнение обязательств по которому требует оформления ПС;
								</li>
								<li>
									Иные документы и информацию, которые содержат сведения (включая сведения, определенные
									(рассчитанные) резидентом самостоятельно), указанные резидентом в заполненной форме ПС;
								</li>
								<li>
									Уведомление об открытии (закрытии) счета (вклада), об изменении реквизитов счета
									(вклада), о наличии счета (вклада) в банке, расположенном за пределами территории
									Российской Федерации, - в случае осуществления валютных операций, связанных с расчетами
									по контракту (кредитному договору), по которому оформляется ПС, через счета резидента,
									открытые в банке-нерезиденте.
								</li>
							</ul>
							<h2>Для переоформления Паспорта сделки (при изменениях сведений) резидент представляет в Банк:</h2>
							<ul class="big_list">
								<li>
									Заявление о переоформлении ПС.
								</li>
								<li>
									Документы и информацию, которые являются основанием внесения изменений в ПС.
								</li>
							</ul>
                            <p></p>
							<p>
								В случае осуществления валютных операций, связанных с зачислением и списанием на/со счета
								резидента валюты РФ или иностранной валюты резидент представляет в Банк Справку о валютных
								операциях в одном экземпляре.
							</p>
							<p>
								При исполнении обязательств по контракту, резидент представляет в Банк подтверждающие
								документы. В случае если по контракту оформлен ПС, подтверждающие документы представляются
								вместе со Справкой о подтверждающих документах в одном экземпляре.
							</p>
						</div>
					</div>
				</div>
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<? $APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"doc",
								array(
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"ADD_SECTIONS_CHAIN" => "N",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"AJAX_OPTION_HISTORY" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"CACHE_TIME" => "36000",
									"CACHE_TYPE" => "N",
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"DISPLAY_DATE" => "Y",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "Y",
									"DISPLAY_TOP_PAGER" => "N",
									"FIELD_CODE" => array(
										0 => "",
										1 => "",
									),
									"FILTER_NAME" => "",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"IBLOCK_ID" => "63",
									"IBLOCK_TYPE" => "rko",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"INCLUDE_SUBSECTIONS" => "Y",
									"MESSAGE_404" => "",
									"NEWS_COUNT" => "100",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_TEMPLATE" => ".default",
									"PAGER_TITLE" => "Новости",
									"PARENT_SECTION" => "108",
									"PARENT_SECTION_CODE" => "Бланки документов",
									"PREVIEW_TRUNCATE_LEN" => "",
									"PROPERTY_CODE" => array(
										0 => "",
										1 => "FILE",
										2 => "",
										3 => "",
									),
									"SET_BROWSER_TITLE" => "N",
									"SET_LAST_MODIFIED" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_STATUS_404" => "N",
									"SET_TITLE" => "N",
									"SHOW_404" => "N",
									"SORT_BY1" => "SORT",
									"SORT_BY2" => "NAME",
									"SORT_ORDER1" => "ASC",
									"SORT_ORDER2" => "ASC",
									"STRICT_SECTION_CHECK" => "N",
									"COMPONENT_TEMPLATE" => "doc"
								),
								false
							); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-4">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<h1>Пользователям системы Клиент-банк предоставляется комплекс дополнительных услуг:</h1>
							<ul class="big_list">
								<li>
									Возможность оформления и направления в Банк стандартизированных форм документов валютного
									контроля, в том числе Заявления на переоформление и закрытие паспорта сделки по системе «Клиент-банк».
								</li>
								<li>
									Электронный документооборот с возможностью отправки в Банк/получения из Банка документов валютного контроля.
								</li>
								<li>
									Оnline-информирование о статусе обработки документов валютного контроля.
								</li>
								<li>
									Системные предупреждения и напоминания о необходимости оформления и сроках направления
									в банк справки о валютных операциях при поступлении иностранной валюты, справки о
									подтверждающих документах на основании поступившей в Банк информации о таможенных декларациях
									из ФТС РФ и пр.
								</li>
								<li>
									Возможность воспользоваться услугой по заполнению за клиента документов валютного контроля
									(согласно тарифам).
								</li>
								<li>
									Возможность получить сведения о состоянии расчетов по всем открытым паспортам сделок
									(представление ведомости банковского контроля согласно тарифам).
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>