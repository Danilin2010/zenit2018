<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Нарушения валютного законодательства");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/mejraschet.png');
?>

	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<p>
					В случае нарушения актов валютного законодательства, Банки обязаны направлять информацию о нарушениях
					клиентами валютного законодательства в территориальное учреждение Банка России для последующей передачи
					органу валютного контроля.
				</p>
				<p>
					Данная информация может быть использована органом валютного контроля для применения к лицам, допустившим
					нарушения, административных наказаний в соответствии со статьей 15.25 «Кодекса Российской Федерации об
					административных правонарушениях».
				</p>
				<p>
					В целях исключения нарушения Вашей организацией валютного законодательства просим Вас соблюдать нормы
					валютного законодательства, а также представлять документы валютного контроля по форме, в порядке и в
					сроки, предусмотренные нормативными документами Банка России.
				</p>
				<p><a href="/upload/docs/currency_control/violations.pdf" target="_blank">Перечень основных нарушений валютного законодательства РФ.</a> </p>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
