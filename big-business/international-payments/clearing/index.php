<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Операции с клиринговыми и национальными валютами");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_rko.png');
?>

	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Операции с клиринговыми валютами</a></li>
				<li><a href="#content-tabs-2">Операции с национальными валютами</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h2>Контакты:</h2>
								<div class="form_application_line">
									<div class="contacts_block">
										Ольга Михайловна Давыдова
										<a href="mailto:o.davydova@zenit.ru">o.davydova@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2025<br>
									</div>
									<div class="note_text">
										Начальник отдела операций с клиринговыми валютами
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Наталия Игоревна Михайлова
										<a href="mailto:natalia.michailova@zenit.ru">natalia.michailova@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2618<br>
									</div>
									<div class="note_text">
										Заместитель начальника Управления международных проектов
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Виктор Павлович Андреев
										<a href="mailto:victor.andreev@zenit.ru">victor.andreev@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2607<br>
									</div>
									<div class="note_text">
										Начальник Управления международных проектов
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<p>
									Банк ЗЕНИТ предоставляет полный комплекс услуг клиентам, заинтересованным в
									использовании клиринговых валют для расчетов по внешнеторговым контрактам со своими
									контрагентами:
								</p>
								<ul>
									<li>
										участие в тендерах Внешэкономбанка по реализации индийский рупий по клиринговым
										расчетам (С45) для клиентов, импортирующих товары из Индии,
									</li>
									<li>
										конверсия в индийских рупиях по клиринговым расчетам (С45),
									</li>
									<li>
										обслуживание расчетов в индийских рупиях по клиринговым расчетам (С45) при импорте
										товаров из Индии.
									</li>
								</ul>
								<p>
									При обслуживании клиентов Банк оказывает услуги по комплексному структурированию и
									сопровождению внешнеэкономической сделки.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h2>Контакты:</h2>
								<div class="form_application_line">
									<div class="contacts_block">
										Виктор Павлович Андреев
										<a href="mailto:victor.andreev@zenit.ru">victor.andreev@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2607<br>
									</div>
									<div class="note_text">
										Начальник Управления международных проектов
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<p>
									Банк ЗЕНИТ предоставляет клиентам возможность открывать текущие счета и вести расчетные
									и конверсионные операции в таких национальных валютах, как китайские юани, индийские
									рупии, южнокорейские воны, вьетнамские донги, белорусские рубли, казахстанские тенге,
									польские злотые, гонконгские и сингапурские доллары. По некоторым из этих валют
									предоставляются также услуги торгового финансирования.
									</p>
								<p>
									Банк ЗЕНИТ консультирует клиентов по вопросам их внешнеторговой деятельности,
									принимает непосредственное участие в разработке внешнеэкономических проектов, оказывает
									клиентам содействие в установлении деловых контактов с официальными и деловыми кругами
									зарубежных стран.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>