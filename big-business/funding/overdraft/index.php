<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Овердрафт");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png');
?>

	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>Овердрафт позволит покрыть разрыв между списанием и поступлением денежных средств на расчетный счет компании и      одновременно оперативно управлять собственной ликвидностью. Источником погашения задолженности по овердрафту является стабильный денежный поток выручки на счета компании.</p>
					<h2>Преимущества</h2>
					<ul>
						<li>Погашение процентов ежедневно</li>
						<li>Процентная ставка ниже уровня действующих ставок по кредитам</li>
						<li>Ежедневное погашение траншей в размере поступившей выручки на счета клиентаы</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
				<div class="wr_max_block">
					<h1>
						Контакты
					</h1>
					<div class="form_application_line">
						<div class="contacts_block">
							Вячеслав Константинович Логин
							<a href="mailto:v.login@zenit.ru">v.login@zenit.ru</a><br>
							+7 (495) 777-57-07, доб. 2833
						</div>
						<div class="note_text">
							Начальник Управления сопровождения и развития клиентских отношений Департамента
							корпоративного бизнеса
						</div>
					</div>
					<div class="form_application_line">
						<div class="contacts_block">
							Алексей Михайлович Баранов
							<a href="mailto:alexey.baranov@zenit.ru">alexey.baranov@zenit.ru</a><br>
							+7 (495) 777-57-07; 937-07-37, доб.3352
						</div>
						<div class="note_text">
							Начальник Отдела развития клиентских отношений Департамента корпоративного бизнеса
						</div>
					</div>
				</div>
			</div>
			<div class="block_type_center">
				<h1>Основные условия</h1>
				<ul class="big_list">
					<li>
						<b>Сумма кредита:</b> не более 50% от среднемесячного оборота по расчетному счету. Максимальный лимит
						по овердрафту рассчитывается исходя не только из текущих оборотов, но и из планируемых. Могут
						учитываться кредитовые обороты по счетам в других банках, если они будут переведены в Банк Зенит.
					</li>
					<li>
						<b>Валюта кредита:</b> рубли.
					</li>
					<li>
						<b>Срок договора:</b> от 1 месяца до года. В зависимости от срока обслуживания в Банке ЗЕНИТ договор
						заключается на 6 или 12 месяцев (минимальный срок обслуживания для открытия овердрафта – 1 месяц).
					</li>
					<li>
						<b>Срок пользования траншем:</b> 30 дней.
					</li>
					<li>
						<b>Погашение процентов:</b> ежедневно.
					</li>
				</ul>
				<div class="note_text">
					В зависимости от особенностей проведения оборотов по счету банк устанавливает фиксированный лимит
					овердрафта на весь срок договора либо плавающий лимит.
				</div>
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>