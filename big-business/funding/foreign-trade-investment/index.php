<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Инвестиционное финансирование внешнеторговых операций");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png');
?>

	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
				<div class="wr_max_block">
					<h1>
						Контакты
					</h1>
					<div class="form_application_line">
						<div class="contacts_block">
							Юлия Викторовна Демонтович
							<a href="mailto:j.demontovich@zenit.ru">j.demontovich@zenit.ru</a><br>
							+7 (495) 937-07-37, доб. 2564
						</div>
						<div class="note_text">
							Заместитель начальника Департамента финансового консалтинга и международных проектов
						</div>
					</div>
					<div class="form_application_line">
						<div class="contacts_block">
							Ирина Алексеевна Милоградова
							<a href="mailto:irina.milogradova@zenit.ru">irina.milogradova@zenit.ru</a><br>
							+7 (495) 937-07-37, доб. 2428
						</div>
						<div class="note_text">
							Начальник отдела структурированных и комиссионных сделок
							Управление торгового финансирования
						</div>
					</div>
				</div>
			</div>
			<div class="block_type_center">
				<div class="text_block">
					<h1>Финансирование импорта под страховое покрытие ЭКА</h1>
					<p>В целях обеспечения значительной экономии средств по сравнению с банковским кредитом и более
						длительных сроков финансирования российских предприятий на приобретение оборудования и/или
						реализацию услуг иностранными контрагентами Банк ЗЕНИТ осуществляет предоставление кредитных
						ресурсов за счет зарубежных источников фондирования под гарантии государственных экспортных
						кредитных агентств (ECA).
					</p>
					<p>
						Преимуществом Банка ЗЕНИТ при осуществлении сделок финансирования со страховым покрытием
						экспортных кредитных агентств является богатый опыт реализации подобных сделок, аккредитация
						Банка в экспортных агентствах стран Европы и Северной Америки, а также заключенные рамочные
						договоры на финансирование с иностранными банками. Стоимость данного вида финансирования
						определяется индивидуально в зависимости от параметров конкретной сделки.
					</p>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

