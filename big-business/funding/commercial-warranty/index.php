<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Банковские гарантии");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png');
?>

	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Описание продукта, преимущества</a></li>
				<li><a href="#content-tabs-2">Условия кредитования</a></li>
				<li><a href="#content-tabs-3">Требования к заемщику</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<div class="text_block">
								<p>
									Банковская гарантия - один из способов обеспечения исполнения обязательств, при котором
									Банк (гарант) выдает по просьбе должника (принципала) письменное обязательство уплатить
									кредитору (бенефициару) денежную сумму при предоставлении им требования об её уплате.
								</p>
								<p>
									Преимущества сотрудничества с Банком ЗЕНИТ:
								</p>
								<ul>
									<li>Срок гарантии – до 3 лет</li>
									<li>
										В качестве частичного обеспечения гарантии может быть принята гарантия АО «Корпорация
										«МСП» / АО «МСП Банк» и поручительство регионального гарантийного фонда по содействию
										кредитованию / развитию малого и среднего предпринимательства / бизнеса»
									</li>
								</ul>
								<p>
									Конечные условия кредитования определяются индивидуально, в зависимости от финансового
									состояния клиента и потребностей бизнеса.
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<h1>Как получить гарантию:</h1>
							<!--step_block-->
							<div class="step_block">
								<div class="step_item">
									<div class="step_num">1</div>
									<div class="step_body">
										<div class="step_title">
											Заполните заявку на сайте Банка и дождитесь звонка менеджера
										</div>
									</div>
								</div>
								<div class="step_item">
									<div class="step_num">2</div>
									<div class="step_body">
										<div class="step_title">
											Пройдите кредитное интервью по телефону или договоритесь о встрече
										</div>
									</div>
								</div>
								<div class="step_item">
									<div class="step_num">3</div>
									<div class="step_body">
										<div class="step_title">
											Подготовьте финансовые и юридические документы в соответствии со списком,
											который составит для Вас менеджер Банка по итогам переговоров
										</div>
									</div>
								</div>
							</div>
							<p>
								После принятия решения на Кредитном Комитете Вас пригласят для открытия расчетного счета и
								подписания кредитно-обеспечительной документации
							</p>
							<!--step_block-->
						</div>
					</div>
				</div>

                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/rates.php",
                    Array(
                        "template"=>"rates",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "Y",
                        "show_blanks" => "N",
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<!--table-->
							<table class="tb min_table">
								<tbody><tr style="width: 1863px;">
									<td>
										Виды предоставляемых банковских гарантий *
									</td>
									<td>
										<ul>
											<li>Платежная гарантия;</li>
											<li>Гарантии в пользу таможенных органов;</li>
											<li>Гарантия возврата авансового платежа;</li>
											<li>Тендерная гарантия;</li>
											<li>Гарантия исполнения обязательств;</li>
											<li>Гарантия исполнения обязательств, установленных  на гарантийный период;</li>
											<li>Гарантии в пользу налоговых органов и Федеральной службы по регулированию алкогольного рынка.</li>
										</ul>
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Стоимость гарантии
									</td>
									<td>
										<ul>
											<li>
												По гарантиям без покрытия - от 3% годовых, определяется индивидуально с
												учетом финансового положения принципала и предоставляемого обеспечения
											</li>
											<li>
												По гарантиям с денежным покрытием – в соответствии
												<a href="https://www.zenit.ru/rus/businesss/operations/index.wbp" target="_blank"> Тарифами</a>
												комиссионного вознаграждения за услуги для юридических лиц и индивидуальных
												предпринимателей ПАО Банк ЗЕНИТ за расчетно-кассовое обслуживание.
											</li>
										</ul>
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Срок действия банковской гарантии
									</td>
									<td>
										до 3-х лет
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Максимальная сумма гарантии:
									</td>
									<td>
										<ul>
											<li>
												до 10 млн. руб. - при поручительстве собственников;
											</li>
											<li>
												до 30 млн. руб. - при залоге товаров в обороте;
											</li>
											<li>до 150 млн. руб. - при залоге движимого и недвижимого имущества,
												а также при гарантиях с покрытием
											</li>
										</ul>
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Комиссия за платеж по банковской гарантии
									</td>
									<td>
										0,2% от суммы платежа по банковской  гарантии
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Неустойка по банковской гарантии
									</td>
									<td>
										0,1% от суммы неисполненного обязательства за каждый день просрочки
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Категория клиентов (принципалов)
									</td>
									<td>
										Юридические лица и индивидуальные предприниматели
									</td>
								</tr>
								</tbody></table>
							<!--table-->
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<!--table-->
							<table class="tb min_table">
								<tbody><tr style="width: 1863px;">
									<td>
										Фактический срок существования бизнеса
									</td>
									<td>
										Не менее 12 месяцев
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Категория заемщиков  / принципалов
									</td>
									<td>
										Юридические лица, индивидуальные предприниматели
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Место регистрации
									</td>
									<td>
										г. Москва, Московская область, г. Санкт-Петербург, Ленинградская область, регионы
										присутствия филиалов/операционных офисов Банка
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Состав учредителей / акционеров
									</td>
									<td>
										Доля участия в уставном капитале государственных, общественных и религиозных
										организаций (объединений), благотворительных и иных фондов – не более 25 %
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Прочие требования
									</td>
									<td>
										Руководители или акционеры данного предприятия ранее не были владельцами или
										руководили предприятиями, признанными банкротами или имевшими длительный период
										неплатежеспособности. Предприятие не имеет существенных налоговых нарушений или
										претензий со стороны государственных и правоохранительных органов.
									</td>
								</tr>
								</tbody></table>
							<!--table-->
							<p>Финансовые результаты<br>
								Деятельность прибыльна (с учетом показателей бухгалтерской отчетности), либо
								Убыточная деятельность оценивается как несущественная (полученные убытки привели к снижению
								чистых активов менее чем на 25% от максимального значения за последние 4 отчетные даты)
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>