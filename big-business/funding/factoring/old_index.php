<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Факторинг");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png'); 
?>
	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Преимущества</a></li>
				<li><a href="#content-tabs-2">Факторинг с правом регресса</a></li>
				<li><a href="#content-tabs-3">Факторинг без права регресса</a></li>
				<li><a href="#content-tabs-4">Реверсивный факторинг</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
<div class="contacts_block">
	<p>Контакты:</p>
	<div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Начальник отдела поддержки продаж
	</div>	
	 Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 2531
</div><br>	
<div class="contacts_block">
	<div class="note_text">
		 Заместитель начальника отдела поддержки продаж
	</div>
	 Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3511
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Руководитель проектов отдела поддержки продаж
	</div>
	 Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3514
</div><br>	
						</div>
						<div class="block_type_center">
							<ul class="big_list">
								<li>
									отсутствие кассовых разрывов – предоставление финансирования в день предоставления отгрузочных документов;
								</li>
								<li>
									финансирование без залога - экономия на расходах по оформлению залога;
								</li>
								<li>долгосрочное сотрудничество – заключение бессрочного договора факторинга;</li>
								<li>увеличение лимитов по мере роста продаж;</li>
								<li>быстрая процедура принятия решения;</li>
								<li>партнерские отношения с торговыми сетями;</li>
								<li>бесперебойное финансирование;</li>
								<li>минимальный пакет документов;</li>
								<li>предварительное рассмотрение в течение 1-3 раб. дней;<li>
								<li>электронный документооборот - финансирование по электронному реестру.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
<div class="contacts_block">
	<p>Контакты:</p>
	<div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Начальник отдела поддержки продаж
	</div>	
	 Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 2531
</div><br>	
<div class="contacts_block">
	<div class="note_text">
		 Заместитель начальника отдела поддержки продаж
	</div>
	 Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3511
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Руководитель проектов отдела поддержки продаж
	</div>
	 Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3514
</div><br>
						</div>
						<div class="block_type_center">
							<h2>Поставщик несет ответственность за исполнение покупателем своих обязательств по оплате поставок </h2>
							<ul class="big_list">
								<li>финансирование до 90% от суммы поставки;</li>
								<li>льготный период ожидания - до 42 календарных дней к отсрочке по контракту; </li>
								<li>информационная поддержка банком - комплексная проверка платежеспособности дебиторов и кредитный анализ;</li>
								<li>Ежедневный мониторинг дебиторской задолженности</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
<div class="contacts_block">
	<p>Контакты:</p>
	<div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Начальник отдела поддержки продаж
	</div>	
	 Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 2531
</div><br>	
<div class="contacts_block">
	<div class="note_text">
		 Заместитель начальника отдела поддержки продаж
	</div>
	 Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3511
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Руководитель проектов отдела поддержки продаж
	</div>
	 Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3514
</div><br>
						</div>
						<div class="block_type_center">
							<h2>Банк берет на себя риск неплатежа покупателя   </h2>
							<ul class="big_list">
								<li>финансирование до 100 % от суммы поставки;</li>
								<li>страхование кредитного риска неоплаты покупателей поставщика;</li>
								<li>информационная поддержка банком - комплексная проверка дебиторов;</li>
								<li>оптимизация структуры баланса;</li>
								<li>безопасное расширение сбытовой сети и географии поставок.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-4">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
<div class="contacts_block">
	<p>Контакты:</p>
	<div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Начальник отдела поддержки продаж
	</div>	
	 Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 2531
</div><br>	
<div class="contacts_block">
	<div class="note_text">
		 Заместитель начальника отдела поддержки продаж
	</div>
	 Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3511
</div><br>
<div class="contacts_block">
	<div class="note_text">
		 Руководитель проектов отдела поддержки продаж
	</div>
	 Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3514
</div><br>
						</div>
						<div class="block_type_center">
							<h2>Факторинг для покупателей, работающих на условиях отсрочки платежа</h2>
							<ul class="big_list">
								<li>финансирование до 100 % от суммы поставки;</li>
								<li>предоставление дополнительной отсрочки до 120 календарных дней;</li>
								<li>возможность получения скидок от поставщиков -  экономия на закупочных ценах.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>