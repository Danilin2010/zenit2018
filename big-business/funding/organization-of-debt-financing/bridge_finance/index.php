<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Бридж-финансирование");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png');
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">
			</div>
            <div class="block_type_center">
                <div class="text_block">
<p>
	 Банк ЗЕНИТ готов предоставить Эмитентам краткосрочное финансирование для:
</p>
<ul>
	<li>пополнения оборотных средств до поступления средств от размещения ценных бумаг; </li>
	<li>выполнения обязательств по выкупу облигаций на вторичном рынке (прохождения Оферты) </li>
	<li>погашения (аммортизации) выпуска облигаций </li>
</ul>
<p>
	 Условия предоставления финансирования подлежат дополнительному согласованию
</p>
<p>
	 Способы предоставления бридж-финансирования:
</p>
<ul>
	<li>Предоставление краткосрочного кредита </li>
	<li><a href="/big-business/funding/organization-of-debt-financing/promissory-notes-program/">Организация выпуска вексельного займа</a></li>
</ul>
                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>