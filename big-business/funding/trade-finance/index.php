<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Торговое финансирование");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png'); 
?>
	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Финансирование импорта</a></li>
				<li><a href="#content-tabs-2">Финансирование экспорта</a></li>
				<li><a href="#content-tabs-3">Финансирование операций на внутреннем рынке</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h1>
									Контакты
								</h1>
								<div class="form_application_line">
									<div class="contacts_block">
										Юлия Викторовна Демонтович 
										<a href="mailto:j.demontovich@zenit.ru">j.demontovich@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2564
									</div>
									<div class="note_text">
										Заместитель начальника Департамента финансового консалтинга и международных проектов
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<p>
									Мы предлагаем нашим Клиентам-импортерам документарное финансирование импортных
									сделок в следующих основных формах:
								</p>
								<ul>
									<li>финансирование с использованием непокрытых документарных аккредитивов, в том числе с отсрочкой платежа и с пост-финансированием;</li>
									<li>финансирование с помощью гарантийных инструментов (гарантии, контргарантии, резервные аккредитивы);</li>
									<li>целевое финансирование импортных контрактов с использованием ресурсов западных банков.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<div class="form_application_line">
									<div class="contacts_block">
										Вячеслав Константинович Логин
										<a href="mailto:v.login@zenit.ru">v.login@zenit.ru</a><br>
										+7 (495) 777-57-07, доб. 2833
									</div>
									<div class="note_text">
										Начальник Управления сопровождения и развития клиентских отношений Департамента
										корпоративного бизнеса
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Алексей Михайлович Баранов
										<a href="mailto:alexey.baranov@zenit.ru">alexey.baranov@zenit.ru</a><br>
										+7 (495) 777-57-07; 937-07-37, доб.3352
									</div>
									<div class="note_text">
										Начальник Отдела развития клиентских отношений Департамента корпоративного бизнеса
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<h1>Основные условия:</h1>
							<ul class="big_list">
								<li>
									<b>Сумма :</b> до 100% от контрактной стоимости
								</li>
								<li>
									<b>Валюта:</b> рубли, доллары, евро, другие валюты (в частности китайские юани).
								</li>
								<li>
									<b>Срок финансирования:</b> до 3-х лет
								</li>
								<li>
									<b>Обеспечение:</b> залог прав требований по контракту, оборудование, недвижимость, сырье,
									залог третьих лиц и бенефициарных владельцев.
								</li>
							</ul>
							<div class="note_text">
								Условия финансирования определяются в зависимости от финансового состояния клиента и структуры сделки.<br>
								Подробнее ознакомиться с основными инструментами, используемыми при осуществлении торгового
								финансирования можно <a href="" target="_blank">здесь</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h1>
									Контакты
								</h1>
								<div class="form_application_line">
									<div class="contacts_block">
										Юлия Викторовна Демонтович 
										<a href="mailto:j.demontovich@zenit.ru">j.demontovich@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2564
									</div>
									<div class="note_text">
										Заместитель начальника Департамента финансового консалтинга и международных проектов
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Вячеслав Константинович Логин
										<a href="mailto:v.login@zenit.ru">v.login@zenit.ru</a><br>
										+7 (495) 777-57-07, доб. 2833
									</div>
									<div class="note_text">
										Начальник Управления сопровождения и развития клиентских отношений Департамента
										корпоративного бизнеса
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Алексей Михайлович Баранов
										<a href="mailto:alexey.baranov@zenit.ru">alexey.baranov@zenit.ru</a><br>
										+7 (495) 777-57-07; 937-07-37, доб.3352
									</div>
									<div class="note_text">
										Начальник Отдела развития клиентских отношений Департамента корпоративного бизнеса
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<p>
									Банк ЗЕНИТ уделяет большое внимание развитию продуктов и услуг в области финансирования
									экспорта. Важным направлением для нас является сотрудничество с клиентами – экспортерами
									товаров, работ, услуг в части оказания помощи в реализации экспортных контрактов на
									максимально благоприятных для наших клиентов условиях.
								</p>
								<p>Мы предлагаем нашим Клиентам-экспортерам :</p>
								<ul>
									<li>
										<a href="/big-business/funding/" target="_blank"> банковские гарантии</a> в обеспечение исполнения
										договорных обязательств поставщика (подрядчика) по экспортным контрактам
										(тендерные, исполнения контракта, возврата аванса, исполнения обязательств
										гарантийного периода)
									</li>
									<li>
										<a href="/big-business/funding/exiar-coverage/" target="_blank">Финансирование
											под страховое покрытие АО ЭКСАР</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h1>
									Контакты
								</h1>
								<div class="form_application_line">
									<div class="contacts_block">
										Юлия Викторовна Демонтович 
										<a href="mailto:j.demontovich@zenit.ru">j.demontovich@zenit.ru</a><br>
										+7 (495) 937-07-37, доб. 2564
									</div>
									<div class="note_text">
										Заместитель начальника Департамента финансового консалтинга и международных проектов
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Вячеслав Константинович Логин
										<a href="mailto:v.login@zenit.ru">v.login@zenit.ru</a><br>
										+7 (495) 777-57-07, доб. 2833
									</div>
									<div class="note_text">
										Начальник Управления сопровождения и развития клиентских отношений Департамента
										корпоративного бизнеса
									</div>
								</div>
								<div class="form_application_line">
									<div class="contacts_block">
										Алексей Михайлович Баранов
										<a href="mailto:alexey.baranov@zenit.ru">alexey.baranov@zenit.ru</a><br>
										+7 (495) 777-57-07; 937-07-37, доб.3352
									</div>
									<div class="note_text">
										Начальник Отдела развития клиентских отношений Департамента корпоративного бизнеса
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<p>
									Банк ЗЕНИТ предлагает использование инструментов торгового финансирования при
									осуществления торговой деятельности на территории РФ.
								</p>
								<ul>
									<li>
										Непокрытые аккредитивы в рублях РФ,  в т.ч.  с отсрочкой платежа по аккредитиву,
										отсрочкой возмещения со стороны клиента-покупателя.
										<p>Основные условия:</p>
										<ul>
											<li>Сумма : до 100% от контрактной стоимости </li>
											<li>Валюта : рубли</li>
											<li>Срок финансирования: до 3-х лет</li>
											<li>
												Обеспечение: залог прав требований по контракту, оборудование, недвижимость,
												сырье, залог третьих лиц и бенефициарных владельцев.
											</li>
										</ul>
										<div class="note_text">
											Условия финансирования определяются в зависимости от финансового состояния
											клиента и структуры сделки.
										</div>
									</li>
									<li>
										<a href="/big-business/funding/" target="_blank">Банковские гарантии  </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>