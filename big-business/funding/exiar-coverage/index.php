<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Финансирование под страховое покрытие ЭКСАР");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_credits.png');
?>
	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Предэкспортное финансирование</a></li>
				<li><a href="#content-tabs-2">Постэкспортное финансирование</a></li>
				<li><a href="#content-tabs-3">Кредит покупателю / банку покупателя (нерезидент)</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h1>
									Контакты
								</h1>
								<div class="form_application_line">
									<div class="contacts_block">
										Константин Николаевич Вильдеев
										<a href="mailto:k.vildeev@zenit.ru ">k.vildeev@zenit.ru </a><br>
										+7 (495) 937-07-37, доб. 3304
									</div>
									<div class="note_text">
										Начальник Управления торгового финансирования
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<h2>Предэкспортное финансирование – предоставление кредита экспортеру для целей исполнения экспортного контракта.</h2>
								<p>
									Основные условия:
								</p>
								<ul>
									<li>объем финансирования – не более 90% от суммы экспортного контракта;</li>
									<li>обязательное обеспечение – залог имущественных прав по экспортному контракту; </li>
									<li>заключение договора страхования с АО «ЭКСАР».</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h1>
									Контакты
								</h1>
								<div class="form_application_line">
									<div class="contacts_block">
										Константин Николаевич Вильдеев
										<a href="mailto:k.vildeev@zenit.ru ">k.vildeev@zenit.ru </a><br>
										+7 (495) 937-07-37, доб. 3304
									</div>
									<div class="note_text">
										Начальник Управления торгового финансирования
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<h2>
									Постэкспортное финансирование – предоставление кредита экспортеру для целей
									рефинансирование понесенных затрат на исполнение экспортного контракта (финансирование
									дебиторской задолженности).
								</h2>
								<p>
									Основные условия:
								</p>
								<ul>
									<li>объем финансирования – не более 90% от суммы дебиторской задолженности;</li>
									<li>обязательное обеспечение – залог имущественных прав по экспортному контракту; </li>
									<li>заключение договора страхования с АО «ЭКСАР».</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<h1>
									Контакты
								</h1>
								<div class="form_application_line">
									<div class="contacts_block">
										Константин Николаевич Вильдеев
										<a href="mailto:k.vildeev@zenit.ru ">k.vildeev@zenit.ru </a><br>
										+7 (495) 937-07-37, доб. 3304
									</div>
									<div class="note_text">
										Начальник Управления торгового финансирования
									</div>
								</div>
							</div>
						</div>
						<div class="block_type_center">
							<div class="text_block">
								<h2>
									Кредит покупателю / банку покупателя (нерезиденту) – финансирования импортера
									(нерезидента) для целей оплаты по экспортному контракту по факту осуществления экспорта.
								</h2>
								<p>
									Основные условия:
								</p>
								<ul>
									<li>наличие экспортного контракта на поставку товаров (работ, услуг) из России;</li>
									<li>заключение договора страхования с АО «ЭКСАР». </li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>