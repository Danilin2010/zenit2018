<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Бивалютный депозит");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_deposits.png');
?>
<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
				<div class="wr_max_block">
					<h2>Контакты</h2>
					<div class="form_application_line">
						<div class="contacts_block">
							Басов Алексей <br>
							+7 (495) 937-07-37, доб. 2068
						</div>
						<br>
						<div class="contacts_block">
							Шабалов Владимир <br>
							+7(495)937-07-37, доб. 2036
						</div>
					</div>
				</div>
			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>Бивалютный депозит — это разновидность банковского депозита с особыми условиями возврата.</p>
					<p>
						Бивалютный депозит закрепляет за Банком право определения валюты возврата вклада по заранее
						согласованному с компанией курсу, при этом комиссия за конвертацию не взимается.
					</p>
					<h2>УСЛОВИЯ РАЗМЕЩЕНИЯ</h2>
					<ul>
						<li>Валюта размещения и возврата – Российский рубль, иностранная валюта;</li>
						<li>Сумма – без ограничений;</li>
						<li>Срок размещения – от 1 дня и до 1095 дней;</li>
						<li>Процентная ставка – определяется в зависимости от условий сделки;</li>
						<li>Возможность пополнения – без права пополнения;</li>
						<li>Частичное снятие – без возможности частичного снятия;</li>
						<li>Досрочный возврат – без права досрочного возврата;</li>
						<li>Выплата процентов – в дату возврата;</li>
						<li>Пролонгация – не предусмотрена.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>