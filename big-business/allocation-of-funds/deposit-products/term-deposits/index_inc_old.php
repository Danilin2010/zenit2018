<h1>Условия размещения</h1>
<ul class="big_list">
	<li>
	Валюта: Российские рубли и иностранная валюта </li>
	<li>
	Сумма депозита не ограничена </li>
	<li>
	Срок размещения от 7 до 1095 дней </li>
	<li>Выплата процентов осуществляется в дату возврата депозита</li>
	<li>Процентная ставка зависит от условий сделки</li>
	<li>Доступные опции депозита:
	<ul>
		<li>пополнение</li>
		<li>частичное снятие средств</li>
		<li>право досрочного возврата средств</li>
	</ul>
 </li>
</ul>