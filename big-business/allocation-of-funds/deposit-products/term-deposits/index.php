<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Срочные депозиты");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_deposits.png');
?>

										<? $APPLICATION->IncludeFile("/local/templates/bz/inc/page-clone/term-deposits/index.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
										);?>

<!--div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
			<p>
 <a href="/big-business/allocation-of-funds/how-to-place-funds-in-the-bank/five-steps-to-income/" style="width:100%" class="button">как разместить средства</a>
			</p>
			<p>
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0" style="width:100%">онлайн заявка</a>
			</p>
		</div>
		<div class="block_type_center">
<?/*$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => ""
	)
);*/?>
		</div>
	</div>
</div-->
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doc",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "doc",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "63",
		"IBLOCK_TYPE" => "rko",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "99",
		"PARENT_SECTION_CODE" => "Типовые формы документов",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"FILE",2=>"",3=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "NAME",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
		</div>
	</div>
</div>

	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<? $APPLICATION->IncludeComponent("bitrix:news.list", "doc", Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					// Формат показа даты
					"ADD_SECTIONS_CHAIN" => "N",
					// Включать раздел в цепочку навигации
					"AJAX_MODE" => "N",
					// Включить режим AJAX
					"AJAX_OPTION_ADDITIONAL" => "",
					// Дополнительный идентификатор
					"AJAX_OPTION_HISTORY" => "N",
					// Включить эмуляцию навигации браузера
					"AJAX_OPTION_JUMP" => "N",
					// Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "Y",
					// Включить подгрузку стилей
					"CACHE_FILTER" => "N",
					// Кешировать при установленном фильтре
					"CACHE_GROUPS" => "Y",
					// Учитывать права доступа
					"CACHE_TIME" => "36000",
					// Время кеширования (сек.)
					"CACHE_TYPE" => "N",
					// Тип кеширования
					"CHECK_DATES" => "Y",
					// Показывать только активные на данный момент элементы
					"DETAIL_URL" => "",
					// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
					"DISPLAY_BOTTOM_PAGER" => "N",
					// Выводить под списком
					"DISPLAY_DATE" => "Y",
					// Выводить дату элемента
					"DISPLAY_NAME" => "Y",
					// Выводить название элемента
					"DISPLAY_PICTURE" => "Y",
					// Выводить изображение для анонса
					"DISPLAY_PREVIEW_TEXT" => "Y",
					// Выводить текст анонса
					"DISPLAY_TOP_PAGER" => "N",
					// Выводить над списком
					"FIELD_CODE" => array(    // Поля
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "",
					// Фильтр
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					// Скрывать ссылку, если нет детального описания
					"IBLOCK_ID" => "63",
					// Код информационного блока
					"IBLOCK_TYPE" => "rko",
					// Тип информационного блока (используется только для проверки)
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					// Включать инфоблок в цепочку навигации
					"INCLUDE_SUBSECTIONS" => "Y",
					// Показывать элементы подразделов раздела
					"MESSAGE_404" => "",
					// Сообщение для показа (по умолчанию из компонента)
					"NEWS_COUNT" => "100",
					// Количество новостей на странице
					"PAGER_BASE_LINK_ENABLE" => "N",
					// Включить обработку ссылок
					"PAGER_DESC_NUMBERING" => "N",
					// Использовать обратную навигацию
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					// Время кеширования страниц для обратной навигации
					"PAGER_SHOW_ALL" => "N",
					// Показывать ссылку "Все"
					"PAGER_SHOW_ALWAYS" => "N",
					// Выводить всегда
					"PAGER_TEMPLATE" => ".default",
					// Шаблон постраничной навигации
					"PAGER_TITLE" => "Новости",
					// Название категорий
					"PARENT_SECTION" => "100",
					// ID раздела
					"PARENT_SECTION_CODE" => "Необходимые документы",
					// Код раздела
					"PREVIEW_TRUNCATE_LEN" => "",
					// Максимальная длина анонса для вывода (только для типа текст)
					"PROPERTY_CODE" => array(    // Свойства
						0 => "",
						1 => "	FILE",
						2 => "",
					),
					"SET_BROWSER_TITLE" => "N",
					// Устанавливать заголовок окна браузера
					"SET_LAST_MODIFIED" => "N",
					// Устанавливать в заголовках ответа время модификации страницы
					"SET_META_DESCRIPTION" => "N",
					// Устанавливать описание страницы
					"SET_META_KEYWORDS" => "N",
					// Устанавливать ключевые слова страницы
					"SET_STATUS_404" => "N",
					// Устанавливать статус 404
					"SET_TITLE" => "N",
					// Устанавливать заголовок страницы
					"SHOW_404" => "N",
					// Показ специальной страницы
					"SORT_BY1" => "SORT",
					// Поле для первой сортировки новостей
					"SORT_BY2" => "NAME",
					// Поле для второй сортировки новостей
					"SORT_ORDER1" => "ASC",
					// Направление для первой сортировки новостей
					"SORT_ORDER2" => "ASC",
					// Направление для второй сортировки новостей
					"STRICT_SECTION_CHECK" => "N",
					// Строгая проверка раздела для показа списка
					"COMPONENT_TEMPLATE" => "doc"
				), false); ?>
			</div>
		</div>
	</div>

<?/*$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/block/rates.php",
    Array(
        "template"=>"rates",
        "all_phone" => "8 (800) 500-66-77",
        "use_clientbank" => "Y",
        "show_blanks" => "N",
    ),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);*/?>
<div class="wr_block_type">
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal_msb", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Заявка на срочный депозит (Крупному бизнесу)",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "20",
		"COMPONENT_TEMPLATE" => "universal_msb",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>