<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Документарные операции");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/big-business/corp_international.png');
?>

	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Документарный аккредитив</a></li>
				<li><a href="#content-tabs-2">Документарное инкассо</a></li>
				<li><a href="#content-tabs-3">Банковские гарантии</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "page",
										"AREA_FILE_SUFFIX" => "inc_tab1",
										"EDIT_TEMPLATE" => ""
									)
								);?>
							</div>
						</div>
						<div class="block_type_center">
							<h1>Банк ЗЕНИТ предлагает клиентам следующие услуги:</h1>
							<ul class="big_list">
								<li>
									открытие импортных аккредитивов, организация подтверждения импортных аккредитивов
									ведущими зарубежными банками (по желанию клиента);
								</li>
								<li>
									авизование, исполнение и подтверждение экспортных аккредитивов, в т. ч. аккредитивов,
									открытых банками стран СНГ;
								</li>
								<li>
									операции с аккредитивами в рублях РФ при расчетах на территории РФ
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "page",
										"AREA_FILE_SUFFIX" => "inc_tab2",
										"EDIT_TEMPLATE" => ""
									)
								);?>
							</div>
						</div>
						<div class="block_type_center">
							<h1>Банк ЗЕНИТ предлагает услуги по:</h1>
							<ul class="big_list">
								<li>
									выставлению документарного инкассо (по экспортным контрактам);
								</li>
								<li>
									выдаче документов по документарному инкассо против платежа или акцепта (по импортным контрактам).
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
							<div class="wr_max_block">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "page",
										"AREA_FILE_SUFFIX" => "inc_tab3",
										"EDIT_TEMPLATE" => ""
									)
								);?>
							</div>
						</div>
						<div class="block_type_center">
							<p>
								Банковские гарантии Банка ЗЕНИТ являются надежным обеспечением обязательств Клиента,
								возникающих перед различными контрагентами в ходе осуществления финансово-хозяйственной
								деятельности. Гарантии широко используются как при операциях клиентов на внутреннем рынке,
								так и при осуществлении внешнеэкономической деятельности.
							</p>
							<p>
								Гарантии Банка ЗЕНИТ принимаются крупными государственными и частными компаниями, банками,
								таможенными, налоговыми органами и могут служить обеспечением самого широкого круга обязательств
								Клиента на территории РФ.
							</p>
							<p>
								Наличие широкой корреспондентской сети позволяет Банку ЗЕНИТ  обеспечивать выпуск гарантий и
								ностранными банками под контр- гарантии Банка ЗЕНИТ.
							</p>
							<p>
								Банком ЗЕНИТ выпускаются такие виды банковских гарантий как:
							</p>
							<ul class="big_list">
								<li>
									в пользу бенефициаров - органов государственной власти РФ (министерств, агентств, служб), в т. ч.:
									<ul>
										<li>таможенных органов (включая гарантии акцизные, генерального обеспечения);</li>
										<li>налоговых органов (включая гарантии «возмещения НДС»);</li>
										<li>органов Федеральной службы по регулированию алкогольного рынка (гарантии для
											получения федеральных специальных марок);</li>
									</ul>
								</li>
								<li>
									используемые для различных целей, в т. ч.:
									<ul>
										<li>для участия в конкурсных процедурах (конкурсные, тендерные); </li>
										<li>для исполнения обязательств по коммерческим (государственным) договорам/контрактам, в т. ч.;</li>
										<ul>
											<li>возврата авансовых платежей;</li>
											<li>платежные;</li>
											<li>на гарантийный период;</li>
											<li>возврата кредита.</li>
										</ul>
									</ul>
								</li>
								<li>
									для целей федеральных законов, в т. ч.:
									<ul>
										<li>44-ФЗ</li>
										<li>223-ФЗ; </li>
										<li>208-ФЗ (обязательное/добровольное предложение по выкупу ценных бумаг);</li>
										<li>214-ФЗ (в пользу участников долевого строительства);</li>
										<li>другие виды банковских гарантий, применяемых в отечественной и международной практике.</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

