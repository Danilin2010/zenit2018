<h2>Контакты</h2>
<div class="form_application_line">
	<div class="contacts_block">
		 Анастасия Анатольевна Шорникова&nbsp;;<br>
 <a href="mailto:a.shornikova@zenit.ru">a.shornikova@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 2332
	</div>
	<div class="note_text">
		 Заместитель начальника Отдела гарантий
	</div>
	<div class="contacts_block">
		 Елена Вагидовна Артемкина&nbsp;<br>
 <a href="mailto:e.artemkina@zenit.ru">e.artemkina@zenit.ru</a><br>
		 +7 (495) 937-07-37, доб. 3148
	</div>
	<div class="note_text">
		 &nbsp;Главный экономист Отдела гарантий
	</div>
</div>
<div class="contacts_block">
	 Владимир Владимирович Богуславский&nbsp;<br>
 <a href="mailto:v.boguslavskiy@zenit.ru">v.boguslavskiy@zenit.ru</a><br>
	 +7 (495) 937-07-37, доб. 2136
</div>
<div class="note_text">
	 &nbsp;&nbsp;Главный экономист Отдела гарантий
</div>