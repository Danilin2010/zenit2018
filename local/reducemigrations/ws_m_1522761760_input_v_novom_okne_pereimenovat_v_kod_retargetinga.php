<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522761760_input_v_novom_okne_pereimenovat_v_kod_retargetinga extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Инпут \"В новом окне\" переименовать в \"Код ретаргетинга\"";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "e5a238f91646a21a579de9835a1a4fd200d43e35";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'carousel';
    }

    static public function newCode() {
        return 'carousel';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "title",
            "button_text",
            "link",
            "new_window",
            "retargeting",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $id=$ar_res["ID"];
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Слайд--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--Название (не отображается)--,".
                        "--ACTIVE_FROM--#--Дата начала активности--,".
                        "--ACTIVE_TO--#--Дата окончания активности--,".
                        "--PROPERTY_title--#--Заголовок--,".
                        "--PREVIEW_TEXT--#--Краткое описание--,".
                        "--PROPERTY_button_text--#--Текст кнопки--,".
                        "--PROPERTY_link--#--Ссылка кнопки--,".
                        "--PROPERTY_new_window--#--Открывать в новом окне--,".
                        "--SORT--#--Порядковый номер--,".
                        "--PREVIEW_PICTURE--#--Изображение--,".
                        "--PROPERTY_retargeting--#--Код ретаргетинга--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('iblock')) {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $id=$ar_res["ID"];
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Слайд--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--Название (не отображается)--,".
                        "--ACTIVE_FROM--#--Дата начала активности--,".
                        "--ACTIVE_TO--#--Дата окончания активности--,".
                        "--PROPERTY_title--#--Заголовок--,".
                        "--PREVIEW_TEXT--#--Краткое описание--,".
                        "--PROPERTY_button_text--#--Текст кнопки--,".
                        "--PROPERTY_link--#--Ссылка кнопки--,".
                        "--PROPERTY_new_window--#--Открывать в новом окне--,".
                        "--SORT--#--Порядковый номер--,".
                        "--PREVIEW_PICTURE--#--Изображение--,".
                        "--PROPERTY_retargeting--#--Открывать в новом окне--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }
}