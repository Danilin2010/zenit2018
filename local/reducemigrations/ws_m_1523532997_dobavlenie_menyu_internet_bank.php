<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1523532997_dobavlenie_menyu_internet_bank extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Добавление меню Интернет банк";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "d4e7ecfdc2535a4e1c385c989eb82110d3b10465";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $menuTypes = GetMenuTypes();
        $menuTypes['internet_banking_menu'] = "Меню Интернет банк";
        SetMenuTypes($menuTypes);
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $menuTypes = GetMenuTypes();
        unset($menuTypes['internet_banking_menu']);
        SetMenuTypes($menuTypes);
    }
}