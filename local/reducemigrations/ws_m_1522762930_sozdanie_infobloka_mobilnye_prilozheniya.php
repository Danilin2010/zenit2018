<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522762930_sozdanie_infobloka_mobilnye_prilozheniya extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание инфоблока \"Мобильные приложения\"";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "3ba5e58cc674c1dcd09969b1f6a5ebcfe7a7270d";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'mobile_applications';
    }

    static public function newCode() {
        return 'mobile_applications';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "link",
            "img",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (Loader::includeModule('iblock')) {
            $builder = new IblockBuilder();
            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if (!$ar_res = $res->Fetch()) {
                $builder->createIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->inRss(false)
                        ->sort(400)
                        ->sections('N')
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Мобильные приложения',
                                ],
                                'en' => [
                                    'NAME' => 'Mobile applications',
                                ],
                            ]
                        );
                });
            }
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if(!$ar_res = $res->Fetch())
            {
                $iblock = $builder->createIblock(self::newCodeType(), 'Банк ЗЕНИТ', function (Iblock $iblock) {
                    $iblock
                        ->siteId(self::newSite())
                        ->code(self::newCode())
                        ->version(2)
                        ->groupId(['2' => 'R']);
                    $iblock->setAttribute('INDEX_ELEMENT', 'N');
                    $iblock->setAttribute('LIST_PAGE_URL', '');
                    $iblock->setAttribute('SECTION_PAGE_URL', '');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '');
                    $iblock
                        ->addProperty('Ссылка')
                        ->code('link')
                        ->required()
                        ->typeString()
                        ->sort(100);

                    $iblock
                        ->addProperty('Картинка')
                        ->code('img')
                        ->required()
                        ->typeFile()
                        ->sort(200)
                    ;

                });
                $id=$iblock->getId();
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Мобильное приложение--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--*Название--,".
                        "--PROPERTY_link--#--Ссылка--,".
                        "--PROPERTY_img--#--Картинка--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                    $Element = new \CIBlockElement;

                    $filePatch="";
                    $filePatch.=$_SERVER["DOCUMENT_ROOT"]."/local/reducemigrations/";
                    $filePatch.=self::hash().'/appstore-app.svg';
                    $file = \CFile::MakeFileArray($filePatch);
                    $arLoadProductArray = Array(
                        "IBLOCK_ID"         => $id,
                        "NAME"              => 'Google Play',
                        "ACTIVE"            => "Y",
                        "SORT"              => "100",
                        'PROPERTY_VALUES' => array(
                            "link"=>"#",
                            "img"=>Array (
                                "n0" => Array(
                                    "VALUE" => $file
                                )
                            ),
                        ),
                    );
                    $Element->Add($arLoadProductArray);

                    $filePatch="";
                    $filePatch.=$_SERVER["DOCUMENT_ROOT"]."/local/reducemigrations/";
                    $filePatch.=self::hash().'/google-play-app.svg';
                    $file = \CFile::MakeFileArray($filePatch);
                    $arLoadProductArray = Array(
                        "IBLOCK_ID"         => $id,
                        "NAME"              => 'AppStore',
                        "ACTIVE"            => "Y",
                        "SORT"              => "200",
                        'PROPERTY_VALUES' => array(
                            "link"=>"#",
                            "img"=>Array (
                                "n0" => Array(
                                    "VALUE" => $file
                                )
                            ),
                        ),
                    );
                    $Element->Add($arLoadProductArray);

                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                \CIBlock::Delete($ar_res["ID"]);
                \CIBlockType::Delete(self::newCodeType());
            }
        }
    }
}