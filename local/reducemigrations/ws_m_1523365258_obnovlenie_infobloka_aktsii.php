<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1523365258_obnovlenie_infobloka_aktsii extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Обновление инфоблока Акции";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "7c83a89281bd0eb9a68a850b020f5c953f25b9e7";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'zenit_actions';
    }

    static public function newCode() {
        return 'zenit_actions';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "link",
            "img_mobile_anons",
            "img_mobile_head",
            "img_anons",
            "img_head",
            "important",
            "title_description",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $id=$ar_res["ID"];
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Элемент--,".
                        "--ACTIVE--#--Активность--,".
                        "--ACTIVE_FROM--#--Начало активности--,".
                        "--ACTIVE_TO--#--Окончание активности--,".
                        "--SORT--#--Сортировка--,".
                        "--NAME--#--Название--,".
                        "--PROPERTY_important--#--Важная акция--,".
                        "--CODE--#--Символьный код--,".
                        "--PREVIEW_TEXT--#--Заголовок для анонса--,".
                        "--TAGS--#--Теги--,".
                        "--edit1_csection1--#----Изображения анонса--,".
                        "--PROPERTY_img_anons--#--Большое изображение (только для широкого блока)--,".
                        "--PROPERTY_img_head--#--Малое изображение (желательно добавлять)--,".
                        "--PROPERTY_title_description--#--Заголовок для описания--,".
                        "--DETAIL_TEXT--#--Описание акции--,".
                        "--PROPERTY_link--#--Открывать ссылку--,".
                        "--XML_ID--#--Внешний код--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $id=$ar_res["ID"];
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Элемент--,".
                        "--ACTIVE--#--Активность--,".
                        "--ACTIVE_FROM--#--Начало активности--,".
                        "--ACTIVE_TO--#--Окончание активности--,".
                        "--SORT--#--Сортировка--,".
                        "--NAME--#--Название--,".
                        "--CODE--#--Символьный код--,".
                        "--PREVIEW_TEXT--#--Заголовок для анонса--,".
                        "--TAGS--#--Теги--,".
                        "--edit1_csection1--#----Изображения анонса--,".
                        "--PROPERTY_img_anons--#--Большое изображение (только для широкого блока)--,".
                        "--PROPERTY_img_head--#--Малое изображение (желательно добавлять)--,".
                        "--PROPERTY_title_description--#--Заголовок для описания--,".
                        "--DETAIL_TEXT--#--Описание акции--,".
                        "--PROPERTY_link--#--Открывать ссылку--,".
                        "--XML_ID--#--Внешний код--,".
                        "--PROPERTY_important--#--Важная акция--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }
}