<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1523359580_obnovlenie_infobloka_aktsii extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Обновление инфоблока Акции";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "9726587ae048eec12a5958b647baa2465a39d476";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'zenit_actions';
    }

    static public function newCode() {
        return 'zenit_actions';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "link",
            "img_mobile_anons",
            "img_mobile_head",
            "img_anons",
            "img_head",
            "important",
            "title_description",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $iblock=$builder->updateIblock($ar_res["ID"], function (Iblock $iblock){
                    $iblock
                        ->addProperty('Заголовок для описания')
                        ->code('title_description')
                        ->sort(700)
                        ->typeString()
                    ;
                    $iblock
                        ->setAttribute('FIELDS', [
                            'CODE' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => array(
                                    'TRANSLITERATION'=>'Y',
                                    'UNIQUE'=>'Y',
                                    'TRANS_LEN'=>'100',
                                    'TRANS_CASE'=>'L',
                                    'TRANS_SPACE'=>'-',
                                    'TRANS_OTHER'=>'-',
                                    'TRANS_EAT'=>'Y',
                                ),
                            ],
                        ]);
                    $iblock->setAttribute('LIST_PAGE_URL', '/media/');
                    $iblock->setAttribute('SECTION_PAGE_URL', '/media/#SECTION_CODE#/');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '/media/#SECTION_CODE#/#ELEMENT_CODE#/');
                });
                $id=$ar_res["ID"];
                if($id>0)
                {
                    $bs = new \CIBlockSection;

                    $arFilter = Array(
                        'IBLOCK_ID'=>$id,
                        'CODE'=>'roznica',
                    );
                    $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
                    while($ar_result = $db_list->GetNext())
                        $bs->Update($ar_result["ID"], array("CODE"=>'retail-actions'));

                    $arFilter = Array(
                        'IBLOCK_ID'=>$id,
                        'CODE'=>'personal',
                    );
                    $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
                    while($ar_result = $db_list->GetNext())
                        $bs->Update($ar_result["ID"], array("CODE"=>'retail-actions'));

                    $arFilter = Array(
                        'IBLOCK_ID'=>$id,
                        'CODE'=>'business',
                    );
                    $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
                    while($ar_result = $db_list->GetNext())
                        $bs->Update($ar_result["ID"], array("CODE"=>'business-actions'));

                    $tabs="<![CDATA[edit1--".
                        "#--Элемент--,".
                        "--ACTIVE--#--Активность--,".
                        "--ACTIVE_FROM--#--Начало активности--,".
                        "--ACTIVE_TO--#--Окончание активности--,".
                        "--SORT--#--Сортировка--,".
                        "--NAME--#--Название--,".
                        "--CODE--#--Символьный код--,".
                        "--PREVIEW_TEXT--#--Заголовок для анонса--,".
                        "--TAGS--#--Теги--,".
                        "--edit1_csection1--#----Изображения анонса--,".
                        "--PROPERTY_img_anons--#--Большое изображение (только для широкого блока)--,".
                        "--PROPERTY_img_head--#--Малое изображение (желательно добавлять)--,".
                        "--PROPERTY_title_description--#--Заголовок для описания--,".
                        "--DETAIL_TEXT--#--Описание акции--,".
                        "--PROPERTY_link--#--Открывать ссылку--,".
                        "--XML_ID--#--Внешний код--,".
                        "--PROPERTY_important--#--Важная акция--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $iblock=$builder->updateIblock($ar_res["ID"], function (Iblock $iblock){
                    $iblock
                        ->deleteProperty('Заголовок для описания')
                    ;
                    $iblock
                        ->setAttribute('FIELDS', [
                            'CODE' => [
                                'IS_REQUIRED' => 'N',
                                'DEFAULT_VALUE' => array(
                                    'TRANSLITERATION'=>'N',
                                ),
                            ],
                        ]);
                    $iblock->setAttribute('LIST_PAGE_URL', '/media/');
                    $iblock->setAttribute('SECTION_PAGE_URL', '/media/actions/');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '/media/actions/#SECTION_CODE#/#ELEMENT_CODE#/');
                });
                $id=$ar_res["ID"];
                if($id>0)
                {
                    $bs = new \CIBlockSection;

                    $arFilter = Array(
                        'IBLOCK_ID'=>$id,
                        'CODE'=>'retail-actions',
                    );
                    $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
                    while($ar_result = $db_list->GetNext())
                        $bs->Update($ar_result["ID"], array("CODE"=>'retail'));

                    $arFilter = Array(
                        'IBLOCK_ID'=>$id,
                        'CODE'=>'business-actions',
                    );
                    $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
                    while($ar_result = $db_list->GetNext())
                        $bs->Update($ar_result["ID"], array("CODE"=>'business'));

                    $tabs="<![CDATA[edit1--".
                        "#--Элемент--,".
                        "--ACTIVE--#--Активность--,".
                        "--ACTIVE_FROM--#--Начало активности--,".
                        "--ACTIVE_TO--#--Окончание активности--,".
                        "--SORT--#--Сортировка--,".
                        "--NAME--#--Название--,".
                        "--CODE--#--Символьный код--,".
                        "--PROPERTY_link--#--Ссылка на внешний ресурс--,".
                        "--TAGS--#--Теги--,".
                        "--edit1_csection1--#----Изображение десктоп--,".
                        "--PROPERTY_img_anons--#--Изображение для анонса--,".
                        "--PROPERTY_img_head--#--Изображение для шапки страницы--,".
                        "--edit1_csection2--#----Изображения адаптив--,".
                        "--PROPERTY_img_mobile_anons--#--Изображение для анонса--,".
                        "--PROPERTY_img_mobile_head--#--Изображение для шапки страницы--,".
                        "--PREVIEW_TEXT--#--Описание для анонса--,".
                        "--XML_ID--#--Внешний код--,".
                        "--PROPERTY_important--#--Важная акция--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }
}