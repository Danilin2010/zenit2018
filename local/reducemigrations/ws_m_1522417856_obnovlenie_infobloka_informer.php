<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522417856_obnovlenie_infobloka_informer extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Обновление инфоблока \"Информер\"";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "eae4d86e61aa15d4977abe3e5769b13bc3959de2";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'informer';
    }

    static public function newCode() {
        return 'informer';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "link",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $builder->updateIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Инфобаннер в хедере',
                                ],
                                'en' => [
                                    'NAME' => 'InfoBanner',
                                ],
                            ]
                        );
                });
            }
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $iblock=$builder->updateIblock($ar_res["ID"], function (Iblock $iblock) {
                    $iblock->setAttribute('NAME', 'Банк ЗЕНИТ');
                });
                $id=$iblock->getId();
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Инфобаннер--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--Название (не отображается)--,".
                        "--ACTIVE_FROM--#--Дата начала активности--,".
                        "--ACTIVE_TO--#--Дата окончания активности--,".
                        "--PREVIEW_TEXT--#--Текст--,".
                        "--PROPERTY_link--#--Ссылка--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $builder = new IblockBuilder();
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $builder->updateIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Инфобаннер',
                                ],
                                'en' => [
                                    'NAME' => 'InfoBanner',
                                ],
                            ]
                        );
                });
            }
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                $iblock=$builder->updateIblock($ar_res["ID"], function (Iblock $iblock) {
                    $iblock->setAttribute('NAME', 'Инфобаннер');
                });
                $id=$iblock->getId();
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Инфобаннер--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--*Название--,".
                        "--ACTIVE_FROM--#--Дата начала активности--,".
                        "--ACTIVE_TO--#--Дата окончания активности--,".
                        "--PROPERTY_link--#--Ссылка--,".
                        "--PREVIEW_TEXT--#--Текст--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                }
            }
        }
    }
}