<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522401397_sozdanie_infobloka_informer extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание инфоблока \"Информер\"";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "5c78e51d39812c9c3f458be3670d2a07935ac3b8";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'informer';
    }

    static public function newCode() {
        return 'informer';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "link",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (Loader::includeModule('iblock')) {
            $builder = new IblockBuilder();
            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if (!$ar_res = $res->Fetch()) {
                $builder->createIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->inRss(false)
                        ->sort(100)
                        ->sections('N')
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Инфобаннер',
                                ],
                                'en' => [
                                    'NAME' => 'InfoBanner',
                                ],
                            ]
                        );
                });
            }
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if(!$ar_res = $res->Fetch())
            {
                $iblock = $builder->createIblock(self::newCodeType(), 'Инфобаннер', function (Iblock $iblock) {
                    $iblock
                        ->siteId(self::newSite())
                        ->code(self::newCode())
                        ->version(2)
                        ->groupId(['2' => 'R']);
                    $iblock->setAttribute('INDEX_ELEMENT', 'N');
                    $iblock->setAttribute('LIST_PAGE_URL', '');
                    $iblock->setAttribute('SECTION_PAGE_URL', '');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '');
                    $iblock
                        ->setAttribute('FIELDS', [
                            'ACTIVE_FROM' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => '=now'
                            ],
                            'ACTIVE_TO' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => '30'
                            ],
                            'PREVIEW_TEXT' => [
                                'IS_REQUIRED' => 'Y',
                            ],
                            'PREVIEW_TEXT_TYPE' => [
                                'DEFAULT_VALUE' => 'html'
                            ],

                        ]);
                    $iblock
                        ->addProperty('Ссылка')
                        ->code('link')
                        ->required()
                        ->typeHtml()
                        ->sort(100);
                });
                $id=$iblock->getId();
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Инфобаннер--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--*Название--,".
                        "--ACTIVE_FROM--#--Дата начала активности--,".
                        "--ACTIVE_TO--#--Дата окончания активности--,".
                        "--PROPERTY_link--#--Ссылка--,".
                        "--PREVIEW_TEXT--#--Текст--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                    $Element = new \CIBlockElement;
                    $arLoadProductArray = Array(
                        "IBLOCK_ID"         => $id,
                        "NAME"              => 'Инфобаннер',
                        "ACTIVE"            => "Y",
                        "ACTIVE_FROM"       => "30.03.2018",
                        "ACTIVE_TO"         => "30.03.2019",
                        "PREVIEW_TEXT"      => "Обратите внимание! 30 января с 22:00 до 23:00 будут проводиться профилактические работы в интернет&#8209;банке. Приносим извинения за возможные неудобства.",
                        "PREVIEW_TEXT_TYPE" => "html",
                        'PROPERTY_VALUES' => array(
                            "link"=>Array("VALUE" => Array ("TEXT" => '<a class="popup-informer__link" href="#"><span>Узнать больше</span></a>', "TYPE" => "html")),
                        ),
                    );
                    $Element->Add($arLoadProductArray);
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                \CIBlock::Delete($ar_res["ID"]);
                \CIBlockType::Delete(self::newCodeType());
            }
        }
    }
}