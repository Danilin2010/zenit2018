<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522670345_sozdanie_infobloka_karusel extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание инфоблока Карусель";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "219387eb05877cbf558a3a28654f2b9cc2fa5a24";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'carousel';
    }

    static public function newCode() {
        return 'carousel';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "title",
            "button_text",
            "link",
            "new_window",
            "retargeting",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (Loader::includeModule('iblock')) {
            $builder = new IblockBuilder();
            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if (!$ar_res = $res->Fetch()) {
                $builder->createIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->inRss(false)
                        ->sort(105)
                        ->sections('N')
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Карусель на главной',
                                ],
                                'en' => [
                                    'NAME' => 'Carousel',
                                ],
                            ]
                        );
                });
            }
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if(!$ar_res = $res->Fetch())
            {
                $iblock = $builder->createIblock(self::newCodeType(), 'Банк ЗЕНИТ', function (Iblock $iblock) {
                    $iblock
                        ->siteId(self::newSite())
                        ->code(self::newCode())
                        ->version(2)
                        ->groupId(['2' => 'R']);
                    $iblock->setAttribute('INDEX_ELEMENT', 'N');
                    $iblock->setAttribute('LIST_PAGE_URL', '');
                    $iblock->setAttribute('SECTION_PAGE_URL', '');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '');
                    $iblock
                        ->setAttribute('FIELDS', [
                            'ACTIVE_FROM' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => '=now'
                            ],
                            'ACTIVE_TO' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => '30'
                            ],
                            'PREVIEW_TEXT' => [
                                'IS_REQUIRED' => 'Y',
                            ],
                            'PREVIEW_TEXT_TYPE' => [
                                'DEFAULT_VALUE' => 'html'
                            ],
                        ]);

                    $iblock
                        ->addProperty('Заголовок')
                        ->code('title')
                        ->required()
                        ->typeHtml()
                        ->sort(100);

                    $iblock
                        ->addProperty('Текст кнопки')
                        ->code('button_text')
                        ->required()
                        ->typeString()
                        ->sort(110);

                    $iblock
                        ->addProperty('Ссылка кнопки')
                        ->code('link')
                        ->required()
                        ->typeString()
                        ->sort(120);

                    $property = $iblock
                        ->addProperty('Открывать в новом окне')
                        ->code('new_window')
                        ->typeDropdown()
                        ->sort(130)
                        ->listType('C')
                    ;
                    $property->addEnum('Y')->xmlId('newWindowY');

                    $iblock
                        ->addProperty('Код ретаргетинга')
                        ->code('retargeting')
                        ->typeString()
                        ->sort(140);

                });
                $id=$iblock->getId();
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Слайд--,".
                        "--ID--#--ID--,".
                        "--ACTIVE--#--Активность--,".
                        "--NAME--#--Название (не отображается)--,".
                        "--ACTIVE_FROM--#--Дата начала активности--,".
                        "--ACTIVE_TO--#--Дата окончания активности--,".
                        "--PROPERTY_title--#--Заголовок--,".
                        "--PREVIEW_TEXT--#--Краткое описание--,".
                        "--PROPERTY_button_text--#--Текст кнопки--,".
                        "--PROPERTY_link--#--Ссылка кнопки--,".
                        "--PROPERTY_new_window--#--Открывать в новом окне--,".
                        "--SORT--#--Порядковый номер--,".
                        "--PREVIEW_PICTURE--#--Изображение--,".
                        "--PROPERTY_retargeting--#--Открывать в новом окне--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);
                    $Element = new \CIBlockElement;
                    $new_window="";
                    $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$id, "PROPERTY_CODE"=>"new_window"));
                    while($enum_fields = $property_enums->GetNext())
                        $new_window=$enum_fields["ID"];
                    $filePatch="";
                    $filePatch.=$_SERVER["DOCUMENT_ROOT"]."/local/reducemigrations/";
                    $filePatch.=self::hash().'/slide-img-1.png';
                    $file = \CFile::MakeFileArray($filePatch);
                    $arLoadProductArray = Array(
                        "IBLOCK_ID"         => $id,
                        "SORT"              => '100',
                        "NAME"              => 'Как накопить на квартиру',
                        "ACTIVE"            => "Y",
                        "ACTIVE_FROM"       => "30.03.2018",
                        "ACTIVE_TO"         => "30.03.2019",
                        "PREVIEW_TEXT"      => "Получите кредит наличными до&nbsp;1&nbsp;000&nbsp;000&nbsp;₽",
                        "PREVIEW_TEXT_TYPE" => "html",
                        "PREVIEW_PICTURE"=>$file,
                        'PROPERTY_VALUES' => array(
                            "title"=>Array("VALUE" => Array (
                                "TEXT" => 'Как накопить на квартиру',
                                "TYPE" => "html",
                            )),
                            "button_text"=>'Узнать больше',
                            "link"=>'#',
                            "retargeting"=>'',
                            "new_window"=>Array("VALUE" => $new_window),
                        ),
                    );
                    $Element->Add($arLoadProductArray);
                    $Element->Add($arLoadProductArray);
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('iblock'))
        {
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if($ar_res = $res->Fetch())
            {
                \CIBlock::Delete($ar_res["ID"]);
                \CIBlockType::Delete(self::newCodeType());
            }
        }
    }
}