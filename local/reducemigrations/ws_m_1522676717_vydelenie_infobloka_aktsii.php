<?php
use WS\ReduceMigrations\Builder\Entity\Iblock;
use WS\ReduceMigrations\Builder\IblockBuilder;
use WS\ReduceMigrations\Builder\Entity\IblockType;
use \Bitrix\Main\Loader;
/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522676717_vydelenie_infobloka_aktsii extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Выделение инфоблока Акции";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "204148d2da0a3af5108b8d8053a4e536fc353e65";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    static public function newCodeType() {
        return 'zenit_actions';
    }

    static public function newCode() {
        return 'zenit_actions';
    }

    static public function newCodeTypeOld() {
        return 'news';
    }

    static public function newCodeOld() {
        return 'actions';
    }

    static public function newCodeNewsOld() {
        return 'news';
    }

    static public function newSite() {
        return 's1';
    }

    public function getListProp($id) {
        $arr=array(
            "link",
            "img_mobile_anons",
            "img_mobile_head",
            "img_anons",
            "img_head",
        );
        $res=array();

        foreach ($arr as $val)
        {
            $arFilter = array(
                'IBLOCK_ID' => $id,
                'CODE' => $val,
            );
            $rsProperty = \CIBlockProperty::GetList(
                array(),
                $arFilter
            );
            if($element = $rsProperty->Fetch())
                $res[$element["CODE"]]=$element["ID"];
        }
        return $res;
    }

    /**
     * Перенос элементов
     **/
    public function Transferring($oldID,$newID)
    {
        $bs = new \CIBlockSection;
        $el = new \CIBlockElement;
        $sections=array();
        $arFilter = Array('IBLOCK_ID'=>$oldID);
        $db_list = CIBlockSection::GetList(Array(), $arFilter, false);
        while($ar_result = $db_list->GetNext())
        {
            $section=array(
                "NAME"=>$ar_result["NAME"],
                "CODE"=>$ar_result["CODE"],
                "IBLOCK_ID" => $newID,
            );

            if($section["CODE"]=="msb")
                $section["NAME"]="Бизнес";

            $ID = $bs->Add($section);
            if($ID>0)
            {
                $section["NEW_ID"]=$ID;
                $sections[$ar_result["ID"]]=$section;
            }
        }
        $arFilter = array(
            "IBLOCK_ID" => $oldID,
        );
        $arSelect = Array(
            "ID",
            "IBLOCK_ID",
            "IBLOCK_TYPE",
            "NAME",
            "DATE_ACTIVE_FROM",
            "ACTIVE",
            "DATE_ACTIVE_TO",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "IBLOCK_SECTION_ID",
            "CODE",
            );
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNext())
        {
            $element=array(
                "NAME"=>$ob["NAME"],
                "CODE"=>$ob["CODE"],
                "DATE_ACTIVE_FROM"=>$ob["DATE_ACTIVE_FROM"],
                "DATE_ACTIVE_TO"=>$ob["DATE_ACTIVE_TO"],
                "ACTIVE"=>$ob["ACTIVE"],
                "PREVIEW_TEXT"=>$ob["PREVIEW_TEXT"],
                "PREVIEW_TEXT_TYPE"=>$ob["PREVIEW_TEXT_TYPE"],
                "IBLOCK_ID" => $newID,
            );
            if($ob["IBLOCK_SECTION_ID"])
                $element["IBLOCK_SECTION_ID"]=$sections[$ob["IBLOCK_SECTION_ID"]]["NEW_ID"];
            if(!$element["DATE_ACTIVE_FROM"])
                $element["DATE_ACTIVE_FROM"]="18.04.2017";
            $el->Add($element);
        }
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        if (Loader::includeModule('iblock')) {
            $builder = new IblockBuilder();
            $res = \CIBlockType::GetList(
                Array(),
                Array(
                    "ID" => self::newCodeType()
                )
            );
            if (!$ar_res = $res->Fetch()) {
                $builder->createIblockType(self::newCodeType(), function (IblockType $type) {
                    $type
                        ->inRss(false)
                        ->sort(110)
                        ->sections('Y')
                        ->lang(
                            [
                                'ru' => [
                                    'NAME' => 'Акции',
                                ],
                                'en' => [
                                    'NAME' => 'Actions',
                                ],
                            ]
                        );
                });
            }
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode()
                )
            );
            if(!$ar_res = $res->Fetch())
            {
                $iblock = $builder->createIblock(self::newCodeType(), 'Банк ЗЕНИТ', function (Iblock $iblock) {
                    $iblock
                        ->siteId(self::newSite())
                        ->code(self::newCode())
                        ->version(2)
                        ->groupId(['2' => 'R']);
                    $iblock->setAttribute('INDEX_ELEMENT', 'N');
                    $iblock->setAttribute('LIST_PAGE_URL', '');
                    $iblock->setAttribute('SECTION_PAGE_URL', '/media/actions/');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '/media/actions/#SECTION_CODE#/');
                    $iblock->setAttribute('DETAIL_PAGE_URL', '/media/actions/#SECTION_CODE#/#ELEMENT_CODE#/');
                    $iblock
                        ->setAttribute('FIELDS', [
                            'ACTIVE_FROM' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => '=now'
                            ],
                            'SECTION_CODE' => [
                                'IS_REQUIRED' => 'Y',
                                'DEFAULT_VALUE' => array(
                                    'TRANSLITERATION'=>'Y',
                                    'TRANS_LEN'=>'100',
                                    'TRANS_CASE'=>'L',
                                    'TRANS_SPACE'=>'-',
                                    'TRANS_OTHER'=>'-',
                                    'TRANS_EAT'=>'Y',
                                ),
                            ],
                        ]);
                    $iblock
                        ->addProperty('Ссылка на внешний ресурс')
                        ->code('link')
                        ->typeString()
                        ->sort(10)
                    ;
                    $iblock
                        ->addProperty('Изображение для анонса.')
                        ->code('img_mobile_anons')
                        ->typeFile()
                        ->fileType('jpg, gif, bmp, png, jpeg')
                        ->sort(500)
                    ;
                    $iblock
                        ->addProperty('Изображение для шапки.')
                        ->code('img_mobile_head')
                        ->typeFile()
                        ->sort(500)
                    ;
                    $iblock
                        ->addProperty('Изображение для анонса')
                        ->code('img_anons')
                        ->typeFile()
                        ->fileType('jpg, gif, bmp, png, jpeg')
                        ->sort(500)
                    ;
                    $iblock
                        ->addProperty('Изображение для шапки')
                        ->code('img_head')
                        ->typeFile()
                        ->fileType('jpg, gif, bmp, png, jpeg')
                        ->sort(500)
                    ;
                });
                $id=$iblock->getId();
                if($id>0)
                {
                    $tabs="<![CDATA[edit1--".
                        "#--Элемент--,".
                        "--ACTIVE--#--Активность--,".
                        "--ACTIVE_FROM--#--Начало активности--,".
                        "--ACTIVE_TO--#--Окончание активности--,".
                        "--SORT--#--Сортировка--,".
                        "--NAME--#--Название--,".
                        "--TAGS--#--Теги--,".
                        "--edit1_csection1--#----Изображение десктоп--,".
                        "--PROPERTY_img_anons--#--Изображение для анонса--,".
                        "--PROPERTY_img_head--#--Изображение для шапки страницы--,".
                        "--edit1_csection2--#----Изображения адаптив--,".
                        "--PROPERTY_img_mobile_anons--#--Изображение для анонса--,".
                        "--PROPERTY_img_mobile_head--#--Изображение для шапки страницы--,".
                        "--PREVIEW_TEXT--#--Описание для анонса--,".
                        "--XML_ID--#--Внешний код--".
                        ";--]]>";
                    foreach (self::getListProp($id) as $old => $new)
                        $tabs = str_replace('--PROPERTY_'.$old.'--', '--PROPERTY_'.$new.'--', $tabs);
                    $arOptions = array(array(
                        'd' => 'Y',
                        'c' => 'form',
                        'n' => 'form_element_'.$id,
                        'v' => array('tabs' => $tabs)
                    ));
                    \CUserOptions::SetOptionsFromArray($arOptions);

                    $resOld = \CIBlock::GetList(
                        Array(),
                        Array(
                            'TYPE'=>self::newCodeTypeOld(),
                            'SITE_ID'=>self::newSite(),
                            "CODE"=>self::newCodeOld(),
                        )
                    );
                    if($ar_resOld = $resOld->Fetch())
                    {
                        self::Transferring($ar_resOld["ID"],$id);
                        $builder->updateIblockType(self::newCodeTypeOld(), function (IblockType $type) {
                            $type
                                ->sort(120)
                            ;
                        });
                        $iblock=$builder->updateIblock($ar_resOld["ID"], function (Iblock $iblock) {
                            $iblock->setAttribute('ACTIVE', 'N');
                            $iblock->setAttribute('NAME', 'Акции (Будет удален)');
                        });


                        $resOld = \CIBlock::GetList(
                            Array(),
                            Array(
                                'TYPE'=>self::newCodeTypeOld(),
                                'SITE_ID'=>self::newSite(),
                                "CODE"=>self::newCodeNewsOld(),
                            )
                        );

                        if($ar_resOld = $resOld->Fetch())
                        {
                            $iblock=$builder->updateIblock($ar_resOld["ID"], function (Iblock $iblock) {
                                $iblock->setAttribute('NAME', 'Банк ЗЕНИТ');
                            });
                        }

                    }
                }
            }
        }
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        if (Loader::includeModule('iblock'))
        {
            $builder = new IblockBuilder();
            $res = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeType(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCode(),
                )
            );
            if($ar_res = $res->Fetch())
            {
                \CIBlock::Delete($ar_res["ID"]);
                \CIBlockType::Delete(self::newCodeType());
            }

            $builder->updateIblockType(self::newCodeTypeOld(), function (IblockType $type) {
                $type
                    ->sort(500)
                ;
            });

            $resOld = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeTypeOld(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCodeOld(),
                )
            );

            if($ar_resOld = $resOld->Fetch())
            {
                $iblock=$builder->updateIblock($ar_resOld["ID"], function (Iblock $iblock) {
                    $iblock->setAttribute('ACTIVE', 'Y');
                    $iblock->setAttribute('NAME', 'Акции');
                });
            }

            $resOld = \CIBlock::GetList(
                Array(),
                Array(
                    'TYPE'=>self::newCodeTypeOld(),
                    'SITE_ID'=>self::newSite(),
                    "CODE"=>self::newCodeNewsOld(),
                )
            );

            if($ar_resOld = $resOld->Fetch())
            {
                $iblock=$builder->updateIblock($ar_resOld["ID"], function (Iblock $iblock) {
                    $iblock->setAttribute('NAME', 'Новости');
                });
            }

        }
    }
}