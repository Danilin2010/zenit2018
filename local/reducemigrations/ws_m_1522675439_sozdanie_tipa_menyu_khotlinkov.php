<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522675439_sozdanie_tipa_menyu_khotlinkov extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Создание типа меню хотлинков";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "e1a82c15cd5ad1f40f5b9574b51be876a00793e1";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $menuTypes = GetMenuTypes();
        $menuTypes['hotlinks_menu'] = "Меню хотлинков 2018";
        SetMenuTypes($menuTypes);
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $menuTypes = GetMenuTypes();
        unset($menuTypes['hotlinks_menu']);
        SetMenuTypes($menuTypes);
    }
}