<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522740684_izmenenie_poryadka_tipov_menyu extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Изменение порядка типов меню";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "7fa11cffd4cd17f4283abc388ad4caba37a0620e";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $menuTypes = GetMenuTypes();
        unset($menuTypes['client_type_menu']);
        unset($menuTypes['main_menu']);
        unset($menuTypes['footer_links']);
        unset($menuTypes['hotlinks_menu']);
        $menuTypes['client_type_menu'] = "Хедер 2018";
        $menuTypes['main_menu'] = "Продуктовое меню 2018";
        $menuTypes['hotlinks_menu'] = "Хотлинки 2018";
        $menuTypes['footer_links'] = "Подвал 2018";
        SetMenuTypes($menuTypes);
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $menuTypes = GetMenuTypes();
        unset($menuTypes['client_type_menu']);
        unset($menuTypes['main_menu']);
        unset($menuTypes['footer_links']);
        unset($menuTypes['hotlinks_menu']);
        $menuTypes['client_type_menu'] = "Тип клиента 2018";
        $menuTypes['main_menu'] = "Главное 2018";
        $menuTypes['footer_links'] = "Подвал 2018";
        $menuTypes['hotlinks_menu'] = "Меню хотлинков 2018";
        SetMenuTypes($menuTypes);
    }
}