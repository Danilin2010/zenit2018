<?php

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1522661298_dobavlenie_novykh_tipov_menyu extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Добавление новых типов меню";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "e00e70116260edbe839e412b57e8a92e35c3346f";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
        $menuTypes = GetMenuTypes();
        $menuTypes['client_type_menu'] = "Тип клиента 2018";
        $menuTypes['main_menu'] = "Главное 2018";
        $menuTypes['footer_links'] = "Подвал 2018";
        SetMenuTypes($menuTypes);
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
        $menuTypes = GetMenuTypes();
        unset($menuTypes['client_type_menu']);
        unset($menuTypes['main_menu']);
        unset($menuTypes['footer_links']);
        SetMenuTypes($menuTypes);
    }
}