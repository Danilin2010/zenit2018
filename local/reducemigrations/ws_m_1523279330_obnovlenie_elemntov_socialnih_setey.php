<?php

use Bitrix\Main\Loader;

/**
 * Class definition update migrations scenario actions
 **/
class ws_m_1523279330_obnovlenie_elemntov_socialnih_setey extends \WS\ReduceMigrations\Scenario\ScriptScenario {

    /**
     * Name of scenario
     **/
    static public function name() {
        return "Обновление элементов Социальных сетей";
    }

    /**
     * Priority of scenario
     **/
    static public function priority() {
        return self::PRIORITY_HIGH;
    }

    /**
     * @return string hash
     */
    static public function hash() {
        return "146fe0aff1afdfc6ad0a07fdd016cbcefebeeb54";
    }

    /**
     * @return int approximately time in seconds
     */
    static public function approximatelyTime() {
        return 1;
    }


	static public function oldData() {
		return array(
			array(
				'name' => 'vk',
				'active' => 'Y',
				'sort' => '10',
				'external_id' => 'vk',
				'link' => 'https://vk.com/bank_zenit',
				'class' => 'vk'
			),
			array(
				'name' => 'f',
				'active' => 'Y',
				'sort' => '20',
				'external_id' => 'f',
				'link' => 'https://www.facebook.com/BankZENIT/',
				'class' => 'f'
			),
			array(
				'name' => 'telegram',
				'active' => 'Y',
				'sort' => '30',
				'external_id' => 'telegram',
				'link' => '#',
				'class' => 'telegram'
			),
			array(
				'name' => 'tw',
				'active' => 'Y',
				'sort' => '40',
				'external_id' => 'tw',
				'link' => 'https://twitter.com/bank_zenit',
				'class' => 'tw'
			),
			array(
				'name' => 'i',
				'active' => 'Y',
				'sort' => '60',
				'external_id' => 'i',
				'link' => '#',
				'class' => 'i'
			),
			array(
				'name' => 'hh',
				'active' => 'n',
				'sort' => '70',
				'external_id' => 'hh',
				'link' => '#',
				'class' => 'hh'
			)
		);
	}


	static public function newData() {
		return array(
			array(
				'name' => 'Вконтакте',
				'active' => 'Y',
				'sort' => '10',
				'external_id' => 'vk',
				'link' => 'https://vk.com/bank_zenit',
				'class' => 'icon__vkontakte-symbol'
			),
			array(
				'name' => 'Фейсбук',
				'active' => 'Y',
				'sort' => '20',
				'external_id' => 'f',
				'link' => 'https://www.facebook.com/BankZENIT/',
				'class' => 'icon__facebook-symbol'
			),
			array(
				'name' => 'Телеграм',
				'active' => 'Y',
				'sort' => '30',
				'external_id' => 'telegram',
				'link' => '#',
				'class' => 'icon__telegram-symbol'
			),
			array(
				'name' => 'Твиттер',
				'active' => 'Y',
				'sort' => '40',
				'external_id' => 'tw',
				'link' => 'https://twitter.com/bank_zenit',
				'class' => 'icon__twitter-symbol'
			),
			array(
				'name' => 'Одноклассники',
				'active' => 'Y',
				'sort' => '50',
				'external_id' => 'ok',
				'link' => '#',
				'class' => 'icon__odnoklassniki-symbol'
			),
			array(
				'name' => 'Инстаграм',
				'active' => 'Y',
				'sort' => '60',
				'external_id' => 'i',
				'link' => '#',
				'class' => 'icon__instagram-symbol'
			)
		);
	}

	const IBLOCK_ID = 5;

    /**
     * Write action by apply scenario. Use method `setData` for save need rollback data
     **/
    public function commit() {
        // my code
		if (Loader::includeModule('iblock'))
		{
			$arFilter = Array(
				'IBLOCK_ID' => static::IBLOCK_ID
			);

			$CIBlockElement = \CIBlockElement::GetList(array(), $arFilter);
			while ($arFields = $CIBlockElement->GetNext())
			{
				\CIBlockElement::Delete($arFields['ID']);
			}

			$newData = static::newData();

			$arLoadProduct = array(
				'IBLOCK_ID' => static::IBLOCK_ID
			);

			foreach ($newData as $item)
			{
				$Element = new \CIBlockElement;

				$arLoadProduct['ACTIVE'] = $item['active'];
				$arLoadProduct['NAME'] = $item['name'];
				$arLoadProduct['SORT'] = $item['sort'];
				$arLoadProduct['EXTERNAL_ID'] = $item['external_id'];
				$arLoadProduct['PROPERTY_VALUES'] = array(
					'LINK' => $item['link'],
					'CLASS' => $item['class']
				);

				$Element->Add($arLoadProduct);
			}
		}
    }

    /**
     * Write action by rollback scenario. Use method `getData` for getting commit saved data
     **/
    public function rollback() {
        // my code
		if (Loader::includeModule('iblock'))
		{
			$arFilter = Array(
				'IBLOCK_ID' => static::IBLOCK_ID
			);

			$CIBlockElement = \CIBlockElement::GetList(array(), $arFilter);
			while ($arFields = $CIBlockElement->GetNext())
			{
				\CIBlockElement::Delete($arFields['ID']);
			}

			$newData = static::oldData();

			$arLoadProduct = array(
				'IBLOCK_ID' => static::IBLOCK_ID
			);

			foreach ($newData as $item)
			{
				$Element = new \CIBlockElement;

				$arLoadProduct['ACTIVE'] = $item['active'];
				$arLoadProduct['NAME'] = $item['name'];
				$arLoadProduct['SORT'] = $item['sort'];
				$arLoadProduct['EXTERNAL_ID'] = $item['external_id'];
				$arLoadProduct['PROPERTY_VALUES'] = array(
					'LINK' => $item['link'],
					'CLASS' => $item['class']
				);

				$Element->Add($arLoadProduct);
			}
		}
    }
}