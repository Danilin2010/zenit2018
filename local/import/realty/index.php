<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once ('ImportRealty.php');
use Aic\realty\ImportRealty;

$config = [
    'iblockId' => '149',
    'filename' => 'data.csv',
    'delimiter' => ';',
];

(new ImportRealty($config))->run();




