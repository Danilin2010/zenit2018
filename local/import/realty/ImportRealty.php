<?php
namespace Aic\realty;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");

class ImportRealty
{

    protected $iblockId = 0;
    protected $iblockSityId = 1;
    protected $filename = 'data.csv';
    protected $delimiter = ';';

    private $dataCity = [];
    private $errors = [];
    private $property = [];

    public function __construct($config)
    {
        \Bitrix\Main\Loader::includeModule('iblock');

        if (array_key_exists('iblockId', $config)) {
            $this->iblockId = (int)$config['iblockId'];
        }

        if (array_key_exists('filename', $config)) {
            $this->filename = $config['filename'];
        }

        if (array_key_exists('delimiter', $config)) {
            $this->delimiter = $config['delimiter'];
        }

        //подтянем параметры мнодественных свойств
        $this->property['classic'] = \CIBlockPropertyEnum::GetList([], ["IBLOCK_ID"=>$this->iblockId, "CODE"=>"classic", 'XML_ID' => 'yes'])->Fetch();
        $this->property['vi'] = \CIBlockPropertyEnum::GetList([], ["IBLOCK_ID"=>$this->iblockId, "CODE"=>"vi", 'XML_ID' => 'yes'])->Fetch();

        $this->dataCity = $this->getCity();
    }


    public function run()
    {
        try {
            if ($this->iblockId <= 0) {
                throw new \Exception('Идентификатор инфоблока не указан или не верный', 400);
                die;
            }

            if ($this->checkEmptyIblock() === false) {
                throw new \Exception('Инфоблок не пустой', 400);
                die;
            }

            if (!file_exists($this->getDataPath())) {
                throw new \Exception('Файл импорта не найден', 400);
                die;
            }

            $this->importCsv();


        } catch (\Exception $e) {
            echo 'Ошибка: ' . $e->getMessage() . ', строка:' . $e->getLine();
        }
    }

    public function getDataPath()
    {
        return dirname(__FILE__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $this->filename;
    }


    /**
     * Чтение файлп CSV
     */
    public function importCsv()
    {
        $csvFile = new \CCSVData('R', true);
        $csvFile->LoadFile($this->getDataPath());
        $csvFile->SetDelimiter($this->delimiter);

        $i = 1;
        while ($arRes = $csvFile->Fetch()) {
            $this->addElement($arRes, $i);
        }
    }


    /** Добавление элемента в инфоблок
     * @param $params
     * @param $cursor
     * @return bool
     */
    public function addElement($params, $cursor)
    {


        $city = $this->cityAlias($params[4]);


        if (array_key_exists($city, $this->dataCity)) {
            $cityId = $this->dataCity[$city]['id'];
        } else {
            $cityId = false;
            $this->errors[] = 'Не найден город в справочнике (строка в csv:' . $cursor . ')';
        }

        $el = new \CIBlockElement;

        $PROP = [
            'gk' => $params[0],
            'uk' => $params[1],
            'region' => $params[3],
            'city' => ['VALUE' =>$cityId],
            'address' => $params[5],
            'coord' =>  $params[6],
            'link' => $params[7],
            'classic' => $this->boolStatus($params[8], 'classic'),
            'vi' => $this->boolStatus($params[9], 'vi'),
        ];

        $arLoadProductArray = Array(
            "IBLOCK_ID"      => $this->iblockId,
            "PROPERTY_VALUES"=> $PROP,
            "NAME"           => $params[2],
            "ACTIVE"         => "Y",
        );

        if ($el->Add($arLoadProductArray)) {
            return true;
        } else {
            $this->errors[] = $el->LAST_ERROR . ' (строка в csv:' . $cursor . ')';
            return false;
        }
    }


    /** Выбираем список городов из справочника с названием в ключе для удобства поиска
     * @return array
     */
    public function getCity()
    {
        $arCity = [];
        $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => $this->iblockSityId], false, []);

        while ($item = $res->GetNext()) {
            $arCity[$item['NAME']] = [
                'id' => $item['ID'],
                'name' => $item['NAME'],
            ];
        }
        return $arCity;
    }


    /** Замена не соответствующих нахваний городов
     * @param $city
     * @return mixed
     */
    public function cityAlias($city)
    {
        $a = ['Москва'];
        $b = ['Москва и МО'];
        return str_replace($a, $b, $city);
    }


    /** Проверяет если ли данные в инфоблоке. Если пустой вернет true иначе false
     * @return bool
     */
    public function checkEmptyIblock()
    {
        $arFilter = [
            "IBLOCK_ID" => $this->iblockId,
        ];

        $res = \CIBlockElement::GetList([], $arFilter, false, ["nPageSize" => 1]);

        if ($res->GetNext()) {
            return false;
        } else {
            return true;
        }
    }


    protected function boolStatus($str, $code)
    {
        echo '<pre>*' . print_r($str, 1) . '*</pre>';
        $param = in_array(strtolower($str), ['да']) ? $this->property[$code]['ID'] : '';
        return ['VALUE' =>$param];
    }
}