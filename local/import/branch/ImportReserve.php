<?php
namespace Aic\branch;

class ImportReserve
{
    private $path = 'https://zenit.ru/local/import/branch/data/reserve.xml';

    private $data = null;
    private $format = null;

    /** Онициализация и парсинг XML
     * @return ImportReserve|\Exception
     */
    public static function parse()
    {
        $object = new ImportReserve();

        try {
            $xml = new \CDataXML();
            $xml->LoadString(file_get_contents($object->path));
            $data = $xml->GetArray();
            $result = [];

            foreach ($data['pointsData']['#']['city'] as $key => $city) {
                $result[$key] = $city['@'];
                $result[$key]['office'] = [];

                if(sizeof($city['#']['office']) > 0) {
                    foreach ($city['#']['office'] as $office) {
                        $result[$key]['office'][] = [
                            'code' => $office['@']['code'],
                            'name' => $office['#']
                        ];
                    }
                }
            }

            $object->data = $result;

        } catch (\Exception $e) {
            return $e;
        }

        return $object;
    }


    /** Возвращает данные
     * @return null
     */
    public function getData()
    {
        if($this->format === null) {
            return $this->data;
        } else {
            return $this->format;
        }
    }

    /** Переводин в формат JSON
     * @return $this
     */
    public function toJson()
    {
        $this->format = json_encode($this->data, JSON_UNESCAPED_UNICODE);
        return $this;
    }

    /** Переводит в формат JS объекта
     * @return $this
     */
    public function toJsObject() {
        $this->format = \CUtil::PhpToJSObject($this->data);
        return $this;
    }
}