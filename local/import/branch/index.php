<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once ('ImportBranch.php');
require_once ('ImportData.php');

use Aic\branch\Import;
$object = new Import();
$object->init();

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_after.php');


