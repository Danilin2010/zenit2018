<?php
namespace Aic\branch;


class ImportData
{
    const
        IB_POINT = 75,
        IB_GROUP = 71,
        IB_BANK = 72,
        IB_SEGMENT = 73,
        IB_SERVICE = 70,
        IB_CITY = 1,
        IB_METRO = 74;

    protected $data = [];

    public function __construct()
    {
        \Bitrix\Main\Loader::includeModule('iblock');
    }

    public function getData()
    {

    }


    /** Выбирает список групп или одну группу по внешнему коду
     * @param bool $uid
     * @return array|bool|mixed
     */
    public function getGroup($uid = false)
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_GROUP;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false);

        while ($ob = $res->GetNext()) {
            $result[$ob['XML_ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }

        $this->data['groups'] = $result;
        return $result;
    }


    /** Добавляет группу если ее нет
     * @param $fields
     */
    public function importGroup($fields)
    {
        $data = $this->getGroup();

        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        if (!empty($fields)) {
            foreach ($fields as $key => $item) {
                $uid = $item['@']['id'];
                $uidListNew[] = $uid;
                $el = new \CIBlockElement;

                //если уже добавлено обновим иначе добавим
                if (array_key_exists($uid, $data)) {
                    $res = $el->Update($data[$uid]['id'], [
                        'ACTIVE' => 'Y',
                        'NAME' => $item['#']
                    ]);
                } else {
                    $arFields = [
                        "IBLOCK_ID" => self::IB_GROUP,
                        'ACTIVE' => 'Y',
                        'NAME' => $item['#'],
                        'XML_ID' => $uid
                    ];
                    if ($PRODUCT_ID = $el->Add($arFields)) {
                        $uidListCurrent[] = $uid;
                    }
                }
            }
        }

        //деактивация элеметов которых нет в XML
        $deleteList = array_diff($uidListCurrent, $uidListNew);
        foreach ($deleteList as $dUid) {
            $this->deactivate($data[$dUid]['id']);
        }
    }

    public function getBank()
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_BANK;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false);

        while ($ob = $res->GetNext()) {
            $result[$ob['XML_ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }
        $this->data['banks'] = $result;
        return $result;
    }

    public function importBank($fields)
    {
        $data = $this->getBank();
        $groups = !empty($this->data['groups']) ? $this->data['groups'] : $this->getGroup();


        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        if (!empty($fields)) {
            foreach ($fields as $key => $item) {
                $uid = $item['@']['id'];
                if (empty($uid)) {
                    continue;
                }

                $uidListNew[] = $uid;
                $el = new \CIBlockElement;

                $PROP['group'] = $groups[$item['@']['group']]['id'];

                //если уже добавлено обновим иначе добавим
                if (array_key_exists($uid, $data)) {
                    echo '-';
                    $res = $el->Update($data[$uid]['id'], [
                        'ACTIVE' => 'Y',
                        'NAME' => $item['#'],
                        "PROPERTY_VALUES" => $PROP,
                    ]);
                } else {
                    $arFields = [
                        "IBLOCK_ID" => self::IB_BANK,
                        'ACTIVE' => 'Y',
                        'NAME' => !empty($item['#']) ? $item['#'] : $item['@']['title'],
                        'XML_ID' => $uid,
                        "PROPERTY_VALUES" => $PROP,
                    ];

                    if ($PRODUCT_ID = $el->Add($arFields)) {
                        $uidListCurrent[] = $uid;
                    } else {
                        echo $el->LAST_ERROR;
                    }
                }
            }
        }

        //деактивация элеметов которых нет в XML
        $deleteList = array_diff($uidListCurrent, $uidListNew);
        foreach ($deleteList as $dUid) {
            $this->deactivate($data[$dUid]['id']);
        }
    }


    /** Список сегментов
     * @return bool
     */
    public function getSegment()
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_SEGMENT;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false);

        while ($ob = $res->GetNext()) {
            $result[$ob['XML_ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }
        $this->data['segments'] = $result;
        return $result;
    }


    /** Добавление и обновление сегментов
     * @param $fields
     */
    public function importSegment($fields)
    {
        $data = $this->getSegment();

        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        if (!empty($fields)) {
            foreach ($fields as $key => $item) {
                $uid = $item['@']['id'];
                $uidListNew[] = $uid;
                $el = new \CIBlockElement;

                //если уже добавлено обновим иначе добавим
                if (array_key_exists($uid, $data)) {
                    $res = $el->Update($key, [
                        'ACTIVE' => 'Y',
                        'NAME' => $item['#']
                    ]);
                } else {
                    $arFields = [
                        "IBLOCK_ID" => self::IB_SEGMENT,
                        'ACTIVE' => 'Y',
                        'NAME' => $item['#'],
                        'XML_ID' => $uid
                    ];
                    if ($PRODUCT_ID = $el->Add($arFields)) {
                        $uidListCurrent[] = $uid;
                    }
                }
            }
        }

        //деактивация элеметов которых нет в XML
        $deleteList = array_diff($uidListCurrent, $uidListNew);
        foreach ($deleteList as $dUid) {
            $this->deactivate($data[$dUid]['id']);
        }
    }


    /** Список сервисов
     * @return bool
     */
    public function getService()
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_SERVICE;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false);

        while ($ob = $res->GetNextElement()) {
            $f = $ob->GetFields();
            $p = $ob->GetProperties();
            $result[$f['XML_ID']] = [
                'id' => $f['ID'],
                'uid' => $f['XML_ID'],
                'name' => $f['NAME'],
                //'serviceId' => $p['service']['ID']
            ];
        }
        $this->data['service'] = $result;
        return $result;
    }


    /** Добавление и обновление сервисов
     * @param $fields
     */
    public function importService($fields)
    {
        $data = $this->getService();
        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        foreach ($fields as $key => $service) {
            //проверим есть ли раздел по названию. Если есть то узнаем ID. Иначе создадим и возмем новый ID,
            $res = \CIBlockSection::GetList([], ['NAME' => trim($service['@']['title']), 'IBLOCK_ID' => self::IB_SERVICE]);
            $ob = $res->Fetch();

            if (empty($ob)) {
                $bs = new \CIBlockSection;
                $arFields = Array(
                    "ACTIVE" => 'Y',
                    "IBLOCK_ID" => self::IB_SERVICE,
                    "NAME" => $service['@']['title'],
                    "UF_TYPE" => $service['@']['type']
                );

                $selctionId = $bs->Add($arFields);

            } else {
                $selctionId = $ob['ID'];
            }

            if (!empty($service['#']['usluga'])) {
                foreach ($service['#']['usluga'] as $item) {
                    $uid = $item['@']['id'];
                    $uidListNew[] = $uid;
                    $el = new \CIBlockElement;

                    if (array_key_exists($uid, $data)) {
                        $res = $el->Update($data[$uid]['id'], [
                            'ACTIVE' => 'Y',
                            'NAME' => $item['@']['title'],
                            'IBLOCK_SECTION_ID' => $selctionId,
                        ]);
                    } else {
                        $arFields = [
                            "IBLOCK_ID" => self::IB_SERVICE,
                            'ACTIVE' => 'Y',
                            'NAME' => $item['@']['title'],
                            'XML_ID' => $uid,
                            'IBLOCK_SECTION_ID' => $selctionId
                        ];
                        if ($PRODUCT_ID = $el->Add($arFields)) {
                            $uidListCurrent[] = $uid;
                        }
                    }
                }
            }
        }

        //деактивация элеметов которых нет в XML
        $deleteList = array_diff($uidListCurrent, $uidListNew);
        foreach ($deleteList as $dUid) {
            $this->deactivate($data[$dUid]['id']);
        }

        //TODO: Удаление - деактивация пустых разделов
    }

    public function getCity()
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_CITY;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false);

        while ($ob = $res->GetNext()) {
            $result[$ob['XML_ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }
        $this->data['city'] = $result;
        return $result;
    }


    /** Список метро
     * @return bool
     */
    public function getMetro()
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_METRO;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false, ['ID', 'XML_ID', 'NAME', 'PROPERTY_city']);

        while ($ob = $res->GetNext()) {
            $result[$ob['XML_ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
                'cityValue' => $ob['PROPERTY_CITY_VALUE'],
                'cityValueId' => $ob['PROPERTY_CITY_VALUE_ID']
            ];
        }
        $this->data['metro'] = $result;
        return $result;
    }

    public function importCity($fields)
    {
        $data = $this->getCity();

        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        foreach ($fields as $city) {
            $uid = $city['@']['id'];
            $uidListNew[] = $uid;

            if (!array_key_exists($uid, $data)) {
                $el = new \CIBlockElement;
                $arFields = [
                    "IBLOCK_ID" => self::IB_CITY,
                    'ACTIVE' => 'Y',
                    'NAME' => $city['@']['title'],
                    'XML_ID' => $uid
                ];

                if ($cityId = $el->Add($arFields)) {
                    $uidListCurrent[] = $uid;
                }
            } else {
                $cityId = $data[$uid]['id'];
            }

            if (!empty($city['#']['metro'])) {
                echo 'Есть метро<br>';

                $this->importMetro($city['#']['metro'], $cityId);
            }
        }
    }


    /** Добавление и изменение метро
     * @param $fields
     * @param $cityId
     */
    public function importMetro($fields, $cityId)
    {
        $data = $this->getMetro();
        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        foreach ($fields as $metro) {
            $uid = $metro['@']['id'];
            $uidListNew[] = $uid;
            $el = new \CIBlockElement;

            if (!array_key_exists($uid, $data)) {
                $mUid = $metro['@']['id'];

                $PROP['city'] = $cityId;
                $arFields = [
                    "IBLOCK_ID" => self::IB_METRO,
                    'ACTIVE' => 'Y',
                    'NAME' => $metro['#'],
                    'XML_ID' => $mUid,
                    "PROPERTY_VALUES" => $PROP,
                ];

                if ($metroId = $el->Add($arFields)) {
                    $uidListCurrent[] = $uid;
                }
            } else {
                $res = $el->Update($data[$uid]['id'], [
                    "IBLOCK_ID" => self::IB_METRO,
                    'ACTIVE' => 'Y',
                    'NAME' => $metro['#'],
                    "PROPERTY_VALUES" => [
                        'city' => $cityId
                    ],
                ]);
            }
        }
    }


    public function getPoints()
    {
        $result = false;
        $filter['IBLOCK_ID'] = self::IB_POINT;

        //делаем запрос
        $res = \CIBlockElement::GetList([], $filter, false, false);

        while ($ob = $res->GetNextElement()) {
            $f = $ob->GetFields();
            $p = $ob->GetProperties();

            //echo '<pre>*' . print_r($p, 1) . '*</pre>';

            $result[$f['XML_ID']] = [
                'id' => $f['ID'],
                'uid' => $f['XML_ID'],
                'name' => $f['NAME'],
                'serviceId' => $p['service']['ID']
            ];


        }
        $this->data['points'] = $result;
        return $result;
    }


    public function importPoints($fields)
    {
        $groups = !empty($this->data['groups']) ? $this->data['groups'] : $this->getGroup();
        $banks = !empty($this->data['banks']) ? $this->data['banks'] : $this->getBank();
        $segments = !empty($this->data['segments']) ? $this->data['segments'] : $this->getSegment();
        $service = !empty($this->data['service']) ? $this->data['service'] : $this->getService();
        $city = !empty($this->data['city']) ? $this->data['city'] : $this->getCity();
        $metroTmp = !empty($this->data['metro']) ? $this->data['metro'] : $this->getMetro();
        $metro = [];

        foreach ($metroTmp as $c) {
            $metro[$c['cityValue']][$c['name']] = $c;
        }

        $data = $this->getPoints();
        $uidListCurrent = array_keys($data);
        $uidListNew = [];

        foreach ($fields['point'] as $point) {
            $item = $point['#'];
            $uid = $item['ID'][0]['#'];
            $uidListNew[] = $uid;

            $el = new \CIBlockElement;

            $serviceId = [];

            if (!empty($item['services'][0]['#']['usluga'])) {
                foreach ($item['services'][0]['#']['usluga'] as $serv) {
                    if (isset($service[$serv['#']]['id'])) {
                        $serviceId[] = $service[$serv['#']]['id'];
                    }
                }
            }

            $metroIdList = [];

            foreach ($item['metro'][0]['#'] as $m) {
                $mKey = $metro[ $city[ $item['cityID'][0]['#'] ]['id'] ][ $m[0]['#'] ]['id'];
                if(!empty($mKey)) {
                    $metroIdList[] = $mKey;
                }
            }

            $PROP = [
                'coord' => $item['cords'][0]['#'],
                'bankId' => $banks[$item['bankID'][0]['#']]['id'],
                'segmentId' => $segments[$item['segmentID'][0]['#']]['id'],
                'cityId' => $city[$item['cityID'][0]['#']]['id'],
                //'metroId' => $metro[ $city[$item['cityID'][0]['#']]['id'] ][ $item['metro'][0]['#']['station1'][0]['#'] ]['id'],
                'metroId' => $metroIdList,
                'maphint' => $item['maphint'][0]['#'],
                'chief' => $item['chief'][0]['#'],
                'buch' => $item['buch'][0]['#'],
                'address' => $item['adress'][0]['#'],
                'phone' => $item['phone'][0]['#'],
                'rekvisits' => $this->stringFormat($item['rekvisits'][0]['#']),
                'mo' => $item['mo'][0]['#'] == true ? 1 : 0,
                'mark' => $item['mark'][0]['#'],
                'service' => $serviceId,
            ];


            $PROP = array_merge($PROP, $this->buildWorktime($item['worktime'][0]['#']));

            /*     if(!empty($item['worktime'][0]['#']['time'])) {
                     if(sizeof($item['worktime'][0]['#']['time']) > 1) {


                         $PROP['worktime_ret'] = [

                         ];

                     } else {
                         $PROP['worktimeRetText'] = $item['worktime'][0]['#']['time'][0]['#'];
                     }

                 }

                 if(!empty($item['worktime'][0]['#']['corpTime'])) {
                     $PROP['worktime_corp'] = $item['worktime'][0]['#']['corpTime'][0]['#'];
                 }

                 if(!empty($item['worktime'][0]['#']['atmTime'])) {
                     $PROP['worktime_atm'] = $item['worktime'][0]['#']['atmTime'][0]['#'];
                 }*/



            if (array_key_exists($uid, $data)) {
                $res = $el->Update($data[$uid]['id'], [
                    'ACTIVE' => 'Y',
                    'NAME' => $item['name'][0]['#'],
                    'PREVIEW_TEXT' => $item['description'][0]['#'],
                    "PROPERTY_VALUES" => $PROP,
                ]);
            } else {
                $arFields = [
                    "IBLOCK_ID" => self::IB_POINT,
                    'ACTIVE' => 'Y',
                    'NAME' => $item['name'][0]['#'],

                    'XML_ID' => $uid,
                    'PREVIEW_TEXT' => $item['description'][0]['#'],
                    "PROPERTY_VALUES" => $PROP,
                ];
                if ($PRODUCT_ID = $el->Add($arFields)) {
                    $uidListCurrent[] = $uid;
                } else {
                    echo $el->LAST_ERROR;
                }
            }
        }


        //деактивация элеметов которых нет в XML
        $deleteList = array_diff($uidListCurrent, $uidListNew);
        foreach ($deleteList as $dUid) {
            $this->deactivate($data[$dUid]['id']);
        }
    }


    /** Подготавливает множественные поля для расписания работы
     * @param $work
     * @param $lanch
     * @return array
     */
    protected function buildWorktimeDays($work, $lanch)
    {
        $week = ['mon' => 'n0', 'tue' => 'n1', 'wed' => 'n2', 'thu' => 'n3', 'fri' => 'n4', 'sat' => 'n5', 'sun' => 'n6'];
        $data = [
            "n0" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
            "n1" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
            "n2" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
            "n3" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
            "n4" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
            "n5" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
            "n6" => ["VALUE" => "нет данных", "DESCRIPTION" => ""],
        ];

        //Время работы
        foreach ($work as $item) {
            if (!empty($item['#'])) {
                $day = $week[$item['@']['day']];
                //TODO: добавить перерывы
                $data[$day]['VALUE'] = $item['#'];
            }
        }
        return $data;
    }


    /** конструктор построения расписания работы
     * @param $data
     * @return array
     */
    protected function buildWorktime($data)
    {

        $prop = [];
        //Если есть ATM
        if (array_key_exists('atm', $data)) {
            if(sizeof($data['atm'][0]['#']['time']) > 1) {
                $prop['worktime_atm'] = $this->stringFormat($this->buildWorktimeDays($data['atm'][0]['#']['time']));
            }
        }

        //Физ лица
        if (array_key_exists('retail', $data)) {
            //Офис
            if(sizeof($data['retail'][0]['#']['time']) > 1) {
                $prop['worktime_ret'] = $this->buildWorktimeDays($data['retail'][0]['#']['time']);
            } else {
                $prop['worktimeRetText'] = $this->stringFormat($data['retail'][0]['#']['time'][0]['#']);
            }

            //Касса
            if(sizeof($data['retail'][0]['#']['kassa']) > 1) {
                $prop['worktime_ret_cashbox'] = $this->buildWorktimeDays($data['retail'][0]['#']['kassa']);
            } else {
                $prop['worktimeRetCashboxText'] = $this->stringFormat($data['retail'][0]['#']['kassa'][0]['#']);
            }
        }

        //ЮР лица
        if (array_key_exists('corp', $data)) {
            //Офис
            if(sizeof($data['corp'][0]['#']['time']) > 1) {
                $prop['worktime_corp'] = $this->buildWorktimeDays($data['corp'][0]['#']['time']);
            } else {
                $prop['worktimeCorpText'] = $this->stringFormat($data['corp'][0]['#']['time'][0]['#']);
            }

            //Касса
            if(sizeof($data['retail'][0]['#']['kassa']) > 1) {
                $prop['worktime_cor_cashbox'] = $this->buildWorktimeDays($data['corp'][0]['#']['kassa']);
            } else {
                $prop['worktimeCorCashboxText'] = $this->stringFormat($data['corp'][0]['#']['kassa'][0]['#']);
            }
        }

        //Операционная касса вне кассового узла
        if (array_key_exists('okvku', $data)) {
            $prop['okvkuText'] = $data['okvku'][0]['#'];
        }




        return $prop;
    }

    /** Деактивация элемента если его нет в списке
     * @param $id
     */
    public function deactivate($id)
    {
        $el = new \CIBlockElement;
        $res = $el->Update($id, ['ACTIVE' => 'N']);
    }


    public function stringFormat($string) {
        return str_replace(['\n'], ['<br>'], $string);
    }
}

