<?php
/**
 * Класс для импорта оффисов и банкоматов из XML файлов
 */
namespace Aic\branch;
use Aic\branch\ImportData;

class Import
{

    protected $path = '';
    protected $fileKey = 'group_keys.xml';
    protected $filePoint = 'group_points.xml';

    private $oData;

    /**
     * Import constructor.
     * @param $path путь до директории
     */
    public function __construct($path = false)
    {
        if ($path === false) {
            $this->path = $_SERVER['DOCUMENT_ROOT'] . '/local/import/branch/data/';
        } else {
            $this->path = $path;
        }

        $this->oData = new ImportData();
    }


    /**
     * Проверяет есть ли файлы в директории
     */
    protected function isFileDir()
    {
        if (file_exists($this->path . $this->fileKey) && file_exists($this->path . $this->filePoint)) {
            return true;
        } else {
            return false;
        }
    }


    protected function parseXml($file)
    {
        $xml = new \CDataXML();
        $xml->Load($this->path . $file);
        $arData = $xml->GetArray();

        if($file === $this->fileKey) {
            $result['groups'] = $arData['marketPointsData']['#']['groups']['0']['#']['group'];
            $result['banks'] = $arData['marketPointsData']['#']['banks']['0']['#']['bank'];
            $result['segments'] = $arData['marketPointsData']['#']['segments']['0']['#']['segment'];
            $result['cities'] = $arData['marketPointsData']['#']['cities']['0']['#']['city'];
            $result['service'] = $arData['marketPointsData']['#']['service'];
        }

        if($file === $this->filePoint) {
            $result = $arData['points']['#'];
        }
        return $result;
    }

    protected function importKeys() {
        $data = $this->parseXml($this->fileKey);

        $this->oData->importGroup($data['groups']);
        $this->oData->importBank($data['banks']);
        $this->oData->importSegment($data['segments']);
        $this->oData->importService($data['service']);
        $this->oData->importCity($data['cities']);
        if(empty($data)) {
            throw new \Exception('Ошибка разбора XML файла', 500);
        }
    }


    protected function importPoint() {
        $data = $this->parseXml($this->filePoint);
        $this->oData->importPoints($data);
        if(empty($data)) {
            throw new \Exception('Ошибка разбора XML файла', 500);
        }
    }

    public function logs()
    {

    }

    /**
     * Инициализация импорта
     */
    public function init()
    {
        try {
            $start = microtime(true);
            if ($this->isFileDir()) {
                echo 'Файлы есть можно парсить<br>';

                $this->importKeys();
                $this->importPoint();
            } else {
                echo 'Нет файлов в директории<br>';
            }

            echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.<br>';
            unlink( $this->path . $this->fileKey);
            unlink( $this->path . $this->filePoint);
        } catch (\Exception $e) {
            echo 'Ошибка: ' . $e->getMessage() . ' [' . $e->getCode() . ']';
        }
    }


}