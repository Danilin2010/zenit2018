<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
foreach($arResult["ITEMS"] as &$arItem){

    if($arItem["DISPLAY_PROPERTIES"]["title_home"])
        $arItem["NAME"]=$arItem["DISPLAY_PROPERTIES"]["title_home"]["DISPLAY_VALUE"];

}unset($arItem);

//echo "<pre>";print_r($arResult["ITEMS"]);echo "</pre>";