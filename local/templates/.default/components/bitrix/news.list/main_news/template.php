<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])>0){?>
<div class="news-slider">
    <div class="news-slider__wrapper" id="news-slider-app">
        <div class="news-slider__header">
            <div class="news-slider__news-date" v-text="itemDate"></div>
            <div class="news-slider__arrows-wrapper">
                <div class="news-slider__arrow-wrapper news-slider__arrow-wrapper_prev">
                    <div class="news-slider__arrow-inner">
                        <svg class="icon news-slider__arrow-icon icon__text-arrow-symbol">
                            <use xlink:href="#icon__text-arrow-symbol"></use>
                        </svg>
                    </div>
                </div>
                <div class="news-slider__arrow-wrapper news-slider__arrow-wrapper_next">
                    <div class="news-slider__arrow-inner">
                        <svg class="icon news-slider__arrow-icon icon__text-arrow-symbol">
                            <use xlink:href="#icon__text-arrow-symbol"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="news-slider__content">
            <!-- Список слайдов. Дату слайда подставлять в data-date-->
            <?foreach($arResult["ITEMS"] as $arItem){?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <a  id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="news-slider__text" href="<?=$arItem['DETAIL_PAGE_URL']?>" data-date="<?=$arItem['DISPLAY_ACTIVE_FROM']?>"><?=$arItem['NAME']?></a>
            <?}?>
        </div>
        <div class="news-slider__footer">
            <?
            if($arResult["ITEMS"][0])
            ?>
            <a class="button button_primary news-slider__button" href="<?=$arResult["ITEMS"][0]["LIST_PAGE_URL"]?>">Все новости</a>
        </div>
    </div>
</div>
<?}?>