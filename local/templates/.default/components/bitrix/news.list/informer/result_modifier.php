<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
foreach($arResult["ITEMS"] as &$arItem){
    if($arItem["FIELDS"]["TIMESTAMP_X"])
        $arItem["UNIX_TIME"]=strtotime($arItem["TIMESTAMP_X"]);

    if($arItem["DISPLAY_PROPERTIES"]["link"]["DISPLAY_VALUE"])
        $arItem["LINK"]=$arItem["DISPLAY_PROPERTIES"]["link"]["DISPLAY_VALUE"];

}unset($arItem);