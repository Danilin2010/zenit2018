<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arTemplateParameters = array(
	"MAIN_PAGE" => Array(
		"NAME" => "Главная страница",
		"TYPE" => "STRING",
		"DEFAULT" => "N",
	),
    "SHOW_INFORMER" => Array(
        "DEFAULT" => "MAIN",
        "NAME" => "Где показывать?",
        "TYPE" => "LIST",
        "VALUES" => array(
            "ALL" => "Показывать на всех",
            "MAIN" => "Показывать на главной",
        )
    ),
);
?>