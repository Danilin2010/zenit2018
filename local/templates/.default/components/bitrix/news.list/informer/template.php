<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(($arParams["SHOW_INFORMER"]=="MAIN" && $arParams["MAIN_PAGE"]=="Y") || $arParams["SHOW_INFORMER"]=="ALL"){
    foreach($arResult["ITEMS"] as $arItem){
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div data-date="<?=$arItem["UNIX_TIME"]?>" class="popup-informer" :class="{&quot;popup-informer_state-show&quot;: popupInformerShow}">
        <div class="popup-informer__wrapper">
            <div class="popup-informer__inner" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <svg class="icon popup-informer__icon icon__inform-symbol">
                    <use xlink:href="#icon__inform-symbol"></use>
                </svg>
                <span class="popup-informer__text"><?=$arItem["PREVIEW_TEXT"]?>
                                <?=$arItem["LINK"]?>
                            </span>
                <a class="popup-informer__close-button" href="#" @click.prevent="setInformerState()">
                    <svg class="icon icon__close-symbol">
                        <use xlink:href="#icon__close-symbol"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <?}
}?>
