<?
use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
foreach($arResult["ITEMS"] as &$arItem){
    if($arItem["DISPLAY_PROPERTIES"]["link"]["VALUE"])
        $arItem["DETAIL_PAGE_URL"]=$arItem["DISPLAY_PROPERTIES"]["link"]["VALUE"];

    if(!$arItem["DISPLAY_PROPERTIES"]["img_head"] && $arItem["DISPLAY_PROPERTIES"]["img_anons"])
        $arItem["DISPLAY_PROPERTIES"]["img_head"]=$arItem["DISPLAY_PROPERTIES"]["img_anons"];

}unset($arItem);
$NewItem=array();
foreach($arResult["ITEMS"] as $key=>$arItem){
    if($arItem["DISPLAY_PROPERTIES"]["important"]["VALUE"]=="Y")
    {
        $NewItem[]=$arItem;
        unset($arResult["ITEMS"][$key]);
    }
};
$arResult["ITEMS"] = array_merge($NewItem, $arResult["ITEMS"]);
//echo "<pre>";print_r($arResult["ITEMS"]);echo "</pre>";