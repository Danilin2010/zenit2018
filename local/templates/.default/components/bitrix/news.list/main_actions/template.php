<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="cards-block">
    <div class="cards-block__wrapper">
        <div class="cards-block__inner">
            <!-- Широкая карточка с картинкой-->
            <?if($arResult["ITEMS"][0]){
                $item=$arResult["ITEMS"][0];
                ?>
            <div class="main-card main-card_circle-decorate main-card_circle-top">
                <!-- Ссылка на акцию-->
                <a class="main-card__wrapper" href="<?=$item["DETAIL_PAGE_URL"]?>">
								<span class="main-card__text-inner">
									<!-- Название акции-->
									<span class="main-card__name"><?=$item["TAGS"]?></span>
                                    <!-- Заголовок акции-->
									<span class="main-card__title"><?=$item["PREVIEW_TEXT"]?></span>
								</span>
                    <span class="main-card__image-wrapper">
									<svg class="main-card__image-inner" preserveAspectRatio="xMaxYMin meet" width="425px" height="396px" viewBox="0 0 425 396" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<defs>
											<path class="main-card__image-clip" id="clip" d="M63.7101849,2.55795385e-13 L425,2.55795385e-13 L425,396 L64.8749834,396 C30.6315098,337.598249 11,269.591717 11,197 C11,125.235387 30.1866986,57.9520685 63.7102484,-0.000109696231 Z"></path>
										</defs>
										<clipPath id="clip-image">
											<use xlink:href="#clip"></use>
										</clipPath>
                                        <!-- Размер картинки должен быть не менее 414px x 396px с соблюдением соотношения сторон-->
                                        <?if($item['DISPLAY_PROPERTIES']['img_anons']['FILE_VALUE']['SRC']){?>
                                            <image class="main-card__image"
                                                   xlink:href="<?=$item['DISPLAY_PROPERTIES']['img_anons']['FILE_VALUE']['SRC']?>"
                                                   x="11" y="0"
                                                   width="414"
                                                   height="396"
                                                   style="clip-path: url(#clip-image);"></image>
                                        <?}?>
										<g class="main-card__image-circle-group">
											<circle class="main-card__image-circle" fill="#FFFFFF" cx="38" cy="65" r="36"></circle>
											<path class="main-card__image-circle" d="M24.5753884,98 C10.289237,93.2132018 0,79.7701448 0,63.9347536 C0,44.0885372 16.1614761,28 36.0976672,28 C40.6436727,28 44.9934056,28.8365504 49,30.363448 C38.8808718,51.9163241 30.6673983,74.5334531 24.5753884,98 Z"></path>
										</g>
									</svg>
								</span>
                </a>
            </div>
            <?}?>
            <!-- Квадратная карточка с треугольником и кругом в углу-->
            <!-- Квадратная карточка с квадратом и кругом в углу-->
            <div class="main-card main-card_square-circle-decorate">
                <!-- Ссылка на акцию-->
                <?if($arResult["ITEMS"][1]){
                $item=$arResult["ITEMS"][1];
                ?>
                <a class="main-card__wrapper" href="<?=$item["DETAIL_PAGE_URL"]?>">
								<span class="main-card__text-inner">
									<!-- Название акции-->
									<span class="main-card__name"><?=$item["TAGS"]?></span>
                                    <!-- Заголовок акции-->
									<span class="main-card__title"><?=$item["PREVIEW_TEXT"]?></span>
								</span>
                    <span class="main-card__image-wrapper">
									<svg class="main-card__image-inner" width="120px" height="120px" viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<path d="M0,-1.47792889e-12 L116,-1.47792889e-12 C118.209139,-1.4783347e-12 120,1.790861 120,4 L120,120 L0,-1.47792889e-12 Z" fill="#10C8D2"></path>
										<g class="main-card__image-circle-group">
											<circle fill="#10C8D2" cx="60" cy="60" r="39"></circle>
											<path d="M32.4228355,32.4228355 C39.4804474,25.3652237 49.2304474,21 60,21 C81.5391052,21 99,38.4608948 99,60 C99,70.7695526 94.6347763,80.5195526 87.5771645,87.5771645 L32.4228355,32.4228355 Z" fill="#FFFFFF"></path>
										</g>
									</svg>
								</span>
                </a>
                <?}?>
            </div>
            <!-- Слайдер новостей главной страницы-->
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "main_news",
                array(
                    "COMPONENT_TEMPLATE" => "main_news",
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => IBLOCK_NEWS,
                    "NEWS_COUNT" => "20",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "ACTIVE_FROM",
                    "SORT_ORDER2" => "DESC",
                    "FILTER_NAME" => "",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "title_home",
                        2 => "",
                    ),
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "j F",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "N",
                    "STRICT_SECTION_CHECK" => "N",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                ),
                $component
            );?>

            <?if($arResult["ITEMS"][2]){
            $item=$arResult["ITEMS"][2];
            ?>
            <div class="main-card main-card_square-decorate">
                <a class="main-card__wrapper" href="<?=$item["DETAIL_PAGE_URL"]?>">
								<span class="main-card__text-inner">
									<span class="main-card__name"><?=$item["TAGS"]?></span>
									<span class="main-card__title"><?=$item["PREVIEW_TEXT"]?></span>
								</span>
                    <span class="main-card__image-wrapper"></span>
                </a>
            </div>
            <?}?>
            <?if($arResult["ITEMS"][3]){
            $item=$arResult["ITEMS"][3];
            ?>
            <div class="main-card main-card_sales-decorate">
                <a class="main-card__wrapper" href="<?if($item["DISPLAY_PROPERTIES"]["link"]["VALUE"]){echo $item["DISPLAY_PROPERTIES"]["link"]["VALUE"];}else{ echo $arParams["LINK_ACTIONS"];}?>">
								<span class="main-card__text-inner">
									<span class="main-card__name">Другое</span>
									<span class="main-card__title"><?=$item["PREVIEW_TEXT"]?></span>
								</span>
                    <span class="main-card__image-wrapper">
									<span class="main-card__image-inner">
										<span class="main-card__image"></span>
										<span class="main-card__image"></span>
										<span class="main-card__image"></span>
									</span>
								</span>
                </a>
            </div>
            <?}?>
        </div>
    </div>
</div>