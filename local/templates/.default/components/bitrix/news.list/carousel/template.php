<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main-slider" id="main-slider-app">
    <div class="main-slider__slick">
        <?foreach($arResult["ITEMS"] as $arItem){?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
        <div class="main-slider__slide">
            <div class="main-slider__slide-inner" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="main-slider__text-wrapper">
                    <div class="main-slider__text-inner">
                        <div class="main-slider__title"><?=$arItem["DISPLAY_PROPERTIES"]["title"]["DISPLAY_VALUE"]?></div>
                        <div class="main-slider__description"><?=$arItem["PREVIEW_TEXT"]?></div>
                    </div>
                    <div class="main-slider__slide-buttons">
                        <a <?if($arItem["DISPLAY_PROPERTIES"]["new_window"]["DISPLAY_VALUE"]=="Y"){?>target="_blank" <?}?> class="button button_primary main-slider__learn-more" href="<?=$arItem["DISPLAY_PROPERTIES"]["link"]["DISPLAY_VALUE"]?>"><?=$arItem["DISPLAY_PROPERTIES"]["button_text"]["~VALUE"]?></a>
                        <button class="button button_transparent main-slider__not-interesting" type="button" @click="nexSlide()">Не интересно</button>
                    </div>
                </div>
                <div class="main-slider__image-wrapper">
                    <div class="main-slider__image-inner">
                        <div class="main-slider__image-half-circle"></div>
                        <?if(is_array($arItem["PREVIEW_PICTURE"])){?>
                        <img class="main-slider__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>">
                        <?}?>
                    </div>
                    <div class="main-slider__counter">
                        <svg class="main-slider__counter-image" viewBox="25 25 50 50">
                            <circle class="main-slider__counter-circle" cx="50" cy="50" r="20" fill="none" :style="{&quot;animation-delay&quot;: firstSlideShow ? &quot;0s&quot; : animationSlide + &quot;ms&quot;}"></circle>
                        </svg>
                        <div class="main-slider__counter-value" style="display: none;" :style="{display: slidesLength ? &quot;block&quot; : &quot;none&quot;}">{{ currentSlide }}/{{ slidesLength }}</div>
                    </div>
                </div>
            </div>
        </div>
        <?}?>
    </div>
    <div class="main-slider__arrows-wrapper">
        <div class="main-slider__arrow-wrapper main-slider__arrow-wrapper_prev">
            <div class="main-slider__arrow-inner">
                <svg class="icon main-slider__arrow-icon icon__angle-arrow-symbol">
                    <use xlink:href="#icon__angle-arrow-symbol"></use>
                </svg>
            </div>
        </div>
        <div class="main-slider__arrow-wrapper main-slider__arrow-wrapper_next">
            <div class="main-slider__arrow-inner">
                <svg class="icon main-slider__arrow-icon icon__angle-arrow-symbol">
                    <use xlink:href="#icon__angle-arrow-symbol"></use>
                </svg>
            </div>
        </div>
    </div>
</div>