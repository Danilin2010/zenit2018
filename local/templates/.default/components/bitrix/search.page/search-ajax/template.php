<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

header('Content-type: application/json');

$searchArr = [
	'success' => true,
	'result' => []
];

if ($arResult['REQUEST']['QUERY'] === false && $arResult['REQUEST']['TAGS'] === false)
{

}
elseif ($arResult['ERROR_CODE'] != 0)
{
	$searchArr['success'] = true;
	$searchArr['error'] = GetMessage('SEARCH_ERROR') . ' ' . $arResult['ERROR_TEXT'] . ' ' . GetMessage('SEARCH_CORRECT_AND_CONTINUE');
}
elseif (count($arResult['SEARCH']) > 0)
{
	foreach($arResult['SEARCH'] as $key => $val)
	{
		$items = [];

		foreach ($val as $arItem)
		{
			$items[] = [
				'name' => $arItem['TITLE_FORMATED'],
				'link' => $arItem['URL']
			];
		}

		$searchArr['result'][] = [
			'title' => $key,
			'items' => $items
		];
	}
}
else
{
	$searchArr['success'] = true;
	$searchArr['error'] = GetMessage('SEARCH_NOTHING_TO_FOUND');
}

echo json_encode($searchArr);
