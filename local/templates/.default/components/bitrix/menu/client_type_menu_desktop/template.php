<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)){?>
<div class="client-type-menu client-type-menu_desktop">
    <?
    foreach($arResult as $arItem){
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <a class="client-type-menu__item <?if($arItem["SELECTED"]){?>client-type-menu__item_active<?}?>" href="<?=$arItem["LINK"]?>">
            <span class="text text_bold client-type-menu__text"><?=$arItem["TEXT"]?></span>
        </a>
    <?}?>
</div>
<?}?>