<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)){?>
    <div class="main-menu__footer-links">
    <?
    foreach($arResult as $arItem){
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <a class="main-menu__link-item" href="<?=$arItem["LINK"]?>">
            <span class="text main-menu__link-text"><?=$arItem["TEXT"]?></span>
        </a>
    <?}?>
</div>
<?}?>