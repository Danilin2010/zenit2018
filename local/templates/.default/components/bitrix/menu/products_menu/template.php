<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)){?>
<div class="products-menu">
    <div class="products-menu__wrapper">
        <nav class="products-menu__inner">
    <?
    foreach($arResult as $arItem){
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
            <a class="products-menu__link" href="<?=$arItem["LINK"]?>">
                <span class="products-menu__link-title"><?=$arItem["TEXT"]?></span>
                <span class="products-menu__link-text"><span><?=$arItem["PARAMS"]["text"]?></span>
								<svg class="icon products-menu__link-arrow icon__text-arrow-symbol">
									<use xlink:href="#icon__text-arrow-symbol"></use>
								</svg>
							</span>
            </a>
        <?}?>
        </nav>
    </div>
</div>
<?}?>