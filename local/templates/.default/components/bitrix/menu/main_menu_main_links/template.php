<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)){?>
    <?
    $first=true;
    $i=0;
    ?>
    <div class="main-menu__main-links">
    <!-- TODO: Класс для текущего раздела меню main-menu__link-item_current-->
    <?
    foreach($arResult as $arItem){
        if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
            continue;
        ?>
        <?$i++;?>
        <?if($first){?>
            <?if($i==$arParams["NUMBER_DISPLAYED"]){?>
                <?$first=false;?>
                <a class="main-menu__link-item main-menu__link-item_more" href="<?=$arItem["LINK"]?>" @click.prevent="onMouseoverMoreLinks($event)" @mouseover="onMouseoverMoreLinks($event)" :class="{&quot;link-item_state-open&quot;: moreLinksStateOpen}">
                    <span class="text main-menu__link-text"><?=$arItem["TEXT"]?></span>
                </a>
                </div>
                <div class="main-menu__more-links" @mouseleave="onMouseleaveMoreLinks()" :style="moreLinksPosition" :class="{&quot;main-menu__more-links_state-open&quot;: moreLinksStateOpen}">
            <?}else{?>
                <a class="main-menu__link-item <?if($arItem["SELECTED"]){?>main-menu__link-item_current<?}?>" href="<?=$arItem["LINK"]?>">
                    <span class="text main-menu__link-text"><?=$arItem["TEXT"]?></span>
                </a>
            <?}?>
        <?}else{?>
            <a class="main-menu__link-item <?if($arItem["SELECTED"]){?>main-menu__link-item_current<?}?>" href="<?=$arItem["LINK"]?>">
                <span class="text main-menu__link-text"><?=$arItem["TEXT"]?></span>
            </a>
        <?}?>
    <?}?>
    </div>
<?}?>
