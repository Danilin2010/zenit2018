var RealtyMap = (function ($) {
    var myMap = null;
    var clusterer = null;
    var data = [];
    var stylePoint;
    var placeholderCollection;
    var cityName;
    var modalMap = null;
    var self = {};


    function renderControl() {
        var width = $(window).width();
        if(width < 992) {
            myMap.controls.add('zoomControl', {
                size: "small",
                position: {
                    top: 100
                }
            });
        } else {
            myMap.controls.add('zoomControl', {
                size: "large",
                position: {
                    top: 100
                }
            });
        }
        console.log(width);
    }


    self.init = function (options) {
        if (options.city) {
            cityName = options.city
        }

        return this;
    };




    self.createMap = function (callback) {
        ymaps.geocode('Россия, ' + cityName + '', {results: 1}).then(function (res) {

            var firstGeoObject = res.geoObjects.get(0);
            myMap = new ymaps.Map("map-bank", {
                center: firstGeoObject.geometry.getCoordinates(),
                zoom: 12,
                controls: [],
            }, {
                searchControlProvider: 'yandex#search'
            });


           /* myMap.controls.add("zoomControl", {
                position: {top: 100, left: 30}
            });*/

            hasInit = true;

            if (callback) {
                callback();
            }

       /*     modalMap = new ymaps.Map("map-modal", {
                center: firstGeoObject.geometry.getCoordinates(),
                zoom: 12,
                controls: []
            }, {
                searchControlProvider: 'yandex#search'
            });*/

            renderControl();

        });
    };


    function debug() {
        myMap.geoObjects.add(new ymaps.Placemark([55.925259, 37.549583], {
            balloonContent: 'цвет <strong>воды пляжа бонди</strong>'
        }));
    }

    self.gotToCity = function (cityName) {
        /* ymaps.geocode('Россия, ' + cityName + '', {results: 1}).then(function (res) {
         var firstGeoObject = res.geoObjects.get(0);
         firstGeoObject.geometry.getCoordinates();
         myMap.setCenter(firstGeoObject.geometry.getCoordinates())
         });*/
    };

    self.removePoints = function () {
        if (clusterer != null) {
            clusterer.removeAll();
            clusterer = null;
        }
    };

    /**
     * Устанавливает точки и события
     * @param points модель c элементами
     */
    self.setNewPoints = function (points) {
        self.removePoints();

        var MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<span style="color: #FFFFFF; font-weight: bold;">$[properties.geoObjects.length]</span>');

        clusterer = new ymaps.Clusterer({
            clusterIcons: clasterStyle(),
            groupByCoordinates: false,
            clusterDisableClickZoom: false,
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false,
            clusterIconContentLayout: MyIconContentLayout,
            maxZoom: 15,
            clusterMaxZoom: 15
        });

        var geoObjects = [];

        for (var i = 0; i < points().length; i++) {
            var pl = new ymaps.Placemark(points()[i].coord, {
                obj: points()[i],
                hintContent: points()[i].name
            }, pointStyle());

            pl.events.add('click', openDialog);
            geoObjects.push(pl);
        }


        clusterer.add(geoObjects);
        myMap.geoObjects.add(clusterer);


        var clasterBounds = clusterer.getBounds();

        myMap.setBounds(clusterer.getBounds());
        var zoom = myMap.getZoom();

        if (zoom > 15) {
            zoom = 15
        } else {
            zoom = (zoom - 0.4);
        }

        myMap.setZoom(zoom);
    };


    self.setModalPoint = function (obj) {
        modalMap.geoObjects.removeAll();
        var placemark = new ymaps.Placemark(obj.coord, {}, pointStyle());
        modalMap.geoObjects.add(placemark);
        modalMap.setCenter(obj.coord);
    }


    function openDialog(placemark) {
        var obj = placemark.get("target").properties.get('obj');
        $(document).trigger("clickRealtyPoint", obj);
    }


    function pointStyle() {
        return {
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/bz/img/map-point.svg',
            iconImageSize: [40, 40],
        }
    }

    function clasterStyle() {

        return [
            {
                href: '/local/templates/bz/img/map-claster.svg',
                color: '#fff',
                size: [40, 40],
                offset: [-20, -20]
            }
        ];
    }

    return self;
})(jQuery);