var flyConstructor={
    useconversion:[
        {val:1000,percent:0},
        {val:500000,percent:50},
        {val:1000000,percent:100}
    ],
    data:{
        summ:'[data-summ]',
        summSelect:'[data-summ-select]'
    },
    /**initCallbackForm**/
    wrCalculations:function(){
        _this=this;
        var bSumm=$(_this.data.summ).val();
        var bIndex=0;

        var $el = $('.wr_shares .shares_container'),
            baraja = $el.baraja();

        $.each(baraja.$items,function(){
            var summ=parseInt($(this).data('summ')),
                index=$(this).index()
                ;
            if(bSumm>=summ)
                bIndex=index;
        });
        baraja.setElement(bIndex);
    },
    /**initCallbackForm**/
    initCallbackForm:function(){
        _this=this;
        $(_this.data.summ).on('change',function(e){
            _this.wrCalculations();
        });
    },
    /**initSlider**/
    initSlider:function(){
        _this=this;
        $(_this.data.summSelect).each(function(){
            var conversion=true;
            conversion=false;
            var this2=this,
                min=$(this2).data('min'),
                max=$(this2).data('max'),
                target=$(this2).data('target'),
                value=$(this2).data('value'),
                suffix=$(this2).data('suffix'),
                del=$(this2).data('del'),
                textMin=min,
                textMax=max,
                textValue=value,
                elTarget=$('#'+target);
            if(conversion)
            {
                _this.conversion=new $.conversion({
                    min:min,
                    max:max,
                    delta:1,
                    grid:_this.useconversion
                });
                textMin=_this.conversion.toSlider(min);
                textMax=_this.conversion.toSlider(max);
                textValue=_this.conversion.toSlider(value);
            }

            var steppips=(textMax-textMin)/2;
            if(typeof(del)=="undefined")
            {
                del=1;
            }else{
                del=parseInt(del);
                if(del<=0)
                    del=1;
            }
            if(typeof(suffix)=="undefined")
                suffix="";
            $(this2).slider({
                range: "min",
                max: textMax,
                min: textMin,
                //step: stepdel,
                value:textValue,
                create: function(){
                    if(conversion)
                        elTarget.val(_this.conversion.fromSlider($(this).slider("value")));
                    else
                        elTarget.val($(this).slider("value"));
                    _this.wrCalculations();
                },
                change: function( event, ui ) {
                    var val=ui.value;
                    if(conversion)
                        val=_this.conversion.fromSlider(val);
                    if(!elTarget.hasClass('changes'))
                        elTarget.val(val);
                    elTarget.removeClass('changes');
                },
                slide: function(event,ui){
                    var val=ui.value;
                    if(conversion)
                        val=_this.conversion.fromSlider(val);
                    elTarget.val(val);
                    _this.wrCalculations();
                }
            }).slider("pips",{
                step:steppips,
                suffix: suffix,
                formatLabel: function(value) {
                    var val=value;
                    if(conversion)
                        val=_this.conversion.fromSlider(val);
                    var valtxt=val/del;
                    valtxt=_this.number(valtxt);
                    return this.prefix + valtxt + this.suffix;
                },
                rest: "label"
            });
            elTarget.number(true,0,',',' ');
            elTarget.on('change',function(){
                var val=$(this).val();
                $(this).addClass('changes');
                if(conversion)
                    val=_this.conversion.toSlider(val);
                $(this2).slider("value",val);
                _this.wrCalculations();
            });
        });
    },
    /**number**/
    number:function(num){
        if(isInteger(num))
            return $.number(num, 0,',',' ');
        else
            return $.number(num, 2,',',' ');
    },
    init:function(){
        _this=this;
        var $el = $('.wr_shares .shares_container'),
         baraja = $el.baraja();
        _this.initSlider();
    }
}




$(document).ready(function(){
    flyConstructor.init();
});