// модальное окно
var overlay, modal, open_modal, close;

// определяем ширину прокрутки в px
var wh = window.innerWidth // вся ширина окна
var whs = document.documentElement.clientWidth // ширина минус прокрутка
var pls = (wh - whs); // вычисляем ширину прокрутки и даем ей имя pls

function ShowModal(div, callback) {
    var overlay = $('#overlay');
    overlay.fadeIn(400,
        function () {
            $(div)
                .css('display', 'block')
                .animate({opacity: 1}, 200, function () {
                    if (callback)
                        callback();
                });
            $('body').addClass("modal-open-top").css('padding-right', pls + 'px');
        });
}

function CloseModal() {

    $('.modal_div').animate({opacity: 0}, 200,
        function () {
            $(this).css('display', 'none');
            $('#overlay').fadeOut(400);
            $('body').css('padding-right', 0 + 'px').removeClass("modal-open-top");
        }
    );

}

$(document).ready(function () {

    overlay = $('#overlay');
    modal = $('.modal_div');
    open_modal = $('.open_modal');

    // Вызов под меню с обратной связью
    jQuery(".open_contact_form").click(function (event) {
        event.preventDefault();
        if (jQuery(this).find("span").hasClass("glyphicon-menu-down") == true) {
            jQuery(this).find("span").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        } else {
            jQuery(this).find("span").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        }
        jQuery(".glyphicon-menu-down").slideToggle("100");
    });

    // Открыть модальное окно
    $('body').on('click', '.open_modal', function (event) {
        event.preventDefault();
        ShowModal($(this).attr('href'));
    });

    // Закрыть модальное окно
    close = $('.modal_close, #overlay');
    close.on('click', function () {
        modal
            .animate({opacity: 0}, 200,
                function () {
                    $(this).css('display', 'none');
                    overlay.fadeOut(400);
                    $('body').css('padding-right', 0 + 'px').removeClass("modal-open-top");
                }
            );
    });
});