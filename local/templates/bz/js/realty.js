var Realty = (function (self, modal, overlay, realtyCity) {
    'use strict';

    var arrid;
    var dom = {};
    var model;
    var ready = false;

    // определяем ширину прокрутки в px
    var wh = window.innerWidth; // вся ширина окна
    var whs = document.documentElement.clientWidth; // ширина минус прокрутка
    var pls = (wh - whs); // вычисляем ширину прокрутки и даем ей имя pls
    var data = {};
    var busy = false;
    var loadStatus = false;

    var map = null;

    var viewModel = function () {
        var self = this;
        self.filter = {
            city: null,
            term: null,
            mortgage: []
        };

        self.items = ko.observableArray([]);
        self.listMessage = ko.observableArray([]);
        self.listCity = ko.observableArray([]);
        self.chosenCity = ko.observable(realtyCityDefaultName);
        self.listMortgage = ko.observableArray([]);
        self.chosenMortgage = ko.observableArray([]);


        self.dialog = {
            id: ko.observable(''),
            gk: ko.observable(''),
            name: ko.observable(''),
            address: ko.observable(''),
            ul: ko.observable(''),
            link: ko.observable(''),
            mortgage: ko.observableArray([]),
            logo: ko.observable(''),
            object: ko.observable(''),
            pageUrl: ko.observable()
        };


        self.selectCity = function (data, event) {
            console.log('=city=');
            model.filter.city = event.target.value;
            model.filter.term = '';
            //model.filter.mortgage = [];

            self.chosenMortgage.removeAll();


            reloadControl();

            getItems(function (res) {
                model.items.removeAll();
                var items = model.items();

                $.map(res.items, function (item) {
                    items.push(new createItem(item));
                });
                model.items.valueHasMutated();

                map.setNewPoints(model.items);

                var cityName = searchCityName(event.target.value);
                map.gotToCity(cityName);
            });
        }

        self.term = function (data, event) {

            model.filter.term = event.target.value;

            getItems(function (res) {
                model.items.removeAll();
                var items = model.items();

                $.map(res.items, function (item) {
                    items.push(new createItem(item));
                });
                model.items.valueHasMutated();

                map.setNewPoints(model.items);

            });
        };


        self.checkMortgage = function (data, event) {
            var isset = false;

            for (var i = 0; i < self.chosenMortgage().length; i++) {
                if (event.target.value == self.chosenMortgage()[i]) {
                    console.log('ДА');
                    self.chosenMortgage.remove(self.chosenMortgage()[i]);
                    isset = true;
                    break;
                }
            }

            if (isset === false) {
                self.chosenMortgage.push(event.target.value);
            }


            self.filter.mortgage = self.chosenMortgage();

            reloadControl();

            getItems(function (res) {
                model.items.removeAll();
                var items = model.items();

                $.map(res.items, function (item) {
                    items.push(new createItem(item));
                });
                model.items.valueHasMutated();

                map.setNewPoints(model.items);

            });
        }
    };

    model = new viewModel();


    function onEvent() {
        $(document).on('clickRealtyPoint', openModalById);
    }


    function createCity(item) {
        this.id = item;
        this.name = item;
    }


    function createItem(item) {
        if (item.props === undefined) {
            item['props'] = {};
        }
        this.id = item.id;
        this.name = item.name;
        this.gk = item.props.gk || '';
        this.ul = item.props.ul || '';
        this.region = item.props.region || '';
        this.address = item.props.address || '';
        this.coord = item.props.coord || [];
        this.link = item.props.link || '';
        this.logo = item.props.logo || '';
        this.object = item.props.object || '';
        //this.classic = item.props.classic || '';
        //this.vi = item.props.vi || '';
        this.mortgage = item.props.mortgage || '';
        this.modal = function (data) {
            model.dialog.name(data.name);
            model.dialog.address(data.address);
            model.dialog.gk(data.gk);
            model.dialog.ul(data.ul);
            model.dialog.link(data.link);
            model.dialog.mortgage(data.mortgage);
            model.dialog.pageUrl(createUrl(data));
            model.dialog.logo(data.logo);
            model.dialog.object(data.object);
            //map.setModalPoint(data);
        }
    }


    function createMortgage(item) {
        this.code = item.code;
        this.name = item.name;
    }


    function getItems(callback) {

        if (ready === false) return false;

        preloaderStart();
        $.ajax({
            url: '/realty/ajax.php',
            data: model.filter,
            type: 'POST',
            dataType: 'json',
            success: function (res) {
                if (callback) {
                    callback(res);
                    preloaderEnd();
                }

            },
            error: function (res) {
                if (callback) {
                    callback(res);
                    preloaderEnd();
                }
            }
        });
    }


    function preloaderStart() {
        console.log('крутилка прелоадера показана');
    }

    function preloaderEnd() {
        console.log('крутилка прелоадера скрыта');
    }


    function searchCityName(id) {

        for (var i = 0; i < model.listCity().length; i++) {
            if (model.listCity()[i].id == id) {
                return model.listCity()[i].name;
            }
        }

        return false;
    }


    function openModalById(event, data) {
        model.dialog.gk(data.gk);
        model.dialog.name(data.name);
        model.dialog.address(data.address);
        model.dialog.ul(data.ul);
        model.dialog.link(data.link);
        model.dialog.logo(data.logo);
        model.dialog.object(data.object);
        model.dialog.mortgage(data.mortgage);
        model.dialog.pageUrl(createUrl(data));

        //map.setModalPoint(data);
        ShowModal('#modal_form-office');

    }


    function createUrl(data) {
        return '/personal/mortgage/new/new/?data=' + $.trim(data.gk) + ', ' + $.trim(data.name);
    }


    function reloadControl() {
        setTimeout(function () {
            $('[data-plugin="selectCity"]').trigger('refresh');
            $('.formstyle').trigger('refresh');

            var title = $('[data-plugin="selectCity"]').data('title');
            $('[data-plugin="selectCity"]').next('.jq-selectbox__select').prepend('<div class="select_title">' + title + '</div>');
        }, 100);
    }

    return {
        init: function () {

            onEvent();

            $.ajaxSetup({cache: false});
            var city = model.listCity();
            var mortgage = model.listMortgage();
            var items = model.items();

            ymaps.ready(function () {
                var options = {
                    city: realtyCityDefaultName
                };

                //установим настройки карты
                map = RealtyMap.init(options);

                //если карта загружена и инициализирована создадим модель
                map.createMap(function () {

                    $.map($.parseJSON(realtyCity), function (item) {
                        city.push(new createCity(item));
                    });

                    $.map($.parseJSON(realtyMortgage), function (item) {
                        mortgage.push(new createMortgage(item));
                    });

                    ko.applyBindings(model);
                    model.listCity.valueHasMutated();
                    model.listMortgage.valueHasMutated();
                    ready = true;

                    map.gotToCity(realtyCityDefaultName);

                    getItems(function (res) {
                        $.map(res.items, function (item) {
                            items.push(new createItem(item));
                        });

                        map.setNewPoints(model.items);
                        model.items.valueHasMutated();
                    });

                    setTimeout(function () {
                        $('[data-plugin="selectCity"]').trigger('refresh');
                        $('.formstyle').trigger('refresh');

                        var title = $('[data-plugin="selectCity"]').data('title');
                        $('[data-plugin="selectCity"]').next('.jq-selectbox__select').prepend('<div class="select_title">' + title + '</div>');
                    }, 100);
                });
            });

        },
    }

})(this, modal, overlay, window.realtyCity);


$(document).ready(function () {
    Realty.init();


    $('.modal_div').on('click', function (e) {
        var $target = $(e.target);

        if (!$target.closest('.modal-dialog').length) {
            console.log('ДА');
            CloseModal();
        }
    });
});