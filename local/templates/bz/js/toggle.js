$(document).ready(function(){

    $('[data-toggle]').each(function(){
        var _this=this,checked=false,offon=false,checkedOff=$(this).data('checkedOff'),checkedOn=$(this).data('checkedOn');
        if($(_this).data('checked')=='Y')
            checked=true;
        if(typeof (checkedOff)!='undefined' && typeof (checkedOn)!='undefined')
            offon=true;
        $(_this).toggles({
            on:checked,
            text: {
                on: '',
                off: ''
            },
            width: 32,
            height: 14
        });
        $(_this).on('toggle', function(e, active) {
            if(active){
                $(_this).parent('.toggle-light-wr').next('.toggle-light-text').addClass('on').removeClass('off');
                $(_this).parent('.toggle-light-wr').prev('.toggle-light-text').removeClass('on').addClass('off');
                if(offon)
                {
                    $(checkedOn).show();
                    $(checkedOff).hide();
                }
            }else{
                $(_this).parent('.toggle-light-wr').next('.toggle-light-text').removeClass('on').addClass('off');
                $(_this).parent('.toggle-light-wr').prev('.toggle-light-text').addClass('on').removeClass('off');
                if(offon)
                {
                    $(checkedOn).hide();
                    $(checkedOff).show();
                }
            }
        });
        $(_this).parent('.toggle-light-wr').next('.toggle-light-text').on('click',function(){
            $(_this).toggles(true);
        });
        $(_this).parent('.toggle-light-wr').prev('.toggle-light-text').on('click',function(){
            $(_this).toggles(false);
        });
    });

});