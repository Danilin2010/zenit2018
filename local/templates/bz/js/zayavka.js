$(document).ready(function(){

    var left_to_step='100%';
    var first_to_step='0px';
    var timer=500;
    var padding=40;

    function MarginResize()
    {
        if($('.wr_form_application').length>0)
        {
            var to_step=$('.to_step');
            var first_step=$('.first_step');
            var position_to_step = $('.wr_form_application').offset();
            var to_step_=$(window).width()-position_to_step.left-padding;
            left_to_step=to_step_+'px';
            first_to_step='-'+(position_to_step.left+first_step.width()-padding)+'px';
            if(!to_step.hasClass('win'))
                to_step.css('margin-left',left_to_step);
            if(first_step.hasClass('win'))
                first_step.css('left',first_to_step);
        }

    }

    setTimeout(function(){MarginResize();},500);

    $(window).resize(function() {
        MarginResize();
    });

    $("form.validate").validate({
        errorPlacement: function(error, element) {
            var parent=element.parents('.wr_complex_input');
            var parentcheckbox=element.parents('.jq-checkbox');
            if(parent.length>0) {
                parent.addClass('error');
                error.insertAfter(parent);
            }else if(parentcheckbox.length>0){
                parentcheckbox.addClass('error');
                error.insertAfter(parentcheckbox);
            }else{
                error.insertAfter(element);
            }
        },
        success: function(error){
            error.prev('.jq-checkbox').removeClass('error');
            error.prev('.wr_complex_input').removeClass('error');
            error.remove();
        },
        rules:{
            surname: {
                required: true
            },
            name: {
                required: true
            },
            patronymic: {
                required: true
            },
            sity: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            agree: {
                required: true
            }
        },
        messages:{
            surname: {
                required: 'Укажите фамилию'
            },
            name: {
                required: 'Укажите имя'
            },
            patronymic: {
                required: 'Укажите отчество'
            },
            sity: {
                required: 'Укажите город'
            },
            phone: {
                required: 'Укажите телефон'
            },
            email: {
                required: 'Укажите e-mail',
                email: 'Укажите правильный e-mail',
            },
            agree: {
                required: 'Нужно согласие'
            }
        },
        submitHandler: function() {
            $('.first_step').addClass('win').animate({'left': first_to_step},timer);
            $('.to_step').addClass('win').animate({'margin-left': "0px"},timer);
            $('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
        }
    });

    $('input[name="email"]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        showMaskOnFocus: false,
        showMaskOnHover: false,
        'placeholder':''
    });

    $('input[name="surname"],input[name="name"],input[name="patronymic"]').inputmask('Regex',
        { regex: "[а-яА-Я]*" }
    );

    $("input[name=phone]").inputmask({
        mask:"+7 (999) 999-99-99",
        showMaskOnHover: false,
        //showMaskOnFocus: false,
        placeholder:'+7 (   )    -  -  '
    });



});
