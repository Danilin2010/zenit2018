$(document).ready(function(){

    $('.form_search').on('submit',function(e){
        e.preventDefault();
        var data=$(this).serialize();
        var action=$(this).attr('action');
        var to=$(this).parents('.wr_js_form').find('.wr_block_search_result');
        $.ajax({
            url: action,
            data: data,
            type: 'get',
            success: function(data){
                to.find('.block_search_result').html(data);
                to.slideDown('fast');
            }
        });
    });

});
