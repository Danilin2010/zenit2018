
		<div class="doc_list">
 			<a href="/upload/medialibrary/858/tariffs_safe_20170405.pdf" title="Тарифы на услуги, предоставляемые ПАО Банк ЗЕНИТ арендаторам индивидуальныхсейфовых ячеек в Головном офисе ПАО Банк ЗЕНИТ" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						Тарифы на услуги, предоставляемые ПАО Банк ЗЕНИТ арендаторам индивидуальных сейфовых ячеек (в Головном офисе и дополнительных офисах Банка, расположенных в Москве и Московской области)
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div>
		<div class="doc_list">
 			<a href="/media/doc/safe/tariffs_safe_20180420.pdf" title="Тарифы на услуги, предоставляемые ПАО Банк ЗЕНИТ арендаторам индивидуальныхсейфовых ячеек в Головном офисе ПАО Банк ЗЕНИТ" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						Тарифы вступающие в действие с 20.04.2018
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div>
		<br />
		<div class="doc_list">
 			<a href="/upload/iblock/53f/tariffs_safe_kursk_20180326.pdf" title="Тарифы на услуги, предоставляемые ПАО Банк ЗЕНИТ арендаторам индивидуальных сейфовых ячеек в Операционном офисе «На Ленина» (г. Курск)" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						Тарифы на услуги, предоставляемые ПАО Банк ЗЕНИТ арендаторам индивидуальных сейфовых ячеек в Операционном офисе «На Ленина» (г. Курск)
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div>
		<div class="doc_list">
 			<a href="/media/doc/safe/tariffs_safe_kursk_20180420.pdf" title="Тарифы на услуги, предоставляемые ПАО Банк ЗЕНИТ арендаторам индивидуальных сейфовых ячеек в Операционном офисе «На Ленина» (г. Курск)" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						Тарифы вступающие в действие с 20.04.2018
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div>
