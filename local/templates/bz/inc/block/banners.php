<div class="banners c-container">
	<div class="wr_banners_item">
		<div class="banners_item">
			<div class="banners_title">
				«Стратегия лидерства»
			</div>
			<div class="banners_text">
				Эффективное вложение средств<br>
				с доходностью до 9,25%
			</div>
			<div class="banners_button">
				<a href="/personal/deposits/leadership-strategy/" class="button min_height">подробнее</a>
			</div>
		</div>
		<div class="banners_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/banners/sl.png');">
		</div>
	</div>
	<div class="wr_banners_item">
		<div class="banners_item">
			<div class="banners_title">
				 Карта с Cash Back
			</div>
			<div class="banners_text">
				Возврат 10% суммы, <br>
				потраченной на развлечения!
			</div>
			<div class="banners_button">
				<a href="/personal/cards/credit-card/credit-card-with-cash-back/" class="button min_height">подробнее</a>
			</div>
		</div>
		<div class="banners_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/banners/card.png');">
		</div>
	</div>
	<div class="wr_banners_item">
		<div class="banners_item">
			<div class="banners_title">
				 Мобильное приложение
			</div>
			<div class="banners_text">
				 Доступ к счетам и картам 24/7<br>
				всегда под рукой
			</div>
			<div class="banners_button">
				<ul class="mobile_banking_line_icon">
					<li><a href="#" class="mobile_banking_line_icon apple">
					<div>
					</div>
					</a></li>
					<li><a href="#" class="mobile_banking_line_icon android">
					<div>
					</div>
					</a></li>
				</ul>
			</div>
		</div>
		<div class="banners_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/banners/003.png');">
		</div>
	</div>
</div>
<br>