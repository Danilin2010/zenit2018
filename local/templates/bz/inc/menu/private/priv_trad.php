<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="priv_trad">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/traditional-banking-products/cash-management-services/">Расчетно-кассовое обслуживание</a></li>
                    <li><a href="/private-banking/traditional-banking-products/documentary-and-international-operations/">Документарные и международные операции</a></li>
                    <li><a href="/private-banking/traditional-banking-products/elite-credit-card/">Элитные банковские карты</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/traditional-banking-products/individual-credit-program/">Индивидуальные кредитные программы</a></li>
                    <li><a href="/private-banking/traditional-banking-products/operations-of-precious-metals/">Операции на рынке драгоценных металлов и обезличенные металлические счета</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/traditional-banking-products/safe-deposit-boxes/">Сейфовые ячейки</a></li>
                    <li><a href="/private-banking/traditional-banking-products/the-collection-of-funds/">Инкассирование денежных средств</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
