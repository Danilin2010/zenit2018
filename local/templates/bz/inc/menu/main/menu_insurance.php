<div class="wr_menu_bottom" id="menu_insurance" style="display: none">
	<div class="menu_bottom z-container">
		<div class="menu_bottom_block">
			<div class="menu_bottom_block_title no_magrin_top">Страхование жизни и здоровья</div>
			<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
				"ADD_SECTIONS_CHAIN" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COUNT_ELEMENTS" => "N",
				"IBLOCK_ID" => "57",
				"IBLOCK_TYPE" => "insurance",
				"SECTION_CODE" => "",
				"SECTION_FIELDS" => array(),
				"SECTION_ID" => "",
				"SECTION_URL" => "/personal/insurance/life/#SECTION_CODE#/",
				"SECTION_USER_FIELDS" => array(),
				"SHOW_PARENT_NAME" => "N",
				"TOP_DEPTH" => "2",
				"VIEW_MODE" => "LINE",
				"CSS_CLASS" => "menu_bottom_block_ul"
			)); ?>
		</div>
		<div class="menu_bottom_block">
			<div class="menu_bottom_block_title no_magrin_top">Страхование имущества</div>
			<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
				"ADD_SECTIONS_CHAIN" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COUNT_ELEMENTS" => "N",
				"IBLOCK_ID" => "58",
				"IBLOCK_TYPE" => "insurance",
				"SECTION_CODE" => "",
				"SECTION_FIELDS" => array(),
				"SECTION_ID" => "",
				"SECTION_URL" => "/personal/insurance/property/#SECTION_CODE#/",
				"SECTION_USER_FIELDS" => array(),
				"SHOW_PARENT_NAME" => "N",
				"TOP_DEPTH" => "2",
				"VIEW_MODE" => "LINE",
				"CSS_CLASS" => "menu_bottom_block_ul"
			)); ?>
		</div>
		<div class="menu_bottom_block"></div>
	</div>
</div>