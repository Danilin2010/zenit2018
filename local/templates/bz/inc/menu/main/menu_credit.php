<div class="wr_menu_bottom wr_burger_menu" id="menu_credit" style="display: none">
	<div class="burger_menu z-container">
		<div class="burger_menu_item">
			<div class="burger_menu_block">
				<div class="menu_bottom_block_title">
					 Потребительское кредитование
				</div>
				<ul class="menu_bottom_block_ul">
					<li><?= \helpers\Helper::a('/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/', 'Потребительский кредит «Специальный»')?></li>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"carloans", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "Y",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "carloans",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "43",
		"IBLOCK_TYPE" => "carloans",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "266",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?>
			</div>
		</div>
		<div class="burger_menu_item">
			<div class="burger_menu_block">
				<div class="menu_bottom_block_title">
					 Автокредитование
				</div>
				<ul class="burger_menu_menu">
					<li><span>Новый автомобиль</span>
						<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"products-selector", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "Y",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "products-selector",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "32",
		"IBLOCK_TYPE" => "carloans",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "255",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?> </li>
	<li><span>Автомобиль с пробегом</span>
		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"products-selector", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "Y",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "products-selector",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "32",
		"IBLOCK_TYPE" => "carloans",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "256",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	),
	false
);?> </li>
					<li><a href="/personal/credits/carloans/dealers">Перечень автосалонов-партнеров Банка</a></li>
				</ul>
			</div>
		</div>
		<div class="burger_menu_item">
			<!--<div class="burger_menu_block">
				<div class="menu_bottom_block_title">
					 Тарифы
				</div>
				<ul class="menu_bottom_block_ul">
					<li><a href="/personal/credits/tariffs">Тарифы и Условия</a></li>
				</ul>
			</div>-->
			<!--div class="burger_menu_block">
				<div class="menu_bottom_block_title">
					 Специальные предложения
				</div>
				<ul class="menu_bottom_block_ul"-->
					<!--li><?= \helpers\Helper::a('/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/', 'Потребительский кредит «Специальный»')?></li-->
					<!--li><?= \helpers\Helper::a('/personal/credits/special-offers/', 'Программа льготного автокредитования')?></li-->
					<!--li><?= \helpers\Helper::a('/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/', 'Потребительский кредит по фиксированной ставке')?>					</li>
					<li><?= \helpers\Helper::a('/personal/credits/carloans/buying-a-new-car-without-a-down-payment_action/', 'Кредит на покупку нового автомобиля')?></li>
					<li><?= \helpers\Helper::a('/personal/credits/carloans/buying-a-used-car-without-a-down-payment_action/', 'Кредит на покупку подержанного автомобиля')?></li-->

				<!--/ul>
			</div><br-->
			<div class="burger_menu_block">
				<div class="menu_bottom_block_title">
					 Рефинансирование
				</div>
				<ul class="menu_bottom_block_ul">
					<li>
						<?= \helpers\Helper::a('/personal/credits/personal-loans/refinancing/', 'Рефинансирование кредитов')?>
					 </li>
					<li>
						<?/*= \helpers\Helper::a('/personal/credits/personal-loans/refinancing/', 'Рефинансирование кредитов')*/?>
					 </li>
					<li>
						<?/*= \helpers\Helper::a('/personal/credits/personal-loans/refinancing/', 'Рефинансирование кредитов')*/?>
					 </li>
				</ul>
				<br>
				<ul class="menu_bottom_block_ul">
					<li class="contribution">
						<a href="/personal/tatneft/#content-tabs-3">Специальные предложения для сотрудников ГК «ТАТНЕФТЬ»</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>