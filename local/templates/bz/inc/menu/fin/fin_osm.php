<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="fin_osm">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/trading-operations/">Торговые операции</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/repo-transactions-with-securities/">Операции РЕПО с ценными бумагами</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
					<li><span>Депозитарные операции</span>
                        <ul>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-information/">Информация о депозитарии</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-services/">Депозитарные услуги</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-rates/">Тарифы депозитария</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-documents/">Документы депозитария</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-securities/">Обслуживаемые ценные бумаги</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-operations/">Проведение операций</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/depositary-edoc/">Электронный документооборот</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/depositary-operations/registrars-and-depositaries/">Перечень регистраторов и депозитариев</a>
                            </li>
                        </ul>
                    </li>
                    <!--<li><a href="/corp-and-fin/operations-on-the-securities-market/custody-services/">Депозитарное обслуживание</a></li>
                    <li><a href="https://old.zenit.ru/rus/investment-banking/depositary-operations/">Депозитарное обслуживание</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/brokerage-service/">Брокерское обслуживание</a></li>-->
					<li><span>Брокерское обслуживание</span>
                        <ul>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/about-the-service/">Об услуге</a>         
                            </li>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/one-account/">Единый торговый счет</a>         
                            </li>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/repo-transactions/">Сделки РЕПО с ценными бумагами</a>         
                            </li>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/margin-trading/">Маржинальная торговля</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/trading-systems/">Торговые системы</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/rates/">Тарифы</a>
                            </li>
                            <li>
                                <a href="/corp-and-fin/brokerage-service/documents/">Документы</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

        </div>
    </div>
</div>
