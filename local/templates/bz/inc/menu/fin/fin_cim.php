<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="fin_cim">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/conversion-and-interbank-markets/operations-with-derivative-financial-instruments/">Операции с ПФИ</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/conversion-and-interbank-markets/conversion-operations/">Конверсионные операции</a></li>
                </ul>
            </div>




        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Организация проектов по привлечению долгового финансирования</span>
                        <ul>
                            <li><a href="/corp-and-fin/organization-of-debt-financing/bonded-loan-issue/">Выпуск облигационного займа</a></li>
                            <li><a href="/corp-and-fin/organization-of-debt-financing/syndicated-lending/">Синдицированное кредитование</a></li>
                            <li><a href="/corp-and-fin/organization-of-debt-financing/support-issuers/">Поддержка эмитентов при прохождении оферт, погашения долга.</a></li>
                            <li><a href="/corp-and-fin/organization-of-debt-financing/promissory-notes-program/">Выпуск вексельной программы</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>
