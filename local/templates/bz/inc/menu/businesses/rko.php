<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_rko">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Открытие и ведение счета</span>
                        <ul>
                            <!--li><a href="https://old.zenit.ru/rus/businesss/operations/reserve_account/">Онлайн-резервирование номера счета</a></li-->
							<li><a href="/businesses/rko/account/opening-of-accounts/">Открытие счетов</a></li>
                            <?/*<li><a href="#">Документы для открытия счета</a></li>*/?>
                        </ul>
                    </li>
                    <li><span>Тарифные планы</span>
                        <ul>
                            <li><a href="/businesses/rko/tariff-plans/main-tariff-plans/">Основные тарифные планы</a></li>
                            <!--<li><a href="/businesses/rko/tariff-plans/tariff-plan-importer/">ТП Импортер</a></li>-->
                            <?/*<li><a href="#">Подобрать тарифный план (калькулятор)</a></li>*/?>
                        </ul>
                    </li>
                    <li><span>Безналичные переводы</span>
                        <ul>
                            <li><a href="/businesses/rko/transfers/rub/">Переводы в рублях</a></li>
                            <li><a href="/businesses/rko/transfers/currency/">Переводы в иностранной валюте</a></li>
                            <!--<li><a href="/businesses/rko/transfers/time/">Операционное время</a></li>-->
                        </ul>
                    </li>
                    <li><span>Дистанционное обслуживание</span>
                        <ul>
                            <li><a href="/businesses/rko/remote/clientbank/">Система Клиент-Банк</a></li>
                            <li><a href="/businesses/rko/remote/1c/">Клиент-Банк из 1С: Предприятие</a></li>
                            <li><a href="/businesses/rko/remote/mobail/">Мобильный банк</a></li>
                            <li><a href="/businesses/rko/remote/sms/">SMS - информирование</a></li>
                            <li><a href="/businesses/rko/remote/indicator/">Индикатор</a></li>
                            <li><a href="/businesses/rko/remote/e-learning-video-courses/">Электронные обучающие видеокурсы</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div><div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Банковские карты</span>
                        <ul>
                            <li><a href="/businesses/rko/bank-card/customs-card/">Таможенная карта</a></li>
                            <li><a href="/businesses/rko/bank-card/corporate-cards/">Корпоративные карты</a></li>
                            <li><a href="/businesses/rko/bank-card/salary-project/">Зарплатный проект</a></li>
                        </ul>
                    </li>
                    <li><span>Эквайринг</span>
                        <ul>
                            <li><a href="/businesses/rko/acquiring/trade-aquaring/">Торговый эквайринг</a></li>
                        </ul>
                    </li>
                    <li><a href="/businesses/rko/collection-and-delivery-of-cash/">Инкассация и доставка наличных</a></li>
					<li><span>Валютный контроль</span>
						<ul>
							<li><a href="/businesses/rko/currency-control/description-and-documents/">Описание и документы</a></li>
							<li><a href="/businesses/rko/currency-control/violation-of-currency-legislation/">Нарушения валютного законодательства</a></li>
						</ul>
					</li>
                </ul>
            </div>
        </div><div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Дополнительные продукты</span>
                        <ul>
                            <li><a href="/businesses/rko/additional/check/">Прием чеков на инкассо</a></li>
                            <li><a href="/businesses/rko/additional/cells/">Сейфовые ячейки</a></li>
                        </ul>
                    </li>
                    <li><span>Специальные предложения и акции</span>
                        <ul>
                            <li><a href="/businesses/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks/">Льготные тарифы для клиентов проблемных банков</a></li>
                            <li><a href="/businesses/rko/special-offers-and-promotions/promotions/">Акции</a></li>
							<li><a href="/businesses/rko/partner-offers">Предложения партнеров</a></li>
                        </ul>
                    </li>
					<li><a href="/businesses/rko/rates/">Тарифы</a></li>
                    <!--<li><span>Тарифы</span>
                        <ul>
                            <li><a href="/businesses/rko/rates/collection-of-tariffs/">Сборник тарифов</a></li>
                        </ul>
                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</div>

