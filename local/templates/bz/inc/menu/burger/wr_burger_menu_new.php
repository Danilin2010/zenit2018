<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="burger_menu">
	<div class="burger_menu z-container">
		<div class="burger_menu_item">
			<div class="burger_menu_title">Ипотека</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Новостройки</span>
						<?$APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"ADD_SECTIONS_CHAIN" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"DISPLAY_TOP_PAGER" => "N",
								"FIELD_CODE" => array(0=>"",1=>"",),
								"FILTER_NAME" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"IBLOCK_ID" => "6",
								"IBLOCK_TYPE" => "mortgage",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"INCLUDE_SUBSECTIONS" => "N",
								"MESSAGE_404" => "",
								"NEWS_COUNT" => "20",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Новости",
								"PARENT_SECTION" => "3",
								"PARENT_SECTION_CODE" => "",
								"PREVIEW_TRUNCATE_LEN" => "",
								"PROPERTY_CODE" => array(0=>"",1=>"",),
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SORT_BY1" => "SORT",
								"SORT_BY2" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_ORDER2" => "ASC",
								"STRICT_SECTION_CHECK" => "N"
							)
						);?>
					</li>

					<li><span>Военная ипотека</span>
						<ul class="burger_menu_menu">
							<li><span>Новостройки</span>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"simple_menu",
									Array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ADD_SECTIONS_CHAIN" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"COMPONENT_TEMPLATE" => "carloans",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_DATE" => "N",
										"DISPLAY_NAME" => "N",
										"DISPLAY_PICTURE" => "N",
										"DISPLAY_PREVIEW_TEXT" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"FIELD_CODE" => array(0=>"",1=>"",),
										"FILTER_NAME" => "",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"IBLOCK_ID" => "6",
										"IBLOCK_TYPE" => "mortgage",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"INCLUDE_SUBSECTIONS" => "N",
										"MESSAGE_404" => "",
										"NEWS_COUNT" => "20",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Новости",
										"PARENT_SECTION" => "257",
										"PARENT_SECTION_CODE" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"PROPERTY_CODE" => array(0=>"",1=>"",),
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SORT_BY1" => "SORT",
										"SORT_BY2" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_ORDER2" => "ASC",
										"STRICT_SECTION_CHECK" => "N"
									)
								);?>
							</li>
							<li><span>Вторичное жилье</span>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"simple_menu",
									Array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ADD_SECTIONS_CHAIN" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"COMPONENT_TEMPLATE" => "carloans",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_DATE" => "N",
										"DISPLAY_NAME" => "N",
										"DISPLAY_PICTURE" => "N",
										"DISPLAY_PREVIEW_TEXT" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"FIELD_CODE" => array(0=>"",1=>"",),
										"FILTER_NAME" => "",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"IBLOCK_ID" => "6",
										"IBLOCK_TYPE" => "mortgage",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"INCLUDE_SUBSECTIONS" => "N",
										"MESSAGE_404" => "",
										"NEWS_COUNT" => "20",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Новости",
										"PARENT_SECTION" => "258",
										"PARENT_SECTION_CODE" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"PROPERTY_CODE" => array(0=>"",1=>"",),
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SORT_BY1" => "SORT",
										"SORT_BY2" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_ORDER2" => "ASC",
										"STRICT_SECTION_CHECK" => "N"
									)
								);?>
							</li>
							<li><span>Рефинансирование военной ипотеки</span>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"simple_menu",
									Array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ADD_SECTIONS_CHAIN" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"COMPONENT_TEMPLATE" => "carloans",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_DATE" => "N",
										"DISPLAY_NAME" => "N",
										"DISPLAY_PICTURE" => "N",
										"DISPLAY_PREVIEW_TEXT" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"FIELD_CODE" => array(0=>"",1=>"",),
										"FILTER_NAME" => "",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"IBLOCK_ID" => "6",
										"IBLOCK_TYPE" => "mortgage",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"INCLUDE_SUBSECTIONS" => "N",
										"MESSAGE_404" => "",
										"NEWS_COUNT" => "20",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Новости",
										"PARENT_SECTION" => "283",
										"PARENT_SECTION_CODE" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"PROPERTY_CODE" => array(0=>"",1=>"",),
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SORT_BY1" => "SORT",
										"SORT_BY2" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_ORDER2" => "ASC",
										"STRICT_SECTION_CHECK" => "N"
									)
								);?>
							</li>
						</ul>
					</li>

					<li><span>Вторичное жилье</span>
						<?$APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"ADD_SECTIONS_CHAIN" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"CHECK_DATES" => "Y",
								"COMPONENT_TEMPLATE" => "carloans",
								"DETAIL_URL" => "",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"DISPLAY_TOP_PAGER" => "N",
								"FIELD_CODE" => array(0=>"",1=>"",),
								"FILTER_NAME" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"IBLOCK_ID" => "6",
								"IBLOCK_TYPE" => "mortgage",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"INCLUDE_SUBSECTIONS" => "N",
								"MESSAGE_404" => "",
								"NEWS_COUNT" => "20",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Новости",
								"PARENT_SECTION" => "4",
								"PARENT_SECTION_CODE" => "",
								"PREVIEW_TRUNCATE_LEN" => "",
								"PROPERTY_CODE" => array(0=>"",1=>"",),
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SORT_BY1" => "SORT",
								"SORT_BY2" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_ORDER2" => "ASC",
								"STRICT_SECTION_CHECK" => "N"
							)
						);?>
					</li>

					<li><span>Дом с землей</span>
						<?$APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"ADD_SECTIONS_CHAIN" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"CHECK_DATES" => "Y",
								"COMPONENT_TEMPLATE" => "carloans",
								"DETAIL_URL" => "",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"DISPLAY_TOP_PAGER" => "N",
								"FIELD_CODE" => array(0=>"",1=>"",),
								"FILTER_NAME" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"IBLOCK_ID" => "6",
								"IBLOCK_TYPE" => "mortgage",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"INCLUDE_SUBSECTIONS" => "N",
								"MESSAGE_404" => "",
								"NEWS_COUNT" => "20",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Новости",
								"PARENT_SECTION" => "7",
								"PARENT_SECTION_CODE" => "",
								"PREVIEW_TRUNCATE_LEN" => "",
								"PROPERTY_CODE" => array(0=>"",1=>"",),
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SORT_BY1" => "SORT",
								"SORT_BY2" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_ORDER2" => "ASC",
								"STRICT_SECTION_CHECK" => "N"
							)
						);?>
					</li>

					<li><span>Специальные предложения</span>
						<ul class="burger_menu_menu">
							<li><a href="/personal/mortgage/stock/sampo_action/">Кредит на приобретение объектов недвижимости в Жилом комплексе «SAMPO»</a></li>
							<li><a href="/personal/mortgage/special-offers/">Акция «Ипотека по двум документам»</a></li>
							<li><a href="/personal/mortgage/mortgage-refinancing/">Рефинансирование ипотеки</a></li>
							<li><a href="/personal/mortgage/family-mortgage/">Семейная ипотека</a></li>
							<li><a href="/personal/tatneft/#content-tabs-2">Специальные предложения для сотрудников ГК «ТАТНЕФТЬ»</a></li>
						</ul>
					</li>
				<li><a href="/personal/mortgage/documents-and-supporting-information/">Документы и дополнительная информация</a></li>
				</ul>
			</div>
		</div>

		<div class="burger_menu_item">
			<div class="burger_menu_title">Кредиты</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Потребительское кредитование</span>
						<?$APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							Array(
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"ADD_SECTIONS_CHAIN" => "N",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"CACHE_TIME" => "3600",
								"CACHE_TYPE" => "A",
								"CHECK_DATES" => "Y",
								"COMPONENT_TEMPLATE" => "carloans",
								"DETAIL_URL" => "",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"DISPLAY_TOP_PAGER" => "N",
								"FIELD_CODE" => array(0=>"",1=>"",),
								"FILTER_NAME" => "",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"IBLOCK_ID" => "43",
								"IBLOCK_TYPE" => "carloans",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"INCLUDE_SUBSECTIONS" => "N",
								"MESSAGE_404" => "",
								"NEWS_COUNT" => "4",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_TEMPLATE" => ".default",
								"PAGER_TITLE" => "Новости",
								"PARENT_SECTION" => "266",
								"PARENT_SECTION_CODE" => "",
								"PREVIEW_TRUNCATE_LEN" => "",
								"PROPERTY_CODE" => array(0=>"",1=>"",),
								"SET_BROWSER_TITLE" => "N",
								"SET_LAST_MODIFIED" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_STATUS_404" => "N",
								"SET_TITLE" => "N",
								"SHOW_404" => "N",
								"SORT_BY1" => "SORT",
								"SORT_BY2" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_ORDER2" => "ASC",
								"STRICT_SECTION_CHECK" => "N"
							)
						);?>
					</li>

					<li><span>Рефинансирование</span>
						<ul class="burger_menu_menu">
							<li><a href="/personal/credits/personal-loans/refinancing/">Рефинансирование кредитов</a> </li>
						</ul>
					</li>

					<li><span> Автокредитование</span>
						<ul class="burger_menu_menu">
							<li><span>Новый автомобиль</span>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"simple_menu",
									Array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ADD_SECTIONS_CHAIN" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"COMPONENT_TEMPLATE" => "carloans",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_DATE" => "N",
										"DISPLAY_NAME" => "N",
										"DISPLAY_PICTURE" => "N",
										"DISPLAY_PREVIEW_TEXT" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"FIELD_CODE" => array(0=>"",1=>"",),
										"FILTER_NAME" => "",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"IBLOCK_ID" => "32",
										"IBLOCK_TYPE" => "carloans",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"INCLUDE_SUBSECTIONS" => "N",
										"MESSAGE_404" => "",
										"NEWS_COUNT" => "20",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Новости",
										"PARENT_SECTION" => "255",
										"PARENT_SECTION_CODE" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"PROPERTY_CODE" => array(0=>"",1=>"",),
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SORT_BY1" => "SORT",
										"SORT_BY2" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_ORDER2" => "ASC",
										"STRICT_SECTION_CHECK" => "N"
									)
								);?>
							</li>
							<li><span>Автомобиль с пробегом</span>
								<?$APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"simple_menu",
									Array(
										"ACTIVE_DATE_FORMAT" => "d.m.Y",
										"ADD_SECTIONS_CHAIN" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"COMPONENT_TEMPLATE" => "carloans",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_DATE" => "N",
										"DISPLAY_NAME" => "N",
										"DISPLAY_PICTURE" => "N",
										"DISPLAY_PREVIEW_TEXT" => "N",
										"DISPLAY_TOP_PAGER" => "N",
										"FIELD_CODE" => array(0=>"",1=>"",),
										"FILTER_NAME" => "",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"IBLOCK_ID" => "32",
										"IBLOCK_TYPE" => "carloans",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"INCLUDE_SUBSECTIONS" => "N",
										"MESSAGE_404" => "",
										"NEWS_COUNT" => "20",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Новости",
										"PARENT_SECTION" => "256",
										"PARENT_SECTION_CODE" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"PROPERTY_CODE" => array(0=>"",1=>"",),
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SORT_BY1" => "SORT",
										"SORT_BY2" => "SORT",
										"SORT_ORDER1" => "ASC",
										"SORT_ORDER2" => "ASC",
										"STRICT_SECTION_CHECK" => "N"
									)
								);?> </li>
						</ul>
					</li>

					<li><span> Специальные предложения</span>
						<ul class="burger_menu_menu">
							<!--li>
								<?= \helpers\Helper::a(
									'/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/',
									'Потребительский кредит по фиксированной ставке'
								);?>
							</li>
							<li>
								<?= \helpers\Helper::a(
									'/personal/credits/carloans/buying-a-new-car-without-a-down-payment_action/',
									'Кредит на покупку нового автомобиля'
								);?>
							</li-->
							<!--li>
								<?= \helpers\Helper::a(
									'/personal/credits/carloans/buying-a-used-car-without-a-down-payment_action/',
									'Кредит на покупку подержанного автомобиля'
								);?>
							</li-->
							<li>
								<?= \helpers\Helper::a(
									'/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/',
									'Потребительский кредит «Специальный»'
								);?>
							</li>
							<li>
								<?= \helpers\Helper::a(
									'/personal/tatneft/#content-tabs-3',
									'Специальные предложения для сотрудников ГК «ТАТНЕФТЬ»'
								);?>
							</li>

						</ul>
					</li>
				</ul>
			</div>
		</div>

		<div class="burger_menu_item">
			<div class="burger_menu_title">Карты</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Кредитные карты</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "credit_cards",
								"IBLOCK_ID" => "29",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "1",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>

					<li><span>Дебетовые карты</span>
					<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "credit_cards",
								"IBLOCK_ID" => "29",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "2",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>
					<li>
						<span>Платежные сервисы</span>
						<ul>
							<li>
								<a href="/personal/cards/card-payments/samsung-pay/">Samsung Pay</a>
							</li>
						</ul>
					</li>

					<li>
						<span>Банки-партнеры</span>
						<ul>
							<li>
								<a href="/personal/cards/partner-banks/">Список банков-партнеров</a>
							</li>
						</ul>
					</li>

					<li>
						<span>Акции</span>
						<ul>
							<li>
								<a href="/personal/tatneft/#content-tabs-1">Специальные предложения для сотрудников ГК «ТАТНЕФТЬ»</a>
							</li>
							<li>
								<a href="/promo/visa/">Команда болельщиков Visa</a>
							</li>
						</ul>
					</li>


					<!--<li><span>Предоплаченные карты</span>-->
					<?/* $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "credit_cards",
								"IBLOCK_ID" => "29",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "252",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
); */?>
					<!--</li>-->
				</ul>
			</div>
		</div>

		<div class="burger_menu_item">
			<div class="burger_menu_title">Вклады</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Максимальный доход</span>
						<? $APPLICATION->IncludeComponent("bitrix:news.list", "simple_menu", array(
							"COMPONENT_TEMPLATE" => "carloans",
							"IBLOCK_TYPE" => "deposits",
							"IBLOCK_ID" => "49",
							"NEWS_COUNT" => "20",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "ASC",
							"SORT_BY2" => "SORT",
							"SORT_ORDER2" => "ASC",
							"FILTER_NAME" => "carloans_top_menu",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"PROPERTY_CODE" => array(
								0 => "",
								1 => "SHARE_CONTRIBUTION",
								3 => "",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION" => "253",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "N",
							"STRICT_SECTION_CHECK" => "N",
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "N",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "Новости",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SET_STATUS_404" => "N",
							"SHOW_404" => "N",
							"MESSAGE_404" => ""
						), false); ?>
					</li>

					<li><span>Накопление</span>
						<? $APPLICATION->IncludeComponent("bitrix:news.list", "simple_menu", array(
							"COMPONENT_TEMPLATE" => "carloans",
							"IBLOCK_TYPE" => "deposits",
							"IBLOCK_ID" => "49",
							"NEWS_COUNT" => "20",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "ASC",
							"SORT_BY2" => "SORT",
							"SORT_ORDER2" => "ASC",
							"FILTER_NAME" => "",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"PROPERTY_CODE" => array(
								0 => "",
								1 => "SHARE_CONTRIBUTION",
								3 => "",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION" => "9",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "N",
							"STRICT_SECTION_CHECK" => "N",
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "N",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "Новости",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SET_STATUS_404" => "N",
							"SHOW_404" => "N",
							"MESSAGE_404" => ""
						), false); ?>
					</li>

					<li><span>Максимальная гибкость</span>
						<? $APPLICATION->IncludeComponent("bitrix:news.list", "simple_menu", array(
							"COMPONENT_TEMPLATE" => "carloans",
							"IBLOCK_TYPE" => "deposits",
							"IBLOCK_ID" => "49",
							"NEWS_COUNT" => "20",
							"SORT_BY1" => "SORT",
							"SORT_ORDER1" => "ASC",
							"SORT_BY2" => "SORT",
							"SORT_ORDER2" => "ASC",
							"FILTER_NAME" => "",
							"FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"PROPERTY_CODE" => array(
								0 => "",
								1 => "SHARE_CONTRIBUTION",
								3 => "",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "3600",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION" => "254",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "N",
							"STRICT_SECTION_CHECK" => "N",
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "N",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "Новости",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SET_STATUS_404" => "N",
							"SHOW_404" => "N",
							"MESSAGE_404" => ""
						), false); ?>
					</li>

					<li><span>Акции</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "deposits",
								"IBLOCK_ID" => "49",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "SHARE_CONTRIBUTION",
									3 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "267",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>
				</ul>
			</div>
		</div>

		<div class="burger_menu_item">
			<div class="burger_menu_title">Платежи и переводы</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><a href="/personal/payments/payment">Оплата услуг</a></li>
					<li><span>Переводы</span>
						<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "N",
							"COUNT_ELEMENTS" => "N",
							"IBLOCK_ID" => "56",
							"IBLOCK_TYPE" => "payments",
							"SECTION_CODE" => "",
							"SECTION_FIELDS" => array(),
							"SECTION_ID" => "",
							"SECTION_URL" => "/personal/payments/transfers/#SECTION_CODE#/",
							"SECTION_USER_FIELDS" => array(),
							"SHOW_PARENT_NAME" => "N",
							"TOP_DEPTH" => "2",
							"VIEW_MODE" => "LINE",
							"CSS_CLASS" => ""
						)); ?>
					</li>
				</ul>
			</div>
		</div>
		<div class="burger_menu_item">
			<div class="burger_menu_title">Страхование</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Страхование жизни и здоровья</span>
						<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "N",
							"COUNT_ELEMENTS" => "N",
							"IBLOCK_ID" => "57",
							"IBLOCK_TYPE" => "insurance",
							"SECTION_CODE" => "",
							"SECTION_FIELDS" => array(),
							"SECTION_ID" => "",
							"SECTION_URL" => "/personal/insurance/life/#SECTION_CODE#/",
							"SECTION_USER_FIELDS" => array(),
							"SHOW_PARENT_NAME" => "N",
							"TOP_DEPTH" => "2",
							"VIEW_MODE" => "LINE",
							"CSS_CLASS" => ""
						)); ?>
					</li>
					<li><span>Страхование имущества</span>
						<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "N",
							"COUNT_ELEMENTS" => "N",
							"IBLOCK_ID" => "58",
							"IBLOCK_TYPE" => "insurance",
							"SECTION_CODE" => "",
							"SECTION_FIELDS" => array(),
							"SECTION_ID" => "",
							"SECTION_URL" => "/personal/insurance/property/#SECTION_CODE#/",
							"SECTION_USER_FIELDS" => array(),
							"SHOW_PARENT_NAME" => "N",
							"TOP_DEPTH" => "2",
							"VIEW_MODE" => "LINE",
							"CSS_CLASS" => ""
						)); ?>
					</li>
				</ul>
			</div>
		</div>
        <div class="burger_menu_item">
            <div class="burger_menu_title">Пенсионное обеспечение</div>
            <div class="burger_menu_block">
                <? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "N",
                    "COUNT_ELEMENTS" => "N",
                    "IBLOCK_ID" => "59",
                    "IBLOCK_TYPE" => "pension",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array(),
                    "SECTION_ID" => "",
                    "SECTION_URL" => "/personal/pension/#SECTION_CODE#/",
                    "SECTION_USER_FIELDS" => array(),
                    "SHOW_PARENT_NAME" => "N",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "LINE",
                    "CSS_CLASS" => "burger_menu_menu"
                )); ?>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_title">Дистанционное банковское обслуживание</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li>
						<?= \helpers\Helper::a(
							'/personal/remote-service/internet-bank/',
							'Интернет-банк и мобильное приложение<br> «ЗЕНИТ Онлайн»'
						);?>
				</ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_title">Индивидуальные сейфы</div>
            <div class="burger_menu_block">
               <ul class="burger_menu_menu">
					<li>
						<?= \helpers\Helper::a(
						'/personal/safes/safe-deposit-box-rental/',
						'Аренда сейфовых ячеек'
						);?>
					</li>
					<li>
						<?= \helpers\Helper::a(
						'/personal/safes/storing-cash-in-safe-deposit-boxes/',
						'Хранение денежных средств в сейфовых ячейках'
						);?>
					</li>
				</ul>
				<?/* $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "N",
                    "COUNT_ELEMENTS" => "N",
                    "IBLOCK_ID" => "60",
                    "IBLOCK_TYPE" => "safes",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array(),
                    "SECTION_ID" => "",
                    "SECTION_URL" => "/personal/safes/#SECTION_CODE#/",
                    "SECTION_USER_FIELDS" => array(),
                    "SHOW_PARENT_NAME" => "N",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "LINE",
                    "CSS_CLASS" => "burger_menu_menu"
					)); */?>
            </div>
        </div>
		<div class="burger_menu_item">
            <div class="burger_menu_title">Инвестиционные услуги</div>
            <div class="burger_menu_block">
               <ul class="burger_menu_menu">
					<li>
						<a href="http://invest.zenit.ru/" target="_blank">ОФБУ (Общие фонды банковского управления)</a>
					</li>
					<li>
						<?= \helpers\Helper::a(
						'/personal/invest/individual-trust-management.php',
						'Индивидуальное доверительное управление'
						);?>
					</li>
					<li>
						<?= \helpers\Helper::a(
						'/personal/invest/individual-investment-accounts.php',
						'Индивидуальные инвестиционные счета'
						);?>
					</li>
					<li><span>Брокерское обслуживание</span>
						<ul>
							<li>
								<a href="/personal/brokerage-service/about-the-service/">Об услуге</a> 		
							</li>
							<li>
								<a href="/personal/brokerage-service/one-account/">Единый торговый счет</a> 		
							</li>
							<li>
								<a href="/personal/brokerage-service/repo-transactions/">Сделки РЕПО с ценными бумагами</a> 		
							</li>
							<li>
								<a href="/personal/brokerage-service/margin-trading/">Маржинальная торговля</a>
							</li>
							<li>
								<a href="/personal/brokerage-service/trading-systems/">Торговые системы</a>
							</li>
							<li>
								<a href="/personal/brokerage-service/rates/">Тарифы</a>
							</li>
							<li>
								<a href="/personal/brokerage-service/documents/">Документы</a>
							</li>
						</ul>
					</li>
					<li><span>Депозитарные операции</span>
						<ul>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-information/">Информация о депозитарии</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-services/">Депозитарные услуги</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-rates/">Тарифы депозитария</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-documents/">Документы депозитария</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-securities/">Обслуживаемые ценные бумаги</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-operations/">Проведение операций</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/depositary-edoc/">Электронный документооборот</a>
							</li>
							<li>
								<a href="/personal/invest/depositary-operations/registrars-and-depositaries/">Перечень регистраторов и депозитариев</a>
							</li>
						</ul>
					</li>
				</ul>
            </div>
        </div>

        <div class="burger_menu_item">
            <div class="burger_menu_title">Тарифы</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li>
						<?= \helpers\Helper::a(
							'/personal/tariffs/',
							'Тарифы для частных лиц'
						);?>
					</li>
                </ul>
            </div>
        </div>
		<div class="burger_menu_item">
            <div class="burger_menu_title">Налоговый вычет</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li>
						<?= \helpers\Helper::a(
							'/personal/tax-deduction/',
							'Налоговый вычет под ключ'
						);?>
					</li>
                </ul>
            </div>

        </div>


	</div>
</div>