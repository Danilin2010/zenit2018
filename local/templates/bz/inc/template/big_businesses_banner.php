<div class="wr_footer_banner wr_footer_big_banner_businesses">
    <div class="footer_banner z-container">
        <div class="footer_banner_left">
            <!--<div class="footer_banner_title">
                Депозиты для бизнеса
            </div>
            <div class="footer_banner_text">
                Вкладывай свою прибыль<br/>
                в дальнейшее развитие компании
            </div>-->
            <div class="footer_banner_title">
                Депозитарное обслуживание
            </div>
            <div class="footer_banner_text">
                Воспользуйтесь услугами <br/>
               одного из крупнейших депозитариев
            </div>
            <a class="button" href="/corp-and-fin/depositary-operations/depositary-information/">подробнее</a>
        </div>
    </div>
</div>