<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Перечень регистраторов и депозитариев, в том числе иностранных, в которых ПАО Банк ЗЕНИТ открыты лицевые счета (счета депо) номинального держателя</h3>
				<ul class="big_list">
					<li>Небанковская кредитная организация акционерное общество "Национальный расчетный депозитарий"</li>
					<li>Общество с ограниченной ответственностью "Евроазиатский Регистратор"</li>
					<li>Акционерное общество "Агентство "Региональный независимый регистратор"</li>
					<li>Clearstream Banking S.A. Luxembourg</li>
					<li>Акционерное общество "РЕЕСТР"</li>
					<li>Акционерное общество "Независимая регистраторская компания"</li>
					<li>Акционерное общество "Регистратор Р.О.С.Т."</li>
					<li>Акционерное общество Инвестиционно-финансовая компания "СОЛИД"</li>
					<li>Общество с ограниченной ответственностью "Московский Фондовый Центр"</li>
					<li>Акционерное общество "Регистраторское общество "СТАТУС"</li>
					<li>Закрытое акционерное общество "Первый Специализированный Депозитарий"</li>
					<li>Акционерное общество "Специализированный регистратор - Держатель реестра акционеров газовой промышленности"</li>
					<li>Закрытое акционерное общество "Профессиональный регистрационный центр"</li>
					<li>Общество с ограниченной ответственностью "Специализированная депозитарная компания "Гарант"</li>
					<li>Акционерное общество "Межрегиональный регистраторский центр"</li>
					<li>Акционерное общество "Новый регистратор"</li>
					<li>Закрытое Акционерное Общество "РДЦ ПАРИТЕТ"</li>
					<li>Общество с ограниченной ответственностью "Оборонрегистр"</li>
					<li>Акционерное общество "Индустрия-РЕЕСТР"</li>
					<li>Sanne Trust Company Limited</li>
				</ul>
			</div>
		</div>
	</div>
</div>