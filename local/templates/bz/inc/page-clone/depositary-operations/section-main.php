<?    
    require ("_path.php");
    $path = $APPLICATION->GetCurPage();
    $url = Href($path);
?>

	<div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <!--ipoteka_list-->
                <div class="ipoteka_list">
                    <div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-information/" ?>" class="ipoteka_item_title">Информация о депозитарии</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-services/" ?>" class="ipoteka_item_title">Депозитарные услуги</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-rates/" ?>" class="ipoteka_item_title">Тарифы депозитария</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-documents/" ?>" class="ipoteka_item_title">Документы депозитария</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-securities/" ?>" class="ipoteka_item_title">Обслуживаемые ценные бумаги</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-operations/" ?>" class="ipoteka_item_title">Проведение операций</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-edoc/" ?>" class="ipoteka_item_title">Электронный документооборот</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/depositary-faq/" ?>" class="ipoteka_item_title">Часто задаваемые вопросы</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
							<a href="<? echo $url."/depositary-operations/registrars-and-depositaries/" ?>" class="ipoteka_item_title">Перечень регистраторов и депозитариев</a>
                            <div class="ipoteka_item_text"></div>
                        </div>
                    </div>
                </div>
                <!--ipoteka_list-->
            </div>
        </div>
    </div>