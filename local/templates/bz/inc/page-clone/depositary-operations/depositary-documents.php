<div class="wr_block_type">
    <div class="block_type to_column c-container">
        <div class="block_type_right">
			<div class="form_application_line">
                <a href="/personal/invest/depositary-operations/depositary-archive/" class="button mt-3">Архив документов</a>
            </div>
			<?$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
				Array(),
				Array("MODE"=>"txt","SHOW_BORDER"=>false)
			);?>

        </div>
        <div class="block_type_center">
            <div class="text_block">
                <p>С 01 января 2015 г. изменен порядок налогообложения доходов иностранных организаций.</p>
                <p><a href="/upload/depositary/letter_confirmation_20150119.pdf" target="_blank">Информационное письмо</a> о необходимости предоставления иностранными организациями подтверждения фактического права на получение доходов.</p>
                <p><a href="/upload/depositary/confirmation_form.doc" target="_blank">Рекомендуемая форма</a> подтверждения фактического права на получение доходов.</p>
                <p>С 01 августа 2017 г. вступает в действие новая редакция Условий осуществления депозитарной деятельности ПАО Банк ЗЕНИТ.</p>
                <p><a href="/upload/depositary/letter_confirmation_20170801.pdf" target="_blank">Информационное письмо</a> о внесении изменений в Условия осуществления депозитарной деятельности ПАО Банк ЗЕНИТ</p>
                <h4>Условия осуществления депозитарной деятельности</h4>
                <ul>
                    <li><a href="/upload/depositary/CL_RGLMNT_20170801.pdf" target="_blank">Действующие условия</a>&nbsp;(c 01.08.2017 г.)</li>
                </ul>
                <h4>Комплект документов для открытия счета депо</h4>
                <ul class="big_list">
                    <li><a href="/upload/depositary/open_acc_fiz.rar" target="_blank">Комплект документов для физических лиц</a></li>
                    <li><a href="/upload/depositary/open_acc_org_201801.rar" target="_blank">Комплект документов для юридических лиц</a></li>
                    <li><a href="/upload/depositary/open_acc_ip_082017.rar" target="_blank">Комплект документов для ИП/ЛЧП</a></li>
                </ul>
                <h4>Формы договоров и&nbsp;соглашений</h4>
                <ul class="big_list">
                    <li>Форма 04. <a href="/upload/depositary/6_21_DEPO_APPLICATION_UR.doc" target="_blank">Заявление о заключении договора о депозитарном обслуживании для юридических лиц</a></li>
                    <li>Форма 05. <a href="/upload/depositary/6_22_DEPO_APPLICATION_FIZ.doc" target="_blank">Заявление о заключении договора о депозитарном обслуживании для физических лиц</a></li>
                    <li><a href="/upload/depositary/15_DEPO_SERVICE_2011.doc" target="_blank">Договор об&nbsp;оказании услуг</a></li>
                    <li><a href="/upload/depositary/agreement_dtr.doc" target="_blank">Соглашение (держательская схема, депонент юридическое лицо) ДТР</a></li>
                    <li><a href="/upload/depositary/agreement_dtr_fz.doc" target="_blank">Соглашение (держательская схема, депонент физическое лицо) ДТР</a></li>
                    <li><a href="/upload/depositary/agreement_chsfr.doc" target="_blank">Соглашение (держательская схема, участник торгов) ЧСФР</a></li>
                </ul>
                <h4>Формы документов, представляемые депонентами в депозитарий</h4>
                <ul class="big_list">
                    <li>Форма 01. <a href="/upload/depositary/6_1__ANKETA_JURIDICAL_PERSON.doc" target="_blank">Анкета Клиента для юридических лиц</a> 
                        <ul>
                            <li><a href="/upload/depositary/depo_anketa.doc" target="_blank">Опросная анкета</a></li>
                            <li><a href="/upload/depositary/fatca_anketa_20171229.doc" target="_blank">Анкета FATCA (для финансовых институтов)</a></li>
                        </ul>
                    </li>
                    <li>Форма 02. <a href="/upload/depositary/6_2__ANKETA_NATURAL_PERSON.doc" target="_blank">Анкета Клиента для физических лиц</a> 
                        <ul>
                            <li><a href="/upload/depositary/ask_list_fiz.doc" target="_blank">Опросный лист</a></li>
                            <li><a href="/upload/depositary/depo_anketa_ip.doc" target="_blank">Опросная анкета (для ИП/ЛЧП)</a></li>
                        </ul>
                    </li>
                    <li>Форма 13. <a href="/upload/depositary/6_13_INSTRUCTION_OPENING_ACC.doc" target="_blank">Поручение на открытие счета депо</a></li>
                    <li>Форма 15. <a href="/upload/depositary/6_15__INSTRUCTION_OPERATOR.doc" target="_blank">Поручение на осуществление операций по счетам депо при заключении договора о брокерском обслуживании</a></li>
                    <li>Форма 03. <a href="/upload/depositary/6_3__CARD_OF_MANAGER.doc" target="_blank">Карточка распорядителя счета депо</a></li>
                    <li>Форма 14. <a href="/upload/depositary/6_14_ANKETA_OPER_TRUSTEE.doc" target="_blank">Анкета оператора/попечителя счета депо</a></li>
                    <li>Форма 06. <a href="/upload/depositary/6_6__INSTRUCTION_RECEIVE.doc" target="_blank">Поручение на&nbsp;зачисление ценных бумаг</a></li>
                    <li>Форма 08. <a href="/upload/depositary/6_8__INSTRUCTION_DELIVER.doc" target="_blank">Поручение на&nbsp;снятие ценных бумаг</a></li>
                    <li>Форма 09. <a href="/upload/depositary/6_9__INSTRUCTION_TRANSFER.doc" target="_blank">Поручение на&nbsp;перемещение ценных бумаг</a></li>
                    <li>Форма 10. <a href="/upload/depositary/6_10_INSTRUCTION_INTERLOCK.doc" target="_blank">Поручение на&nbsp;блокировку ценных бумаг</a></li>
                    <li>Форма 11. <a href="/upload/depositary/6_11_INSTRUCTION_DEPLOY.doc" target="_blank">Поручение на&nbsp;разблокировку ценных бумаг</a></li>
                    <li>Форма 12. <a href="/upload/depositary/6_12_INSTRUCTION_CANCELLATION.doc" target="_blank">Поручение на&nbsp;отмену операции</a></li>
                    <li>Форма 07. <a href="/upload/depositary/6_7__LIST_OF_SECURITIES.doc" target="_blank">Опись сертификатов ценных бумаг</a></li>
                    <li>Форма 16. <a href="/upload/depositary/6_16_INSTRUCTION_VOTE.doc" target="_blank">Поручение на осуществление прав</a></li>
                    <li>Форма 17. <a href="/upload/depositary/6_17__INSTRUCTION_DATA_OPERATION.doc" target="_blank">Поручение на информационную операцию</a></li>
                    <li>Форма 18. <a href="/upload/depositary/6_18_INSTRUCTION_WRITEOFF.doc" target="_blank">Поручение на списание денежных средств</a></li>
                    <li><a href="/upload/depositary/8_POWER_OF_ATTORNEY_COURIE_2016.doc" target="_blank">Образец курьерской доверенности</a></li>
                    <li><a href="/upload/depositary/9_NEW_POWER_OF_ATTORNEY_MANAGER.doc" target="_blank">Образец доверенности на распоряжение счетом депо</a></li>
                    <li><a href="/upload/depositary/13_POWER_OF_ATTORNEY_SAFE_2011.doc" target="_blank">Образец доверенности на&nbsp;получение/передачу сертификатов ценных бумаг</a></li>
                    <li><a href="/upload/depositary/10_NEW_POWER_OF_ATTORNEY_TRUSTEE.doc" target="_blank">Образец доверенности&nbsp;Попечителя</a></li>
                    <li><a href="/upload/depositary/11_NEW_POWER_OF_ATTORNEY_OPERATOR.doc" target="_blank">Образец доверенности&nbsp;Оператора</a></li>
                    <li><a href="/upload/depositary/12_POWER_OF_ATTORNEY_TRANSFER_AGENT_2011.doc" target="_blank">Образец доверенности, выдаваемой представителю Банка ЗЕНИТ с&nbsp;целью перерегистрации ценных бумаг в&nbsp;реестрах владельцев/депозитариях</a></li>
                    <li><a href="/upload/depositary/14_CANCELLATION_TRUSTEE_2011.doc" target="_blank">Уведомление об отзыве доверенности</a></li>
                </ul>
                <p><a href="/upload/depositary/instructions.rar" target="_blank">Формы документов</a>, представляемые депонентами в депозитарий (русс/англ)</p>
                <p><a href="/upload/depositary/reports_20170405.rar" target="_blank">Формы документов</a>, представляемые депозитарием депонентам</p>
                <h4><a id="anc1" name="anc1"></a>Электронный документооборот</h4>
                <ul class="big_list">
                    <li><a href="/upload/depositary/iDepo_UR.rar" target="_blank">Документы на подключение к СЭД Интернет Депозитарий-Клиент для юридического лица</a></li>
                    <li><a href="/upload/depositary/iDepo_FIZ.rar" target="_blank">Документы на подключение к СЭД Интернет Депозитарий-Клиент для физического лица</a></li>
                    <li><a href="/media/rus/content/docs/new_version_rules_idepo_30514.rar" target="_blank">Правила электронного документооборота при использовании СЭД Интернет Депозитарий-Клиент</a></li>
                    <li><a href="/upload/depositary/ADD_DEPO_SWIFT_v4_2.doc" target="_blank">Дополнительное соглашение к депозитарному договору об использовании системы SWIFT</a></li>
                    <li><a href="/upload/depositary/BZ_HANDBOOK_RU.doc" target="_blank">Форматы сообщений SWIFT, применяемые при осуществлении электронного документооборота</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>