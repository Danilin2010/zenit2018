<?	
	require ("_path.php");
	$path = $APPLICATION->GetCurPage();
	$url = Href($path);
?>

	<div class="right_top_line">
		<div class="block_top_line"></div>
		<ul class="right_menu">
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-information/" ?>">Информация о депозитарии</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-services/" ?>">Депозитарные услуги</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-rates/" ?>">Тарифы депозитария</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-documents/" ?>">Документы депозитария</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-securities/" ?>">Обслуживаемые ценные бумаги</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-operations/" ?>">Проведение операций</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-edoc/" ?>">Электронный документооборот</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/depositary-faq/" ?>">Часто задаваемые вопросы</a>
			</li>
			<li>
				<a href="<? echo $url."/depositary-operations/registrars-and-depositaries/" ?>">Перечень регистраторов и депозитариев</a>
			</li>
		</ul>
	</div>