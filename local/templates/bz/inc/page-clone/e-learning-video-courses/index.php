<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Электронные обучающие видеокурсы");
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h2>Электронные обучающие видеокурсы</h2><br>
						<div class="step_block">
					<p>Прямой доступ к услугам Банка из бухгалтерских программ на платформе 1С</p>
					
							<div class="step_item" id="">
								<div class="step_num">
									 i
								</div>
								<div class="step_body">
									<div class="step_title">
									</div>
									<div class="step_text">
										<a href="#modal_online-video2" class="open_modal bigmaxwidth">Загрузка модуля «1С:DirectBank»</a>
									</div>
								</div>
							</div>
							
							<div class="step_item" id="">
								<div class="step_num">
									 i
								</div>
								<div class="step_body">
									<div class="step_title">
									</div>
									<div class="step_text">
										<a href="#modal_online-video3" class="open_modal bigmaxwidth">Установка модуля «1С:DirectBank»</a>
									</div>
								</div>
							</div>							
					<p>Интернет-банк iBank2</p>
					
							<div class="step_item" id="">
								<div class="step_num">
									 i
								</div>
								<div class="step_body">
									<div class="step_title">
									</div>
									<div class="step_text">
										 <a href="#modal_online-video4" class="open_modal bigmaxwidth">Первичная регистрация клиента</a>
									</div>
								</div>
							</div>
							
							<div class="step_item" id="">
								<div class="step_num">
									 i
								</div>
								<div class="step_body">
									<div class="step_title">
									</div>
									<div class="step_text">
										 <a href="#modal_online-video1" class="open_modal bigmaxwidth">Использование услуги проверки контрагентов «Индикатор»</a>
									</div>
								</div>
							</div>
						</div>
			</div>
		</div>
	</div>
</div>
    <div class="modal-open modal form">
        <div id="modal_online-video4" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-sm-12 col-md-12 modal_fon px-4">
                        <div class="row first_step">
                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div class="button_two">
									<h2>Первичная регистрация клиента</h2>
								</div>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div align="center">
									<video src="/upload/medialibrary/bfa/Initial_registration_of_the_client.mp4" width="400" height="300" controls="controls" poster="image.jpg"> </video>
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
    <div class="modal-open modal form">
        <div id="modal_online-video1" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-sm-12 col-md-12 modal_fon px-4">
                        <div class="row first_step">
                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div class="button_two">
									<h2>Использование услуги проверки контрагентов «Индикатор»</h2>
								</div>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div align="center">
									<video src="/upload/medialibrary/f2e/Indicator_review_of_counterparty.mp4" width="400" height="300" controls="controls" poster="image.jpg"> </video>
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="modal-open modal form">
        <div id="modal_online-video2" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-sm-12 col-md-12 modal_fon px-4">
                        <div class="row first_step">
                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div class="button_two">
									<h2>Загрузка модуля «1С:DirectBank»</h2>
								</div>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div align="center">
									<video src="/upload/medialibrary/2aa/Download_iBank2_module_for_1C.mp4" width="400" height="300" controls="controls" poster="image.jpg"> </video>
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal-open modal form">
        <div id="modal_online-video3" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-sm-12 col-md-12 modal_fon px-4">
                        <div class="row first_step">
                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div class="button_two">
									<h2>Установка модуля «1С:DirectBank»</h2>
								</div>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div align="center">
									<video src="/upload/medialibrary/fd3/Module_installation_iBank2_for_1C.mp4" width="400" height="300" controls="controls" poster="image.jpg"> </video>
								</div>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>