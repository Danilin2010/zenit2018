<div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Преимущества</a></li>
			<li><a href="#content-tabs-2">С правом регресса</a></li>
			<li><a href="#content-tabs-3">Без права регресса</a></li>
			<li><a href="#content-tabs-4">Реверсивный</a></li>
			<li><a href="#content-tabs-5">Для поставщиков торговых сетей</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="contacts_block">
							<p>
								 Контакты:
							</p>
							 <!--div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851-->
						</div>
						 <!--br-->
						<div class="contacts_block">
							<div class="note_text">
								 Начальник отдела поддержки продаж
							</div>
							Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Заместитель начальника отдела поддержки продаж
							</div>
							Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99<br>
							моб.: +7 (964) 566-76-39
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Руководитель проектов отдела поддержки продаж
							</div>
							Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
					</div>
					<div class="block_type_center">
						<ul class="big_list">
							<li>
							отсутствие кассовых разрывов – предоставление финансирования в день предоставления отгрузочных документов; </li>
							<li>
							финансирование без залога - экономия на расходах по оформлению залога; </li>
							<li>долгосрочное сотрудничество – заключение бессрочного договора факторинга;</li>
							<li>увеличение лимитов по мере роста продаж;</li>
							<li>быстрая процедура принятия решения;</li>
							<li>партнерские отношения с торговыми сетями;</li>
							<li>бесперебойное финансирование;</li>
							<li>минимальный пакет документов;</li>
							<li>предварительное рассмотрение в течение 1-3 раб. дней;</li>
							<li> </li>
							<li>электронный документооборот - финансирование по электронному реестру. <br>
							 (на сегодняшний день у Банка имеется широкий опыт работы с провайдерами ЭДО, в том числе Тензор, СКБ Контур, Эдисофт)</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<p>
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
						</p>
						<div class="contacts_block">
							<p>
								 Контакты:
							</p>
							 <!--div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851-->
						</div>
						 <!--br-->
						<div class="contacts_block">
							<div class="note_text">
								 Начальник отдела поддержки продаж
							</div>
							Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Заместитель начальника отдела поддержки продаж
							</div>
							Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99<br>
							моб.: +7 (964) 566-76-39
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Руководитель проектов отдела поддержки продаж
							</div>
							Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
					</div>
					<div class="block_type_center">
						<h2>Поставщик несет ответственность за исполнение покупателем своих обязательств по оплате поставок </h2>
						<ul class="big_list">
							<li>финансирование до 90% от суммы поставки;</li>
							<li>льготный период ожидания - до 42 календарных дней к отсрочке по контракту; </li>
							<li>информационная поддержка банком - комплексная проверка платежеспособности дебиторов и кредитный анализ;</li>
							<li>Ежедневный мониторинг дебиторской задолженности</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<p>
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
						</p>
						<div class="contacts_block">
							<p>
								 Контакты:
							</p>
							 <!--div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851-->
						</div>
						 <!--br-->
						<div class="contacts_block">
							<div class="note_text">
								 Начальник отдела поддержки продаж
							</div>
							Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Заместитель начальника отдела поддержки продаж
							</div>
							Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99<br>
							моб.: +7 (964) 566-76-39
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Руководитель проектов отдела поддержки продаж
							</div>
							Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
					</div>
					<div class="block_type_center">
						<h2>Банк берет на себя риск неплатежа покупателя </h2>
						<ul class="big_list">
							<li>финансирование до 100 % от суммы поставки;</li>
							<li>страхование кредитного риска неоплаты покупателей поставщика;</li>
							<li>информационная поддержка банком - комплексная проверка дебиторов;</li>
							<li>оптимизация структуры баланса;</li>
							<li>безопасное расширение сбытовой сети и географии поставок.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<p>
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
						</p>
						<div class="contacts_block">
							<p>
								 Контакты:
							</p>
							 <!--div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851-->
						</div>
						 <!--br-->
						<div class="contacts_block">
							<div class="note_text">
								 Начальник отдела поддержки продаж
							</div>
							Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Заместитель начальника отдела поддержки продаж
							</div>
							Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99<br>
							моб.: +7 (964) 566-76-39
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Руководитель проектов отдела поддержки продаж
							</div>
							Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
					</div>
					<div class="block_type_center">
						<h2>Факторинг для покупателей, работающих на условиях отсрочки платежа</h2>
						<ul class="big_list">
							<li>финансирование до 100 % от суммы поставки;</li>
							<li>предоставление дополнительной отсрочки до 120 календарных дней;</li>
							<li>возможность получения скидок от поставщиков - экономия на закупочных ценах.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-5">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<p>
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
						</p>
						<div class="contacts_block">
							<p>
								 Контакты:
							</p>
							 <!--div class="note_text">
		 Начальник управления факторинга
	</div>
	Золкин Геннадий Александрович <a href="mailto:g.zolkin@zenit.ru">g.zolkin@zenit.ru</a><br>
	+7 (495) 937-07-37, доб. 3851-->
						</div>
						<div class="contacts_block">
							<div class="note_text">
								 Начальник отдела поддержки продаж
							</div>
							Ярыгин Андрей Александрович <a href="mailto:a.yarygin@zenit.ru">a.yarygin@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Заместитель начальника отдела поддержки продаж
							</div>
							Типикина Елизавета Борисовна <a href="mailto:e.tipikina@zenit.ru">e.tipikina@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99<br>
							моб.: +7 (964) 566-76-39
						</div>
 <br>
						<div class="contacts_block">
							<div class="note_text">
								 Руководитель проектов отдела поддержки продаж
							</div>
							Минеев Владимир Валерьевич <a href="mailto:v.mineev@zenit.ru">v.mineev@zenitfinance.ru</a><br>
							тел.: +7 (495) 150-26-99
						</div>
 <br>
					</div>
					<div class="block_type_center">
						<div class="row">
							<h2>Факторинг для поставщиков торговых сетей</h2>
							<p>
								 Поставщикам, использующим электронный документооборот (EDI), предлагается специальная программа по факторингу.
								<br><div class="simple_benefits"><div class="pict money"></div> <b style="padding-left:25px;"> Стоимость финансирования  от 10,5 % годовых.</b></div>
								<br>
							</p>

							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 1
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div>
											 Для получения финансирования поставщик направляет заявку на факторинг и минимальный пакет документов в электронном виде:
											<p>
											</p>
											<ul class="big_list">
												<li><a title="Анкета Клиента для принятия на факторинговое обслуживание в ПАО Банк ЗЕНИТ" href="/upload/medialibrary/7a7/anketa_factoring.doc">Анкета Клиента</a></li>
												<li>Контракт на поставку продукции (актуальная версия)</li>
												<li>Карточку счета 62 за последние полные 12 месяцев по расчетам между поставщиком и федеральной сетью в формате excel</li>
												<li>Отчет фактора за период, соответствующий периоду карточки 62 счета, по федеральным сетям, планируемым к передаче на факторинговое обслуживание (предоставляется при наличии у поставщика факторинга).</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 2
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div>
											 В течение 1-2 рабочих дней Банк сообщает предварительное решение о финансировании поставщика (при условии предоставления полного пакета документов, указанных выше).
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 3
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div>
											 Утверждение лимита и подготовка документации занимает еще 3- 5 рабочих дней. <br>
											 <!--Эффективная ставка по факторингу от 10,5 % годовых-->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_center">
					<div class="text" style=""padding:  10px !important; background:  white !important; border-radius:  5px !important;>
						 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"factor", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "factor",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Заявка ".$arResult["NAME"],
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "12",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>