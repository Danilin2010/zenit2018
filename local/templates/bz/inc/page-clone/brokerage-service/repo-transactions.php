<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<?
		$APPLICATION->IncludeFile(
    		SITE_TEMPLATE_PATH."/inc/page-clone/brokerage-service/right_links.php",
    		Array(),
    		Array("MODE"=>"txt","SHOW_BORDER"=>false)
		);?>
		<?
		include ("/inc/page-clone/brokerage-service/_path.php");
		$path = $APPLICATION->GetCurPage();
		$url = Href($path);
		?>

		<div class="block_type_center">
			<div class="text_block">
				<ul class="big_list">
					<li>совокупный объем не менее 10 млн. руб.; </li>
					<li>срок 1-7 дней с возможностью продления; </li>
					<li>процентная ставка зависит от конъюнктуры денежного рынка (МБК); </li>
					<li>в качестве обеспечения принимаются акции и облигации российских эмитентов, на которые установлены лимиты в Банке ЗЕНИТ. </li>
				</ul>
				<p>
					В рамках брокерского обслуживания на российском фондовом рынке Банк ЗЕНИТ предлагает своим клиентам возможность совершения сделок обратного РЕПО. Данная услуга является дополнением к непокрытым (маржинальным) сделкам и представляет интерес главным образом для долгосрочных инвесторов.
				</p>
				<p>
					<a href="<? echo $url."/brokerage-service/options-repo-transactions/" ?>">Параметры совершения сделок РЕПО с ценными бумагами</a>.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
     <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "broker",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "result_edit.php",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "result_list.php",
        "RIGHT_TEXT" => "",
        "SEF_MODE" => "N",
        "SOURCE_TREATMENT" => "Заявка на брокерское обслуживание: ".$arResult["NAME"],
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
        "WEB_FORM_ID" => "16"
    )
);?>
</div>