<div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Акции</a></li>
			<li><a href="#content-tabs-2">Корпоративные облигации</a></li>
			<li><a href="#content-tabs-3">Субфедеральные облигации</a></li>
			<li><a href="#content-tabs-4">Государственные облигации</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<?
					$APPLICATION->IncludeFile(
						SITE_TEMPLATE_PATH."/inc/page-clone/brokerage-service/right_links.php",
						Array(),
						Array("MODE"=>"txt","SHOW_BORDER"=>false)
					);?>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								Список ценных бумаг для совершения сделок репо/ Перечень ценных бумаг, для которых при расчете активов и/или обязательств клиента принимаются фиксированные дисконты
							</p>
							<table class="responsive-stacked-table tb" cellspacing="0" cellpadding="0">
							<thead>
							<tr>
								<td>
									Инструмент
								</td>
								<td>
									Дисконт для сделок до 1 дня, %
								</td>
								<td>
									Дисконт для сделок до 7 (14) дней, %
								</td>
								<td>
									Margin Call для сделок до 7 (14) дней, %
								</td>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									ВТБ
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Газпром
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Лукойл
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									ММК
								</td>
								<td>
									20
								</td>
								<td>
									25 (30)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Мосэнерго
								</td>
								<td>
									35
								</td>
								<td>
									40 (45)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									МТС
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									НЛМК
								</td>
								<td>
									20
								</td>
								<td>
									25 (30)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Норильский никель
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Роснефть
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Ростелеком ао
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Ростелеком ап
								</td>
								<td>
									25
								</td>
								<td>
									30 (35)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Сбербанк ао, ап
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Сургутнефтегаз ао, ап
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Татнефть ао
								</td>
								<td>
									20
								</td>
								<td>
									25 (30)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Татнефть ап
								</td>
								<td>
									35
								</td>
								<td>
									40 (45)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									Транснефть ап
								</td>
								<td>
									15
								</td>
								<td>
									20 (25)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							<tr>
								<td>
									ФСК ЕЭС
								</td>
								<td>
									25
								</td>
								<td>
									30 (35)
								</td>
								<td>
									7 (10)
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								Список ценных бумаг для совершения сделок репо/ Перечень ценных бумаг, для которых при расчете активов и/или обязательств клиента принимаются фиксированные дисконты
							</p>
							<table class="responsive-stacked-table tb" cellspacing="0" cellpadding="0">
							<thead>
							<tr>
								<td>
								</td>
								<td colspan="3">
									Параметры сделок РЕПО
								</td>
							</tr>
							<tr>
								<td>
									Инструмент
								</td>
								<td>
									Дисконт для сделок до 1 дня, %
								</td>
								<td>
									Дисконт для сделок до 7 (14) дней, %
								</td>
								<td>
									Margin Call, %
								</td>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									ВТБ (вып. 6, БО-8, БО-19, БО-20, БО-21, БО-22, БО-26, БО-30, БО-43)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ВЭБ (вып. 6, 8, 9, 10, 18, 19, 21, 31, БО-01, БО-04, БО-14, БО-16)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ВЭБ (вып. 31,&nbsp; БО-04, БО-14)
								</td>
								<td>
									10
								</td>
								<td>
									12 (14)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ВТБ-Лизинг Финанс (вып. 2, 3, 4, 7, 8, 9, БО-2, БО-4)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ВЭБ-Лизинг (вып.1, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ВЭБ-Лизинг (вып БО-01, БО-04, БО-05, БО-06, БО-07, БО-08)
								</td>
								<td>
									10
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Газпром (вып. БО-19, БО-20, БО-21)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Газпром Капитал (вып.4, 5, 6)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Газпромбанк (БО-5, БО-6, БО-7, БО-8, БО-9, БО-10, 6, 7)
								</td>
								<td>
									8
								</td>
								<td>
									12 (15)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Газпромнефть (вып. 4, 8, 9, 10, 11, 12)
								</td>
								<td>
									10
								</td>
								<td>
									12 (14)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									МТС (вып. 3)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									МТС (вып.2, 5, 7, 8)
								</td>
								<td>
									10
								</td>
								<td>
									12 (14)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									НК Роснефть (вып. 4, 5, 6, 7, 8, 9, 10)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									НК Роснефть (вып. БО-01, БО-02, БО-03, БО-04, БО-05, БО-06, БО-07, БО-08, БО-09, БО-10, БО-11, БО-12, БО-13, БО-14, БО-15, БО-16, БО-17, БО-24)
								</td>
								<td>
									10
								</td>
								<td>
									12 (14)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									РСХБ (вып.3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, БО-4, БО-7, БО-13, БО-14, БО-15)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									РЖД (вып. 11, 12, 14, 15, 16, 17, 18, 19, 23, 28, 30, 32, 33, 34)
								</td>
								<td>
									8
								</td>
								<td>
									10(12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Транснефть (вып.1, 2, 3, БО-1, БО-2, БО-3)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ФСК ЕЭС (вып. 7, 9, 10, 11, 15, 25)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ФСК ЕЭС (вып. 6, 8, 13, 18, 19, 22)
								</td>
								<td>
									10
								</td>
								<td>
									12 (14)
								</td>
								<td>
									5
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								Список ценных бумаг для совершения сделок репо/ Перечень ценных бумаг, для которых при расчете активов и/или обязательств клиента принимаются фиксированные дисконты
							</p>
							<table class="responsive-stacked-table tb" cellspacing="0" cellpadding="0">
							<thead>
							<tr>
								<td>
									&nbsp;
								</td>
								<td colspan="3">
									Параметры сделок РЕПО
								</td>
							</tr>
							<tr>
								<td>
									Инструмент
								</td>
								<td>
									Дисконт для сделок до 1 дня, %
								</td>
								<td>
									Дисконт для сделок до 7 (14) дней, %
								</td>
								<td>
									Margin Call, %
								</td>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									Волгоградская область&nbsp;(34005, 35002, 35003, 35005)
								</td>
								<td>
									10
								</td>
								<td>
									15 (17)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Краснодарский край (34004)
								</td>
								<td>
									10
								</td>
								<td>
									15 (18)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Красноярский край&nbsp;(34005, 34006, 34007, 34008)
								</td>
								<td>
									10
								</td>
								<td>
									15 (19)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Липецкая область (34007, 34009, 35008)
								</td>
								<td>
									10
								</td>
								<td>
									15 (17)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Москва (44, 49, 56, 64, 66, 67)
								</td>
								<td>
									8
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Москва (48)
								</td>
								<td>
									10
								</td>
								<td>
									12 (14)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Нижегородская область (34006, 34007, 34008, 34009)
								</td>
								<td>
									10
								</td>
								<td>
									15 (17)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Омская область (34001)
								</td>
								<td>
									10
								</td>
								<td>
									12 (15)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Республика Коми (32010, 35008, 35011)
								</td>
								<td>
									10
								</td>
								<td>
									15 (17)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Республика Саха (Якутия) (35003, 35004, 35005, 35006)
								</td>
								<td>
									8
								</td>
								<td>
									12 (15)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Самарская область (35007, 35008, 35009, 35010)
								</td>
								<td>
									10
								</td>
								<td>
									15 (17)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Свердловская обл (34001, 34002)
								</td>
								<td>
									10
								</td>
								<td>
									15 (18)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Тверская область&nbsp;(34007, 34008, 34009)
								</td>
								<td>
									12
								</td>
								<td>
									15 (20)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Ярославская область&nbsp;(34011, 34012, 34013)
								</td>
								<td>
									10
								</td>
								<td>
									15 (17)
								</td>
								<td>
									5
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								Список ценных бумаг для совершения сделок репо/ Перечень ценных бумаг, для которых при расчете активов и/или обязательств клиента принимаются фиксированные дисконты
							</p>
							<table class="responsive-stacked-table tb" cellspacing="0" cellpadding="0">
							<thead>
							<tr>
								<td>
									&nbsp;
								</td>
								<td colspan="3">
									Параметры сделок РЕПО
								</td>
							</tr>
							<tr>
								<td>
									Инструмент
								</td>
								<td>
									Дисконт для сделок до 1 дня, %
								</td>
								<td>
									Дисконт для сделок до 7 (14) дней, %
								</td>
								<td>
									Margin Call, %
								</td>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>
									Облигации Федерального займа (25077, 25079, 25080)
								</td>
								<td>
									5
								</td>
								<td>
									7 (10)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									Облигации Федерального займа (24018, 25075, 25081, 25082, 26204, 26205, 26206, 26207, 26208, 26209, 26210, 26211, 26212, 26214, 26215, 26216, 29006, 29011, 46005, 46007, 46008, 46009, 46011, 46012, 46014, 46017, 46018, 46019, 46020, 46021, 46022, 46023, 48001)
								</td>
								<td>
									7
								</td>
								<td>
									10 (12)
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ЕвроОблигации Федерального займа (2018, 2019, 2020, 2022)
								</td>
								<td>
									&nbsp;9
								</td>
								<td>
									12 (14)&nbsp;
								</td>
								<td>
									5
								</td>
							</tr>
							<tr>
								<td>
									ЕвроОблигации Федерального займа (2023, 2028, 2030)&nbsp;
								</td>
								<td>
									10&nbsp;
								</td>
								<td>
									13 (15)&nbsp;
								</td>
								<td>
									&nbsp;5&nbsp;
								</td>
							</tr>
							<tr>
								<td>
									&nbsp;ЕвроОблигации Федерального займа (2042, 2043)
								</td>
								<td>
									11
								</td>
								<td>
									14 (16)&nbsp;
								</td>
								<td>
									&nbsp;5&nbsp;
								</td>
							</tr>
							<tr>
								<td>
									&nbsp;ЕвроОблигации Федерального займа (2015)
								</td>
								<td>
									6
								</td>
								<td>
									&nbsp;8 (11)&nbsp;
								</td>
								<td>
									&nbsp;5&nbsp;
								</td>
							</tr>
							<tr>
								<td>
									ЕвроОблигации Федерального займа (2017)&nbsp;
								</td>
								<td>
									&nbsp;7&nbsp;
								</td>
								<td>
									&nbsp;9 (12)&nbsp;
								</td>
								<td>
									&nbsp;5&nbsp;
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="block_type to_column c-container">
		<div class="block_type_center">
				<h3>
					Таблица определения величины плавающего дисконта
				</h3>
				<table class="responsive-stacked-table tb" cellpadding="0" cellspacing="0">
				<tbody>
				<tr>
					<td>
						Отношение количества ценных бумаг в портфеле к среднедневному обороту
					</td>
					<td>
						Дисконт, %
					</td>
				</tr>
				<tr>
					<td>
						до 0.001
					</td>
					<td>
						0
					</td>
				</tr>
				<tr>
					<td>
						0.001 - 0.006
					</td>
					<td>
						3
					</td>
				</tr>
				<tr>
					<td>
						0.006 - 0.020
					</td>
					<td>
						5
					</td>
				</tr>
				<tr>
					<td>
						0.020 - 0.133
					</td>
					<td>
						10
					</td>
				</tr>
				<tr>
					<td>
						0.133 - 0.167
					</td>
					<td>
						15
					</td>
				</tr>
				<tr>
					<td>
						0.167 - 0.200
					</td>
					<td>
						20
					</td>
				</tr>
				<tr>
					<td>
						0.200 - 0.250
					</td>
					<td>
						25
					</td>
				</tr>
				<tr>
					<td>
						0.250 - 0.333
					</td>
					<td>
						30
					</td>
				</tr>
				<tr>
					<td>
						0.333 - 0.500
					</td>
					<td>
						40
					</td>
				</tr>
				<tr>
					<td>
						свыше 0.500
					</td>
					<td>
						50
					</td>
				</tr>
				</tbody>
				</table>

		</div>
	</div>
</div>