<div class="wr_block_type">
	<div class="content_rates_tabs">
		<div class="content_tab_wrapper">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Описание</a></li>
				<li><a href="#content-tabs-2">Программы</a></li>
				<li><a href="#content-tabs-3">Возможности терминала</a></li>
				<li><a href="#content-tabs-4">Безопасность и подключение</a></li>
			</ul>
		</div>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<div class="text_block">
							<h2>Информационно-торговый комплекс для работы на фондовом рынке NetInvestor</h2>
							 Клиентам предоставляется возможность совершать операции на всех площадках России:
							<ul class="big_list">
								<li>Московская Биржа</li>
								<li>FORTS</li>
							</ul>
							<h2>Комплекс NetInvestor</h2>
							<p>
								 Система NetInvestor - информационно-торговая система для торговли ценными бумагами через Интернет. Комплекс обеспечивает богатые возможности управления портфелем, развитые функции аналитической обработки данных и удобный, функциональный интерфейс для совершения торговых операций.
							</p>
							<h2>Профессиональным участникам предоставляются возможности:</h2>
							<ul class="big_list">
								<li>Доступ к портфелю компании одновременно нескольких трейдеров</li>
								<li>Возможность заведения собственных клиентов и манипулирования их портфелями&nbsp;</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right"></div>
					<div class="block_type_center">
						<h3>Программы NetInvestor</h3>
						<!--simple_benefits-->
						<div class="simple_benefits">
							<div class="simple_benefits_item">
								<div class="pict" style="background-image: url('/upload/img/brokerage/image_327.png');"></div>
								<div class="body">
									<div class="title">
										NetInvestor Professional
									</div>
									<div class="text">
										Универсальный торговый терминал, совмещающий в себе широкий функционал для торговли и обладающий богатыми возможностями по анализу, графическому отображению и экспорту данных в программы технического анализа и MS Excel в режиме on-line.
									</div>
								</div>
							</div><div class="simple_benefits_item">
								<div class="pict" style="background-image: url('/upload/img/brokerage/image_328.png');"></div>
								<div class="body">
									<div class="title">
										NIPocket
									</div>
									<div class="text">
										Клиентский торговый терминал системы NetInvestor для карманных компьютеров класса PocketPC.<br>
										Терминал NIPocket предоставляет данные о сделках и котировках, позволяет строить графики цены, выставляет заявки, условные заявки SL и TP, производит мониторинг портфеля и состояния маржи.
									</div>
								</div>
							</div><div class="simple_benefits_item">
								<div class="pict" style="background-image: url('/upload/img/brokerage/image_326.png');"></div>
								<div class="body">
									<div class="title">
										NITrustManager
									</div>
									<div class="text">
										Модуль Доверительного управления NetInvestor, позволяющий управлять портфелями групп клиентов. Производит автоматическое выставление заявок одновременно за несколько клиентских счетов в зависимости от выбранной торговой стратегии.
									</div>
								</div>
							</div><div class="simple_benefits_item">
								<div class="pict" style="background-image: url('/upload/img/brokerage/image_329.png');"></div>
								<div class="body">
									<div class="title">
										NetInvestor API
									</div>
									<div class="text">
										Предлагает программный способ получения торговой информации для расширенной аналитики, а также выставления заявок в торговую систему с высокой скоростью и возможностью обратной связи. Решение используется для:<br>
											 - Написания автоматических торговых систем и роботов;<br>
											 - Разработки собственных приложений и приводов.<br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<div class="text_block">
							<h2>Возможности торгового терминала NetInvestor</h2>
							<ul class="big_list">
								<li>Просмотр рыночной информации: котировки, сделки, новостные ленты</li>
								<li>Мониторинг состояния портфеля</li>
								<li>Отслеживание своих сделок и заявок</li>
								<li>Контроль позиции в режиме реального времени с учетом выставленных заявок на покупку/продажу с учетом комиссии брокера и биржи</li>
								<li>Контроль заемных активов</li>
							</ul>
							<ul class="big_list">
								<li>Широкий графический инструментарий</li>
								<li>Большая библиотека индикаторов технического анализа</li>
								<li>Торговля из графиков</li>
								<li>Гибкие настройки для анализа</li>
								<li>Шаблоны</li>
							</ul>
							<ul class="big_list">
								<li>Минимизированные окна выставления заявки</li>
								<li>Интеллектуальные приказы стоп-лосс, тейк-профит</li>
								<li>Автоматическая проверка лимитов</li>
								<li>Мгновенное закрытие позиций</li>
							</ul>
							<ul class="big_list">
								<li>Расширенные возможности стакана</li>
								<li>Кнопки для быстрых операций</li>
								<li>Торговля с клавиатуры</li>
							</ul>
							<ul class="big_list">
								<li>Анализ фьючерсов и опционов</li>
								<li>Доска опционов</li>
								<li>Моделятор</li>
								<li>Торговля спредами</li>
								<li>Графики стратегий</li>
								<li>3D поверхности стратегий</li>
							</ul>
							<ul class="big_list">
								<li>Новости</li>
								<li>Сканер рынка</li>
								<li>Экспорт в системы ТА, MS Excel, базы данных</li>
								<li>Клиентам всегда доступна оперативная информация агентства «Интерфакс» - экономика, финансы и рынки (ЭФиР - Брокер)</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<h3>Безопасность комплекса NetInvestor</h3>
						<p>
							 Система NetInvestor использует средства шифрования и электронной цифровой подписи (ЭЦП) сообщений. Электронная цифровая подпись (ЭЦП) - реквизит электронного документа, предназначенный для защиты данного электронного документа от подделки, полученный в результате криптографического преобразования информации и позволяющий идентифицировать владельца сертификата ключа подписи, а также установить отсутствие искажения информации в электронном документе.
						</p>
						<h3>Подключение к комплексу NetInvestor</h3>
						<p>
							 Получить доступ к&nbsp;торгам могут любые физические и&nbsp;юридические лица с&nbsp;любого компьютера, оборудованного доступом в&nbsp;Интернет.
						</p>
						<p>
							 Специалисты Банка окажут необходимую поддержку для оптимальной настройки комплекса.
						</p>
						<ul class="big_list">
							<li><a target="_blank" href="/upload/docs/brokerage/NIPro_setup.exe">Дистрибутив</a> торгового комплекса</li>
							<li><a href="/upload/docs/brokerage/NIPro_upgrade.exe">Обновление</a> торгового комплекса</li>
							<li><a target="_blank" href="/upload/docs/brokerage/Instr_Nipro.pdf">Руководство</a> по настройке торгового комплекса</li>
							<li><a target="_blank" href="http://www.netinvestor.ru/manual_ni_professional.aspx">Руководство</a> пользователя</li>
						</ul>
						<p>
 							<a target="_blank" href="http://netinvestor.ru/demo_investor.aspx">Демо-доступ</a> к системе Интернет-трейдинга&nbsp;Netinvestor
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>