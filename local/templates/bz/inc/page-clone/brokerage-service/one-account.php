<div class="wr_block_type">
	<div class="block_type to_column z-container">
		<?
		$APPLICATION->IncludeFile(
    		SITE_TEMPLATE_PATH."/inc/page-clone/brokerage-service/right_links.php",
    		Array(),
    		Array("MODE"=>"txt","SHOW_BORDER"=>false)
		);?>
		<div class="block_type_center">
			<div class="text_block">
				<ul class="big_list">
					<li>Брокерский счет является мультивалютным (рубли, доллары, евро) </li>
					<li>При совершении сделок неважно, на каком из рынков находятся денежные средства Клиента </li>
					<li>Все деньги и валюты доступны в любой момент времени и учитываются централизованно </li>
				</ul>
				<p>
					 Данная технология позволяет клиентам совершать операции на любой торговой площадке без необходимости предварительного депонирования на ней активов. В результате клиент получает возможность работать на той торговой площадке, конъюнктура на которой кажется ему наиболее привлекательной, а Банк обеспечит наличие на ней необходимых активов. <br>
				</p>
				<p>
					Применяемая в Банке ЗЕНИТ технология Единого Торгового Счета сводит к минимуму временные задержки и позволяет клиенту максимизировать свой доход. Данная услуга предоставляется бесплатно.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
     <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "broker",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "result_edit.php",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "result_list.php",
        "RIGHT_TEXT" => "",
        "SEF_MODE" => "N",
        "SOURCE_TREATMENT" => "Заявка на брокерское обслуживание: ".$arResult["NAME"],
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
        "WEB_FORM_ID" => "16"
    )
);?>
</div>