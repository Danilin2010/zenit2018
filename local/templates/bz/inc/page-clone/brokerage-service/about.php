<div class="wr_block_type">
	<div class="block_type to_column z-container">
		 <?
		$APPLICATION->IncludeFile(
    		SITE_TEMPLATE_PATH."/inc/page-clone/brokerage-service/right_links.php",
    		Array(),
    		Array("MODE"=>"txt","SHOW_BORDER"=>false)
		);?>
		<div class="block_type_center">
			<div class="text_block">
				<ul class="big_list">
					<li>Две системы интернет-трейдинга (Quik, NetInvestor)</li>
					<li>Возможность совершения сделок РЕПО и непокрытых (маржинальных) сделок</li>
					<li>Единый торговый счет (мгновенный перевод денег между рынками)</li>
					<li>Широкий перечень финансовых инструментов (акции, облигации, еврооблигации, фьючерсы и опционы, валюты, депозитарные расписки, ETF)</li>
					<li>Биржевые (МосБиржа, LSE, NYSE) и внебиржевые рынки</li>
					<li>Удобные тарифы </li>
				</ul>
				<p>
					 Брокерское обслуживание предоставляет клиентам возможность самостоятельно совершать сделки с финансовыми инструментами на биржевом и внебиржевом рынках. Проведение операций возможно из любой точки мира как посредством несложных систем интернет-трейдинга, так и по телефону. <br>
				</p>
				<p>
					 Услуга доступна юридическим и физическим лицам, как резидентам, так и нерезидентам.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
     <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "broker",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "result_edit.php",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "result_list.php",
        "RIGHT_TEXT" => "",
        "SEF_MODE" => "N",
        "SOURCE_TREATMENT" => "Заявка на брокерское обслуживание: ".$arResult["NAME"],
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
        "WEB_FORM_ID" => "16"
    )
);?>
</div>