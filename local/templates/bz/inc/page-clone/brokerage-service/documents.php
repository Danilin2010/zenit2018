<div class="wr_block_type">
	<div class="block_type to_column z-container">
		<?
		$APPLICATION->IncludeFile(
    		SITE_TEMPLATE_PATH."/inc/page-clone/brokerage-service/right_links.php",
    		Array(),
    		Array("MODE"=>"txt","SHOW_BORDER"=>false)
		);?>
		<div class="block_type_center">
			<div class="text_block">
				<p>
					<strong>Вниманию действующих клиентов!</strong> Обращаем ваше внимание на изменения, внесенные в Регламент оказания услуг на финансовых рынках ПАО Банк ЗЕНИТ, которые вступают в силу с 01 ноября 2017 г. С текстом изменений можно ознакомиться <a href="/upload/docs/brokerage/Brokreglament2017.doc">здесь</a>.
				</p>
				<p>
					Договоры:
				</p>
				<ul class="big_list">
					<li><a href="/upload/docs/brokerage/Brokreglament.doc">Регламент оказания услуг на финансовых рынках</a></li>
					<li><a href="/personal/invest/depositary-operations/depositary-documents/ ">Депозитарные документы (для физ. и юр. лиц)</a></li>
				</ul>
				<p>
					Бланки поручений для регламента:
				</p>
				<ul class="big_list">
					<li><a href="/upload/docs/brokerage/moneyorder.doc">Поручение на вывод/перевод денежных средств</a></li>
					<li><a href="/upload/docs/brokerage/repoorder.doc">Поручение на сделку РЕПО</a></li>
					<li><a href="/upload/docs/brokerage/dealorder.doc">Поручение на совершение сделки с ценными бумагами</a></li>
					<li><a href="/upload/docs/brokerage/margin13.doc">Заявление на увеличение кредитного плеча</a></li>
					<li><a href="/upload/docs/brokerage/declmmvb.doc">Декларация о рисках для работы с котировальным списком «И» ФБ ММВБ</a></li>
					<li><a href="/upload/docs/brokerage/oferta.doc">Уведомление о продаже облигаций</a></li>
				</ul>
				<p>
					Бланки поручений для договора на комплексное обслуживание:
				</p>
				<ul class="big_list">
					<li><a href="/upload/docs/brokerage/poruchenie1.doc">Поручение на вывод/перевод денежных средств</a></li>
					<li><a href="/upload/docs/brokerage/poruchenie2.doc">Поручение на сделку РЕПО</a></li>
					<li><a href="/upload/docs/brokerage/poruchenie3.doc">Поручение на совершение сделки с ценными бумагами</a></li>
					<li><a href="/upload/docs/brokerage/closeagr-ul.doc">Соглашение о расторжении договора на комплексное обслуживание и перехода на регламент (для юридических лиц)</a></li>
					<li><a href="/upload/docs/brokerage/closeagr-fl.doc">Соглашение о расторжении договора на комплексное обслуживание и перехода на регламент (для физических лиц)</a></li>
				</ul>
				<p>
					Генерация криптоключа:
				</p>
				<ul class="big_list">
					<li><a href="/upload/docs/brokerage/keygen.zip">Генерация криптоключа NetInvestor</a></li>
					<li><a href="/media/rus/download/keygenquik.zip">Генерация криптоключей Quik</a></li>
				</ul>
				<p>
					Дополнительные документы:
				</p>
				<ul class="big_list">
					<li><a href="/upload/docs/brokerage/jurdocs.doc">Список необходимых для предоставления юр. документов</a></li>
					<li><a href="/upload/docs/brokerage/kval-investor.pdf">Порядок присвоения клиентам Банка статуса "квалифицированный инвестор"</a></li>
				</ul>
				<p>
					Документы FATCA:
				</p>
				<ul class="big_list">
					<li><a href="/upload/docs/brokerage/Form%20W-9.doc">Форма W-9 (для физ. лиц)</a></li>
					<li><a href="/upload/docs/brokerage/Form%20W-8BEN.doc">Форма W-8BEN (для физ. лиц)</a></li>
					<li><a href="/upload/docs/brokerage/Assent.doc">Согласие на передачу данных (для физ. лиц)</a></li>
					<li><a href="/upload/docs/brokerage/Form%20OFR.doc">Анкета-форма сертификации ОФР</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
     <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "broker",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "result_edit.php",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "result_list.php",
        "RIGHT_TEXT" => "",
        "SEF_MODE" => "N",
        "SOURCE_TREATMENT" => "Заявка на брокерское обслуживание: ".$arResult["NAME"],
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
        "WEB_FORM_ID" => "16"
    )
);?>
</div>