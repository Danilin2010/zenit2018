<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?$APPLICATION->ShowTitle()?></title>

    <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico" />
    <meta name="theme-color" content="#1da9af">
    <link rel="icon" sizes="192x192" href="<?=SITE_TEMPLATE_PATH?>/img/icon/android-chrome-192.png">
    <meta content="app-id=1187159866" name="apple-itunes-app">
    <meta content="app-id=ru.zenit.zenitonline" name="google-play-app">

    <link rel="manifest" href="/manifest/manifest.json">

    <?
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/slick/slick.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.scroolly.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery-ui/jquery-ui.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.number.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.formstyler.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/baraja/modernizr.custom.79639.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/baraja/jquery.baraja.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/vivus/pathformer.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/vivus/vivus.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/smartbanner/_jquery.smartbanner.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/toggles/toggles.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.validate.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/additional-methods.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/localization/messages_ru.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/inputmask/jquery.inputmask.bundle.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/slider-pips/jquery-ui-slider-pips.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.ui.touch-punch.min.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.mousewheel.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.kinetic.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.smoothDivScroll-1.3.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/vendors/jquery.cookie.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jslider/jquery.slider-bundled.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/toggle.js");
    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/baraja.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/city.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/slider.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/top_banner.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/main.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/custom.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/form_search.js');

    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/contact-form.js');
    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/zayavka.js");


    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/slick/slick.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/jquery-ui/jquery-ui.structure.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/baraja/baraja.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/smartbanner/jquery.smartbanner.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/toggles/css/toggles.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/toggles/css/themes/toggles-modern.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/toggles/css/themes/toggles-light.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/slider-pips/jquery-ui-slider-pips.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/jslider/css/jquery.jslider-all.css");
    //Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/font/Lato/font.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/font/ProximaNova/font.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/toggle.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/icon.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/gui.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/_sky.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/slider.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/main.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/1600.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/900.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/mobile.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/offices-mobile.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/about.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/about-mobile.css");
    //Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/menu-drob.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/offices.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/search.css");

    $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "sect",
            "EDIT_TEMPLATE" => "",
            "AREA_FILE_SUFFIX" => "private_banking_style",
        ),
        false
    );
    //Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css");
    ?>

    <?$APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/inc/counters/gtm.php",
        Array(),
        Array("MODE"=>"txt","SHOW_BORDER"=>false)
    );?>

    <!--<link rel="stylesheet" type="text/css" href="pixel/pixel1.css" />-->
    <?$APPLICATION->ShowHead()?>
    <script type="text/javascript">
        $(function() {
            $.smartbanner({
                title: 'ЗЕНИТ Онлайн',
                author: 'в мобильном удобнее',
                price: null,
                icon: '<?=SITE_TEMPLATE_PATH?>/img/icon/android-chrome-192.png',
                button: 'Открыть',
                scale: $('body').width() / window.screen.width,
                //scale: 2,
                speedIn: 300,
                speedOut: 400,
                daysHidden: 15,
                daysReminder: 90,
                force: null
            });
        });
    </script>

    <?/*<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet" />*/?>

</head>

<body
    class="<?if($APPLICATION->GetCurPage(true) == "/index.php" || $APPLICATION->GetDirProperty("sky_show")=="Y") {?>main_page<?}else{?>to_page<?}?> <?BufferContent::ShowTitle('to_page_class');?>">
<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/counters/gtm_body.php",
    Array(),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>
<div id="panel"><?$APPLICATION->ShowPanel()?></div>
<!--select_city-->
<?$APPLICATION->IncludeComponent(
	"aic.bz:city.list",
	".default",
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	),
	false
);?>
<!--select_sity-->
<div class="main_wrapper">
	<?if($APPLICATION->GetCurPage(true) == "/index.php" || $APPLICATION->GetDirProperty("sky_show")=="Y") {?>
    <div class="sky">
        <div class="clouds_one"></div>
        <div class="clouds_two"></div>
        <div class="clouds_three"></div>
    </div>
	<?}?>

    <div id="header-modal" class="wr_fix_top">
        <div class="fix_top">
            <!--top_line-->
            <div class="wr_top_line">
                <div class="top_line z-container">
                    <div class="top_line_right">
                        <ul class="ul_top_line_right">
                            <?$APPLICATION->IncludeComponent("bitrix:menu", "li_menu", Array(
                                "COMPONENT_TEMPLATE" => "main_menu",
                                "ROOT_MENU_TYPE" => "toplineright",	// Тип меню для первого уровня
                                "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                                "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                            ),
                                false
                            );?>
                            <li class="b_select_city <?if(!ChekCook()){?>open<?}?>">
                                <a href="#" title="Выбор города" data-select-city><?=GetSity()?></a>
                                <div class="wr_select_city_in">
                                    <div class="select_city">
                                        <div class="select_city_text">
                                            Ваш город
                                        </div>
                                        <div class="select_city_name">
                                            <?=GetSity()?>
                                        </div>
                                        <table>
                                            <tr>
                                                <td><a class="button maxwidth" data-close-city data-set-cook-city href="javascript:void(0)">Да</a></td>
                                                <td><a href="#" title="Выбор города" data-select-city>выбрать другой</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <?/*<li><a href="/offices/"><div class="ico map"></div>Банкоматы</a> и <a href="/offices/">офисы</a></li>*/?>
                            <li><a href="/offices/?type=atm"><div class="ico map"></div>Банкоматы</a> и <a href="/offices/">офисы</a></li>
                            <li class="menu_contact"><a href="" class="cd-dropdown-trigger"><div class="ico note"></div>Связаться с нами</a>
                                <div class="container-menu">
                                    <div class="cd-dropdown" id="menu_contact">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="title-cont">Бесплатно по россии</div>
                                                <div class="phone">8 (800) 500-66-77</div>
                                                <div class="content">Для физических лиц</div>
                                                <div class="phone">8 (800) 500-40-82</div>
                                                <div class="content">Для юридических лиц</div>
                                                <div class="row icon">
                                                    <div class="col-sm-6 col-md-6 icon-linck">
                                                        <a href="skype:zenitgroup.ru?call">
                                                            <i class="fa fa-skype" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <!--<div class="col-sm-6 col-md-6 icon-linck">
                                                        <a href="#">
                                                            <i class="fa fa-weixin" aria-hidden="true"></i>
                                                        </a>
                                                    </div>-->
                                                    <div class="col-sm-12 col-md-12 linck">
                                                        <a href="#modal_form-contact" class="open_modal">Обратная связь</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/inc/template/search.php",
                            Array(),
                            Array("MODE"=>"txt","SHOW_BORDER"=>false)
                        );?>
                    </div>
                    <!--ul_top_line-->
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "ul_top_line", Array(
                            "COMPONENT_TEMPLATE" => "main_menu",
                            "ROOT_MENU_TYPE" => "toplineleft",	// Тип меню для первого уровня
                            "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                            "MAX_LEVEL" => "1",	// Уровень вложенности меню
                            "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        ),
                        false
                    );?>
                    <!--ul_top_line-->
                </div>
            </div>
            <!--top_line-->
            <!--header-->
            <div class="wr_header">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_top_line", Array(
                    "COMPONENT_TEMPLATE" => "main_menu",
                    "ROOT_MENU_TYPE" => "toplineleft",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                    false
                );?>
                <div class="b_wr_header">
                    <div class="header z-container">
                        <div class="wr_mobail_min_menu wr_js_form">
                            <ul class="mobail_min_menu">
                                <!--<li><a href="#" data-select-city class="map"></a></li>-->
								<li><a href="/offices/" class="map"></a></li>
                                <li><a href="#modal_form-contact" class="open_contact_form note"></a></li>
                                <li><a href="#" class="menu"></a></li>
                            </ul>
                            <div class="container-menu glyphicon-menu-down">
                                <div class="cd-dropdown" id="menu_contact">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="title-cont">Бесплатно по россии</div>
                                            <a href="tel:88005006677" class="phone">8 (800) 500-66-77</a>
                                            <div class="content">Для физических лиц</div>
                                            <a href="tel:88005004082" class="phone">8 (800) 500-40-82</a>
                                            <div class="content">Для юридических лиц</div>
                                            <div class="row icon">
                                                <div class="col-sm-6 col-md-6 col-mb-6 col-mt-6 icon-linck">
                                                    <a href="skype:zenitgroup.ru?call">
                                                        <i class="fa fa-skype" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                               <?/* <div class="col-sm-6 col-md-6 col-mb-6 col-mt-6 icon-linck">
                                                    <a href="#">
                                                        <i class="fa fa-weixin" aria-hidden="true"></i>
                                                    </a>
                                                </div>*/?>
                                                <div class="col-sm-12 col-md-12 col-mb-12 col-mt-12 linck">
                                                    <a href="#modal_form-contact" class="open_contact_form open_modal">Обратная связь</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobail_show_menu">
                                <div class="mobail_show_menu_close"></div>
                                <a href="#" class="mobail_show_menu_castle"></a>
                                <?$APPLICATION->IncludeFile(
                                    SITE_TEMPLATE_PATH."/inc/template/mobail_search.php",
                                    Array(),
                                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                                );?>
                            </div>
                            <?$APPLICATION->IncludeFile(
                                SITE_TEMPLATE_PATH."/inc/template/search_result.php",
                                Array(),
                                Array("MODE"=>"txt","SHOW_BORDER"=>false)
                            );?>
                        </div>
                        <a class="logo" href="/"></a>
                        <a href="tel:88005006677" class="mobile-header-phone">8 (800) 500-66-77</a>
                        <!--top_menu-->
                        <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"main_menu",
	array(
		"COMPONENT_TEMPLATE" => "main_menu",
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"CLASS_MENU" => $APPLICATION->GetDirProperty("class_menu")
	),
	false
);?>
                        <!--top_menu-->
                        <a href="#" data-show="menu_net_bank" class="net_bank"><div class="ico close"></div>Интернет-банк</a>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "sect",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_SUFFIX" => "all_products",
                            ),
                            false,
                            array('HIDE_ICONS' => 'Y')
                        );?>
                    </div>
                </div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "sect",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_SUFFIX" => "menu",
                    ),
                    false,
                    array('HIDE_ICONS' => 'Y')
                );?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "sect",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_SUFFIX" => "burger",
                    ),
                    false,
                    array('HIDE_ICONS' => 'Y')
                );?>
                <!--menu_net_bank-->
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/menu/menu_net_bank.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
                <!--menu_net_bank-->
            </div>
            <!--header-->
        </div>
    </div>



<?if($APPLICATION->GetCurPage(true) == "/index.php") {?>
		<!--main_slider-->
		<? $APPLICATION->IncludeComponent("bitrix:news.list",
//        "main_slider",
        "main_selection", //TODO: Тестовый шаблон выбора продукта
        Array(
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "banners",
		// Тип информационного блока (используется только для проверки)
		"IBLOCK_ID" => "2",
		// Код информационного блока
		"NEWS_COUNT" => "20",
		// Количество новостей на странице
		"SORT_BY1" => "SORT",
		// Поле для первой сортировки новостей
		"SORT_ORDER1" => "ASC",
		// Направление для первой сортировки новостей
		"SORT_BY2" => "NAME",
		// Поле для второй сортировки новостей
		"SORT_ORDER2" => "ASC",
		// Направление для второй сортировки новостей
		"FILTER_NAME" => "",
		// Фильтр
		"FIELD_CODE" => array(    // Поля
			0 => "DETAIL_TEXT",
			1 => "",
		),
		"PROPERTY_CODE" => array(    // Свойства
			0 => "PERCENT",
			1 => "LINK",
			2 => "SUFFIX",
			3 => "",
		),
		"CHECK_DATES" => "Y",
		// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",
		// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"AJAX_MODE" => "N",
		// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",
		// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",
		// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",
		// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",
		// Дополнительный идентификатор
		"CACHE_TYPE" => "A",
		// Тип кеширования
		"CACHE_TIME" => "36000",
		// Время кеширования (сек.)
		"CACHE_FILTER" => "N",
		// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",
		// Учитывать права доступа
		"PREVIEW_TRUNCATE_LEN" => "",
		// Максимальная длина анонса для вывода (только для типа текст)
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		// Формат показа даты
		"SET_TITLE" => "N",
		// Устанавливать заголовок страницы
		"SET_BROWSER_TITLE" => "N",
		// Устанавливать заголовок окна браузера
		"SET_META_KEYWORDS" => "N",
		// Устанавливать ключевые слова страницы
		"SET_META_DESCRIPTION" => "N",
		// Устанавливать описание страницы
		"SET_LAST_MODIFIED" => "N",
		// Устанавливать в заголовках ответа время модификации страницы
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		// Включать инфоблок в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "N",
		// Включать раздел в цепочку навигации
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		// Скрывать ссылку, если нет детального описания
		"PARENT_SECTION" => "147",
		// ID раздела
		"PARENT_SECTION_CODE" => "",
		// Код раздела
		"INCLUDE_SUBSECTIONS" => "N",
		// Показывать элементы подразделов раздела
		"DISPLAY_DATE" => "N",
		// Выводить дату элемента
		"DISPLAY_NAME" => "Y",
		// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",
		// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",
		// Выводить текст анонса
		"PAGER_TEMPLATE" => ".default",
		// Шаблон постраничной навигации
		"DISPLAY_TOP_PAGER" => "N",
		// Выводить над списком
		"DISPLAY_BOTTOM_PAGER" => "N",
		// Выводить под списком
		"PAGER_TITLE" => "Новости",
		// Название категорий
		"PAGER_SHOW_ALWAYS" => "N",
		// Выводить всегда
		"PAGER_DESC_NUMBERING" => "N",
		// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",
		// Показывать ссылку "Все"
		"PAGER_BASE_LINK_ENABLE" => "N",
		// Включить обработку ссылок
		"SET_STATUS_404" => "N",
		// Устанавливать статус 404
		"SHOW_404" => "N",
		// Показ специальной страницы
		"MESSAGE_404" => "",
		// Сообщение для показа (по умолчанию из компонента)
	), false); ?>
		<!--main_slider-->
	<? }else{?>
    <?BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/inner/all.png');?>
    <?//BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/inner/_inner-mortage.png');?>


    <?
    $html_text_for_caps=$APPLICATION->GetDirProperty("html_text_for_caps");
    BufferContent::SetTitle('headertext',$html_text_for_caps);
    ?>


    <?if($APPLICATION->GetDirProperty("hide_big_slide")!="Y"){?>
        <div class="page-promo-banner">
            <img class="banner-background" src="<?BufferContent::ShowTitle('top_breadcrumbs_images');?>" alt="">
            <div class="blur-wrap">
                <div class="img-container">
                    <img class="blur-image" src="<?BufferContent::ShowTitle('top_breadcrumbs_images');?>" alt="">
                </div>
                <div class="blur-content">
                    <div class="c-container">
                        <?BufferContent::ShowTitle('headerpic');?>
                        <div class="title">
                            <?$APPLICATION->ShowTitle(false,false);?>
                        </div>
                        <div class="text">
                            <?BufferContent::ShowTitle('headertext');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?if($APPLICATION->GetDirProperty("use_page_frame")=="Y"){?><div class="wr_block_type"><div class="block_type"><div class="text_block"><?}?>

    <?}?>
<?} ?>

<?if($APPLICATION->GetDirProperty("type_bank_page")=="Y"){?>
    <div class="container about-page">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12 col-mt-0 col-md-8">
                    <div class="row">
                        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                            <div class="block-card note_text">
<?}?>

<?if($APPLICATION->GetDirProperty("type_bank_page_block")=="Y"){?>
    <div class="container about-page">
        <div class="row">
            <!-- Правый блок мобилка и планшет -->
            <div class="col-sm-12 col-md-4 hidden-lg">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <!-- Содержимое -->
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mb-5 main-content-top">
                        <div class="block-card note_text">
<?}?>





