<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult["SECTION_EXT"]){?>
    <h1><?=$arResult["SECTION_EXT"]["NAME"]?></h1>
<?}?>
<!--step_block-->
<div class="step_block">
    <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="step_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="step_num" <?if($arItem["ALTERNATIVE"]){?>style="background-color: <?=$arItem["ALTERNATIVE"]?>;" <?}?>><?=$key+1?></div>
        <div class="step_body">
            <div class="step_title">
                <?echo $arItem["NAME"];?>
            </div>
            <div class="step_text">
                <?echo $arItem["PREVIEW_TEXT"];?>
            </div>
        </div>
    </div>
    <?endforeach;?>
</div>
<!--step_block-->
