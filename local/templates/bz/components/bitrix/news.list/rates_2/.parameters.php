<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "ALL_PHONE" => Array(
        "NAME" => GetMessage("ALL_PHONE"),
        "TYPE" => "STRING",
    ),
    "CODE_SITY" => Array(
        "NAME" => GetMessage("CODE_SITY"),
        "TYPE" => "STRING",
    ),
    "USE_CLIENTBANK" => Array(
        "NAME" => GetMessage("USE_CLIENTBANK"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
    ),
	"SHOW_BLANKS" => Array(
		"NAME" => GetMessage("SHOW_BLANKS"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
	),
);
?>