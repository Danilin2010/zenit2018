<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
    <div class="conditions">
        <h3>Банковская Группа ЗЕНИТ</h3>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="logo-bank">
                <img src="<?=$arItem["DISPLAY_PROPERTIES"]["LOGO"]["FILE_VALUE"]["SRC"]?>">
            </div>
        <?endforeach;?>
        <div class="logo-bank">
            <img src="images/logo-5.png">
        </div>
    </div>
</div>

<!--div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
    <div class="conditions">
        <h3>Банковская Группа ЗЕНИТ</h3>
        <div class="logo-bank">
            <img src="images/logo-1.png">
        </div>
        <div class="logo-bank">
            <img src="images/logo-2.png">
        </div>
        <div class="logo-bank">
            <img src="images/logo-3.png">
        </div>
        <div class="logo-bank">
            <img src="images/logo-4.png">
        </div>
        <div class="logo-bank">
            <img src="images/logo-5.png">
        </div>
    </div>
</div-->