<?

if (is_array($arResult["SECTION"]["PATH"]) && (count($arResult["SECTION"]["PATH"]) > 0)) {
    $arResult["SECTION_EXT"] = end($arResult["SECTION"]["PATH"]);
}

foreach($arResult["ITEMS"] as &$arItem)
{
    if($arItem["DISPLAY_PROPERTIES"]["TYPE_ELEMENT"])
    {
        if($arItem["DISPLAY_PROPERTIES"]["TYPE_ELEMENT"]["VALUE_XML_ID"]=="TYPE_ELEMENT_INFO")
            $arItem["PICT_TYPE"]="circle";
        else if($arItem["DISPLAY_PROPERTIES"]["TYPE_ELEMENT"]["VALUE_XML_ID"]=="TYPE_ELEMENT_WARNING")
            $arItem["PICT_TYPE"]="triangle";
    }
}unset($arItem);
