<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
        <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="block-card note_text">
                <h2><?=$arItem["NAME"]?></h2>
                <?=$arItem["FIELDS"]["DETAIL_TEXT"]?>
                <a href="<?=$arItem["DISPLAY_PROPERTIES"]["BUTTON_LINK"]["VALUE"]?>" class="button min_height">история банка</a>
            </div>
        </div>
<?endforeach;?>