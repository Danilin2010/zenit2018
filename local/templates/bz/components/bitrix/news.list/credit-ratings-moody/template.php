<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container about-page">
    <div class="row">
        <!-- Содержимое -->
        <div class="col-sm-12 col-mt-0 col-md-12">
            <div class="row">
                <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                    <div class="block-card pt-0 note_text">
                        <!--content_tab-->
                        <div class="content_rates_tabs">
                            <ul class="content_tab c-container">
                                <?foreach($arResult["ITEMS"] as $arItem):?>
                                    <?
                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                    ?>
                                    <li><a href="#content-tabs-<?=$arItem["EXTERNAL_ID"]?>"><?=$arItem["DISPLAY_PROPERTIES"]["UNDER_TITLE"]["VALUE"]?></a></li>
                                <?endforeach;?>
                            </ul>

                            <?foreach($arResult["ITEMS"] as $arItem):?>
                                <?
                                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <div class="content_body" id="content-tabs-<?=$arItem["EXTERNAL_ID"]?>">
                                    <span id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                        <h3><?=$arItem["NAME"]?></h3>
                                        <table class="tb">
                                            <thead>
                                            <tr>
                                                <td>
                                                    Категория
                                                </td>
                                                <td>
                                                    Рейтинг
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["FORECAST"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["FORECAST"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["FORECAST"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["UNSECURED_DEBT"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["UNSECURED_DEBT"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["UNSECURED_DEBT"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["COUNTERPARTY_ASSESSMENT"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["COUNTERPARTY_ASSESSMENT"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["COUNTERPARTY_ASSESSMENT"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["COUNTERPARTY_ASSESSMENT"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["COUNTERPARTY_ASSESSMENT"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["COUNTERPARTY_ASSESSMENT"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["FOREIGN_CURRENCY"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["FOREIGN_CURRENCY"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["FOREIGN_CURRENCY"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["NATIONAL_CURRENCY"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["NATIONAL_CURRENCY"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["NATIONAL_CURRENCY"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["RATING_FOREIGN_CURRENCY"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["RATING_FOREIGN_CURRENCY"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["RATING_FOREIGN_CURRENCY"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["LONG_TERM_ISSUER"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["LONG_TERM_ISSUER"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["LONG_TERM_ISSUER"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["DEFAULT_FOREIGN_CURREN"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["DEFAULT_FOREIGN_CURREN"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["DEFAULT_FOREIGN_CURREN"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["SUPPORT_RATING"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["SUPPORT_RATING"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["SUPPORT_RATING"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["RESILIENCE_RATING"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["RESILIENCE_RATING"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["RESILIENCE_RATING"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]["CREDIT_RATING"])) :?>
                                                    <tr>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["CREDIT_RATING"]["NAME"]?>
                                                        </td>
                                                        <td>
                                                            <?=$arItem["DISPLAY_PROPERTIES"]["CREDIT_RATING"]["VALUE"]?>
                                                        </td>
                                                    </tr>
                                                <?endif;?>
                                            </tbody>
                                        </table>
                                    </span>
                                </div>
                            <?endforeach;?>
                        </div>
                        <!--content_tab-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
