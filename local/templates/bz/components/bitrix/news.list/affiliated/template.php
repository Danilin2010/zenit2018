<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container about-page">
    <div class="row">
        <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <!-- Содержимое -->
        <div class="col-sm-12 col-mt-0 col-md-8">
            <div class="row">
                <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                    <div class="preview-card note_text">
                        <div class="preview-content-filtr">
                            <p>
                                На данной странице Вы можете ознакомиться со списками аффилированных лиц и изменениями в списки аффилированных лиц.
                            </p>
                            <p>
                                Списки и Изменения в них даны в обратной хронологической последовательности.
                            </p>
                        </div>
                        <?foreach($arResult["ITEMS"] as $arItem):?>
                            <?
                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <div class="preview-content">
                                <h3 class="title"><?=$arItem["PROPERTIES"]["DATE"]["VALUE"]?></h3>
                                <div class="doc_list" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                    <a href="<?=$arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>" class="doc_item" target="_blank">
                                        <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                                        </div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                <?=$arItem["NAME"]?>
                                            </div>
                                            <div class="doc_note">
                                                <?=$arItem["FILE"]["FILE_SIZE"]?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="faq">
                                    <?foreach($arItem["CHILDREN"] as $Children){?>
                                        <?
                                            $this->AddEditAction($Children['ID'], $Children['EDIT_LINK'], CIBlock::GetArrayByID($Children["IBLOCK_ID"], "ELEMENT_EDIT"));
                                            $this->AddDeleteAction($Children['ID'], $Children['DELETE_LINK'], CIBlock::GetArrayByID($Children["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                        ?>
                                        <div id="<?=$this->GetEditAreaId($Children['ID']);?>" class="faq_item">
                                            <div class="faq_top">
                                                <div class="faq_pict">
                                                    <div class="faq_arr">
                                                    </div>
                                                </div>
                                                <div class="faq_top_text">
                                                    Изменения от <?=$Children["PROPERTY_DATE_VALUE"]?>
                                                </div>
                                            </div>
                                            <div class="faq_text" style="display: none;">
                                                <div class="contrnt-otchet">
                                                    <div class="doc_list">
                                                        <a href="<?=$Children["FILE"]["SRC"]?>" class="doc_item">
                                                            <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>" target="_blank">
                                                            </div>
                                                            <div class="doc_body">
                                                                <div class="doc_text">
                                                                    Изменения в <?=$Children["NAME"]?>
                                                                </div>
                                                                <div class="doc_note">
                                                                    <?=$Children["FILE"]["FILE_SIZE"]?>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?}?>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>
