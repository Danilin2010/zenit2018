<?
/**
 * @var $arResult
 * @var $arParams
 */

foreach($arResult["ITEMS"] as &$item){
    $item["FILE"]=GetFile($item["NAME"],$item["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]);
}unset($item);


$sections = array();

// выборка секций
$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y');
$rsSect = CIBlockSection::GetList(array(), $arFilter);
while ($arSect = $rsSect->GetNext())
{
    //echo '<pre>' . print_r($arSect, 1) . '</pre>';
    $sections[] = $arSect;
}

$arResult['SECTIONS'] = $sections;