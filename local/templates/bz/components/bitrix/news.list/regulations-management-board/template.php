<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">
    <div class="col-sm-12">
        <div class="block-card right note_text">
            <div class="doc_list">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <a id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="doc_item full-contener" href="<?=$arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>" target="_blank">
                        <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                        </div>
                        <div class="doc_body">
                            <div class="doc_text">
                                <?=$arItem["NAME"]?>
                            </div>
                            <div class="doc_note">
                                <?=$arItem["FILE"]["FILE_SIZE"]?>
                            </div>
                        </div>
                    </a>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>

