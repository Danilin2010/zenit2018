<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
    <div class="block-card-no block-card note_text pt-3">
        <h3>Кредитные рейтинги</h3>
        <div class="row views-1">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-sm-12 col-mt-8 col-md-4">
                    <div class="title">
                        <?=$arItem["NAME"]?>
                    </div>
                    <div class="sub-title">
                        <?=$arItem["DISPLAY_PROPERTIES"]["FIRST_VALUE"]["VALUE"]?>
                    </div>
                    <div class="content">
                        <?=$arItem["DISPLAY_PROPERTIES"]["FORECAST"]["VALUE"]?>
                    </div>
                </div>
            <?endforeach;?>
        </div>
    </div>
</div>