<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container about-page">
    <div class="row">
        <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <!-- Содержимое -->
        <div class="col-sm-12 col-mt-0 col-md-8">
            <div class="row">
                <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                    <div class="block-card note_text">
                        <div class="faq">
                            <?$i=0;?>
                                <?foreach($arResult["NEW_ITEMS"] as $key=>$NewItems){?>
                                    <div class="faq_item <?if($i<=0){?>open<?}?>">
                                        <div class="faq_top">
                                            <div class="faq_pict">
                                                <div class="faq_arr">
                                                </div>
                                            </div>
                                            <div class="faq_top_text">
                                                <?=$key?> год
                                            </div>
                                        </div>
                                        <div class="faq_text" <?if($i<=0){?>style="display: block;"<?}?>>
                                            <?foreach($NewItems as $arItem){?>
                                            <?
                                                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                            ?>
                                                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="contrnt-otchet">
                                                    <div class="data">
                                                        <?=$arItem["NAME"]?>:
                                                    </div>
                                                    <div class="doc_list">
                                                        <a href="<?=$arItem["FILE"]["SRC"]?>" class="doc_item" target="_blank">
                                                            <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                                                            </div>
                                                            <div class="doc_body">
                                                                <div class="doc_text">
                                                                    <?=$arItem["PROPERTIES"]["TITLE_DOC"]["VALUE"]?>
                                                                </div>
                                                                <div class="doc_note">
                                                                    <?=$arItem["FILE"]["FILE_SIZE"]?>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?}?>
                                        </div>
                                    </div>
                                <?$i++;?>
                            <?}?>
                        </div>
                    </div>
                </div>

                <!--pre-->
                    <!--? print_r($arResult["ITEMS"])?-->
                <!--/pre-->

            </div>
        </div>
        <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>

