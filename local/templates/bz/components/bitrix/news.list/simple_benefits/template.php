<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["PARENT_SECTION_CODE"]){?>
    <h1><?=$arParams["PARENT_SECTION_CODE"]?></h1>
<?} elseif($arResult["SECTION_EXT"]){?>
    <h1><?=$arResult["SECTION_EXT"]["NAME"]?></h1>
<?}?>
<!--simple_benefits-->
<div class="simple_benefits">
    <?foreach($arResult["ITEMS"] as $arItem){
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?><div class="simple_benefits_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="pict " <?if($arItem['PREVIEW_PICTURE']['SRC']){?>style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>');"<?}?>></div>
            <div class="body">
                <div class="title">
                    <?=$arItem['NAME']?>
                </div>
                <div class="text">
                    <?echo $arItem["PREVIEW_TEXT"];?>
                </div>
            </div>
        </div><?}?>
</div>
<!--simple_benefits-->