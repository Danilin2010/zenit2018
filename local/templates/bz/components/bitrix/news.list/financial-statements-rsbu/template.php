<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="content_body pt-0" id="content-tabs-1">
    <div class="preview-content-filtr">
        <div class="jq-selectbox jqselect formstyle bank">
            <form method="get" class="resubmit">
                <select id="reporting-type" data-programm="" data-plasholder="Укажите тип отчетности" data-title="Укажите тип отчетности" class="formstyle" name="reporting">
                    <option value="all" selected="">Все отчеты</option>
                    <?foreach($arResult["REPORTINGTYPE"] as $key => $rep){?>
                        <option <?if($arParams["REPORTING_TYPE"]==$key){?> selected <?}?> value="<?= $key ?>">
                            <?= $rep ?>
                        </option>
                    <?}?>
                </select>
            </form>
        </div>

        <div class="faq mt-5">
            <?$i=0;?>
                <?foreach($arResult["NEW_ITEMS"] as $key=>$NewItems){?>
                    <div class="faq_item <?if($i<=0){?>open<?}?>">
                        <div class="faq_top">
                            <div class="faq_pict">
                                <div class="faq_arr">
                                </div>
                            </div>
                            <div class="faq_top_text">
                                <?=$arResult['SECTIONS'][$key]?> год
                            </div>
                        </div>
                        <div class="faq_text" <?if($i<=0){?>style="display: block;"<?}?>>
                            <?foreach($NewItems as $arItem){?>
                                <?
                                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="contrnt-otchet">
                                    <div class="data">
                                        <?=$arItem["DISPLAY_PROPERTIES"]["DATA"]["VALUE"]?> (опубликовано: <?=$arItem["DISPLAY_PROPERTIES"]["DATA_PUBLICATION"]["VALUE"]?>)
                                    </div>
                                    <div class="doc_list">
                                        <a href="<?=$arItem["FILE"]["SRC"]?>" class="doc_item" target="_blank">
                                            <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                                            </div>
                                            <div class="doc_body">
                                                <div class="doc_text">
                                                    <?=$arItem["NAME"]?>
                                                </div>
                                                <div class="doc_note">
                                                    <?=$arItem["FILE"]["FILE_SIZE"]?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                 <?$i++;?>
            <?}?>
        </div>
    </div>
</div>