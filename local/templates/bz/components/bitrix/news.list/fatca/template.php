<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4">
    <div class="block-card pt-1">
        <table class="responsive-stacked-table tb">

            <thead>
            <tr>
                <td>
                    Name
                </td>
                <td>
                    Status
                </td>
                <td>
                    FI Type
                </td>
                <td>
                    GIIN
                </td>
            </tr>
            </thead>
            <tbody>
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <td>
                        <?=$arItem["NAME"]?>
                    </td>
                    <td>
                        <?=$arItem["DISPLAY_PROPERTIES"]["STATUS"]["VALUE"]?>
                    </td>
                    <td>
                        <?=$arItem["DISPLAY_PROPERTIES"]["FI_TYPE"]["VALUE"]?>
                    </td>
                    <td>
                        <?=$arItem["DISPLAY_PROPERTIES"]["GIIN"]["VALUE"]?>
                    </td>
                </tr>
            <?endforeach;?>
            </tbody>
        </table>
    </div>
</div>