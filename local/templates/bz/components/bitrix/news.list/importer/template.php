<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block_type to_column c-container">
	<? if ($arResult["ITEMS"]): ?>
		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="block_type_center" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
				<h1>Условия </h1>
				<!--conditions-->
				<div class="conditions wide">
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["EXTERNAL_TRANSFER"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							Внешний перевод<br> в любой валюте
						</div>
					</div>
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["CURRENCY_CONTROL_DISCOUNT"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							Скидка на услуги<br> валютного контроля
						</div>
					</div>
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["MONTH_FEE"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							Ежемесячная <br>плата
						</div>
					</div>
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["CONNECTION_FEE"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							Стоимость<br> подключения
						</div>
					</div>
				</div>
				<!--conditions-->
				<div class="text_block">
					<?= $arItem["PREVIEW_TEXT"] ?>
				</div>
			</div>
		<? endforeach; ?>
	<? else: ?>
		Тарифов по выбранному региону не обнаружено.
	<? endif; ?>
</div>

