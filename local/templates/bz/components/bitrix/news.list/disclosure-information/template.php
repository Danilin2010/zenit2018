<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container about-page">
    <div class="row">
        <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <!-- Содержимое -->
        <div class="col-sm-12 col-mt-0 col-md-8">
            <div class="row">
                <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mt-3">
                    <?foreach($arResult["ITEMS"] as $arItem):?>
                        <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="block-card note_text">
                            <div class="doc_list">
                                <a class="doc_item full-contener" href="<?=$arItem["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]["SRC"]?>" target="_blank">
                                    <div class="doc_pict <?=$arItem["FILES"]["TYPE"]?>">
                                    </div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            <?=$arItem["NAME"]?>
                                        </div>
                                        <div class="doc_note">
                                            <?=$arItem["FILES"]["FILE_SIZE"]?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?endforeach;?>
                    <div class="ipoteka_list my-5">
                        <div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/rus/about_bank/disclosure/disclosure-information/information-risks/" class="ipoteka_item_title">
                                    Информация о рисках на консолидированной основе
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div><div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/rus/about_bank/disclosure/disclosure-information/information-rates/" class="ipoteka_item_title">
                                    Информация о ставках по вкладам для физических лиц
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div><div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/rus/about_bank/disclosure/disclosure-information/information-bank/" class="ipoteka_item_title">
                                    Информация об инструментах капитала Банка
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div><div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/rus/about_bank/disclosure/disclosure-information/information-capital/" class="ipoteka_item_title">
                                    Информация об инструментах капитала Банковской группы
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div>
						<div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/rus/about_bank/disclosure/disclosure-information/branches-and-offices" class="ipoteka_item_title">
                                    Сведения о филиалах и представительствах Банка
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div>
						<div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/rus/about_bank/disclosure/disclosure-information/information-insurance-companies/" class="ipoteka_item_title">
                                    Информация о требованиях к страховым компаниям - партнерам
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>
