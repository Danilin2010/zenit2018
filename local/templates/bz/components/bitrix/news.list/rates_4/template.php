<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? //echo '<pre>' . print_r($arParams, 1) . '</pre>';?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
            <div class="right_top_line">
                <h2>Контакты</h2>
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <?if($arItem["LIST"]){?>
                        <?foreach($arItem["LIST"] as $arList){?>
                            <div class="form_application_line" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                                <?if(!$arList["NOT_USE_NAME"]){?>
                                <div class="right_bank_title">
                                    <?=$arList["NAME"]?>
                                </div>
                                <?}?>
                                <div class="contacts_block">
                                    <?if($arList["EMAIL"]){?><a href="mailto:<?=$arList["EMAIL"]?>"><?=$arList["EMAIL"]?></a><br><?}?>
                                    <?=$arList["PHONE"]?>
                                </div>
                            </div>
                        <?}?>
                    <?}?>
                <?}?>
                <?if($arParams["ALL_PHONE"]){?>
                <div class="right_bank_title">Телефон прямой линии</div>
                <div class="form_application_line">
                    <div class="contacts_block">
                        <?=$arParams["ALL_PHONE"]?>
                    </div>
                    <div class="note_text">
                        звонок по России бесплатный
                    </div>
                </div>
                <?}?>
            </div>
		</div>
		<div class="block_type_center">
			<div class="text_block">

           <!--doc-->
				<div class="doc_list">
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <?if($arItem["RATES"]){?>
                        <a href="<?=$arItem["RATES"]["SRC"]?>" class="doc_item1" target="_blank" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                            <div class="doc_pict <?=$arItem["RATES"]["TYPE"]?>"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Тарифы комиссионного вознаграждения ПАО Банк ЗЕНИТ за услуги для юридических лиц, индивидуальных предпринимателей и физических лиц, занимающихся в установленном законодательством Российской Федерации порядке частной практикой

                                </div>
                                <div class="doc_note">
									<?/*=$arItem["RATES"]["FILE_SIZE"]*/?>
                                </div>
                            </div>
                        </a>
                    <?}?>
                    <?if($arItem["BLANKS"] && $arParams["SHOW_BLANKS"] == "Y"){?>
                        <h2 <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>><!--Бланки Заявлений--></h2>
                        <?foreach ($arItem["BLANKS"] as $blank):?>
                            <a href="<?=$blank["SRC"]?>" target="_blank" class="doc_item1" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                                <div class="doc_pict <?=$blank["TYPE"]?>"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        <?=$blank["NAME"]?>
                                    </div>
                                    <div class="doc_note">
                                        <?=$blank["FILE_SIZE"]?>
                                    </div>
                                </div>
                            </a>
                        <?endforeach;?>
                    <?}?>
                <?}?>
				</div>				
					<br>
						<div class="doc_list"> 
							<a href="/upload/iblock/e0a/tk_tarifs_20170313.pdf" target="_blank" class="doc_item1" >
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Тарифы комиссионного вознаграждения ПАО Банк ЗЕНИТ по операциям с Таможенными картами                                    </div>
                                    <div class="doc_note">
                                       <!-- 409.15 Кб -->                                   </div>
                                </div>
							</a>
						</div>
            <!--doc-->
				<br/>
				<?$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH."/inc/block/rates_4.php",
					Array(
						/*"template"=>"rates",
						"all_phone" => "8 (800) 500-66-77",
						"use_clientbank" => "N",
"show_blanks" => "N",*/
					),
					Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>

			</div>
    	</div>
	</div>
</div>