<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="form_application_line">
    <div class="rates_right_top">
        <select data-programm data-plasholder="Ваш город" data-title="Ваш город" class="formstyle" name="code_sity">
            <?foreach($arResult["ITEMS"] as $arItem){?>
                <option value="<?=$arItem["ID"]?>" <?if($arItem["ID"]==$arParams["CODE_SITY"]){?>selected<?}?>><?=$arItem["NAME"]?></option>
            <?}?>
        </select>
    </div>
    <?if($arParams["USE_CLIENTBANK"]=="Y"){?>
        <div class="form_application_line">
            <div class="contacts_block">
                <a target="_blank" href="https://cb.zenit.ru/">Вход в Клиент-Банк</a>
            </div>
        </div>
        <h2 >Служба технической поддержки</h2>
        <div class="form_application_line">
            <div class="right_bank_title">
                по Москве
            </div>
            <div class="contacts_block">
                (495) 745-79-30
            </div>
        </div>
        <div class="form_application_line">
            <div class="right_bank_title">
                по России
            </div>
            <div class="contacts_block">
                <a href="mailto:cbsupport@zenit.ru">cbsupport@zenit.ru</a><br>
                8-800-700-15-17 доб. 3000
            </div>
            <div class="note_text">
                звонок по России бесплатный
            </div>
        </div>
    <?}?>
    <div class="rates_right_body">
        <div class="rates_right_right">
            <h2>Тарифы</h2>
            <!--doc-->
            <div class="doc_list">
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <?if($arItem["RATES"]){?>
                        <a href="<?=$arItem["RATES"]["SRC"]?>" class="doc_item" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                            <div class="doc_pict <?=$arItem["RATES"]["TYPE"]?>"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание
                                </div>
                                <div class="doc_note">
                                    <?=$arItem["RATES"]["FILE_SIZE"]?>
                                </div>
                            </div>
                        </a>
                    <?}?>
                    <?if($arItem["BLANKS"] && $arParams["SHOW_BLANKS"] == "Y"){?>
                        <h2 <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>Бланки Заявлений</h2>
                        <?foreach ($arItem["BLANKS"] as $blank):?>
                            <a href="<?=$blank["SRC"]?>" class="doc_item" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                                <div class="doc_pict <?=$blank["TYPE"]?>"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        <?=$blank["NAME"]?>
                                    </div>
                                    <div class="doc_note">
                                        <?=$blank["FILE_SIZE"]?>
                                    </div>
                                </div>
                            </a>
                        <?endforeach;?>
                    <?}?>
                <?}?>
            </div>
            <!--doc-->
        </div>
        <div class="rates_right_left">
            <h2>Контакты</h2>
            <?foreach($arResult["ITEMS"] as $arItem){?>
                <?if($arItem["LIST"]){?>
                    <?foreach($arItem["LIST"] as $arList){?>
                        <div class="form_application_line" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                            <?if(!$arList["NOT_USE_NAME"]){?>
                                <div class="right_bank_title">
                                    <?=$arList["NAME"]?>
                                </div>
                            <?}?>
                            <div class="contacts_block">
                                <?if($arList["EMAIL"]){?><a href="mailto:<?=$arList["EMAIL"]?>"><?=$arList["EMAIL"]?></a><br><?}?>
                                <?=$arList["PHONE"]?>
                            </div>
                        </div>
                    <?}?>
                <?}?>
            <?}?>
            <?if($arParams["ALL_PHONE"]){?>
                <div class="right_bank_title">Телефон прямой линии</div>
                <div class="form_application_line">
                    <div class="contacts_block">
                        <?=$arParams["ALL_PHONE"]?>
                    </div>
                    <div class="note_text">
                        звонок по России бесплатный
                    </div>
                </div>
            <?}?>
        </div>
    </div>
</div>
