<?php
//echo '<pre>' . print_r($arResult["ITEMS"], 1) . '</pre>';

// получаем разделы
$dbResSect = \CIBlockSection::GetList(
	Array("SORT"=>"ASC"),
	Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'])
);

//Получаем разделы и собираем в массив
while($sectRes = $dbResSect->GetNext())
{
	$arSections[] = $sectRes;
}



//Собираем  массив из Разделов и элементов
foreach($arSections as $arSection){

	foreach($arResult["ITEMS"] as $key=>$arItem){

		if($arItem['IBLOCK_SECTION_ID'] == $arSection['ID']){
			$arSection['ELEMENTS'][] =  $arItem;
		}
	}

	$arElementGroups[] = $arSection;

}

$arResult["ITEMS"] = $arElementGroups;

foreach ($arResult["ITEMS"]  as $k => $item)
{

	$arResult["SECTIONS"][$k]["NAME"] = $item["NAME"];
	foreach ($item["ELEMENTS"] as $key =>$element)
	{

		$arResult["SECTIONS"][$k]["FILES"][] = GetFile($element["NAME"], $element["DISPLAY_PROPERTIES"]["FILES"]["FILE_VALUE"]);
		$arResult["SECTIONS"][$k]["FILES"][$key]["CITY"] = $element["DISPLAY_PROPERTIES"]["CITY"]["VALUE"];

	}


}
