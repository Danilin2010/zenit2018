<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? //echo '<pre>' . print_r($arParams, 1) . '</pre>';?>
                <h2>Контакты</h2>
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <?if($arItem["LIST"]){?>
                        <?foreach($arItem["LIST"] as $arList){?>
                            <div class="form_application_line" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                                <?if(!$arList["NOT_USE_NAME"]){?>
                                <div class="right_bank_title">
                                    <?=$arList["NAME"]?>
                                </div>
                                <?}?>
                                <div class="contacts_block">
                                    <?if($arList["EMAIL"]){?><a href="mailto:<?=$arList["EMAIL"]?>"><?=$arList["EMAIL"]?></a><br><?}?>
                                    <?=$arList["PHONE"]?>
                                </div>
                            </div>
                        <?}?>
                    <?}?>
                <?}?>
                <?if($arParams["ALL_PHONE"]){?>
                <div class="right_bank_title">Телефон прямой линии</div>
                <div class="form_application_line">
                    <div class="contacts_block">
                        <?=$arParams["ALL_PHONE"]?>
                    </div>
                    <div class="note_text">
                        звонок по России бесплатный
                    </div>
                </div>
                <?}?>
