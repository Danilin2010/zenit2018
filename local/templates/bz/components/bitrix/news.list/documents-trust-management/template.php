<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container about-page">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-12 col-mt-0 col-md-12">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card pt-0">
                            <table class="tb">
                                <thead>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        дата размещения на сайте
                                    </td>
                                    <td>
                                        дата вступления в силу
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <?foreach($arResult["ITEMS"] as $arItem):?>
                                    <?
                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                    ?>
                                    <tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                        <td>
                                            <a href="<?=$arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>" target="_blank"><?=$arItem["NAME"]?></a> <?=$arItem["DETAIL_TEXT"]?>
                                        </td>
                                        <td>
                                            <?=$arItem["DISPLAY_PROPERTIES"]["DATE_POSTED_SITE"]["VALUE"]?>
                                        </td>
                                        <td>
                                            <?=$arItem["DISPLAY_PROPERTIES"]["DATE_ENTRY_INTO_FORCE"]["VALUE"]?>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                                </tbody>
                            </table>
                            <div class="note_text">
                                <p>
                                    Информация о стандартной стратегии управления и стандартном инвестиционном профиле представлена на странице каждого <a href="http://invest.zenit.ru/">Общего фонда банковского управления</a>.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>