<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global \CMain $APPLICATION */
/** @global \CUser $USER */
/** @global \CDatabase $DB */
/** @var \CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var \CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (!empty($arResult['ITEMS'])) {?>
<div class="wr_block_type">
	<!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab z-container">
		<?php foreach ($arResult['ITEMS'] as $arItem) { ?>
			<li><a href="#content-tabs-<?= $arItem['ID'] ?>"><?= $arItem['NAME'] ?></a></li>
		<?php } ?>
		</ul>
		<?php foreach ($arResult['ITEMS'] as $arItem) { ?>
		<div class="content_body" id="content-tabs-<?= $arItem['ID'] ?>">
			<div class="wr_block_type gray">
				<div class="block_type to_column z-container">
					<div class="block_type_right">
						<?= $arItem['PREVIEW_TEXT'] ?>
					</div>
					<div class="block_type_center">
						<?= $arItem['DETAIL_TEXT'] ?>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<!--content_tab-->
</div>
<?php } ?>