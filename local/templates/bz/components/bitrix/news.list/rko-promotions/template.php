<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? //echo '<pre>' . print_r($arResult["PROMOTIONS"], 1) . '</pre><br>';
?>
<ul class="content_tab c-container">
<?foreach($arResult["PROMOTIONS"] as $arItem):?>
	<li><a href="#content-tabs-<?=$arItem["ID"]?>"><?=$arItem["NAME"]?></a></li>
<?endforeach;?>
</ul>
<?foreach($arResult["PROMOTIONS"] as $arItem):?>
	<div class="content_body" id="content-tabs-<?=$arItem["ID"]?>">
		<div class="wr_block_type">
			<div class="block_type  c-container">
				<div class="block_type_center">
					<h1>Условия акции</h1>
					<!--conditions-->
					<div class="conditions">
						<div class="conditions_item">
							<div class="conditions_item_title">
								<?=$arItem["open_account"]?> <span>руб</span>
							</div>
							<div class="conditions_item_text">
								открытие счета
							</div>
						</div><div class="conditions_item">
							<div class="conditions_item_title">
								<?=$arItem["connection_to_cb_system"]?> <span>руб</span>
							</div>
							<div class="conditions_item_text">
								подключение к системе Клиент-Банк
							</div>
						</div><div class="conditions_item">
							<div class="conditions_item_title">
								<?=$arItem["service_to_cb_system"]?>
							</div>
							<div class="conditions_item_text">
								обслуживание системы Клиент-Банк
							</div>
						</div>
					</div>
					<!--conditions-->
					<div class="text_block">
						<?=$arItem["PREVIEW_TEXT"]?>
					</div>
				</div>
			</div>
		</div>
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_center">
					<h1>Как воспользоваться?</h1>
					<?$i = 0; foreach ($arItem["STEPS"] as $step): $i++?>
					<!--step_block-->
					<div class="step_block">
						<div class="step_item">
							<div class="step_num"><?=$i?></div>
							<div class="step_body">
								<div class="step_title">
									<?=$step["NAME"]?> 
								</div>
								<div class="step_text">
									<?=$step["PREVIEW_TEXT"]?>
								</div>
							</div>
						</div>
					</div>
					<!--step_block-->
					<?endforeach;?>
				</div>
			</div>
		</div>
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
				</div>
				<div class="block_type_center">
					<div class="warning">
						<div class="pict circle"></div>
						<div class="text">
							Дополнительную информацию о специальных условиях Вы можете получить, позвонив по телефону
							+7 (495) 937-09-94
						</div>
					</div>
				</div>
			</div>
		</div>
        <?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/inc/block/rates.php",
            Array(
                "template"=>"rates",
                "all_phone" => "8 (800) 500-66-77",
                "use_clientbank" => "Y",
                "show_blanks" => "N",
            ),
            Array("MODE"=>"txt","SHOW_BORDER"=>false)
        );?>
	</div>
<?endforeach;?>

