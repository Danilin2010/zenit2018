<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<?
    $count=round(count($arResult)/2,0)
    ?>

<ul class="block_menu">

<?
foreach($arResult as $key=>$arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;


    if($count==$key)
    {
        echo '</ul><ul class="block_menu">';
    }

?>
	<?if($arItem["SELECTED"]):?>
		<li><a href="<?=$arItem["LINK"]?>" class="selected <?=$arItem["LINK"]=='#modal_form-contact'?'open_modal':'';?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>" class="<?=$arItem["LINK"]=='#modal_form-contact'?'open_modal':'';?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>