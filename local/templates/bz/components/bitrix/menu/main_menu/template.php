<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <ul class="top_menu <?=$arParams["CLASS_MENU"]?>">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
		<li><a
                href="<?=$arItem["LINK"]?>"
               <?if($arItem["PARAMS"]["data-show"]){?>data-show="<?=$arItem["PARAMS"]["data-show"]?>"<?}?>
                <?if($arItem["SELECTED"]):?>class="selected"<?endif?>
                ><?=$arItem["TEXT"]?></a></li>

<?endforeach?>
</ul>
<?endif?>
