<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult)){?>
    <ul class="ul_top_line">
        <?foreach($arResult as $arItem){?>
            <li class="<?if($arItem["CHILDREN"]){?>button_select_top_menu<?}?>">
                <a class="<?if($arItem["SELECTED"]){?>select<?}?>" <?if(!$arItem["CHILDREN"]){?>href="<?=$arItem["LINK"]?>"<?}?>><?=$arItem["TEXT"]?></a>
                <?if($arItem["CHILDREN"]){?>
                    <ul>
                        <?foreach($arItem["CHILDREN"] as $val){?>
                            <?//if(!$val["SELECTED"]){?>
                                <li><a class="<?=($val['SELECTED'])?'selected':''?>" href="<?=$val["LINK"]?>" <?/*if($val["PARAMS"]["data-show"]){?>data-show="<?=$val["PARAMS"]["data-show"]?>"<?}*/?>><?=$val["TEXT"]?></a></li>
                            <?//}?>
                        <?}?>
                    </ul>
                <?}?>
            </li>
        <?}?>
    </ul>
<?}?>