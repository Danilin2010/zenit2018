<?

$newMenu=array();
foreach($arResult as $val)
{
    if($val["PARAMS"]["data-group"]){
        $name=$val["PARAMS"]["data-group"];
        if($newMenu[count($newMenu)-1]["TEXT"]!=$name)
        {
            $newMenu[]=array(
                "TEXT"=>$name,
                "CHILDREN"=>array(),
            );
        }
        $newMenu[count($newMenu)-1]["CHILDREN"][]=$val;
    }else{
        $newMenu[]=$val;
    }
}

foreach($newMenu as &$val)
{
    if($val["CHILDREN"]){
        foreach($val["CHILDREN"] as &$child)
        {
            if($child["SELECTED"])
            {
                $val["TEXT"]=$child["TEXT"];
                $val["SELECTED"]=$child["SELECTED"];
            }
        }
    }
}unset($val);
$arResult=$newMenu;