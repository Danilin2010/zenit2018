<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult)){?>
    <div class="mobail_bottom_menu">
        <div class="ul_mobail_bottom_menu">
            <?foreach($arResult as $arItem){
            ?><div class="ul_mobail_bottom_menu_item">
                <a <?if($arItem["SELECTED"]){?>class="select"<?}?><?if($arItem["PARAMS"]["data-show"]){?>data-show="<?=$arItem["PARAMS"]["data-show"]?>"<?}?>><?=$arItem["TEXT"]?></a>
            </div><?
            }?>
        </div>
    </div>
<?}?>
