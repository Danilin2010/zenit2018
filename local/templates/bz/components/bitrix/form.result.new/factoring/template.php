<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<?
if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
    && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
    $ajax = "Y";
}

if ($ajax == "Y")
{
    $APPLICATION->RestartBuffer();
    $errors = false;
    if ($arResult["isFormErrors"] == "Y")
    {
        $errors = $arResult["FORM_ERRORS_TEXT"];
        $errors = str_replace(array(
            '<p><font class="errortext">',
            '</font></p>'
        ), "", $errors);
        $errors = str_replace("<br>", "</li><li>", $errors);
        $errors = "<ul><li>" . $errors . "</li></ul>";
    }
    if ($errors)
    {
        $arr = array(
            'errors' => $errors,
            'result_id' => (int)$_REQUEST["RESULT_ID"]
        );
    }
    else
    {
        $arr = array('result_id' => (int)$_REQUEST["RESULT_ID"]);
    }
    echo json_encode($arr);
    die();
}


?>

<? if ($arResult["isFormNote"] != "Y" /*|| $arResult["isFormErrors"] != "Y"*/){?>
    <div class="block_type to_column c-container reversal">
        <div class="block_type_right">
            <div class="filter_block_title right">
                Онлайн-заявка
            </div>
            <!--step_filter_block-->
            <div class="wr_step_filter_block_b">
                <div class="wr_step_filter_block">
                    <div class="step_filter_block">
                        <div class="filter_block">

                            <div class="filter_block_body">
                                <div class="time_icon"></div>
                                <div class="form_note">
                                    <?=$arParams["~RIGHT_TEXT"]?>
                                </div>
                                <div class="form_note bottom">
                                    <div class="form_note_left">
                                        *
                                    </div>
                                    <div class="form_note_right">
                                        Все поля обязательны<br/>
                                        для заполнения
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><div class="step_filter_block">
                        <div class="filter_block">

                            <div class="filter_block_body">
                                <div class="new_icon"></div>
                                <div class="form_note">
                                    Подготовьте необходимые документы<br/>
                                    для предоставления в банковское<br/>
                                    отделение.
                                </div>
                            </div>
                            <div class="filter_block_body">
                                <div class="dom_icon"></div>
                                <div class="form_note">
                                    Посетите удобное отделение банка<br/>
                                    для завершения оформления<br/>
                                    выбранной услуги.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--step_filter_block-->
        </div>
        <div class="block_type_center">
            <div class="form_errors_text"><? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?></div>
            <!--form_application-->
            <div class="wr_form_application">
                <div class="filter_block_title">
                    Онлайн-заявка
                </div>
                <div class="step_form_application first_step">
                    <?= $arResult["FORM_HEADER"] ?>
                    <?
                    $q = $arResult['QUESTIONS'];
                    // print_r($q['city']['STRUCTURE'][0]['ID']);
                    ?>
                    <?/*<form action="" class="validate application">*/?>
  <div class="step_form_application first_step"> 
    <div class="wr_complex_input complex_input_noicon "> 
      <div class="complex_input_body"> 
        <div class="text"><?=$FORM->ShowInputCaption("name_company","Название компании")?></div>
       <?=$FORM->ShowInput('name_company')?> 		</div>
     </div>
     
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon "> 	 
      <div class="complex_input_body"> 		 
        <div class="text"><?=$FORM->ShowInputCaption("name_fio","Контактное лицо(ФИО)")?></div>
       <?=$FORM->ShowInput('name_fio')?> 		</div>
     	</div>
   		   
    <br />

    <div class="wr_complex_input complex_input_noicon "> 	 
      <div class="complex_input_body"> 	 
        <div class="text"><?=$FORM->ShowInputCaption("telefon","Телефон")?></div>
       <?=$FORM->ShowInput('telefon')?> 		</div>
     	</div>
   		   
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon "> 	 
      <div class="complex_input_body"> 	 
        <div class="text"> <?=$FORM->ShowInputCaption("email","Адрес e-mail")?></div>
       <?=$FORM->ShowInput('email')?> 		</div>
     	</div>
   		   
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon "> 	 
      <div class="complex_input_body"> 
        <div class="text"> <?=$FORM->ShowInputCaption("area_work","Сфера деятельности компании")?></div>
       <?=$FORM->ShowInput('area_work')?> 		</div>
     	</div>
   		   
    <br />
   
    <div class="wr_complex_input complex_input_noicon " > 
      <br />
     	 
      <div class="text"><?=$FORM->ShowInputCaption("finans_size","Требуемый объем финансирования по факторингу (рубли)")?> </div>
     
      <br />
     	 
      <div class="complex_input_body">	 <?=$FORM->ShowInput('finans_size')?> 	</div>
     	</div>
   		 
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon " > 
      <br />
     	 
      <div class="text"><?=$FORM->ShowInputCaption("debitor_vol","Количество дебиторов ")?></div>
     
      <br />
     
      <div class="complex_input_body"> <?=$FORM->ShowInput('debitor_vol')?> 	</div>
     	</div>
   		 
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon " > 
      <div class="text"> <?=$FORM->ShowInputCaption("main_debitor","Основные дебиторы")?></div>
     
      <br />
     
      <br />
     	 
      <div class="complex_input_body"> <?=$FORM->ShowInput('main_debitor')?> 		</div>
     	</div>
   		 
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon " > 
      <br />
     	 
      <div class="text"><?=$FORM->ShowInputCaption("payment_deferred","Средняя отсрочка платежа ")?></div>
     
      <br />
     
      <div class="complex_input_body"> <?=$FORM->ShowInput('payment_deferred')?> 	</div>
     	</div>
   		 
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon " > 
      <br />
     
      <div class="text"><?=$FORM->ShowInputCaption("company_location","Расположение компании")?> </div>
     
      <br />
     
      <div class="complex_input_body"><?=$FORM->ShowInput('company_location')?></div>
     	</div>
   		 
    <br />
   	 
    <div class="wr_complex_input complex_input_noicon "> 	 
      <div class="complex_input_body"> 
        <div class="text"> <?=$FORM->ShowInputCaption("additionally","")?></div>
       <?=$FORM->ShowInput('additionally')?> 		</div>
     	</div>
   		 
    <br />
   
    <div style="text-align: center;"> <?=$FORM->ShowInput('soglasie')?><?=$FORM->ShowRequired()?></div>
   
    <br />
   
    <div style="text-align: center;">   </div>
   
    <div style="text-align: center;"> <?=$FORM->ShowSubmitButton("Отправить","button")?><?=$FORM->ShowResetButton("Очистить","button")?></div>
   </div>
					
                    <?= $arResult["FORM_FOOTER"] ?>
                </div>
                <div class="step_form_application to_step">
                    <div class="form_application">
                        <div class="form_application_ok"></div>
                        <div class="form_application_title">
                            Ваша заявка принята!
                        </div>
                        <div class="form_application_text">
							В ближайшее время мы свяжемся с вами, чтобы уточнить информацию.
                            <!--Номер заявки<b data-result></b>. Подтверждение отправлено на e-mall. Срок<br/>
                            рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                            свяжется с Вами в ближайшее время.-->
                        </div>
                    </div>
                </div>
            </div>
            <!--form_application-->
        </div>
    </div>
<?}else{?>
    <div class="block_type to_column">
        <div class="block_type_right">
            <!--step_filter_block-->
            <div class="wr_step_filter_block_b">
                <div class="wr_step_filter_block">
                    <div class="step_filter_block">
                        <div class="filter_block">
                              <div class="filter_block_body">
                                <div class="new_icon"></div>
                                <div class="form_note">
                                    Подготовьте необходимые документы<br/>
                                    для предоставления в банковское<br/>
                                    отделение.
                                </div>
                            </div>
                            <div class="filter_block_body">
                                <div class="dom_icon"></div>
                                <div class="form_note">
                                    Посетите удобное отделение банка<br/>
                                    для завершения оформления<br/>
                                    выбранной услуги.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--step_filter_block-->
        </div>
        <div class="block_type_center">
            <div class="wr_form_application">
                <div class="form_application">
                    <div class="form_application_ok alt"></div>
                    <div class="form_application_title">
                        Ваша заявка принята!
                    </div>
                    <div class="form_application_text">
						В ближайшее время мы свяжемся с вами, чтобы уточнить информацию.
						<!--Номер заявки <b><?/*=(int)$_REQUEST["RESULT_ID"]*/?></b>. Подтверждение отправлено на e-mall. Срок<br/>
                        рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                        свяжется с Вами в ближайшее время.-->
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>



