<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<?
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $ajax = "Y";
}
if ($ajax == "Y") {
    $APPLICATION->RestartBuffer();
    $errors = false;
    if ($arResult["isFormErrors"] == "Y") {
        $errors = $arResult["FORM_ERRORS_TEXT"];
        $errors = str_replace(array('<p><font class="errortext">', '</font></p>'), "", $errors);
        $errors = str_replace("<br>", "</li><li>", $errors);
        $errors = "<ul><li>" . $errors . "</li></ul>";
    }
    if ($errors) {
        $arr = array('errors' => $errors, 'result_id' => (int)$_REQUEST["RESULT_ID"]);
    } else {
        $arr = array('result_id' => (int)$_REQUEST["RESULT_ID"]);
    }
    echo json_encode($arr);
    die();
}
?>
<? if ($arResult["isFormNote"] != "Y") { ?>
    <? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>
    <!--form_application-->
    <div class="big_wr_form_application">
        <?= $arResult["FORM_HEADER"] ?>
        <input type="hidden" name="web_form_apply" value="Y"/>
        <? $q = $arResult['QUESTIONS']; ?>
        <div class="snippet-online-reserve">
            <div class="snippet-online-reserve-body">
                <div class="snippet-online-reserve-text">
                    Для того чтобы воспользоваться
                    сервисом Вам необходимо:
                </div>
                <div class="snippet-online-reserve-ul">
                    <div class="snippet-online-reserve-li">
                        ознакомиться с условиями предоставления
                        сервиса
                    </div>
                    <div class="snippet-online-reserve-li">
                        подтвердить согласие с условиями
                        предоставления сервиса;
                    </div>
                    <div class="snippet-online-reserve-li">
                        заполнить краткую анкету.
                    </div>
                </div>
                <div class="snippet-online-reserve-chek">
                    <label>
                        <?= $q['agreement']['HTML_CODE'] ?>
                        <? /*Согласен с <a href="https://www.zenit.ru/media/rus/content/pdf/personal/pd_confirm.pdf" target="_blank">условиями</a>*/ ?>
                        Согласен с <a class="open_modal" href="#modal_form-agreement">условиями</a>
                    </label>
                </div>
                <div class="snippet-online-reserve-button">
                    <input class="button" value="Зарезервировать" type="submit">
                </div>
            </div>
        </div>
        <div class="form_errors_text"><? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?></div>
        <div class="wr_form_application online-reserve-applicatio">
            <div class="step_form_application first_step">
                <div class="form_application">
                    <h3>Выберите вид юридического лица</h3>
                    <div class="form_application_line">
                        <? showType($q['type'], $arResult["arrVALUES"]) ?>
                    </div>
                </div>
                <div class="form_application">
                    <h3>О компании</h3>
                    <?
                    wrFormShowProp(array('full_name_of_ul', 'abbreviated_name', 'organizational_form',), $q, $arResult["arrVALUES"], 'legal');
                    ?>
                    <?
                    wrFormShowProp(array('full_name_individual_entrepreneur', 'short_name_of_pi',), $q, $arResult["arrVALUES"], 'ip');
                    ?>
                    <h3>Данные компании</h3>
                    <div class="form_application_line">
                        <div class="form_application_to">
                            <? FormShowProp($q['tin'], $arResult["arrVALUES"]); ?>
                        </div>
                        <div class="form_application_to" data-type-line="legal">
                            <? FormShowProp($q['ogrn'], $arResult["arrVALUES"]); ?>
                        </div>
                        <div class="form_application_to" data-type-line="ip">
                            <? FormShowProp($q['ogrnip'], $arResult["arrVALUES"]); ?>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_to" data-type-line="legal">
                            <? FormShowProp($q['type_activity'], $arResult["arrVALUES"]); ?>
                        </div>
                        <div class="form_application_to">
                            <? FormShowProp($q['annual_revenue_volume'], $arResult["arrVALUES"]); ?>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_to">
                            <? FormSityProp($q['city'], $arResult["arrVALUES"]); ?>
                        </div>
                        <div class="form_application_to">
                            <? FormOfficesProp($q['office'], $arResult["arrVALUES"]); ?>
                        </div>
                    </div>
                    <h3>Контактная информация</h3>
                    <div class="form_application_line">
                        <div class="form_application_to">
                            <? FormShowProp($q['phone'], $arResult["arrVALUES"]); ?>
                        </div>
                        <div class="form_application_to">
                            <? FormShowProp($q['email'], $arResult["arrVALUES"]); ?>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <? //=$q['comments']['HTML_CODE']?>
                        <? FormShowProp($q['comments'], $arResult["arrVALUES"]); ?>
                    </div>
                    <div class="form_application_line form_text">
                        Для резервирования номера счета, необходимо получить код подтверждения,
                        который будет направлен на указанный Вами адрес электронной почты.
                        Код действителен в течение 15 минут!
                    </div>
                    <div class="form_application_line confirmation_block">
                        <div class="form_application_to">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">Код подтверждения</div>
                                    <input type="text" class="simple_input" autocomplete="off" name="confirmation"
                                           value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_to">
                            <input data-setcode class="button" value="Получить код" type="button">
                        </div>
                    </div>
                    <div class="form_application_line buttom_line">
                        <div class="form_application_line buttom_line_captcha">
                            <?
                            if ($arResult["isUseCaptcha"] == "Y") {
                                ?>
                                <input type="hidden"
                                       name="captcha_sid"
                                       value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"
                                     width="180" height="40"/>

                                <input type="text" name="captcha_word" size="30" maxlength="50" value=""
                                       class="inputtext"/>
                                <?
                            } // isUseCaptcha
                            ?>
                            <div id="recaptchaErrop" class="diverror">Подтвердите, что вы не робот</div>
                        </div>
                        <div class="form_application_line buttom_line_button">
                            <input class="button" value="Продолжить" type="submit">
                        </div>
                    </div>
                </div>
                <?= $arResult["FORM_FOOTER"] ?>
            </div>
            <div class="step_form_application to_step">
                <div class="form_application">
                    <div class="form_application_ok"></div>
                    <div class="form_application_title">
                        Ваша заявка принята!
                    </div>
                    <div class="form_application_text">
                        Номер заявки <b data-result></b>. Подтверждение отправлено на e-mall. Срок<br/>
                        рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                        свяжется с Вами в ближайшее время.
                    </div>
                </div>
            </div>
        </div>
        <?= $arResult["FORM_FOOTER"] ?>
        <!--form_application-->
    </div>
<? } else { ?>
    <div class="big_wr_form_application">
        <div class="form_application">
            <div class="form_application_ok"></div>
            <div class="form_application_title">
                Ваша заявка принята!
            </div>
            <div class="form_application_text">
                Номер заявки <b data-result></b>. Подтверждение отправлено на e-mall. Срок<br/>
                рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                свяжется с Вами в ближайшее время.
            </div>
        </div>
    </div>
<? } ?>


