$(document).ready(function(){

    var left_to_step='100%';
    var first_to_step='0px';
    var timer=500;
    var padding=40;
    padding=0;

    function MarginResize()
    {
        var to_step=$('.to_step');
        var first_step=$('.first_step');
        var position_to_step = $('.wr_form_application').offset();
        var to_step_=$(window).width()-position_to_step.left-padding + 100;
        left_to_step=to_step_+'px';
        first_to_step='-'+(position_to_step.left+first_step.width()-padding)+'px';
        if(!to_step.hasClass('win'))
            to_step.css('margin-left',left_to_step);
        if(first_step.hasClass('win'))
            first_step.css('left',first_to_step);
    }

    setTimeout(function(){MarginResize();},500);

    $(window).resize(function() {
        MarginResize();
    });


    function ChekType(active){
        if(active) {
            $("[data-type-line=legal]").show();
            $("[data-type-line=ip]").hide();
        }else {
            $("[data-type-line=legal]").hide();
            $("[data-type-line=ip]").show();
        }
    }
    $("[data-toggle-type]").on('toggle', function(e, active) {
        var name=$(this).data('name');
        var N=$(this).data('checkedN');
        var Y=$(this).data('checkedY');
        if(active)
            $('input[name='+name+']').val(Y);
        else
            $('input[name='+name+']').val(N);
        ChekType(active);
    });
    ChekType($("[data-toggle-type]").data('toggles').active);


    function TriggerForm()
    {
        $('#select_form_text_43, #select_form_text_44').trigger('refresh');
        $('#select_form_text_43, #select_form_text_44').each(function(){
            var title=$(this).data('title');
            if(typeof (title)!='undefined')
                $(this).next('.jq-selectbox__select').prepend('<div class="select_title">'+title+'</div>');
            else
                $(this).next('.jq-selectbox__select').prepend('<div class="select_title"></div>');
            var id=$(this).data('id');
            var val=$(this).find('option:selected').text();
            $('#'+id).val(val);
        });
    }
    $("#select_form_text_44").chained("#select_form_text_43");
    $(document).on('change', '#select_form_text_43, #select_form_text_44', function () {
        TriggerForm();
    });
    TriggerForm();

    $('input[name="form_email_41"]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        showMaskOnFocus: false,
        showMaskOnHover: false,
        'placeholder':''
    });

    $("input[name=form_text_39]").inputmask({
        mask:"+7 (999) 999-99-99",
        showMaskOnHover: false,
        //showMaskOnFocus: false,
        placeholder:'+7 (   )    -  -  '
    });

    $("input[name=form_text_32]").inputmask({
        mask:"9",
        repeat: 13,
        greedy: false,
        showMaskOnHover: false,
        placeholder:''
    });

    $("input[name=form_text_33]").inputmask({
        mask:"9",
        repeat: 15,
        greedy: false,
        showMaskOnHover: false,
        placeholder:''
    });





    $('.snippet-online-reserve-chek input[type=checkbox]').on('change',function (e) {
        if($(this).prop('checked'))
        {
            $('.online-reserve-applicatio').stop().slideDown();
        }else{
            $('.online-reserve-applicatio').stop().slideUp();
        }
    });

    $.validator.addMethod("type_ip", function (value, element) {
        return value.length || $('[name="form_dropdown_type"]').val()==20;
    },"Это поле необходимо заполнить.");
    $.validator.addMethod("type_legal", function (value, element) {
        return value.length || $('[name="form_dropdown_type"]').val()==19;
    },"Это поле необходимо заполнить.");

    $('[data-setcode]').on('click',function (e) {
        var url='/ajax/confirmation/get.php';
        var email=$('[name=form_email_41]').val();
        $.ajax({
            url: url,
            type: "POST",
            data: {'email':email},
            dataType: 'json',
            success: function(data){

            }
        });
        $(this).val('Получить код еще раз');
    });

    $("form[name=account_number]").validate({
        errorPlacement: function(error, element) {
            var parent=element.parents('.wr_complex_input');
            var parentcheckbox=element.parents('.jq-checkbox');
            var parentto=element.parents('.form_application_to');
            if(parent.length>0) {
                parent.addClass('error');
                error.insertAfter(parent);
            }else if(parentcheckbox.length>0){
                parentcheckbox.addClass('error');
                error.insertAfter(parentcheckbox);
            }else if(parentto.length>0){
                parentto.append(error);
            }else{
                error.insertAfter(element);
            }
        },
        success: function(error){
            error.prev('.jq-checkbox').removeClass('error');
            error.prev('.wr_complex_input').removeClass('error');
            error.remove();
        },
        rules:{
            form_dropdown_type: {
                required: true
            },
            form_text_29: {
                type_ip: true
            },
            form_text_30: {
                type_ip: true
            },
            form_text_31: {
                required: true,
                inn:true
            },
            form_text_33: {
                type_ip: true,
                maxlength: 15,
                minlength: 15
            },
            form_dropdown_annual_revenue_volume: {
                required: true
            },
            form_text_43: {
                required: true
            },
            form_text_44: {
                required: true
            },
            form_text_39: {
                required: true,
                pattern: /^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/
            },
            form_email_41: {
                required: true,
                email2: true
            },
            form_dropdown_type_activity: {
                type_legal: true
            },
            form_text_32: {
                type_legal: true,
                maxlength: 13,
                minlength: 13
            },
            form_dropdown_organizational_form: {
                type_legal: true
            },
            form_text_22: {
                type_legal: true
            },
            form_text_21: {
                type_legal: true
            },
            'form_checkbox_agreement[]': {
                required: true
            },
            confirmation:{
                required: true,
                remote:{
                    url: "/ajax/confirmation/set.php",
                    type: "post",
                    data: {
                        email: function() {
                            return $('[name=form_email_41]').val();
                        }
                    }
                }
            }
        },
        messages:{
            confirmation:{
                required: 'Введите полученный код',
                remote: 'Код неверный или его действие истекло'
            },
            'form_checkbox_agreement[]': {
                required: 'Необходимо дать согласие'
            },
        },
        submitHandler: function(form) {
            var $form = $(form);
            var url='/ajax/form/reserve.php';
            if($('.confirmation_block').hasClass('show'))
            {
                if($('.buttom_line_captcha').hasClass('show'))
                {
                    if (grecaptcha.getResponse()){
                        $('#recaptchaErrop').hide();
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: $form.serialize(),
                            dataType: 'json',
                            success: function(data){
                                if(data.errors)
                                {
                                    $('.form_errors_text').html(data.errors);
                                }else{
                                    if(data.result_id>0)
                                        $('[data-result]').text(data.result_id);
                                    MarginResize();
                                    $('.first_step').addClass('win').animate({
                                        'left': first_to_step,
                                        'height': $('.to_step').height()+'px'
                                    },timer);
                                    $('.to_step').addClass('win').animate({'margin-left': "0px"},timer);
                                    var offset =$('.to_step').offset();
                                    var topel = offset.top;
                                    $('body').animate({ scrollTop: topel },1500);
                                }
                            }
                        });
                    }else{
                        $('#recaptchaErrop').show();
                    }
                }else{
                    $('.buttom_line_captcha').addClass('show');
                }
            }else{
                $('.confirmation_block').addClass('show');
            }
        }
    });

});