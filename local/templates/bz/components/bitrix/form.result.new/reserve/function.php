<?

function FormShowProp($QUESTION,$arrVALUES)
{
    if(count($QUESTION["STRUCTURE"])>1)
    {
        echo $QUESTION['HTML_CODE'];
    }else{
        $STRUCTURE=$QUESTION["STRUCTURE"][0];
        $val=$arrVALUES["form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]];
        ?>
        <div class="wr_complex_input complex_input_noicon <?if(strlen($val)>0){?>use<?}?>">
            <div class="complex_input_body">
                <div class="text"><?=$QUESTION['CAPTION']?></div>
                <?=$QUESTION['HTML_CODE']?>
            </div>
        </div>
        <?
    }
    //data-plasholder="Выберите" data-title="Программа" class="formstyle"
}

function wrFormShowProp($questions,$q,$arrVALUES,$type)
{
    foreach($questions as $val){
        ?><div class="form_application_line" data-type-line="<?=$type?>">
            <?FormShowProp($q[$val],$arrVALUES);?>
        </div><?
    }
}

function showType($questions,$arrVALUES)
{
    $ip=$questions["STRUCTURE"][0];
    $legal=$questions["STRUCTURE"][1];
    $form_dropdown_type=(int)$arrVALUES["form_dropdown_type"];
    if($form_dropdown_type<=0)
        $form_dropdown_type=$ip["ID"];
    $checked="Y";
    if($form_dropdown_type==$ip["ID"])
        $checked="N";
    ?>
    <input type="hidden"
           name="form_<?=$ip['FIELD_TYPE']?>_<?="type"?>"
           value="<?=$form_dropdown_type?>">
    <div class="wr-toggle-light-text">
        <div class="toggle-light-text <?if($form_dropdown_type==$ip["ID"]){?>on<?}else{?>off<?}?>"><?=$ip["MESSAGE"]?></div>
        <div class="toggle-light-wr">
            <div class="toggle toggle-light"
                 data-toggle data-toggle-type
                 data-checked-n="<?=$ip["ID"]?>" data-checked-y="<?=$legal["ID"]?>"
                 data-checked="<?=$checked?>" data-name="form_<?=$ip['FIELD_TYPE']?>_<?="type"?>"></div>
        </div>
        <div class="toggle-light-text <?if($form_dropdown_type==$legal["ID"]){?>on<?}else{?>off<?}?>"><?=$legal["MESSAGE"]?></div>
    </div>
    <?
}


function FormSityProp($QUESTION,$arrVALUES)
{
    $list=array();
    if (Bitrix\Main\Loader::includeModule('aic.bz'))
    {
        $Geo=new \Aic\Bz\cGeo();
        $list=$Geo->GetSimpleSity();
    }
    $STRUCTURE=$QUESTION["STRUCTURE"][0];
    $val=$arrVALUES["form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]];
    if(strlen($val)<=0)
        $val=GetSity();
    ?>
    <input type="hidden"
           name="form_<?=$STRUCTURE['FIELD_TYPE']?>_<?="type"?>"
           id="form_<?=$STRUCTURE['FIELD_TYPE']?>_<?="type"?>"
           value="<?=$val?>">
    <select id="<?="select_form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]?>"
            data-id="<?="form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]?>"
            name="<?="form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]?>"
            data-plasholder="<?=$QUESTION['CAPTION']?>"
            data-title="<?=$QUESTION['CAPTION']?>" class="formstyle">
        <?foreach($list as $v){?>
            <option <?if($val==$v["NAME"]){?>selected<?}?> value="id_<?=$v["ID"]?>"><?=$v["NAME"]?></option>
        <?}?>
    </select>
    <?
}

function FormOfficesProp($QUESTION,$arrVALUES)
{
    $list=array();
    if (Bitrix\Main\Loader::includeModule('aic.bz'))
    {
        $Geo=new \Aic\Bz\cGeo();
        $list=$Geo->GetSimpleOffices();
    }
    $STRUCTURE=$QUESTION["STRUCTURE"][0];
    $val=$arrVALUES["form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]];
    ?>
    <input type="hidden"
           name="form_<?=$STRUCTURE['FIELD_TYPE']?>_<?="type"?>"
           id="form_<?=$STRUCTURE['FIELD_TYPE']?>_<?="type"?>"
           value="<?=$val?>">
    <select id="<?="select_form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]?>"
            data-id="<?="form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]?>"
            name="<?="form_".$STRUCTURE["FIELD_TYPE"]."_".$STRUCTURE["ID"]?>"
            data-plasholder="<?=$QUESTION['CAPTION']?>"
            data-title="<?=$QUESTION['CAPTION']?>" class="formstyle">
        <?foreach($list as $v){?>
            <option <?if($val==$v["NAME"]){?>selected<?}?> value="id_<?=$v["ID"]?>" data-chained="id_<?=$v["CITY_ID"]?>"><?=$v["NAME"]?></option>
        <?}?>
    </select>
    <?
}