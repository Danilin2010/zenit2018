<?

class SponsorshipForm
{
    public static function showProp($QUESTION, $arrVALUES)
    {
        $STRUCTURE = $QUESTION["STRUCTURE"][0];
        $val = $arrVALUES["form_" . $STRUCTURE["FIELD_TYPE"] . "_" . $STRUCTURE["ID"]];
        ?>
        <div class="wr_complex_input complex_input_noicon <? if (strlen($val) > 0) {
            ?>use<?
        } ?>">
            <div class="complex_input_body">
                <div class="text"><?= $QUESTION['CAPTION'] ?></div>
                <?= $QUESTION['HTML_CODE'] ?>
            </div>
        </div>
        <?
    }
}

