<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arTemplateParameters = array(
	"SOURCE_TREATMENT" => Array(
		"NAME" => GetMessage("SOURCE_TREATMENT"),
		"TYPE" => "STRING",
	),
    "RIGHT_TEXT" => Array(
        "NAME" => GetMessage("RIGHT_TEXT"),
        "TYPE" => "STRING",
    ),
);
?>
