<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<?
if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
    && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
    $ajax = "Y";
}

if ($ajax == "Y")
{
    $APPLICATION->RestartBuffer();
    $errors = false;
    if ($arResult["isFormErrors"] == "Y")
    {
        $errors = $arResult["FORM_ERRORS_TEXT"];
        $errors = str_replace(array(
            '<p><font class="errortext">',
            '</font></p>'
        ), "", $errors);
        //$errors = str_replace($errors);
        //$errors = '<ul><li>' . $errors . '</li></ul>';
    }
    if ($errors)
    {
        $arr = array(
            'errors' => $errors,
            'result_id' => (int)$_REQUEST["RESULT_ID"]
        );
    }
    else
    {
        $arr = array('result_id' => (int)$_REQUEST["RESULT_ID"]);
    }
    echo json_encode($arr);
    die();
}
?>
<!-- FORM: contact -->
<? if ($arResult["isFormNote"] != "Y" /*|| $arResult["isFormErrors"] != "Y"*/){?>
    <div class="modal-open modal form">
        <div id="modal_form-contact" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-sm-12 col-md-12 modal_fon px-4">
                        <div class="row first_step">
                            <div class="col-sm-12 col-md-12">
                                <div class="form_errors_text">
                                    <? if ($arResult["isFormErrors"] == "Y"): ?>
                                        <?= $arResult["FORM_ERRORS_TEXT"]; ?>
                                    <? endif; ?>
                                </div>
                            </div>
                            <?= $arResult["FORM_HEADER"] ?>
                            <? $q = $arResult['QUESTIONS']; ?>
                            <input type="hidden" name="web_form_apply" value="Y"/>
                            <input type="hidden" name="form_<?=$q['source']['STRUCTURE'][0]['FIELD_TYPE']?>_<?=$q['source']['STRUCTURE'][0]["ID"]?>"
                                   value="<?=$arParams["SOURCE_TREATMENT"]?>">

                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <h3>Задать вопрос</h3>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <?FormShowPropSelect($q['topic'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <?FormShowProp($q['user_name'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <?FormShowProp($q['phone'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <?FormShowProp($q['email_contact'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <?FormShowProp($q['your_question'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="col-sm-8 col-md-8">
                                <div class="form_application_button">
                                    <label><?= $q['agreement']['HTML_CODE'] ?>Согласен с <a target="_blank" href="https://www.zenit.ru/media/rus/content/pdf/personal/pd_confirm.pdf">условиями</a> обработки данных</label>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form_application_button_right">
                                    <input class="button" value="отправить" type="submit">
                                </div>
                            </div>
                            <?= $arResult["FORM_FOOTER"] ?>
                        </div>
                        <div class="step_form to_step">
                            <div class="block_type to_column">
                                <div class="block_type_center">
                                    <div class="wr_form_application">
                                        <div class="form_application">
                                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                                            <div class="form_application_ok alt"></div>
                                            <div class="form_application_title">
                                                Ваш вопрос принят!
                                            </div>
                                            <div class="form_application_text">
                                                Представитель банка свяжется с Вами в ближайшее время.<br/>
                                                Благодарим за обращение.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}else{?>
    <div class="block_type to_column">
        <div class="block_type_center">
            <div class="wr_form_application">
                <div class="form_application">
                    <div class="form_application_ok alt"></div>
                    <div class="form_application_title">
                        Ваш вопрос принят!
                    </div>
                    <div class="form_application_text">
                        Представитель банка свяжется с Вами в ближайшее время.<br/>
                        Благодарим за обращение.
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>
