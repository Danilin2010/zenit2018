<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

$arTemplateParameters['CSS_CLASS'] = array(
	'NAME' => 'CSS Class',
	'DEFAULT' => '',
	'TYPE' => 'STRING',
	'MULTIPLE' => 'N',
	'COLS' => 25,
	'REFRESH'=> 'Y',
);
