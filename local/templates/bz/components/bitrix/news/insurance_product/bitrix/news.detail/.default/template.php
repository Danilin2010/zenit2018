<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<? if ($arResult["PREVIEW_TEXT"]): ?>
					<h2><?= $arResult["PREVIEW"] ?></h2>
				<? endif; ?>
				<? if ($arResult["DETAIL_TEXT"]): ?>
					<p>
						<?= $arResult["DETAIL_TEXT"] ?>
					</p>
				<? endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	<!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Преимущества</a></li>
			<li><a href="#content-tabs-2">Основные параметры</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
				<div class="block_type to_column c-container">
					<div class="block_type_right"></div>
					<div class="block_type_center">
						<?=$arResult["PROPERTIES"]["advantage"]["~VALUE"]["TEXT"]?>
					</div>
				</div>
		</div>
		<div class="content_body" id="content-tabs-2">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<?=$arResult["PROPERTIES"]["parametres"]["~VALUE"]["TEXT"]?>
					</div>
				</div>
		</div>
	</div>
</div>