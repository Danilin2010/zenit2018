<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//echo '<pre>' . print_r($arResult, 1) . '</pre>';?>
<div class="div_mini_filter show_min">
	<div class="div_mini_filter_title">
		Фильтр
		<div class="arr"></div>
	</div>
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" id="news-filter-mini">
	<div class="div_mini_filter_body">
		<? foreach ($arResult["ITEMS"] as $arItem):
			if (array_key_exists("HIDDEN", $arItem)):
				echo $arItem["INPUT"];
			endif;
		endforeach; ?>
		<!--filter_block-->
		<?foreach($arResult["ITEMS"] as $arItem){?>
		<? if (!array_key_exists("HIDDEN", $arItem)): ?>
		<div class="filter_block">
			<div class="filter_block_title">
				<?=$arItem["NAME"]?>
			</div>
			<div class="filter_block_body">
				<?foreach ($arItem["LIST"] as $id => $list) {?>
				<div class="filter_block_body_item">
					<label><input
							class="formstyle"
							type="<?=strtolower($arItem["TYPE"])?>"
							name="<?=$arItem["INPUT_NAME"]?>[]"
							value="<?=$id?>"/><?=$list?></label>
				</div>
				<?}?>
			</div>
		</div>
		<?endif;?>
		<!--filter_block-->
		<?}?>
		<!--filter_block-->
		<div class="filter_block">
			<div class="filter_block_title">
				По годам
			</div>
			<div class="filter_block_body">
				<div class="filter_block_body_item">
					<label><input
							class="formstyle"
							type="radio"
							name="year"
							checked
							value="1"/>Все</label>
				</div>
				<?foreach ($arResult["YEARS"] as $year):?>
					<div class="filter_block_body_item">
						<label><input
								class="formstyle"
								type="radio"
								name="year"
								value="<?=$year?>"/><?=$year?></label>
					</div>
				<?endforeach;?>
			</div>
		</div>
		<!--filter_block-->
		<div class="form_application_line">
			<input class="button bigmaxwidth" value="Применить фильтр" type="submit">
		</div>
	</div>
		</form>
</div>





<div class="block_type_right">
	<div class="wr_form_block">
		<div class="right_top_line">
			<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" id="news-filter">
			<div class="block_top_line hide_min"></div>
			<!--filter_block-->
			<div class="filter_block hide_min">
				<? foreach ($arResult["ITEMS"] as $arItem):
					if (array_key_exists("HIDDEN", $arItem)):
						echo $arItem["INPUT"];
					endif;
				endforeach; ?>
				<?foreach($arResult["ITEMS"] as $arItem){?>
					<? if (!array_key_exists("HIDDEN", $arItem)): ?>
				<div class="filter_block_title">
					<?=$arItem["NAME"]?>
				</div>
				<div class="filter_block_body">
					<?foreach ($arItem["LIST"] as $id => $list) {?>
						<div class="filter_block_body_item">
							<label><input
									class="formstyle"
									type="<?=strtolower($arItem["TYPE"])?>"
									name="<?=$arItem["INPUT_NAME"]?>[]"
									name="<?=$arItem["INPUT_NAME"]?>"
									value="<?=$id?>"/><?=$list?></label>
						</div>
							<?}?>

				</div>
					<?endif;?>
				<?}?>
			</div>
			<!--filter_block-->
			<br class="hide_min">
				<input type="hidden" name="year" value="all">
				<input type="hidden" name="set_filter" value="Фильтр">
			<input type="hidden" name="set_filter" value="Y" />
		</form>
			<?$APPLICATION->IncludeComponent(
				"asd:subscribe.quick.form",
				"news-subscribe",
				array(
					"FORMAT" => "text",
					"INC_JQUERY" => "N",
					"NOT_CONFIRM" => "N",
					"RUBRICS" => array(
						0 => "1",
					),
					"SHOW_RUBRICS" => "N",
					"COMPONENT_TEMPLATE" => "news-subscribe"
				),
				false
			);?>
		</div>
	</div>
</div>
<div class="block_type_center" style="min-height: 1000px;">



	<div class="filtr_news_top hide_min">
		Показать за
		<select class="formstyle min" name="year">
			<option value="all">Весь период</option>
			<?foreach ($arResult["YEARS"] as $year):?>
				<option value="<?=$year?>"><?=$year?></option>
			<?endforeach;?>
		</select>
	</div>
<?/*<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
	<?foreach($arResult["ITEMS"] as $arItem):
		if(array_key_exists("HIDDEN", $arItem)):
			echo $arItem["INPUT"];
		endif;
	endforeach;?>
	<table class="data-table" cellspacing="0" cellpadding="2">
	<thead>
		<tr>
			<td colspan="2" align="center"><?=GetMessage("IBLOCK_FILTER_TITLE")?></td>
		</tr>
	</thead>
	<tbody>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?if(!array_key_exists("HIDDEN", $arItem)):?>
				<tr>
					<td valign="top"><?=$arItem["NAME"]?>:</td>
					<td valign="top"><?=$arItem["INPUT"]?></td>
				</tr>
			<?endif?>
		<?endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">
				<input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" /></td>
		</tr>
	</tfoot>
	</table>
</form>*/?>
