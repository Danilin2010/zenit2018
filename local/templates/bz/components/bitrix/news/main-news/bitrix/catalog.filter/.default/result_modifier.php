<?php
$arResult["YEARS"] = array();
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arResult["YEARS"][] = date("Y", strtotime($arFields["DATE_ACTIVE_FROM"]));
}

$arResult["YEARS"] = array_unique($arResult["YEARS"]);