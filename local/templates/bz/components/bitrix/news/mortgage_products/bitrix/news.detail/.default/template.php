<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!--content_tab-->
<div class="content_rates_tabs">
    <ul class="content_tab c-container">
        <?/*<li><a href="#content-tabs-1">Расчет параметров</a></li>*/?>
        <li><a href="#content-tabs-2">Условия и требования</a></li>
        <li><a href="#content-tabs-4">Погашение</a></li>
        <li><a href="#content-tabs-5">Документы</a></li>
        <li><a href="#content-tabs-3">Страхование</a></li>
        <li><a href="#content-tabs-6">FAQ</a></li>
    </ul>
    <?/*<div class="content_body" id="content-tabs-1">
            <!--<div class="wr_block_type">
                <div class="block_type">
                    <div class="block_type_center">
                        <h1>
                            Рассчитайте ваше ипотечное предложение
                            <div class="note_text">
                                Срок рассмотрения онлайн-заявки составляет до 10 дней
                            </div>
                        </h1>
                    </div>
                </div>
            </div>-->
            <div class="wr_block_type white">
                <div class="block_type to_column">
                    <!--promo_benefits-->
                    <div class="promo_benefits">
                        <?
                        if($arResult["DISPLAY_PROPERTIES"]["ADDITIONALLY"]["ELEMENT_VALUE"]){
                            foreach ($arResult["DISPLAY_PROPERTIES"]["ADDITIONALLY"]["ELEMENT_VALUE"] as $val)
                            {
                                ?><div class="promo_benefits_item">
                                    <div class="pict" style="background-image: url('<?=$val["PREVIEW_PICTURE"]["SRC"]?>');"></div>
                                    <div class="body">
                                        <div class="title">
                                            <?=$val["NAME"]?>
                                        </div>
                                        <div class="text">
                                            <?=$val["PREVIEW_TEXT"]?>
                                        </div>
                                    </div>
                                </div><?
                            }
                        }
                        ?>
                    </div>
                    <!--promo_benefits-->
                </div>
            </div>
            <div class="wr_block_type gray">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/application.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
        </div>*/?>
    <div class="content_body" id="content-tabs-2">
        <?if($arResult["DISPLAY_PROPERTIES"]["MILITARY"]["VALUE"]=="Y"){?>
            <?if($arResult["DISPLAY_PROPERTIES"]["MILITARY_PROGRAM"]){?>
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/calculator/military.php",
                    Array(
                        "programm"=>$arResult["DISPLAY_PROPERTIES"]["MILITARY_PROGRAM"]["VALUE_XML_ID"],
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            <?}?>
        <?}elseif($arResult["DISPLAY_PROPERTIES"]["CALCULATOR_PROGRAM"]){?>
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/calculator/mortgage.php",
                Array(
                    "programm"=>$arResult["DISPLAY_PROPERTIES"]["CALCULATOR_PROGRAM"]["VALUE_XML_ID"],
                ),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        <?}?>
        <div class="wr_block_type">
            <div class="block_type to_column c-container">
                <div class="block_type_right">
                    <div class="form_application_line">
                        <div class="right_top_line">
                            <div class="block_top_line"></div>
                            <?
                            if($arResult["DISPLAY_PROPERTIES"]["CONDITIONS"]["ELEMENT_VALUE"]){
                                foreach ($arResult["DISPLAY_PROPERTIES"]["CONDITIONS"]["ELEMENT_VALUE"] as $val)
                                {
                                    ?><div class="right_bank_block">
                                    <div class="right_bank_title">
                                        <?=$val["NAME"]?>
                                    </div>
                                    <div class="right_bank_text note_text">
                                        <?=$val["PREVIEW_TEXT"]?>
                                    </div>
                                    </div><?
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form_application_line note_text">
                        <?if($arResult["DETAIL_TEXT"]){
                            echo $arResult["DETAIL_TEXT"];
                        }?><br>
                        <a href="#"
                           class="button mt-3"
                           data-yourapplication
                           data-classapplication=".wr_form_application"
                           data-formrapplication=".content_rates_tabs"
                           data-formrapplicationindex="0"
                            >Оставить заявку</a>
                    </div>
                </div>
                <div class="block_type_center">
					<?if($arResult["DISPLAY_PROPERTIES"]["MAX_PRICE"]["SUMM"] || $arResult["DISPLAY_PROPERTIES"]["MIN_INITIAL_DEPOSIT"]["VALUE"]||$arResult["DISPLAY_PROPERTIES"]["RATE"]["VALUE"]||$arResult["PROPERTIES"]["TIME_CREDIT"]["VALUE"]["TEXT"]){?>
                    <!--conditions-->
                    <div class="conditions">
                        <div class="conditions_item">
                            <div class="conditions_item_title">
								<?if($arResult["PROPERTIES"]["SIZE_CREDIT"]["VALUE"]["TEXT"]){?>
								<span><?=htmlspecialcharsBack($arResult["PROPERTIES"]["SIZE_CREDIT"]["VALUE"]["TEXT"])?></span>
								<?} else {?>
                                <span>до</span> <?=$arResult["MAX_PRICE"]["SUMM"]?> <span><?=$arResult["MAX_PRICE"]["SUFF"]?></span>
								<?}?>
                            </div>
                            <div class="conditions_item_text">
                                Размер кредита
                            </div>
                        </div><div class="conditions_item">
                            <div class="conditions_item_title">
								<?if($arResult["PROPERTIES"]["TIME_CREDIT"]["VALUE"]["TEXT"]){?>
								<span><?=htmlspecialcharsBack($arResult["PROPERTIES"]["TIME_CREDIT"]["VALUE"]["TEXT"])?></span>
                            </div>
                            <div class="conditions_item_text">
                                Срок кредита
                            </div>
								<?} else {?>
                                <span>от</span> <?=$arResult["DISPLAY_PROPERTIES"]["MIN_INITIAL_DEPOSIT"]["VALUE"]?>%
                            </div>
                            <div class="conditions_item_text">
                                первый взнос
                            </div>
								<?}?>

                        </div><div class="conditions_item">
                            <div class="conditions_item_title">
							<?if($arResult["DISPLAY_PROPERTIES"]["MILITARY"]["VALUE"]=="Y"){?>
                                <span></span> <?=$arResult["DISPLAY_PROPERTIES"]["RATE"]["VALUE"]?>%
								<?} else {?>
								<span>от</span> <?=$arResult["DISPLAY_PROPERTIES"]["RATE"]["VALUE"]?>%
								<?}?>
                            </div>
                            <div class="conditions_item_text">
                                ставка
                            </div>
                        </div>
                    </div>
                    <!--conditions-->
					<?}?>
					<div class="text_block">
                        <?if($arResult["DISPLAY_PROPERTIES"]["PURPOSE_LOAN"]["VALUE"]){?>
                            <h2>Цель кредита</h2>
                            <p>
                                <?=$arResult["DISPLAY_PROPERTIES"]["PURPOSE_LOAN"]["~VALUE"]["TEXT"]?>
                            </p>
                        <?}?>
                        <?if($arResult["DISPLAY_PROPERTIES"]["LENDING_TERMS"]["ELEMENT_VALUE"] || $arResult["DISPLAY_PROPERTIES"]["LENDING_TERMS_EXT"]["VALUE"]){?>
                            <h2>Условия кредитования</h2>
                            <ul>
                                <?if($arResult["DISPLAY_PROPERTIES"]["LENDING_TERMS"]["ELEMENT_VALUE"]){?>
                                    <?foreach ($arResult["DISPLAY_PROPERTIES"]["LENDING_TERMS"]["ELEMENT_VALUE"] as $key=>$val){?>
                                        <li><b><?=$val["NAME"]?></b> <?= $val["PREVIEW_TEXT"]?></li>
                                    <?}?>
                                <?}?>
                            </ul>
                            <?if($arResult["DISPLAY_PROPERTIES"]["LENDING_TERMS_EXT"]["VALUE"]){?>
                                <?=$arResult["DISPLAY_PROPERTIES"]["LENDING_TERMS_EXT"]["~VALUE"]["TEXT"]?>
                            <?}?>
                        <?}?>
                        <?if($arResult["DISPLAY_PROPERTIES"]["REQUIREMENTS_BORROWER"]["ELEMENT_VALUE"]){?>
                            <h2>Вы можете получить кредит, если:</h2>
                            <ul>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["REQUIREMENTS_BORROWER"]["ELEMENT_VALUE"] as $key=>$val){?>
                                    <li><?=$val["NAME"]?></li>
                                <?}?>
                            </ul>
                        <?}?>
                        <?
                        if($arResult["DISPLAY_PROPERTIES"]["SECURING_LOAN"]["ELEMENT_VALUE"]){?>
                            <?
                            $arrNote=array();
                            $arrNoteID=array();
                            ?>
                            <h2>Обеспечение кредита</h2>
                            <ul>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["SECURING_LOAN"]["ELEMENT_VALUE"] as $key=>$val){?>
                                    <?
                                    $useNote=false;
                                    if(count($val["NOTE"])>0)
                                    {
                                        foreach ($val["NOTE"] as $note)
                                        {
                                            if(!$arrNoteID[$note["ID"]])
                                            {
                                                $arrNoteID[$note["ID"]]=count($arrNoteID)+1;
                                                $arrNote[$note["ID"]]=$note;
                                            }
                                        }
                                    }
                                    ?>
                                    <?if($key>0){/*?>
                                        <li class="note_text">и/или</li>
                                    <?*/}?>
                                    <li>
                                        <?=$val["NAME"]?>
                                        <?
                                        if(count($val["NOTE"])>0)
                                            foreach ($val["NOTE"] as $note)
                                                echo str_repeat("* ", $arrNoteID[$note["ID"]]);
                                        ?>
                                    </li>
                                <?}?>
                            </ul>
                            <?if(count($arrNote)>0){?>
                                <?foreach($arrNote as $n){?>
                                    <div class="note_text">
                                        <?=str_repeat("* ", $arrNoteID[$note["ID"]])?> <?=$n["PREVIEW_TEXT"]?>
                                    </div>
                                <?}?>
                            <?}?>
                        <?}?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_body" id="content-tabs-3">
        <div class="wr_block_type">
            <div class="block_type to_column c-container">
                <div class="block_type_right">
                    <?/*<div class="right_top_line">
                            <div class="block_top_line"></div>
                            <div class="right_bank_block">
                                <?$APPLICATION->IncludeFile(
                                    SITE_TEMPLATE_PATH."/inc/block/insurance.php",
                                    Array(),
                                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                                );?>
                            </div>
                        </div>*/?>
                </div>
                <div class="block_type_center">
                    <div class="text_block">
                        <?=$arResult["DISPLAY_PROPERTIES"]["INSURANCE"]["~VALUE"]["TEXT"]?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_body" id="content-tabs-4">
        <div class="wr_block_type">
            <div class="block_type to_column c-container">
                <div class="block_type_right">
                    <?/*<div class="right_top_line">
                            <div class="block_top_line"></div>
                            <?$APPLICATION->IncludeFile(
                                SITE_TEMPLATE_PATH."/inc/block/contact.php",
                                Array(),
                                Array("MODE"=>"txt","SHOW_BORDER"=>false)
                            );?>
                        </div>*/?>
                </div>
                <div class="block_type_center">
                    <div class="text_block">

                        <?
                        $arrNote=array();
                        $arrNoteID=array();
                        ?>

                        <h2>Погашение кредита осуществляется</h2>
                        <ul>
                            <?if($arResult["DISPLAY_PROPERTIES"]["REPAYMENT"]["ELEMENT_VALUE"]){?>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["REPAYMENT"]["ELEMENT_VALUE"] as $key=>$val){?>
                                    <li>
                                        <?=$val["NAME"]?>
                                    </li>
                                <?}?>
                            <?}?>
                            <?if($arResult["DISPLAY_PROPERTIES"]["EARLY_REPAYMENT"]["ELEMENT_VALUE"]){?>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["EARLY_REPAYMENT"]["ELEMENT_VALUE"] as $key=>$val){?>
                                    <?if($key>0){/*?>
                                        <li class="note_text">и/или</li>
                                    <?*/}?>
                                    <li>
                                        <?=$val["NAME"]?>
                                    </li>
                                <?}?>
                            <?}?>
                        </ul>

                        <?if($arResult["DISPLAY_PROPERTIES"]["LOAN_REPAYMENT"]["ELEMENT_VALUE"]){?>
                            <h2>Досрочное погашение</h2>
                            <ul>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["LOAN_REPAYMENT"]["ELEMENT_VALUE"] as $key=>$val){?>
                                    <?if($key>0){/*?>
                                        <li class="note_text">и/или</li>
                                    <?*/}?>
                                    <li>
                                        <?=$val["NAME"]?>
                                    </li>
                                <?}?>
                            </ul>
                        <?}?>

                        <?if($arResult["DISPLAY_PROPERTIES"]["REPLENISHMENT_ACCOUNTS"]["ELEMENT_VALUE"]){?>
                            <h2>Пополнение открытого в Банке счета</h2>
                            <ul>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["REPLENISHMENT_ACCOUNTS"]["ELEMENT_VALUE"] as $key=>$val){?>
                                    <?if($key>0){/*?>
                                        <li class="note_text">и/или</li>
                                    <?*/}?>
                                    <li>
                                        <?=$val["NAME"]?>
                                    </li>
                                <?}?>
                            </ul>
                        <?}?>

                        <?if($arResult["DISPLAY_PROPERTIES"]["RESPONSIBILITY_BORROWER"]["VALUE"]){?>
                            <h2>Ответственность заемщика за ненадлежащее исполнение условий договора, размер неустойки (штрафа, пени)</h2>
                            <p>
                                <?=$arResult["DISPLAY_PROPERTIES"]["RESPONSIBILITY_BORROWER"]["~VALUE"]["TEXT"]?>
                            </p>
                        <?}?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_body" id="content-tabs-5">
        <?/*<div class="wr_block_type">
                <div class="block_type to_column">
                    <div class="block_type_right">

                    </div>
                    <div class="block_type_center">
                        <h1>Документы для скачивания</h1>
                        <div class="text_block">
                            <!--doc-->
                            <div class="doc_list">
                                <?foreach ($arResult["FILE"] as $file){?><a href="<?=$file["SRC"]?>" class="doc_item">
                                    <div class="doc_pict <?=$file["TYPE"]?>"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            <?=$file["NAME"]?>
                                        </div>
                                        <div class="doc_note">
                                            <?=$file["FILE_SIZE"]?>
                                        </div>
                                    </div>
                                </a><?}?>
                            </div>
                            <!--doc-->
                        </div>
                    </div>
                </div>
            </div>*/?>
        <div class="wr_block_type white">
            <div class="block_type to_column c-container">
                <div class="block_type_right">
                    <?if(count($arResult["FILE"])>0){?>
                        <h2>Документы для скачивания</h2>
                        <!--doc-->
                        <div class="doc_list">
                            <?foreach ($arResult["FILE"] as $file){?>
                                <a href="<?=$file["SRC"]?>" target="_blank" class="doc_item">
                                    <div class="doc_pict <?=$file["TYPE"]?>"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            <?=$file["NAME"]?>
                                        </div>
                                        <div class="doc_note">
                                            <?=$file["FILE_SIZE"]?>
                                        </div>
                                    </div>
                                </a>
                            <?}?>
                        </div>
                        <!--doc-->
                    <?}?>
                </div>
                <div class="block_type_center">
                    <?/*if($arResult["DISPLAY_PROPERTIES"]["ACCREDITED_OBJECTS"]["VALUE"]){?>
                        <a href="<?=$arResult["DISPLAY_PROPERTIES"]["ACCREDITED_OBJECTS"]["~VALUE"]?>">Аккредитованные Банком объекты недвижимости</a>
                    <?}?>
                    <?/*if($arResult["DISPLAY_PROPERTIES"]["INSURANCE_COMPANIES"]["VALUE"]["TEXT"]){?>
                            <h2>Перечень страховых компаний</h2>
                            <p>
                                <?=$arResult["DISPLAY_PROPERTIES"]["INSURANCE_COMPANIES"]["~VALUE"]["TEXT"]?>
                            </p>
                        <?}*/?>
                    <?
                    PrintTextElement($arResult["DISPLAY_PROPERTIES"],array(
                        "ACCREDITED_OBJECTS",
                        // "INSURANCE_COMPANIES",
                    ));
                    ?>
                    <?
                    PrintList($arResult["DISPLAY_PROPERTIES"],array(
                        //"ACCREDITED_OBJECTS",
                        "APPLICATION_DOCUMENTS",
                        //"INFORMATION_COMPANY",
                        //"INFORMATION_COMPANY_BUSINESS",
                        "INFORMATION_WAGES",
                        "ADDITIONAL_BORROWER",
                        "ADDITIONAL_INFORMATION_BORROWER",
                        //"INSURANCE_COMPANIES",
                        //"FAQ",
                    ));
                    ?>
					<?if($arResult["DISPLAY_PROPERTIES"]["LIST_DOC"]["VALUE"]["TEXT"]){?>
					<p><?=$arResult["DISPLAY_PROPERTIES"]["LIST_DOC"]["VALUE"]["TEXT"]?></p>
                    <p>
						<?}?>
<?=htmlspecialcharsBack($arResult["PROPERTIES"]["LIST_DOC"]["VALUE"]["TEXT"])?>
                        Для рассмотрения заявки необходимо заполнить
                        <a href="<?=$arResult["FILE"][0]["SRC"]?>" target="_blank">заявление-анкету </a> на получение кредита.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="content_body" id="content-tabs-6">
        <div class="wr_block_type">
            <div class="block_type to_column c-container">
                <div class="block_type_right">

                </div>
                <div class="block_type_center">
                    <?
                    PrintFaq($arResult["DISPLAY_PROPERTIES"],array(
                        "FAQ",
                    ));
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>


