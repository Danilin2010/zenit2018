<?
foreach ($arResult["DISPLAY_PROPERTIES"] as &$prop)
{
    if($prop["USER_TYPE"]=="ChekboxElementList")
        $prop["ELEMENT_VALUE"]=GetElementValue($prop["VALUE"],$Nation="NATION");
}unset($prop);


if($arResult["DISPLAY_PROPERTIES"]["MIN_PRICE"]["VALUE"]){
    $arResult["MIN_PRICE"]=GetPrice($arResult["DISPLAY_PROPERTIES"]["MIN_PRICE"]["VALUE"]);
}

if($arResult["DISPLAY_PROPERTIES"]["MAX_PRICE"]["VALUE"]){
    $arResult["MAX_PRICE"]=GetPrice($arResult["DISPLAY_PROPERTIES"]["MAX_PRICE"]["VALUE"]);
}
$arResult["FILE"]=array();

$arrNewFile=array(
    "STATEMENT",
    "STATEMENT_SURETY",
    "INSURANCE_COMPANIES",
    "INFORMATION_COMPANY",
    "INFORMATION_COMPANY_BUSINESS",
    "APPLICATION_SELLER",
    "APPLICATION_PLEDGOR",
);

foreach($arrNewFile as $name)
    if($arResult["DISPLAY_PROPERTIES"][$name])
        $arResult["FILE"][]=GetFile($arResult["DISPLAY_PROPERTIES"][$name]["NAME"],$arResult["DISPLAY_PROPERTIES"][$name]["FILE_VALUE"]);


$cp = $this->__component;
$arrCash=array(
    "NAME",
    "~PREVIEW_TEXT",
    "DISPLAY_PROPERTIES",
    "MAX_PRICE",
);

if (is_object($cp))
{
    foreach($arrCash as $value)
    {
        $cp->arResult[$value] = $arResult[$value];
        $cp->SetResultCacheKeys(array($value));
        if (!isset($arResult[$value]))
            $arResult[$value] = $cp->arResult[$value];
    }
}