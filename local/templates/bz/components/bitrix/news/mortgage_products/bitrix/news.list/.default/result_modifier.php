<?
$arResult["SECTION_EXT"] = array();
if (is_array($arResult["SECTION"]["PATH"]) && (count($arResult["SECTION"]["PATH"]) > 0)) {
    $arResult["SECTION_EXT"] = end($arResult["SECTION"]["PATH"]);
}
$Len=90;
foreach($arResult["ITEMS"] as &$arItem)
{
    $obParser = new CTextParser;
    $arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["PREVIEW_TEXT"],$Len);
}unset($arItem);

$cp = $this->__component;
$arrCash=array(
    "SECTION_EXT",
    "ITEMS",
);

if (is_object($cp))
{
    foreach($arrCash as $value)
    {
        $cp->arResult[$value] = $arResult[$value];
        $cp->SetResultCacheKeys(array($value));
        if (!isset($arResult[$value]))
            $arResult[$value] = $cp->arResult[$value];
    }
}