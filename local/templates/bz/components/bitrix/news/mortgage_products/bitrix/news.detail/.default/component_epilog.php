<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */
use \Bitrix\Main\Page\Asset;
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "Y",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "universal",
		"SOURCE_TREATMENT" => "Заявка на Ипотечный кредит: ".$arResult["NAME"],
		"RIGHT_TEXT" => "Заполните заявку. <br/>Это займет не более  10 минут.",
		"SEF_FOLDER" => "/local/templates/bz/components/bitrix/news/mortgage_products/bitrix/news.detail/.default/",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
    <!--content_tab-->
    <div class="block_type to_column">
        <!--banners-->
        <?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/inc/block/banners.php",
            Array(),
            Array("MODE"=>"txt","SHOW_BORDER"=>false)
        );?>
        <!--banners-->
    </div>


<?

ob_start();
?>
    <?=$arResult["~PREVIEW_TEXT"]?>
    <?/*<ul class="chek_list">
        <?if($arResult["MAX_PRICE"]){?>
            <li>до <?=$arResult["MAX_PRICE"]["SUMM"]?>&nbsp;<?=$arResult["MAX_PRICE"]["SUFF"]?> руб.</li>
        <?}?>
        <?if($arResult["DISPLAY_PROPERTIES"]["MIN_INITIAL_DEPOSIT"]){?>
            <li>от <?=$arResult["DISPLAY_PROPERTIES"]["MIN_INITIAL_DEPOSIT"]["VALUE"]?> % первый взнос</li>
        <?}?>
        <?if($arResult["DISPLAY_PROPERTIES"]["MAX_TERM_MORTGAGE"]){?>
            <li>до <?=$arResult["DISPLAY_PROPERTIES"]["MAX_TERM_MORTGAGE"]["VALUE"]?> лет</li>
        <?}?>
    </ul>*/?>
<?if($arResult["DISPLAY_PROPERTIES"]["DESCRIPTION_TITLE"]){?>
    <?=$arResult["DISPLAY_PROPERTIES"]["DESCRIPTION_TITLE"]["DISPLAY_VALUE"]?>
<?}?>
    <p><a href="#" class="button"
       data-yourapplication
       data-classapplication=".wr_form_application"
       data-formrapplication=".content_rates_tabs"
       data-formrapplicationindex="0"
    >Оставить заявку</a></p>
<?
$html = ob_get_contents();
ob_end_clean();
BufferContent::SetTitle('headertext',$html);
BufferContent::SetTitle('to_page_class','to_page_big_top');
if ($arResult['IBLOCK_SECTION_ID'] == "7") {
    BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/top_banner/build_mount.png');
} else {
    BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/top_banner/builders.png');
}

if($arResult["DISPLAY_PROPERTIES"]["MILITARY"]["VALUE"]=="Y") {
    if($arResult["DISPLAY_PROPERTIES"]["MILITARY_PROGRAM"]) {
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/function.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/varmilitary.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/military.js');
    }
}elseif($arResult["DISPLAY_PROPERTIES"]["CALCULATOR_PROGRAM"]){
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/function.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/varmortgage.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/mortgage.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/calculator/conversion.js');
}


//echo "<pre>";print_r($arResult["MAX_PRICE"]);echo "</pre>";