<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */


ob_start();
?>
<?if(isset($arResult["SECTION_EXT"]["~DESCRIPTION"]) && (strlen($arResult["SECTION_EXT"]["~DESCRIPTION"]) > 0)){?>
    <?=$arResult["SECTION_EXT"]["~DESCRIPTION"]?>
<?}?>
<?
$html = ob_get_contents();
ob_end_clean();
BufferContent::SetTitle('headertext',$html);
BufferContent::SetTitle('to_page_class','to_page_big_top');
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
    $ajax="Y";
if($ajax!="Y")
{
    if(count($arResult["ITEMS"])==1){
        LocalRedirect($arResult["ITEMS"][0]["DETAIL_PAGE_URL"]);
    }
}



