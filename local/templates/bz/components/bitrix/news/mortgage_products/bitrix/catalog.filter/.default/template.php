<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block_type_right">
    <div class="wr_form_block">
        <form action="<? echo $arResult["FORM_ACTION"] ?>" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" id="mortgage_filter">
            <? foreach ($arResult["ITEMS"] as $arItem):
                if (array_key_exists("HIDDEN", $arItem)):
                    echo $arItem["INPUT"];
                endif;
            endforeach; ?>
            <!--filter_block-->
            <? foreach ($arResult["ITEMS"]['QUESTIONS'] as $arItem): ?>
                <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                    <!--filter_block-->
                    <div class="filter_block">
                        <div class="filter_block_title mini_show">
                            <?= $arItem["NAME"] ?>
                        </div>
                        <div class="filter_block_body">
                            <? foreach ($arItem["OPTIONS"] as $k => $option): ?>
                                <div class="filter_block_body_item">
                                    <label><input class="formstyle" type="checkbox" name="<?=$arItem['INPUT_NAME']?>"
                                                  value="<?= $k ?>"/><?= $option ?></label>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <!--filter_block-->
                <? endif ?>
            <? endforeach; ?>
            <input type="hidden" name="set_filter" value="Y">
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('#mortgage_filter').on('change', function(){
            var form = $(this).serialize();
            var action = $(this).attr('action');
            $.ajax({
                url: action,
                method: "post",
                data: form,
                success: function(data){
                   var list =  $(data).find('.ipoteka_list').html();
                   $('.ipoteka_list').html(list);
                   //console.log(list);
                }

            });
            return false;
        });

    });//ready
</script>

	<?/*<div class="filter_block">
		<div class="filter_block_title">
			Тип недвижимости
		</div>
		<div class="filter_block_body">
			<div class="filter_block_body_item">
				<label><input class="formstyle" type="radio" name="type" value="new"/>Новостройка</label>
			</div>
			<div class="filter_block_body_item">
				<label><input class="formstyle" type="radio" name="type" value="old"/>Готовое жилье</label>
			</div>
			<div class="filter_block_body_item">
				<label><input class="formstyle" type="radio" name="type" value="all"/>Любой</label>
			</div>
		</div>
	</div>
	<!--filter_block-->*/?>
