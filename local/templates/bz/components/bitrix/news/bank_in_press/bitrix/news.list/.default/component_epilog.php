<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */


ob_start();
?>

<?if(isset($arResult["SECTION_EXT"]["~DESCRIPTION"]) && (strlen($arResult["SECTION_EXT"]["~DESCRIPTION"]) > 0)){?>
	<?=$arResult["SECTION_EXT"]["~DESCRIPTION"]?>
<?}?>

<?
$html = ob_get_contents();
ob_end_clean();
BufferContent::SetTitle('headertext',$html);



/*
ob_start();
?>
<div class="wr-toggle-light-text"><div class="toggle-light-text off">Кредитные</div><div class="toggle-light-wr"><div class="toggle toggle-light" data-toggle data-checked="Y" data-name="type"></div></div><div class="toggle-light-text on">Дебетовые</div></div>
<?
$html = ob_get_contents();
ob_end_clean();

$APPLICATION->SetTitle($html);
$APPLICATION->SetPageProperty('title', 'Карты');
*/

BufferContent::SetTitle('to_page_class','to_page_big_top');
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/top_banner/common-image.jpg');

?>
