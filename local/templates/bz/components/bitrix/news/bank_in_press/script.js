$(document).ready(function () {
    var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

	$('#news-filter-mini').on(
		'change', function() {
			collectFilter();
		}
	);

	$('#news-filter').on(
		'change', function() {
			collectFilter();
		}
	);

	$('select[name="year"]').on(
		'change', function () {
			collectFilter();
		}
	);

	$('#show_more_news').on(
		'click', function() {
			collectFilter();
		}
	);

	$('#confirmed-styler').on(
		'change', function() {
			if($(this).hasClass("checked"))
			{
                if(pattern.test($('#email').val()))
					$('#asd_subscribe_submit').removeAttr("disabled");
			} else {
				$('#asd_subscribe_submit').attr("disabled", "true");
			}
		}
	);

	$('#email').on('blur', function() {
		if($(this).val() != '') {
			if(pattern.test($(this).val())){
				$('#asd_subscribe_res').text('');
				if($('#confirmed-styler').hasClass("checked"))
                	$('#asd_subscribe_submit').removeAttr("disabled");
			} else {
				$('#asd_subscribe_res').css('color', 'red');
				$('#asd_subscribe_res').text('email указан неверно');
                $('#asd_subscribe_submit').attr("disabled", "true");

            }
		} else {
			$('#asd_subscribe_res').css('color', 'red');
			$('#asd_subscribe_res').text('Поле email не должно быть пустым');
            $('#asd_subscribe_submit').attr("disabled", "true");

        }
	});

    $('#email').on('keyup', function () {
		if ($('#asd_subscribe_res').text() && pattern.test($(this).val()) && $('#confirmed-styler').hasClass("checked")) {
            $('#asd_subscribe_res').text('');
            $('#asd_subscribe_submit').removeAttr("disabled");
        }
    });

});


function collectFilter() {
	var year = $('select[name="year"]').val();
	var category = [];
	var count = $('#show_more_news').attr('data-count');
	$('#news-filter').find('input[type="checkbox"]:checked').each(function(){

		category[category.length]=$(this).val();
	});
	var form = {
		'year':year,
		'arrFilter_pf[category_news][]': category,
		'set_filter': 'Y',
		'count' : count
	};
	$.ajax(
		{
			url: "",
			method: "post",
			data: form,
			success: function(data) {
				var list = $(data).find('.news_list').html();
				$('.news_list').html(list);
				var button = $(data).find('#show_more_news').attr('data-count');
				console.log(button);
				$('#show_more_news').attr('data-count', button);
			}
		}
	);
	return false;

}

