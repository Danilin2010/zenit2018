<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
$html = $arResult["PREVIEW_TEXT"];
//BufferContent::SetTitle('headertext',$html);
//$APPLICATION->SetTitle($arResult['NAME'])
$cp = $this->__component;
$cp->SetResultCacheKeys(array('PREVIEW_TEXT'));
?>
<?// echo '<pre>' . print_r($arResult, 1) . '</pre>';?>
<div class="wr_block_type">
	<!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Описание</a></li>
			<li><a href="#content-tabs-2">Ставки</a></li>
			<?if(!empty($arResult["PROPERTIES"]["unusual_conditions"]["~VALUE"]["TEXT"])):?>
			<li><a href="#content-tabs-3">Условия досрочного расторжения</a></li>
			<?endif;?>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">

					</div>
					<div class="block_type_center">
						<h1>Особенности вклада</h1>
						<ul class="big_list">
							<li>Валюта: <?foreach ($arResult["PROPERTIES"]["currency"]["VALUE"] as $currency){ echo $currency.", "; }?></li>
							<li>Срок хранения: <?=$arResult["PROPERTIES"]["deposit_term"]["VALUE"]?></li>
							<li>Сумма вклада: <?=$arResult["PROPERTIES"]["deposit_sum"]["VALUE"]?></li>
							<?= ($arResult["PROPERTIES"]["cashin"]["VALUE"] == "Y") ? "<li>Возможность неоднократного пополнения вклада</li>" : "<li>Пополнение вклада не предусмотрено</li>" ?>
							<li><?= $arResult["PROPERTIES"]["percent_payment"]["VALUE"] ?></li>
							<? if ($arResult["PROPERTIES"]["breaking"]["ID"]): ?>
								<li><?= $arResult["PROPERTIES"]["breaking"]["VALUE"] ?></li>
							<? endif; ?>
							<?= ($arResult["PROPERTIES"]["premium_cards"]["VALUE"] == "Y") ? "<li>Премиальные банковские карты международных платежных систем с бесплатным обслуживанием</li>" : "" ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<!--table-->
					<?=$arResult["PROPERTIES"]["rates_table"]["~VALUE"]["TEXT"]?>
					<!--table-->
				</div>
			</div>
		</div>
		<?if(!empty($arResult["PROPERTIES"]["unusual_conditions"]["~VALUE"]["TEXT"])):?>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<!--table-->
					<?=$arResult["PROPERTIES"]["unusual_conditions"]["~VALUE"]["TEXT"]?>
					<!--table-->
				</div>
			</div>
		</div>
		<?endif;?>
	</div>
</div>
