<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}

if ($arResult['ACTION']['status']=='error') {
	ShowError($arResult['ACTION']['message']);
} elseif ($arResult['ACTION']['status']=='ok') {
	ShowNote($arResult['ACTION']['message']);
}
?>
<style>
	/*#asd_subscribe_res{
		color: #1a9599 !important;
	}*/
</style>
<div class="block_top_line hide_min"></div>
<?$aSubscr = checkSubscibe(1);
if($aSubscr):?>
<div id="asd_subscribe">
	<h2>Вы подписаны<br> на новости Банка ЗЕНИТ</h2>
	<p><b>Адрес подписки:</b> <br>
		<span><?=$aSubscr['EMAIL']?></span>
	</p>
	<a href="/about/subscr_edit.php?unsubscribe=Отписаться&action=unsubscribe&ID=<?=$aSubscr["ID"]?>" type="button" class="button bigmaxwidth">Отменить</a>
</div>
<?else:?>
<h2><?=GetMessage("ASD_SUBSCRIBEFORM_NAME")?></h2>
<div id="asd_subscribe_res"></div>
<form action="<?= POST_FORM_ACTION_URI?>" method="post" id="asd_subscribe_form">
	<?= bitrix_sessid_post()?>
	<input type="hidden" name="asd_subscribe" value="Y" />
	<input type="hidden" name="charset" value="<?= SITE_CHARSET?>" />
	<input type="hidden" name="site_id" value="<?= SITE_ID?>" />
	<input type="hidden" name="asd_rubrics" value="<?= $arParams['RUBRICS_STR']?>" />
	<input type="hidden" name="asd_format" value="<?= $arParams['FORMAT']?>" />
	<input type="hidden" name="asd_show_rubrics" value="<?= $arParams['SHOW_RUBRICS']?>" />
	<input type="hidden" name="asd_not_confirm" value="<?= $arParams['NOT_CONFIRM']?>" />
	<input type="hidden" name="asd_key" value="<?= md5($arParams['JS_KEY'].$arParams['RUBRICS_STR'].$arParams['SHOW_RUBRICS'].$arParams['NOT_CONFIRM'])?>" />
	<div class="form_application_line">
		<div class="wr_complex_input complex_input_noicon">
			<div class="complex_input_body">
				<div class="text">e-mail</div>
				<input type="text" class="simple_input" id="email" name="asd_email" value="" />
			</div>
		</div>
		<div id="valid" style="color: red"></div>
	</div>
	<div class="form_application_line form_application_line_button_line wr_div_agree" >
		<label>
			<input type="checkbox" class="formstyle"
				   name="form_checkbox_agreement[]"
				   id="confirmed"
				   value="7">
            <div class="div_agree">
                Я согласен с <a target="_blank" href="/">условиями</a> передачи и хранения информации
            </div>

		</label>
	</div>
	<div class="form_application_line">
		<input type="submit" name="asd_submit" id="asd_subscribe_submit" disabled class="button bigmaxwidth" value="<?=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")?>" />
	</div>
	<?if (isset($arResult['RUBRICS'])):?>
		<br/>
		<?foreach($arResult['RUBRICS'] as $RID => $title):?>
		<input type="checkbox" name="asd_rub[]" id="rub<?= $RID?>" value="<?= $RID?>" />
		<label for="rub<?= $RID?>"><?= $title?></label><br/>
		<?endforeach;?>
	<?endif;?>
</form>
<?endif;?>