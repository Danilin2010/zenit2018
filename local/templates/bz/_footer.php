<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>


<?if($APPLICATION->GetDirProperty("type_bank_page")=="Y"){?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>

<?if($APPLICATION->GetDirProperty("type_bank_page_block")=="Y"){?>
                    </div>
                </div>
            </div>
        </div>
            <!-- Правый блок для ПС -->
            <div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <?$APPLICATION->IncludeFile(
                            SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                            Array(),
                            Array("MODE"=>"txt","SHOW_BORDER"=>false)
                        );?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>

<?

$promo=trim($_REQUEST["promo"]);
if(strlen($promo)>0){
    BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/promo/'.$promo);
}
?>

<?if($APPLICATION->GetCurPage(true)==SITE_DIR.'index.php'){?>
    <!--main_tile-->
    <?$APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/inc/template/main_tile.php",
        Array(),
        Array("MODE"=>"txt","SHOW_BORDER"=>false)
    );?>
    <!--main_tile-->
    <!--footer_banner-->
    <?$APPLICATION->IncludeFile(
        SITE_TEMPLATE_PATH."/inc/template/footer_banner.php",
        Array(),
        Array("MODE"=>"txt","SHOW_BORDER"=>false)
    );?>
    <!--footer_banner-->
<?}else{?>
    <?if($APPLICATION->GetDirProperty("use_page_frame")=="Y"){?></div></div></div><?}?>
<?}?>
<!--footer-->
<div class="footer">
    <div class="first_line z-container">
        <div class="call">
            <div class="call_title">
                Бесплатный звонок
            </div>
            <div class="call_phone">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/template/phone1.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <div class="call_phone min">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/template/phone2.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <div class="call_button">
                <a class="button footer" href="mailto:info@zenit.ru">написать нам</a>
                <a class="skype" href="skype:zenitgroup.ru?call"></a>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_menu", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
);?>
        <div class="links">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "fuuter_net", Array(
	"COMPONENT_TEMPLATE" => "main_news",
		"IBLOCK_TYPE" => "icon",	// Тип информационного блока (используется только для проверки)
		"IBLOCK_ID" => "5",	// Код информационного блока
		"NEWS_COUNT" => "10",	// Количество новостей на странице
		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_BY2" => "NAME",	// Поле для второй сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"FILTER_NAME" => "",	// Фильтр
		"FIELD_CODE" => array(	// Поля
			0 => "DETAIL_TEXT",
			1 => "",
		),
		"PROPERTY_CODE" => array(	// Свойства
			0 => "CLASS",
			1 => "LINK",
			2 => "",
		),
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000",	// Время кеширования (сек.)
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SHOW_404" => "N",	// Показ специальной страницы
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
	),
	false
);?>
            <div class="call_title">
                Мобильный банк
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:news.list", "fuuter_app", Array(
	"COMPONENT_TEMPLATE" => "main_news",
		"IBLOCK_TYPE" => "icon",	// Тип информационного блока (используется только для проверки)
		"IBLOCK_ID" => "4",	// Код информационного блока
		"NEWS_COUNT" => "10",	// Количество новостей на странице
		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_BY2" => "NAME",	// Поле для второй сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"FILTER_NAME" => "",	// Фильтр
		"FIELD_CODE" => array(	// Поля
			0 => "DETAIL_TEXT",
			1 => "",
		),
		"PROPERTY_CODE" => array(	// Свойства
			0 => "CLASS",
			1 => "LINK",
			2 => "",
		),
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000",	// Время кеширования (сек.)
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SHOW_404" => "N",	// Показ специальной страницы
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
	),
	false
);?>
        </div>
    </div>
    <div class="to_line">
        <div class="z-container">
            <div class="license">
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/template/license.php",
                    Array(),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/text_block.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
            <div class="text_block min">
                <div class="text">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/info.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--footer-->
</div>

<!-- Модальное окно для отображения формы обратной связи -->
<? $APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "contact",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "Y",
        "WEB_FORM_ID" => "2",
        "COMPONENT_TEMPLATE" => "universal",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
); ?>
<!-- Модальное окно с условиями онлайн-резервирования счета -->
<?$APPLICATION->IncludeComponent("bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "reserve_agreement",
        "EDIT_TEMPLATE" => "",
    )
);?>

<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/counters/ga.php",
    Array(),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>
<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/counters/ya.php",
    Array(),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>

<div id="overlay"> <!-- пoдлoжкa, дoлжнa быть oднa нa стрaнице --> </div>
<div class="wr_mobail_city <?if(!ChekCook()){?>open<?}?>">
    <div class="wr_select_city_in mobail">
        <div class="select_city">
            <div class="select_city_text">
                Ваш город
            </div>
            <div class="select_city_name">
                <?=GetSity()?>
            </div>
            <table>
                <tr>
                    <td><a class="button maxwidth" data-close-city data-set-cook-city href="javascript:void(0)">Да</a></td>
                    <td><a href="#" title="Выбор города" data-select-city>выбрать другой</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>