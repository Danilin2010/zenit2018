//set slider animation parameters
var duration = 400,
    epsilon = (1000 / 60 / duration) / 4,
    customMinaAnimation = bezier(.42,.03,.77,.63, epsilon);

//define a radialSlider object
var radialSlider = function(element,exceptionsSlide,callbackSlide,callbackParent) {
    this.element = element;
    this.callbackSlide = callbackSlide;
    this.callbackParent = callbackParent;
    this.exceptionsSlide = exceptionsSlide;
    this.nextSize=45+19;
    this.nextDeltaRight=65;
    this.nextDeltaBottom=18;
    this.slider = this.element.find('.cd-radial-slider');
    this.slides = this.slider.children('li');
    this.slidesNumber = this.slides.length;
    this.visibleIndex = 0;
    this.nextVisible = 1;
    this.prevVisible = this.slidesNumber - 1;
    this.navigation = this.element.find('.cd-radial-slider-navigation');
    this.animating = false;
    this.mask = this.element.find('.cd-round-mask');
    this.rightMask = this.mask.find('mask').eq(1);
    this.AllInitNext();
    return this;
}

radialSlider.prototype.AllInitNext = function() {
    clipPathNext = Snap('#'+this.slides.eq(this.nextVisible).find('circle').attr('id'));
    var self = this;
    if ($(window).width() <= 992) {
        self.nextSize=15+10;
        self.nextDeltaRight=23;
        self.nextDeltaBottom=33;
        clipPathNext.attr({
            'r': 35
        });
    }else if ($(window).width() <= 1100){
        self.nextSize=35+10;
        self.nextDeltaRight=23;
        self.nextDeltaBottom=23;
    }
    this.slides.find('circle').attr({
        'cx': $(document).width()-self.nextSize-self.nextDeltaRight,
        'cy': $(document).height()-self.nextSize-self.nextDeltaBottom
    });

}

radialSlider.prototype.InitNext = function() {
    var self = this;
    var clipPathVisible = Snap('#'+this.slides.eq(this.visibleIndex).find('circle').attr('id')),
        clipPathPrev = Snap('#'+this.slides.eq(this.prevVisible).find('circle').attr('id')),
        clipPathNext = Snap('#'+this.slides.eq(this.nextVisible).find('circle').attr('id'));
    clipPathNext.attr({
        'cx': $(document).width()-self.nextSize-self.nextDeltaRight,
        'cy': $(document).height()-self.nextSize-self.nextDeltaBottom
    });
    clipPathPrev.attr({
        'cx': self.nextDeltaRight,
        'cy': $(document).height()-self.nextSize-self.nextDeltaBottom
    });
}


radialSlider.prototype.update = function(direction) {
    this.updateIndexes(direction);
    this.updateSlides(direction);
}

radialSlider.prototype.updateExt = function(index) {
    if(this.visibleIndex != index && index>=0 && index<this.slidesNumber)
        this.updateIndexesExt(index);
}

radialSlider.prototype.updateIndexes = function(direction) {
    if(direction == 'next')
        this.visibleIndex = ( this.visibleIndex + 1 < this.slidesNumber) ? this.visibleIndex + 1 : 0;
    else
        this.visibleIndex = ( this.visibleIndex > 0 ) ? this.visibleIndex - 1 : this.slidesNumber - 1;
    this.nextVisible = ( this.visibleIndex + 1 < this.slidesNumber) ? this.visibleIndex + 1 : 0;
    this.prevVisible = ( this.visibleIndex > 0 ) ? this.visibleIndex - 1 : this.slidesNumber - 1;
}

radialSlider.prototype.updateIndexesExt = function(index) {
    var direction = 'prev';
    if(index>this.visibleIndex)
        direction = 'next';
    if(direction == 'next')
    {
        this.prevVisible=this.visibleIndex;
        this.visibleIndex = index;
        this.nextVisible = ( this.visibleIndex + 1 < this.slidesNumber) ? this.visibleIndex + 1 : 0;
    }else{
        this.nextVisible=this.visibleIndex;
        this.visibleIndex = index;
        this.prevVisible = ( this.visibleIndex > 0 ) ? this.visibleIndex - 1 : this.slidesNumber - 1;
    }
    this.updateSlides(direction);
}

radialSlider.prototype.updateSlides = function(direction) {
    var self = this;
    var clipPathVisible = Snap('#'+this.slides.eq(this.visibleIndex).find('circle').attr('id')),
        clipPathPrev = Snap('#'+this.slides.eq(this.prevVisible).find('circle').attr('id')),
        clipPathNext = Snap('#'+this.slides.eq(this.nextVisible).find('circle').attr('id'));
    var radius1 = this.slider.data('radius1'),
        radius2 = this.slider.data('radius2'),
        centerx = ( direction == 'next' ) ? this.slider.data('centerx2') : this.slider.data('centerx1');
    this.slides.removeClass('next-slide prev-slide');
    this.slides.eq(this.visibleIndex).addClass('is-animating').removeClass('next-slide');
    if($.inArray(parseInt(this.nextVisible),this.exceptionsSlide)>=0){
        radius1=0;
    }else if ($(window).width() <= 992) {
        radius1=35;
    }
    if( direction == 'next' ) {
        this.slides.eq(this.visibleIndex).addClass('content-reveal-left');
        this.slides.eq(this.prevVisible).addClass('content-hide-left');
        clipPathNext.attr({
            'r': radius1
        });
        this.slides.eq(this.nextVisible).addClass('next-slide');
        this.slides.filter('.prev-slide').addClass('scale-down');
    }else{
        this.slides.eq(this.visibleIndex).addClass('not-animating');
        this.slides.eq(this.visibleIndex).addClass('content-reveal-right');
        this.slides.eq(this.nextVisible).addClass('content-hide-right');
        this.slides.eq(this.visibleIndex).find('image').attr('style', 'mask: url(#'+this.rightMask.attr('id')+')');
        clipPathPrev.attr({
            'r': radius1
        });
        this.slides.eq(this.prevVisible).addClass('prev-slide');
    }
    clipPathVisible.attr({
        'r': radius1
    }).animate({'r': radius2}, duration, customMinaAnimation, function(){
        if( direction == 'next' ) {
            self.slides.filter('.prev-slide').removeClass('prev-slide scale-down');
            clipPathPrev.attr({
                'r': radius1
            });
            self.slides.eq(self.prevVisible).removeClass('visible').addClass('prev-slide');
        } else {
            self.slides.filter('.next-slide').removeClass('next-slide scale-down');
            clipPathNext.attr({
                'r': radius1
            });
            self.slides.eq(self.nextVisible).removeClass('visible').addClass('next-slide');
        }
        self.slides.eq(self.visibleIndex).removeClass('not-animating').removeClass('is-animating').addClass('visible').find('image').removeAttr('style');
        self.slides.filter('.move-up').removeClass('move-up');
        setTimeout(function(){
            self.slides.eq(self.visibleIndex).removeClass('content-reveal-left content-reveal-right');
            self.slides.eq(self.prevVisible).removeClass('content-hide-left content-hide-right');
            self.slides.eq(self.nextVisible).removeClass('content-hide-left content-hide-right');
            self.animating =  false;
        }, 100);
        self.InitNext();
        self.callbackSlide(self);
    });
}


function bezier(x1, y1, x2, y2, epsilon){
    //https://github.com/arian/cubic-bezier
    var curveX = function(t){
        var v = 1 - t;
        return 3 * v * v * t * x1 + 3 * v * t * t * x2 + t * t * t;
    };

    var curveY = function(t){
        var v = 1 - t;
        return 3 * v * v * t * y1 + 3 * v * t * t * y2 + t * t * t;
    };

    var derivativeCurveX = function(t){
        var v = 1 - t;
        return 3 * (2 * (t - 1) * t + v * v) * x1 + 3 * (- t * t * t + 2 * v * t) * x2;
    };

    return function(t){

        var x = t, t0, t1, t2, x2, d2, i;

        // First try a few iterations of Newton's method -- normally very fast.
        for (t2 = x, i = 0; i < 8; i++){
            x2 = curveX(t2) - x;
            if (Math.abs(x2) < epsilon) return curveY(t2);
            d2 = derivativeCurveX(t2);
            if (Math.abs(d2) < 1e-6) break;
            t2 = t2 - x2 / d2;
        }

        t0 = 0, t1 = 1, t2 = x;

        if (t2 < t0) return curveY(t0);
        if (t2 > t1) return curveY(t1);

        // Fallback to the bisection method for reliability.
        while (t0 < t1){
            x2 = curveX(t2);
            if (Math.abs(x2 - x) < epsilon) return curveY(t2);
            if (x > x2) t0 = t2;
            else t1 = t2;
            t2 = (t1 - t0) * .5 + t0;
        }

        // Failure
        return curveY(t2);

    };
}

