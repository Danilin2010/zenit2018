<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeTemplateLangFile(__FILE__);

$arTemplate = Array(
    "NAME"=>GetMessage("BZ_TEMPLATE_DESCRIPTION_NAME"),
    "DESCRIPTION"=> GetMessage("BZ_TEMPLATE_DESCRIPTION_DESC")
);

?>