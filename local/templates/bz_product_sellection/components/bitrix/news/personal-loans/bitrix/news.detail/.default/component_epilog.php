<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */
use \Bitrix\Main\Page\Asset;
?>

<?if ($_GET['get_header'] == 'Y') {
    $APPLICATION->RestartBuffer();
    $data = array(
        'TITLE' => $arResult['NAME'],
        'TEXT' => $arResult['~PREVIEW_TEXT'],
        'LIST' => $arResult['DISPLAY_PROPERTIES']['DESCRIPTION_TITLE']['DISPLAY_VALUE'],
        'IMAGE'=> 'local/templates/bz/img/top_banner/inner-personal.png'
    );
    echo trim(json_encode($data, true));
    die();
}
else { ?>
    <?
    $APPLICATION->IncludeComponent("bitrix:form.result.new", "universal", array("CACHE_TIME" => "3600", "CACHE_TYPE" => "N", "CHAIN_ITEM_LINK" => "", "CHAIN_ITEM_TEXT" => "", "EDIT_URL" => "", "IGNORE_CUSTOM_TEMPLATE" => "N", "LIST_URL" => "", "SEF_MODE" => "N", "SUCCESS_URL" => "", "USE_EXTENDED_ERRORS" => "Y", "WEB_FORM_ID" => "1", "COMPONENT_TEMPLATE" => "universal", "SOURCE_TREATMENT" => "Заявка на Потребительское кредитование: " . $arResult["NAME"], "RIGHT_TEXT" => "Заполните заявку и получите кредит<br/>в течение 2 дней. Это займет<br/>не более  10 минут.", "VARIABLE_ALIASES" => array("WEB_FORM_ID" => "WEB_FORM_ID", "RESULT_ID" => "RESULT_ID",)), false); ?>
    <?
    ob_start();
    ?>

    <?= $arResult["PREVIEW_TEXT"] ?>
    <?/*<ul class="chek_list">
                <?if(!empty($arResult["PROPERTIES"]["SUM_AMOUNT"]["VALUE"])):?>
                    <li>до <?=GetPrettySum($arResult["PROPERTIES"]["SUM_AMOUNT"]["VALUE"])?> млн. руб.</li>
                <?endif;?>
                    <li>от <?=$arResult["PROPERTIES"]["STAKE_MIN"]["VALUE"]?> % первый взнос</li>
                    <li>до <?=$arResult["PROPERTIES"]["PERIOD_MAX"]["VALUE"]?> лет</li>
                </ul>*/ ?>
    <?
    if ($arResult["DISPLAY_PROPERTIES"]["DESCRIPTION_TITLE"]) { ?>
        <?= $arResult["DISPLAY_PROPERTIES"]["DESCRIPTION_TITLE"]["DISPLAY_VALUE"] ?><br/><br/>
    <?
    } ?>
    <p><a href="#"
          class="button"
          data-yourapplication
          data-classapplication=".wr_form_application"
          data-formrapplication=".content_rates_tabs"
          data-formrapplicationindex="0"
        >Оставить заявку</a></p>

    <?
    $html = ob_get_contents();
    ob_end_clean();
    BufferContent::SetTitle('headertext', $html);
    BufferContent::SetTitle('to_page_class', 'to_page_big_top');
//BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/inner/_inner-mortage.png');
    BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/inner/inner-personal.png');

    if ($arResult["DISPLAY_PROPERTIES"]["CALCULATOR_PROGRAM"]) {
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/calculator/function.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/calculator/varconsumer.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/calculator/mortgage.js');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/calculator/conversion.js');
    }
}?>
