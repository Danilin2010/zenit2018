<?php
foreach ($arResult["DISPLAY_PROPERTIES"] as &$prop)
{
    if($prop["USER_TYPE"]=="ChekboxElementList")
        $prop["ELEMENT_VALUE"]=GetElementValue($prop["VALUE"],$Nation="NATION");
}unset($prop);

/*$arResult["FILE"]=array();
if($arResult["DISPLAY_PROPERTIES"]["DOCS_TARIFFS"]){
    $arResult["FILE"][]=GetFile($arResult["DISPLAY_PROPERTIES"]["DOCS_TARIFFS"]["DESCRIPTION"][0],$arResult["DISPLAY_PROPERTIES"]["DOCS_TARIFFS"]["FILE_VALUE"][0]);
}*/

if($arResult["DISPLAY_PROPERTIES"]["SUM_AMOUNT"]["VALUE"])
    $arResult["DISPLAY_PROPERTIES"]["SUM_AMOUNT"]["VALUE"]=GetPrice($arResult["DISPLAY_PROPERTIES"]["SUM_AMOUNT"]["VALUE"]);

$arResult["FILE"]=array();
if($arResult["DISPLAY_PROPERTIES"]["DOCS_TARIFFS"]){
    foreach ($arResult["DISPLAY_PROPERTIES"]["DOCS_TARIFFS"]["FILE_VALUE"] as $k => $arDoc)
    {
        $arResult["FILE"][$k] = GetFile($arDoc["DESCRIPTION"], $arDoc);
    }

}

$cp = $this->__component;
$arrCash=array(
    "NAME",
    "PREVIEW_TEXT",
    "PROPERTIES",
    "SUM_AMOUNT",
    "STAKE_MIN",
    "PERIOD_MAX",
    "DISPLAY_PROPERTIES",
);

if (is_object($cp))
{
    foreach($arrCash as $value)
    {
        $cp->arResult[$value] = $arResult[$value];
        $cp->SetResultCacheKeys(array($value));
        if (!isset($arResult[$value]))
            $arResult[$value] = $cp->arResult[$value];
    }
}
