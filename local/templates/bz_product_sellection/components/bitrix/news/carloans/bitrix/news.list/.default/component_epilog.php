<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */


ob_start();
?>
<?if(isset($arResult["SECTION_EXT"]["~DESCRIPTION"]) && (strlen($arResult["SECTION_EXT"]["~DESCRIPTION"]) > 0)){?>
    <?=$arResult["SECTION_EXT"]["~DESCRIPTION"]?>
<?}?>
<?
$html = ob_get_contents();
ob_end_clean();
BufferContent::SetTitle('headertext',$html);



