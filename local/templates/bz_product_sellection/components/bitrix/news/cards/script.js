$(document).ready(function () {

    $('#cards_filter').on('change', function () {
        var form = $(this).serialize();
        var action = $(this).attr('action');
        $.ajax({
            url: action,
            method: "post",
            data: form,
            success: function (data) {
                var list = $(data).find('.wr_card_list').html();
                $('.wr_card_list').html(list);
                var form = $(data).find('.section-id').val();
                $('.section-id').val(form);
                console.log(form);
            }

        });
        return false;
    });

    $('.wr-toggle-light-text>div').on('click', function () {
        var on = $('div .toggle-on');
        var off = $('div .toggle-off');
        var section_id = 1;
        if(on.hasClass('active'))
            section_id = 2;
        else
            section_id = 1;
        $.ajax({
            url: "",
            method: "post",
            data: {section_id: section_id,
                del_filter: "Сбросить"},
            success: function (data) {
                var list = $(data).find('.wr_card_list').html();
                $('.wr_card_list').html(list);
                var form = $(data).find('.section-id').val();
                $('.section-id').val(form);
            }

        });

    });

    $(document).on('click','.transparent', function () {
        var news_count = 20;
        var section_id = $(this).attr('data-type');
        console.log(section_id);
        $.ajax({
            url: "",
            method: "post",
            data: {
                section_id: section_id,
                show_all: news_count
            },
            success: function (data) {
                var list = $(data).find('.wr_card_list').html();
                $('.wr_card_list').html(list);
            }

        });
        $(this).hide();
        return false;
    })

});