<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

        <div class="block_type_center">
            <!--card_list-->
            <div class="wr_card_list">
                <div class="card_list" >
                    <? foreach ($arResult["ITEMS"] as $arItem) {
                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <span  class="section" data-type="<?=$arParams["SECTION_ID"]?>"></span>
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="wr_card_item"
                       id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="card_item">
                            <div class="wr_pict_card_item_substrate"></div>
                            <div class="wr_pict_card_item"
                                 style="background-image: url('<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>');"></div>
                            <div class="left_card_item">
                                <div class="pict_card_item"
                                     style="background-image: url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>');"></div>
                                <div class="b_text">Узнать больше</div>
                            </div>
                            <div class="right_card_item">
                                <div class="title_card_item"><?= $arItem["NAME"] ?></div>
                                <div class="text_card_item">
                                    <?= $arItem["PREVIEW_TEXT"] ?>
                                </div>
                                <div class="bottom_card_item">
                                    <?if($arItem["DISPLAY_PROPERTIES"]["DESCRIPTION_LIST"]["ELEMENT_VALUE"]){
                                        foreach ($arItem["DISPLAY_PROPERTIES"]["DESCRIPTION_LIST"]["ELEMENT_VALUE"] as $key=>$val) {
                                            if($key>2)
                                                continue;
                                            ?><div class="bottom_card_item_block">
                                                <div class="bottom_card_item_title">
                                                    <?=$val["NAME"]?>
                                                </div>
                                                <div class="bottom_card_item_text">
                                                    <?=$val["PREVIEW_TEXT"]?>
                                                </div>
                                            </div><?
                                        }
                                    }?>
                                </div>
                            </div>
                        </div>
                        </a><? } ?>
                </div>
                <?if(count($arResult["ITEMS"]) > $arParams['NEWS_COUNT'])?>
                <div class="button_line <?=(count($arResult["ITEMS"]) > $arParams['NEWS_COUNT']) ? "" : "hidden"?>">
                    <a href="#" class="button transparent maxwidth "  data-type="<?=$arParams["SECTION_ID"]?>">показать все карты</a>
                </div>
            </div>
            <!--card_list-->
        </div>
