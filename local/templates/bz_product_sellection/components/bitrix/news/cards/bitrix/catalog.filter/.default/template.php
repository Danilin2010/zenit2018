<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block_type_right">
    <div class="wr_form_block">
        <form action="<? echo $arResult["FORM_ACTION"] ?>" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
              class="form_block" id="cards_filter">
            <!--filter_block-->
            <? foreach ($arResult["ITEMS"] as $arItem):
                if (array_key_exists("HIDDEN", $arItem)):
                    echo $arItem["INPUT"];
                endif;
            endforeach; ?>
            <? foreach ($arResult["ITEMS"]["QUESTIONS"] as $arItem): ?>
                <? if (!array_key_exists("HIDDEN", $arItem)): ?>
                    <? if ($arItem["INPUT_NAME"] == "arrFilter_pf[CARD_STATUS]"): ?>
                        <div class="filter_block">
                            <div class="filter_block_title">
                                <?= $arItem["NAME"] ?>
                            </div>
                            <div class="filter_block_body">
                                <div class="filter_block_body_item">
                                    <label><input class="formstyle" type="radio" name="<?= $arItem["INPUT_NAME"] ?>"
                                                  value=""/>Все карты</label>
                                </div>
                                <? foreach ($arItem["OPTIONS"] as $id => $option): ?>
                                    <div class="filter_block_body_item">
                                        <label><input
                                                class="formstyle"
                                                type="radio"
                                                name="<?= $arItem["INPUT_NAME"] ?>"
                                                <?if($arItem["INPUT_VALUE"]==$id){?>checked<?}?>
                                                            value="<?= $id ?>"/><?= $option ?></label>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                        <!--filter_block-->
                    <? else: ?>
                        <!--filter_block-->
                        <div class="filter_block">
                            <div class="filter_block_title">
                                <?= $arItem["NAME"] ?>
                            </div>
                            <div class="filter_block_body">
                                <? foreach ($arItem["OPTIONS"] as $id => $option): ?>
                                    <div class="filter_block_body_item">
                                        <label><input class="formstyle" type="checkbox"
                                                      name="<?= $arItem["INPUT_NAME"] ?>"
                                                      value="<?= $id ?>"/><?= $option ?></label>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                        <!--filter_block-->
                    <? endif; ?>
                <? endif; ?>
            <? endforeach; ?>
            <?//echo '<pre>'.print_r($arParams,1).'</pre>';?>
            <input type="hidden" name="section_id" value="<?=$arParams["SECTION_ID"]?>" class="section-id"/>
            <input type="hidden" name="set_filter" value="Y"/>
            <!--filter_block-->
        </form>
    </div>
</div>