$(document).ready(function(){

    var VivusList=[];

    $('.main_slider').on('init', function(event,slick){
        var speed= slick.options.autoplaySpeed/10-slick.options.autoplaySpeed/100;
        if(slick.$dots!=null)
        {
            slick.$dots.find('.svg_a').each(function(){
                var id=$(this).attr('id');
                var vivus=new Vivus($(this).attr('id'),{duration:speed});
                vivus.stop();
                VivusList[VivusList.length]={
                    id:id,
                    vivus:vivus
                }
            });
        }
        if(VivusList.length>0)
            VivusList[0].vivus.play();
    });

    $('.main_slider').on('beforeChange', function(event,slick,currentSlide,nextSlide){
        if(VivusList.length>0)
        {
            VivusList[nextSlide].vivus.setFrameProgress(0);
            VivusList[nextSlide].vivus.play();
        }
    });

    $('.main_slider').hover(function(){
        var index=$(this).find('li.slick-active').index();
        if(VivusList.length>0)
        {
            VivusList[index].vivus.stop();
        }
    },function(){
        var index=$(this).find('li.slick-active').index();
        if(VivusList.length>0)
        {
            VivusList[index].vivus.play();
        }
    });

    $('.main_slider').slick({
        draggable:false,
        touchMove:false,
        autoplay: true,
        pauseOnHover: true,
        autoplaySpeed: 6000,
        useTransform:false,
        fade: true,
        dots: true,
        customPaging: function(slider, i){
            return $('<svg id="svg_a_'+(i+1)+'" class="svg_a" viewBox="0 0 25 25"><path class="path_svg_a" fill="none" stroke="#1da9af" stroke-width="2" d="' +
                'M12.5,23 ' +
                'A11,11 0 0 1 2,12.5 ' +
                'A11,11 0 0 1 12.5,2 ' +
                'A11,11 0 0 1 23,12.5 ' +
                'A11,11 0 0 1 12.5,23 ' +
                '"></path></svg>');
        },
        slide:'.wr_main_slider .main_slider_item',
        appendArrows: $('.wr_main_slider .main_slider_wrapper_naw'),
        appendDots: $('.wr_main_slider .main_slider_wrapper_naw'),
        nextArrow:'<button type="button" class="slick-next"><div class="arr"></div></button>',
        prevArrow:'<button type="button" class="slick-prev"><div class="arr"></div></button>'
    });

    $('.main_news_item_block').slick({
        draggable:false,
        touchMove:false,
        infinite:false,
        useTransform:false,
        slidesToShow: 2,
        slidesToScroll: 2,
        nextArrow:'<button type="button" class="slick-next"><div class="arr"></div></button>',
        prevArrow:'<button type="button" class="slick-prev"><div class="arr"></div></button>',
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    $('.photo_blocks').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var max=$(this).find('.photo_blocks_item').length;
        $(this).find('.caption_all').text(max);
        $(this).find('.caption_num').text(nextSlide+1);
    });
    $('.photo_blocks').slick({
        draggable:false,
        touchMove:false,
        infinite:false,
        useTransform:false,
        slidesToShow: 1,
        slidesToScroll: 1,
        slide:'.photo_blocks .photo_blocks_item',
        nextArrow:'<button type="button" class="slick-next"><div class="arr"></div></button>',
        prevArrow:'<button type="button" class="slick-prev"><div class="arr"></div></button>'
    });


});