var MilitaryCreditCalculator={
    varProgramm:{},
    calculation:{
        table:[]
    },
    test:{
        //paymentDate:'',// Дата платежа
        //paymentPeriodFrom:'',// Платеж за период с
        //paymentPeriodTo:'',// Платеж за период по
        //numberDays:'',// Кол-во дней
        //annuityPaymentAmount:0,// Размер аннуитетного платежа
        //paymentsAccumulative:0,// Платежи накопительно-ипотечной системы
        //meansBorrower:0,// Собственные средства ЗАЕМЩИКА
        //paymentPrincipalDebt:'',// Оплата Основного долга
        //paymentPrincipalInterest:0,// Оплата Основных процентов
        //accumulationCurrentPaymentPeriod:'',// Накопление Основных процентов в текущем платежном периоде
        //paymentAccruedInterest:0,// Оплата накопленных процентов
        //earlyRepaymentPrincipalDebt:0,// Досрочное погашение Основного долга
        //balancePrincipalDebt:_this.variantsParam.summ,// Остаток  Основного долга
        //accumulatedUnpaidInterest:'',// Накопленные неоплаченные  проценты
        //weightStart:'',// весок нач пер
        //weightCon:'',// весок кон пер
        //percentages:''// проценты
        //financialFlow:''// фин.поток
    },
    variantsParam:{
        programType:'secondaryStd',
        conditionSemOrStd:0,
        precent:12.5,
        term:250,
        summ:2600000,
        minSumm:600000,
        maxSumm:2600000,
        registrationPeriod:5,
        dataStart:new Date(2017,(1-1),12),
        dataBirth:new Date(1993,(3-1),19),
        accountCVD:true,
        paymentsNIS:false
    },
    data:{
        programm:'[data-programm]',
        summ:'[data-summ]',
        year:'[data-year]'
    },
    print:{
        monf:'[data-print-monf]',
        summ:'[data-print-summ]',
        rate:'[data-print-rate]'
    },
    initParam:{



    },
    /**functions**/
    /**setPropCalculations**/
    setPropCalculations:function(){
        _this=this;
    },
    /**getPropCalculations**/
    getPropCalculations:function(){
        _this=this;
        var programm=$(_this.data.programm).val();
        if(typeof(_this.varProgramm.varPrecent[programm])!='undefined')
        {
            _this.variantsParam.programType=programm;
            if(_this.varProgramm.accountCVD)
                _this.variantsParam.maxSumm=_this.varProgramm.varPrecent[programm].takingown;
            else
                _this.variantsParam.maxSumm=_this.varProgramm.varPrecent[programm].withown;
            _this.variantsParam.precent=_this.varProgramm.varPrecent[programm].precent;
        }
        var year=$(_this.data.year).val();
        var today = new Date();
        today.setYear(1900+today.getYear()-year);
        _this.variantsParam.dataBirth=new Date(today.getTime());
        _this.variantsParam.dataStart=new Date();
    },

    getAc10:function(){
        _this=this;
        var ac10;
        if(
            _this.calculation.table[_this.calculation.table.length-1].accumulatedUnpaidInterest>0
            ||
            _this.calculation.table[_this.calculation.table.length-1].paymentPrincipalDebt<_this.calculation.table[_this.calculation.table.length-1].balancePrincipalDebt
        )
            ac10=0;
        else
            ac10=1;
        return ac10;
    },
    DateCell:function() {
        _this=this;
        var MaksSrok, DataVyd, MaksFGKU, MinFGKU, MaksSumKred, i,
            Sobsr
        ;
        Sobsr =_this.variantsParam.accountCVD;
        _this.variantsParam.accountCVD=false;
        MinFGKU = _this.variantsParam.minSumm;
        MaksFGKU = _this.variantsParam.maxSumm;
        var day=0;
        var data=new Date();
        if((data.getDate()-_this.variantsParam.dataBirth.getDate())<=0)
            day=1;
        else
            day=0;
        MaksSrok=540-((data.getFullYear()-_this.variantsParam.dataBirth.getFullYear())*12+(data.getMonth()-_this.variantsParam.dataBirth.getMonth())-1*day)-_this.variantsParam.registrationPeriod;


        DataVyd = new Date(_this.variantsParam.dataStart.getTime());


        _this.variantsParam.term=MaksSrok;
        _this.variantsParam.summ=MaksFGKU;
        _this.variantsParam.dataStart=DataVyd;

        MaksSumKred = MaksFGKU;

        _this.initTable();

        var ac10=_this.getAc10();
        var FinalSum=false;


        if(ac10==1){
            FinalSum=true;
        }else{
            for(i=1;parseInt((MaksFGKU-MinFGKU)/1000);i++)
            {
                _this.variantsParam.summ=MaksSumKred - i*1000;
                _this.initTable();
                ac10=_this.getAc10();
                if(ac10==1){
                    MaksSumKred = MaksSumKred - i * 1000;
                    FinalSum=true;
                    break;
                }
            }
        }
        _this.variantsParam.accountCVD=Sobsr;
    },
    PSK:function(){
        _this=this;
        //return _this.PSK2014();
        return Math.round10(_this.PSK2014(),-3);
    },
    PSK2014:function() {
        _this=this;
        var df;
        var PRECISION_REQ = 0.0000001,
            MaxIter = 1000;
        var i,
            BaseP, KBP, NVP, denom, nPr, nPr0, nPr1, Lrate, Hrate, Ek, dRaz, dRazB, dRazB_I, dRazBB,
            done,
            d0,
            count
            ;
        nPr = 0.05;nPr0 = 0;nPr1 = 0.1;count = 0;Lrate = 0.00001;Hrate = 1;var x = 0;
        BaseP = 365 / 12;KBP = 12;
        d0 = new Date(_this.calculation.table[0].paymentDate.getTime());
        done = false;
        var Qk;var DateAdd;
        while(!done)
        {
            NVP = 0;
            Qk = 0;
            Ek = 0;
            denom=(1+Ek*nPr)*Math.pow((1 + nPr),(Qk));
            NVP=NVP+(-1*_this.calculation.table[0].balancePrincipalDebt)/denom;
            for(i=0;i<_this.calculation.table.length;i++){
                df=new Date(_this.calculation.table[i].paymentDate.getTime());
                DateAdd=new Date(_this.variantsParam.dataStart.getTime());
                DateAdd.setMonth(DateAdd.getMonth()+i+1);
                if(df < DateAdd)
                    Qk = i - 2+2;
                else
                    Qk = i - 1+2;
                Ek=(df-DateAdd)/60/60/24/1000/BaseP;
                denom=(1+Ek*nPr)*Math.pow((1 + nPr),(Qk));
                NVP=NVP+_this.calculation.table[i].financialFlow/denom;
            }
            if(NVP < 0)
                nPr1 = nPr;
            else
                nPr0 = nPr;
            if((NVP>=(-1*PRECISION_REQ)) && (NVP<=PRECISION_REQ))
                done = true;
            else
                nPr=nPr0+(nPr1-nPr0)/2;
            if(count > MaxIter)
                done = true;
            count = count + 1
        }
        nPr = nPr * 100 * 12;
        return nPr;
    },
    /**VPR**/
    VPR:function(year,table,month){
        _this=this;
        var res=0;
        $.each(table,function(key,value){
            if(value.year==year)
            {
                if(month==11 && _this.variantsParam.conditionSemOrStd==0)
                {
                    res=value.contributionDecember;
                }else if(month==11 && _this.variantsParam.conditionSemOrStd==8){
                    res=value.monthlyContributionDecember;
                }else if(month!=11 && _this.variantsParam.conditionSemOrStd==0){
                    res=value.monthlyFundedContributionPlannedYear;
                }else if(month!=11 && _this.variantsParam.conditionSemOrStd==8){
                    res=value.amountContributionPlannedYear;
                }
            }
        });
        return res;
    },
     /**initCallbackForm**/
    initCallbackForm:function(){
        _this=this;
        $(_this.data.programm).on('change',function(e){
            setTimeout(function(){_this.wrCalculations();},300);
        });
        $(_this.data.summ).on('change',function(e){
            setTimeout(function(){_this.wrCalculations();},300);
         });
        $(_this.data.year).on('change',function(e){
            //_this.wrCalculations();
        });
    },
    initTable:function(){
        _this=this;
        _this.calculation.table=[];
        var temp1=_this.GetFirstLine();
        _this.calculation.table[_this.calculation.table.length]=temp1;
        var use=true;
        do {
            var temp2=_this.GetTableLine(_this.calculation.table[_this.calculation.table.length-1],_this.calculation.table.length);
            //if(temp2.balancePrincipalDebt>0)
            if((_this.calculation.table.length+1)<=_this.variantsParam.term && temp2)
                _this.calculation.table[_this.calculation.table.length]=temp2;
            else
                use=false;
        }while(use)
    },
    /**getStartParam**/
    getStartParam:function(){
        _this.DateCell();
        _this.calculation.PSK=_this.PSK();
    },
    /**GetTableLine**/
    GetTableLine:function(item,key){
        _this=this;
        var NewBlock={};
        if(key<_this.variantsParam.term)
        {
            NewBlock.balancePrincipalDebt=Math.round10(item.balancePrincipalDebt-item.paymentPrincipalDebt-item.earlyRepaymentPrincipalDebt,-2);
        }else{
            return false;
        }

        NewBlock.paymentPeriodFrom=new Date(item.paymentPeriodTo.getTime());
        NewBlock.paymentPeriodFrom.setDate(item.paymentPeriodTo.getDate()+1);
        NewBlock.paymentPeriodTo=getLastDayOfMonth(NewBlock.paymentPeriodFrom.getFullYear(), NewBlock.paymentPeriodFrom.getMonth());

        var lastDay=getLastDayOfMonth(NewBlock.paymentPeriodFrom.getFullYear(), NewBlock.paymentPeriodFrom.getMonth());
        var dayNed=[7,1,2,3,4,5,6][lastDay.getDay()];
        if
        (dayNed>5)
        {
            NewBlock.paymentDate=new Date(lastDay.getTime());
            NewBlock.paymentDate.setDate(lastDay.getDate()+8-dayNed);
        }else{
            NewBlock.paymentDate=new Date(lastDay.getTime());
        }
        if(key==_this.variantsParam.term)
        {
            NewBlock.numberDays=getDayDelta(NewBlock.paymentDate,-NewBlock.paymentPeriodFrom)+1;
        }else{
            NewBlock.numberDays= getDayDelta(NewBlock.paymentPeriodTo,NewBlock.paymentPeriodFrom)+1
        }


        NewBlock.weightStart=_this.checkYear(NewBlock.paymentPeriodFrom.getFullYear());
        NewBlock.weightCon=_this.checkYear(NewBlock.paymentPeriodTo.getFullYear());


        var endLastMonf=new Date(item.paymentDate.getFullYear(),item.paymentDate.getMonth(),item.paymentDate.daysInMonth());
        var endMonf=new Date(NewBlock.paymentPeriodFrom.getFullYear(),NewBlock.paymentPeriodFrom.getMonth(),NewBlock.paymentPeriodFrom.daysInMonth());

        if(NewBlock.weightStart==0 && NewBlock.weightCon==0)
        {
            NewBlock.percentages=Math.round10(
                _this.variantsParam.precent/36500*
                (getDayDelta(item.paymentDate,item.paymentPeriodTo)*item.balancePrincipalDebt
                +(NewBlock.numberDays-getDayDelta(item.paymentDate,item.paymentPeriodTo))*NewBlock.balancePrincipalDebt)
                ,-2);
        }else{
            if(NewBlock.weightStart==1 && NewBlock.weightCon==1)
            {
                NewBlock.percentages=Math.round10(_this.variantsParam.precent/36600*(getDayDelta(item.paymentDate,item.paymentPeriodTo)*item.balancePrincipalDebt+(NewBlock.numberDays-getDayDelta(item.paymentDate,item.paymentPeriodTo))*NewBlock.balancePrincipalDebt),-2);
            }else{
                if(NewBlock.weightStart==1 && NewBlock.weightCon==0)
                {
                    NewBlock.percentages=Math.round10(_this.variantsParam.precent*(getDayDelta(item.paymentDate,item.paymentPeriodTo)/36600*item.balancePrincipalDebt+ getDayDelta(endLastMonf,item.paymentDate)/36600*NewBlock.balancePrincipalDebt+(NewBlock.numberDays-( getDayDelta(endMonf,NewBlock.paymentPeriodFrom)+1))/36500*NewBlock.balancePrincipalDebt),-2);
                }else{
                    if(NewBlock.weightStart==0 && NewBlock.weightCon==1)
                    {
                        NewBlock.percentages=Math.round10(_this.variantsParam.precent*( getDayDelta(item.paymentDate,item.paymentPeriodTo)/36500*item.balancePrincipalDebt+ getDayDelta(endLastMonf,item.paymentDate)/36500*NewBlock.balancePrincipalDebt+(NewBlock.numberDays-( getDayDelta(endMonf,NewBlock.paymentPeriodFrom)+1))/36600*NewBlock.balancePrincipalDebt),-2);
                    }else{
                        NewBlock.percentages=0;
                    }
                }
            }
        }
        NewBlock.paymentPrincipalInterest=Math.round10(NewBlock.percentages,-2);


        if(key<_this.variantsParam.term && NewBlock.balancePrincipalDebt!=0)
        {
            if((key+1)==_this.variantsParam.term ||
                Math.round10(
                    _this.variantsParam.summ*_this.variantsParam.precent/1200/(1-Math.pow((1+_this.variantsParam.precent/1200),(1-_this.variantsParam.term)))
                    -Math.round10(NewBlock.percentages,-2),-2
                )>NewBlock.balancePrincipalDebt
            )
            {
                NewBlock.paymentPrincipalDebt=NewBlock.balancePrincipalDebt;
            }else{
                NewBlock.paymentPrincipalDebt=Math.round10(
                    _this.variantsParam.summ*_this.variantsParam.precent/1200/(1-Math.pow((1+_this.variantsParam.precent/1200),(1-_this.variantsParam.term)))
                    -Math.round10(NewBlock.percentages,-2)
                    ,-2);
            }
        }else{
            NewBlock.paymentPrincipalDebt=0;
        }

        NewBlock.paymentsAccumulative=0;
        if(key==1)
        {
            if((key+1)<=_this.variantsParam.term && NewBlock.balancePrincipalDebt!=0)
            {
                if(NewBlock.paymentPrincipalDebt==NewBlock.balancePrincipalDebt)
                {
                    //NewBlock.paymentsAccumulative+=I23;
                }else{
                    NewBlock.paymentsAccumulative+=_this.VPR(
                        NewBlock.paymentPeriodFrom.getFullYear(),
                        _this.varProgramm.contributions,
                        NewBlock.paymentPeriodFrom.getMonth()
                    );
                }
            }else{
                NewBlock.paymentsAccumulative+0;
            }
            if((key+1)<=_this.variantsParam.term && item.balancePrincipalDebt!=0)
            {
                if(item.paymentPrincipalDebt==item.balancePrincipalDebt)
                {
                    //NewBlock.paymentsAccumulative+=I22;
                }else{
                    NewBlock.paymentsAccumulative+=_this.VPR(
                        item.paymentPeriodFrom.getFullYear(),
                        _this.varProgramm.contributions,
                        item.paymentPeriodFrom.getMonth()
                    );
                }
            }else{
                NewBlock.paymentsAccumulative+0;
            }
        }else{
            if((key+1)<=_this.variantsParam.term)
            {
                var temp=_this.VPR(
                    NewBlock.paymentPeriodFrom.getFullYear(),
                    _this.varProgramm.contributions,
                    NewBlock.paymentPeriodFrom.getMonth()
                );

                if(temp>(NewBlock.balancePrincipalDebt+Math.round10(NewBlock.percentages,-2)+item.accumulatedUnpaidInterest))
                {
                    NewBlock.paymentsAccumulative=NewBlock.balancePrincipalDebt+Math.round10(NewBlock.percentages,-2)+item.accumulatedUnpaidInterest;
                }else{
                    NewBlock.paymentsAccumulative=temp;
                }
            }else{
                NewBlock.paymentsAccumulative=0;
            }
        }

        if((key+1)<=_this.variantsParam.term)
        {
            if((NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt-NewBlock.paymentPrincipalInterest)<0 || item.accumulatedUnpaidInterest==0)
            {
                NewBlock.paymentAccruedInterest=0;
            }else{
                if((NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt-NewBlock.paymentPrincipalInterest)>item.accumulatedUnpaidInterest)
                    NewBlock.paymentAccruedInterest=item.accumulatedUnpaidInterest;
                else
                    NewBlock.paymentAccruedInterest=NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt-NewBlock.paymentPrincipalInterest;
            }
        }else{
            NewBlock.paymentAccruedInterest=0;
        }




        if((key+1)<=_this.variantsParam.term)
        {
            if(
                key==_this.variantsParam.term
                ||
                Math.round10(_this.variantsParam.summ*_this.variantsParam.precent/1200/(1-Math.pow((1+_this.variantsParam.precent/1200),(1-_this.variantsParam.term))),-2)
                >
                NewBlock.balancePrincipalDebt+(item.accumulatedUnpaidInterest-NewBlock.paymentAccruedInterest)+Math.round10(NewBlock.percentages,-2)
            )
            {
                NewBlock.annuityPaymentAmount=NewBlock.balancePrincipalDebt+
                    (item.accumulatedUnpaidInterest-NewBlock.paymentAccruedInterest)+Math.round10(NewBlock.percentages,-2)
            }else{
                NewBlock.annuityPaymentAmount=Math.round10(_this.variantsParam.summ*_this.variantsParam.precent/1200/(1-Math.pow((1+_this.variantsParam.precent/1200),(1-_this.variantsParam.term))),-2)
            }
        }else{
            NewBlock.annuityPaymentAmount=0;
        }







        NewBlock.meansBorrower=0;
        if(key==1)
        {
            if(NewBlock.balancePrincipalDebt==0)
            {
                NewBlock.meansBorrower+=0;
            }else{
               if(!_this.variantsParam.accountCVD)
               {
                   NewBlock.meansBorrower+=0;
               }else{
                   if((item.accumulationCurrentPaymentPeriod-NewBlock.paymentsAccumulative+NewBlock.annuityPaymentAmount)<0)
                   {
                       NewBlock.meansBorrower+=0;
                   }else{
                       NewBlock.meansBorrower+=item.accumulationCurrentPaymentPeriod-NewBlock.paymentsAccumulative+NewBlock.annuityPaymentAmount;
                   }
               }
            }
        }else{
            if(NewBlock.balancePrincipalDebt==0)
            {
                NewBlock.meansBorrower+=0;
            }else{
                if(!_this.variantsParam.accountCVD)
                {
                    NewBlock.meansBorrower+=0;
                }else{
                    if(NewBlock.paymentsAccumulative>=NewBlock.annuityPaymentAmount)
                    {
                        NewBlock.meansBorrower+=0;
                    }else{
                        NewBlock.meansBorrower+=NewBlock.annuityPaymentAmount-NewBlock.paymentsAccumulative;
                    }
                }
            }
        }
        //meansBorrower

        NewBlock.accumulationCurrentPaymentPeriod=0;
        if(key==1)
        {
            if(NewBlock.balancePrincipalDebt==0)
            {
                NewBlock.accumulationCurrentPaymentPeriod=0;
            }else{
                if(NewBlock.annuityPaymentAmount-NewBlock.paymentsAccumulative-NewBlock.meansBorrower<0)
                {
                    NewBlock.accumulationCurrentPaymentPeriod=0;
                }else{
                    NewBlock.accumulationCurrentPaymentPeriod=NewBlock.annuityPaymentAmount-NewBlock.paymentsAccumulative-NewBlock.meansBorrower;
                }
            }
        }else{
            if(NewBlock.balancePrincipalDebt==0)
            {
                NewBlock.accumulationCurrentPaymentPeriod=0;
            }else{
                if(Math.round10(NewBlock.percentages,-2)>NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt)
                {
                    NewBlock.accumulationCurrentPaymentPeriod=Math.round10(NewBlock.percentages-(NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt)-NewBlock.meansBorrower,-2);
                }else{
                    NewBlock.accumulationCurrentPaymentPeriod=Math.round10(NewBlock.percentages-(Math.round10(NewBlock.percentages,-2))-NewBlock.meansBorrower,-2);
                }
            }
        }
        //accumulationCurrentPaymentPeriod

        NewBlock.earlyRepaymentPrincipalDebt=0;

        if((key+1)<=_this.variantsParam.term && NewBlock.balancePrincipalDebt!=0)
        {
            if((NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt-NewBlock.paymentPrincipalInterest-NewBlock.paymentAccruedInterest)<0)
            {
                NewBlock.earlyRepaymentPrincipalDebt=0;
            }else{
                NewBlock.earlyRepaymentPrincipalDebt=NewBlock.paymentsAccumulative-NewBlock.paymentPrincipalDebt-NewBlock.paymentPrincipalInterest-NewBlock.paymentAccruedInterest;
            }
        }else{
            NewBlock.earlyRepaymentPrincipalDebt=0;
        }
        //earlyRepaymentPrincipalDebt

        NewBlock.accumulatedUnpaidInterest=Math.round10(
            (_this.GetSummAccumulationCurrentPaymentPeriod()+NewBlock.accumulationCurrentPaymentPeriod)
            -(_this.GetSummPaymentAccruedInterest()+NewBlock.paymentAccruedInterest),
            -2);

        //accumulatedUnpaidInterest



        NewBlock.financialFlow=NewBlock.paymentsAccumulative+NewBlock.meansBorrower;


        return NewBlock;
    },
    GetSummPaymentAccruedInterest:function() {
        _this=this;
        var summ=0;
        $.each(_this.calculation.table,function(key,value){
            summ+=value.paymentAccruedInterest;
        });
        return summ;
    },
    GetSummAccumulationCurrentPaymentPeriod:function() {
        _this=this;
        var summ=0;
        $.each(_this.calculation.table,function(key,value){
            summ+=value.accumulationCurrentPaymentPeriod;
        });
        return summ;
    },
    /**GetFirstLine**/
    GetFirstLine:function(){
        _this=this;
        var New={};
        New.balancePrincipalDebt=_this.variantsParam.summ;
        New.paymentPeriodFrom=new Date(_this.variantsParam.dataStart.getTime());;
        New.paymentPeriodFrom.setDate(_this.variantsParam.dataStart.getDate()+1);
        New.paymentDate=getLastDayOfMonth(New.paymentPeriodFrom.getFullYear(), New.paymentPeriodFrom.getMonth());
        New.paymentPeriodTo=new Date(New.paymentDate.getTime());
        New.numberDays=getDayDelta(New.paymentPeriodTo,New.paymentPeriodFrom)+1;
        New.annuityPaymentAmount=0;
        New.paymentsAccumulative=0;
        New.meansBorrower=0;
        New.paymentPrincipalInterest=0;
        New.financialFlow=0;

        New.weightStart=_this.checkYear(New.paymentPeriodFrom.getFullYear());
        New.weightCon=_this.checkYear(New.paymentPeriodTo.getFullYear());

        var endEar=new Date(New.paymentPeriodFrom.getFullYear(),11,31);
        if(New.weightStart==0 && New.weightCon==0)
        {
            New.percentages=Math.round10(New.balancePrincipalDebt*_this.variantsParam.precent/36500*New.numberDays,-2);
        }else{
            if(New.weightStart==1 && New.weightCon==1)
            {
                New.percentages=Math.round10(New.balancePrincipalDebt*_this.variantsParam.precent/36600*New.numberDays,-2);
            }else{
                if(New.weightStart==1 && New.weightCon==0)
                {
                    New.percentages=Math.round10(
                        New.balancePrincipalDebt*_this.variantsParam.precent*
                        (
                            (getDayDelta(endEar,New.paymentPeriodFrom)+1)/36600+
                            (New.numberDays-(getDayDelta(endEar,New.paymentPeriodFrom)+1))/36500
                        )
                        ,-2);
                }else{
                    if(New.weightStart==0 && New.weightCon==1)
                    {
                        New.percentages=Math.round10(
                            New.balancePrincipalDebt*_this.variantsParam.precent*(
                                (getDayDelta(endEar,New.paymentPeriodFrom)+1)/36500+
                                (New.numberDays-(getDayDelta(endEar,New.paymentPeriodFrom)+1))/36600
                            )
                            ,-2);
                    }else{
                        New.percentages=0;
                    }
                }
            }
        }
        New.accumulationCurrentPaymentPeriod=Math.round10(New.percentages-New.paymentPrincipalInterest,-2);
        var paymentPrincipalDebt=New.paymentsAccumulative-New.annuityPaymentAmount-New.accumulationCurrentPaymentPeriod;
        if(paymentPrincipalDebt<0)
            New.paymentPrincipalDebt=0;
        else
            New.paymentPrincipalDebt=paymentPrincipalDebt;
        New.paymentAccruedInterest=0;
        New.earlyRepaymentPrincipalDebt=0;
        New.accumulatedUnpaidInterest=Math.round10((New.accumulationCurrentPaymentPeriod-New.paymentAccruedInterest),-2);


        return New;
    },
    /**checkYear**/
    checkYear:function(year) {
        return (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) ? 1 : 0;
    },
    /**printResult**/
    printResult:function(){
        _this=this;
        $(_this.print.monf).text($.number(_this.calculation.table[1].annuityPaymentAmount, 0,',',' '));
        $(_this.print.summ).text($.number(_this.variantsParam.summ, 0,',',' '));
        $(_this.print.rate).text($.number(_this.variantsParam.precent, 2,',',' '));
    },
    /**wrCalculations**/
    wrCalculations:function(){
        _this=this;
        _this.getPropCalculations();
        _this.initvarProgramm();
        _this.getStartParam();
        _this.printResult();

    }/*.process()*/,
    /**initvarProgramm**/
    initvarProgramm:function(){
        _this=this;

        if(_this.variantsParam.programType=='primaryStd' || _this.variantsParam.programType=='secondaryStd')
            _this.variantsParam.conditionSemOrStd=8;
        else
            _this.variantsParam.conditionSemOrStd=0;

        $.each(_this.varProgramm.contributions,function(key,value){
            value.monthlyFundedContributionPlannedYear=value.amountContributionPlannedYear*2;
            value.contributionDecember=value.monthlyContributionDecember*2;
            if(_this.variantsParam.paymentsNIS)
                value.fundedContributionPlannedYear=value.fixed;
            else
                value.fundedContributionPlannedYear=value.old;

            if(typeof(value.accumulativeContributionPreviousYear)=='undefined')
                value.accumulativeContributionPreviousYear=_this.varProgramm.contributions[key-1].fundedContributionPlannedYear;

            if(typeof(value.indicesDeflators)=='undefined')
                value.indicesDeflators=value.fundedContributionPlannedYear/value.accumulativeContributionPreviousYear;
            value.propertyProhibitChange=Math.floor(value.fundedContributionPlannedYear/12);

            value.i=value.fundedContributionPlannedYear-value.monthlyFundedContributionPlannedYear*11;
            value.k=Math.floor(value.old/12);
            value.l=value.old-value.k*11;
        });
    },
    /**initSlider**/
    initSlider:function(){
        _this=this;
        $(".card_to_card_select").each(function(){
            var this2=this,
                min=$(this2).data('min'),
                max=$(this2).data('max'),
                target=$(this2).data('target'),
                value=$(this2).data('value'),
                suffix=$(this2).data('suffix'),
                del=$(this2).data('del'),
                elTarget=$('#'+target);
            if(typeof(del)=="undefined")
            {
                del=1;
            }else{
                del=parseInt(del);
                if(del<=0)
                    del=1;
            }

            if(typeof(suffix)=="undefined")
                suffix="";
            $(this2).slider({
                range: "min",
                max: max,
                min: min,
                value:value,
                create: function(){
                    elTarget.val($(this).slider("value"));
                    //_this.wrCalculations();
                },
                change: function( event, ui ) {
                    elTarget.val(ui.value);
                    setTimeout(function(){_this.wrCalculations();},300);
                },
                slide: function(event,ui){
                    elTarget.val(ui.value);
                    //_this.wrCalculations();
                }
            }).slider("pips",{
                step:(max-min)/2,
                //step:(max)/2-min,
                suffix: suffix,
                formatLabel: function(value) {
                    return this.prefix + value/del + this.suffix;
                },
                rest: "label"
            });
            elTarget.number(true,0,',',' ');
            elTarget.on('change',function(){
                var val=$(this).val();
                $(this2).slider("value",val);
            });
        });

    },
    /**init**/
    init:function(varProgramm){
        _this=this;

        _this.varProgramm=varProgramm;

        _this.initSlider();

        _this.initCallbackForm();


        setTimeout(function(){_this.wrCalculations();},300);


        //console.log(_this.variantsParam);
        //console.log(_this.calculation);

        //console.log(_this);
    }

}
