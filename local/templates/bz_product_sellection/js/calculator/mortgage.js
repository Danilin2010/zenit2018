var CreditCalculator={
    varProgramm:{},
    calculation:{max:{},client:{},coefficient:{pti:{},oti:{}}},
    variantsParam:{
        currency:['RUR','USD','EUR'],
        proofIncome:['accordingBank','2NDFL'],
        typeLoan:['mortgage','consumer'],
        region:['moscow','region'],
        useconversion:['cost_real_estate','amount_credit','year','yearmin']
    },
    useconversion:{
        cost_real_estate:[
            {val:5000000,percent:50},
            {val:15000000,percent:75}
        ],
        amount_credit:[
            {val:10000000,percent:50},
            {val:20000000,percent:75}
        ],
        year:[
            {val:1,percent:0},
            {val:5,percent:16},
            {val:10,percent:32},
            {val:15,percent:48},
            {val:20,percent:64},
            {val:25,percent:80},
            {val:30,percent:96}
        ],
        yearmin:[
            {val:1,percent:0},
            {val:5,percent:50},
            {val:9,percent:100}
        ]
    },
    data:{
        programm:'[data-programm]',
        region:'[data-region]',
        summ:'[data-summ]',
        contribution:'[data-contribution]',
        year:'[data-year]',
        amountCredit:'[data-amount-credit]',
        monthlyPayment:'[data-monthly-payment]',
        contributionrub:'[data-contributionrub]',
        togglefirst:'[data-toggle-first]',
        firstprecent:'[data-first-precent]',
        firstrub:'[data-first-rub]',
        client:'[data-client]',
        pledgetext:'[data-pledgetext]',
        yearsklonenie:'[data-yearsklonenie]',
        hidepledge:'[data-hidepledge]',
        hideregion:'[data-hideregion]'
    },
    print:{
        monf:'[data-print-monf]',
        summ:'[data-print-summ]',
        rate:'[data-print-rate]',
        first:'[data-print-first]',
        firstrub:'[data-print-first-rub]'
    },
    initParam:{
        client:false,
        summ:5000000,
        first:0,
        term:180,
        currency:'RUR',
        course:1,
        proofIncome:'accordingBank',
        typeLoan:'mortgage',
        region:'moscow',
        pti:60,
        oti:70,
        percent:13.5,
        incomeAccordingBank:{
            borrower: 169007.58, coBorrowers: 0
        },
        income2NDFL:[
            {borrower:84503.79,coBorrowers:84503.79}
        ],
        costs:{
            livingWage:18947,
            familyComposition:1,
            otherCredit:0,
            alimony:0,
            additionalCosts:0
        }
    },
    /**functions**/
    /**setPropCalculations**/
    setPropCalculations:function(){
        _this=this;
        var programm=$(_this.data.programm).val();
        var client=$(_this.data.client).prop('checked');
        _this.initParam.client=client;
        var region=$(_this.data.region+':checked').val();
        _this.setYearCredit(programm);
        _this.setFirstCalk();
        _this.setSumm(programm,region);
        //_this.setFirstCalkRub(programm,region);
    },
    /**setSumm**/
    setSumm:function(programm,region){
        _this=this;
        var min=0,max=0;
        if(typeof(_this.varProgramm[programm].tab1.cost.min[region])!='undefined')
            min=_this.varProgramm[programm].tab1.cost.min[region];
        if(typeof(_this.varProgramm[programm].tab1.cost.max[region])!='undefined')
            max=_this.varProgramm[programm].tab1.cost.max[region];
        _this.setSliderMinMax(_this.data.summ,max,min);
        var creditMin=0,creditMax=0;
        if(_this.varProgramm[programm].pledge)
        {
            //creditMin=min;
            //creditMax=max/**70/100*/;
            if(typeof(_this.varProgramm[programm].tab1.summ.min[region])!='undefined')
                creditMin=parseInt(_this.varProgramm[programm].tab1.summ.min[region]);
            if(typeof(_this.varProgramm[programm].tab1.summ.max[region])!='undefined')
                creditMax=parseInt(_this.varProgramm[programm].tab1.summ.max[region]);

        }else{
            if(typeof(_this.varProgramm[programm].tab1.summ.min[region])!='undefined')
                creditMin=parseInt(_this.varProgramm[programm].tab1.summ.min[region]);
            if(typeof(_this.varProgramm[programm].tab1.summ.max[region])!='undefined')
                creditMax=parseInt(_this.varProgramm[programm].tab1.summ.max[region]);
        }
        _this.setSliderMinMax(_this.data.amountCredit,creditMax,creditMin);
        //_this.setSliderMinMax(_this.data.contributionrub,creditMax,0);
    },
    /**setFirstCalk**/
    setFirstCalk:function(){
        _this=this;
        var programm=$(_this.data.programm).val();
        var summ=parseInt($(_this.data.summ).val());
        var region=$(_this.data.region+':checked').val();

        var min=0,max=0;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            min=_this.varProgramm[programm].tab1.contribution.min;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            max=_this.varProgramm[programm].tab1.contribution.max;
        if(min==0)
            min=0;
        if(max==0)
            max=0;


        var Rmin=(summ/100)*min;
        var Rmax=(summ/100)*max;
        var maxSumm=_this.varProgramm[programm].tab1.summ.max[region];
        var minSumm=_this.varProgramm[programm].tab1.summ.min[region];

        /*if(typeof(maxSumm)!='undefined' && typeof(minSumm)!='undefined')
        {
            maxSumm=parseInt(maxSumm);
            minSumm=parseInt(minSumm);

            if((summ-Rmax)<minSumm)
                max=Math.round10(100*(summ-minSumm)/summ,0);

            if((summ-Rmin)>maxSumm)
                min=Math.round10(100*(summ-maxSumm)/summ,0);
        }*/

        _this.setSliderMinMax(_this.data.contribution,max,min);
    },
    setFirstCalkRub:function(){
        var summ=parseInt($(_this.data.summ).val());
        var programm=$(_this.data.programm).val();
        var region=$(_this.data.region+':checked').val();

        _this=this;
        var min=0,max=0;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            min=_this.varProgramm[programm].tab1.contribution.min;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            max=_this.varProgramm[programm].tab1.contribution.max;
        if(min==0)
            min=0;
        if(max==0)
            max=0;

        var Rmin=(summ/100)*min;
        var Rmax=(summ/100)*max;

        var maxSumm=_this.varProgramm[programm].tab1.summ.max[region];
        var minSumm=_this.varProgramm[programm].tab1.summ.min[region];
        _this.setSliderMinMax(_this.data.contributionrub,Rmax,Rmin);
    },
    /**setYearCredit**/
    setYearCredit:function(programm){
        _this=this;
        var min=0,max=0;
        $.each(_this.varProgramm[programm].tab2,function(key,value){
            if(typeof(value.yearMin)!='undefined' && (value.yearMin<min || min==0))
                min=value.yearMin;
            if(typeof(value.yearMax)!='undefined' && (value.yearMax>max || max==0))
                max=value.yearMax;
        });
        if(min==0)
            min=1;
        if(max==0)
            max=30;
        _this.setSliderMinMax(_this.data.year,max,min);
    },
    reinitSlider:function(element){
        $(element).slider2("reInit",{});
    },
    /**setSliderMinMax**/
    setSliderMinMax:function(element,max,min){
        max=parseInt(max);
        min=parseInt(min);
        var programm=$(_this.data.programm).val();
        var id=$(element).attr('id');
        var slider=$("[data-target="+id+"]");
        var val=$(element).val();
        var textVal=val;
        var textMax=max;
        var textMin=min;

        var useconversion=$(slider).data('useconversion');
        var useconversionmin=$(slider).data('useconversionmin');


        if(slider.hasClass('card_to_card_select2'))
        {
            var heterogeneity=false;
            var stepScale=4;
            var scale=[];
            scale[scale.length]=_this.getLabelSlider(min);
            for(var i=min+(max-min)/stepScale;i<max;i+=(max-min)/stepScale)
                scale[scale.length]=_this.getLabelSlider(i);
            scale[scale.length]=_this.getLabelSlider(max);

            if(typeof(slider.data('conversion'))!='undefined')
            {

                var prop={
                    max:max,
                    min:min
                };
                if(maxmin>=max)
                {
                    prop.grid=_this.useconversion[useconversionmin];
                }else{
                    prop.grid=_this.useconversion[useconversion];
                }
                var conversion=slider.data('conversion');
                conversion.reInit(prop);

                scale=[];
                scale[scale.length]=_this.getLabelSlider(conversion.fromSlider(0));
                for(var i=100/stepScale;i<100;i+=100/stepScale)
                    scale[scale.length]=_this.getLabelSlider(conversion.fromSlider(i));
                scale[scale.length]=_this.getLabelSlider(conversion.fromSlider(100));

                heterogeneity=[];
                jQuery.each(_this.useconversion[useconversion],function(index,value){
                    heterogeneity[heterogeneity.length]=value.percent+'/'+value.val;
                });
            }

            if(_this.varProgramm[programm].pledge==true)
                heterogeneity=false;

            $(element).slider2("reInit",{
                from: min,
                to: max,
                heterogeneity:heterogeneity,
                scale: scale
            });

           /* console.log(min);
            console.log(scale);*/

            if(val>max)
                $(element).slider2("value",max);
            else if(val<min)
                $(element).slider2("value",min);
            else
                $(element).slider2("value",val);

        }else{
            var stepdel=$(slider).data('stepdel');
            if(typeof(stepdel)=="undefined")
                stepdel=1;
            else{
                stepdel=parseInt(stepdel);
                if(stepdel<=0)
                    stepdel=1;
            }
            var stepdelh=$(slider).data('stepdelh');

            var stepdelhptir1=parseInt($(slider).data('stepdelhptir1'));
            var stepdelhtir1=parseInt($(slider).data('stepdelhtir1'));
            var stepdelhptir2=parseInt($(slider).data('stepdelhptir2'));
            var stepdelhtir2=parseInt($(slider).data('stepdelhtir2'));

            if(max<=stepdelhptir1 && stepdelhtir1>0)
                stepdelh=stepdelhtir1;
            if(max<=stepdelhptir2 && stepdelhtir2>0)
                stepdelh=stepdelhtir2;

            if(typeof(stepdelh)=="undefined")
                stepdelh=4;
            else{
                stepdelh=parseInt(stepdelh);
                if(stepdelh<=0)
                    stepdelh=4;
            }
            var stepdelmin=$(slider).data('stepdelmin');
            var maxmin=$(slider).data('maxmin');

            if(typeof(slider.data('conversion'))!='undefined')
            {
                var prop={
                    max:max,
                    min:min
                };
                if(maxmin>=max)
                {
                    prop.grid=_this.useconversion[useconversionmin];
                }else{
                    prop.grid=_this.useconversion[useconversion];
                }
                var conversion=slider.data('conversion');
                conversion.reInit(prop);
                textMin=conversion.toSlider(textMin);
                textMax=conversion.toSlider(textMax);
                textVal=conversion.toSlider(textVal);
                slider.data('conversion',conversion);
            }

            if(maxmin>=max)
            {
                stepdel=stepdelmin;
            }

            var steppips;
            if(stepdelh!=false)
                steppips=(textMax-textMin)/stepdelh;
            else
                steppips=false;

            slider
                .slider("option","max",textMax)
                .slider("option","min",textMin)
                .slider("option","step",stepdel)
                .slider("pips","options",'step',steppips)
                .slider("pips", "refresh")
            ;
            if(val>max)
            {
                $(element).val(max);
                $(element).addClass('changes');
                $(slider).slider("value",textMax);
            }else if(val<min)
            {
                $(element).val(min);
                $(element).addClass('changes');
                $(slider).slider("value",textMin);
            }else{
                $(element).addClass('changes');
                $(slider).slider("value",textVal);
            }
        }
    },
    /**initCallbackForm**/
    initCallbackForm:function(){
        _this=this;
        $(_this.data.client).on('change',function(e){
            _this.altwrCalculations();
        });
        $(_this.data.programm).on('change',function(e){
            _this.setPropCalculations();
            _this.wrCalculations();
        });
        $(_this.data.region).on('change',function(e){
            _this.altwrCalculations();
        });
        $(_this.data.summ).on('change',function(e){
            //_this.setSummInput(parseInt($(this).val()));
            //_this.wrCalculations();
        });
        $(_this.data.contribution).on('change',function(e){
            //_this.setSummInput(parseInt($(this).val()));
            //_this.wrCalculations();
        });
        $(_this.data.year).on('change',function(e){
            _this.wrCalculations();
        });

        $(_this.data.monthlyPayment).number(true,0,',',' ');

        $(_this.data.monthlyPayment).on('change',function(e){

            _this.setReversInput(parseInt($(this).val()));

        });

        $(_this.data.amountCredit).on('change',function(e){
            //_this.setAmountSumm(parseInt($(this).val()));
        });

        $(_this.data.contributionrub).on('change',function(e){
            //_this.setFirstSumm(parseInt($(this).val()));
        });

        $(_this.data.togglefirst).on('toggle', function(e, active) {
            if(active){
                $(_this.data.firstprecent).hide();
                $(_this.data.firstrub).show();
            }else{
                $(_this.data.firstprecent).show();
                $(_this.data.firstrub).hide();
            }
        });

    },

    /****/
    setSummInput:function(contribution){
        _this=this;

        //var summ=$(_this.data.summ).val();
        //var first=contribution*summ/100;
        //_this.printResultSlider(_this.data.contributionrub,first);
        //_this.printResultSlider(_this.data.contribution,firstrub);


        var summ=parseInt($(_this.data.summ).val());
        var first=contribution*summ/100;
        //var contribution=100*first/summ;

        var all=summ-first;
        _this.printResultSlider(_this.data.contributionrub,first);
        _this.printResultSlider(_this.data.amountCredit,all);
        _this.calculation.client.summ=all;
        _this.calculation.client.first=first;
        _this.calculation.client.firstrub=contribution;
        // _this.wrCalculations();
        _this.getStartParam();
        //_this.setFirstCalkRub();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();

    },

    setReversInput:function(old_annuity){
        _this=this;
        _this.calculation.client.annuity=annuity;
        var annuity=old_annuity;
        var percent=_this.calculation.client.percent;
        var term=_this.calculation.client.term;
        var first=_this.calculation.client.first;

        var programm=$(_this.data.programm).val();
        var region=$(_this.data.region+':checked').val();



        var summ=annuity*(12/percent*100)*(1-Math.pow(1+percent/100/12,-term));

        var cost=_this.varProgramm[programm].tab1.summ;
        var minSumm=parseInt(cost.min[region]);
        var maxSumm=parseInt(cost.max[region]);
        if(summ<minSumm)
            summ=minSumm;
        if(summ>maxSumm)
            summ=maxSumm;


        if(_this.varProgramm[programm].pledge==true)
        {
            var AllSumm=summ*0.7;
            _this.printResultSlider(_this.data.summ,AllSumm);
            _this.calculation.client.summ=summ;

        }else{
            var AllSumm=first+summ;
            _this.printResultSlider(_this.data.summ,AllSumm);
            var firstrub=100*(first/AllSumm);
            _this.calculation.client.summ=summ;
            _this.calculation.client.firstrub=firstrub;
            _this.printResultSlider(_this.data.contribution,firstrub);
        }




        _this.getStartParam();
        _this.setFirstCalkRub();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();


    },

    setMainInput:function(Summ){
        _this=this;
        Summ=parseInt(Summ);
        var AllSumm=Summ;

        var programm=$(_this.data.programm).val();

        var precent=_this.varProgramm[programm].tab1.contribution;
        var region=$(_this.data.region+':checked').val();

        var first=parseInt($(_this.data.contributionrub).val());


        if(_this.varProgramm[programm].pledge==true)
        {
            var cost=_this.varProgramm[programm].tab1.cost;
            var minAll=parseInt(cost.min[region]);
            var maxAll=parseInt(cost.max[region]);


            if(AllSumm<minAll)
                AllSumm=minAll;
            if(AllSumm>maxAll)
                AllSumm=maxAll;

            if(AllSumm!=Summ)
            {
                _this.reinitSlider(_this.data.summ);
                _this.printResultSlider(_this.data.summ,AllSumm);
            }

            var resAll=AllSumm/0.7;

            _this.calculation.client.summ=resAll;
            _this.printResultSlider(_this.data.amountCredit,resAll);

        }else{
            var cost=_this.varProgramm[programm].tab1.summ;
            var minAll=first+parseInt(cost.min[region]);
            var maxAll=first+parseInt(cost.max[region]);
            if(AllSumm<minAll)
                AllSumm=minAll;
            if(AllSumm>maxAll)
                AllSumm=maxAll;
            if(AllSumm!=Summ)
            {
                _this.reinitSlider(_this.data.summ);
                _this.printResultSlider(_this.data.summ,AllSumm);
            }
            var contribution=100*first/AllSumm;
            _this.calculation.client.firstrub=contribution;
            _this.printResultSlider(_this.data.contribution,contribution);
            var resAll=AllSumm-first;
            _this.calculation.client.summ=resAll;
            _this.printResultSlider(_this.data.amountCredit,resAll);
        }







        _this.getStartParam();
        _this.setFirstCalkRub();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();

    },

    ChekAmountSumm:function(){
        _this=this;
    },
    /****/
    extSetAmountSumm:function(all){
        _this=this;
        var resAll=all;
        var programm=$(_this.data.programm).val();
        var cost=_this.varProgramm[programm].tab1.contribution;

        var region=$(_this.data.region+':checked').val();
        var summ=parseInt($(_this.data.summ).val());

        if(_this.varProgramm[programm].pledge==true)
        {


            var minAll=_this.varProgramm[programm].tab1.summ.min;
            var maxAll=_this.varProgramm[programm].tab1.summ.max;

            if(resAll<minAll)
                resAll=minAll;
            if(resAll>maxAll)
                resAll=maxAll;


            var allSumm=resAll*0.7;
            _this.printResultSlider(_this.data.summ,allSumm);

            _this.calculation.client.summ=resAll;


            if(resAll!=all)
            {
                _this.reinitSlider(_this.data.amountCredit);
                _this.printResultSlider(_this.data.amountCredit,resAll);
            }


        }else{
            var minAll=summ*cost.min/100;
            var maxAll=summ*cost.max/100;
            if(resAll<minAll)
                resAll=minAll;
            if(resAll>maxAll)
                resAll=maxAll;

            var first=summ-resAll;
            var contribution=100*first/summ;

            _this.calculation.client.summ=resAll;
            _this.calculation.client.first=first;
            _this.calculation.client.firstrub=contribution;
            _this.printResultSlider(_this.data.contributionrub,first);
            _this.printResultSlider(_this.data.contribution,contribution);
            if(resAll!=all)
            {
                _this.reinitSlider(_this.data.amountCredit);
                _this.printResultSlider(_this.data.amountCredit,resAll);
            }


        }










        _this.getStartParam();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();
    },
    setAmountSumm:function(summ){
        _this=this;
        _this.extSetAmountSumm(summ);
    },
    setFirstSumm:function(first){
        _this=this;
        var summ=parseInt($(_this.data.summ).val());
        var contribution=100*first/summ;

        var all=summ-first;
        _this.printResultSlider(_this.data.contribution,contribution);
        _this.printResultSlider(_this.data.amountCredit,all);
        _this.calculation.client.summ=all;
        _this.calculation.client.first=first;
        _this.calculation.client.firstrub=contribution;
        _this.initParam.contribution=contribution;

       // _this.wrCalculations();
        _this.getStartParam();
        //_this.setFirstCalk();
        //_this.setFirstCalkRub();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();
    },
    /**getStartParam**/
    getStartParam:function(){
        _this=this;
        var programm=$(_this.data.programm).val();
        var region=$(_this.data.region+':checked').val();
        var summ=$(_this.data.summ).val();
        var contribution=parseInt($(_this.data.contribution).val());
        var year=$(_this.data.year).val();
        _this.initParam.term=year*12;

        //_this.initParam.first=contribution*summ/100;

        _this.initParam.first=parseInt($(_this.data.contributionrub).val());
        contribution=Math.round10(_this.initParam.first*100/summ);
        _this.printResultSlider(_this.data.contribution,contribution,true);

        var contribution1=parseInt($(_this.data.contribution).val());
        if(contribution1!=contribution)
        {
            contribution=contribution1;
            _this.initParam.first=contribution*summ/100;
        }

        var programm=$(_this.data.programm).val();
        if(_this.varProgramm[programm].pledge)
        {
            _this.initParam.summ=summ;
            contribution=0;
            _this.initParam.first=0;
        }else{
            _this.initParam.summ=summ-_this.initParam.first;
        }

        _this.initParam.contribution=contribution;
        _this.percentCalculations(programm,year,contribution);
    },
    /**percentCalculations**/
    percentCalculations:function(programm,year,contribution){
        _this=this;
        var percent=0;
        $.each(_this.varProgramm[programm].tab2,function(key,value){
            var yearMin=value.yearMin,yearMax=value.yearMax,
                feeMax=value.feeMax, feeMin=value.feeMin,
                less50=value.less50, more50=value.more50,
                less=value.less;
            if(typeof(yearMin)!='undefined' && typeof(yearMax)!='undefined')
            {
                if(year>=yearMin && year<=yearMax)
                {
                    if(typeof(less)!='undefined')
                    {
                        percent=less;
                    }else if(typeof(less50)!='undefined' && typeof(more50)!='undefined'){
                        if(contribution<50)
                            percent=less50;
                        else if(contribution>=50)
                            percent=more50;
                    }
                }
            }else if(typeof(feeMin)!='undefined' && typeof(feeMax)!='undefined'){
                if(contribution>=feeMin && contribution<=feeMax)
                {
                    if(typeof(less)!='undefined')
                        percent=less;
                }
            }
        });
        if(percent==0)
            percent=3.5;
        if(_this.initParam.client)
            percent=percent-0.5;
        _this.initParam.percent=percent;
    },
    /**interimCalculations**/
    interimCalculations:function(){
        _this=this;
        var income=0;
        if(_this.initParam.proofIncome=='accordingBank'){
            if(typeof(_this.initParam.incomeAccordingBank.borrower)!='undefined')
                income=income+_this.initParam.incomeAccordingBank.borrower;
            if(typeof(_this.initParam.incomeAccordingBank.coBorrowers)!='undefined')
                income=income+_this.initParam.incomeAccordingBank.coBorrowers;
        }else if(_this.initParam.proofIncome=='2NDFL'){
            $.each(_this.initParam.income2NDFL,function(key,value){
                if(typeof(value.borrower)!='undefined')
                    income=income+value.borrower;
                if(typeof(value.coBorrowers)!='undefined')
                    income=income+value.coBorrowers;
            });
        }
        income=Math.round10(income,-2);
        _this.calculation.income=income;
        var costs=0;
        costs=costs+_this.initParam.costs.livingWage*_this.initParam.costs.familyComposition;
        costs=costs+_this.initParam.costs.otherCredit;
        costs=costs+_this.initParam.costs.alimony;
        costs=costs+_this.initParam.costs.additionalCosts;
        costs=Math.round10(costs,-2);
        _this.calculation.costs=costs;
        var r5=0;
        if(_this.initParam.typeLoan=='mortgage')
            r5=1;
        switch (_this.initParam.currency){
            case "RUR":
                var t1=_this.initParam.pti*_this.calculation.income/100;
                var t2=_this.initParam.oti*_this.calculation.income/100-_this.calculation.costs;
                var t=t1;
                if(t1>t2)
                    t=t2;
                var summ=t*(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)))/(_this.initParam.percent/100/12);
                summ=Math.round10(summ,-2);
                _this.calculation.max.summ=summ;
                break;
            case "USD":
                var t1=_this.initParam.pti*_this.calculation.income/_this.initParam.course/100;
                var t2=_this.initParam.oti*_this.calculation.income/_this.initParam.course/100-_this.calculation.costs/_this.initParam.course;
                var t=t1;
                if(t1>t2)
                    t=t2;
                var summ=t*(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)))/(_this.initParam.percent/100/12);
                summ=Math.round10(summ,-2);
                _this.calculation.max.summ=summ;
                break;
            default:

                break;
        }
        var annuity=_this.calculation.max.summ*_this.initParam.percent/100/12/(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)));
        annuity=Math.round10(annuity,-2);
        _this.calculation.max.annuity=annuity;
        _this.calculation.max.term=_this.initParam.term;
        _this.calculation.max.currency=_this.initParam.currency;
        _this.calculation.max.percent=_this.initParam.percent;
        switch (_this.initParam.currency){
            case "RUR":
                var pti=_this.calculation.max.annuity/_this.calculation.income;
                pti=Math.round10(pti,-4)*100;
                _this.calculation.max.pti=pti;
                var oti=(_this.calculation.max.annuity+_this.calculation.costs)/_this.calculation.income;
                oti=Math.round10(oti,-4)*100;
                _this.calculation.max.oti=oti;
                break;
            case "USD":
                var pti=_this.calculation.max.annuity/(_this.calculation.income/_this.initParam.course);
                pti=Math.round10(pti,-4)*100;
                _this.calculation.max.pti=pti;
                var oti=(_this.calculation.max.annuity+_this.calculation.costs/_this.initParam.course)/(_this.calculation.income/_this.initParam.course);
                oti=Math.round10(oti,-4)*100;
                _this.calculation.max.oti=oti;
                break;
            default:
                break;
        }
        if(typeof(_this.calculation.client.summ)=="undefined")
            _this.calculation.client.summ=_this.initParam.summ;
        var annuityClient=_this.calculation.client.summ*_this.initParam.percent/100/12/(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)));
        annuityClient=Math.round10(annuityClient,-2);
        _this.calculation.client.annuity=annuityClient;

        _this.calculation.client.term=_this.initParam.term;
        _this.calculation.client.currency=_this.initParam.currency;
        _this.calculation.client.percent=_this.initParam.percent;
        if(typeof(_this.calculation.client.first)=="undefined")
            _this.calculation.client.first=_this.initParam.first;
        if(typeof(_this.calculation.client.firstrub)=="undefined")
            _this.calculation.client.firstrub=_this.initParam.contribution;
        switch (_this.initParam.currency){
            case "RUR":
                var pti=_this.calculation.client.annuity/_this.calculation.income;
                pti=Math.round10(pti,-4)*100;
                _this.calculation.client.pti=pti;
                var oti=(_this.calculation.client.annuity+_this.calculation.costs)/_this.calculation.income;
                oti=Math.round10(oti,-4)*100;
                _this.calculation.client.oti=oti;
                break;
            case "USD":
                var pti=_this.calculation.client.annuity/(_this.calculation.income/_this.initParam.course);
                pti=Math.round10(pti,-4)*100;
                _this.calculation.client.pti=pti;
                var oti=(_this.calculation.client.annuity+_this.calculation.costs/_this.initParam.course)/(_this.calculation.income/_this.initParam.course);
                oti=Math.round10(oti,-4)*100;
                _this.calculation.client.oti=oti;
                break;
            default:
                break;
        }
        _this.calculation.coefficient.pti.calculated=_this.calculation.client.pti;
        _this.calculation.coefficient.pti.max=_this.initParam.pti;
        _this.calculation.coefficient.oti.calculated=_this.calculation.client.oti;
        _this.calculation.coefficient.oti.max=_this.initParam.oti;
        switch (_this.initParam.currency){
            case "RUR":
                var coefficientMax=_this.calculation.coefficient.pti.max/100*_this.calculation.income*(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)))/(_this.initParam.percent/100/12);
                coefficientMax=Math.round10(coefficientMax,-2);
                _this.calculation.coefficient.pti.coefficientMax=coefficientMax;


                var coefficientMaxOti=(_this.calculation.coefficient.oti.max/100*_this.calculation.income-_this.calculation.costs)*(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)))/(_this.initParam.percent/100/12);
                coefficientMaxOti=Math.round10(coefficientMaxOti,-2);
                _this.calculation.coefficient.oti.coefficientMax=coefficientMaxOti;
                break;
            case "USD":
                var coefficientMax=_this.calculation.coefficient.pti.max/100*_this.calculation.income/_this.initParam.course*(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)))/(_this.initParam.percent/100/12);
                coefficientMax=Math.round10(coefficientMax,-2);
                _this.calculation.coefficient.pti.coefficientMax=coefficientMax;

                var coefficientMaxOti=(_this.calculation.coefficient.oti.max/100*_this.calculation.income/_this.initParam.course-_this.calculation.costs/_this.initParam.course)*(1-Math.pow((1+_this.initParam.percent/100/12),(r5-_this.initParam.term)))/(_this.initParam.percent/100/12);
                coefficientMaxOti=Math.round10(coefficientMaxOti,-2);
                _this.calculation.coefficient.oti.coefficientMax=coefficientMaxOti;
                break;
            default:
                break;
        }
    },
    /**printResult**/
    printResult:function() {
        _this = this;
        $(_this.print.monf).text($.number(_this.calculation.client.annuity, 0, ',', ' '));
        $(_this.print.summ).text($.number(_this.calculation.client.summ, 0, ',', ' '));
        $(_this.print.rate).text($.number(_this.calculation.client.percent, 2, ',', ' '));
        $(_this.print.first).text($.number(_this.calculation.client.first, 0, ',', ' '));
        $(_this.print.firstrub).text($.number(_this.calculation.client.firstrub, 0, ',', ' '));

        $(_this.data.monthlyPayment).val($.number(_this.calculation.client.annuity, 0, ',', ''));

        _this.printResultSlider(_this.data.amountCredit, _this.calculation.client.summ);
        _this.printResultSlider(_this.data.contributionrub, _this.calculation.client.first);

        var programm = $(_this.data.programm).val();
        if (_this.varProgramm[programm].pledge) {
            $(_this.data.pledgetext).text('Стоимость закладываемого объекта');
            $(_this.data.hidepledge).hide();
        } else {
            $(_this.data.pledgetext).text('Стоимость недвижимости');
            $(_this.data.hidepledge).show();
        }

        if (_this.varProgramm[programm].hideregion) {
            $(_this.data.hideregion).hide();
        }else{
            $(_this.data.hideregion).show();
        }

        $(_this.data.yearsklonenie).text(sklonenie(_this.initParam.term / 12, ['год', 'года', 'лет']));

    },
    printResultSlider:function(id,value,first){
        if(first)
            $(id).val($.number(value, 0,',',''));


        var slider=$("[data-target="+$(id).attr('id')+"]");

        if(slider.hasClass('card_to_card_select2'))
        {
            $(id).slider2("value",value);
            //$(id).val(value).change();
        }else{
            if(typeof(slider.data('conversion'))!='undefined')
                $(slider).slider("value",slider.data('conversion').toSlider(value));
            else
                $(slider).slider("value",value);
        }

        if(!first)
            $(id).val($.number(value, 0,',',''));
    },
    /**wrCalculations**/
    altwrCalculations:function(){

        _this.setPropCalculations();
        _this.getStartParam();

        _this.setFirstCalk();
        _this.setFirstCalkRub();

        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();
    },
    wrCalculations:function(){
        _this=this;
        //console.log(this.calculation);
        //_this.setPropCalculations();
        _this.getStartParam();

        //_this.setFirstCalk();
        _this.setFirstCalkRub();

        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();
    },

    /**initvarProgramm**/
    initvarProgramm:function(){
        _this=this;
        $.each(_this.varProgramm,function(key,value){
            if(value.tab1 && value.tab1.contribution && !value.tab1.cost)
            {
                if(value.tab1.contribution.min==0)
                {
                    value.tab1.cost={
                        min:{
                            moscow: parseInt(value.tab1.summ.min.moscow)*0.7,
                            region: parseInt(value.tab1.summ.min.region)*0.7
                        },
                        max:{
                            moscow: parseInt(value.tab1.summ.max.moscow)*0.7,
                            region: parseInt(value.tab1.summ.max.region)*0.7
                        }
                    };
                }else{
                    value.tab1.cost={
                        min:{
                            moscow: parseInt(value.tab1.contribution.min*value.tab1.summ.min.moscow/100)+parseInt(value.tab1.summ.min.moscow),
                            region: parseInt(value.tab1.contribution.min*value.tab1.summ.min.region/100)+parseInt(value.tab1.summ.min.region)
                        },
                        max:{
                            moscow: Math.floor10((Math.floor10(value.tab1.summ.max.moscow/(value.tab1.contribution.min/100), 0)/10000000),0)*10000000,
                            region: Math.floor10((Math.floor10(value.tab1.summ.max.region/(value.tab1.contribution.min/100), 0)/10000000),0)*10000000
                        }
                    };
                }
            }
            if(value.tab2)
            {
                $.each(value.tab2,function(tab2Key,tab2Value){
                    if(tab2Value.less50)
                        tab2Value.more50=tab2Value.less50-0.25;
                });
            }
            if(value.tab3)
            {
                for($i=0;$i<value.tab3.length;$i++)
                    if(($i+1)<value.tab3.length)
                        value.tab3[$i].maxPayment=value.tab3[$i+1].incomeMin*value.tab3[$i].pti/100;
            }
        });
    },
    /**initSlider**/
    getLabelSlider:function(num){
        if(num>=1000000)
            return $.number(num/1000000, 2, ',', ' ')+' МЛН';
        else if(num>=1000)
            return $.number(num/1000, 2, ',', ' ')+' ТЫС';
        else
            return $.number(num, 2, ',', ' ');
    },
    setPointer:function(val,this2,useamount,setfirstfumm){
        _this=this;
        if($(this2).data('target')=="first_calk")
            _this.setSummInput(val);
        if($(this2).data('target')=="main_ptop")
            _this.setMainInput(val);
        else if(useamount=="Y")
            _this.setAmountSumm(parseInt(val));
        else if(setfirstfumm=="Y")
            _this.setFirstSumm(parseInt(val));
        else
            _this.wrCalculations();
    },
    initSlider:function(){
        _this=this;
        $(".card_to_card_select").each(function(){
            var this2=this,
                min=$(this2).data('min'),
                max=$(this2).data('max'),
                target=$(this2).data('target'),
                value=$(this2).data('value'),
                suffix=$(this2).data('suffix'),
                del=$(this2).data('del'),
                elTarget=$('#'+target),
                useconversion=$(this2).data('useconversion'),
                conversion=false,
                textMin=min,
                textMax=max,
                textValue=value,
                useamount=$(this2).data('useamount'),
                setfirstfumm=$(this2).data('setfirstfumm'),
                stepdel=$(this2).data('stepdel'),
                stepdelh=$(this2).data('stepdelh')
                ;
            if(typeof(del)=="undefined")
            {
                del=1;
            }else{
                del=parseInt(del);
                if(del<=0)
                    del=1;
            }

            if(typeof(stepdel)=="undefined")
                stepdel=1;
            else{
                stepdel=parseInt(stepdel);
                if(stepdel<=0)
                    stepdel=1;
            }

            if(typeof(stepdelh)=="undefined")
                stepdelh=4;
            else{
                stepdelh=parseInt(stepdelh);
                if(stepdelh<=0)
                    stepdelh=4;
            }

            if(typeof(useconversion)!='undefined')
            {
                conversion=true;
                $(this2).data('conversion',
                    new $.conversion({
                    min:min,
                    max:max,
                    delta:1,
                    grid:_this.useconversion[useconversion]
                    })
                );
                textMin=$(this2).data('conversion').toSlider(min);
                textMax=$(this2).data('conversion').toSlider(max);
                textValue=$(this2).data('conversion').toSlider(value);
            }
            var steppips;
            if(stepdelh!=false)
                steppips=(textMax-textMin)/stepdelh;
            else
                steppips=false;

            //steppips=10;

            if(typeof(suffix)=="undefined")
                suffix="";
            $(this2).slider({
                range: "min",
                max: textMax,
                min: textMin,
                step: stepdel,
                value:textValue,
                create: function(){
                    if(conversion)
                        elTarget.val($(this2).data('conversion').fromSlider($(this).slider("value")));
                    else
                        elTarget.val($(this).slider("value"));
                },
                change: function( event, ui ) {
                    var val=ui.value;
                    if(conversion)
                        val=$(this2).data('conversion').fromSlider(val);
                    if(!elTarget.hasClass('changes'))
                    {

                            elTarget.val(val);

                    }else{

                    }
                    elTarget.removeClass('changes');
                },
                slide: function(event,ui){
                    //console.log(event);
                    //console.log(ui);
                    var val=ui.value;



                    if(conversion)
                        val=$(this2).data('conversion').fromSlider(val);
                    elTarget.val(val);

                    _this.setPointer(val,this2,useamount,setfirstfumm);

                }
            }).slider("pips",{
                step:steppips,
                suffix: suffix,
                formatLabel: function(value) {
                    var val=value;
                    if(conversion)
                        val=$(this2).data('conversion').fromSlider(val);
                    var valtxt=val/del;
                    valtxt=_this.number(valtxt);
                    return this.prefix + valtxt + this.suffix;
                },
                rest: "label"
            });

            elTarget.number(true,0,',',' ');
            elTarget.on('change',function(){
                var val=$(this).val();
                $(this).addClass('changes');
                if(conversion)
                    val=$(this2).data('conversion').toSlider(val);
                $(this2).slider("value",val);
                _this.setPointer(val,this2,useamount,setfirstfumm);
                //$(this2).val($(this).val());
            });
        });
        $(".card_to_card_select2").each(function(){
            var this2=this,
                conversion=false,
                useconversion=$(this2).data('useconversion'),
                min=$(this2).data('min'),
                max=$(this2).data('max'),
                value=$(this2).data('value'),
                target=$(this2).data('target'),
                elTarget=$('#'+target),
                useamount=$(this2).data('useamount'),
                setfirstfumm=$(this2).data('setfirstfumm'),
                stepdel=$(this2).data('stepdel')
                ;

            var heterogeneity=false;
            var stepScale=4;
            var scale=[];
            var step=100000;
            scale[scale.length]=_this.getLabelSlider(min);
            for(var i=min+(max-min)/stepScale;i<max;i+=(max-min)/stepScale)
                scale[scale.length]=_this.getLabelSlider(i);
            scale[scale.length]=_this.getLabelSlider(max);

            if(typeof(useconversion)!='undefined')
            {
                conversion=true;
                $(this2).data('conversion',
                    new $.conversion({
                        min:min,
                        max:max,
                        delta:1,
                        grid:_this.useconversion[useconversion]
                    })
                );

                scale=[];
                scale[scale.length]=_this.getLabelSlider($(this2).data('conversion').fromSlider(0));
                for(var i=100/stepScale;i<100;i+=100/stepScale)
                    scale[scale.length]=_this.getLabelSlider($(this2).data('conversion').fromSlider(i));
                scale[scale.length]=_this.getLabelSlider($(this2).data('conversion').fromSlider(100));

                heterogeneity=[];
                jQuery.each(_this.useconversion[useconversion],function(index,value){
                    heterogeneity[heterogeneity.length]=value.percent+'/'+value.val;
                });
            }
            var BigVal=$(elTarget).val();
            $(elTarget).slider2({
                from: min,
                to: max,
                heterogeneity:heterogeneity,
                scale: scale,
                limits: false,
                step: step,
                //round:1000,
                onStateSlide: function(){
                    var val=$(elTarget).val();
                    if(BigVal!=val)
                    {
                        BigVal=val;
                        //console.log(val);
                        _this.setPointer(val,this2,useamount,setfirstfumm);
                    }
                }
            });
            elTarget.number(true,0,',',' ');
            elTarget.on('change',function(){
                var val=$(this).val();
                $(elTarget).slider2("value", val);
                BigVal=val;
                _this.setPointer(val,this2,useamount,setfirstfumm);
            });
        });
    },
    /**number**/
    number:function(num){
        if(isInteger(num))
            return $.number(num, 0,',',' ');
        else
            return $.number(num, 2,',',' ');
    },
    /**init**/
    init:function(varProgramm,reInitParam){
        _this=this;
        var programm=$(_this.data.programm).val();
        $(_this.data.firstprecent).hide();

        _this.varProgramm=varProgramm;

        if(reInitParam)
            _this.initParam = $.extend(_this.initParam,reInitParam);

        _this.initvarProgramm();
        _this.initSlider();


        _this.setPropCalculations();
        _this.wrCalculations();
        _this.initCallbackForm();


        var first=parseInt($(_this.data.contributionrub).val());
        var summ=parseInt($(_this.data.summ).val());
        var contribution=100*first/summ;

        var all=summ-first;
        if(_this.varProgramm[programm].pledge==true)
            all=summ/0.7;
        _this.printResultSlider(_this.data.contribution,contribution);
        _this.printResultSlider(_this.data.amountCredit,all);
        _this.calculation.client.summ=all;
        _this.getStartParam();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();

    }
}