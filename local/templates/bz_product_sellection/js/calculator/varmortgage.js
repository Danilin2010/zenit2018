var varProgramm={
    new:{
        tab1:{
            contribution:{
                min:20,
                max:80
            },
            summ:{
                min:{
                    moscow: '600000',
                    region: '300000'
                },
                max:{
                    moscow: '16000000',
                    region: '12000000'
                }
            }
        },
        tab2:[
            {
                yearMin:1,
                yearMax:3,
                less50:13.5//,
                //more50:13.25
            },
            {
                yearMin:4,
                yearMax:10,
                less50:13.75//,
                //more50:13.5
            },
            {
                yearMin:11,
                yearMax:15,
                less50:14//,
                //more50:13.75
            },
            {
                yearMin:16,
                yearMax:20,
                less50:14.25,
                more50:14
            },
            {
                yearMin:21,
                yearMax:30,
                less50:14.5//,
                //more50:14.25
            }
        ],
        tab3:[
            {
                incomeMax:60000,
                pti:40
            },
            {
                incomeMin:60000,
                incomeMax:120000,
                pti:50
            },
            {
                incomeMin:120000,
                pti:60
            }
        ]
    },
    apartment:{
        tab1:{
            contribution:{
                min:15,
                max:85
            },
            summ:{
                min:{
                    moscow: '600000',
                    region: '300000'
                },
                max:{
                    moscow: '16000000',
                    region: '12000000'
                }
            }
        },
        tab2:[
            {
                yearMin:1,
                yearMax:3,
                less50:13
            },
            {
                yearMin:4,
                yearMax:10,
                less50:13.25
            },
            {
                yearMin:11,
                yearMax:15,
                less50:13.5
            },
            {
                yearMin:16,
                yearMax:20,
                less50:13.75
            },
            {
                yearMin:21,
                yearMax:30,
                less50:14
            }
        ],
        tab3:[
            {
                incomeMax:60000,
                pti:40
            },
            {
                incomeMin:60000,
                incomeMax:120000,
                pti:50
            },
            {
                incomeMin:120000,
                pti:60
            }
        ]
    },
    room:{
        tab1:{
            contribution:{
                min:20,
                max:80
            },
            summ:{
                min:{
                    moscow: '800000',
                    region: '300000'
                },
                max:{
                    moscow: '14000000',
                    region: '10500000'
                }
            }
        },
        tab2:[
            {
                yearMin:1,
                yearMax:3,
                less50:13.5
            },
            {
                yearMin:4,
                yearMax:10,
                less50:13.75
            },
            {
                yearMin:11,
                yearMax:15,
                less50:14
            },
            {
                yearMin:16,
                yearMax:20,
                less50:14.25
            },
            {
                yearMin:21,
                yearMax:25,
                less50:14.5
            }
        ],
        tab3:[
            {
                incomeMax:60000,
                pti:40
            },
            {
                incomeMin:60000,
                incomeMax:120000,
                pti:50
            },
            {
                incomeMin:120000,
                pti:60
            }
        ]
    },
    house:{
        tab1:{
            contribution:{
                min:30,
                max:70
            },
            summ:{
                min:{
                    moscow: '600000',
                    region: '300000'
                },
                max:{
                    moscow: '16000000',
                    region: '12000000'
                }
            }
        },
        tab2:[
            {
                yearMin:1,
                yearMax:3,
                less50:14.5
            },
            {
                yearMin:4,
                yearMax:10,
                less50:14.75
            },
            {
                yearMin:11,
                yearMax:15,
                less50:15
            },
            {
                yearMin:16,
                yearMax:20,
                less50:15.25
            },
            {
                yearMin:21,
                yearMax:25,
                less50:15.5
            }
        ],
        tab3:[
            {
                incomeMax:60000,
                pti:40
            },
            {
                incomeMin:60000,
                incomeMax:120000,
                pti:50
            },
            {
                incomeMin:120000,
                pti:60
            }
        ]
    },
    special:{
        hideregion:true,
        tab1:{
            contribution:{
                min:10,
                max:90
            },
            summ:{
                min:{
                    moscow: '600000',
                    region: '300000'
                },
                max:{
                    moscow: '30000000',
                    region: '12000000'
                }
            }
        },
        tab2:[
            {
                yearMin:1,
                yearMax:30,
                less:10.5
            }
        ],
        tab3:[
            {
                incomeMax:60000,
                pti:40
            },
            {
                incomeMin:60000,
                incomeMax:120000,
                pti:50
            },
            {
                incomeMin:120000,
                pti:60
            }
        ]
    },
    ahml:{
        tab1:{
            contribution:{
                min:20,
                max:85
            },
            summ:{
                min:{
                    moscow: '300000',
                    region: '300000'
                },
                max:{
                    moscow: '20000000',
                    region: '10000000'
                }
            }
        },
        tab2:[
            {
                feeMin:20,
                feeMax:30,
                less:11
            },
            {
                feeMin:30,
                feeMax:50,
                less:10.75
            },
            {
                feeMin:50,
                feeMax:85,
                less:10.50
            }
        ]
    },
    pledge:{
        pledge:true,
        tab1:{
            cost:{
                min:{
                    moscow: '770000',
                    region: '385000'
                },
                max:{
                    moscow: '20000000',
                    region: '15000000'
                }
            },
            summ:{
                min:{
                    moscow: '540000',
                    region: '270000'
                },
                max:{
                    moscow: '14000000',
                    region: '10500000'
                }
            }
        },
        tab2:[
            {
                yearMin:1,
                yearMax:1,
                less:15.5
            },
            {
                yearMin:2,
                yearMax:3,
                less:16
            },
            {
                yearMin:4,
                yearMax:5,
                less:16.5
            },
            {
                yearMin:6,
                yearMax:7,
                less:17
            },
            {
                yearMin:8,
                yearMax:15,
                less:17.5
            }
        ],
        tab3:[
            {
                incomeMax:60000,
                pti:40
            },
            {
                incomeMin:60000,
                incomeMax:120000,
                pti:50
            },
            {
                incomeMin:120000,
                pti:60
            }
        ]
    },




    financedbank:{
        hideregion:true,
        tab1:{
            contribution:{
                min:20,
                max:80
            },
            cost:{
                min:{
                    moscow: '375000',
                    region: '375000'
                },
                max:{
                    moscow: '225000000',
                    region: '225000000'
                }
            },
            summ:{
                min:{
                    moscow: '300000',
                    region: '300000'
                },
                max:{
                    moscow: '45000000',
                    region: '45000000'
                }
            }
        },
        tab2:[
            {
                feeMin:20,
                feeMax:29,
                less:14.5
            },
            {
                feeMin:30,
                feeMax:59,
                less:14
            },
            {
                feeMin:50,
                feeMax:80,
                less:13.50
            }
        ]
    }
}


$(document).ready(function(){
    CreditCalculator.init(varProgramm);
});