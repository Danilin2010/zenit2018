var AutoCalculator={
    varProgramm:{},
    calculation:{max:{},client:{},coefficient:{pti:{},oti:{}}},
    data:{
        withmileage:'[data-withmileage]',
        kasko:'[data-kasko]',
        programm:'[data-programm]',
        region:'[data-region]',
        summ:'[data-summ]',
        contribution:'[data-contribution]',
        contributionrub:'[data-contributionrub]',
        year:'[data-year]',
        togglefirst:'[data-toggle-first]',
        firstprecent:'[data-first-precent]',
        amountCredit:'[data-amount-credit]',
        firstrub:'[data-first-rub]'
    },
    print:{
        monf:'[data-print-monf]',
        summ:'[data-print-summ]',
        rate:'[data-print-rate]',
        first:'[data-print-first]',
        firstrub:'[data-print-first-rub]'
    },
    initParam:{

    },

    useconversion:{
        cost_real_estate:[
            {val:2000000,percent:33.33333333333333},
            {val:5000000,percent:66.66666666666667}
        ],
        amount_credit:[
            {val:1000000,percent:33.33333333333333},
            {val:2000000,percent:66.66666666666667}
        ]
    },

    /**initCallbackForm**/
    initCallbackForm:function(){
        _this=this;
        var change=[
            _this.data.withmileage,
            _this.data.kasko,
            _this.data.programm,
            _this.data.region,
            _this.data.summ,
            //_this.data.contribution,
            _this.data.year
        ];

        $(_this.data.contribution).on('change',function(e){
            _this.setSummInput(parseInt($(this).val()));
            _this.wrCalculations();
        });

        $(change.join(', ')).on('change',function(e){
            _this.wrCalculations();
        });

        $(_this.data.amountCredit).on('change',function(e){
            _this.setAmountSumm(parseInt($(this).val()));
        });

        $(_this.data.contributionrub).on('change',function(e){
            //_this.setFirstSumm(parseInt($(this).val()));
        });

        $(_this.data.togglefirst).on('toggle', function(e, active) {
            if(active){
                $(_this.data.firstprecent).hide();
                $(_this.data.firstrub).show();
            }else{
                $(_this.data.firstprecent).show();
                $(_this.data.firstrub).hide();
            }
        });

    },

    printResultSlider:function(id,value,first){


        var val=value;

        var slider=$("[data-target="+$(id).attr('id')+"]");
        if(typeof(slider.data('conversion'))!='undefined')
            value=slider.data('conversion').toSlider(value);

        var max=slider.slider("option",'max');
        var min=slider.slider("option",'min');

        if(value<min)
        {
            value=min;
            val=value;
            if(typeof(slider.data('conversion'))!='undefined')
                val=slider.data('conversion').fromSlider(val);
        }

        if(value>max)
        {
            value=max;
            val=value;
            if(typeof(slider.data('conversion'))!='undefined')
                val=slider.data('conversion').fromSlider(val);
        }

        if(first)
            $(id).val($.number(val, 0,',',''));


        //$(id).val($.number(val, 0,',',''));
        $(slider).slider("value",value);

        if(!first)
            $(id).val($.number(val, 0,',',''));
    },

    /****/
    setSummInput:function(contribution){
        var summ=$(_this.data.summ).val();
        var first=contribution*summ/100;
        _this.printResultSlider(_this.data.contributionrub,first);
        //_this.printResultSlider(_this.data.contribution,firstrub);
    },

    /**initvarProgramm**/
    initvarProgramm:function(){
        _this=this;

        var withmileage=$(_this.data.withmileage).prop('checked');

        if(withmileage)
        {
            _this.varProgramm.commercial.tab1.summ.min.moscow=90000;
            _this.varProgramm.commercial.tab1.summ.min.region=90000;
            _this.varProgramm.commercial.tab1.contribution.min=40;
        }else{
            _this.varProgramm.commercial.tab1.summ.min.moscow=100000;
            _this.varProgramm.commercial.tab1.summ.min.region=100000;
            _this.varProgramm.commercial.tab1.contribution.min=30;
        }

        $.each(_this.varProgramm,function(key,value){
            if(value.tab1 && value.tab1.contribution)
            {
                value.tab1.cost={

                    min:{
                        moscow: parseInt(Math.round10(value.tab1.summ.min.moscow*(1+value.tab1.contribution.min/100))),
                        region: parseInt(Math.round10(value.tab1.summ.min.region*(1+value.tab1.contribution.min/100)))
                    },
                    max:{
                        moscow: parseInt(Math.round10(value.tab1.summ.max.moscow/(value.tab1.contribution.min/100),6)),
                        region: parseInt(Math.round10(value.tab1.summ.max.region/(value.tab1.contribution.min/100),6))
                    }
                };

            }

            if(value.tab3)
            {
                for($i=0;$i<value.tab3.length;$i++)
                    if(($i+1)<value.tab3.length)
                        value.tab3[$i].maxPayment=value.tab3[$i+1].incomeMin*value.tab3[$i].pti/100;
            }
        });
    },

    setFirstSumm:function(first){
        _this=this;
        var summ=$(_this.data.summ).val();
        var contribution=100*first/summ;
        _this.printResultSlider(_this.data.contribution,contribution);
        _this.wrCalculations();
    },

    /**initSlider**/
    initSlider:function(){
        _this=this;
        $(".card_to_card_select").each(function(){
            var this2=this,
                min=$(this2).data('min'),
                max=$(this2).data('max'),
                target=$(this2).data('target'),
                value=$(this2).data('value'),
                suffix=$(this2).data('suffix'),
                del=$(this2).data('del'),
                elTarget=$('#'+target),
                useconversion=$(this2).data('useconversion'),
                conversion=false,
                textMin=min,
                textMax=max,
                textValue=value,
                useamount=$(this2).data('useamount'),
                setfirstfumm=$(this2).data('setfirstfumm'),
                stepdel=$(this2).data('stepdel'),
                stepdelh=$(this2).data('stepdelh')
                ;
            if(typeof(del)=="undefined")
            {
                del=1;
            }else{
                del=parseInt(del);
                if(del<=0)
                    del=1;
            }

            if(typeof(stepdel)=="undefined")
                stepdel=1;
            else{
                stepdel=parseInt(stepdel);
                if(stepdel<=0)
                    stepdel=1;
            }

            if(typeof(stepdelh)=="undefined")
                stepdelh=2;
            else{
                stepdelh=parseInt(stepdelh);
                if(stepdelh<=0)
                    stepdelh=2;
            }

            if(typeof(useconversion)!='undefined')
            {
                conversion=true;
                $(this2).data('conversion',
                    new $.conversion({
                        min:min,
                        max:max,
                        delta:1,
                        grid:_this.useconversion[useconversion]
                    })
                );
                textMin=$(this2).data('conversion').toSlider(min);
                textMax=$(this2).data('conversion').toSlider(max);
                textValue=$(this2).data('conversion').toSlider(value);
            }
            var steppips;
            if(stepdelh!=false)
                steppips=(textMax-textMin)/stepdelh;
            else
                steppips=false;

            if(typeof(suffix)=="undefined")
                suffix="";

            /*
            if(typeof(useconversion)!='undefined')
            {
                if(stepdel==1)
                {
                    stepdel=(textMax-textMin)/100;
                }
            }
            */

            $(this2).slider({
                range: "min",
                max: textMax,
                min: textMin,
                step: stepdel,
                value:textValue,
                create: function(){
                    if(conversion)
                        elTarget.val($(this2).data('conversion').fromSlider($(this).slider("value")));
                    else
                        elTarget.val($(this).slider("value"));
                },
                change: function( event, ui ) {
                    /********/
                    var val=ui.value;
                    if(conversion)
                        val=$(this2).data('conversion').fromSlider(val);
                    if(!elTarget.hasClass('changes'))
                        elTarget.val(val);
                    elTarget.removeClass('changes');

                    /*var val=ui.value;
                    var max=$(this2).slider("option",'max');
                    var min=$(this2).slider("option",'min');
                    if(conversion)
                    {
                        val=$(this2).data('conversion').fromSlider(val);
                        min=$(this2).data('conversion').fromSlider(min);
                        max=$(this2).data('conversion').fromSlider(max);
                    }
                    if(val>max || val<min)
                        elTarget.val(val);*/
                },
                slide: function(event,ui){
                    var val=ui.value;
                    if(conversion)
                        val=$(this2).data('conversion').fromSlider(val);
                    elTarget.val(val);

                    if($(this2).data('target')=="first_calk")
                        _this.setSummInput(val);

                    if(useamount=="Y")
                        _this.setAmountSumm(parseInt(val));
                    else if(setfirstfumm=="Y")
                        _this.setFirstSumm(parseInt(val));
                    else
                        _this.wrCalculations();
                }
            }).slider("pips",{
                step:steppips,
                suffix: suffix,
                formatLabel: function(value) {
                    var val=value;
                    if(conversion)
                        val=$(this2).data('conversion').fromSlider(val);
                    var valtxt=val/del;
                    valtxt=_this.number(valtxt);
                    return this.prefix + valtxt + this.suffix;
                },
                rest: "label"
            });
            elTarget.number(true,0,',',' ');
            elTarget.on('change',function(){
                var val=$(this).val();
                if(conversion)
                    val=$(this2).data('conversion').toSlider(val);
                $(this).addClass('changes');
                $(this2).slider("value",val);
            });
        });
    },
    setAmountSumm:function(summ){
        _this=this;

        var dataSumm=parseInt($(this.data.summ).val());
        var programm=$(_this.data.programm).val();
        var cost=_this.varProgramm[programm].tab1.summ;
        var region=$(_this.data.region+':checked').val();
        var firstrub;
        if(summ>cost.max[region])
        {
            summ=cost.max[region];
            $(_this.data.amountCredit).val(summ);
        }
        if(summ<cost.min[region])
        {
            summ=cost.min[region];
            $(_this.data.amountCredit).val(summ);
        }

        //console.log(_this);
        //_this.calculation.client.first;
        _this.calculation.client.summ=parseInt(summ);

        _this.calculation.client.annuity=
            _this.calculation.client.summ*
            (_this.calculation.client.percent/100/12)
            /(1-Math.pow(1+_this.calculation.client.percent/100/12,-_this.calculation.client.term));
        //if(_this)
        _this.calculation.client.firstrub=100*(_this.calculation.client.first/(_this.calculation.client.first+_this.calculation.client.summ));
        _this.calculation.client.firstrub=Math.round10(_this.calculation.client.firstrub);
        _this.initParam.contribution=_this.calculation.client.firstrub;
        _this.printResultSlider(_this.data.contribution,_this.calculation.client.firstrub,true);


        var contribution1=parseInt($(_this.data.contribution).val());
        if(contribution1!=_this.calculation.client.firstrub)
        {
            firstrub=contribution1;
            _this.calculation.client.firstrub=firstrub;
            _this.initParam.contribution=_this.calculation.client.firstrub;
            _this.calculation.client.first=_this.calculation.client.firstrub*dataSumm/100;
        }else{

        }
        var nesSumm=_this.calculation.client.first+_this.calculation.client.summ;
        _this.printResultSlider(_this.data.summ,nesSumm);
        $(this.data.summ).val(_this.calculation.client.first+_this.calculation.client.summ);


        _this.printResult();
    },
    /**number**/
    number:function(num){
        if(isInteger(num))
            return $.number(num, 0,',',' ');
        else
            return $.number(num, 2,',',' ');
    },
    /**setSliderMinMax**/
    setSliderMinMax:function(element,max,min){
        max=parseInt(max);min=parseInt(min);
        var id=$(element).attr('id');
        var slider=$("[data-target="+id+"]");
        var val=$(element).val();
        var textVal=val;
        var textMax=max;
        var textMin=min;

        var stepdel=$(slider).data('stepdel');
        if(typeof(stepdel)=="undefined")
            stepdel=1;
        else{
            stepdel=parseInt(stepdel);
            if(stepdel<=0)
                stepdel=1;
        }

        var stepdelh=$(slider).data('stepdelh');
        if(typeof(stepdelh)=="undefined")
            stepdelh=2;
        else{
            stepdelh=parseInt(stepdelh);
            if(stepdelh<=0)
                stepdelh=2;
        }

        var stepdelmin=$(slider).data('stepdelmin');
        var maxmin=$(slider).data('maxmin');

        var useconversion=$(slider).data('useconversion');
        var useconversionmin=$(slider).data('useconversionmin');

        if(typeof(slider.data('conversion'))!='undefined')
        {
            var prop={
                max:max,
                min:min
            };
            if(maxmin>=max)
            {
                prop.grid=_this.useconversion[useconversionmin];
            }else{
                prop.grid=_this.useconversion[useconversion];
            }
            var conversion=slider.data('conversion');
            conversion.reInit(prop);
            textMin=conversion.toSlider(textMin);
            textMax=conversion.toSlider(textMax);
            textVal=conversion.toSlider(textVal);
            slider.data('conversion',conversion);
        }
        var steppips;
        if(stepdelh!=false)
            steppips=(textMax-textMin)/stepdelh;
        else
            steppips=false;
        if(maxmin>=max)
        {
            stepdel=stepdelmin;
        }
        slider
            .slider("option","max",textMax)
            .slider("option","min",textMin)
            .slider("option","step",stepdel)
            .slider("pips","options",'step',steppips)
            .slider("pips", "refresh")
        ;
        if(val>max)
        {
            $(element).val(max);
            $(element).addClass('changes');
            $(slider).slider("value",textMax);
        }else if(val<min)
        {
            $(element).val(min);
            $(element).addClass('changes');
            $(slider).slider("value",textMin);
        }else{
            $(element).addClass('changes');
            $(slider).slider("value",textVal);
        }
    },
    /**setPropCalculations**/
    setPropCalculations:function(){
        _this=this;
        var programm=$(_this.data.programm).val();
        var region=$(_this.data.region+':checked').val();

        var kasko=$(_this.data.kasko).prop('checked');
        _this.initParam.kasko=kasko;
        var withmileage=$(_this.data.withmileage).prop('checked');
        _this.initParam.withmileage=withmileage;

        _this.setYearCredit(programm);
        _this.setSumm(programm,region);
        _this.setFirstCalk(programm);
        //_this.setFirstCalkRub(programm,region);
    },
    setFirstCalkRub:function(){
        var summ=$(_this.data.summ).val();
        var programm=$(_this.data.programm).val();
        _this=this;
        var min=0,max=0;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            min=_this.varProgramm[programm].tab1.contribution.min;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            max=_this.varProgramm[programm].tab1.contribution.max;
        if(min==0)
            min=10;
        if(max==0)
            max=80;

        _this.setSliderMinMax(_this.data.contributionrub,(summ/100)*max,(summ/100)*min);
    },
    /**setSumm**/
    setSumm:function(programm,region){
        _this=this;
        var min=0,max=0;
        if(typeof(_this.varProgramm[programm].tab1.cost.min[region])!='undefined')
            min=_this.varProgramm[programm].tab1.cost.min[region];
        if(typeof(_this.varProgramm[programm].tab1.cost.max[region])!='undefined')
            max=_this.varProgramm[programm].tab1.cost.max[region];

        _this.setSliderMinMax(_this.data.summ,max,min);


        var creditMin=0,creditMax=0;
        if(typeof(_this.varProgramm[programm].tab1.summ.min[region])!='undefined')
            creditMin=_this.varProgramm[programm].tab1.summ.min[region];
        if(typeof(_this.varProgramm[programm].tab1.summ.max[region])!='undefined')
            creditMax=_this.varProgramm[programm].tab1.summ.max[region];
        _this.setSliderMinMax(_this.data.amountCredit,creditMax,creditMin);


    },
    /**setFirstCalk**/
    setFirstCalk:function(programm){
        _this=this;
        var min=0,max=0;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            min=_this.varProgramm[programm].tab1.contribution.min;
        if(typeof(_this.varProgramm[programm].tab1.contribution)!='undefined')
            max=_this.varProgramm[programm].tab1.contribution.max;
        if(min==0)
            min=10;
        if(max==0)
            max=80;
        _this.setSliderMinMax(_this.data.contribution,max,min);
    },
    /**setYearCredit**/
    setYearCredit:function(programm){
        _this=this;
        var min=0,max=0;
        $.each(_this.varProgramm[programm].tab2,function(key,value){
            if(typeof(value.yearMin)!='undefined' && (value.yearMin<min || min==0))
                min=value.yearMin;
            if(typeof(value.yearMax)!='undefined' && (value.yearMax>max || max==0))
                max=value.yearMax;
        });
        if(min==0)
            min=1;
        if(max==0)
            max=30;
        _this.setSliderMinMax(_this.data.year,max*12,min*12);
    },


    ChekAmountSumm:function() {
        _this=this;
        var summ=parseInt($(_this.data.amountCredit).val());
        var programm=$(_this.data.programm).val();
        var cost=_this.varProgramm[programm].tab1.summ;
        var region=$(_this.data.region+':checked').val();
        if(summ>parseInt(cost.max[region]))
        {
            _this.setAmountSumm(parseInt(cost.max[region]));
            var dataSumm=parseInt($(this.data.summ).val());
            _this.calculation.client.first=_this.calculation.client.firstrub*dataSumm/100;
            //_this.printResultSlider(_this.data.summ,contribution);
        }
        if(summ<parseInt(cost.min[region]))
        {
            _this.setAmountSumm(parseInt(cost.min[region]));
            var dataSumm=parseInt($(this.data.summ).val());
            _this.calculation.client.first=_this.calculation.client.firstrub*dataSumm/100;
        }




        _this.printResult();

    },

    /**getStartParam**/
    getStartParam:function(){
        _this=this;
        var programm=$(_this.data.programm).val();
        var region=$(_this.data.region+':checked').val();
        var summ=parseInt($(_this.data.summ).val());
        if(_this.initParam.kasko)
            summ=summ*1.08;
        //var contribution=parseInt($(_this.data.contribution).val());
        var year=parseInt($(_this.data.year).val())/12;

        _this.initParam.term=year;


        //_this.initParam.first=contribution*summ/100;

        _this.initParam.first=parseInt($(_this.data.contributionrub).val());
        var contribution=Math.round10(_this.initParam.first*100/summ);
        _this.printResultSlider(_this.data.contribution,contribution);

        var contribution1=parseInt($(_this.data.contribution).val());
        if(contribution1!=contribution)
        {
            contribution=contribution1;
            _this.initParam.first=contribution*summ/100;
        }



        _this.initParam.contribution=contribution;
        _this.initParam.summ=summ-_this.initParam.first;
        _this.percentCalculations(programm,year,contribution);









    },
    /**percentCalculations**/
    percentCalculations:function(programm,year,contribution){
        _this=this;
        var percent=0;
        $.each(_this.varProgramm[programm].tab2,function(key,value){
            var yearMin=value.yearMin,yearMax=value.yearMax,
                feeMax=value.feeMax, feeMin=value.feeMin,
                less50=value.less50, more50=value.more50,
                less=value.less;
            if(typeof(yearMin)!='undefined' && typeof(yearMax)!='undefined')
            {
                if(year>=yearMin && year<=yearMax)
                {
                    if(typeof(less)!='undefined')
                    {
                        percent=less;
                    }else if(typeof(less50)!='undefined' && typeof(more50)!='undefined'){
                        if(contribution<50)
                            percent=less50;
                        else if(contribution>=50)
                            percent=more50;
                    }
                }
            }else if(typeof(feeMin)!='undefined' && typeof(feeMax)!='undefined'){
                if(contribution>=feeMin && contribution<=feeMax)
                {
                    if(typeof(less)!='undefined')
                        percent=less;
                }
            }
        });
        if(percent==0)
            percent=3.5;
        _this.initParam.percent=percent;
    },



    /**interimCalculations**/
    interimCalculations:function(){
        _this=this;
        _this.calculation.client={};
        _this.calculation.client.summ=_this.initParam.summ;

        //if(_this.initParam.kasko)
        //    _this.calculation.client.summ=_this.calculation.client.summ*1.08;

        _this.calculation.client.percent=_this.initParam.percent;
        _this.calculation.client.first=_this.initParam.first;
        _this.calculation.client.contribution=parseInt(_this.initParam.contribution);
        _this.calculation.client.term=parseInt(_this.initParam.term);
        _this.calculation.client.annuity=
            _this.calculation.client.summ*_this.calculation.client.percent/100/12/
            (
                1-Math.pow(1+_this.calculation.client.percent/100/12,-_this.calculation.client.term*12)
            );

        _this.calculation.client.firstrub= _this.calculation.client.contribution;
    },


    /**printResult**/
    printResult:function(){
        _this=this;
        $(_this.print.summ).text($.number(_this.calculation.client.summ, 0,',',' '));
        $(_this.print.monf).text($.number(_this.calculation.client.annuity, 0,',',' '));
        $(_this.print.rate).text($.number(_this.calculation.client.percent, 2,',',' '));
        $(_this.print.first).text($.number(_this.calculation.client.first, 0,',',' '));
        $(_this.print.firstrub).text($.number(_this.calculation.client.firstrub, 0,',',' '));


        _this.printResultSlider(_this.data.contributionrub,_this.calculation.client.first);

        _this.printResultSlider(_this.data.amountCredit,_this.calculation.client.summ);


        //$(_this.data.yearsklonenie).text(sklonenie(_this.initParam.term/12,['год','года','лет']));

    },

    /**wrCalculations**/
    wrCalculations:function(){
        _this=this;
        _this.initvarProgramm();
        _this.setPropCalculations();
        _this.getStartParam();
        _this.setFirstCalkRub();
        _this.interimCalculations();
        _this.printResult();
        _this.ChekAmountSumm();
    },

    /**init**/
    init:function(varProgramm){
        _this=this;
        $(_this.data.firstrub).hide();

        _this.varProgramm=varProgramm;
        _this.initSlider();

        _this.wrCalculations();
        _this.initCallbackForm();

        console.log(_this);

    }
}