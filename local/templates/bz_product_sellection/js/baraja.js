$(document).ready(function(){
    var $el = $('.wr_shares .shares_container'),
        baraja = $el.baraja();
    // Навигация
    $('.wr_shares .nav-prev').on('click',function(event){
        baraja.previous();
    });
    $('.wr_shares .nav-next').on('click',function(event){
        baraja.next();
    });
});