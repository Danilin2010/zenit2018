$(document).ready(function(){

    $('[data-set-cook-city]').on('click',function(){
        $.cookie('setcity',1,{expires:300});
    });

    var selectCity=$('[data-wr-city] div');
    $('[data-close-city],[data-set-city],[data-wr-city]').on('click',function(){
        $('.b_select_city').removeClass('open');
        $('.wr_mobail_city').removeClass('open');
        $('.wr_select_city').css('position', 'initial');
        $('body').removeClass('modal-open-top');
        selectCity.fadeOut('fast');
        return false;
    });

    $('[data-select-city]').on('click',function(){
        var top=$(window).scrollTop()+30,
            maxtop=$(document).height()-$('[data-wr-city] div').height(),
            windowHeight=$(document).height(),
            sityHeight=$('.select_city').height()
            ;
        if(top>maxtop)
            top=maxtop;
/*
        if(sityHeight>windowHeight)
            top=0;
        else
            top=(windowHeight-sityHeight)/2;
        */

        $('body').addClass('modal-open-top');
        $('.wr_select_city').css('position', 'fixed')
            .children('.select_city').css('top', top+30+'px');
        $('.b_select_city').removeClass('open');
        $('.wr_mobail_city').removeClass('open');

        selectCity.fadeIn('fast',function(e){
            var top;
            var windowHeight=$(window).height(),
                sityHeight=$('.select_city').height()
                ;
            if(sityHeight>windowHeight)
                top=0;
            else
                top=(windowHeight-sityHeight)/2;
            $('.wr_select_city').css('top',top+'px');
        });
        return false;
    });

});