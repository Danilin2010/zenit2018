var Office = (function (self, modal, overlay, OfficeMap) {
    //  'use strict';

    var arrid, atmOfficeDisplay;
    var dom = {};
    var model;

    // определяем ширину прокрутки в px
    var wh = window.innerWidth; // вся ширина окна
    var whs = document.documentElement.clientWidth; // ширина минус прокрутка
    var pls = (wh - whs); // вычисляем ширину прокрутки и даем ей имя pls
    var type = 'branch';
    var data = {};
    var busy = false;
    var loadStatus = false;

    var viewModel = function () {
        var self = this;
        self.items = ko.observableArray('');
        self.listMessage = ko.observable('');
        self.listCity = ko.observableArray([]);
        self.selectedOptions = ko.observable(city.NAME);
        self.dialog = {
            id: ko.observable(''),
            name: ko.observable(''),
            phone: ko.observable(''),
            metro: ko.observable(''),
            address: ko.observable(''),

            worktimeRet: ko.observable(''),
            worktimeRetCashbox: ko.observable(''),
            worktimeCorp: ko.observable(''),
            worktimeCorCashbox: ko.observable(''),
            worktimeAtm: ko.observable(''),
            serviceUr: ko.observableArray([]),
            serviceFiz: ko.observableArray([]),
            serviceAtm: ko.observableArray([])

        };
    };

    model = new viewModel();


    function createItems(items) {
        var self = this;
        self.id = items.id;
        self.name = items.name || '';
        self.description = items.description || '';
        self.type = items.type || '';
        self.coord = items.coord || '';
        self.metro = items.prop.metro.value || false;
        self.phone = items.prop.phone.value || '';
        self.address = items.prop.address.value || '';
        self.service = items.prop.service.value || [];

        self.worktimeRet = items.prop.worktimeRetText.value || '';
        self.worktimeRetCashbox = items.prop.worktimeRetCashboxText.value || '';
        self.worktimeCorp = items.prop.worktimeCorpText.value || '';
        self.worktimeCorCashbox = items.prop.worktimeCorCashboxText.value || '';
        self.worktimeAtm = items.prop.worktimeAtmText.value || '';

        self.baukUid = items.prop.bankId.uid || '';

        self.serviceAtm = [];

        if (items.type == 'atm') {
            for (var i = 0; i < self.service.length; i++) {
                self.serviceAtm.push(self.service[i].name);
            }
        }

        self.modal = openDialog
    }


    function openDialog($data) {
        model.dialog.id($data.id);
        model.dialog.name($data.name);
        model.dialog.phone($data.phone);
        model.dialog.metro($data.metro);
        model.dialog.address($data.address);

        model.dialog.worktimeRet($data.worktimeRet);
        model.dialog.worktimeRetCashbox($data.worktimeRetCashbox);
        model.dialog.worktimeCorp($data.worktimeCorp);
        model.dialog.worktimeCorCashbox($data.worktimeCorCashbox);
        model.dialog.worktimeAtm($data.worktimeAtm);

        model.dialog.serviceUr([]);
        model.dialog.serviceFiz([]);
        model.dialog.serviceAtm([]);



        var fiz = {};
        var ur = {};
        var atm = {};

        //Собираем объект с группами сервисов
        for (var i = 0; i < $data.service.length; i++) {
            if ($data.service[i].typeCode == 'ur') {
                if (!ur.hasOwnProperty($data.service[i].typeName)) {
                    ur[$data.service[i].typeName] = [];
                }
                ur[$data.service[i].typeName].push($data.service[i].name);
            }

            if ($data.service[i].typeCode == 'fiz') {
                if (!fiz.hasOwnProperty($data.service[i].typeName)) {
                    fiz[$data.service[i].typeName] = []
                }

                fiz[$data.service[i].typeName].push($data.service[i].name);
            }

            if ($data.service[i].typeCode == 'atm') {
                if (!atm.hasOwnProperty($data.service[i].typeName)) {
                    atm[$data.service[i].typeName] = []
                }
                atm[$data.service[i].typeName].push($data.service[i].name);
            }
        }

        //Выделяем сервисы для физлиц
        for (var p in fiz) {
            var el = [];
            for (var i = 0; i < fiz[p].length; i++) {
                el.push(fiz[p][i]);
            }
            model.dialog.serviceFiz.push({title: p, items: el});
        }


        //Выделяем сервисы для юрлиц
        for (var p in ur) {
            var el = [];
            for (var i = 0; i < ur[p].length; i++) {
                el.push(ur[p][i]);
            }
            model.dialog.serviceUr.push({title: p, items: el});
        }

        //Выделяем сервисы для банкоматов
        for (var p in atm) {
            var el = [];
            for (var i = 0; i < atm[p].length; i++) {
                el.push(atm[p][i]);
            }
            model.dialog.serviceAtm.push({title: p, items: el});
        }

        //OfficeMap.setMobailPointer($data.id,$data.coord,$data.type,$data.baukUid);
    }


    var loadCity = false;

    // Собираем данные с форм
    function onEvent() {
        $(arrid.join(', ')).on('change', dataSender);

        $('[data-filter="mobile"]').on('click', function (e) {
            //позиционируем карту если переопределен город
            var option = $('#modal_form_filtr').find('[data-block="screen"]:visible').find('select[name="city"] option:selected');

            if(option.val() == '') {
                model.selectedOptions(city.NAME);
            } else {
                model.selectedOptions(option.text());
            }

            GetReceive('#modal_form_filtr');
        });


        $('[data-control="mobile-search"]').on('change', mobileSearch);

        // Переключение внешнего вида список или карта, офис или банкомат
        $(atmOfficeDisplay).on('toggle', function (e, active) {
            filterReset();
            if (active) {
                $("#list-bank").addClass("hidden");
                $("#map-bank").removeClass("hidden");
            } else {
                $("#map-bank").addClass("hidden");
                $("#list-bank").removeClass("hidden");
            }
        });

        $('[data-name=type_cash_list]').on('toggle', function (e, active) {
            filterReset();
            model.selectedOptions(city.NAME);
            model.listCity([]);
            $('[name="bank"] option:first').prop('selected', true);
            $('[name="bank"]').trigger('refresh');
            var title = $('[name="bank"]').data('title');
            $('[name="bank"]').next('.jq-selectbox__select').each(function(){
                if($(this).find('.select_title').length<=0)
                    $(this).prepend('<div class="select_title">' + title + '</div>');
            });
            setToggleData(active);
            GetReceive();
        });

        // Построение маршрута
        $('.open_modal.marshrut').click(function () {
            $('#marshrut').attr('data-placeholder', $(this).attr('id'));
        });

        $(document).on('click', '[data-placeholder]', function () {
            var id = $(this).attr('data-placeholder');
            OfficeMap.addMultiRoute(id);
        });

        // Открыть модальное окно
        $('#filtr-atm-ofice-all').on('click', '.open_modal', function (event) {
            event.preventDefault();
            var id=$(this).attr('id');
            ShowModal($(this).attr('href'),function(){
                OfficeMap.setMobailPointerById(id);
            });
        });


        $('[data-control="mobile-city"]').on('change', function (e) {
            loadCityList('#modal_form_filtr');
        });
        
    }


    function setToggleData(active) {
        if (active) {
            type = 'atm';
            $("#cash-dispenser-map").addClass("hidden");
            $("#offices-map").removeClass("hidden");
            $("#offices").addClass("hidden");
            $("#cash-dispenser").removeClass("hidden");

            $('#mobile_filter_atm').removeClass('hidden');
            $('#mobile_filter_office').addClass('hidden');
        } else {
            type = 'branch';
            $("#offices-map").addClass("hidden");
            $("#cash-dispenser-map").removeClass("hidden");
            $("#cash-dispenser").addClass("hidden");
            $("#offices").removeClass("hidden");
            $('#mobile_filter_atm').addClass('hidden');
            $('#mobile_filter_office').removeClass('hidden');
        }
    }

    function dataSender(e, selector) {

        var sentCancel = $(e.target).attr('data-event') == 'false' ? true : false;
        if (loadCity === true || sentCancel === true) {
            return false;
        }



        if(!selector) {
           selector = '#filtr-atm-ofice-all';
        }

        model.selectedOptions(city.NAME);

        //Если выбран город
        if ($(e.target).attr('name') == 'bank') {
            if (bankZenit.id == $(e.target).val() || $(e.target).val() == '') {
                loadCity = true;
                model.listCity([]);

                setTimeout(function () {
                    loadCity = false;
                    $('[data-plugin="selectCity"]').trigger('refresh');
                    GetReceive(selector);
                }, 200);

            } else {
                //запрашиваем актуальный список городов
                GetData(selector, function (filter) {
                    loadCity = true;
                    $.ajax({
                        type: 'get',
                        url: '/offices/cities.php',
                        data: filter,
                        response: 'text',
                        dataType: 'json',
                        success: function (data) {

                            var items = model.listCity();
                            model.listCity.removeAll();

                            for (var i = 0; i < data.city.length; i++) {
                                items.push(data.city[i]);
                            }
                            model.listCity.valueHasMutated();
                            setTimeout(function () {
                                loadCity = false;
                                $('[data-plugin="selectCity"]').trigger('refresh');
                                var title = $('[data-plugin="selectCity"]').data('title');
                                $('[data-plugin="selectCity"]').next('.jq-selectbox__select').prepend('<div class="select_title">' + title + '</div>');
                            }, 300);
                        },
                    });
                });
            }
        } else {
            GetReceive(selector);
        }

        //позиционируем карту если переопределен город
        if ($(e.target).attr('name') == 'city') {
            var option = $(e.target).find('option:selected');
            if(option.val() == '') {
                model.selectedOptions(city.NAME);
            } else {
                model.selectedOptions(option.text());
            }
        }

        var elem = $('[name=' + $(this).attr('name') + ']').not(this);
        if ($(this).attr('type') == "checkbox") {
            elem.prop('checked', $(this).prop('checked')).trigger('refresh');
        } else {
            elem.val($(this).val());
        }
    }


    function GetData(selector, callback) {
        data = {
            date: new Date(),
            type: type
        };

        var needUserCoord = false;

        $(selector).find('input[type=checkbox]:checked,select,input[type=text]').each(function () {
            if ($(this).attr('type') == 'checkbox') {
                if (!data.hasOwnProperty($(this).attr('name'))) {
                    data[$(this).attr('name')] = [];
                }
                data[$(this).attr('name')].push($(this).val());

                if ($(this).val() == 'nearest') {
                    needUserCoord = true;
                }
            } else {
                if ($(this).val() != '') {
                    data[$(this).attr('name')] = $(this).val();
                }
            }
        });

        //если фильтр требует координат пользователя сделаем запрос и по колбеку вернем данные
        if (needUserCoord === true) {
            OfficeMap.getUserLocation(function (coord) {
                data['coord'] = coord;
                callback(data);
            });
        } else {
            if (callback) {
                callback(data);
            }
        }

        return data;
    }

    var map = null;

    function GetReceive(selector) {

        model.items.removeAll();
        model.listMessage('загрузка...');

        GetData(selector, function (filter) {
            sendData(filter);
        });
    }




    function sendData(filter) {
        if(busy == true) {
            return false;
        }

        busy = true;
        $.ajax({
            type: 'get',
            url: '/offices/ajax.php',
            data: filter,
            response: 'text',
            dataType: 'json',
            success: function (data) {
                var data = data.items;
                model.listMessage('');
                var items = model.items();

                for (var i = 0; i < data.length; i++) {
                    items.push(new createItems(data[i]));
                }

                if (model.items().length == 0) {
                    model.listMessage('По вашему запросу ничего не найдено.');
                }

                model.items.valueHasMutated();

                if (!OfficeMap.hasInit()) {
                    ymaps.ready(function () {
                        OfficeMap.initMap(function () {
                            OfficeMap.addPoints(data);
                        });
                    });
                } else {
                    OfficeMap.removePoint();
                    OfficeMap.addPoints(data);
                }

                if(loadStatus === true) {
                    OfficeMap.goToCity(model.selectedOptions());
                }

                busy = false;
                loadStatus = true;
            },
            error: function (e) {
                busy = false;
                console.log(e);
            }
        });
    }



    function loadCityList(selector) {
        GetData(selector, function (filter) {
            loadCity = true;
            $.ajax({
                type: 'get',
                url: '/offices/cities.php',
                data: filter,
                response: 'text',
                dataType: 'json',
                success: function (data) {
                    var items = model.listCity();
                    model.listCity.removeAll();

                    for (var i = 0; i < data.city.length; i++) {
                        items.push(data.city[i]);
                    }
                    model.listCity.valueHasMutated();
                    setTimeout(function () {
                        loadCity = false;
                        $('[data-plugin="selectCity"]').trigger('refresh');
                        var title = $('[data-plugin="selectCity"]').data('title');
                        if($('[data-plugin="selectCity"]').next('.jq-selectbox__select').find('.select_title').length<=0)
                            $('[data-plugin="selectCity"]').next('.jq-selectbox__select').prepend('<div class="select_title">' + title + '</div>');
                    }, 300);
                }
            });
        });
    }

    /**
     * сбрасывает фильтр обнуляя контролы и объект данных
     */
    function filterReset() {
        data = {};
        dom.activeBlock.find('input[type=checkbox]:checked,select,input[type=text]').each(function () {
            if ($(this).attr('type') == 'checkbox') {
                $(this).prop('checked', false).trigger('refresh');
            } else {
                $(this).val('');
            }
        });
    }

    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    function mobileSearch(e) {
        var data = {
            type: type,
            q: $(e.target).val(),
            date: new Date()
        };

        model.items.removeAll();
        model.listMessage('загрузка...');
        sendData(data);
    }


    return {
        init: function () {
            if (getUrlParameter('type').length) {
                type = getUrlParameter('type');
                setToggleData(type.length);
                $('.toggle[data-name="type_cash_list"]').trigger('click');
            }

            OfficeMap.init();
            arrid = ['#filtr-atm-ofice-all'];
            atmOfficeDisplay = $('[data-name=type_visual]');

            dom = {
                activeBlock: $('[data-block="screen"]')
            };

            onEvent();
            // В модальном окне по клику на построение маршрута перекидывам на карту
            $('.close_marshrut').click(function () {
                $('#list-bank.offices').addClass('hidden');
                $('#map-bank.offices').removeClass('hidden');
                $('[data-name=type_visual]').toggles(true);
            });


            ko.applyBindings(model);

            GetReceive();
        },
        openDialog: function (id) {
            for (var i = 0; i < model.items().length; i++) {
                if (model.items()[i].id == id) {
                    openDialog(model.items()[i]);
                    break;
                }
            }
        }
    }

})(this, modal, overlay, OfficeMap);


$(document).ready(function () {
    Office.init();
});