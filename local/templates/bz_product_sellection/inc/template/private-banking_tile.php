<!--main_cards-->
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"banner_main_product", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "NAME",
			3 => "PREVIEW_TEXT",
			4 => "PREVIEW_PICTURE",
			5 => "DETAIL_TEXT",
			6 => "DETAIL_PICTURE",
			7 => "DATE_ACTIVE_FROM",
			8 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "28",
		"IBLOCK_TYPE" => "banners",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "6",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "154",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "DIGIT",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "banner_main_product",
		"STRICT_SECTION_CHECK" => "N",

	),
	false
);?>
<!--main_cards-->

<!--services-->
<div class="wr_services z-container">
    <div class="wr_exchange_title">Актуальное</div>
    <div class="services">
        <div class="left no_background">
            <a href="#" class="services_right_block">
                <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/011.png')">
                    <div class="h_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/012.png')"></div>
                </div>
                <div class="body">
                    <div class="title">
                        Открыть расчетный счет
                    </div>
                    <div class="text">
                        Узнай что необходимо для открытия счета
                    </div>
                </div>
            </a>
            <a href="#" class="services_right_block">
                <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/013.png')">
                    <div class="h_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/014.png')"></div>
                </div>
                <div class="body">
                    <div class="title">
                        Открыть расчетный счет
                    </div>
                    <div class="text">
                        Узнай что необходимо для открытия счета
                    </div>
                </div>
            </a>
        </div><div class="right">
            <a href="#" class="services_right_block">
                <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/015.png')">
                    <div class="h_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/016.png')"></div>
                </div>
                <div class="body">
                    <div class="title">
                        Вопрос сотруднику банка
                    </div>
                    <div class="text">
                        Задайте вопрос по любому из наших продуктов
                    </div>
                </div>
            </a>
            <a href="#" class="services_right_block">
                <div class="pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/017.png')">
                    <div class="h_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/services/018.png')"></div>
                </div>
                <div class="body">
                    <div class="title">
                        Вопрос сотруднику банка
                    </div>
                    <div class="text">
                        Задайте вопрос по любому из наших продуктов
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<!--services-->
