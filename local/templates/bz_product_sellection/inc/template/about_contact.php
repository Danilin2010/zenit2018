<div class="row manager-top">
    <div class="block-card right note_text">
        <h2>
            Доп. информация
            <div class="note_text">
                Размещается всегда в крайнем правом блоке
            </div>
        </h2>
        <h2>Связаться с банком</h2>
        <div class="form_application_line">
            <div class="contacts_block">
                <a href="mailto:email@zenit.ru">email@zenit.ru</a><br>
                +7 (495) 967-11-11<br>
                8 (800) 500-66-77
            </div>
            <div class="note_text">
                звонок по России бесплатный
            </div>
        </div>
        <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
    </div>
</div>