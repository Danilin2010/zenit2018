<div class="wr_footer_banner wr_footer_big_banner_businesses">
    <div class="footer_banner z-container">
        <div class="footer_banner_left">
            <div class="footer_banner_title">
                Депозиты для бизнеса
            </div>
            <div class="footer_banner_text">
                Вкладывай свою прибыль<br/>
                в дальнейшее развитие компании
            </div>
            <a class="button" href="#">подробнее</a>
        </div>
    </div>
</div>