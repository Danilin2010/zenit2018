<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="corp_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
			<div class="burger_menu_title">Финансирование</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><a href="/big-business/funding/overdraft/">Овердрафт</a></li>
                    <li><a href="/big-business/funding/loans-for-working-capital/">Кредитование на пополнение оборотных средств</a></li>
                    <li><a href="/big-business/funding/investment-lending/">Инвестиционное кредитование</a></li>
                    <li><a href="/big-business/funding/project-financing/">Проектное финансирование</a></li>
                    <li><a href="/big-business/funding/trade-finance/">Торговое финансирование</a></li>
                    <li><a href="/big-business/funding/foreign-trade-investment/">Финансирование  по программам Банка ЗЕНИТ</a></li>
                    <li><a href="/big-business/funding/exiar-coverage/">Финасирование под страховое покрытие АО ЭКСАР</a></li>
                    <li><a href="/big-business/funding/foreign-trade-investment/">Финансирование импорта под страховое покрытие ЭКА</a></li>
                    <?/*<li><a href="#">Инвестиционное финансирование внешнеторговых операций</a></li>
                    <li><a href="#">Банковские гарантии</a></li>*/?>
                    <li><a href="/big-business/funding/organization-of-debt-financing/promissory-notes-program/">Выпуск вексельной программы</a></li>
                    <li><a href="/big-business/funding/organization-of-debt-financing/bonded-loan-issue/">Выпуск облигационного займа</a></li>
                    <li><a href="/big-business/funding/organization-of-debt-financing/syndicated-lending/">Синдицированное кредитование</a></li>
                    <li><a href="/big-business/funding/organization-of-debt-financing/issue-of-eurobonds/">Выпуск еврооблигаций и кредитных нот (CLN, LPN)</a></li>
                    <li><a href="/big-business/funding/organization-of-debt-financing/debt-and-distressed-assets/">Услуги по реструктуризации долга и по работе с проблемными активами</a></li>
                    <li><a href="/big-business/funding/organization-of-debt-financing/support-issuers/">Поддержка эмитентов при прохождении оферт, погашения долга.</a></li>
                    <li><a href="/big-business/funding/factoring/">Факторинг</a></li>
                    <?/*<li><a href="#">Финансирование под страховое покрытие ЭКСАР</a></li>
					<li><span>Факторинг</span>
                        <ul>
                            <li><a href="#">Факторинг с регрессом</a></li>
                            <li><a href="#">Факторинг без регресса</a></li>
                            <li><a href="#">Реверсивный факторинг</a></li>
                            <li><a href="#">Индивидуальные финансовые решения</a></li>
                            <li><a href="#">Информация для дебиторов</a></li>
                        </ul>
					</li>*/?>
				</ul>
			</div>
        </div><div class="burger_menu_item">
            <div class="burger_menu_title">Расчетно-кассовое обслуживание</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Тарифные планы</span>
                        <ul>
                            <li><a href="/big-business/rko/tariff-plans/main-tariff-plans/">Основные тарифные планы</a></li>
                            <li><a href="/big-business/rko/tariff-plans/tariff-plan-importer/">ТП Импортер</a></li>
                        </ul>
                    </li>
                    <li><span>Безналичные переводы</span>
                        <ul>
                            <li><a href="/big-business/rko/transfers/rub">Переводы в рублях</a></li>
                            <li><a href="/big-business/rko/transfers/currency">Переводы в иностранной валюте</a></li>
                            <li><a href="/big-business/rko/transfers/time">Операционное время</a></li>
                        </ul>
                    </li>
                    <li><span>Банковские карты</span>
                        <ul>
                            <li><a href="/big-business/rko/bank-card/customs-card">Таможенная карта</a></li>
                            <li><a href="/big-business/rko/bank-card/salary-project">Зарплатный проект</a></li>
                        </ul>
                    </li>
                    <li><span>Эквайринг</span>
                        <ul>
                            <li><a href="/big-business/rko/acquiring/trade-aquaring/">Торговый экваринг</a></li>
                        </ul>
                    </li>
                    <li><a href="/big-business/rko/conversion-operations/">Конверсионные операции</a></li>
                    <li><span>Специальные предложения и акции</span>
                        <ul>
                            <li><a href="/big-business/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks">Льготные тарифы для клиентов проблемных банков</a></li>
                            <li><a href="/big-business/rko/special-offers-and-promotions/promotions">Акции</a></li>
                        </ul>
                    </li>
                    <li><span>Дистанционное обслуживание</span>
                        <ul>
                            <li><a href="/big-business/rko/remote/clientbank">Клиент-Банк</a></li>
                            <li><a href="/big-business/rko/remote/1c">iBank2 для 1С:Предприятие</a></li>
                            <li><a href="/big-business/rko/remote/sms">SMS - информирование</a></li>
                        </ul>
                    </li>
                </ul>
                <?/*
                <ul class="burger_menu_menu">
                    <li><a href="#">Онлайн-резервирование номера расчетного счета</a></li>
                    <li><span>Тарифные планы</span>
                        <ul>
                            <li><a href="#">Льготные тарифы для клиентов банков с отозванной лицензией</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Открытие счетов</a></li>
                    <li><a href="#">Зарплатный проект</a></li>
                    <li><a href="#">Корпоративные карты</a></li>
                    <li><a href="#">Эквайринг</a></li>
                    <li><a href="#">Безналичные переводы</a></li>
                    <li><span>Дистанционное банковское обслуживание</span>
                        <ul>
                            <li><a href="#">Клиент-Банк</a></li>
                            <li><a href="#">iBank2 для 1С:Предприятие</a></li>
                            <li><a href="#">SMS - информирование</a></li>
                        </ul>
                    </li>
                    <li><span>Валютный контроль</span>
                        <ul>
                            <li><a href="#">Нормативные документы</a></li>
                            <li><a href="#">Бланки документов и порядок их заполнения</a></li>
                            <li><a href="#">Порядок представления документов</a></li>
                            <li><a href="#">Система Клиент-банк и валютный контроль</a></li>
                            <li><a href="#">Нарушения валютного законодательства</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Конверсионные операции</a></li>
                    <li><span>Инкассация</span>
                        <ul>
                            <li><a href="#">Online-заявка</a></li>
                            <li><a href="#">Инкассация на специальных бронеавтомобилях</a></li>
                            <li><a href="#">Инкассация с использованием банкоматов</a></li>
                            <li><a href="#">Договоры и формы документов</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Сейфовые ячейки</a></li>
                    <li><a href="#">Прием чеков на инкассо</a></li>
                </ul>
                */?>
            </div>
        </div><div class="burger_menu_item">
            <div class="burger_menu_title">Размещение денежных средств</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Срочное размещение</span>
                        <ul>
                            <li><a href="/big-business/allocation-of-funds/deposit-products/term-deposits">Депозиты</a></li>
                            <li><a href="/big-business/allocation-of-funds/deposit-products/dual-currency-deposit">Бивалютный депозит</a></li>
                            <li><a href="/big-business/allocation-of-funds/deposit-products/minimum-balance-on-the-account">Неснижаемый остаток на расчетном счете</a></li>
                        </ul>
                    </li>
                    <li><span>Ценные бумаги</span>
                        <ul>

                            <li><a href="/big-business/allocation-of-funds/securities/certificates-of-deposit/">Депозитные сертификаты</a></li>
                        </ul>
                    </li>
                    <li><a href="/big-business/documentary-operations/">Документарные операции</a></li>
                </ul>
                <?/*
                <ul class="burger_menu_menu">
                    <li><a href="#">Как разместить средства в Банке?</a></li>
                    <li><a href="#">Депозиты</a></li>
                    <li><a href="#">Неснижаемый остаток на расчетном счете</a></li>
                    <li><a href="#">Векселя</a></li>
                    <li><a href="#">Депозитные сертификаты</a></li>
                    <li><a href="#">Бивалютный депозит</a></li>
                </ul>
                */?>
            </div>
		</div>
    </div>
</div>