<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="fin_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
			<div class="burger_menu_title">Конверсионный и межбанковский рынки</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/conversion-and-interbank-markets/operations-with-derivative-financial-instruments/">Операции с ПФИ</a></li>
                    <li><a href="/corp-and-fin/conversion-and-interbank-markets/conversion-operations/">Конверсионные операции</a></li>
				</ul>
			</div>
            <a href="/corp-and-fin/correspondent-relations/" class="menu_bottom_block_title">Корреспондентские отношения</a>

           <?/* <div class="burger_menu_title">Организация проектов по привлечению долгового финансирования</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="#">Организация облигационного займа</a></li>
                    <li><a href="#">Синдицированное кредитование</a></li>
                    <li><a href="#">Поддержка эмитентов при прохождении оферт, погашения долга.</a></li>
                    <li><a href="#">Бридж-финансирование</a></li>
                    <li><a href="#">Организация вексельной программы</a></li>
                </ul>
            </div>*/?>
        </div><div class="burger_menu_item">
            <div class="burger_menu_title">Операции на рынке ценных бумаг</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/trading-operations/">Торговые операции</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/repo-transactions-with-securities/">Операции РЕПО с ценными бумагами</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/custody-services/">Депозитарное обслуживание</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/brokerage-service/">Брокерское обслуживание</a></li>
                </ul>
            </div>

            <div class="burger_menu_title">Организация проектов по привлечению долгового финансирования</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/organization-of-debt-financing/bonded-loan-issue/">Выпуск облигационного займа</a></li>
                    <li><a href="/corp-and-fin/organization-of-debt-financing/syndicated-lending/">Синдицированное кредитование</a></li>
                    <li><a href="/corp-and-fin/organization-of-debt-financing/support-issuers/">Поддержка эмитентов при прохождении оферт, погашения долга.</a></li>
                    <li><a href="/corp-and-fin/organization-of-debt-financing/promissory-notes-program/">Выпуск вексельной программы</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">
            <?/*<div class="burger_menu_title">Банконтные операции</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="#">Операции с наличной валютой</a></li>
                    <li><a href="#">Инкассация</a></li>
                </ul>
            </div>*/?>
            <div class="burger_menu_title">Другие продукты</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/program-for-bank-cards-for-partner-banks/">Программы по банковским картам для банков-партнеров</a></li>
                    <li><a href="/corp-and-fin/details-of-pjsc-bank-zenit/">Реквизиты ПАО Банк ЗЕНИТ</a></li>


                </ul>
            </div>*/?>
		</div><div class="burger_menu_item">


        </div>
    </div>
</div>