<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="mcb_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
			<div class="burger_menu_title">Расчетно-кассовое обслуживание</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Открытие и ведение счета</span>
                        <ul>
                            <li><a href="/businesses/rko/account/online-reserve">Онлайн-резервирование номера счета</a></li>
                            <?/*<li><a href="#">Документы для открытия счета</a></li>*/?>
                        </ul>
					</li>
					<li><span>Тарифные планы</span>
                        <ul>
                            <li><a href="/businesses/rko/tariff-plans/main-tariff-plans/">Основные тарифные планы</a></li>
                            <li><a href="/businesses/rko/tariff-plans/tariff-plan-importer/">ТП Импортер</a></li>
                            <?/*<li><a href="#">Подобрать тарифный план (калькулятор)</a></li>*/?>
                        </ul>
					</li>
					<li><span>Безналичные переводы</span>
                        <ul>
                            <li><a href="/businesses/rko/transfers/rub">Переводы в рублях</a></li>
                            <li><a href="/businesses/rko/transfers/currency">Переводы в иностранной валюте</a></li>
                            <li><a href="/businesses/rko/transfers/time">Операционное время</a></li>
                        </ul>
					</li>
                    <li><span>Дистанционное обслуживание</span>
                        <ul>
                            <li><a href="/businesses/rko/remote/clientbank">Система Клиент-Банк</a></li>
                            <li><a href="/businesses/rko/remote/1c">Клиент-Банк из 1С: Предприятие</a></li>
                            <li><a href="/businesses/rko/remote/mobail">Мобильный банк</a></li>
                            <li><a href="/businesses/rko/remote/sms">SMS - информирование</a></li>
                            <li><a href="/businesses/rko/remote/indicator">Индикатор</a></li>
                        </ul>
                    </li>
                    <li><span>Банковские карты</span>
                        <ul>
                            <li><a href="/businesses/rko/bank-card/customs-card">Таможенная карта</a></li>
                            <li><a href="/businesses/rko/bank-card/salary-project">Зарплатный проект</a></li>
                        </ul>
                    </li>
                    <li><span>Эквайринг</span>
                        <ul>
                            <li><a href="/businesses/rko/acquiring/trade-aquaring/">Торговый экваринг</a></li>
                        </ul>
                    </li>
                    <li><a href="/businesses/rko/collection-and-delivery-of-cash/">Инкассация и доставка наличных</a></li>
                    <li><span>Дополнительные продукты</span>
                        <ul>
                            <li><a href="/businesses/rko/additional/check">Прием чеков на инкассо</a></li>
                            <li><a href="/businesses/rko/additional/cells">Сейфовые ячейки</a></li>
                        </ul>
                    </li>
                    <li><span>Специальные предложения и акции</span>
                        <ul>
                            <li><a href="/businesses/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks">Льготные тарифы для клиентов проблемных банков</a></li>
                            <li><a href="/businesses/rko/special-offers-and-promotions/promotions">Акции</a></li>
                        </ul>
                    </li>
                    <li><span>Тарифы</span>
                        <ul>
                            <li><a href="/businesses/rko/rates/collection-of-tariffs">Сборник тарифов</a></li>
                        </ul>
                    </li>
				</ul>
			</div>
        </div><div class="burger_menu_item">
            <div class="burger_menu_title">Кредиты</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/loans/working-capital-loan">Оборотное кредитование</a></li>
                    <li><a href="/businesses/loans/overdraft">Овердрафт</a></li>
                    <li><a href="/businesses/loans/tender-loan">Тендерный кредит</a></li>
                    <li><span>Инвестиционное кредитование</span>
                        <ul>
                            <li><a href="/businesses/loans/investment-lending/real-estate">Недвижимость</a></li>
                            <li><a href="/businesses/loans/investment-lending/equipment-and-transport">Оборудование и транспорт</a></li>
                        </ul>
                    </li>
                    <li><span>Государственная поддержка субъектов МСБ</span>
                        <ul>
                            <li><a href="/businesses/loans/state-support-of-smes/program-6-5-jsc-corporation-smes">Программа 6,5 АО "Корпорации "МСП"</a></li>
                            <li><a href="/businesses/loans/state-support-of-smes/warranty-support-of-jsc-corporation-smes-jsc-sme-bank">Гарантийная поддержка АО "Корпорация "МСП/АО "МСП Банк"</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="burger_menu_title">Гарантии</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/warranty/electronic-guarantee-for-the-participants-of-public-procurement">Электронная гарантия для участников госзакупок</a></li>
                    <li><a href="/businesses/warranty/commercial-warranty">Коммерческие гарантии</a></li>
                </ul>
            </div>
        </div><div class="burger_menu_item">
            <div class="burger_menu_title">Размещение средств</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/allocation-of-funds/how-to-place-funds-in-the-bank/five-steps-to-income">Как разместить средства в Банке?</a></li>
                    <li><span>Депозитные продукты</span>
                        <ul>
                            <li><a href="/businesses/allocation-of-funds/deposit-products/term-deposits">Срочные депозиты</a></li>
                            <li><a href="/businesses/allocation-of-funds/deposit-products/dual-currency-deposit">Бивалютный депозит</a></li>
                            <li><a href="/businesses/allocation-of-funds/deposit-products/minimum-balance-on-the-account">Неснижаемый остаток на расчетном счете</a></li>
                        </ul>
                    </li>
                    <li><span>Ценные бумаги</span>
                        <ul>

                            <li><a href="/businesses/allocation-of-funds/securities/certificates-of-deposit/">Депозитные сертификаты</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
			<div class="burger_menu_title">Международные расчеты</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Валютный контроль</span>
                        <ul>
                            <li><a href="/businesses/international-payments/currency-control/description-and-documents">Описание и документы</a></li>
                            <li><a href="/businesses/international-payments/currency-control/violation-of-currency-legislation">Нарушения валютного законодательства</a></li>
                        </ul>
					</li>
					<li><a href="/businesses/international-payments/conversion-operations">Конверсионные операции</a></li>
                    <li><a href="/businesses/international-payments/documentary-operations">Документарные операции</a></li>
				</ul>
			</div>

		</div><div class="burger_menu_item">


        </div>
    </div>
</div>