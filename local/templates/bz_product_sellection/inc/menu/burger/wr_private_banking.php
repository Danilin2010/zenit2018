<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="private_banking_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "simple_menu",
                    array(
                        "ADD_CLASS"=>"burger_menu_menu",
                        "IBLOCK_TYPE" => "privatebanking",
                        "IBLOCK_ID" => "77",
                        "NEWS_COUNT" => "100",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "N",
                        "STRICT_SECTION_CHECK" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => ""
                    ),
                    false
                ); ?>
            </div>
        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "simple_menu",
                    array(
                        "ADD_CLASS"=>"burger_menu_menu",
                        "IBLOCK_TYPE" => "privatebanking",
                        "IBLOCK_ID" => "69",
                        "NEWS_COUNT" => "100",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "3600",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "N",
                        "STRICT_SECTION_CHECK" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => ""
                    ),
                    false
                ); ?>
            </div>


            <?/*<div class="burger_menu_title">Привлечение средств в премиальные вклады</div>*/?>

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/savings-and-investments/investment-management/">Управление инвестициями</a></li>
                    <li><a href="/private-banking/savings-and-investments/investment-management/brokerage-service/">Брокерское обслуживание</a></li>
                    <li><a href="/private-banking/savings-and-investments/investment-management/collective-trust-management/">Коллективное доверительное управление</a></li>
                    <li><a href="/private-banking/savings-and-investments/investment-management/depositary-services/">Услуги депозитария</a></li>
                </ul>
            </div>

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/savings-and-investments/investment-management/individual-trust-management/">Индивидуальное доверительное управление активами Клиента</a></li>
                    <li><a href="/private-banking/savings-and-investments/investment-management/investment-portfolio/">Структурирование инвестиционного портфеля Клиента</a></li>
                    <li><a href="/private-banking/savings-and-investments/investment-management/private-consultant-advisory/">Частный консультант Advisory</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">


            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/traditional-banking-products/cash-management-services/">Расчетно-кассовое обслуживание</a></li>
                    <li><a href="/private-banking/traditional-banking-products/documentary-and-international-operations/">Документарные и международные операции</a></li>
                    <li><a href="/private-banking/traditional-banking-products/elite-credit-card/">Элитные банковские карты</a></li>
                </ul>
            </div>

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/traditional-banking-products/individual-credit-program/">Индивидуальные кредитные программы</a></li>
                    <li><a href="/private-banking/traditional-banking-products/operations-of-precious-metals/">Операции на рынке драгоценных металлов и обезличенные металлические счета</a></li>
                </ul>
            </div>

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/private-banking/traditional-banking-products/safe-deposit-boxes/">Сейфовые ячейки</a></li>
                    <li><a href="/private-banking/traditional-banking-products/the-collection-of-funds/">Инкассирование денежных средств</a></li>
                </ul>
            </div>


		</div>
    </div>
</div>