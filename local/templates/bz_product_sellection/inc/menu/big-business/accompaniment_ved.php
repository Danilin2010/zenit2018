<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="big_business_accompaniment_ved">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Валютный контроль</span>
                        <ul>
                            <li><a href="#">Нормативные документы</a></li>
                            <li><a href="#">Бланки документов и порядок их заполнения</a></li>
                            <li><a href="#">Порядок представления документов</a></li>
                            <li><a href="#">Система Клиент-банк и валютный контроль</a></li>
                            <li><a href="#">Нарушения валютного законодательства</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Конверсионные операции</a></li>
                    <li><span>Операции с национальными валютами</span>
                        <ul>
                            <li><a href="#">Юань в международных расчетах</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Операции с клиринговыми валютами</span>
                        <ul>
                            <li><a href="#">Участие в тендерах Внешэкономбанка</a></li>
                            <li><a href="#">Конверсия в клиринговых валютах</a></li>
                            <li><a href="#">Обсуживание расчетов в клиринговых валютах</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Торговое финансирование</a></li>
                    <li><a href="#">Финасирование под страховое<br>покрытие АО ЭКСАР</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="#"></a>Инвестиционное финансирование<br> внешнеторговых операций</li>
                    <li><a href="#">Документарные операции</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>