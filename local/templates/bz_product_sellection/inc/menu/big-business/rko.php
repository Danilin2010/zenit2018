<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="big_business_rko">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Открытие и ведение счета</span>
                        <ul>
                            <li><a href="#">Специальный счет для формирования фонда капитального ремонта</a></li>
                            <li><a href="#">Документы для открытия счета</a></li>
                        </ul>
                    </li>
                    <li><span>Тарифы</span>
                        <ul>
                            <li><a href="#">Тарифные планы</a></li>
                            <li><a href="#">Базовые тарифы</a></li>
                        </ul>
                    </li>
                    <li><span>Безналичные переводы</span>
                        <ul>
                            <li><a href="/big-business/rko/transfers/rub">Переводы в рублях</a></li>
                            <li><a href="/big-business/rko/transfers/currency">Переводы в иностранной валюте</a></li>
                            <li><a href="/big-business/rko/transfers/time">Операционное время</a></li>
                        </ul>
                    </li>
                    <li><span>Банковские карты</span>
                        <ul>
                            <li><a href="#">Корпоративные карты</a></li>
                            <li><a href="/big-business/rko/bank-card/customs-card">Таможенная карта</a></li>
                        </ul>
                    </li>
                    <li><span>Документарные операции</span>
                        <ul>
                            <li><a href="#">Аккредитивы</a></li>
                            <li><a href="#">Банковские гарантии</a></li>
                            <li><a href="#">Инкассо</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/big-business/rko/bank-card/salary-project">Зарплатные проекты</a></li>
                    <li><span>Эквайринг</span>
                        <ul>
                            <li><a href="/big-business/rko/acquiring/trade-aquaring/">Торговый экваринг</a></li>
                            <li><a href="#">Кассовые решения</a></li>
                            <li><a href="#">Интернет-эквайринг</a></li>
                        </ul>
                    </li>
                    <li><span>Кассовые операции и инкассация</span>
                        <ul>
                            <li><a href="#">Инкассация</a></li>
                            <li><a href="#">Операции с наличными</a></li>
                            <li><a href="#">Договоры и формы документов</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Организация операционного подразделения <br>Банка на территории клиента (ОКВКУ)</a>
                    </li>
                    <li><a href="/big-business/rko/conversion-operations/">Конверсионные операции</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Операции с клиринговыми и <br>национальными валютами</span>
                        <ul>
                            <li><a href="#">Операции с клиринговыми валютами</a></li>
                            <li><a href="#">Юань в международных расчетах</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Сейфовые ячейки</a></li>
                    <li><span>Специальные предложения и акции</span>
                        <ul>
                            <li>
                                <a href="/big-business/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks">Льготные
                                    тарифы для клиентов проблемных банков</a></li>
                            <li><a href="/big-business/rko/special-offers-and-promotions/promotions">Акции</a></li>
                        </ul>
                    </li>
                    <li><span>Дистанционное обслуживание</span>
                        <ul>
                            <li><a href="/big-business/rko/remote/clientbank/">Клиент-Банк</a></li>
                            <li><a href="/big-business/rko/remote/1c/">iBank2 для 1С:Предприятие</a></li>
                            <li><a href="/big-business/rko/remote/sms/">SMS - информирование</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>