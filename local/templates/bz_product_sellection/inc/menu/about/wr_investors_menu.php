<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="investors_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><a href="/about/investors/promotions">Акции</a></li>
                    <li><span>Информация для акционеров</span>
                        <ul>
                            <li><a href="/about/investors/information-shareholders/mandatory-offer">Акционерам</a></li>
                            <li><a href="/about/investors/information-shareholders/shareholders">Обязательное предложение</a></li>
                        </ul>
                    </li>
				</ul>
			</div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/about/investors/attracting-financing">Привлечение долгового финансирования</a></li>
                    <li><a href="/about/investors/issue-bonds">Эмиссии облигаций</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>