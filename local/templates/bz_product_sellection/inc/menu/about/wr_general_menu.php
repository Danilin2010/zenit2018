<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="general_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><a href="/about/general/history">История</a></li>
                    <li><a href="/about/general/charter-bank">Устав Банка ЗЕНИТ</a></li>
                    <li><span>Общая информация корпоративного управления</span>
                        <ul>
                            <li><a href="/about/general/general-information-corporate-governance/corporate-governance">Корпоративное управление</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/management-bank">Правление Банка</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/board-directors-bank">Совет директоров Банка</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/committees-under-bank">Комитеты при Совете директоров Банка</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/general-meeting-shareholders">Общее собрание акционеров</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/corporate-secretary">Корпоративный секретарь</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/internal-control">Внутренний контроль</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/audit-committee">Ревизионная комиссия</a></li>
                            <li><a href="/about/general/general-information-corporate-governance/principles-corporate-governance">Принципы корпоративного управления</a></li>
                        </ul>
                    <li><a href="/about/general/strategy">Стратегия</a></li>
                    <li><a href="/about/general/credit-ratings">Кредитные рейтинги</a></li>
                    <li><a href="/about/general/licenses-membership">Лицензии и членство в ассоциациях</a></li>
                    <li><span>Информация о корпоративных ценностях</span>
                        <ul>
                            <li><a href="/about/general/information-corporate-values/corporate-values">Корпоративные ценности</a></li>
                            <li><a href="/about/general/information-corporate-values/information-policy">Информационная политика</a></li>
                        </ul>
                    </li>
                    <li><a href="/about/general/requisites">Реквизиты</a></li>
                    <li><a href="/about/general/insiders">Инсайдерам</a></li>
				</ul>
			</div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Информация о социальных проектах</span>
                        <ul>
                            <li><a href="/about/general/information-social-projects/social-projects">Социальные проекты</a></li>
                            <li><a href="/about/general/information-social-projects/charity-strategy">Стратегия благотворительной деятельности Банковской группы ЗЕНИТ</a></li>
                            <li><a href="/about/general/information-social-projects/rugby-world">Кубок мира по регби-7 и Фестиваль детского регби в Москве</a></li>
                            <li><a href="/about/general/information-social-projects/getting-charity">Получение благотворительной и спонсорской помощи</a></li>
                        </ul>
                    </li>
                    <li><a href="/upload/about/pdf/personal_data.pdf" target="_blank">Политика в отношении обработки персональных данных</a></li>
                    <li><a href="/about/general/countering-laundering">Противодействие отмыванию преступных доходов</a></li>
                    <li><a href="/about/general/trust-line">Линия доверия</a></li>
                    <li><a href="/about/general/fatca">FATCA</a></li>
                    <li><a href="/about/general/insurance-payments">Страховые выплаты по решениям АСВ</a></li>
                    <li><a href="/about/general/property-sale">Имущество для реализации</a></li>
                    <li><a href="/about/general/jobs">Вакансии</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">


        </div>
    </div>
</div>