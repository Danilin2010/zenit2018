<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="disclosure_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/about/disclosure/posts/">Сообщения о существенных фактах</a></li>
                    <li><a href="/about/disclosure/quarterly-reports/">Ежеквартальные отчеты по ценным бумагам</a></li>
                    <li><a href="/about/disclosure/affiliated/">Список аффилированных лиц</a></li>
                    <li><a href="/about/disclosure/annual-reports/">Годовые отчеты ПАО Банк ЗЕНИТ</a></li>
                    <li><a href="/about/disclosure/list-insider/">Перечень инсайдерской информации</a></li>
                    <li><a href="/about/disclosure/financial-statements/">Финансовая отчетность</a></li>
                    <li><a href="/about/disclosure/equity-structure/">Структура акционерного капитала</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <!--li><span>Информация о квалификации и опыте работы руководителей Банка и его филиалов</span>
                        <ul>
                            <li><a href="/about/disclosure/Information-qualifications/heads/">Информация о руководителях и главных бухгалтерах филиалов Банка</a></li>
                        </ul>
                    </li-->
                    <li><a href="/about/disclosure/information-qualifications/">Информация о квалификации и опыте работы руководителей Банка и его филиалов</a></li>
                    <li><a href="/about/disclosure/additional-issue/">Дополнительный выпуск акций</a></li>
                    <li><a href="/about/disclosure/disclosure-information/">Раскрытие информации для регулятивных целей</a></li>
                    <!--li><span>Раскрытие информации для регулятивных целей</span>
                        <ul>
                            <li><a href="/about/disclosure/disclosure-information/information-risks/">Информация о рисках на консолидированной основе</a></li>
                            <li><a href="/about/disclosure/disclosure-information/information-rates/">Информация о ставках по вкладам для физических лиц</a></li>
                            <li><a href="/about/disclosure/disclosure-information/information-bank/">Информация об инструментах капитала Банка</a></li>
                            <li><a href="/about/disclosure/disclosure-information/information-capital/">Информация об инструментах капитала Банковской группы</a></li>
                        </ul>
                    </li-->
                    <li><a href="/about/disclosure/disclosure-information-professional/">Раскрытие информации профессиональным участником рынка ценных бумаг</a></li>
                    <!--li><span>Раскрытие информации профессиональным участником рынка ценных бумаг</span>
                        <ul>
                            <li><a href="/about/disclosure/disclosure-information-professional/documents-trust-management/">Документы по доверительному управлению</a></li>
                        </ul>
                    </li-->
                </ul>
            </div>
        </div>
    </div>
</div>