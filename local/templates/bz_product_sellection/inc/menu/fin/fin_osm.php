<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="fin_osm">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/trading-operations/">Торговые операции</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/repo-transactions-with-securities/">Операции РЕПО с ценными бумагами</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/custody-services/">Депозитарное обслуживание</a></li>
                    <li><a href="/corp-and-fin/operations-on-the-securities-market/brokerage-service/">Брокерское обслуживание</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

        </div>
    </div>
</div>
