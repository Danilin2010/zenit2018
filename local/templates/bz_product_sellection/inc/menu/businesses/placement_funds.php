<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_placement_funds">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li>
            <a href="/businesses/allocation-of-funds/how-to-place-funds-in-the-bank/five-steps-to-income/" class="">Как разместить средства в Банке?</a>
                    </li>
                </ul>
            </div>
        </div><div class="burger_menu_item">
            <div class="menu_bottom_block_title">Депозитные продукты</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/allocation-of-funds/deposit-products/term-deposits/">Срочные депозиты</a></li>
                    <li><a href="/businesses/allocation-of-funds/deposit-products/dual-currency-deposit/">Бивалютный депозит</a></li>
                    <li><a href="/businesses/allocation-of-funds/deposit-products/minimum-balance-on-the-account/">Неснижаемый остаток на расчетном счете</a></li>
                </ul>
            </div>
        </div><div class="burger_menu_item">
            <div class="menu_bottom_block_title">Ценные бумаги</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">

                    <li><a href="/businesses/allocation-of-funds/securities/certificates-of-deposit/">Депозитные сертификаты</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

