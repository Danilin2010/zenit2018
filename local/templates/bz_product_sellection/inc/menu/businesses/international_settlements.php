<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_international_settlements">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="menu_bottom_block_title">Валютный контроль</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/international-payments/currency-control/description-and-documents/">Описание и документы</a></li>
                    <li><a href="/businesses/international-payments/currency-control/violation-of-currency-legislation/">Нарушения валютного законодательства</a></li>
                </ul>
            </div>


        </div><div class="burger_menu_item">

            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/international-payments/conversion-operations/">Конверсионные операции</a></li>
                    <li><a href="/businesses/international-payments/documentary-operations/">Документарные операции</a></li>
                </ul>
            </div>

        </div><div class="burger_menu_item">

        </div>
    </div>
</div>

