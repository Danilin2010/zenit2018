<div class="wr_menu_bottom" id="menu_payments" style="display: none">
	<div class="menu_bottom z-container">
		<div class="menu_bottom_block">
			<a href="/personal/payments/payment" class="menu_bottom_block_title">Оплата услуг</a><br />
			<div class="menu_bottom_block_title no_magrin_top">Переводы</div>
			<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
				"ADD_SECTIONS_CHAIN" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COUNT_ELEMENTS" => "N",
				"IBLOCK_ID" => "56",
				"IBLOCK_TYPE" => "payments",
				"SECTION_CODE" => "",
				"SECTION_FIELDS" => array(),
				"SECTION_ID" => "",
				"SECTION_URL" => "/personal/payments/transfers/#SECTION_CODE#/",
				"SECTION_USER_FIELDS" => array(),
				"SHOW_PARENT_NAME" => "N",
				"TOP_DEPTH" => "2",
				"VIEW_MODE" => "LINE",
				"CSS_CLASS" => "menu_bottom_block_ul"
			)); ?>
		</div><div class="menu_bottom_block">
            <a href="http://bz.rusrobots.ru/personal/cards/credit-card/credit-card-iglobe-platinum/" class="menu_banner"
               style="
               background-image: url('/upload/iblock/8bc/cart.png');
     background-size: auto;
    background-position: 260px 157px;
               "
                >
                <div class="menu_banner_title">
                    «Мир Путешествий»
                </div>
                <div class="menu_banner_text">
                    до 15 миль каждые 73 рубля
                    потраченные на iglobe.ru

                </div>
                <div class="menu_banner_more">
                    подробнее
                </div>
            </a>
        </div><div class="menu_bottom_block">
            <a href="https://my.zenit.ru/wb/" class="menu_banner"
               style="
               background-image: url('/local/templates/bz/img/services/001.png');
                   background-size: auto;
    background-position: 260px 135px;
               "
                >
                <div class="menu_banner_title">
                    Мобильный банкинг
                </div>
                <div class="menu_banner_text">
                    Мгновенно между любыми картами любых банков
                </div>
                <div class="menu_banner_more">
                    подробнее
                </div>
            </a>
        </div>
    </div>
</div>