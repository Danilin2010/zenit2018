<div class="banners c-container">
    <div class="wr_banners_item">
        <div class="banners_item">
            <div class="banners_title">
                Брокерское обслуживание
            </div>
            <div class="banners_text">
                Покупайте и продавайте ценные<br>
                бумаги на организованном рынке<br>
                ценных бумаг самостоятельно.
            </div>
            <div class="banners_button">
                <a href="#" class="button min_height">подробнее</a>
            </div>
        </div>
        <div class="banners_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/banners/001.png');"></div>
    </div><div class="wr_banners_item">
        <div class="banners_item">
            <div class="banners_title">
                Перевод с карты на карту
            </div>
            <div class="banners_text">
                Моментальный перевод денежных<br>
                средств на карту клиента<br>
                другого банка.
            </div>
            <div class="banners_button">
                <a href="/personal/payments/transfers/card-to-card/" class="button min_height">подробнее</a>
            </div>
        </div>
        <div class="banners_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/banners/002.png');"></div>
    </div><div class="wr_banners_item">
        <div class="banners_item">
            <div class="banners_title">
                Мобильный банк
            </div>
            <div class="banners_text">
                Статистика по вашим счетам, любые<br>
                платежи и переводы. Все под рукой<br>
                и абсолютно бесплатно.
            </div>
            <div class="banners_button">
                <ul class="mobile_banking_line_icon">
                    <li><a href="#" class="mobile_banking_line_icon apple"><div></div></a></li>
                    <li><a href="#" class="mobile_banking_line_icon android"><div></div></a></li>
                </ul>
            </div>
        </div>
        <div class="banners_pict" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/banners/003.png');"></div>
    </div>
</div>