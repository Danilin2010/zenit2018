<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?$APPLICATION->ShowTitle()?></title>

    <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.ico" />
    <meta name="theme-color" content="#1da9af">
    <link rel="icon" sizes="192x192" href="<?=SITE_TEMPLATE_PATH?>/img/icon/android-chrome-192.png">
    <meta content="app-id=1187159866" name="apple-itunes-app">
    <meta content="app-id=ru.zenit.zenitonline" name="google-play-app">

    <link rel="manifest" href="/manifest/manifest.json">
    <?
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/slick/slick.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.scroolly.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery-ui/jquery-ui.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.number.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.formstyler.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/baraja/modernizr.custom.79639.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/baraja/jquery.baraja.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/vivus/pathformer.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/vivus/vivus.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/smartbanner/_jquery.smartbanner.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/toggles/toggles.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.validate.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/additional-methods.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/localization/messages_ru.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/inputmask/jquery.inputmask.bundle.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/slider-pips/jquery-ui-slider-pips.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.ui.touch-punch.min.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.mousewheel.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.kinetic.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jquery.smoothDivScroll-1.3.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/vendors/jquery.cookie.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/vendors/jslider/jquery.slider-bundled.js");

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/toggle.js");
    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/baraja.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/city.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/slider.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/top_banner.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/main.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/custom.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/form_search.js');

    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/js/contact-form.js');
    //Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/zayavka.js");


    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/slick/slick.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/jquery-ui/jquery-ui.structure.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/baraja/baraja.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/smartbanner/jquery.smartbanner.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/toggles/css/toggles.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/toggles/css/themes/toggles-modern.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/toggles/css/themes/toggles-light.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/slider-pips/jquery-ui-slider-pips.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendors/jslider/css/jquery.jslider-all.css");
    //Asset::getInstance()->addCss("https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/font/Lato/font.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/font/ProximaNova/font.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/toggle.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/icon.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/gui.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/_sky.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/slider.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/main.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/1600.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/900.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/mobile.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/offices-mobile.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/about.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/about-mobile.css");
    //Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/menu-drob.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/offices.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/search.css");

    //Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css");
    ?>


    <?/*<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet" />*/?>
    <?$APPLICATION->ShowHead()?>
</head>

<body>
<?if ($_GET['get_header']) {

    $response = array(
        'status' => true,
        'message' => 'Success',
        'data' => BufferContent::ShowTitle('top_breadcrumbs_images')
    );
    json_encode($response);
}?>
<div class="main_wrapper">






