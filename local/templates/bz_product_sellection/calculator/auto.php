<div class="wr_block_type gray calculator ">
    <div class="block_type to_column c-container">
        <h1>Рассчитайте ваше предложение по автокредиту</h1>
        <div class="block_type_center">
            <form action="" class="calculator_mortgage">
                <div class="form_application">
                    <?if($programm){?>
                        <input type="hidden" data-programm name="programm" value="<?=$programm?>"/>
                    <?}else{?>
                        <div class="form_application_line">
                            <select data-programm data-plasholder="Выберите программу" data-title="Программа кредитования" class="formstyle" name="programm">
                                <option value="new" selected>Новый</option>
                                <option value="mileage">С пробегом</option>
                                <option value="commercial">Коммерческое</option>
                                <?/*<option value="support">С господдержкой</option>*/?>
                                <option value="residual">С остаточным пл</option>
                            </select>
                        </div>
                    <?}?>
                    <div class="form_application_line">
                        <div class="form_application_line_title">
                            Регион
                        </div>
                        <div class="form_application_item_label">
                            <label><input data-region class="formstyle" type="radio" name="region" value="moscow" checked/>Московский регион, Санкт-Петербург</label>
                        </div>
                        <div class="form_application_tree">
                            <label><input data-region class="formstyle" type="radio" name="region" value="region"/>Другие регионы</label>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_line_title">
                            Сумма кредита
                        </div>
                        <div class="wr_card_to_card">
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-amount-credit type="text" class="simple_input big" id="amount-credit" value="3250000">
                                </div>
                                <div class="complex_input_block summ"></div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="amount-credit"
                                 data-max="32000000"
                                 data-min="1000000"
                                 data-value="3250000"
                                 data-suffix=" МЛН"
                                 data-del="1000000"
                                 data-useamount="Y"
                                 data-useconversion="amount_credit"
                                ></div>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_line_title" data-pledgetext>
                            Стоимость приобретаемого автомобиля
                        </div>
                        <div class="wr_card_to_card">
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-summ type="text" class="simple_input big" id="main_ptop" value="2050000">
                                </div>
                                <div class="complex_input_block summ"></div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="main_ptop"
                                 data-max="32000000"
                                 data-min="1000000"
                                 data-value="2050000"
                                 data-del="1000000"
                                 data-suffix=" МЛН"
                                 data-useconversion="cost_real_estate"
                                ></div>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_line_title">
                            <div class="form_application_line_title_right" data-first-rub><span data-print-first-rub>50</span> %</div><div class="form_application_line_title_right" data-first-precent><span data-print-first>5 000 000</span> <span class="rub">₽</span></div>
                            Первоначальный взнос
                            <div class="wr-toggle-light-text line_initial">
                                <div class="toggle-light-text on">В процентах</div>
                                <div class="toggle-light-wr">
                                    <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N" data-name="type"></div>
                                </div>
                                <div class="toggle-light-text off">В рублях</div>
                            </div>
                        </div>
                        <div class="wr_card_to_card" data-first-precent>
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-contribution type="text" class="simple_input big" id="first_calk" value="35">
                                </div>
                                <div class="complex_input_block">%</div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="first_calk"
                                 data-max="80"
                                 data-min="10"
                                 data-value="35"
                                 data-suffix="%"
                                ></div>
                        </div>
                        <div class="wr_card_to_card" data-first-rub>
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-contributionrub type="text" class="simple_input big" id="first_calk_rub" value="714000">
                                </div>
                                <div class="complex_input_block summ"></div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="first_calk_rub"
                                 data-setfirstfumm="Y"
                                 data-max="32000000"
                                 data-min="1000"
                                 data-del="1000000"
                                 data-value="714000"
                                 data-suffix=" МЛН"
                                 data-useconversion1="cost_real_estate"
                                ></div>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_line_title">
                            Срок
                        </div>
                        <div class="wr_card_to_card">
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-year type="text" class="simple_input big" id="year_calk" value="48">
                                </div>
                                <div class="complex_input_block" data-yearsklonenie style="width: 155px;">месяцев</div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="year_calk"
                                 data-max="84"
                                 data-min="12"
                                 data-value="48"
                                 data-stepdel=12
                                 data-stepdelh=24
                                ></div>
                        </div>
                    </div>
                </div>
                <div class="form_application_line">
                    <label><input data-kasko class="formstyle" type="checkbox" name="kasko" value="Y" />Оформлю полис КАСКО</label>
                </div>

                <div style="display: none;" class="form_application_line">
                    <label><input data-withmileage class="formstyle" type="checkbox" name="withmileage" value="Y" />с пробегом</label>
                </div>
            </form>
        </div>
        <div class="block_type_right">
            <div class="right_top_line">
                <div class="text_block right_bank_block right_bank_summ">
                    <div class="right_bank_text">
                        Размер кредита
                    </div>
                    <div class="right_bank_summ_summ big">
                        <span data-print-summ>17 560 000</span> <span class="rub">₽</span>
                    </div>
                    <div class="wr_right_bank_summ_block">
                        <div class="right_bank_summ_block">
                            <div class="right_bank_text">
                                Ежемесячный платеж
                            </div>
                            <div class="right_bank_summ_summ">
                                <span data-print-monf>44 433</span> <span class="rub">₽</span>
                            </div>
                        </div><div class="right_bank_summ_block">
                            <div class="right_bank_text">
                                ставка
                            </div>
                            <div class="right_bank_summ_summ">
                                <span data-print-rate>10,5</span>%
                            </div>
                        </div>
                    </div>
                    <a href="#" class="button"
                       data-yourapplication=""
                       data-classapplication=".wr_form_application"
                       data-formrapplication=".content_rates_tabs"
                       data-formrapplicationindex="0">Оставить заявку</a>
                </div>
                <div class="right_bank_title" style="text-align: center;">
                    Данный расчет носит справочный характер и не является офертой
                </div>
            </div>
        </div>
    </div>
</div>