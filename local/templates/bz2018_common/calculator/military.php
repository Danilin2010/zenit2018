<div class="wr_block_type gray calculator ">
    <div class="block_type to_column c-container">
        <h1>Военная ипотека</h1>
        <div class="block_type_center">
            <form action="" class="calculator_mortgage">
                <div class="form_application">
                    <?if($programm){?>
                        <input type="hidden" data-programm name="programm" value="<?=$programm?>"/>
                    <?}else{?>
                        <div class="form_application_line">
                            <select data-programm data-plasholder="Выберите программу" data-title="Программа кредитования" class="formstyle" name="programm">
                                <option value="primaryFamily" selected>Первичка(семейн.)</option>
                                <option value="secondaryFamily">Вторичка(семейн.)</option>
                                <option value="primaryStd">Первичка (стд.)</option>
                                <option value="secondaryStd">Вторичка (стд.)</option>
                            </select>
                        </div>
                    <?}?>
                    <div class="form_application_line">
                        <div class="form_application_line_title">
                            Ваш возраст
                        </div>
                        <div class="wr_card_to_card">
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-year type="text" class="simple_input big" id="year_calk" value="35">
                                </div>
                                <div class="complex_input_block">лет</div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="year_calk"
                                 data-useconversion="Y"
                                 data-max="40"
                                 data-min="23"
                                 data-value="35"
                                ></div>
                        </div>
                    </div>

                    <div class="form_application_line">
                        <div class="form_application_line_title">
                            Размер собственных средств
                        </div>
                        <div class="wr_card_to_card">
                            <div class="wr_complex_input big">
                                <div class="complex_input_body">
                                    <input data-summ type="text" class="simple_input big" id="main_ptop" value="1000000">
                                </div>
                                <div class="complex_input_block summ"></div>
                            </div>
                            <div class="card_to_card_select"
                                 data-target="main_ptop"
                                 data-max="2000000"
                                 data-min="0"
                                 data-value="1000000"
                                 data-suffix=" МЛН"
                                 data-del="1000000"
                                ></div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div class="block_type_right">
            <div class="right_top_line">
                <div class="text_block right_bank_block right_bank_summ">
                    <div class="right_bank_text">
                        Размер кредита
                    </div>
                    <div class="right_bank_summ_summ big">
                        <span data-print-summ></span> <span class="rub">₽</span>
                    </div>
                    <div class="wr_right_bank_summ_block">
                        <div class="right_bank_summ_block">
                            <div class="right_bank_text">
                                Ежемесячный платеж
                            </div>
                            <div class="right_bank_summ_summ">
                                <span data-print-monf></span> <span class="rub">₽</span>
                            </div>
                        </div><div class="right_bank_summ_block">
                            <div class="right_bank_text">
                                ставка
                            </div>
                            <div class="right_bank_summ_summ">
                                <span data-print-rate></span>%
                            </div>
                        </div>
                    </div>
                    <a href="#" class="button"
                       data-yourapplication=""
                       data-classapplication=".wr_form_application"
                       data-formrapplication=".content_rates_tabs"
                       data-formrapplicationindex="0">Оставить заявку</a>
                </div>
                <div class="right_bank_title" style="text-align: center;">
                    Данный расчет носит справочный характер и не является офертой
                </div>
            </div>
        </div>
    </div>
</div>