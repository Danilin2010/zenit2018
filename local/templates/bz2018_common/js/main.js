$(document).ready(function(){

    /**faq**/
    $('.faq .faq_top').on('click',function(){
        var item=$(this).parent('.faq_item');
        var list=item.parent('.faq').children('.faq_item');
        if(item.hasClass('open'))
        {
            item.removeClass('open');
            item.find('.faq_text').stop().slideUp('fast');
        }else{
            list.removeClass('open');
            item.addClass('open');
            list.find('.faq_text').stop().slideUp('fast');
            item.find('.faq_text').stop().slideDown('fast');
        }
        return false;
    });
    /**faq**/


    $('.button_select_top_menu').on('click',function(e){
        if ($(e.target).closest(".button_select_top_menu ul").length) return;
        if($('.button_select_top_menu>ul').hasClass('open'))
        {
            $('.button_select_top_menu>ul').removeClass('open');
            $('.button_select_top_menu>ul').stop().slideUp('fast');
        }else{
            $('.button_select_top_menu>ul').addClass('open');
            $('.button_select_top_menu>ul').stop().slideDown('fast');
        }
    });

    $(document).click(function(e){
        if ($(e.target).closest(".button_select_top_menu").length) return;
        $('.button_select_top_menu>ul').removeClass('open');
        $('.button_select_top_menu>ul').stop().slideUp('fast');
    });


    $('.mobail_min_menu .menu').on('click',function(){
        $('.mobail_show_menu').addClass('open');
        $('.mobail_bottom_menu').addClass('open');
        var select=$('.mobail_bottom_menu .select');
        if(select.length==1)
        {
            var show=select.data('show');
            if(typeof(show)!='undefined')
            {
                ShowButtonMenu(select[0]);
            }
        }
        jQuery(".glyphicon-menu-down").slideUp("100");
        //$('.wr_block_search input[type=text]').focus();
        return false;
    });

    $('.mobail_show_menu .mobail_show_menu_close').on('click',function(){
        $('.mobail_show_menu').removeClass('open');
        $('.mobail_bottom_menu').removeClass('open');
        $('.mobail_show_menu input').val('');
        $('.header>.all_products, .header>.net_bank, .header .top_menu a').removeClass('open');
        $('.wr_menu_bottom').stop().slideUp('fast');
        $("body").removeClass("modal-open-top").css('padding-right', '0px');
        //$('.ul_mobail_bottom_menu a').removeClass('select');
        $('.wr_menu_bottom').stop().slideUp('fast');
        $('.wr_block_search_result').find('.block_search_result').html('');
        $('.wr_block_search_result').slideUp('fast');
        return false;
    });

    $(document).click(function(e){
        if ($(e.target).closest(".wr_header").length) return;
        $('.mobail_show_menu').removeClass('open');
        $('.mobail_bottom_menu').removeClass('open');
        $('.wr_mobail_min_menu .wr_block_search_result').find('.block_search_result').html('');
        $('.wr_mobail_min_menu .wr_block_search_result').slideUp('fast');
        e.stopPropagation();
    });



    $('.wr_top_line').on('click', '.search',function(e){
        if ($(e.target).closest(".wr_block_search_result").length) return;
        $('.wr_block_search').addClass('open');
        $('.wr_block_search input[type=text]').focus();
        return false;
    });

    $('.wr_block_search .block_search_close').on('click',function(){
        $('.wr_block_search').removeClass('open');
        $('.wr_block_search input[type=text]').val('');
        $('.wr_block_search_result').find('.block_search_result').html('');
        $('.wr_block_search_result').slideUp('fast');
        return false;
    });

    function ShowButtonMenu(_this)
    {
        var wh = window.innerWidth; // вся ширина окна
        var whs = document.documentElement.clientWidth; // ширина минус прокрутка
        var pls = (wh - whs); // вычисляем ширину прокрутки и даем ей имя pls

        var show=$(_this).data('show');
        if(!$(_this).hasClass('open'))
        {

            if($(window).width()<=992)
                $("body").addClass("modal-open-top").css('padding-right', pls + 'px');


            $('.header>.all_products, .header>.net_bank, .header .top_menu a').removeClass('open');
            $(this).addClass('open');
            $('.wr_menu_bottom').stop().slideUp('fast');
            $('#'+show).stop().slideDown('fast');
        }else{
            $("body").removeClass("modal-open-top").css('padding-right', '0px');
            $('.header>.all_products, .header>.net_bank, .header .top_menu a').removeClass('open');
            $('.wr_menu_bottom').stop().slideUp('fast');
        }
    }
    function hideButtonMenu() {
        $('.wr_menu_bottom').stop().slideUp();
        //$("body").removeClass("modal-open-top").css('padding-right', '0px');
        $('.header>.all_products, .header>.net_bank, .header .top_menu a').removeClass('select');
        $('.wr_block_search_result').find('.block_search_result').html('');
        $('.wr_block_search_result').slideUp('fast');
    }

    $('.mobail_bottom_menu a[data-show]').on('click',function(e){
        $('.ul_mobail_bottom_menu a').removeClass('select');
        $(this).addClass('select');
        ShowButtonMenu(this);
        return false;
    });

    $('.mobail_bottom_menu a').not('[data-show]').on('click',function(e){
        $('.ul_mobail_bottom_menu a').removeClass('select');
        $(this).addClass('select');
        //return false;
    });

    $('[data-show]').not('.mobail_bottom_menu a').on('click',function(){
        if (!$(this).hasClass('select')) {
            $('[data-show]').not('.mobail_bottom_menu a').each(function () {
                $(this).removeClass('select')
            });
            $(this).addClass('select');
            ShowButtonMenu(this);
        }
        else {
            hideButtonMenu()
        }


        return false;
    });

  $('.burger_menu_menu span').parent('li').on('click', function (e) {
    if ($(e.target).closest('[href]').length > 0) return true;
    if ($(window).width() > 900) {

      if (!$(this).hasClass('open')) {
        // $('.burger_menu_menu span').parent('li').removeClass('open');
        // $('.burger_menu_menu span').parent('li').find('ul').stop().slideUp('fast');
        $(this).addClass('open');
        $(this).find('ul').eq(0).stop().slideDown('fast');
      } else if (!$(this).children('ul').length) {
        $(this).removeClass('open');
        $(this).find('ul').eq(0).stop().slideUp('fast');
      } else {
        $(this).removeClass('open');
        $(this).find('ul').eq(0).stop().slideUp('fast');
      }
    }
    return false;
  });

    $(document).on('click', function(e){
        if ($(e.target).closest(".wr_menu_bottom").length) return;
            hideButtonMenu()
    });

    $(".exchange_rates_tabs,.content_rates_tabs").tabs({
        beforeActivate: function(event,ui){
            var settime=ui.newTab.data('settime');
            if(typeof (settime)!='undefined')
                $('[data-totime=Y]').text(settime);
        },
        create: function( event, ui ) {
            $(ui.tab).parent().wrap("<div class='content_tab_wrapper'></div>");
        }
    });

    $('.formstyle').styler({
        onFormStyled: function(){
            $('select.formstyle').each(function(){
                var title=$(this).data('title');
                if(typeof (title)!='undefined')
                    $(this).next('.jq-selectbox__select').prepend('<div class="select_title">'+title+'</div>');
                else
                    $(this).next('.jq-selectbox__select').prepend('<div class="select_title"></div>');
            });
        }
    });



    /*$('.wr_footer_banner .footer_banner_car').scroolly([
        {
            to: 'el-bottom = vp-top',
            onScroll: function(element, offset, length){
                length=$(document).height();
                offset=offset+$(window).height();
                var top=element.position().top;
                var progress = (offset-top) / (length-top);
                if(progress<0)
                    progress=0;
                element.css('background-position', $.scroolly.getTransitionFloatValue(0, 100, progress)+'%'+' center');
            }
        }
    ]);*/

    $('.wr_block_type .block_type.to_column').each(function(e){
        var block_type_right=$(this).find('.block_type_right .wr_form_block').height();
        var element_height=$(this).height();
        if(block_type_right<element_height && block_type_right<$(window).height() && $(window).height()<=400)
        {
            $(this).scroolly([
                {
                    to: 'el-top = top',
                    class: 'no-fix'
                },
                {
                    from: 'el-top = top+147px',
                    //from: 'el-top = top+0px',
                    to: 'el-bottom = vp-top',
                    class: 'fix',
                    onScroll: function (element, offset, length) {
                        var block_type_right=element.find('.block_type_right .wr_form_block').height();
                        var element_height=element.height();
                        var delta=160;
                        element.parent('.wr_block_type').css('min-height',(block_type_right+delta)+'px');
                        if(block_type_right<element_height && block_type_right<$(window).height() && (length-block_type_right-delta)<offset)
                            element.addClass('bottom_fix');
                        else
                            element.removeClass('bottom_fix');
                    }
                }
            ]);
        }
    });





    $('.wr_fix_top').scroolly([
        {
            from: 'doc-top',
            to: 'el-bottom=top',
            class: 'no_fix',
            onScroll: function (element, offset, length) {
                var topEl=$('.main_wrapper').position().top;
                var delta=0;
                if(topEl>0)
                    delta=146;
                if(parseInt(offset)<=delta)
                {
                    element.removeClass('fix');
                    element.removeClass('no_fix_res');
                    element.addClass('no_fix');
                }
            }
        },
        {
            from: 'el-bottom=top',
            to: 'el-bottom=top-150px',
            class: 'no_fix_res'
        },
        {
            from: 'el-bottom=top-150px',
            to:'doc-bottom+150px',
            //class: 'fix',
            onTopIn: function (element) {
                element.removeClass('fix');
                element.addClass('no_fix_res');
                element.removeClass('no_fix');
            },
            onDirectionChanged: function(element, direction){
                if(direction<0)
                {
                    element.addClass('fix');
                    element.removeClass('no_fix_res');
                    element.removeClass('no_fix');
                }else{
                    element.removeClass('fix');
                    element.addClass('no_fix_res');
                    element.removeClass('no_fix');
                }
            }
        }
    ],$('.fix_top'));

    $('.complex_input_body').on('click',function(e){
        if ($(e.target).closest("input").length) return;
        $(this).find("input").focus();
    });

    $('.complex_input_body .simple_input').on('focus',function(e){
        var parent=$(this).parents('.wr_complex_input');
        parent.addClass('focus use');
    }).on('blur',function(e){
        var parent=$(this).parents('.wr_complex_input');
        parent.removeClass('focus');
        var val=$(this).val();
        if(val.length<=0)
            parent.removeClass('use');
    });

    $('.complex_input_body .simple_input').on('change',function(e){
        var parent=$(this).parents('.wr_complex_input');
        var val=$(this).val();
        if(val.length<=0)
            parent.removeClass('use');
        else
            parent.addClass('use');
    });

    $('[data-yourapplication]').on('click',function(e){
        e.preventDefault();
        var classapplication=$(this).data('classapplication'),
        formrapplication=$(this).data('formrapplication'),
        formrapplicationindex=$(this).data('formrapplicationindex'),
        scroll_el = $(classapplication);
        if(typeof (formrapplication)!='undefined' && typeof (formrapplicationindex)!='undefined')
            $(formrapplication).tabs( "option", "active", formrapplicationindex);
        if ($(scroll_el).length != 0)
            $('html, body').animate({scrollTop:$(scroll_el).offset().top},500);
    });

    function SetMinTable() {
        var width=$(window).width()-40;
        $('table.min_table tr').css('width',width+'px');
    }
    $( window ).resize(function() {
        SetMinTable();
    });
    SetMinTable();

    $('.div_mini_filter_title').on('click',function(e){
        var parent=$(this).parent();
        parent.toggleClass('open');
    });


    $(".ul_mobail_bottom_menu").smoothDivScroll({
        touchScrolling: true,
        manualContinuousScrolling: false,
        hotSpotScrolling: false,
        mousewheelScrolling: false
    });

    /**Валидация электронной почты**/
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };

    jQuery.validator.addMethod("email2", function(value, element) {
        return this.optional(element) || isValidEmailAddress(value);
    }, "Укажите правильный e-mail");


    function checkINN(inputNumber){
        //преобразуем в строку
        inputNumber = "" + inputNumber;
        //преобразуем в массив
        inputNumber = inputNumber.split('');
        //для ИНН в 10 знаков
        if((inputNumber.length == 10) && (inputNumber[9] == ((2 * inputNumber[  0] + 4 * inputNumber[1] + 10 * inputNumber[2] + 3 * inputNumber[3] + 5 * inputNumber[4] + 9 * inputNumber[5] + 4 * inputNumber[6] + 6 * inputNumber[7] + 8 * inputNumber[8]) % 11) % 10)){
            return true;
            //для ИНН в 12 знаков
        }else if((inputNumber.length == 12) && ((inputNumber[10] == ((7 * inputNumber[ 0] + 2 * inputNumber[1] + 4 * inputNumber[2] + 10 * inputNumber[3] + 3 * inputNumber[4] + 5 * inputNumber[5] + 9 * inputNumber[6] + 4 * inputNumber[7] + 6 * inputNumber[8] + 8 * inputNumber[9]) % 11) % 10) && (inputNumber[11] == ((3 * inputNumber[ 0] + 7 * inputNumber[1] + 2 * inputNumber[2] + 4 * inputNumber[3] + 10 * inputNumber[4] + 3 * inputNumber[5] + 5 * inputNumber[6] + 9 * inputNumber[7] + 4 * inputNumber[8] + 6 * inputNumber[9] + 8 * inputNumber[10]) % 11) % 10))){
            return true;
        }else{
            return false;
        }
    }

    jQuery.validator.addMethod("inn", function(value, element) {
        return this.optional(element) || checkINN(value);
    }, "Введите корректный ИНН");

});
