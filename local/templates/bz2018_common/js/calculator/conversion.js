(function( $ ) {
    $.conversion = function(options) {
        this.settings = $.extend({
            min:0,
            max:30000000,
            delta:1,
            grid:[{val:5000000,percent:50},{val:15000000,percent:75}]
        }, options);
        this.reInit= function(options){
            this.settings = $.extend(this.settings,options);
            this.init();
        };
        this.chekValue=function(A,K) {
            return A.reduce(
                function(prev, item) {
                    return (!prev && item.val== K)?true:prev;
                },
                false
            );
        };
        this.comparePercent=function(percentA, percentB) {
            return percentA.percent - percentB.percent;
        };
        this.toSlider=function(val) {
            var res=0;
            $.each(this.segments,function(key,value){
                if(val>=value.minVval && val<=value.maxVval)
                    res=((100*((val-value.minVval)/(value.maxVval-value.minVval)))*(value.maxPercent-value.minPercent)/100)+value.minPercent;
            });
            if(res==0 && this.segments.length>0 && val>this.segments[this.segments.length-1].maxVval)
                res=100;
            res=res*this.settings.delta;
            return res;
        };
        this.fromSlider=function(percent) {
            percent=percent/this.settings.delta;
            var res=0;
            $.each(this.segments,function(key,value){
                if(percent>=value.minPercent && percent<=value.maxPercent)
                    res=((value.maxVval-value.minVval)*((100*(percent-value.minPercent)/(value.maxPercent-value.minPercent))/100))+value.minVval;
            });
            return res;
        };
        this.initSegments=function(){
            this.segments=[];
            if(this.grid.length>1)
            {
                for(var i=1;i<this.grid.length;i++)
                {
                    this.segments[this.segments.length]={
                        minPercent:this.grid[i-1].percent,
                        maxPercent:this.grid[i].percent,
                        minVval:this.grid[i-1].val,
                        maxVval:this.grid[i].val
                    };
                }
            }
        };
        this.init=function(){
            var grid=[];
            var max=this.settings.max,
                min=this.settings.min;
            $.each(this.settings.grid,function(key,value){
                if(value.val<=max && value.val>=min)
                    grid[grid.length]=value;
            });
            this.grid=grid;
            if(!this.chekValue(this.grid,this.settings.min))
                this.grid[this.grid.length]={val:this.settings.min,percent:0};
            if(!this.chekValue(this.grid,this.settings.max))
                this.grid[this.grid.length]={val:this.settings.max,percent:100};
            var used = {};
            this.grid = this.grid.filter(function(obj){
                return obj.percent in used ? 0:(used[obj.percent]=1);
            });
            this.grid.sort(this.comparePercent);
            this.initSegments();
        };
        this.init();
    };
})(jQuery);