<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global \CMain $APPLICATION */
/** @global \CUser $USER */
/** @global \CDatabase $DB */
/** @var \CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var \CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (!empty($arResult['SECTIONS'])) { ?>
<ul<?= !empty($arParams['CSS_CLASS']) ? ' class="' . $arParams['CSS_CLASS'] . '"' : '' ?>>
<?php foreach ($arResult['SECTIONS'] as $arItem) { ?>
	<li>
		<?= \helpers\Helper::a($arItem['SECTION_PAGE_URL'], $arItem["NAME"])?>
	</li>
<?php } ?>
</ul>
<?php } ?>