<?
$newArr=array();

foreach($arResult["ITEMS"] as &$item){

    $date=$item["IBLOCK_SECTION_ID"];
    if(!$newArr[$date])
        $newArr[$date]=array();

    $item["FILE"]=GetFile($item["NAME"],$item["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]);


    $item["CHANGES_CHARTER"]=array();
    $arSelect = Array("ID", "NAME","PROPERTY_FILE");
    $arFilter = Array("IBLOCK_ID"=>135,"ACTIVE"=>"Y","PROPERTY_CHANGES_CHARTER"=>$item["ID"]);
    $res = CIBlockElement::GetList(Array("DATE_ACTIVE_FROM"=>"DESC"), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNext())
    {
        $ob["FILE"]=CFile::GetByID($ob["PROPERTY_FILE_VALUE"])->Fetch();
        $ob["FILE"]["SRC"]=CFile::GetPath($ob["PROPERTY_FILE_VALUE"]);
        $ob["FILE"]=GetFile($ob["NAME"],$ob["FILE"]);
        $item["CHANGES_CHARTER"][]=$ob;
    }
    $newArr[$date][]=$item;
}unset($item);

$arResult["NEW_ITEMS"]=$newArr;

$arSections = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y');
$rsSect = CIBlockSection::GetList(array(), $arSections);

while ($arSect = $rsSect->GetNext()) {
    $sections[$arSect["ID"]] = $arSect["NAME"];
}
$arResult['SECTIONS']=$sections;

$reporting = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "CODE"=>"REPORTING_TYPE"));
$rep = array();
while($enum_fields = $reporting->GetNext())

{
    $rep[$enum_fields["ID"]] = $enum_fields["VALUE"];
}
$arResult["REPORTINGTYPE"]= $rep;

?>

<!--?echo "<pre>";print_r($arResult["ITEMS"]);echo "</pre>";?-->