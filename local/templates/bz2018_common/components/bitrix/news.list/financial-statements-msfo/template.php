<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<pre><?//print_r($arResult["NEW_ITEMS"])?></pre>
<div class="content_body pt-0" id="content-tabs-2">
    <div class="preview-content-filtr pt-4">
        <div class="faq">
            <?$i=0;?>
                <?foreach($arResult["NEW_ITEMS"] as $key=>$NewItems){?>
                    <div class="faq_item <?if($i<=0){?>open<?}?>">
                        <div class="faq_top">
                            <div class="faq_pict">
                                <div class="faq_arr">
                                </div>
                            </div>
                            <div class="faq_top_text">
                                <?=$arResult['SECTIONS'][$key]?> год
                            </div>
                        </div>
                        <div class="faq_text" <?if($i<=0){?>style="display: block;"<?}?>>
                            <?foreach($NewItems as $arItem){?>
                                <?
                                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                ?>
                                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="contrnt-otchet">
                                    <? if (!empty($arItem["DISPLAY_PROPERTIES"]["DATA"])) :?>
                                        <div class="data">
                                            Дата предоставления отчетности в Банк России: <?=$arItem["DISPLAY_PROPERTIES"]["DATA"]["VALUE"]?>
                                        </div>
                                    <?endif;?>
                                    <? if (!empty($arItem["DISPLAY_PROPERTIES"]["DATA_PUBLICATION"])) :?>
                                        <div class="data">
                                            Дата размещения отчетности на сайте: <?=$arItem["DISPLAY_PROPERTIES"]["DATA_PUBLICATION"]["VALUE"]?>
                                        </div>
                                    <?endif;?>
                                    <div class="data">
                                        <?=$arItem["DISPLAY_PROPERTIES"]["SHORT_DESCRIPTION"]["DEFAULT_VALUE"]?>
                                    </div>
                                    <div class="doc_list">
                                        <a href="<?=$arItem["FILE"]["SRC"]?>" class="doc_item" target="_blank">
                                            <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                                            </div>
                                            <div class="doc_body">
                                                <div class="doc_text">
                                                    <?=$arItem["NAME"]?>
                                                </div>
                                                <div class="doc_note">
                                                    <?=$arItem["FILE"]["FILE_SIZE"]?>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                <?$i++;?>
            <?}?>
        </div>
    </div>
</div>
