<?
$newArr=array();

foreach($arResult["ITEMS"] as $item){

    $date=$item["IBLOCK_SECTION_ID"];
    if(!$newArr[$date])
        $newArr[$date]=array();

    $item["FILE"]=GetFile($item["NAME"],$item["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]);
    $newArr[$date][]=$item;

}
$arResult["NEW_ITEMS"]=$newArr;


$arSections = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y');
$rsSect = CIBlockSection::GetList(array(), $arSections);
while ($arSect = $rsSect->GetNext()) {
    $sections[$arSect["ID"]] = $arSect["NAME"];
}
$arResult['SECTIONS']=$sections;

?>