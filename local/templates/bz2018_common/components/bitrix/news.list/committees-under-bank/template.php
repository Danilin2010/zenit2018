<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container about-page">
    <div class="row">
        <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <!-- Содержимое -->
        <div class="col-sm-12">
            <div class="col-sm-12 col-mt-0 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 main-content-top">
                        <div class="block-card note_text">
                            <p>
                                Совет директоров Банка вправе создавать временные и постоянные комитеты для предварительного изучения и рассмотрения наиболее важных вопросов, относящихся к компетенции Совета директоров Банка. Комитеты при Совете директоров Банка действуют на основании положений, утвержденных Советом директоров Банка. Персональные составы комитетов формируются Советом директоров Банка. Комитеты являются консультативными органами Совета директоров Банка, которые возглавляются одним из членов Совета директоров Банка.
                            </p>
                            <p>
                                При Совете директоров ПАО Банк ЗЕНИТ действуют следующие постоянные комитеты: <strong>Комитет по стратегическому планированию, Комитет по аудиту, Комитет по кадрам и вознаграждениям</strong>.
                            </p>
                        </div>
                    </div>
                    <?foreach($arResult["ITEMS"] as $arItem):?>
                        <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        ?>
                        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-sm-12 col-mb-pr-0 col-mt-pr-0 col-mt-0 pr-4">
                            <div class="block-card-no block-card note_text pt-3">
                                <h3><?=$arItem["NAME"]?></h3>
                                <?=$arItem["DETAIL_TEXT"]?>
                                <div class="doc_list">
                                    <a class="doc_item" href="<?=$arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>" target="_blank">
                                        <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                                        </div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                <?=$arItem["DISPLAY_PROPERTIES"]["FILE_NAME"]["VALUE"]?>
                                            </div>
                                            <div class="doc_note">
                                                <?=$arItem["FILE"]["FILE_SIZE"]?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?endforeach;?>
                </div>
            </div>
        </div>
        <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>
