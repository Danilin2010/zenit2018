<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem){
    $slidersData[$arItem['ID']] = array(
        'PREVIEW_TEXT' => $arItem["PREVIEW_TEXT"], // заголовок слайда
        'DETAIL_TEXT' => $arItem["DETAIL_TEXT"], // подзаголовок слайда
        'IMAGE' => $arItem["PREVIEW_PICTURE"]["SRC"], // картинка слайда
        'LINK' => $arItem["PROPERTIES"]["LINK"]["VALUE"], // ссылка. Используем, если слайд не входит в подборщик
        'START_ID' => $arItem["PROPERTIES"]["FIRST_STEP_ID"]["VALUE"], // ID первого шага
        'BUTTON_TEXT' => $arItem["PROPERTIES"]["BUTTON_TEXT"]["VALUE"] // Текст кнопки
    );
}

$slidersData = json_encode($slidersData);
/*?>

<!--main_slider-->
<div class="wr_main_slider"><div class="main_slider">
        <? $slidersData = array();
        foreach($arResult["ITEMS"] as $arItem){
            $slidersData[$arItem['ID']] = array(
                ['PREVIEW_TEXT'] => $arItem["PREVIEW_TEXT"],
                ['DETAIL_TEXT'] => $arItem["DETAIL_TEXT"],
                ['IMAGE'] => $arItem["PREVIEW_PICTURE"]["SRC"]
            )
            ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="main_slider_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="main_slider_item_wr z-container">
                <div class="left">
                    <div class="title">
                        <?echo $arItem["PREVIEW_TEXT"];?>
                    </div>
                    <div class="text">
                        <?echo $arItem["DETAIL_TEXT"];?>
                    </div>
                    <a class="button" href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                        <?=($arItem["PROPERTIES"]["BUTTON_TEXT"]["VALUE"])? $arItem["PROPERTIES"]["BUTTON_TEXT"]["VALUE"]:GetMessage('BUTTON_READ_MORE')?>
                        </a>
                </div>
                <div class="pict" style="
                background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');
                width: <?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>px;
                height: <?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>px;
                margin-left: -<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]/2?>px;
                "></div>
                <div class="right">
                    <div class="precent"><?if($arItem["DISPLAY_PROPERTIES"]["SUFFIX"]["VALUE"]){?><span><?=$arItem["DISPLAY_PROPERTIES"]["SUFFIX"]["VALUE"]?></span> <?}?><?=$arItem["DISPLAY_PROPERTIES"]["PERCENT"]["VALUE"]?></div>
                </div>
            </div>
        </div>
        <?}?>
        <div class="wr_main_slider_wrapper_naw"><div class="main_slider_wrapper_naw"></div></div>
    </div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "page",
            "EDIT_TEMPLATE" => "",
            "AREA_FILE_SUFFIX" => "before_footer",
        ),
        false,
        array('HIDE_ICONS' => 'Y')
    );?>
</div>
<!--main_slider-->
*/

?>
<!--<link href=/local/templates/bz/components/bitrix/news.list/main_selection/static/css/app.css?--><?//=md5_file('local/templates/bz/components/bitrix/news.list/main_selection/static/css/app.css');?><!-- rel=stylesheet>-->
<div id=app></div>
<script>
  var sliderData = <?=$slidersData;?>
</script>
<script type=text/javascript src=/local/templates/bz/components/bitrix/news.list/main_selection/static/js/manifest.js></script>
<script type=text/javascript src=/local/templates/bz/components/bitrix/news.list/main_selection/static/js/vendor.js></script>
<script type=text/javascript src=/local/templates/bz/components/bitrix/news.list/main_selection/static/js/app.js></script>
<style>

    /* TODO: удалить из разметки */
    .clouds_one, .clouds_two, .clouds_three {
        display: none;
    }
</style>
