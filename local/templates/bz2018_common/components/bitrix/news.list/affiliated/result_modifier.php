<?
foreach($arResult["ITEMS"] as &$item) {
    $item["FILE"]=GetFile($item["NAME"],$item["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]);

    $item["CHILDREN"]=array();
    $arSelect = Array("ID", "NAME", "PROPERTY_DATE","PROPERTY_FILE");
    $arFilter = Array("IBLOCK_ID"=>86,"ACTIVE"=>"Y","PROPERTY_PARENT"=>$item["ID"]);
    $res = CIBlockElement::GetList(Array("PROPERTY_DATA"=>"ASC"), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNext())
    {
        $ob["FILE"]=CFile::GetByID($ob["PROPERTY_FILE_VALUE"])->Fetch();
        $ob["FILE"]["SRC"]=CFile::GetPath($ob["PROPERTY_FILE_VALUE"]);
        $ob["FILE"]=GetFile($ob["NAME"],$ob["FILE"]);
        $item["CHILDREN"][]=$ob;
    }

    }unset($item);
?>


<!--?echo "<pre>";print_r($arResult["ITEMS"]);echo "</pre>";?-->
