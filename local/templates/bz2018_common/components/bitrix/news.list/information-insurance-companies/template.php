<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <!--div class="content_body pt-0" id="content-tabs-3">
            <span id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="issue-bonds"-->
<div class="wr_block_type">
    <div class="block_type to_column c-container">
        <div class="block_type_right">
                            <h2>Связаться с банком</h2>
                            <div class="form_application_line">
                                <div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
                                     +7 (495) 967-11-11<br>
                                     8 (800) 500-66-77
                                </div>
                                <div class="note_text">
                                     звонок по России бесплатный
                                </div>
                            </div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
        </div>
        <div class="block_type_center">
            <div class="text_block">
                <?=$arItem["DETAIL_TEXT"]?>
            </div>
        </div>
    </div>
</div>
            <!--/span>
    </div-->
<?endforeach;?>