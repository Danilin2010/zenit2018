<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? //echo '<pre>' . print_r($arResult["ITEMS"], 1) . '</pre>';?>
<?foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?><div class="ipoteka_item">
		<div class="wr_ipoteka_item">
			<div class="wr_pict_ipoteka_item" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></div>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="ipoteka_item_title">
				<?=$arItem["NAME"]?>
			</a>
			<div class="ipoteka_item_snippets">
				<div class="ipoteka_item_snippet">
					<div class="ipoteka_item_snippet_top">
						до <?=GetPrettyNumber(trim($arItem["DISPLAY_PROPERTIES"]["MAX_PRICE"]["VALUE"]))?><span class="rub">₽</span>
					</div>
					<div class="ipoteka_item_snippet_bottom">
						сумма кредита
					</div>
				</div>
				<div class="ipoteka_item_snippet">
					<div class="ipoteka_item_snippet_top">
						до <span class="numer"><?=$arItem["DISPLAY_PROPERTIES"]["MAX_TERM_MORTGAGE"]["VALUE"]?></span> <?=sklonenie(intval($arItem["DISPLAY_PROPERTIES"]["MAX_TERM_MORTGAGE"]["VALUE"]), array('года', 'года', 'лет'))?>
					</div>
					<div class="ipoteka_item_snippet_bottom">
						срок кредита
					</div>
				</div>
			</div>
			<div class="ipoteka_item_text">
				<?=$arItem["PREVIEW_TEXT"]?>
			</div>
		</div>
	</div><? } ?>

