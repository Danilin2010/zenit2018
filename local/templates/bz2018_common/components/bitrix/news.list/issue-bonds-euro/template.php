<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (!empty($arResult['ITEMS'])) { ?>
	<?php foreach ($arResult['ITEMS'] as $i => $item) { ?>
		<?php
		$arFile = GetFile($item['FIELDS']['NAME'], $item['DISPLAY_PROPERTIES']['FILE']['FILE_VALUE']);
		?>
		<div class="doc_list">
			<a href="<?= $arFile['SRC'] ?>" class="doc_item" target="_blank">
				<div class="doc_pict <?= $arFile['TYPE'] ?>">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						<?= $arFile['NAME'] ?>
					</div>
					<div class="doc_note">
						<?= $arFile['FILE_SIZE'] ?>
					</div>
				</div>
			</a>
		</div>
	<?php } ?>
<?php } ?>