<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="container about-page">
    <div class="row">
        <!-- Правый блок мобилка и планшет -->
        <div class="col-sm-12 col-md-4 hidden-lg">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <!-- Содержимое -->
        <div class="col-sm-12 col-mt-0 col-md-8">
            <div class="row">
                <div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mb-5 main-content-top">
                    <div class="preview-card note_text">
                        <div class="preview-content-filtr">
                            <div class="jq-selectbox jqselect formstyle bank">
                                <form method="get" class="resubmit">
                                    <select id="year-filter" data-programm="" data-plasholder="Укажите год публикации" data-title="Укажите год публикации" class="formstyle" name="year">
                                        <option value="all" selected="">Все годы</option>?>
                                        <?foreach($arResult['SECTIONS'] as $section){?>
                                            <option <?if($arResult['SECTIONS']==$section){?>selected<?}?> value="<?= $section['ID']?>"><?= $section['NAME']?></option>
                                        <?}?>
                                    </select>
                                </form>
                            </div>
                        </div>
                        <?foreach($arResult["ITEMS"] as $arItem):?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="preview-content">
                                <div class="data">
                                    <?=$arItem["DISPLAY_PROPERTIES"]["DATE"]["VALUE"]?>
                                </div>
                                <div class="doc_list">
                                    <a href="<?=$arItem["DISPLAY_PROPERTIES"]["FILE"]["FILE_VALUE"]["SRC"]?>" class="doc_item" target="_blank">
                                        <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>">
                                        </div>
                                        <div class="doc_body">
                                            <div class="doc_text">
                                                <?=$arItem["NAME"]?>
                                            </div>
                                            <div class="doc_note">
                                                <?=$arItem["FILE"]["FILE_SIZE"]?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                    <div class="ipoteka_list my-5">
                        <div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/about/disclosure/disclosure-information/information-risks/" class="ipoteka_item_title">
                                    Информация о рисках на консолидированной основе
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div><div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/about/disclosure/disclosure-information/information-rates/" class="ipoteka_item_title">
                                    Информация о ставках по вкладам для физических лиц
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div><div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/about/disclosure/disclosure-information/information-bank/" class="ipoteka_item_title">
                                    Информация об инструментах капитала Банка
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div><div class="ipoteka_item">
                            <div class="wr_ipoteka_item">
                                <div class="wr_pict_ipoteka_item" style="background-image: url('/local/templates/bz/img/f_snippet.png');"></div>
                                <a href="/about/disclosure/disclosure-information/information-capital/" class="ipoteka_item_title">
                                    Информация об инструментах капитала Банковской группы
                                </a>
                                <div class="ipoteka_item_text">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Правый блок для ПС -->
        <div class="col-md-4 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-sm-12">
                    <?$APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/inc/template/about_contact.php",
                        Array(),
                        Array("MODE"=>"txt","SHOW_BORDER"=>false)
                    );?>
                </div>
            </div>
        </div>
    </div>
</div>
