<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \helpers\DateTime2HR;
?>




    <?foreach($arResult["ITEMS"] as $i => $arItem):
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
			<a href="<?= !empty($arItem['DISPLAY_PROPERTIES']['link']['VALUE']) ? $arItem['DISPLAY_PROPERTIES']['link']['VALUE'] : $arItem["DETAIL_PAGE_URL"] ?>"<?= !empty($arItem['DISPLAY_PROPERTIES']['link']['VALUE']) ? ' target="_blank"' : '' ?> class="main_news_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="wr_main_news_item">
					<div class="data">
						<div class="num">
							<?if($arItem["DISPLAY_ACTIVE_FROM"]){
								echo date("j", strtotime($arItem["ACTIVE_FROM"]));
							}?>
						</div>
						<?if($arItem["DISPLAY_ACTIVE_FROM"]){
							echo DateTime2HR::DateLocalize($arItem["ACTIVE_FROM"]);
						}?>
					</div>
					<div class="text">
						<?echo $arItem["NAME"];?><?php /*<span class="more">...</span>*/?>
					</div>
				</div>
			</a>
	<? endforeach; ?>





    <?/*
    <div class="main_news_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="main_news_date">
            <?if($arItem["DISPLAY_ACTIVE_FROM"]){echo $arItem["DISPLAY_ACTIVE_FROM"];}?>
        </div>
        <div class="main_news_text">
            <?echo $arItem["PREVIEW_TEXT"];?>
        </div>
        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="main_tile_more"><?=GetMessage('BUTTON_READ_MORE')?><div class="ico tile_more"></div></a>
    </div><?if($i==0){?><div class="main_news_sep"></div><?}?><?
    }?>
     


<?endforeach;?>
	</div>
</div>*/?>