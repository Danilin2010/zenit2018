<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? $city = GetSityID();?>
<?foreach($arResult["SECTIONS"] as $section):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
			</div>
			<div class="block_type_center" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<h1><?=$section["NAME"]?></h1>
				<div class="text_block">
					<!--doc-->
					<div class="doc_list">
						<? foreach ($section["FILES"] as $file)
						{
							if (in_array($city, $file["CITY"]))
							{ ?><a href="<?= $file["SRC"] ?>" class="doc_item" target="_blank">
								<div class="doc_pict <?= $file["TYPE"] ?>"></div>
								<div class="doc_body">
									<div class="doc_text">
										<?= $file["NAME"] ?>
									</div>
									<div class="doc_note">
										<?= $file["FILE_SIZE"] ?>
									</div>
								</div>
								</a><?}
						}?>
					</div>
					<!--doc-->
				</div>
			</div>
		</div>
	</div>

<?endforeach;?>

