<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult["SECTION_EXT"] && $arParams["SHOW_TITLE"]=="Y"){?>
    <h1><?=$arResult["SECTION_EXT"]["NAME"]?></h1>
<?}?>
<!--promo_benefits-->
<div class="promo_benefits <?if($arParams["BIG_ELEMENT"]=="Y"){echo "big_element";}?>">
    <?foreach($arResult["ITEMS"] as $arItem){
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?><div class="promo_benefits_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="pict " <?if($arItem['PREVIEW_PICTURE']['SRC']){?>style="background-image: url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>');"<?}?>></div>
        <div class="body">
            <div class="title">
                <?=$arItem['~NAME']?>
            </div>
            <div class="text">
                <?echo $arItem["PREVIEW_TEXT"];?>
            </div>
        </div>
    </div><?}?>
</div>
<!--promo_benefits-->
