<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}

foreach ($arResult['ITEMS'] as &$item)
{
	$item['PARENT'] = [];
	$arSelect = [
		'ID',
		'NAME',
		'PROPERTY_FILE'
	];
	$arFilter = [
		'IBLOCK_ID' => 139,
		'ACTIVE' => 'Y',
		'PROPERTY_PARENT' => $item['ID']
	];
	$element = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
	while ($arItem = $element->GetNext())
	{
		$arItem['FILE'] = \CFile::GetByID($arItem['PROPERTY_FILE_VALUE'])->Fetch();
		$arItem['FILE']['SRC'] = \CFile::GetPath($arItem['PROPERTY_FILE_VALUE']);
		$arItem['FILE'] = GetFile($arItem['NAME'], $arItem['FILE']);
		$item['PARENT'][] = $arItem;
	}
}

unset($item);
