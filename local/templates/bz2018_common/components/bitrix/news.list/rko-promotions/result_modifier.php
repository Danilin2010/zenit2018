<?php
foreach ($arResult["ITEMS"] as $k => $item)
{
	$arResult["PROMOTIONS"][$k]["ID"] = $item["ID"];
	$arResult["PROMOTIONS"][$k]["NAME"] = $item["NAME"];
	$arResult["PROMOTIONS"][$k]["PREVIEW_TEXT"] = $item["PREVIEW_TEXT"];
	$arResult["PROMOTIONS"][$k]["STEPS"] = getSteps($item["ID"]);
	$arResult["PROMOTIONS"][$k]["open_account"] = $item["PROPERTIES"]["open_account"]["VALUE"];
	$arResult["PROMOTIONS"][$k]["region"] = $item["PROPERTIES"]["region"]["VALUE"];
	$arResult["PROMOTIONS"][$k]["service_to_cb_system"] = $item["PROPERTIES"]["service_to_cb_system"]["VALUE"];
	$arResult["PROMOTIONS"][$k]["connection_to_cb_system"] = $item["PROPERTIES"]["connection_to_cb_system"]["VALUE"];
}
function getSteps($PROP_ID)
{
	$step = "";
	$arSelect = Array("ID", "NAME", "PREVIEW_TEXT");
	$arFilter = Array("IBLOCK_ID"=>IntVal(66), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_promotions" => $PROP_ID);
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$step[$arFields["ID"]]["NAME"] = $arFields["NAME"];
		$step[$arFields["ID"]]["PREVIEW_TEXT"] = $arFields["PREVIEW_TEXT"];
	}
	return $step;
}