<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

       <!-- Содержимое -->
<br>
                        <div class="preview-content">
                            <?foreach($arResult["NEW_ITEMS"] as $key=>$NewItems){?>
                                <h3 class="title"><?=$arResult['SECTIONS'][$key]?></h3>
                                <?foreach($NewItems as $arItem){?>
                                    <?
                                        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                                        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                    ?>
                                    <div class="doc_list mb-4">
                                        <a href="<?=$arItem["FILE"]["SRC"]?>" class="doc_item" target="_blank">
                                            <div class="doc_pict <?=$arItem["FILE"]["TYPE"]?>"></div>
                                            <div class="doc_body">
                                                <div class="doc_text">
                                                    <?=$arItem["NAME"]?>
                                                </div>
                                                <div class="doc_note">
                                                    <?=$arItem["FILE"]["FILE_SIZE"]?>
                                                </div>
                                            </div>
                                        </a>
                                        <? if (!empty($arItem["DISPLAY_PROPERTIES"]["TITLE_AMENDMENT_CHARTER"])) :?>
                                            <div class="faq">
                                                <div class="faq_item">
                                                    <div class="faq_top">
                                                        <div class="faq_pict">
                                                            <div class="faq_arr"></div>
                                                        </div>
                                                        <div class="faq_top_text"><?=$arItem["DISPLAY_PROPERTIES"]["TITLE_AMENDMENT_CHARTER"]["VALUE"]?></div>
                                                    </div>
                                                    <?foreach($arItem["CHANGES_CHARTER"] as $Children){?>
                                                        <?
                                                            $this->AddEditAction($Children['ID'], $Children['EDIT_LINK'], CIBlock::GetArrayByID($Children["IBLOCK_ID"], "ELEMENT_EDIT"));
                                                            $this->AddDeleteAction($Children['ID'], $Children['DELETE_LINK'], CIBlock::GetArrayByID($Children["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                                                        ?>
                                                        <div class="faq_text" style="display: none;">
                                                            <div id="<?=$this->GetEditAreaId($Children['ID']);?>" class="contrnt-otchet">
                                                                <div class="doc_list">
                                                                    <a href="<?=$Children["FILE"]["SRC"]?>" class="doc_item" target="_blank">
                                                                        <div class="doc_pict <?=$Children["FILE"]["TYPE"]?>"></div>
                                                                        <div class="doc_body">
                                                                            <div class="doc_text">
                                                                                <?=$Children["NAME"]?>
                                                                            </div>
                                                                            <div class="doc_note">
                                                                                <?=$Children["FILE"]["FILE_SIZE"]?>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?}?>
                                                </div>
                                            </div>
                                        <?endif;?>
                                    </div>
                                <?}?>
                            <?}?>
                        </div>