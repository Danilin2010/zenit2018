<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? //echo '<pre>' . print_r($arParams, 1) . '</pre>';?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
            <div class="right_top_line">
                <h2>Контакты</h2>
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <?if($arItem["LIST"]){?>
                        <?foreach($arItem["LIST"] as $arList){?>
                            <div class="form_application_line" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                                <?if(!$arList["NOT_USE_NAME"]){?>
                                <div class="right_bank_title">
                                    <?=$arList["NAME"]?>
                                </div>
                                <?}?>
                                <div class="contacts_block">
                                    <?if($arList["EMAIL"]){?><a href="mailto:<?=$arList["EMAIL"]?>"><?=$arList["EMAIL"]?></a><br><?}?>
                                    <?=$arList["PHONE"]?>
                                </div>
                            </div>
                        <?}?>
                    <?}?>
                <?}?>
                <?if($arParams["ALL_PHONE"]){?>
                <div class="right_bank_title">Телефон прямой линии</div>
                <div class="form_application_line">
                    <div class="contacts_block">
                        <?=$arParams["ALL_PHONE"]?>
                    </div>
                    <div class="note_text">
                        звонок по России бесплатный
                    </div>
                </div>
                <?}?>
            </div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
           <!--doc-->
				<div class="doc_list">
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <?if($arItem["RATES"]){?>
                        <a href="<?=$arItem["RATES"]["SRC"]?>" class="doc_item1" target="_blank" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                            <div class="doc_pict <?=$arItem["RATES"]["TYPE"]?>"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    Тарифы комиссионного вознаграждения ПАО Банк ЗЕНИТ за услуги для юридических лиц, индивидуальных предпринимателей и физических лиц, занимающихся в установленном законодательством Российской Федерации порядке частной практикой

					<?global $USER;
					if ($USER->IsAdmin())
					{
						//echo '<pre>' . print_r($arItem["RATES"]["NAME"], 1) . '</pre>';
					}?>
                                </div>
                                <div class="doc_note">
									<?/*=$arItem["RATES"]["FILE_SIZE"]*/?>
                                </div>
                            </div>
                        </a>
                    <?}?>
					<?php/* if (false): */?>
                    <?if($arItem["BLANKS"] && $arParams["SHOW_BLANKS"] == "Y"){?>
                        <h2 <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>><!--Бланки Заявлений--></h2>
                        <?foreach ($arItem["BLANKS"] as $blank):?>
                            <a href="<?=$blank["SRC"]?>" target="_blank" class="doc_item1" data-showid="<?=$arItem["ID"]?>" <?if($arItem["ID"]!=$arParams["CODE_SITY"]){?>style="display:none;"<?}?>>
                                <div class="doc_pict <?=$blank["TYPE"]?>"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        <?=$blank["NAME"]?>
                                    </div>
                                    <div class="doc_note">
                                        <?=$blank["FILE_SIZE"]?>
                                    </div>
                                </div>
                            </a>
                        <?endforeach;?>
                    <?}?>
					<?php/* endif; */?> 

                <?}?>
							<br><div class="doc_list"> 
							<a href="/upload/iblock/e0a/tk_tarifs_20170313.pdf" target="_blank" class="doc_item1" >
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Тарифы комиссионного вознаграждения ПАО Банк ЗЕНИТ по операциям с Таможенными картами                                    </div>
                                    <div class="doc_note">
                                       <!-- 409.15 Кб -->                                   </div>
                                </div>
							</a>
							<!--a href="/media/doc/business/rko/tariffs/app_packs_20171225.pdf" target="_blank" class="doc_item1" >
                                <div class="doc_pict pdf"></div>
                                <div class="doc_body">
                                    <div class="doc_text">
                                        Заявление на обслуживание счета по Тарифному плану.                                     </div>
                                    <div class="doc_note">
                                       <!-- 310.73 Кб -->                                   
									<!--/div>
                                </div>
							</a-->
							</div>

            <!--doc-->
<?php if (false): ?>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:news.list",
                                "doc_2",
                                array(
                                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000",
                                    "CACHE_TYPE" => "A",
                                    "CHECK_DATES" => "Y",
                                    "DETAIL_URL" => "",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "DISPLAY_DATE" => "Y",
                                    "DISPLAY_NAME" => "N",
                                    "DISPLAY_PICTURE" => "Y",
                                    "DISPLAY_PREVIEW_TEXT" => "Y",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "FIELD_CODE" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "FILTER_NAME" => "",
                                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                    "IBLOCK_ID" => "63",
                                    "IBLOCK_TYPE" => "rko",
                                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "MESSAGE_404" => "",
                                    "NEWS_COUNT" => "100",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "PAGER_TITLE" => "Новости",
                                    "PARENT_SECTION" => "89",
                                    "PARENT_SECTION_CODE" => " ",
                                    "PREVIEW_TRUNCATE_LEN" => "",
                                    "PROPERTY_CODE" => array(
                                        0 => "",
                                        1 => "FILE",
                                        2 => "",
                                    ),
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "N",
                                    "SHOW_404" => "N",
                                    "SORT_BY1" => "SORT",
                                    "SORT_BY2" => "NAME",
                                    "SORT_ORDER1" => "ASC",
                                    "SORT_ORDER2" => "ASC",
                                    "STRICT_SECTION_CHECK" => "N",
                                    "COMPONENT_TEMPLATE" => "doc"
                                ),
                                false
                            );?>
<?php endif; ?> 
				</div>
			</div>
    	</div>
	</div>
</div>