<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="after_news_item">
		<div class="after_news_item_data">
			<?=$arItem["DISPLAY_ACTIVE_FROM"]?>
		</div>
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="after_news_item_title"><?=$arItem["NAME"]?></a>
		<div class="after_news_item_text">
			<?=$arItem["PREVIEW_TEXT"]?>
		</div>
	</div>
<?endforeach;?>

