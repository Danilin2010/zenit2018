<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<p id="<?= $this->GetEditAreaId($arItem['ID']); ?>"></p>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<h1>Дополнительно при открытии счета</h1>
				<!--conditions-->
				<div class="conditions">
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["blank_verification"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							<?= $arItem["PROPERTIES"]["blank_verification"]["NAME"] ?>
						</div>
					</div>
					<div class="conditions_item last-item">
						<? if (($arItem["PROPERTIES"]["copy_verification"]["VALUE"])> 0){?>
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["copy_verification"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							<?= $arItem["PROPERTIES"]["copy_verification"]["NAME"] ?>
						</div>
						<? } 
						else {?>
						<div class="conditions_item_title">
						</div>
						<div class="conditions_item_text">
						</div>
						<?} ?>
					</div>
				</div>
				<!--conditions-->
			</div>
		</div>
	</div>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
			</div>
			<div class="block_type_center">
				<?=$arItem["PROPERTIES"]["tariffs_table"]["~VALUE"]["TEXT"]?>
				<p>«-» - услуга не включена в Тарифный план. Комиссия за оказание услуги взимается согласно базовым тарифам.</p>
				<!--br><?=$arItem["PROPERTIES"]["operation_time"]["~VALUE"]["TEXT"]?>-->
			</div>
		</div>
	</div>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<h1>Дополнительно в ТП Импортер</h1>
				<!--conditions-->
				<div class="conditions">
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["currency_account"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							<?= $arItem["PROPERTIES"]["currency_account"]["NAME"] ?>
						</div>
					</div>
					<div class="conditions_item last-item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["external_transfer"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							<?= $arItem["PROPERTIES"]["external_transfer"]["NAME"] ?>
						</div>
					</div>
					<div class="conditions_item">
						<div class="conditions_item_title">
							<?= $arItem["PROPERTIES"]["currency_control_discount"]["VALUE"] ?>
						</div>
						<div class="conditions_item_text">
							<?= $arItem["PROPERTIES"]["currency_control_discount"]["NAME"] ?>
						</div>
					</div>
				</div>
				<!--conditions-->
			</div>
		</div>
	</div>
<? endforeach; ?>

<? //echo '<pre>' . print_r($arResult["ITEMS"], 1) . '</pre>'; ?>