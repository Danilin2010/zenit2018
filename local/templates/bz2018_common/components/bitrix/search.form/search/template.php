<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="search wr_js_form" title="Поиск по сайту">
    <div class="ico_search"></div>
    <div class="wr_block_search">
        <div class="block_search">
            <form class="form_search" action="<?=$arResult["FORM_ACTION"]?>">
                <input name="s" type="hidden" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" />
                <?/* Для чего это? Похоже лишняя
                <div class="block_search_ico"></div>
                */?>
                <div class="block_search_close"></div>
                <div class="block_search_input">
                    <?if($arParams["USE_SUGGEST"] === "Y"):?><?$APPLICATION->IncludeComponent(
                        "bitrix:search.suggest.input",
                        "",
                        array(
                            "NAME" => "q",
                            "VALUE" => "",
                            "INPUT_SIZE" => 15,
                            "DROPDOWN_SIZE" => 10,
                        ),
                        $component, array("HIDE_ICONS" => "Y")
                    );?><?else:?><input type="text" name="q" value="" autocomplete="off" placeholder="Я ищу..." size="15" maxlength="50" /><?endif;?>
                </div>
            </form>
        </div>
        <div class="wr_block_search_result">
            <div class="block_search_result z-container">

            </div>
        </div>
    </div>
</div>