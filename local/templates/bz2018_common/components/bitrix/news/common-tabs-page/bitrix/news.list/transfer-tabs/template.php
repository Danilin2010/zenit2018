<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (!empty($arResult['ITEMS'])) { ?>
<div class="wr_block_type">
	<!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
		<?php foreach ($arResult['ITEMS'] as $arItem) {?>
			<li><a href="#content-tabs-<?= $arItem['ID'] ?>"><?= $arItem['NAME'] ?></a></li>
		<?php } ?>
		</ul>
	<?php foreach ($arResult['ITEMS'] as $arItem) {?>
		<div class="content_body" id="content-tabs-<?= $arItem['ID'] ?>">
			<?php if (!empty($arItem['PREVIEW_TEXT']) || !empty($arItem['DETAIL_TEXT'])) {?>
			<div class="wr_block_type<?= !empty($arItem['DISPLAY_PROPERTIES']['files']['VALUE']) ? ' white' : '' ?>">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<?= $arItem['PREVIEW_TEXT'] ?>
					</div>
					<div class="block_type_center">
						<?= $arItem['DETAIL_TEXT'] ?>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if (!empty($arItem['DISPLAY_PROPERTIES']['files']['VALUE'])) { ?>
			<div class="wr_block_type gray">
				<div class="block_type to_column c-container">
					<div class="block_type_right"></div>
					<div class="block_type_center">
						<div class="text_block">
							<!--doc-->
							<div class="doc_list">
								<? if (isset($arItem['DISPLAY_PROPERTIES']['files']['FILE_VALUE']['ID'])) { ?>
									<?php $arFile = $arItem['DISPLAY_PROPERTIES']['files']['FILE_VALUE'] ?>
									<a href="<?= $arFile['SRC'] ?>" target="_blank" class="doc_item">
										<div class="doc_pict<?= ' ' . getFileIconClass($arFile['SRC'])?>"></div>
										<div class="doc_body">
											<div class="doc_text">
												<?= $arFile['DESCRIPTION'] ?>
											</div>
											<div class="doc_note">
												<?= formatFileSize($arFile['FILE_SIZE']) ?>
											</div>
										</div>
									</a>
								<?php } else { ?>
									<?php foreach ($arItem['DISPLAY_PROPERTIES']['files']['FILE_VALUE'] as $arFile) { ?><a href="<?= $arFile['SRC'] ?>" target="_blank" class="doc_item">
										<div class="doc_pict<?= ' ' . getFileIconClass($arFile['SRC'])?>"></div>
										<div class="doc_body">
											<div class="doc_text">
												<?= $arFile['DESCRIPTION'] ?>
											</div>
											<div class="doc_note">
												<?= formatFileSize($arFile['FILE_SIZE']) ?>
											</div>
										</div>
									</a><?php } ?>
								<?php } ?>
							</div>
							<!--doc-->
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	<?php } ?>
	</div>
	<!--content_tab-->
</div>
<?php } ?>