<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (!empty($arResult['SECTIONS'])) { ?>
	<div class="wr_block_type">
		<div class="block_type to_column">
			<div class="block_type_right">
			</div>
			<div class="block_type_center">
				<!--ipoteka_list-->
				<div class="ipoteka_list">
					<?php foreach ($arResult['SECTIONS'] as $arItem) { ?><div class="ipoteka_item">
						<div class="wr_ipoteka_item">
							<div class="wr_pict_ipoteka_item" style="background-image: url('<?= SITE_TEMPLATE_PATH ?>/img/f_snippet.png');">
							</div>
							<a href="<?= $arItem['SECTION_PAGE_URL'] ?>" class="ipoteka_item_title"><?= $arItem['NAME'] ?></a>
							<div class="ipoteka_item_text">
								<?= $arItem['UF_DESCRIPTION'] ?>
							</div>
						</div>
					</div><?php } ?>
				</div>
				<!--ipoteka_list-->
			</div>
		</div>
	</div>
<?php } ?>