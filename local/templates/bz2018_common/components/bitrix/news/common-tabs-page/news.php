<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $USER;
if (!$USER->IsAdmin())
{
	LocalRedirect('/');
}
?><? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "", Array(
	"ADD_SECTIONS_CHAIN" => "N",
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"COUNT_ELEMENTS" => "N",
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	"SECTION_CODE" => "",
	"SECTION_FIELDS" => array("UF_DESCRIPTION"),
	"SECTION_ID" => $_REQUEST["SECTION_ID"],
	"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
	"SECTION_USER_FIELDS" => array(),
	"SHOW_PARENT_NAME" => "N",
	"TOP_DEPTH" => "2",
	"VIEW_MODE" => "LINE"
), $component); ?>
