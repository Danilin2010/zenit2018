<?
foreach ($arResult["DISPLAY_PROPERTIES"] as &$prop)
{
    if($prop["USER_TYPE"]=="ChekboxElementList")
        $prop["ELEMENT_VALUE"]=GetElementValue($prop["VALUE"],$Nation="NATION");
}unset($prop);


if($arResult["DISPLAY_PROPERTIES"]["MIN_PRICE"]["VALUE"]){
    $arResult["MIN_PRICE"]=GetPrice($arResult["DISPLAY_PROPERTIES"]["MIN_PRICE"]["VALUE"]);
}

if($arResult["DISPLAY_PROPERTIES"]["MAX_PRICE"]["VALUE"]){
    $arResult["MAX_PRICE"]=GetPrice($arResult["DISPLAY_PROPERTIES"]["MAX_PRICE"]["VALUE"]);
}


if (Bitrix\Main\Loader::includeModule('aic.bz'))
{
    $resConditions=new \Aic\Bz\CartConditions();
    $Conditions=$resConditions->getConditions($arResult["ID"]);
    $arResult["CONDITIONS"]= $Conditions;
}


$cp = $this->__component;
$arrCash=array(
    "NAME",
    "~PREVIEW_TEXT",
    "DISPLAY_PROPERTIES",
    "DETAIL_PICTURE",
    "PREVIEW_PICTURE",
);

if (is_object($cp))
{
    foreach($arrCash as $value)
    {
        $cp->arResult[$value] = $arResult[$value];
        $cp->SetResultCacheKeys(array($value));
        if (!isset($arResult[$value]))
            $arResult[$value] = $cp->arResult[$value];
    }
}