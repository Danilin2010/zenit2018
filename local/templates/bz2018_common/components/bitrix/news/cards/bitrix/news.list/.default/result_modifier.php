<?
foreach ($arResult["ITEMS"] as &$arItem) {
    foreach ($arItem["DISPLAY_PROPERTIES"] as &$prop)
    {
        if($prop["USER_TYPE"]=="ChekboxElementList")
            $prop["ELEMENT_VALUE"]=GetElementValue($prop["VALUE"],$Nation="NATION");
    }unset($prop);
}unset($arItem);


$arResult["SECTION_EXT"] = array();
if (is_array($arResult["SECTION"]["PATH"]) && (count($arResult["SECTION"]["PATH"]) > 0)) {
    $arResult["SECTION_EXT"] = end($arResult["SECTION"]["PATH"]);
}

$cp = $this->__component;
$arrCash=array(
    "SECTION_EXT",
);

if (is_object($cp))
{
    foreach($arrCash as $value)
    {
        $cp->arResult[$value] = $arResult[$value];
        $cp->SetResultCacheKeys(array($value));
        if (!isset($arResult[$value]))
            $arResult[$value] = $cp->arResult[$value];
    }
}

