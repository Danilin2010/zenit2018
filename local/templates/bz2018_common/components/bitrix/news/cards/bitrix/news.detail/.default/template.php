<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wr_block_type">
    <div class="block_type to_column c-container" style="overflow: hidden;">
        <div class="block_type_right">
            <div class="right_top_line">
				<h2>Вам может понадобиться</h2>
				<a href="/personal/cards/methods-of-deposit/" class="button" >Пополнение карт</a>
                <!--<div class="block_top_line"></div>-->
                <?
                PrintRightList($arResult["DISPLAY_PROPERTIES"],array(
                    "ADVANTAGES_RIGHT",
                ));
                ?>
            </div>
        </div>
        <div class="block_type_center">
            <!--conditions-->
            <?if(($arResult["MIN_PRICE"] || $arResult["MAX_PRICE"]) || $arResult["DISPLAY_PROPERTIES"]["GRACE_PERIOD"] || $arResult["DISPLAY_PROPERTIES"]["MAINTENANCE"]){?>
            <div class="conditions"><?
                if($arResult["MIN_PRICE"] || $arResult["MAX_PRICE"]){
                    ?><div class="conditions_item norm_width">
                    <div class="conditions_item_title">
                        <?if($arResult["MIN_PRICE"]){?>
                            <?=$arResult["MIN_PRICE"]["SUMM"]?> <span><?=$arResult["MIN_PRICE"]["SUFF"]?></span>
                            —
                        <?}?>
                        <?if($arResult["MAX_PRICE"]){?>
                            <?=$arResult["MAX_PRICE"]["SUMM"]?> <span><?=$arResult["MAX_PRICE"]["SUFF"]?></span>
                        <?}?>
                    </div>
                    <div class="conditions_item_text">
                        Размер кредита
                    </div>
                    </div><?}?><?if($arResult["DISPLAY_PROPERTIES"]["GRACE_PERIOD"]){?><div class="conditions_item norm_width">
                    <div class="conditions_item_title">
                        <span>до</span>
                        <?=$arResult["DISPLAY_PROPERTIES"]["GRACE_PERIOD"]["VALUE"]?>
                        <?=sklonenie($arResult["DISPLAY_PROPERTIES"]["GRACE_PERIOD"]["VALUE"], array('дня','дней','дней'))?>
                    </div>
                    <div class="conditions_item_text">
                        льготный период
                    </div>
                    </div><?}?><?if($arResult["DISPLAY_PROPERTIES"]["MAINTENANCE"]){?><div class="conditions_item norm_width">
                    <div class="conditions_item_title">
                        <?=$arResult["DISPLAY_PROPERTIES"]["MAINTENANCE"]["VALUE"]?> ₽
                    </div>
                    <div class="conditions_item_text">
                        за обслуживаемый период
                    </div>
                    </div><?}?>
            </div>
            <?}?>
            <?if($arResult["CONDITIONS"] && is_array($arResult["CONDITIONS"]) && count($arResult["CONDITIONS"])>0){?>
            <div class="conditions">
                <?foreach($arResult["CONDITIONS"] as $conditions){
                    ?><div class="conditions_item norm_width">
                        <div class="conditions_item_title">
                            <?=$conditions["PREVIEW_TEXT"]?>
                        </div>
                        <div class="conditions_item_text">
                            <?=$conditions["DETAIL_TEXT"]?>
                        </div>
                    </div><?
                }?>
            </div>
            <?}?>
            <!--conditions-->
            <?if($arResult["DISPLAY_PROPERTIES"]["ARBITRARY_ADVANTAGES"]){?>
            <div class="text_block">
            <?
            PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                "ARBITRARY_ADVANTAGES",
            ),false,false,2);
            ?>
            </div>
            <?}?>
            <?
            PrintList($arResult["DISPLAY_PROPERTIES"],array(
                "ADVANTAGE",
            ),'big_list to_column');
            ?>
        </div>
    </div>
    <?if($arResult["DISPLAY_PROPERTIES"]["ADDITIONALLY"]){?>
    <div class="block_type c-container">
        <div class="block_type_center">
            <?
            PrintPromoBenefits($arResult["DISPLAY_PROPERTIES"],array(
                "ADDITIONALLY",
            ));
            ?>
        </div>
    </div>
    <?}?>
</div>
<?if($arResult["DISPLAY_PROPERTIES"]["CASH_WITHDRAWAL"]){?>
<div class="wr_block_type">
    <div class="block_type to_column c-container">
        <div class="block_type_right">
            <!--<div class="block_top_line"></div>-->
            <div class="form_application_line">
            <?
            PrintRightList($arResult["DISPLAY_PROPERTIES"],array(
                "CLIENT_REQUIREMENTS",
            ));
            ?>
            </div>
            <div class="form_application_line">
                <a href="#"
                   class="button"
                   data-yourapplication
                   data-classapplication=".wr_form_application"
				>Оставить заявку</a>
            </div>
        </div>
        <div class="block_type_center">
            <?/* <h1>Требования к заемщику</h1>
            <div class="text_block">*/?>
                <?
                PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                    "CASH_WITHDRAWAL",
                ),false,false,2);
                ?>
            <?/*</div>*/?>
        </div>
    </div>
</div>
<?}?>
<?if($arResult["DISPLAY_PROPERTIES"]["DOWNLOADS"]){?>
<div class="wr_block_type">
    <div class="block_type c-container">
        <div class="block_type_center">
            <?PrintBigFile($arResult["DISPLAY_PROPERTIES"],array(
                "DOWNLOADS",
            ),true);?>
            <?
            /*PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                "SECURING_LOAN",
            ),false,false,2);
            */?>
        </div>
    </div>
</div>
<?}?>


