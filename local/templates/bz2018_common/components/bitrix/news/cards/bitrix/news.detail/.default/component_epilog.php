<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */
?>
    <div class="wr_block_type">
        <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "universal",
		"SOURCE_TREATMENT" => "Заявка на Карту: ".$arResult["NAME"],
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
        <div class="block_type to_column">
            <!--banners-->
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/block/banners.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
            <!--banners-->
        </div>
    </div>
<?
ob_start();
?>
<?/*if(is_array($arResult["PREVIEW_PICTURE"])){?>
    <div class="detail_picture" style="background-image: url('<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>')"></div>
<?}*/?>
<?if(isset($arResult["~PREVIEW_TEXT"]) && (strlen($arResult["~PREVIEW_TEXT"]) > 0)){?>
    <?=$arResult["~PREVIEW_TEXT"]?>
<?}?>
<?if($arResult["DISPLAY_PROPERTIES"]["DESCRIPTION_TITLE"]){?>
    <?=$arResult["DISPLAY_PROPERTIES"]["DESCRIPTION_TITLE"]["DISPLAY_VALUE"]?>
<?}?>
    <p><a href="#" class="button"
       data-yourapplication
       data-classapplication=".wr_form_application"
        >Оставить заявку</a></p>
<?
$html = ob_get_contents();
ob_end_clean();

ob_start();
?>
<?if(is_array($arResult["PREVIEW_PICTURE"])){?>
    <div class="detail_picture" style="background-image: url('<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>')"></div>
<?}?>
<?
$htmlpic = ob_get_contents();
ob_end_clean();
BufferContent::SetTitle('headertext',$html);
BufferContent::SetTitle('to_page_class','to_page_big_top');
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/top_banner/street.png');
BufferContent::SetTitle('headerpic',$htmlpic);

