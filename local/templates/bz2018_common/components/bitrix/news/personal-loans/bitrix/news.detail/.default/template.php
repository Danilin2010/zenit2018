<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!--content_tab-->
<div class="content_rates_tabs">
    <ul class="content_tab z-container">
        <?/*<li><a href="#content-tabs-1">Расчет параметров</a></li>*/?>
        <li><a href="#content-tabs-2">Условия и документы</a></li>
        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["INSURANCE"]["ID"])):?>
            <li><a href="#content-tabs-3">Страхование</a></li>
        <?endif;?>
        <li><a href="#content-tabs-4">Погашение кредита</a></li>
        <li><a href="#content-tabs-5">Документы и тарифы</a></li>
    </ul>
    <?/*<div class="content_body" id="content-tabs-1">
                <div class="wr_block_type">
                    <div class="block_type">
                        <div class="block_type_center">
                            <h1>
                                Рассчитайте ваше предложение по кредиту
                                <div class="note_text">
                                    Срок рассмотрения онлайн-заявки составляет до 10 дней
                                </div>
                            </h1>
                        </div>
                    </div>
                </div>
                <div class="wr_block_type">
                    <div class="block_type to_column">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <h1>Требования к заемщику</h1>
                            <ul class="big_list">
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["BORROWER_REQUIEREMENTS"]["ELEMENT_VALUE"] as $require):?>
                                <li>
                                    <?=$require["NAME"]?>
                                </li>
                                <?endforeach;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>*/?>
    <div class="content_body" id="content-tabs-2">
        <?if($arResult["DISPLAY_PROPERTIES"]["CALCULATOR_PROGRAM"]){?>
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/calculator/consumer.php",
                Array(
                    "programm"=>$arResult["DISPLAY_PROPERTIES"]["CALCULATOR_PROGRAM"]["VALUE_XML_ID"],
                ),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        <?}?>
        <div class="wr_block_type">
            <div class="wr_block_type">
                <div class="block_type to_column z-container">
                    <div class="block_type_right">
                        <div class="form_application_line">
                            <div class="right_top_line">
                               <!-- <div class="block_top_line"></div>-->
                                <div class="right_bank_block">
                                    <div class="right_bank_title">
                                        <?=$arResult["DISPLAY_PROPERTIES"]["CURRENCY"]["ELEMENT_VALUE"][0]["NAME"]?>
                                    </div>
                                    <div class="right_bank_text">
                                        Валюта кредита
                                    </div>
                                </div>
                                <div class="right_bank_block">
                                    <div class="right_bank_title">
                                        2 дня
                                    </div>
                                    <div class="right_bank_text">
                                        срок рассмотрения заявки
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line">
                            <a href="#"
                               class="button"
                               data-yourapplication
                               data-classapplication=".wr_form_application"
                               data-formrapplication=".content_rates_tabs"
                               data-formrapplicationindex="0"
                                >Оставить заявку</a>
                        </div>
                    </div>
                    <div class="block_type_center">
                        <h1>Условия кредитования</h1>
                        <!--conditions-->
                        <div class="conditions">
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["STAKE_MIN"]["ID"])){?><div class="conditions_item">
                                <div class="conditions_item_title">
                                    <span>от</span> <?=$arResult["DISPLAY_PROPERTIES"]["STAKE_MIN"]["VALUE"]?> %
                                </div>
                                <div class="conditions_item_text">
                                    ставка
                                </div>
                                </div><?}?><?if(!empty($arResult["DISPLAY_PROPERTIES"]["PERIOD_MAX"])){?><div class="conditions_item">
                                <div class="conditions_item_title">
                                    <span>до</span> <?=$arResult["DISPLAY_PROPERTIES"]["PERIOD_MAX"]["VALUE"]?> лет
                                </div>
                                <div class="conditions_item_text">
                                    срок кредита
                                </div>
                                </div><?}?><?if(!empty($arResult["DISPLAY_PROPERTIES"]["SUM_AMOUNT"])){?><div class="conditions_item">
                                <div class="conditions_item_title">
                                    <span>до</span>
                                    <?=$arResult["DISPLAY_PROPERTIES"]["SUM_AMOUNT"]["VALUE"]["SUMM"]?>
                                    <span><?=$arResult["DISPLAY_PROPERTIES"]["SUM_AMOUNT"]["VALUE"]["SUFF"]?></span>
                                </div>
                                <div class="conditions_item_text">
                                    сумма кредита
                                </div>
                                </div><?}?>
                        </div>
                        <!--conditions-->
                        <div class="text_block">
                            <ul>
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["CREDIT_CONDITIONS"]["ELEMENT_VALUE"] as $condition):?>
                                    <li>
                                        <?=$condition["NAME"]?>
                                        <?if(!empty($condition["PREVIEW_TEXT"])) echo $condition["PREVIEW_TEXT"];?>
                                    </li>

                                <?endforeach;?>
                            </ul>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["UNUSUAL_CREDIT_CONDITIONS"]["ID"]))
                                echo $arResult["DISPLAY_PROPERTIES"]["UNUSUAL_CREDIT_CONDITIONS"]["~VALUE"]["TEXT"]?>
                            <?if($arResult["DISPLAY_PROPERTIES"]["NOTE_TEXT"]){?>
                                <div class="note_text">
                                    <?=$arResult["DISPLAY_PROPERTIES"]["NOTE_TEXT"]["~VALUE"]["TEXT"]?>
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
            <?if($arResult["DISPLAY_PROPERTIES"]["BORROWER_REQUIEREMENTS"]){?>
            <div class="wr_block_type">
                <div class="block_type to_column z-container">
                    <div class="block_type_right">

                    </div>
                    <div class="block_type_center">
                        <h1>Требования к заемщику</h1>
                        <ul class="big_list">
                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["BORROWER_REQUIEREMENTS"]["ELEMENT_VALUE"] as $require):?>
                                <li>
                                    <?=$require["NAME"]?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
            <?}?>
            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["CARPAWN_DOCS"]) || $arResult["DISPLAY_PROPERTIES"]["BORROWER_WARRANTER_DOCS"] || $arResult["DISPLAY_PROPERTIES"]["CARPAWN_DOCS"]):?>
                <div class="wr_block_type">
                    <div class="block_type to_column no-fix z-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <h1>
                                Необходимые документы
                                <div class="note_text">
                                    <div class="wr-toggle-light-text">
                                        <div class="toggle-light-text on">Для заемщика и поручителя</div>
                                        <?if(!empty($arResult["DISPLAY_PROPERTIES"]["CARPAWN_DOCS"])):?>
                                            <div class="toggle-light-wr">
                                                <div
                                                    class="toggle toggle-light"
                                                    data-toggle
                                                    data-checked="N"
                                                    data-checked-off=".first_b_prop"
                                                    data-checked-on=".to_b_prop"
                                                    data-name="type"></div>
                                            </div>
                                            <div class="toggle-light-text off">при залоге автомобиля</div>
                                        <?endif;?>
                                    </div>
                                </div>

                            </h1>
                            <ul class="big_list first_b_prop">
                                <?foreach ($arResult["DISPLAY_PROPERTIES"]["BORROWER_WARRANTER_DOCS"]["ELEMENT_VALUE"] as $docs):?>
                                    <li>
                                        <?=$docs["NAME"]?><br>
                                        <?if(!empty($docs["PREVIEW_TEXT"]))
                                            echo $docs["PREVIEW_TEXT"];?>
                                    </li>
                                <?endforeach;?>
                            </ul>
                            <?if(!empty($arResult["DISPLAY_PROPERTIES"]["CARPAWN_DOCS"])):?>
                                <ul class="big_list to_b_prop" style="display: none;">
                                    <?foreach ($arResult["DISPLAY_PROPERTIES"]["CARPAWN_DOCS"]["ELEMENT_VALUE"] as $carpawn_docs):?>
                                        <li>
                                            <?=$carpawn_docs["NAME"]?>
                                        </li>
                                    <?endforeach;?>
                                </ul>
                            <?endif;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
        </div>
    </div>
    <?if(!empty($arResult["DISPLAY_PROPERTIES"]["INSURANCE"]["ID"])):?>
        <div class="content_body" id="content-tabs-3">
            <div class="wr_block_type">
                <div class="block_type to_column z-container">
                    <div class="block_type_right">
                        <div class="right_top_line">
                            <!--<div class="block_top_line"></div>-->
                            <div class="right_bank_block">
                                Страхование осуществляеться<br />
                                в соответствии с <a href="#">тарифами</a><br />
                                страховых компаний
                            </div>
                        </div>
                    </div>
                    <div class="block_type_center">
                        <div class="text_block">
                            <h2><?=$arResult["DISPLAY_PROPERTIES"]["INSURANCE"]["NAME"]?></h2>
                            <?=$arResult["DISPLAY_PROPERTIES"]["INSURANCE"]["~VALUE"]["TEXT"]?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endif;?>
    <div class="content_body" id="content-tabs-4">
        <div class="wr_block_type">
            <div class="block_type to_column z-container">
                <div class="block_type_right">
                    <div class="right_top_line">
                        <div class="block_top_line"></div>
                        <h2>Связаться с банком</h2>
                        <div class="form_application_line">
                            <div class="contacts_block">
                                info@zenit.ru<br>
                                +7 (495) 967-11-11<br>
                                8 (800) 500-66-77
                            </div>
                            <div class="note_text">
                                звонок по России бесплатный
                            </div>
                        </div>
                        <a href="#modal_form-contact" class="button open_modal bigmaxwidth">задать вопрос</a>
                    </div>
                </div>
                <div class="block_type_center">
                    <div class="text_block">
                        <h2>Погашение кредита осуществляется</h2>
                        <?=$arResult["DISPLAY_PROPERTIES"]["CREDIT_REPAYMENT"]["~VALUE"]["TEXT"]?>
                        <h2>Досрочное погашение</h2>
                        <?=$arResult["DISPLAY_PROPERTIES"]["EARLY_REPAYMENT"]["~VALUE"]["TEXT"]?>
                        <h2>Пополнение открытого в Банке счета</h2>
                        <ul>
                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["REPLENISHMENT"]["ELEMENT_VALUE"] as $option):?>
                                <li>
                                    <?=$option["NAME"]?>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?if(count($arResult["FILE"])):?>
        <div class="content_body" id="content-tabs-5">
            <div class="wr_block_type">
                <div class="block_type to_column z-container">
                    <div class="block_type_right">
                        <h2>Связаться с банком</h2>
                        <div class="form_application_line">
                            <div class="contacts_block">
                                info@zenit.ru<br>
                                +7 (495) 967-11-11<br>
                                8 (800) 500-66-77
                            </div>
                            <div class="note_text">
                                звонок по России бесплатный
                            </div>
                        </div>
                        <a href="#modal_form-contact" class="button open_modal bigmaxwidth">задать вопрос</a>
                    </div>
                    <div class="block_type_center">
                        <h1>Документы для скачивания</h1>
                        <div class="text_block">
                            <!--doc-->
                            <div class="doc_list">
                                <?foreach ($arResult["FILE"] as $file){?>
                                <a href="<?=$file["SRC"]?>" target="_blank" class="doc_item">
                                    <div class="doc_pict <?=$file["TYPE"]?>"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            <?=$file["NAME"]?>
                                        </div>
                                        <div class="doc_note">
                                            <?=$file["FILE_SIZE"]?>
                                        </div>
                                    </div>
                                    </a><?}?>
                            </div>
                            <!--doc-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?endif;?>
</div>



