<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle("Банк ЗЕНИТ в СМИ");
?>
	<div class="wr_block_type gray">
	<div class="block_type to_column c-container">
<? //echo '<pre>' . print_r($arResult, 1) . '</pre>';?>
	<a href="/rus/about_bank/bank_in_press/" class="top_arr_prew show_min">
		<div class="arr"></div>Назад
	</a>

	<!--h1 class="hide_min">Новости<a href="http://<?=$_SERVER['SERVER_NAME']?>/rus/about_bank/bank_in_press/rss/" class="rss" target="_blank"></a></h1-->

	<div class="block_type_right">
		<div class="wr_form_block">
			<div class="right_top_line">
				<?$APPLICATION->IncludeComponent("asd:subscribe.quick.form", "news-subscribe", array(
	"FORMAT" => "text",
		"INC_JQUERY" => "N",
		"NOT_CONFIRM" => "N",
		"RUBRICS" => array(
			0 => "1",
		),
		"SHOW_RUBRICS" => "N",
		"COMPONENT_TEMPLATE" => "news-subscribe"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
				<br>          <br class=" hide_min">

				<div class="block_top_line hide_min"></div>
				<h2>Новости банка в соцсетях</h2>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "fuuter_net", array(
	"COMPONENT_TEMPLATE" => "main_news",
		"IBLOCK_TYPE" => "icon",
		"IBLOCK_ID" => "5",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "NAME",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "DETAIL_TEXT",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "CLASS",
			1 => "LINK",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => ""
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>

			</div>
		</div>
	</div>
	<div class="block_type_center">

	<div class="news_detail">
		<div class="news_detail_body">
			<div class="news_detail_data"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
			<div class="news_detail_title">
				<?=$arResult["NAME"]?>
			</div>
			<div class="news_detail_source"><?=$arResult["PROPERTIES"]["source_author"]["VALUE"]?> <?=$arResult["PROPERTIES"]["source_link"]["VALUE"]?></div>
			<div class="news_detail_body_wr" style="text-align:justify">
				<p><b><?=$arResult["PREVIEW_TEXT"]?></b></p>
				<?=$arResult["DETAIL_TEXT"]?>
			</div>
		</div>
		<div class="bottom">
			<div class="left">
				Спасибо за отзыв!
			</div>
			<div class="right">
				Поделиться:
				<ul class="share_net" id="share_net">
					<li><a href="#" class="vk"></a></li>
					<li><a href="#" class="f"></a></li>
					<li><a href="#" class="tw" ></a></li>
				</ul>
			</div>
		</div>
	</div>

		<div class="after_news">
			<div class="after_news_title">
				<a href="/rus/about_bank/bank_in_press/" class="after_news_title_link">Все новости</a>
				<h2>Другие новости</h2>
			</div>
			<div class="after_news_list">
				<?$GLOBALS["arrFilter"] = array(
					"!ID" => $arResult["ID"],
				);?>
				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"another-news", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "#SERVER_NAME#/rus/about_bank/bank_in_press/#CODE#/",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "145",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "source_author",
			1 => "source_link",
			2 => "category_news",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "another-news",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
			</div>
		</div>
	</div>




	</div>
	</div>
<script type="text/javascript">
	var share2 = document.getElementById('share_net');
	Ya.share2('share_net', {
		content: {
			url: '',
			title: ''
		},

		contentByService: {
			twitter: {
				url: 'https://twitter.com/bank_zenit',
			},
			vkontakte: {
				url: 'https://vk.com/bank_zenit',
			},
			facebook: {
				url: 'https://www.facebook.com/BankZENIT/',

			}
		},
		theme: {
			services: 'facebook,twitter,vkontakte',
			bare: true
		}

	});
</script>