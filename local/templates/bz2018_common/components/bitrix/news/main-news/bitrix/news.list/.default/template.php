<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \helpers\DateTime2HR; ?>
<div class="news_list">
	<? //echo '<pre>' . print_r($arResult["ITEMS"], 1) . '</pre>';?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="news_list_item">
		<?if($arItem["PREVIEW_PICTURE"]):?>
		<div class="pict" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')"></div>
		<div class="body right">
		<?else:?>
		<div class="body">
		<?endif;?>
			<div class="news_list_item_data">
				<?=$arItem["DISPLAY_ACTIVE_FROM"]?>
			</div>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="news_list_item_title">
				<?=$arItem["NAME"]?>
			</a>
			<div class="news_list_item_text">
				<?=$arItem["PREVIEW_TEXT"]?>
			</div>
			<div class="news_list_item_source">
				<?=$arItem["PROPERTIES"]["source_author"]["VALUE"]?>
				<?=$arItem["PROPERTIES"]["source_link"]["VALUE"]?>
			</div>
		</div>
	</div>
<?endforeach;?>
</div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<div class="button_line">
		<a href="#" class="button transparent maxwidth" id="show_more_news" data-count="<?=$arParams["NEWS_COUNT"]?>">показать еще новости</a>
	</div>
	<?endif;?>
</div>

<div class="contant_block_fixwd">
	<div class="block_top_line"></div>
	<h2>Контакты для прессы</h2>
	<div class="contacts_block">
		8 (800) 500-66-77
		<br>
		<a href="mailto:pr@zenit.ru">pr@zenit.ru</a>
	</div>
</div>
