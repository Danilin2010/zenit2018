<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var string $strErrorMessage
 * @param CBitrixComponent $component
 * @param CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 */
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "universal",
		"SOURCE_TREATMENT" => "Заявка на Вклад: ".$arResult["NAME"],
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
<?
ob_start();
?>
<?if($arResult["~PREVIEW_TEXT"]){?>
    <?=$arResult["~PREVIEW_TEXT"]?>
<?}?>
    <p><a href="#" class="button"
          data-yourapplication
          data-classapplication=".wr_form_application"
          data-formrapplication=".content_rates_tabs"
          data-formrapplicationindex="0"
            >Хочу открыть</a></p>
<?
$html = ob_get_contents();
ob_end_clean();
BufferContent::SetTitle('headertext',$html);
BufferContent::SetTitle('to_page_class','to_page_big_top');
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/top_banner/garden.png');

//echo "<pre>";print_r($arResult["MAX_PRICE"]);echo "</pre>";