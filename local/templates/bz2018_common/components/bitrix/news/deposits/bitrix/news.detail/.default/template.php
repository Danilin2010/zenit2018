<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

   <!--content_tab-->
    <div class="content_rates_tabs">
        <ul class="content_tab c-container">
            <li><a href="#content-tabs-1">Описание</a></li>
            <li><a href="#content-tabs-2">Ставки</a></li>
            <?/*<li><a href="#content-tabs-3">Документы</a></li>*/?>
            <?if($arResult["DISPLAY_PROPERTIES"]["HIDE_ONLINE"]["DISPLAY_VALUE"]!="Y"){?>
            <li><a href="#content-tabs-4">Открыть онлайн</a></li>
            <?}?>
        </ul>
        <div class="content_body" id="content-tabs-1">
            <?if(strlen($arResult["DETAIL_TEXT"])>0){?>
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <?if($arResult["DISPLAY_PROPERTIES"]["DOCUMENTATION"]){?>
                    <div class="block_type_right">
                        <h2>Документы</h2>
                        <!--doc-->
                        <?PrintBigFile($arResult["DISPLAY_PROPERTIES"],array(
                            "DOCUMENTATION",
                        ),false);?>
                    </div>
                    <?}?>
                    <div class="block_type_center">
                        <div class="text_block">
                            <?=$arResult["DETAIL_TEXT"]?>
                        </div>
                    </div>
                </div>
            </div>
            <?}?>

            <?
            PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                "MINIMUM_BALANCE",
            ));
            ?>
        </div>
        <div class="content_body" id="content-tabs-2">
            <?
            PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                "BETTING_ODDS",
            ));
            ?>
            <?
            PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                "ADDITIONS",
            ),false);
            ?>
        </div>
        <?/*<div class="content_body" id="content-tabs-3">
            <div class="wr_block_type">
                <div class="block_type">
                    <div class="block_type_center">
                        <?PrintBigFile($arResult["DISPLAY_PROPERTIES"],array(
                            "DOCUMENTATION",
                        ));?>
                    </div>
                </div>
            </div>
        </div>*/?>
        <?if($arResult["DISPLAY_PROPERTIES"]["HIDE_ONLINE"]["DISPLAY_VALUE"]!="Y"){?>
        <div class="content_body" id="content-tabs-4">
            <?
            PrintBigElement($arResult["DISPLAY_PROPERTIES"],array(
                "OPEN_ONLINE",
            ),false);
            ?>
        </div>
        <?}?>
    </div>

