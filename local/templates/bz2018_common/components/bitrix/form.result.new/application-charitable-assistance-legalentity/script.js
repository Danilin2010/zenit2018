$(document).ready(function(){

    var left_to_step='100%';
    var first_to_step='0px';
    var timer=500;
    var padding=40;
    padding=0;

    function MarginResize()
    {
        var to_step=$('.to_step_jr');
        var first_step=$('.first_step_jr');
        var position_to_step = $('.wr_form_application').offset();
        var to_step_=$(window).width()-position_to_step.left-padding + 100;
        left_to_step=to_step_+'px';
        first_to_step='-'+(position_to_step.left+first_step.width()-padding)+'px';
        if(!to_step.hasClass('win'))
            to_step.css('margin-left',left_to_step);
        if(first_step.hasClass('win'))
            first_step.css('left',first_to_step);
    }

    setTimeout(function(){MarginResize();},500);

    $(window).resize(function() {
        MarginResize();
    });

    setTimeout(function () {
        $("form[name=SIMPLE_FORM_5]").validate({
            errorPlacement: function(error, element) {
                var parent=element.parents('.wr_complex_input');
                var parentcheckbox=element.parents('.jq-checkbox');
                if(parent.length>0) {
                    parent.addClass('error');
                    error.insertAfter(parent);
                }else if(parentcheckbox.length>0){
                    parentcheckbox.addClass('error');
                    error.insertAfter(parentcheckbox);
                }else{
                    error.insertAfter(element);
                }
            },
            success: function(error){
                error.prev('.jq-checkbox').removeClass('error');
                error.prev('.wr_complex_input').removeClass('error');
                error.remove();
            },
            rules:{
                form_text_55: {
                    required: true
                },
                form_text_60: {
                    required: true
                },
                form_text_61: {
                    required: true
                },
                form_text_62: {
                    required: true
                },
                form_textarea_63: {
                    required: true
                },
                form_textarea_64: {
                    required: true
                },
                form_text_65: {
                    required: true
                },
                form_text_66: {
                    required: true
                },
                form_text_67: {
                    required: true
                },
                form_text_68: {
                    required: true
                },
                form_text_69: {
                    required: true,
                    email2: true
                },
                form_textarea_70: {
                    required: true
                },
                'form_checkbox_agreement[]': {
                    required: true
                }
            },
            messages:{
                form_text_55: {
                    required: 'Укажите название организации'
                },
                form_text_60: {
                    required: 'Укажите название мероприятия'
                },
                form_text_61: {
                    required: 'Укажите дата и время проведения мероприятия'
                },
                form_text_62: {
                    required: 'Укажите место проведения мероприятия'
                },
                form_textarea_63: {
                    required: 'Опишите проект'
                },
                form_textarea_64: {
                    required: 'Укажите размер пожертвования'
                },
                form_text_65: {
                    required: 'Укажите участие других жертвователей'
                },
                form_text_66: {
                    required: 'Укажите ответственное лицо с указанием должности'
                },
                form_text_67: {
                    required: 'Укажите как к Вам можно обращаться?'
                },
                form_text_68: {
                    required: 'Укажите адрес, телефон, факс'
                },
                form_text_69: {
                    required: 'Укажите e-mail',
                    email2: 'Укажите правильный e-mail',
                },
                form_textarea_70: {
                    required: 'Укажите дополнительную информацию'
                },
                'form_checkbox_agreement[]': {
                    required: 'Нужно согласие'
                }
            },
            submitHandler: function(form) {
                var $form = $(form);
                var url='/ajax/form/universal.php';
                $.ajax({
                    url: url,
                    type: "POST",
                    data: $form.serialize(),
                    dataType: 'json',
                    success: function(data){
                        if(data.errors)
                        {
                            $('.form_errors_text').text(data.errors);
                        }else{
                            if(data.result_id>0)
                                $('[data-result]').text(data.result_id);
                            $('.first_step_jr').addClass('win').animate({'left': first_to_step},timer);
                            $('.to_step_jr').addClass('win').animate({'margin-left': "0px"},timer);
                            $('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
                            $('.wr_step_filter_block .step_filter_block:first').animate({'opacity': "0"},timer);
                        }
                    }
                });

            }
        });
    },300);



    $('input[name="form_email_6"]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        showMaskOnFocus: false,
        showMaskOnHover: false,
        'placeholder':''
    });

});
