<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<?
if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
    && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
    $ajax = "Y";
}

if ($ajax == "Y")
{
    $APPLICATION->RestartBuffer();
    $errors = false;
    if ($arResult["isFormErrors"] == "Y")
    {
        $errors = $arResult["FORM_ERRORS_TEXT"];
        $errors = str_replace(array(
            '<p><font class="errortext">',
            '</font></p>'
        ), "", $errors);
        $errors = str_replace("<br>", "</li><li>", $errors);
        $errors = "<ul><li>" . $errors . "</li></ul>";
    }
    if ($errors)
    {
        $arr = array(
            'errors' => $errors,
            'result_id' => (int)$_REQUEST["RESULT_ID"]
        );
    }
    else
    {
        $arr = array('result_id' => (int)$_REQUEST["RESULT_ID"]);
    }
    echo json_encode($arr);
    die();
}


?>

<? if ($arResult["isFormNote"] != "Y"){?>
    <div class="block_type to_column">
        <div class="block_type_center">
            <div class="form_errors_text"><? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?></div>
            <!--form_application-->
            <div class="wr_form_application">
                <div class="step_form_application first_step_jr">
                    <?= $arResult["FORM_HEADER"] ?>
                    <?
                    $q = $arResult['QUESTIONS'];
                    // print_r($q['city']['STRUCTURE'][0]['ID']);
                    ?>
                    <?/*<form action="" class="validate application">*/?>
                    <input type="hidden" name="web_form_apply" value="Y"/>
                    <input type="hidden" name="form_<?=$q['source']['STRUCTURE'][0]['FIELD_TYPE']?>_<?=$q['source']['STRUCTURE'][0]["ID"]?>"
                           value="<?=$arParams["SOURCE_TREATMENT"]?>">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="form_application_line">
                                <?SponsorshipForm::showProp($q['company'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="form_application_line">
                                <?SponsorshipForm::showProp($q['project_name'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="form_application_line">
                                <?SponsorshipForm::showProp($q['date'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="form_application_line">
                                <?SponsorshipForm::showProp($q['place'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="form_application_line">
                                <?SponsorshipForm::showProp($q['description'],$arResult["arrVALUES"]);?>
                            </div>
                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['amount_size'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['capability'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['participation_others'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['person_charge'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['person_name'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['address'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['mail'], $arResult["arrVALUES"]); ?>
                            </div>

                            <div class="form_application_line">
                                <? SponsorshipForm::showProp($q['other_info'], $arResult["arrVALUES"]); ?>
                            </div>


                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="row">
                                <div class="col-sm-12 col-md-9">
                                    <label><?= $q['agreement']['HTML_CODE'] ?>Я согласен с <a target="_blank" href="https://www.zenit.ru/media/rus/content/pdf/personal/pd_confirm.pdf">условиями</a> передачи информации</label>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <input class="button" value="отправить" type="submit">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $arResult["FORM_FOOTER"] ?>
                </div>
                <div class="step_form_application to_step_jr">
                    <div class="form_application">
                        <div class="form_application_ok"></div>
                        <div class="form_application_title">
                            Ваша заявка принята!
                        </div>
                        <div class="form_application_text">
                            Номер заявки <b data-result></b>. Подтверждение отправлено на e-mall. Срок<br/>
                            рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                            свяжется с Вами в ближайшее время.
                        </div>
                    </div>
                </div>
            </div>
            <!--form_application-->
        </div>
    </div>
<?}else{?>
    <div class="block_type to_column">
        <div class="block_type_right">
            <!--step_filter_block-->
            <div class="wr_step_filter_block_b">
                <div class="wr_step_filter_block">
                    <div class="step_filter_block">
                        <div class="filter_block">
                            <div class="filter_block_body">
                                <div class="new_icon"></div>
                                <div class="form_note">
                                    Подготовьте необходимые документы<br/>
                                    для предоставления в банковское<br/>
                                    отделение.
                                </div>
                            </div>
                            <div class="filter_block_body">
                                <div class="dom_icon"></div>
                                <div class="form_note">
                                    Посетите удобное отделение банка<br/>
                                    для завершения оформления<br/>
                                    выбранной услуги.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--step_filter_block-->
        </div>
        <div class="block_type_center">
            <div class="wr_form_application">
                <div class="form_application">
                    <div class="form_application_ok alt"></div>
                    <div class="form_application_title">
                        Ваша заявка принята!
                    </div>
                    <div class="form_application_text">
                        Номер заявки <b><?=(int)$_REQUEST["RESULT_ID"]?></b>. Подтверждение отправлено на e-mall. Срок<br/>
                        рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                        свяжется с Вами в ближайшее время.
                    </div>
                </div>
            </div>
        </div>
    </div>
<?}?>



