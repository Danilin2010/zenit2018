

$(document).ready(function(){

    var left_to_step='100%';
    var first_to_step='0px';
    var timer=500;
    var padding=40;
    padding=0;

    function MarginResize()
    {
        var to_step=$('.to_step_jr');
        var first_step=$('.first_step_jr');
        var position_to_step = $('.wr_form_application').offset();
        var to_step_=$(window).width()-position_to_step.left-padding + 100;
        left_to_step=to_step_+'px';
        first_to_step='-'+(position_to_step.left+first_step.width()-padding)+'px';
        if(!to_step.hasClass('win'))
            to_step.css('margin-left',left_to_step);
        if(first_step.hasClass('win'))
            first_step.css('left',first_to_step);
    }

    setTimeout(function(){MarginResize();},500);

    $(window).resize(function() {
        MarginResize();
    });

    setTimeout(function () {

        $("form[name=sponsorship]").validate({
            errorPlacement: function(error, element) {
                var parent=element.parents('.wr_complex_input');
                var parentcheckbox=element.parents('.jq-checkbox');
                if(parent.length>0) {
                    parent.addClass('error');
                    error.insertAfter(parent);
                }else if(parentcheckbox.length>0){
                    parentcheckbox.addClass('error');
                    error.insertAfter(parentcheckbox);
                }else{
                    error.insertAfter(element);
                }
            },
            success: function(error){
                error.prev('.jq-checkbox').removeClass('error');
                error.prev('.wr_complex_input').removeClass('error');
                error.remove();
            },

            submitHandler: function(form) {
                var $form = $(form);
                var url='/ajax/form/sponsorship.php';
                $.ajax({
                    url: url,
                    type: "POST",
                    data: $form.serialize(),
                    dataType: 'json',
                    success: function(data){

                        if(data.errors)
                        {
                            $form.find('.form_errors_text').text(data.errors);
                        }else{
                            if(data.result_id>0) {
                                $form.parents('.wr_form_application').find('[data-result]').text(data.result_id);
                            }
                            $form.parents('.wr_form_application').find('.first_step_jr').addClass('win').animate({'left': '-150%'},timer);
                            $form.parents('.wr_form_application').find('.to_step_jr').addClass('win').animate({'margin-left': "0px"},timer);
                            $form.parents('.wr_form_application').find('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
                            $form.parents('.wr_form_application').find('.wr_step_filter_block .step_filter_block:first').animate({'opacity': "0"},timer);
                        }
                    }
                });

            }
        });



        $.validator.addClassRules({
            js_company: {
                required: true,
            },
            js_project_name: {
                required: true,
            },
            js_date: {
                required: true,
            },
            ja_place: {
                required: true,
            },
            js_description: {
                required: true,
            },
            js_amount_size: {
                required: true,
            },
            js_capability: {
                required: true,
            },
            js_participation_others: {
                required: true,
            },
            js_person_charge: {
                required: true,
            },
            js_person_name: {
                required: true,
            },
            js_address: {
                required: true,
            },
            js_mail: {
                required: true,
                email2: true
            },
            js_other_info: {
                required: true,
            },
            js_agreement: {
                required: true,
            }
        });


        $.validator.messages.js_company = {
            required: 6666
        }



    },300);



    $('input[name="form_email_6"]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        showMaskOnFocus: false,
        showMaskOnHover: false,
        'placeholder':''
    });

});
