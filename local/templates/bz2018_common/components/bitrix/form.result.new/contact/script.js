
$(document).ready(function(){
    
    // Форма обратной связи

    var left_to_step='100%';
    var first_to_step='0px';
    var timer=500;
    var padding=40;
    padding=0;

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

        function MarginResize()
        {
            var to_step=$('.to_step');
            var first_step=$('.first_step');
            var position_to_step = $('.modal-open').offset();
            var to_step_=$(window).width()-position_to_step.left-padding + 100;
            left_to_step=to_step_+'px';
            first_to_step='-'+(position_to_step.left+first_step.width()-padding)+'px';
            if(!to_step.hasClass('win'))
                to_step.css('margin-left',left_to_step);
            if(first_step.hasClass('win'))
                first_step.css('left',first_to_step);
        }

        setTimeout(function(){MarginResize();},500);

        $(window).resize(function() {
            MarginResize();
        });

        setTimeout(function () {
            $("form[name=SIMPLE_FORM_2]").validate({
                errorPlacement: function(error, element) {
                    var parent=element.parents('.wr_complex_input');
                    var parentcheckbox=element.parents('.jq-checkbox');
                    if(parent.length>0) {
                        parent.addClass('error');
                        error.insertAfter(parent);
                    }else if(parentcheckbox.length>0){
                        parentcheckbox.addClass('error');
                        error.insertAfter(parentcheckbox);
                    }else{
                        error.insertAfter(element);
                    }
                },
                success: function(error){
                    error.prev('.jq-checkbox').removeClass('error');
                    error.prev('.wr_complex_input').removeClass('error');
                    error.remove();
                },
                rules:{
                    topic: {
                        required: true
                    },
                    form_text_10: {
                        required: true
                    },
                    form_text_11: {
                        pattern: /^\+7\(\d{3}\)\d{3}\-\d{2}\-\d{2}$/,
                        required: true
                    },
                    form_text_12: {
                        required: true,
                        email2: true
                    },
                    form_textarea_13: {
                        required: true
                    },
                    'form_checkbox_agreement[]': {
                        required: true
                    }
                },
                messages:{
                    topic: {
                        required: 'Укажите тему обращения'
                    },
                    form_text_10: {
                        required: 'Укажите фамилия, имя и отчество'
                    },
                    form_text_11: {
                        required: 'Укажите телефон',
                        pattern: 'Некорректно указан номер'
                    },
                    form_text_12: {
                        required: 'Укажите e-mail',
                        email2: 'Укажите правильный e-mail',
                    },
                    form_textarea_13: {
                        required: 'Укажите Ваш вопрос',
                    },
                    'form_checkbox_agreement[]': {
                        required: 'Нужно согласие'
                    }
                },
                submitHandler: function(form) {
                    var $form = $(form);
                    var url='/ajax/form/contact.php';
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: $form.serialize(),
                        dataType: 'json',
                        success: function(data){
                            if(data.errors)
                            {
                                $('.form_errors_text').text(data.errors);
                                console.log(data.errors);
                            }else{
                                if(data.result_id>0)
                                    $('[data-result]').text(data.result_id);
                                $('.first_step').addClass('win').animate({'left': first_to_step},timer);
                                $('.to_step').addClass('win').animate({'margin-left': "0px"},timer);
                                $('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
                                $('.wr_step_filter_block .step_filter_block:first').animate({'opacity': "0"},timer);
                            }
                        },
                        error: function (data) {
                            $('[data-result]').text(data.result_id);
                            $('.first_step').addClass('win').animate({'left': first_to_step},timer);
                            $('.to_step').addClass('win').animate({'margin-left': "0px"},timer);
                            $('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
                            $('.wr_step_filter_block .step_filter_block:first').animate({'opacity': "0"},timer);
                        }
                    });

                }
            });
        },300);

        $('input[name="form_text_12"]').inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&@'*+/=?^_`{|}~\-]",
                    cardinality: 1,
                    casing: "lower"
                }
            },
            showMaskOnFocus: false,
            showMaskOnHover: false,
            'placeholder':''
        });

        $("input[name=form_text_11]").inputmask({
            mask:"+7(999)999-99-99",
            showMaskOnHover: false
        });

    } else {

        function MarginResize()
        {
            var to_step=$('.to_step');
            var first_step=$('.first_step');
            var position_to_step = $('.modal-open').offset();
            var to_step_=$(window).width()-position_to_step.left-padding + 100;
            left_to_step=to_step_+'px';
            first_to_step='-'+(position_to_step.left+first_step.width()-padding)+'px';
            if(!to_step.hasClass('win'))
                to_step.css('margin-left',left_to_step);
            if(first_step.hasClass('win'))
                first_step.css('left',first_to_step);
        }

        setTimeout(function(){MarginResize();},500);

        $(window).resize(function() {
            MarginResize();
        });

        setTimeout(function () {
            $("form[name=SIMPLE_FORM_2]").validate({
                errorPlacement: function(error, element) {
                    var parent=element.parents('.wr_complex_input');
                    var parentcheckbox=element.parents('.jq-checkbox');
                    if(parent.length>0) {
                        parent.addClass('error');
                        error.insertAfter(parent);
                    }else if(parentcheckbox.length>0){
                        parentcheckbox.addClass('error');
                        error.insertAfter(parentcheckbox);
                    }else{
                        error.insertAfter(element);
                    }
                },
                success: function(error){
                    error.prev('.jq-checkbox').removeClass('error');
                    error.prev('.wr_complex_input').removeClass('error');
                    error.remove();
                },
                rules:{
                    topic: {
                        required: true
                    },
                    form_text_10: {
                        required: true
                    },
                    form_text_11: {
                        required: true,
                        pattern: /^\+7\(\d{3}\)\d{3}\-\d{2}\-\d{2}$/
                    },
                    form_text_12: {
                        required: true,
                        email: true
                    },
                    form_textarea_13: {
                        required: true
                    },
                    'form_checkbox_agreement[]': {
                        required: true
                    }
                },
                messages:{
                    topic: {
                        required: 'Укажите тему обращения'
                    },
                    form_text_10: {
                        required: 'Укажите фамилия, имя и отчество'
                    },
                    form_text_11: {
                        required: 'Укажите телефон',
                        pattern: 'Некорректно указан номер'
                    },
                    form_text_12: {
                        required: 'Укажите e-mail',
                        email2: 'Укажите правильный e-mail',
                    },
                    form_textarea_13: {
                        required: 'Укажите Ваш вопрос',
                    },
                    'form_checkbox_agreement[]': {
                        required: 'Нужно согласие'
                    }
                },
                submitHandler: function(form) {
                    var $form = $(form);
                    var url='/ajax/form/contact.php';
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: $form.serialize(),
                        dataType: 'json',
                        success: function(data){
                            if(data.errors)
                            {
                                $('.form_errors_text').text(data.errors);
                                console.log(data.errors);
                            }else{
                                if(data.result_id>0)
                                    $('[data-result]').text(data.result_id);
                                $('.first_step').addClass('win').animate({'left': first_to_step},timer);
                                $('.to_step').addClass('win').animate({'margin-left': "0px"},timer);
                                $('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
                                $('.wr_step_filter_block .step_filter_block:first').animate({'opacity': "0"},timer);
                            }
                        },
                        error: function (data) {
                            $('[data-result]').text(data.result_id);
                            $('.first_step').addClass('win').animate({'left': first_to_step},timer);
                            $('.to_step').addClass('win').animate({'margin-left': "0px"},timer);
                            $('.wr_step_filter_block').animate({'margin-left': "-100%"},timer);
                            $('.wr_step_filter_block .step_filter_block:first').animate({'opacity': "0"},timer);
                        }
                    });

                }
            });
        },300);

        $('input[name="form_text_10"]').inputmask(
            { regex: "[а-яА-Я ]*" }
        );

        $('input[name="form_text_12"]').inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    cardinality: 1,
                    casing: "lower"
                }
            },
            showMaskOnFocus: false,
            showMaskOnHover: false,
            'placeholder':''
        });

        $("input[name=form_text_11]").inputmask({
            mask:"+7(999)999-99-99",
            showMaskOnHover: false,
            placeholder:'+7(   )   -  -  '
        });
    }

});