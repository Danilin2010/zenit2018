<title>Маржинальная торговля</title>
<div class="wr_block_type">
	<div class="block_type to_column z-container">
		<?
		$APPLICATION->IncludeFile(
    		SITE_TEMPLATE_PATH."/inc/page-clone/brokerage-service/right_links.php",
    		Array(),
    		Array("MODE"=>"txt","SHOW_BORDER"=>false)
		);?>
		<div class="block_type_center">
			<div class="text_block">
				<table class="responsive-stacked-table tb" cellspacing="0" cellpadding="0">
				<tbody>
				<tr>
					<td>
						<p align="center">
							 &nbsp;
						</p>
					</td>
					<td>
						<p align="center">
							 Стоимость overnight, %
						</p>
					</td>
					<td>
						<p align="center">
							 Стоимость годовых, %
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p align="center">
							 Денежные средства
						</p>
					</td>
					<td>
						<p align="center">
							 0,041
						</p>
					</td>
					<td>
						<p align="center">
							 14,965
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p align="center">
							 Ценные бумаги
						</p>
					</td>
					<td>
						<p align="center">
							 0,041
						</p>
					</td>
					<td>
						<p align="center">
							 14,965
						</p>
					</td>
				</tr>
				</tbody>
				</table>
				<p>
					 Список ликвидных ценных бумаг, принимаемых ПАО Банком ЗЕНИТ в обеспечение при совершении маржинальных сделок, а также ставки рыночного риска можно посмотреть <a href="/upload/docs/brokerage/ZenitMarginList.xls">здесь</a>.
				</p>
				<p>
					 Процентная ставка по использованию кредитного плеча является плавающей и зависит от конъюнктуры денежного рынка в конкретный момент времени.&nbsp;
				</p>
				<p>
					 Система маржинальной торговли органично встроена в систему Интернет-трейдинга NetInvestor: клиенту предоставляется возможность мониторинга состояния и использования заемных активов и организация риск–менеджмента с возможностью выставления заявок типа «стоп–лосс».&nbsp;
				</p>
				<p>
					 Дополнительно к маржинальному кредитованию к услугам клиентов предоставляется возможность совершения биржевых сделок РЕПО, где процентная ставка и объем операции определяются в индивидуальном порядке.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
     <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "broker",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "result_edit.php",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "result_list.php",
        "RIGHT_TEXT" => "",
        "SEF_MODE" => "N",
        "SOURCE_TREATMENT" => "Заявка на брокерское обслуживание: ".$arResult["NAME"],
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
        "WEB_FORM_ID" => "16"
    )
);?>
</div>