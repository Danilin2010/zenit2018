<?	
	require ("_path.php");
	$path = $APPLICATION->GetCurPage();
	$url = Href($path);
?>

<div class="block_type_right">
	<div class="right_top_line">
		<div class="block_top_line"></div>
		<ul class="right_menu">
			<li>
				<a href="<? echo $url."brokerage-service/about-the-service/" ?>">Об услуге</a>
			</li>
			<li>
				<a href="<? echo $url."brokerage-service/one-account/" ?>">Единый торговый счет</a>
			</li>
			<li>
				<a href="<? echo $url."brokerage-service/repo-transactions/" ?>">Сделки РЕПО с ценными бумагами</a>
			</li>
			<li>
				<a href="<? echo $url."brokerage-service/margin-trading/" ?>">Маржинальная торговля</a>
			</li>
			<li>
				<a href="<? echo $url."brokerage-service/trading-systems/" ?>">Торговые системы</a>
			</li>
			<li>
				<a href="<? echo $url."brokerage-service/rates/" ?>">Тарифы</a>
			</li>
			<li>
				<a href="<? echo $url."brokerage-service/documents/" ?>">Документы</a>
			</li>
		</ul>
	</div>
</div>