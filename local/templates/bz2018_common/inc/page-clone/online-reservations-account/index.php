<?/*
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ОНЛАЙН-РЕЗЕРВИРОВАНИЕ СЧЕТА");
*/?>
    <div class="modal-open modal form">
        <div id="modal_online-reserv" class="modal_div">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="col-sm-12 col-md-12 modal_fon px-4">
                        <div class="row first_step">
                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<div class="button_two">
                                	<h2>Онлайн-резервирование номера счета</h2>
								</div>
                            </div>
                            <div class="col-sm-12 col-md-12">
								<p>Здравствуйте.</p>
								<p>В настоящее время в тестовом режиме запущен новый сервис онлайн-резервирования номера счета. Пока он доступен только пользователям Московского региона.<br>
									Вы из Москвы или Московской области?</p><br>
								<div class="button_two">
								<a href="https://ok.zenit.ru" rel="nofollow" class="button">ДА</a>
								<a href="https://old.zenit.ru/rus/businesss/operations/reserve_account/" rel="nofollow" class="button two" >НЕТ</a>
								</div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?/*require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");*/?>