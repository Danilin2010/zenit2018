<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Корпоративные карты");
$APPLICATION->SetPageProperty("description", "Корпоративные карты");
$APPLICATION->SetTitle("Корпоративные карты");
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">

<p>Банк ЗЕНИТ предлагает юридическим лицам и индивидуальным предпринимателям международные корпоративные карты платежных систем MasterCard Worldwide и Visa International.</p>
<p>Корпоративные карты Банка ЗЕНИТ являются надёжным и удобным средством оплаты представительских, хозяйственных и командировочных расходов в России и за рубежом.</p>
<h2>БЫСТРО, УДОБНО, ВЫГОДНО</h2>
<ul style="list-style-type: square;">
<li>Снятие наличных без предварительного заказа, оплата товаров и услуг - мгновенно</li>
<li>Снятие наличных круглосуточно в любом банкомате</li>
<li>Оплата товаров и услуги в торговых точках и сети интернет в рублях - без ограничений</li>
<li>Сокращение операционных расходов и времени, связанных с выдачей подотчетных сумм</li>
<li>Снижение риска потери или кражи наличных денежных средств</li>
<li>Возможность выпуска необходимого количества карт для вашего бизнеса и установление для каждой карты индивидуального лимита расходных операций</li>
<li>Контроль за расходованием средств</li>
<li>Минимизация риска превышения расходов</li>
<li>Повышение лояльности работников предприятия.</li>
</ul>
<h2>На какие цели можно использовать корпоративную карту</h2>
<ul style="list-style-type: square;">
<li>Безналичная оплата представительских, командировочных расходов</li>
	<li>Получение наличных денежных средств в рублях на хозяйственные и прочие расходы в соответствии с <a href="#" data-yourapplication="" data-classapplication=".tarifs" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">тарифами и условиями обслуживания</a></li>
<li>Карта не может быть использована для расчетов по оплате труда и выплат социального характера</li>
</ul>
<h2>Как оформить корпоративную карту в Банке ЗЕНИТ?</h2>
<ul style="list-style-type: square;">
<li>Обратитесь <!--в Банк ЗЕНИТ по телефону (495) 937-09-94 или--> в <a title="Банк ЗЕНИТ" href="/offices/">любой офис Банка</a>. <!--Также Вы можете оставить <a title="заявка на карту" href="/rus/businesss/operations/corp_cards/application.wbp">online-заявку</a> на сайте Банка.--></li>
<li>Заполните <a href="https://old.zenit.ru/media/rus/content/pdf/sme/application_ur_cards_account.pdf" target="_blank">заявление</a> для открытия счета, заключите <a href="https://old.zenit.ru/media/rus/content/pdf/sme/contract_ur_cards.pdf" target="_blank">Договор</a> банковского счета, предусматривающий проведение расчетов по операциям, совершаемым с использованием корпоративных банковских карт.</li>
<li>Определите круг ответственных сотрудников и оформите на их имя корпоративные карты, заполнив соответствующие <a href="https://old.zenit.ru/media/rus/content/pdf/sme/application_ur_cards.pdf" target="_blank">заявления</a>, и приложите копии паспортов держателей карт. При необходимости Вы всегда сможете заказать корпоративные карты в необходимом количестве на новых сотрудников своей компании.</li>
				</ul><span class="tarifs"></span>
<h2>Тарифы на выпуск и обслуживание корпоративных карт</h2>
<ul class="big_list">
<li><a href="https://old.zenit.ru/media/rus/content/pdf/corp_clients/tariffs_cards_ur_20161201.pdf" target="_blank">Тарифы по обслуживанию корпоративных банковских карт</a> <br /><a href="/media/rus/content/pdf/corp_clients/tariffs_cards_ur_20180301.pdf" target="_blank">Тарифы</a>, вступающие в действие 01.03.2018</li>
</ul>
<!--p>Если у вас еще нет расчетного счета в Банке ЗЕНИТ, всю необходимую информацию Вы можете получить в разделе &laquo;<a title="index" href="/rus/businesss/operations/index.wbp">Расчетно-кассовое обслуживание</a>&raquo;.</p-->
<!--p>Дополнительную информацию об условиях обслуживания корпоративных карт Вы можете получить, позвонив по телефону (495) 937-09-94, либо оставив&nbsp;<a title="заявка на карту" href="/rus/businesss/operations/corp_cards/application.wbp">online-заявку</a> на сайте Банка.</p-->

			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>