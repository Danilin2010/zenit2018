<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
        <?$APPLICATION->IncludeFile(
            SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
            Array(),
            Array("MODE"=>"txt","SHOW_BORDER"=>false)
        );?>
		</div>
		<h2>Раскрытие информации депозитарием ПАО Банк ЗЕНИТ:<br>архив документов</h2>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Дата окончания срока действия: 31.07.2017</h3>
				<ul class="big_list">
					<li>Дата вступления в действие: 05.04.2017<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20170731/CL_RGLMNT_20170405.pdf">Условия осуществления депозитарной деятельности</a>
					<ul>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20170731/9E5967BD-C2BA-5383-0094-B318D804BF49/reports_20170405.rar">формы документов, предоставляемые депозитарием депонентам</a> </li>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20170731/9E5967BD-C2BA-5383-0094-B318D804BF49/instructions_20170405.rar">формы документов, предоставляемые депонентами в депозитарий</a> </li>
					</ul>
 </li>
				</ul>
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Дата окончания срока действия: 04.04.2017</h3>
				<ul class="big_list">
					<li>Дата вступления в действие: 04.10.2016<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20170404/CL_RGLMNT_20161004.pdf">Условия осуществления депозитарной деятельности</a>
					<ul>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20170404/33127DCD-C2BA-5383-011C-135CC1734D18/reports_20161004.rar">формы документов, предоставляемые депозитарием депонентам</a> </li>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20170404/33127DCD-C2BA-5383-011C-135CC1734D18/instructions_20161004.rar">формы документов, предоставляемые депонентами в депозитарий</a> </li>
					</ul>
 </li>
				</ul>
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Дата окончания срока действия: 03.10.2016</h3>
				<ul>
					<li>Дата вступления в действие: 01.07.2016<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20161003/CL_RGLMNT_20160701.pdf">Условия осуществления депозитарной деятельности</a>
					<ul class="big_list">
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20161003/forms/reports_20160701.rar">формы документов, предоставляемые депозитарием депонентам</a> </li>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20161003/forms/instructions_20160701.rar">формы документов, предоставляемые депонентами в депозитарий</a> </li>
					</ul>
 </li>
				</ul>
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Дата окончания срока действия: 30.06.2016</h3>
				<ul class="big_list">
					<li>Дата вступления в действие: 13.06.2016<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20160630/CL_RGLMNT_20160613.pdf">Условия осуществления депозитарной деятельности</a>
					<ul>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20160630/C06448B7-C2BA-5383-0049-3DCA2FAA5DCA/instructions_20160613.rar">формы документов, предоставляемые депонентами в депозитарий</a> </li>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20160630/C06448B7-C2BA-5383-0049-3DCA2FAA5DCA/reports_20160613.rar">формы документов, предоставляемые депозитарием депонентам</a> </li>
					</ul>
 </li>
				</ul>
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Дата окончания срока действия: 12.06.2016</h3>
				<ul class="big_list">
					<li>Дата вступления в действие: 01.02.2016<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20160612/CL_RGLMNT_20160201.pdf">Условия осуществления депозитарной деятельности</a>
					<ul>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20160612/4EEF6E3A-C2BA-5383-0049-3DCAA9CBF94A/instructions_20160612.rar">формы документов, предоставляемые депонентами в депозитарий</a>
 </li>
						<li>в том числе<br>
 <a target="_blank" href="/upload/depositary/archive_docs/finished_20160612/4EEF6E3A-C2BA-5383-0049-3DCAA9CBF94A/reports.rar">формы документов, предоставляемые депозитарием депонентам</a>
 </li>
					</ul>
 </li>
				</ul>
 			</div>
		</div>
	</div>
</div>