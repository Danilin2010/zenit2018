<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h3>Важная информация</h3>
				<p>
					 Доводим до Вашего сведения об изменениях, вступающих в действие с 29.02.2016, при оказании услуг в рамках перевода ценных бумаг с местом расчетов в НКО ЗАО НРД. В поручениях на перевод ценных бумаг (формы 06, 08) с местом расчетов в НКО ЗАО НРД вводится обязательно сверяемые поля «Дата (период времени) исполнения» и «Дата сделки», заполнение которых является обязательным. Данные требования связаны с изменениями в процедурах расчетов операций в НКО ЗАО НРД.
				</p>
			</div>
			<p>
				 Прием поручений и (или) других документов от Депонентов (уполномоченных представителей) осуществляется по рабочим дням с 9.00 до 18.00 (до 16.45 по пятницам и на 1 час меньше в предпраздничные дни) по московскому времени.
			</p>
			<p>
				 Поручение считается принятым к исполнению текущим операционным днем при условии, что поручение, включая документы, необходимые для его исполнения, получены Депозитарием не позднее 17.00 (не позднее 15.45 по пятницам и на 1 час ранее в предпраздничные дни) текущего операционного дня.
			</p>
			<p>
				 В случае, если поручения и (или) другие документы приняты по завершении указанного времени, они принимаются к исполнению следующим операционным днем.
			</p>
			<h2>ПРИМЕРЫ ЗАПОЛНЕНИЯ ПОРУЧЕНИЙ&nbsp;НА ВЫПОЛНЕНИЕ&nbsp;ДЕПОЗИТАРНЫХ ОПЕРАЦИЙ</h2>
			<h3>Образцы заполнения поручений при зачислении ценных бумаг</h3>
			<ul class="big_list">
				<li><a href="/upload/depositary/06_reestr.pdf" target="_blank">Зачисление из реестра</a></li>
				<li><a href="/upload/depositary/06_dkk.pdf" target="_blank">Зачисление из ЗАО «ДКК»</a></li>
				<li><a href="/upload/depositary/06_nrd.pdf" target="_blank">Зачисление из НКО ЗАО «НРД»</a>&nbsp;</li>
				<li><a href="/upload/depositary/06_clearstream.pdf" target="_blank">Зачисление из Сlearstream banking S.A.</a></li>
				<li><a href="/upload/depositary/06_Euroclear.pdf" target="_blank">Зачисление из Euroclear</a></li>
				<li><a href="/upload/depositary/06_ZENIT.pdf" target="_blank">Зачисление внутри ПАО Банк ЗЕНИТ</a></li>
			</ul>
			<h3>Образцы заполнения поручений при списании ценных бумаг</h3>
			<ul class="big_list">
				<li><a href="/upload/depositary/08_reestr.pdf" target="_blank">Списание в реестр</a></li>
				<li><a href="/upload/depositary/08_dkk.pdf" target="_blank">Списание в ЗАО «ДКК»</a></li>
				<li><a href="/upload/depositary/08_NRD.pdf" target="_blank">Списание в НКО ЗАО «НРД»</a></li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/A1AE1B77-1D93-4821-A2A8-F126E4C9CA55/08_Clearstream.pdf" target="_blank">Списание в Сlearstream banking S.A.</a></li>
				<li><a href="/upload/depositary/08_Euroclear.pdf" target="_blank">Списание в Euroclear</a>&nbsp;</li>
				<li><a href="/upload/depositary/08_ZENIT.pdf" target="_blank">Списание&nbsp; внутри ПАО Банк ЗЕНИТ</a></li>
				<li><a href="/upload/depositary/6_8__INSTRUCTION_DELIVER_NORNICKEL.pdf" target="_blank">Списание акций ОАО ГМК Норильский никель</a></li>
			</ul>
			<h3>Сроки исполнения операций</h3>
			<p>
 <a href="/upload/depositary/time_list_cst.pdf" target="_blank">График исполнения</a>&nbsp;отдельных депозитарных операций.
			</p>
		</div>
	</div>
</div>