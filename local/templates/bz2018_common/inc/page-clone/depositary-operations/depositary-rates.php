<div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Действующие</a></li>
			<li><a href="#content-tabs-2">Архив тарифов</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<?$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
							Array(),
							Array("MODE"=>"txt","SHOW_BORDER"=>false)
						);?>
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								<a href="/upload/depositary/tarifs/tariffs_depo_20180201.pdf" target="_blank">Тарифы</a> за услуги депозитария, действующие с 01.02.2018 г.<br>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<table cellpadding="0" cellspacing="0">
							<tbody>
							<tr>
								<td>
									<p>
										 Наименование документа
									</p>
								</td>
								<td>
									<p>
										 Документ
									</p>
								</td>
								<td>
									<p>
										 Период действия
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tariffs_depo_20161201.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.12.2016 по 31.01.2018
									</p>
								</td>
							</tr>

							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tariffs_depo_20160111.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 11.01.2016 по 30.11.2016
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tariffs_depo_20150901.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.09.2015 по 10.01.2016
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tariffs_depo_20150601.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.06.2015 по 31.08.2015
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tariffsDepo_20140601.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.06.2014 по 31.05.2015
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tariffsDepo_2013.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.05.2013 по 31.05.2014
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tarifdep_010410.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 31.03.2010 по 30.04.2013
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tarifdep_010509_n.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.05.2009 по 31.03.2010
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/tarifdep_new.pdf" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.07.2007 по 30.04.2009
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Тарифы
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/Tariff_off_30.06.2007.doc" target="_blank">Тарифы за услуги депозитария</a>
									</p>
								</td>
								<td>
									<p>
										 с 01.02.2001 по 30.06.2007
									</p>
								</td>
							</tr>
							<tr>
								<td>
									<p>
										 Прейскурант
									</p>
								</td>
								<td>
									<p>
 <a href="/upload/depositary/tarifs/preyskurant.pdf" target="_blank">Прейскурант услуг по перерегистрации прав на ценные бумаги</a>
									</p>
								</td>
								<td>
									<p>
										 до 30.04.2009
									</p>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>