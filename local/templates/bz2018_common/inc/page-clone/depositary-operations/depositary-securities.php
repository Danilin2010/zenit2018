<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p>
					<a target="_blank" href="/upload/depositary/securities/common_list_2318.xls">Общий список ценных бумаг</a>, принятых на обслуживание по состоянию на 01.03.2018 г.
				</p>
				<p>
					<a target="_blank" href="/upload/depositary/securities/not_in_list_2318.xls">Список иностранных финансовых инструментов</a>, не квалифицированных в качестве ценных бумаг по состоянию на 01.03.2018 г.
				</p>
				<p>
					<a target="_blank" href="/upload/depositary/securities/in_list_2318.xls">Список иностранных ценных бумаг</a>, квалифицированных в качестве ценных бумаг по состоянию на 01.03.2018 г.
				</p>
			</div>
		</div>
	</div>
</div>