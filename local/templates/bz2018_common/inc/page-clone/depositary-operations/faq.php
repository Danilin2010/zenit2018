<div class="wr_block_type">
    <div class="block_type to_column c-container">
        <div class="block_type_right">
            <?$APPLICATION->IncludeFile(
                SITE_TEMPLATE_PATH."/inc/page-clone/depositary-operations/right_links.php",
                Array(),
                Array("MODE"=>"txt","SHOW_BORDER"=>false)
            );?>
        </div>
        <div class="block_type_center">
            <div class="text_block">
				<div class="faq">
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Что нужно для открытия счета депо в Депозитарии физическому лицу?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Физическое лицо для открытия счета депо в Депозитарии должно предоставить анкету депонента для физических лиц, два экземпляра депозитарного договора и копию документа, удостоверяющего личность. Подробнее с требованиями можно ознакомиться в Общих условиях осуществления депозитарной деятельности.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Что нужно для открытия счета депо в Депозитарии юридическому лицу?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Юридическое лицо для открытия счета депо в Депозитарии должно предоставить анкету депонента для юридических лиц, два экземпляра депозитарного договора и нотариально заверенные копии учредительных документов. Подробнее с требованиями можно ознакомиться в Общих условиях осуществления депозитарной деятельности.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Нужно ли уведомлять налоговую инспекцию об открытии счета депо?</div>
						</div>
						<div class="faq_text" style="display: none;">
							По состоянию на текущий момент уведомлять налоговую инспекцию об открытии счета депо не требуется.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Могут ли нерезиденты открывать счета в Депозитарии?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Ограничений для нерезидентов РФ для открытия счета депо не существует.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Где можно прочитать о тарифах Депозитария?</div>
						</div>
						<div class="faq_text" style="display: none;">
							С тарифами Депозитария Вы можете ознакомиться в п. 2.6.6. раздела Депозитарные операции, а также обратившись за конультацией к сотрудникам Депозитария.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Взимается ли плата за хранение с физических лиц, находящихся на брокерском обслуживании?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Плата за хранение ценных бумаг с физических лиц, находящихся на брокерском обслуживании не взимается.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Возможна ли оплата депозитарных комиссий с брокерского счета?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Да, возможна. Вы должны выбрать вариант оплаты счетов в анкете депонента.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Как рассчитывается плата за хранение?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Плата за хранение ценных бумаг рассчитывается согласно тарифам депозитария. С тарифами депозитария Вы может ознакомиться в разделе 2.6.6.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Можно ли передать поручение в Депозитарий в электронном виде?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Да. Для этого Вам потребуется заключить договора с Депозитарием для работы по системе iDepo или дополнительные соглашения к счету депо для работы по системе S.W.I.F.T. или Telex.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Что нужно чтобы подключиться к системе iDepo?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Для работы в системе iDepo Вам необходимо заключить соглашение о работе в этой системе и пройди процедуру подключения. Более подробную информацию Вы можете найти в разделе 2.6.10. Электронный документооборот.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Какие форматы S.W.I.F.T. Депозитарий использует для взаимодействия с депонентом?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Депозитарий использует для взаимодействия с депонентом 5XX форматы в соответствии с рекомендациями S.W.I.F.T. Более подробную информацию Вы можете найти в разделе 2.6.10. Электронный документооборот.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Где можно посмотреть полный список депозитарных услуг, предоставляемых Депозитарием?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Полный список депозитарных услуг, предоставляемых Депозитарием вы можете посмотреть в Общих условия осуществления депозитарной деятельности в разделе 2.6.7.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Где можно взять бланки поручений?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Типовые формы поручений Вы можете взять в разделе 2.6.7.
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">Как мне заполнить поручение?</div>
						</div>
						<div class="faq_text" style="display: none;">
							Примеры заполнения типовых форм поручений приведены в разделе <a href="/personal/invest/depositary-operations/depositary-operations/">"Проведение операций"</a>.
						</div>
					</div>


				</div>
            </div>
        </div>
    </div>
</div>