<div class="block_type to_column c-container">
    <div class="block_type_right">
        <!--step_filter_block-->
        <div class="wr_step_filter_block_b">
            <div class="wr_step_filter_block">
                <div class="step_filter_block">
                    <div class="filter_block">
                        <div class="filter_block_title">
                            Онлайн-заявка
                        </div>
                        <div class="filter_block_body">
                            <div class="time_icon"></div>
                            <div class="form_note">
                                Заполните заявку.<br/>
                                Это займет не более 10 минут.
                            </div>
                            <div class="form_note bottom">
                                <div class="form_note_left">
                                    *
                                </div>
                                <div class="form_note_right">
                                    Все поля обязательны<br/>
                                    для заполнения
                                </div>
                            </div>
                        </div>
                    </div>
                </div><div class="step_filter_block">
                    <div class="filter_block">
                        <div class="filter_block_title">
                            Онлайн заявка
                        </div>
                        <div class="filter_block_body">
                            <div class="new_icon"></div>
                            <div class="form_note">
                                Подготовьте необходимые документы<br/>
                                для предоставления в банковское<br/>
                                отделение.
                            </div>
                        </div>
                        <div class="filter_block_body">
                            <div class="dom_icon"></div>
                            <div class="form_note">
                                Посетите удобное отделение банка<br/>
                                для завершения оформления<br/>
                                выбранной услуги.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--step_filter_block-->
    </div>
    <div class="block_type_center">
        <!--form_application-->
        <div class="wr_form_application">
            <div class="step_form_application first_step">
                <form action="" class="validate application">
                    <div class="form_application">
                        <?/*
                        <div class="form_application_line">
                            <select disabled data-plasholder="Выберите кредит" data-title="интересующая услуга" class="formstyle" name="service">
                                <option>Выберите кредит</option>
                                <option selected>Кредит без обеспечения</option>
                            </select>
                        </div>
                        */?>
                        <div class="form_application_line">
                            <div class="form_application_tree">
                                <div class="wr_complex_input complex_input_noicon">
                                    <div class="complex_input_body">
                                        <div class="text">Фамилия</div>
                                        <input type="text" name="surname" class="simple_input" placeholder="" value="">
                                    </div>
                                </div>
                            </div><div class="form_application_tree">
                                <div class="wr_complex_input complex_input_noicon">
                                    <div class="complex_input_body">
                                        <div class="text">Имя</div>
                                        <input type="text" name="name" class="simple_input" placeholder="" value="">
                                    </div>
                                </div>
                            </div><div class="form_application_tree">
                                <div class="wr_complex_input complex_input_noicon">
                                    <div class="complex_input_body">
                                        <div class="text">Отчество</div>
                                        <input type="text" name="patronymic" class="simple_input" placeholder="" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div class="text">город</div>
                                    <input type="text" name="sity" class="simple_input" placeholder="" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div class="text">телефон</div>
                                    <input type="text" class="simple_input" name="phone" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div class="text">e-mail</div>
                                    <input type="text" name="email" class="simple_input" placeholder="" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_line form_application_line_button_line">
                            <div class="form_application_button_left">
                                <label><input class="formstyle" type="checkbox" name="agree" value="Y"/>Я согласен с <a href="">условиями</a> передачи информации</label>
                            </div>
                            <div class="form_application_button_right">
                                <input class="button" value="далее" type="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="step_form_application to_step">
                <div class="form_application">
                    <div class="form_application_ok"></div>
                    <div class="form_application_title">
                        Ваша заявка принята!
                    </div>
                    <div class="form_application_text">
                        Номер заявки <b>36958</b>. Подтверждение отправлено на e-mail.<br/>
                        Представитель банка свяжется с Вами в ближайшее время.
                    </div>
                </div>
            </div>
        </div>
        <!--form_application-->
    </div>
</div>