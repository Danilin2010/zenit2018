<a class="header-links__item header-links__item_search-control" @click.prevent="setSearchState()">
    <svg class="icon header-links__icon icon__search-icon-symbol">
        <use xlink:href="#icon__search-icon-symbol"></use>
    </svg>
    <span class="text text_bold header-links__desktop-text">Поиск</span>
</a>