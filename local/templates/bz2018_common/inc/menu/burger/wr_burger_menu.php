<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="burger_menu">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
			<div class="burger_menu_title">Кредиты</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Ипотечное кредитование</span>
						<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"simple_menu",
								array(
								"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
									"CHILD_MENU_TYPE" => "mortgage",	// Тип меню для остальных уровней
									"DELAY" => "N",	// Откладывать выполнение шаблона меню
									"MAX_LEVEL" => "1",	// Уровень вложенности меню
									"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
										0 => "",
									),
									"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
									"MENU_CACHE_TYPE" => "A",	// Тип кеширования
									"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
									"ROOT_MENU_TYPE" => "mortgage",	// Тип меню для первого уровня
									"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
								)
							);?>
					</li>
					<li><span>Потребительское кредитование</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "carloans",
								"IBLOCK_ID" => "43",
								"NEWS_COUNT" => "4",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>
					<li><span>Автокредитование</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "carloans",
								"IBLOCK_ID" => "32",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>
				</ul>
			</div>
			<div class="burger_menu_title">Карты</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Кредитные карты</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "credit_cards",
								"IBLOCK_ID" => "29",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "1",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>
					<li><span>Дебетовые карты</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "credit_cards",
								"IBLOCK_ID" => "29",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "2",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>
				</ul>
			</div>
        </div><div class="burger_menu_item">
			<div class="burger_menu_title">Вклады</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Максимальная гибкость</span>
						<?
						/*global $carloans_top_menu;
						$carloans_top_menu=array(
							"IBLOCK_SECTION_ID"=>false,
						);*/
						$APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "deposits",
								"IBLOCK_ID" => "49",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "carloans_top_menu",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
                                    0 => "",
                                    1 => "SHARE_CONTRIBUTION",
                                    3 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "254",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>

					<li><span>Максимальный доход</span>
						<?
						/*global $carloans_top_menu;
						$carloans_top_menu=array(
							"IBLOCK_SECTION_ID"=>false,
						);*/
						$APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "deposits",
								"IBLOCK_ID" => "49",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "carloans_top_menu",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
									0 => "",
									1 => "SHARE_CONTRIBUTION",
									3 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "253",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>

					<li><span>Накопление</span>
						<? $APPLICATION->IncludeComponent(
							"bitrix:news.list",
							"simple_menu",
							array(
								"COMPONENT_TEMPLATE" => "carloans",
								"IBLOCK_TYPE" => "deposits",
								"IBLOCK_ID" => "49",
								"NEWS_COUNT" => "20",
								"SORT_BY1" => "SORT",
								"SORT_ORDER1" => "ASC",
								"SORT_BY2" => "SORT",
								"SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "",
								"FIELD_CODE" => array(
									0 => "",
									1 => "",
								),
								"PROPERTY_CODE" => array(
                                    0 => "",
                                    1 => "SHARE_CONTRIBUTION",
                                    3 => "",
								),
								"CHECK_DATES" => "Y",
								"DETAIL_URL" => "",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"AJAX_OPTION_ADDITIONAL" => "",
								"CACHE_TYPE" => "N",
								"CACHE_TIME" => "3600",
								"CACHE_FILTER" => "N",
								"CACHE_GROUPS" => "Y",
								"PREVIEW_TRUNCATE_LEN" => "",
								"ACTIVE_DATE_FORMAT" => "d.m.Y",
								"SET_TITLE" => "N",
								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
								"ADD_SECTIONS_CHAIN" => "N",
								"HIDE_LINK_WHEN_NO_DETAIL" => "N",
								"PARENT_SECTION" => "9",
								"PARENT_SECTION_CODE" => "",
								"INCLUDE_SUBSECTIONS" => "N",
								"STRICT_SECTION_CHECK" => "N",
								"DISPLAY_DATE" => "N",
								"DISPLAY_NAME" => "N",
								"DISPLAY_PICTURE" => "N",
								"DISPLAY_PREVIEW_TEXT" => "N",
								"PAGER_TEMPLATE" => ".default",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "N",
								"PAGER_TITLE" => "Новости",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "N",
								"PAGER_BASE_LINK_ENABLE" => "N",
								"SET_STATUS_404" => "N",
								"SHOW_404" => "N",
								"MESSAGE_404" => ""
							),
							false
						); ?>
					</li>

					<li><span>Специальные предложения</span></li>
					<!--<li><a target="_blank" href="https://www.zenit.ru/rus/retail/vklady/archive/">Архив вкладов</a></li>-->
					<?/*<li><span>Пенсионное обеспечение</span>
                        <ul>
                            <li><a href="#">Индивидуальные инвестиционные счета</a></li>
                            <li><a href="#">Индексируемые депозиты</a></li>
                            <li><a href="#">ПИФы (Паевые Инвестиционные Фонды)</a></li>
                            <li><a href="#">ОФБУ (Общие Фонды Банковского Управления)</a></li>
                        </ul>
                    </li>*/?>
				</ul>
			</div>
			<div class="burger_menu_title">Платежи и переводы</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><a href="/personal/payments/payment">Оплата услуг</a></li>
					<li><span>Переводы</span>
					<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
						"ADD_SECTIONS_CHAIN" => "N",
						"CACHE_GROUPS" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "N",
						"COUNT_ELEMENTS" => "N",
						"IBLOCK_ID" => "56",
						"IBLOCK_TYPE" => "payments",
						"SECTION_CODE" => "",
						"SECTION_FIELDS" => array(),
						"SECTION_ID" => "",
						"SECTION_URL" => "/personal/payments/transfers/#SECTION_CODE#/",
						"SECTION_USER_FIELDS" => array(),
						"SHOW_PARENT_NAME" => "N",
						"TOP_DEPTH" => "2",
						"VIEW_MODE" => "LINE",
						"CSS_CLASS" => ""
					)); ?>
					</li>
				</ul>
			</div>
			<div class="burger_menu_title">Индивидуальные сейфы</div>
			<div class="burger_menu_block">
				<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
					"ADD_SECTIONS_CHAIN" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "N",
					"COUNT_ELEMENTS" => "N",
					"IBLOCK_ID" => "60",
					"IBLOCK_TYPE" => "safes",
					"SECTION_CODE" => "",
					"SECTION_FIELDS" => array(),
					"SECTION_ID" => "",
					"SECTION_URL" => "/personal/safes/#SECTION_CODE#/",
					"SECTION_USER_FIELDS" => array(),
					"SHOW_PARENT_NAME" => "N",
					"TOP_DEPTH" => "2",
					"VIEW_MODE" => "LINE",
					"CSS_CLASS" => "burger_menu_menu"
				)); ?>
			</div>
        </div><div class="burger_menu_item">
			<div class="burger_menu_title">Страхование</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><span>Страхование жизни и здоровья</span>
						<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "N",
							"COUNT_ELEMENTS" => "N",
							"IBLOCK_ID" => "57",
							"IBLOCK_TYPE" => "insurance",
							"SECTION_CODE" => "",
							"SECTION_FIELDS" => array(),
							"SECTION_ID" => "",
							"SECTION_URL" => "/personal/insurance/life/#SECTION_CODE#/",
							"SECTION_USER_FIELDS" => array(),
							"SHOW_PARENT_NAME" => "N",
							"TOP_DEPTH" => "2",
							"VIEW_MODE" => "LINE",
							"CSS_CLASS" => ""
						)); ?>
					</li>
					<li><span>Страхование имущества</span>
						<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
							"ADD_SECTIONS_CHAIN" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "N",
							"COUNT_ELEMENTS" => "N",
							"IBLOCK_ID" => "58",
							"IBLOCK_TYPE" => "insurance",
							"SECTION_CODE" => "",
							"SECTION_FIELDS" => array(),
							"SECTION_ID" => "",
							"SECTION_URL" => "/personal/insurance/property/#SECTION_CODE#/",
							"SECTION_USER_FIELDS" => array(),
							"SHOW_PARENT_NAME" => "N",
							"TOP_DEPTH" => "2",
							"VIEW_MODE" => "LINE",
							"CSS_CLASS" => ""
						)); ?>
					</li>
				</ul>
			</div>
			<div class="burger_menu_title">Пенсионное обеспечение</div>
			<div class="burger_menu_block">
				<? $APPLICATION->IncludeComponent("bitrix:catalog.section.list", "menu", Array(
					"ADD_SECTIONS_CHAIN" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "N",
					"COUNT_ELEMENTS" => "N",
					"IBLOCK_ID" => "59",
					"IBLOCK_TYPE" => "pension",
					"SECTION_CODE" => "",
					"SECTION_FIELDS" => array(),
					"SECTION_ID" => "",
					"SECTION_URL" => "/personal/pension/#SECTION_CODE#/",
					"SECTION_USER_FIELDS" => array(),
					"SHOW_PARENT_NAME" => "N",
					"TOP_DEPTH" => "2",
					"VIEW_MODE" => "LINE",
					"CSS_CLASS" => "burger_menu_menu"
				)); ?>
			</div>

			<div class="burger_menu_title">Дистанционное банковское обслуживание</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li>
						<?= \helpers\Helper::a(
								'/personal/remote-service/internet-bank/',
								'Интернет-банк и мобильное приложение<br> «ЗЕНИТ Онлайн»'
						);?>
					</li>
				</ul>
			</div>

			<div class="burger_menu_title">Тарифы</div>
			<div class="burger_menu_block">
				<ul class="burger_menu_menu">
					<li><a href="/personal/tariffs/">Тарифы для частных лиц</a></li>
				</ul>
			</div>

		</div><div class="burger_menu_item">
            <?/*
            <div class="burger_menu_title">Другие услуги</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="#">Дистанционное обслуживание</a></li>
                    <li><a href="#">Переводы</a></li>
                    <li><a href="#">Расчетно-кассовое обслуживание</a></li>
                    <li><span>Индивидуальные сейфы</span>
                        <ul>
                            <li><a href="#">Максимальный доход</a></li>
                            <li><a href="#">Дальновидный выбор</a></li>
                            <li><a href="#">Быстрый старт</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Личный адвокат</a></li>
                </ul>
            </div>
            <div class="burger_menu_title">Предложения для военных</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Военная ипотека</span>
                        <ul>
                            <li><a href="#">Максимальный доход</a></li>
                            <li><a href="#">Дальновидный выбор</a></li>
                            <li><a href="#">Быстрый старт</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Потребительский кредит</a></li>
                </ul>
            </div>
            */?>
        </div>
    </div>
</div>