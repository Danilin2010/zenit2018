<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_loans">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <div class="menu_bottom_block_title">Кредиты</div>
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/loans/working-capital-loan/">Оборотное кредитование</a></li>
                    <li><a href="/businesses/loans/overdraft/">Овердрафт</a></li>
                    <li><a href="/businesses/loans/tender-loan/">Тендерный кредит</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="menu_bottom_block_title">Инвестиционное кредитование</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/loans/investment-lending/real-estate/">Недвижимость</a></li>
                    <li><a href="/businesses/loans/investment-lending/equipment-and-transport/">Оборудование и
                            транспорт</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="menu_bottom_block_title">Государственная поддержка субъектов МСБ</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/loans/state-support-of-smes/program-6-5-jsc-corporation-smes/">Программа
                            6,5 АО "Корпорации "МСП"</a></li>
                    <li>
                        <a href="/businesses/loans/state-support-of-smes/warranty-support-of-jsc-corporation-smes-jsc-sme-bank/">Гарантийная
                            поддержка АО "Корпорация "МСП/АО "МСП Банк"</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="menu_bottom_block_title">Гарантии</div>
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/businesses/warranty/electronic-guarantee-for-the-participants-of-public-procurement/">Электронная
                            гарантия для участников госзакупок</a></li>
                    <li><a href="/businesses/warranty/commercial-warranty/">Коммерческие гарантии</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="menu_bottom_block_title"><a href="/businesses/loans/factoring/">Факторинг</a></div>
            <div class="burger_menu_block">
                <!--ul class="burger_menu_menu">
                    <li><a href="/businesses/loans/factoring/">Факторинг</a></li>
                </ul-->
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="menu_bottom_block_title"></div>
            <div class="burger_menu_block">
                <!--ul class="burger_menu_menu">
                    <li><a href="/businesses/loans/factoring/">Факторинг</a></li>
                </ul-->
            </div>
        </div>
    </div>

</div>

