<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="big_business_financing">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/big-business/funding/overdraft/">Овердрафт</a></li>
                    <li><a href="/big-business/funding/loans-for-working-capital/">Кредитование на пополнение<br>оборотных
                            средств</a></li>
                    <li><a href="/big-business/funding/investment-lending/">Инвестиционное кредитование</a></li>
                </ul>
            </div>
        </div>

        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/big-business/funding/project-financing">Проектное финансирование</a></li>
                    <li><span>Торговое финансирование</span>
                        <ul>
                            <li><a href="/big-business/funding/foreign-trade-investment/">Финансирование по программам
                                    Банка
                                    ЗЕНИТ</a></li>
                            <li><a href="/big-business/funding/exiar-coverage/">Финасирование под страховое покрытие АО
                                    ЭКСАР</a></li>
                            <li><a href="/big-business/funding/foreign-trade-investment/">Финансирование импорта под
                                    страховое
                                    покрытие ЭКА</a></li>
                        </ul>
                    </li>
					<li><a href="/big-business/funding/commercial-warranty/">Банковские гарантии</a></li>
                </ul>
            </div>
        </div>

        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li>
                        <span>Организация привлечения<br> долгового финансирования</span>
                        <ul>
                            <li>
                                <a href="/big-business/funding/organization-of-debt-financing/promissory-notes-program/">Выпуск
                                    вексельной программы</a></li>
                            <li><a href="/big-business/funding/organization-of-debt-financing/bonded-loan-issue/">Выпуск
                                    облигационного займа</a></li>
                            <li><a href="/big-business/funding/organization-of-debt-financing/syndicated-lending/">Синдицированное
                                    кредитование</a></li>
                            <li><a href="/big-business/funding/organization-of-debt-financing/issue-of-eurobonds/">Выпуск
                                    еврооблигаций и кредитных нот (CLN, LPN)</a></li>
                            <li>
                                <a href="/big-business/funding/organization-of-debt-financing/debt-and-distressed-assets/">Услуги
                                    по реструктуризации долга и по работе с проблемными активами</a></li>
                            <li><a href="/big-business/funding/organization-of-debt-financing/support-issuers/">Поддержка
                                    эмитентов при прохождении оферт, погашения долга.</a></li>
                            <li><a href="#">Бридж-финансирование</a></li>
                        </ul>
                    </li>
                    <li><a href="/big-business/funding/factoring/">Факторинг</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
