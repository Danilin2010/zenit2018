<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="big_business_placement_funds">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Срочное размещение</span>
                        <ul>
                            <li><a href="/big-business/allocation-of-funds/deposit-products/term-deposits">Депозиты</a>
                            </li>
                            <li><a href="/big-business/allocation-of-funds/deposit-products/special-deposit-overhaul">Специальный депозит капитального ремонта</a></li>
                            <li><a href="/big-business/allocation-of-funds/deposit-products/dual-currency-deposit">Бивалютный
                                    депозит</a></li>
                            <li>
                                <a href="/big-business/allocation-of-funds/deposit-products/minimum-balance-on-the-account">Неснижаемый
                                    остаток на расчетном счете</a></li>
                        </ul>
                    </li>
                    <? /*<li><a href="/big-business/documentary-operations/">Документарные операции</a></li>*/ ?>
                </ul>
                <? /*<ul class="burger_menu_menu">
                    <li><a href="#">Как разместить средства в Банке?</a></li>
                    <li><a href="#">Депозиты</a></li>
                </ul>*/ ?>
            </div>
        </div>
		<div class="burger_menu_item">
			<ul class="burger_menu_menu">
				<li><span>Ценные бумаги</span>
					<ul>
						<li><a href="#">Векселя</a></li>
						<li><a href="/big-business/allocation-of-funds/securities/certificates-of-deposit/">Депозитные
								сертификаты</a></li>
					</ul>
				</li>
			</ul>
		</div>
        <? /*
        <div class="burger_menu_item">
            <div class="burger_menu_block">
              <ul class="burger_menu_menu">
                    <li><a href="#">Неснижаемый остаток на расчетном счете</a></li>
                    <li><a href="#">Векселя</a></li>
                </ul>
            </div>
        </div>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                        <li><a href="#">Депозитные сертификаты</a></li>
                        <li><a href="#">Бивалютный депозит</a></li>
                    </ul>
            </div>
        </div>
        */ ?>
    </div>
</div>