<div class="wr_menu_bottom wr_burger_menu" style="display: none" id="big_business_accompaniment_ved">
    <div class="burger_menu z-container">
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/big-business/international-payments/conversion-operations/">Конверсионные операции</a></li>
                    <li><a href="/big-business/international-payments/clearing/">Операции с клиринговыми и <br>национальными валютами</a>
                    </li>
                </ul>
            </div>
        </div>
        <?/*<div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><span>Операции с клиринговыми валютами</span>
                        <ul>
                            <li><a href="#">Участие в тендерах Внешэкономбанка</a></li>
                            <li><a href="#">Конверсия в клиринговых валютах</a></li>
                            <li><a href="#">Обсуживание расчетов в клиринговых валютах</a></li>
                        </ul>
                    </li>
                    <li><a href="/big-business/international-payments/foreign-trade-investment/">Торговое финансирование</a></li>
                    <li><a href="/big-business/international-payments/exiar-coverage/">Финасирование под страховое<br>покрытие АО ЭКСАР</a></li>
                </ul>
            </div>
        </div>*/?>
        <div class="burger_menu_item">
            <div class="burger_menu_block">
                <ul class="burger_menu_menu">
                    <li><a href="/big-business/international-payments/foreign-trade-investment/">Инвестиционное финансирование<br> внешнеторговых операций</a></li>
                    <li><a href="/big-business/international-payments/documentary-operations/">Документарные операции</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>