<div class="wr_menu_bottom" id="menu_net_bank" style="display: none;">
    <div class="menu_bottom z-container">
        <div class="menu_bottom_block">
            <a href="https://my.zenit.ru/wb/" class="menu_bottom_snippet">
                <div class="menu_bottom_snippet_pict simpl"></div>
                <div class="menu_bottom_snippet_text">
                    <b>ЗЕНИТ Онлайн</b><br>
                    для личных финансов
                </div>
                <div class="button transparent min_height">Войти</div>
                <div class="bottom_blank">
                    Мобильный банк
                    <ul class="mobile_banking_line_icon">
						<li><div class="mobile_banking_line_icon apple"><div></div></div></li>
						<li><div class="mobile_banking_line_icon android"><div></div></div></li>
                    </ul>
                </div>
            </a>
        </div><div class="menu_bottom_block">
            <a href="https://old.zenit.ru/cb/" class="menu_bottom_snippet">
                <div class="menu_bottom_snippet_pict bisness"></div>
                <div class="menu_bottom_snippet_text">
                    <b>ЗЕНИТ Бизнес</b><br>
                    для компаний
                </div>
                <div class="button transparent min_height">Войти</div>
                <div class="bottom_blank">
                    Мобильный банк-клиент
                    <ul class="mobile_banking_line_icon">
						<li><div class="mobile_banking_line_icon apple"><div></div></div></li>
						<li><div class="mobile_banking_line_icon android"><div></div></div></li>
                    </ul>
                </div>
            </a>
        </div><div class="menu_bottom_block">
            <div class="menu_bottom_block_paggin">
                <?/*<div class="menu_bottom_block_title">Справочная информация</div>
                <ul class="menu_bottom_block_ul">
                    <li><a href="#">Как подключиться к интернет-банку</a></li>
                    <li><a href="#">Частые вопросы</a></li>
                    <li><a href="#">Тарифы и документы</a></li>
                    <li><a href="#">Дополнительная информация</a></li>
                </ul>*/?>
            </div>
        </div>
    </div>
</div>