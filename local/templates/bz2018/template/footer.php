<?php
/**
 * @global \CMain $APPLICATION
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
?>
</main>
<footer class="footer">
    <div class="footer__wrapper">
        <div class="footer__inner">
            <div class="footer__numbers-wrapper">
                <div class="footer__number-item">
                    <div class="footer__number-title">Бесплатный звонок</div>
                    <?$APPLICATION->IncludeFile(
						"/inc/footer-phone-free.php",
                        Array(),
                        Array("SHOW_BORDER"=>true,"MODE"=>"php")
                    );?>
                </div>
                <div class="footer__number-item">
                    <div class="footer__number-title">Если вы за границей</div>
                    <?$APPLICATION->IncludeFile(
						"/inc/footer-phone-abroad.php",
                        Array(),
                        Array("SHOW_BORDER"=>true,"MODE"=>"php")
                    );?>
                </div>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "footer_site_links_wrapper", Array(
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "ROOT_MENU_TYPE" => "footer_links",	// Тип меню для первого уровня
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                "COMPONENT_TEMPLATE" => ".default"
            ),
                false
            );?>
            <div class="footer__external-links-wrapper">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "mobile_applications",
                    array(
                        "COMPONENT_TEMPLATE" => "main_actions",
                        "IBLOCK_TYPE" => "mobile_applications",
                        "IBLOCK_ID" => IBLOCK_MOBILE_APPLICATIONS,
                        "NEWS_COUNT" => "10",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "",
                        "FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "link",
                            2 => "img",
                            3 => "",
                        ),
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "SET_TITLE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "STRICT_SECTION_CHECK" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "PAGER_TITLE" => "Новости",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => ""
                    ),
                    false
                );?>
				<?php $APPLICATION->IncludeFile(
					"/inc/footer-social.php",
					Array(),
					Array("MODE" => "php", "SHOW_BORDER" => false)
				) ?>
            </div>
        </div>
		<div class="footer__more-info">
			<div class="footer__info-item footer__info-item_copyright">
				<?php $APPLICATION->IncludeFile(
					"/inc/footer-license.php",
					Array(),
					Array("MODE" => "txt", "SHOW_BORDER" => false)
				) ?>
			</div>
			<div class="footer__info-item footer__info-item_link">
				<?php $APPLICATION->IncludeFile(
					"/inc/footer-text.php",
					Array(),
					Array("MODE" => "txt", "SHOW_BORDER" => false)
				) ?>
			</div>
			<div class="footer__info-item footer__info-item_link">
				<?php $APPLICATION->IncludeFile(
					"/inc/footer-info.php",
					Array(),
					Array("MODE" => "txt", "SHOW_BORDER" => false)
				) ?>
			</div>
		</div>
    </div>
</footer>


<script src="<?=SITE_TEMPLATE_PATH?>/scripts/vendor.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/scripts/app.min.js"></script>


</body>
</html>