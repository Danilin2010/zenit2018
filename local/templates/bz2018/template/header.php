<?php
/**
 * @global \CMain $APPLICATION
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
	die();
}
use lukoil\helpers\PrepMeta;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

$request = Application::getInstance()->getContext()->getRequest();
$currentDirectory = $request->getRequestedPageDirectory();
$currentPage = $request->getRequestedPage();

$asset = Asset::getInstance();

?><!DOCTYPE html>
<html class="html html_no-js" lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="HandheldFriendly" content="true">
    <meta name="MobileOptimized" content="width">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#0a1244">
    <meta name="msapplication-navbutton-color" content="#0a1244">
    <meta name="apple-mobile-web-app-status-bar-style" content="#0a1244">

    <title><?php $APPLICATION->ShowTitle() ?></title>
    <?php $APPLICATION->ShowHead() ?>

    <!-- Styles-->
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/app.min.css">

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/favicon.ico" type="image/x-icon">
    <!--if !global.isDevelopment && global.head.androidIcons-->

    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/android-chrome-36.png" sizes="36" type="image/png">
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/android-chrome-48.png" sizes="48" type="image/png">
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/android-chrome-72.png" sizes="72" type="image/png">
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/android-chrome-96.png" sizes="96" type="image/png">
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/android-chrome-144.png" sizes="144" type="image/png">
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicons/android-chrome-192.png" sizes="192" type="image/png">
    <link rel="manifest" href="<?=SITE_TEMPLATE_PATH?>/favicons/manifest.json">

    <meta content="app-id=1187159866" name="apple-itunes-app">
    <meta content="app-id=ru.zenit.zenitonline" name="google-play-app">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KCQB8S');</script>
    <!-- End Google Tag Manager -->

</head>
<body class="page">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KCQB8S";
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<? $APPLICATION->ShowPanel() ?>
<div id="svg-sprite" data-src="<?=SITE_TEMPLATE_PATH?>/img/sprites.svg"></div>
<header class="header" id="header-app">
    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("inc/informer.php"),
        Array(),
        Array("SHOW_BORDER"=>false)
    );?>
    <div class="header__wrapper">
        <div class="header__inner">
            <a class="main-logo main-logo_mobile" href="/">
                <!--+e('image', {t: 'img'})(src=`${global.paths.static}/${BEMPUG.getCurrentBlock()}/main-logo.svg` alt='Законодательное собрание')-->
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("svg/main-logo.svg"),
                    Array(),
                    Array("SHOW_BORDER"=>false)
                );?>
            </a>
            <?$APPLICATION->IncludeComponent("bitrix:menu", "client_type_menu_desktop", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "client_type_menu",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
            <div class="header-links header__links" :class="{&quot;header-links_state-hidden&quot;: menuStateOpen}" v-if="!searchStateOpen">
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("inc/header-links-location.php"),
                    Array(),
                    Array("SHOW_BORDER"=>true,"MODE"=>"php")
                );?>
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("inc/header-links-contacts.php"),
                    Array(),
                    Array("SHOW_BORDER"=>true,"MODE"=>"php")
                );?>
                <?$APPLICATION->IncludeFile(
                    $APPLICATION->GetTemplatePath("inc/header-links-search.php"),
                    Array(),
                    Array("SHOW_BORDER"=>true,"MODE"=>"php")
                );?>
            </div>
            <!-- Основное меню-->
            <div class="main-menu" :class="{&quot;main-menu_state-open&quot;: menuStateOpen}">
                <a class="main-logo main-logo_desktop" href="/" title="На главную">
                    <!--+e('image', {t: 'img'})(src=`${global.paths.static}/${BEMPUG.getCurrentBlock()}/main-logo.svg` alt='Законодательное собрание')-->
                    <?$APPLICATION->IncludeFile(
                        $APPLICATION->GetTemplatePath("svg/main-logo_desktop.svg"),
                        Array(),
                        Array("SHOW_BORDER"=>false)
                    );?>
                </a>
                <button class="main-menu__menu-control" type="button" @click.prevent="setMobileMenuState()" v-if="!searchStateOpen">
                    <span class="text text_semi-bold main-menu__burger-text">Меню</span>
                    <span class="burger-button main-menu__burger-button" :class="{&quot;burger-button_state-active&quot;: menuStateOpen}">
								<span class="burger-button__line burger-button__line_1"></span>
								<span class="burger-button__line burger-button__line_2"></span>
								<span class="burger-button__line burger-button__line_3"></span>
							</span>
                </button>
                <nav class="main-menu__list-wrapper" :style="{height: popupHeight, marginTop: menuMarginTop + &quot;px&quot;}">
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "client_type_menu_mobile", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "client_type_menu",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "COMPONENT_TEMPLATE" => ".default"
                    ),
                        false
                    );?>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "button-internet-bank", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "internet_banking_menu",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main_menu_main_links", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "main_menu",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "main_menu_main_links",
		"NUMBER_DISPLAYED" => "6"
	),
	false
);?>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "main_menu_footer_links", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "footer_links",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                        "COMPONENT_TEMPLATE" => ".default"
                    ),
                        false
                    );?>
                </nav>
                <button class="main-menu__search-control" type="button" @click.prevent="setSearchState()">
                    <span class="text text_semi-bold main-menu__text">Поиск</span>
                    <svg class="icon main-menu__search-icon icon__search-icon-symbol">
                        <use xlink:href="#icon__search-icon-symbol"></use>
                    </svg>
                </button>
                <?$APPLICATION->IncludeComponent("bitrix:menu", "button-internet-bank-desktop", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "internet_banking_menu",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
);?>
            </div>
        </div>
        <div class="contacts-popup" :style="{height: popupHeight, marginTop: menuMarginTop + &quot;px&quot;}" :class="{&quot;contacts-popup_state-open&quot;: contactsStateOpen}">
            <div class="contacts-popup__wrapper">
                <div class="contacts-popup__inner">
                    <div class="contacts-popup__close-button" @click.prevent="setContactsState()">
                        <svg class="icon icon__close-symbol">
                            <use xlink:href="#icon__close-symbol"></use>
                        </svg>
                    </div>
                    <div class="contacts-popup__title">Связаться с нами</div>
                    <?/*<div class="contacts-popup__button-wrapper">
                        <a class="button button_primary contacts-popup__question-button">Задать вопрос</a>
                    </div>*/?>
                    <div class="contacts-popup__numbers-wrapper">
                        <div class="contacts-popup__numbers-inner contacts-popup__numbers-inner_peoples">
							<div class="contacts-popup__numbers-title">Частным лицам</div>
							<div class="contacts-popup__number-item">
								<?php $APPLICATION->IncludeFile(
									"/inc/header-phone-business-mobile.php",
									Array(),
									Array("SHOW_BORDER" => true ,"MODE" => "php")
								); ?>
								<div class="contacts-popup__number-title">Бесплатно с мобильных</div>
							</div>
                            <div class="contacts-popup__number-item">
								<?php $APPLICATION->IncludeFile(
									"/inc/header-phone-personal-free.php",
									Array(),
									Array("SHOW_BORDER" => true ,"MODE" => "php")
								); ?>
                                <div class="contacts-popup__number-title">Бесплатно по России</div>
                            </div>
                            <div class="contacts-popup__number-item">
								<?php $APPLICATION->IncludeFile(
									"/inc/header-phone-personal-abroad.php",
									Array(),
									Array("SHOW_BORDER" => true, "MODE" => "php")
								); ?>
                                <div class="contacts-popup__number-title">Если вы за границей</div>
                            </div>
                        </div>
                        <div class="contacts-popup__numbers-inner contacts-popup__numbers-inner_business">
                            <div class="contacts-popup__numbers-title">Бизнесу</div>
                            <div class="contacts-popup__number-item">
								<?php $APPLICATION->IncludeFile(
									"/inc/header-phone-business-free.php",
									Array(),
									Array("SHOW_BORDER" => true, "MODE" => "php")
								); ?>
                                <div class="contacts-popup__number-title">Бесплатно по России</div>
                            </div>
                            <div class="contacts-popup__number-item">
								<?php $APPLICATION->IncludeFile(
									"/inc/header-phone-business-abroad.php",
									Array(),
									Array("SHOW_BORDER" => true, "MODE" => "php")
								); ?>
                                <div class="contacts-popup__number-title">Если вы за границей</div>
                            </div>
                        </div>
                    </div>
					<?php $APPLICATION->IncludeFile(
						"/inc/header-social.php",
						Array(),
						Array("MODE" => "php", "SHOW_BORDER" => false)
					) ?>
                </div>
            </div>
        </div>
        <!-- Форма поиска-->
        <div class="main-search" :style="{height: popupHeight, marginTop: menuMarginTop + &quot;px&quot;}" :class="{&quot;main-search_state-open&quot;: searchStateOpen}">
            <div class="main-search__wrapper">
                <div class="main-search__inner">
                    <form class="main-search__form" action="/search.php" @submit.prevent="getSearchResult($event)" autocomplete="off">
                        <div class="main-search__field-wrapper">
                            <input class="main-search__field" ref="search" name="search_string" v-model="searchString" placeholder="Я ищу...">
                            <a class="main-search__clear-field" href="#" v-if="searchString.length" @click.prevent="clearSearchField()">
                                <svg class="icon icon__close-symbol">
                                    <use xlink:href="#icon__close-symbol"></use>
                                </svg>
                            </a>
                        </div>
                        <button class="button button_primary main-search__search-button">Найти</button>
                        <a class="main-search__close-button" href="#" @click.prevent="setSearchState()">
                            <svg class="icon icon__close-symbol">
                                <use xlink:href="#icon__close-symbol"></use>
                            </svg>
                        </a>
                    </form>
                    <div class="main-search__result" v-if="searchResult.status === 200 &amp;&amp; searchResult.body.success">
                        <div class="main-search__result-category" v-if="searchResult.body.result.length" v-for="category in searchResult.body.result">
                            <div class="main-search__category-title">{{ category.title }}</div>
                            <div class="main-search__result-item" v-for="item in category.items">
								<a class="main-search__item-link" :href="item.link" v-html="item.name"></a>
                            </div>
                        </div>
                        <div class="main-search__error" v-if="!searchResult.body.result.length">Ничего не найдено</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>



<main class="content">





