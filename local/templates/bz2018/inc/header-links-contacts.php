<a class="header-links__item" href="#" @click.prevent="setContactsState()">
    <svg class="icon header-links__icon icon__contacts-icon-symbol">
        <use xlink:href="#icon__contacts-icon-symbol"></use>
    </svg>
    <span class="text text_semi-bold header-links__mobile-text">Контакты</span>
    <span class="text text_bold header-links__desktop-text">Контакты</span>
</a>