<a class="header-links__item" href="/offices/?type=atm">
    <svg class="icon header-links__icon icon__map-marker-symbol">
        <use xlink:href="#icon__map-marker-symbol"></use>
    </svg>
    <span class="text text_semi-bold header-links__mobile-text">На карте</span>
    <span class="text text_bold header-links__desktop-text">Банкоматы и офисы</span>
</a>