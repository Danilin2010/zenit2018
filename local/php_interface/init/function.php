<?php
function sklonenie($n, $forms) {
    return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
}

function GetPrettyNumber($number) {
    (intval($number) > 999999) ? $precision = 6: $precision = 3; //количество вырезаемых символов
    if($precision == 6 )
    {
        $number=round($number/1000000,2);
        $summ = "млн.";
    }else{
        $number=round($number/1000,2);
        $summ = "тыс.";
    }
    //$number = substr($number, 0, -$precision);
    return '<span class="numer">' . $number . '</span> ' . $summ . ' ';
}

function GetPrettySum($number) {
    (intval($number) > 999999) ? $precision = 6: $precision = 3; //количество вырезаемых символов
    if($precision == 6 )
    {
        $number=round($number/1000000,2);
        $summ = "млн.";
    }else{
        $number=round($number/1000,2);
        $summ = "тыс.";
    }
    //$number = substr($number, 0, -$precision);
    return $number . '<span> ' . $summ . '</span> ';
}

function GetItem($val,$note=false)
{
    $res = CIBlockElement::GetByID($val);
    if($ar_res = $res->GetNext())
    {
        $new=array(
            "NAME"=>$ar_res["NAME"],
            "SORT"=>$ar_res["SORT"],
            "PREVIEW_TEXT"=>$ar_res["~PREVIEW_TEXT"],
        );
        if($ar_res["PREVIEW_PICTURE"]>0)
        {
            $arFile = CFile::GetFileArray($ar_res["PREVIEW_PICTURE"]);
            $arFile["SRC"] = CFile::GetPath($ar_res["PREVIEW_PICTURE"]);
            $new["PREVIEW_PICTURE"]=$arFile;
        }
        if($note)
        {
            $new["NOTE"]=array();
            $resNOTE = CIBlockElement::GetProperty($ar_res["IBLOCK_ID"],$ar_res["ID"],"sort","asc",array("CODE" =>$note));
            while ($ob = $resNOTE->GetNext())
            {
                $resNOTEEl = CIBlockElement::GetByID($ob['VALUE']);
                if($ar_resNOTEEl = $resNOTEEl->GetNext())
                {
                    $new["NOTE"][]=array(
                        "ID"=>$ar_resNOTEEl["ID"],
                        "NAME"=>$ar_resNOTEEl["NAME"],
                        "PREVIEW_TEXT"=>$ar_resNOTEEl["~PREVIEW_TEXT"],
                    );
                }
            }
        }
        return $new;
    }
    return false;
}

function GetPrice($val)
{
    if($val>=1000000)
    {
        $res=array(
            "SUMM"=>round($val/1000000,2),
            "SUFF"=>"млн.",
        );
    }else{
        $res=array(
            "SUMM"=>round($val/1000,2),
            "SUFF"=>"тыс.",
        );
    }
    return $res;
}

function GetFile($Name,$FileValue)
{
    if($FileValue["ID"])
        $FileValue=array($FileValue);
    $res=array();
    foreach ($FileValue as $File)
    {
        $file_size=$File["FILE_SIZE"];
        if($file_size<1024)
        {
            $file_size=$file_size." Б";
        }elseif($file_size<(1024*1024)){
            $file_size=round($file_size/1024,2)." Кб";
        }else{
            $file_size=round($file_size/1024/1024,2)." Мб";
        }
        preg_match('/.+\.(\w+)$/xis', $File["SRC"], $pocket);
        $type=mb_strtolower($pocket[1]);
        $res=array(
            "TYPE"=>$type,
            "NAME"=>$Name,
            "FILE_SIZE" => $file_size,
            "SRC"=>$File["SRC"],
        );
    }
    return $res;
}

function PrintList($DisplayProperties,$arrElements,$class="big_list")
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]["ELEMENT_VALUE"]){?>
            <h2><?=$DisplayProperties[$code]["NAME"]?></h2>
            <ul class="<?=$class?>">
                <?foreach ($DisplayProperties[$code]["ELEMENT_VALUE"] as $key=>$val){?>
                    <li>
                        <?=$val["NAME"]?>
                    </li>
                <?}?>
            </ul>
        <?}
    }
}

function PrintPromoBenefits($DisplayProperties,$arrElements)
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]["ELEMENT_VALUE"]){
            ?><div class="promo_benefits"><?
            foreach ($DisplayProperties[$code]["ELEMENT_VALUE"] as $key=>$val)
            {
                ?><div class="promo_benefits_item">
                <div class="pict" style="background-image: url('<?=$val["PREVIEW_PICTURE"]["SRC"]?>');"></div>
                <div class="body">
                    <div class="title">
                        <?=$val["NAME"]?>
                    </div>
                    <div class="text">
                        <?=$val["PREVIEW_TEXT"]?>
                    </div>
                </div>
                </div><?
            }
            ?></div><?
        }
    }
}

function PrintRightList($DisplayProperties,$arrElements)
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]["ELEMENT_VALUE"]){
            foreach ($DisplayProperties[$code]["ELEMENT_VALUE"] as $key=>$val)
            {
                ?><div class="right_bank_block">
                <div class="right_bank_title">
                    <?=$val["NAME"]?>
                </div>
                <div class="right_bank_text">
                    <?=$val["PREVIEW_TEXT"]?>
                </div>
                </div><?
            }
        }
    }
}

function PrintTextElement($DisplayProperties,$arrElements)
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]["ELEMENT_VALUE"]){?>
            <h2><?=$DisplayProperties[$code]["NAME"]?></h2>
            <p>
                <?foreach ($DisplayProperties[$code]["ELEMENT_VALUE"] as $key=>$val){?>
                        <?=$val["PREVIEW_TEXT"]?>
                <?}?>
            </p>
        <?}
    }
}

function PrintBigElement($DisplayProperties,$arrElements,$showTitle=true,$showFrame=true,$hnum=1)
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]["DISPLAY_VALUE"]){?>
            <?if($showFrame){?><div class="wr_block_type"><div class="block_type c-container"><div class="block_type_center"><?}?>
            <?if($showTitle){?><h<?=$hnum?>><?=$DisplayProperties[$code]["NAME"]?></h<?=$hnum?>><?}?>
            <?=$DisplayProperties[$code]["DISPLAY_VALUE"]?>
            <?if($showFrame){?></div></div></div><?}?>
        <?}
    }
}

function PrintBigFile($DisplayProperties,$arrElements,$UseFrame=true)
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]){
            $FileValue=$DisplayProperties[$code]["FILE_VALUE"];
            if($FileValue["ID"])
                $FileValue=array($FileValue);
            ?>
            <?if($UseFrame){?><div class="text_block"><?}?>
                <!--doc-->
                <div class="doc_list"><?
                    foreach ($FileValue as $File) {
                        $Description=$File["DESCRIPTION"];
                        if(strlen($Description)<=0)
                            $Description=$DisplayProperties[$code]["NAME"];
                        $nFile=GetFile($Description,$File);
                        ?><a href="<?=$File["SRC"]?>" target="_blank" class="doc_item">
                            <div class="doc_pict <?=$nFile["TYPE"]?>"></div>
                            <div class="doc_body">
                                <div class="doc_text">
                                    <?=$nFile["NAME"]?>
                                </div>
                                <div class="doc_note">
                                    <?=$nFile["FILE_SIZE"]?>
                                </div>
                            </div>
                        </a><?
                    }
                ?></div>
                <!--doc-->
            <?if($UseFrame){?></div><?}?>
        <?}
    }
}

function PrintFaq($DisplayProperties,$arrElements)
{
    foreach ($arrElements as $code)
    {
        if($DisplayProperties[$code]["ELEMENT_VALUE"]){?>
            <h1><?=$DisplayProperties[$code]["NAME"]?></h1>
            <div class="text_block">
                <!--faq-->
                <div class="faq">
                    <?foreach ($DisplayProperties[$code]["ELEMENT_VALUE"] as $key=>$val){?>
                        <div class="faq_item">
                            <div class="faq_top">
                                <div class="faq_pict"><div class="faq_arr"></div></div>
                                <div class="faq_top_text"><?=$val["NAME"]?>?</div>
                            </div>
                            <div class="faq_text">
                                <?=$val["PREVIEW_TEXT"]?>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        <?}
    }
}

function formatFileSize($size)
{
	$formattedSize = 0;
	$unit = 'Б';

	if ($size / 1000000 < 1)
	{
		if ($size / 1000 < 1)
		{
			if ($size / 1 < 1)
			{

			}
			else
			{
				$formattedSize = number_format($size / 1, 2, ',', ' ');
			}
		}
		else
		{
			$formattedSize = number_format($size / 1000, 2, ',', ' ');
			$unit = 'К' . $unit;
		}
	}
	else
	{
		$formattedSize = number_format($size / 1000000, 2, ',', ' ');
		$unit = 'М' . $unit;
	}

	return $formattedSize . ' ' . $unit;
}

function getFileIconClass($file)
{
	$class = '';
	$arClass = [
		'doc',
		'pdf',
		'xls'
	];

	$arFile = explode('.', $file);
	$extensions = end($arFile);

	if (!empty($extensions))
	{
		foreach ($arClass as $cssClass)
		{
			$found = preg_match('/' . $cssClass . '/', $extensions, $matches);

			if ($found)
			{
				$class = $cssClass;
			}
		}
	}

	return $class;
}

function multisort($array, $index)
{
    foreach($array as $key=>$arr)
        $new_arr[$key]=$arr[$index];
    asort($new_arr);
    $keys = array_keys($new_arr);
    foreach($keys as $key)
        $result[$key] = $array[$key];
    return $result;
}

function GetElementValue($Value,$Nation="NATION")
{
    $ElementValue=array();
    if(is_array($Value))
    {
        foreach ($Value as $val)
            if($ar_res = GetItem($val,$Nation))
                $ElementValue[]=$ar_res;
    }else{
        if($ar_res = GetItem($Value,$Nation))
            $ElementValue[]=$ar_res;
    }
    $ElementValue= multisort($ElementValue, "SORT");
    return $ElementValue;
}
/**
 *  Функция отправки рассылки при добавлении новости
 */
function OnAfterElementAddHandler (&$arFields)
{
    $RUB_ID = 1;
    $arNew = array();

    //file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log.txt','<pre>' . print_r($arFields, 1) . '</pre>', FILE_APPEND);
    \CModule::IncludeModule('iblock');
    if ($arFields['IBLOCK_ID'] == 3 && $arFields['ACTIVE'] == 'Y' )
    {
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "DETAIL_TEXT");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>intval($arFields['IBLOCK_ID']), "ID" => intval($arFields["ID"]), "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

        while($ob = $res->GetNextElement())
        {
            $arProp['fields'] = $ob->GetFields();
            $arProp['props'] = $ob->GetProperties();

        }
        $arNew["id"] = $arProp['fields']["ID"];
        $arNew["name"] = $arProp['fields']["NAME"];
        $arNew["preview"] = $arProp['fields']["PREVIEW_TEXT"];
        $arNew["detail"] = $arProp['fields']["DETAIL_TEXT"];
        $arNew["link"] = $arProp['fields']["DETAIL_PAGE_URL"];
        $arNew["source_link"] = $arProp["props"]["source_link"]["VALUE"];
        $arNew["source_author"] = $arProp["props"]["source_link"]["VALUE"];


        //file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ar-log.txt','<pre>' . print_r($arNew, 1) . '</pre>', FILE_APPEND);

    }
    if(\CModule::IncludeModule("subscribe"))
    {
        //активные и подтвержденные адреса, подписанные на рубрики
        /*$subscr = CSubscription::GetList(
			array("ID"=>"ASC"),
			array("RUBRIC"=>$RUB_ID, "CONFIRMED"=>"Y", "ACTIVE"=>"Y",)
		);
		while(($subscr_arr = $subscr->Fetch()))
		{
			$arID[] = $subscr_arr["ID"];
			$aEmail[] = $subscr_arr["EMAIL"];
		} Пока не нужны */
        $link = "http://".$_SERVER['SERVER_NAME'].$arNew["link"];
        //$aEmail = implode(", ",$aEmail);
        $SUBJECT = "Рассылка новостей с сайта Банка Зенит";
        $BODY = "<a href='".$link."'>".$link."</a><br>
				<h1>" .$arNew["name"]. "</h1><br>
				<b>".$arNew["preview"]."</b><br><br>
				<div>".$arNew["detail"]."</div><br>
				<p><a href='http://".$_SERVER['SERVER_NAME']."about/unsuscribe.php?mid=#MAIL_ID#&mhash=#MAIL_MD5#'>Чтобы отписаться от рассылки пройдите по данной ссылке</a></p>";
        $CHARSET = "Windows-1251";
        //$EMAIL_FILTER = $aEmail;
        $DIRECT_SEND = "Y";
        $BODY_TYPE = "html";

        $posting = new CPosting;
        $arFields = Array(
            "FROM_FIELD" => "no-reply@zenit.bk",
            "TO_FIELD" => "abramov@aicrobotics.ru",
            "SUBJECT" => $SUBJECT,
            "BODY_TYPE" => ($BODY_TYPE <> "html"? "text":"html"),
            "BODY" => $BODY,
            "DIRECT_SEND" => ($DIRECT_SEND <> "Y"? "N":"Y"),
            "CHARSET" => $CHARSET,
            "RUB_ID" => array($RUB_ID),
        );
        if($STATUS <> "")
        {
            if($STATUS<>"S" && $STATUS<>"E" && $STATUS<>"P")
                $STATUS = "D";
            $arFields["STATUS"] = $STATUS;
            if($STATUS == "D")
            {
                $arFields["DATE_SENT"] = false;
                $arFields["SENT_BCC"] = "";
                $arFields["ERROR_EMAIL"] = "";
            }
        }
        $ID = $posting->Add($arFields);
        if($ID == false)
            echo $posting->LAST_ERROR;
        if($ID)
        {
            $posting->ChangeStatus($ID, "P");
            $posting->AutoSend($ID);
        }
    }


}

/** Функция определения наличия подписки у текущего пользователя
 * @param $suscrId id подписки
 *
 * @return bool
 */
function checkSubscibe($suscrId)
{
    CModule::IncludeModule("subscribe");
    $aSubscr = CSubscription::GetUserSubscription();
    $aSubscrRub = CSubscription::GetRubricArray(intval($aSubscr["ID"]));
    if($aSubscr['EMAIL'] && in_array(intval($suscrId),$aSubscrRub) && $aSubscr['ACTIVE'] == "Y" )
    {
        return $aSubscr;
    } else
    {
        return false;
    }
}

/** По email возвращает id подписки
 * @param $sf_EMAIL
 *
 * @return int
 */
function getSubscrId($sf_EMAIL)
{
    $subscription = CSubscription::GetByEmail($sf_EMAIL);
    if($res = $subscription->ExtractFields("str_"))
        return intval($res["ID"]);

}
