<?php
class BufferContent
{

    function ShowTitle($name){
        global $APPLICATION;
        echo $APPLICATION->AddBufferContent(Array('BufferContent', "GetTitle"),$name);
    }

    function SetTitle($name,$value){
        global ${$name};
        ${$name}=$value;
    }

    function GetTitle($name){
        global ${$name};
        if(!${$name})
            ${$name}="";
        return ${$name};
    }

}