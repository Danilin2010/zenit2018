<?php

function my_onBeforeResultAdd($WEB_FORM_ID, &$arFields, &$arrVALUES)
{
    if (Bitrix\Main\Loader::includeModule('aic.bz'))
    {
        $Geo=new \Aic\Bz\cOnlineReserve();
        $Geo->onBeforeResultAdd($WEB_FORM_ID, $arFields, $arrVALUES);
    }
}

AddEventHandler('form', 'onBeforeResultAdd', 'my_onBeforeResultAdd');