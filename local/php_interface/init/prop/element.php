<?
AddEventHandler("iblock",'OnIBlockPropertyBuildList',array('ChekboxElementList','GetUserTypeDescription'));


class ChekboxElementList
{
    function GetUserTypeDescription()
    {
        return array(
            'PROPERTY_TYPE'   => 'E',
            'USER_TYPE'       => 'ChekboxElementList',
            'DESCRIPTION'         => 'Привязка к элементам в виде chekbox',
            "GetPropertyFieldHtml" => array("ChekboxElementList","GetPropertyFieldHtml"),
            "GetPropertyFieldHtmlMulty" => array("ChekboxElementList","GetPropertyFieldHtmlMulty"),
            "GetPublicEditHTML" => array("CIBlockPropertyElementList","GetPropertyFieldHtml"),
            "GetPublicEditHTMLMulty" => array("CIBlockPropertyElementList","GetPropertyFieldHtmlMulty"),
            "GetPublicViewHTML" => array("CIBlockPropertyElementList", "GetPublicViewHTML"),
            "GetAdminFilterHTML" => array("CIBlockPropertyElementList","GetAdminFilterHTML"),
            "PrepareSettings" =>array("CIBlockPropertyElementList","PrepareSettings"),
            "GetSettingsHTML" =>array("CIBlockPropertyElementList","GetSettingsHTML"),
            "GetExtendedValue" => array("CIBlockPropertyElementList", "GetExtendedValue"),


        );
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $bWasSelect = false;
        $options = ChekboxElementList::GetOptionsHtmlChekbox($arProperty, array($value["VALUE"]), $strHTMLControlName,$bWasSelect);
        $html = $options;
        return  $html;
    }

    function GetElements($IBLOCK_ID)
    {
        static $cache = array();
        $IBLOCK_ID = intval($IBLOCK_ID);

        if(!array_key_exists($IBLOCK_ID, $cache))
        {
            $cache[$IBLOCK_ID] = array();
            if($IBLOCK_ID > 0) {
                $arSelect = array(
                    "ID",
                    "NAME",
                    "PREVIEW_TEXT",
                    "IN_SECTIONS",
                    "IBLOCK_SECTION_ID",
                );
                $arFilter = array(
                    "IBLOCK_ID" => $IBLOCK_ID,
                    //"ACTIVE" => "Y",
                    "CHECK_PERMISSIONS" => "Y",
                );
                $arOrder = array(
                    "SORT" => "ASC",
                    "NAME" => "ASC",
                    "ID" => "ASC",
                );
                $rsItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
                while ($arItem = $rsItems->GetNext())
                {
                    if($arItem["PREVIEW_TEXT"]){
                        $obParser = new CTextParser;
                        $arItem["PREVIEW_TEXT"] = $obParser->html_cut($arItem["~PREVIEW_TEXT"], 120);
                    }
                    $cache[$IBLOCK_ID][] = $arItem;
                }

            }
        }
        return $cache[$IBLOCK_ID];
    }

    function GetOptionsHtmlChekbox($arProperty, $values, $strHTMLControlName,&$bWasSelect)
    {
        $settings = CIBlockPropertyElementList::PrepareSettings($arProperty);
        $type="radio";
        if($settings["multiple"]=="Y")
            $type="checkbox";
        $options = array();
        $bWasSelect = false;
        foreach(ChekboxElementList::GetElements($arProperty["LINK_IBLOCK_ID"]) as $arItem)
        {
            //print_r($arItem);
            $option='';
            $option.='<label><input name="'.$strHTMLControlName["VALUE"];
            if($settings["multiple"]=="Y")
                $option.='[]';
            $option.='" type="'.$type.'" value="'.$arItem["ID"].'"';
            if(in_array($arItem["~ID"], $values))
            {
                $option .= ' checked';
                $bWasSelect = true;
            }
            $option .= '>'.$arItem["NAME"].'('.$arItem["PREVIEW_TEXT"].')</label>';
            $options[]=$option;
        }
        return  implode("<br/>",$options);
    }

    function GetPropertyFieldHtmlMulty($arProperty, $value, $strHTMLControlName)
    {
        $max_n = 0;
        $values = array();
        if(is_array($value))
        {
            foreach($value as $property_value_id => $arValue)
            {
                if (is_array($arValue))
                    $values[$property_value_id] = $arValue["VALUE"];
                else
                    $values[$property_value_id] = $arValue;

                if(preg_match("/^n(\\d+)$/", $property_value_id, $match))
                {
                    if($match[1] > $max_n)
                        $max_n = intval($match[1]);
                }
            }
        }

        $settings = CIBlockPropertyElementList::PrepareSettings($arProperty);
        if($settings["size"] > 1)
            $size = ' size="'.$settings["size"].'"';
        else
            $size = '';

        if($settings["width"] > 0)
            $width = ' style="width:'.$settings["width"].'px"';
        else
            $width = '';

        if($settings["multiple"]=="Y")
        {
            $newVal=array();
            foreach($value as $val)
                $newVal[]=$val["VALUE"];
            $bWasSelect = false;
            $options = ChekboxElementList::GetOptionsHtmlChekbox($arProperty, $newVal, $strHTMLControlName,$bWasSelect);
            $html = $options;
        }
        else
        {
            if(end($values) != "" || substr(key($values), 0, 1) != "n")
                $values["n".($max_n+1)] = "";

            $name = $strHTMLControlName["VALUE"]."VALUE";

            $html = '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb'.md5($name).'">';
            foreach($values as $property_value_id=>$value)
            {
                $html .= '<tr><td>';

                $bWasSelect = false;
                $options = CIBlockPropertyElementList::GetOptionsHtml($arProperty, array($value), $bWasSelect);

                $html .= '<select name="'.$strHTMLControlName["VALUE"].'['.$property_value_id.'][VALUE]"'.$size.$width.'>';
                $html .= '<option value=""'.(!$bWasSelect? ' selected': '').'>'.GetMessage("IBLOCK_PROP_ELEMENT_LIST_NO_VALUE").'</option>';
                $html .= $options;
                $html .= '</select>';

                $html .= '</td></tr>';
            }
            $html .= '</table>';

            $html .= '<input type="button" value="'.GetMessage("IBLOCK_PROP_ELEMENT_LIST_ADD").'" onClick="if(window.addNewRow){addNewRow(\'tb'.md5($name).'\', -1)}else{addNewTableRow(\'tb'.md5($name).'\', 1, /\[(n)([0-9]*)\]/g, 2)}">';
        }
        return  $html;
    }

}
?>