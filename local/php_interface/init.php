<?php
/**
 * /local/class - root директория автоподключаемых классов проекта (PSR-4)
 */
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/vendor/autoload.php');

helpers\SetConst::init();

AddEventHandler("iblock", "OnAfterIBlockElementAdd",  "OnAfterElementAddHandler");
AddEventHandler("subscribe", "BeforePostingSendMail", array("SubscribeHandlers", "BeforePostingSendMailHandler"));

include($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/init/prop/element.php');
require($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/init/BufferContent.php");
require($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/init/sity.php");
require($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/init/function.php");
include($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/init/form.php');


