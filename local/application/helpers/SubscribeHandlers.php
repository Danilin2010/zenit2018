<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 21.06.2017
 * Time: 12:25
 */

namespace helpers;

class SubscribeHandlers
{
	function BeforePostingSendMailHandler($arFields)
	{
		$rsSub = CSubscription::GetByEmail($arFields["EMAIL"]);
		$arSub = $rsSub->Fetch();

		$arFields["BODY"] = str_replace("#MAIL_ID#", $arSub["ID"], $arFields["BODY"]);
		$arFields["BODY"] = str_replace("#MAIL_MD5#", SubscribeHandlers::GetMailHash($arFields["EMAIL"]), $arFields["BODY"]);

		return $arFields;
	}

	function GetMailHash($email)
	{
		return md5(md5($email) . MAIL_SALT);
	}
}