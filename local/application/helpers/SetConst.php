<?php

namespace helpers;

use Bitrix\Main\Application as App;
use \Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

class SetConst{

    public static function init()
    {
        self::IsPrint();
        self::IsAjax();
        self::IsPost();
        self::IsMainPage();
        self::IblockConst();
        self::FormConst();
        self::HighloadIblockConst();
    }

    private static function IsPrint(){
        if (!defined("IS_PRINT"))
        {
            $request = App::getInstance()->getContext()->getRequest();
            $print = htmlspecialchars($request->getQuery("print"));
            if($print=="Y" ||$print=="y")
                define("IS_PRINT", true);
            else
                define("IS_PRINT", false);
        }
    }

    private static function IsMainPage(){
        if (!defined("IS_MAIN_PAGE"))
        {
            global $APPLICATION;
            if($APPLICATION->GetCurPage(true)==SITE_DIR.'index.php')
                define("IS_MAIN_PAGE", true);
            else
                define("IS_MAIN_PAGE", false);
        }
    }

    private static function IsAjax(){
        $ajax=false;
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            $ajax=true;
        if (!defined("IS_AJAX"))
                define("IS_AJAX", $ajax);
    }

    private static function IsPost(){
        if (!defined("IS_POST"))
        {
            $request = App::getInstance()->getContext()->getRequest();
            if($request->isPost())
                define("IS_POST", true);
            else
                define("IS_POST", false);
        }
    }

    private static function IblockConst(){
        /**
         * Определение констант кодов инфоблоков
         * Правило определения: все небуквенные символы заменяются на "_", получившаяся строка переводится в верхний регистр.
         * Добавляеться префикс iblock_
         */
        if (Loader::includeModule('iblock'))
        {
            $cIBlock = \CIBlock::GetList();
            while($items = $cIBlock->Fetch())
            {
                $code = "iblock_".trim($items['CODE']);
                $id = (int) $items['ID'];
                self::initConst($id,$code);
            }
        }
    }

    private static function HighloadIblockConst(){
        /**
         * Определение констант кодов Highload инфоблоков
         * Правило определения: все небуквенные символы заменяются на "_", получившаяся строка переводится в верхний регистр.
         * Добавляеться префикс HIGHLOAD_
         */
        if (Loader::includeModule('highloadblock'))
        {
            $hlblock = HL\HighloadBlockTable::getList();
            while($reshlblock=$hlblock->fetch())
            {
                $code = "highload_".trim($reshlblock['NAME']);
                $id = (int) $reshlblock['ID'];
                self::initConst($id,$code);
            }
        }
    }

    private static function FormConst(){
        /**
         * Определение констант кодов форм
         * Правило определения: все небуквенные символы заменяются на "_", получившаяся строка переводится в верхний регистр.
         * Добавляеться префикс FORM_
         */
        if (Loader::includeModule('form'))
        {
            $rsForms = \CForm::GetList();
            while ($arForm = $rsForms->Fetch())
            {
                $code = "form_".trim($arForm['VARNAME']);
                $id = (int) $arForm['ID'];
                self::initConst($id,$code);
            }
        }
    }

    private static function initConst($id,$code)
    {
        if (!empty($code))
        {
            $const = preg_replace('/\W/', '_', $code);
            $const = mb_convert_case($const, MB_CASE_UPPER);
            if (!defined($const))
            {
                define($const, $id);
            }
        }
    }

}