<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 24.01.2017
 * Time: 17:42
 */

namespace helpers;

class DateTime2HR
{
	public static function DateLocalize($DATA) {
		$MES = array(
			'01' => 'января',
			'02' => 'февраля',
			'03' => 'марта',
			'04' => 'апреля',
			'05' => 'мая',
			'06' => 'июня',
			'07' => 'июля',
			'08' => 'августа',
			'09' => 'сентября',
			'10' => 'октября',
			'11' => 'ноября',
			'12' => 'декабря'
		);
		$arData = explode('.', $DATA);
		$d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
		//$newData = $d.' '.$MES[$arData[1]].' '.$arData[2];
		$newData = $MES[$arData[1]];
		return $newData;
	}
}