<?php
/**
 * Created by PhpStorm.
 * User: abram
 * Date: 21.12.2017
 * Time: 10:35
 */

namespace helpers;

class Helper
{

	static function a($link, $text)
	{
		global $APPLICATION;
		$currentDir = $APPLICATION->GetCurDir();

		$class = null;
		if ($link == $currentDir)
		{
			$class = ' class="active"';
		}
$html = <<<HTML
<a href="$link"$class>$text</a> 
HTML;

		return $html;
	}
}