<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;

$newClass = array();
$arrClass = array(
    'Offices'=>'offices',
    'City'=>'city',
    'Contactfaces'=>'contactfaces',
    'Segment'=>'segment',
    'Banks'=>'banks',
    'CartConditions'=>'cartconditions',
);

foreach ($arrClass as $key => $value)
{
    $newClass['Aic\Bz\Content\\'.$key.'Table'] = 'content/'.$value.'/'.$value.'.php';
    $newClass['Aic\Bz\Content\\'.$key.'PropMultipleTable']='content/'.$value.'/'.$value.'propmultiple.php';
    $newClass['Aic\Bz\Content\\'.$key.'PropSimpleTable']='content/'.$value.'/'.$value.'propsimple.php';
    $newClass['Aic\Bz\Content\\'.$key.'SectionTable']='content/'.$value.'/'.$value.'section.php';
}


$arrClass = array(
    'Actuality'=>'actuality',
    'Facts'=>'facts',
);

foreach ($arrClass as $key => $value)
{
    $newClass['Aic\Bz\Content\Reporting\\'.$key.'Table'] = 'content/reporting/'.$value.'/'.$value.'.php';
    $newClass['Aic\Bz\Content\Reporting\\'.$key.'PropMultipleTable']='content/reporting/'.$value.'/'.$value.'propmultiple.php';
    $newClass['Aic\Bz\Content\Reporting\\'.$key.'PropSimpleTable']='content/reporting/'.$value.'/'.$value.'propsimple.php';
    $newClass['Aic\Bz\Content\Reporting\\'.$key.'SectionTable']='content/reporting/'.$value.'/'.$value.'section.php';
}



Loader::includeModule("highloadblock");
Loader::includeModule("balamarket.orm");
\CModule::AddAutoloadClasses("aic.bz",$newClass);
