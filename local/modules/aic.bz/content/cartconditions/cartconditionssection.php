<?php
namespace Aic\Bz\Content;

class CartConditionsSectionTable extends \Balamarket\Orm\Entity\IblockSection
{
    public static function getIblockId()
    {
        return CartConditionsTable::getIblockId();
    }
}