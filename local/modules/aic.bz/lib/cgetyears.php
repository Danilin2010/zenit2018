<?php

namespace Aic\Bz;


use Bitrix\Main\Entity;


class cGetYears
{
    private $id;

    function __construct($id)
    {
        $this->id=$id;
    }

    public function GetYears()
    {
        $res=array();
        $result = Content\ActualityTable::getList(
            array(
                "select" => array(
                    "DATA_YEAR",
                ),
                'group'=>array(
                    "DATA_YEAR",
                ),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                ),
                'runtime'=>array(
                    new Entity\ExpressionField('DATA_YEAR', 'YEAR(%s)', 'PROPERTY_SIMPLE.DATA'),
                ),
            )
        );
        while($el=$result->fetch())
            $res[]=$el["DATA_YEAR"];
        return $res;
    }


}