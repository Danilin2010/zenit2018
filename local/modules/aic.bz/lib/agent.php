<?php

namespace Aic\Bz;

use Aic\Bz\cRates;

class Agent {

    const module_id="aic.bz";
    const AgentReadRates='\Aic\Bz\Agent::ReadRates();';

    function ReadRates()
    {
        $Rates=new cRates();
        $Rates->ReadRates();
        return self::AgentReadRates;
    }

    function AddAgent()
    {
        if(!self::ChekAgent(self::AgentReadRates))
        {
            \CAgent::AddAgent(
                self::AgentReadRates,
                self::module_id,
                "N",
                60,
                \ConvertTimeStamp(time()+60,"FULL"),
                "Y",
                \ConvertTimeStamp(time()+60,"FULL")
            );
        }
    }

    function RemoveAgents()
    {
        \CAgent::RemoveModuleAgents(self::module_id);
    }

    function ChekAgent($name=self::AgentReadRates)
    {
        $res = \CAgent::GetList(Array("ID" => "DESC"), array("NAME" => $name));
        if($res->GetNext())
            return true;
        else
            return false;
    }

}
