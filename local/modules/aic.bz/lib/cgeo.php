<?php

namespace Aic\Bz;

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Entity;

class cGeo
{

    private $iddefprop=1;
    private $segment=array();

    private $idAtmType=44; //Банкомат
    private $idOfficeType=45; //Офис

    private $BankZenit='b00005';//Банк Зенит

    const TYPE_OFFICE = 45;

    function __construct()
    {

    }

    /*
     * Подключения связей при выборе городов через офисы
     */
    function get_runtime(){
        return array(
            'CITY_ID' => new Entity\ExpressionField('CITY_ID', '%s', 'PROPERTY_SIMPLE.cityId'),
            'CITY' => new Entity\ReferenceField(
                'CITY',
                Content\SegmentTable::getEntity(),
                array(
                    '=this.CITY_ID' => 'ref.ID',
                )
            ),
            'CITY_NAME' => new Entity\ExpressionField('CITY_NAME', '%s', 'CITY.NAME'),


            'BANK_ID' => new Entity\ExpressionField('BANK_ID', '%s', 'PROPERTY_SIMPLE.bankId'),
            'BANK' => new Entity\ReferenceField(
                'BANK',
                Content\SegmentTable::getEntity(),
                array(
                    '=this.BANK_ID' => 'ref.ID',
                )
            ),
            'BANK_CODE' => new Entity\ExpressionField('BANK_CODE', '%s', 'BANK.XML_ID'),


            'SEGMENT_ID' => new Entity\ExpressionField('SEGMENT_ID', '%s', 'PROPERTY_SIMPLE.segmentId'),
            'SEGMENT' => new Entity\ReferenceField(
                'SEGMENT',
                Content\SegmentTable::getEntity(),
                array(
                    '=this.SEGMENT_ID' => 'ref.ID',
                )
            ),
            'SEGMENT_CODE' => new Entity\ExpressionField('SEGMENT_CODE', '%s', 'SEGMENT.PROPERTY_SIMPLE.TYPE'),
        );
    }

    /*
     * возвращает текущий город, его id и телефон по GeoIP
     */
    public function get_current_city_by_geo(){
        $ret = false;
        if($_SESSION['city'] && count($_SESSION['city'])>0 && strlen($_SESSION['city']["NAME"])>0)
            $ret=$_SESSION['city'];
        if(!$ret)
        {
            if (Loader::includeModule("altasib.geoip")){
                $arData = \ALX_GeoIP::GetAddr();
                if($arData['city'] && strlen($arData['city'])>0)
                {
                    $result = Content\OfficesTable::getList(
                        array(
                            "select" => array(
                                'CITY_NAME',
                                'CITY_ID',
                            ),
                            'group'=>array('CITY_ID'),
                            'limit'=>1,
                            'order'=>array("CITY_NAME"=>"ASC"),
                            "filter" => array(
                                "=ACTIVE"=>"Y",
                                "=CITY_NAME"=>$arData['city'],
                                "=BANK_CODE"=>$this->BankZenit,
                            ),
                            'runtime' => $this->get_runtime(),
                        )
                    );
                    if($city=$result->fetch())
                    {
                        $city["ID"]=$city["CITY_ID"];
                        $city["NAME"]=$city["CITY_NAME"];
                        $ret=$city;
                    }
                }
            }
        }
        if(!$ret){
            $result = Content\CityTable::getList(
                array(
                    "select" => array(
                        "ID",'NAME',
                    ),
                    'limit'=>1,
                    "filter" => array(
                        "=ACTIVE"=>"Y",
                        "=PROPERTY_MULTIPLE_AUTO.VALUE"=>$this->iddefprop,
                    ),
                )
            );
            if($city=$result->fetch())
                $ret=$city;
        }
        if($ret)
            $_SESSION['city']=$ret;
        return $ret;
    }

    /*
    * Получение города
    */
    function get_city(){
        $city = $this->get_current_city_by_geo();
        return $city;
    }

    /*
    * Установка города
    */
    function set_city(){
        GLOBAL $USER;
        $request = Application::getInstance()->getContext()->getRequest();
        $set_sity=(int)$request->get("set_sity");
        if($set_sity>0)
        {
            $result = Content\OfficesTable::getList(
                array(
                    "select" => array(
                        'CITY_NAME',
                        'CITY_ID',
                    ),
                    'limit'=>1,
                    'group'=>array('CITY_ID'),
                    'order'=>array("CITY_NAME"=>"ASC"),
                    "filter" => array(
                        "=ACTIVE"=>"Y",
                        "=CITY_ID"=>$set_sity,
                        "=BANK_CODE"=>$this->BankZenit,
                        "=SEGMENT_CODE"=>self::TYPE_OFFICE,
                    ),
                    'runtime' => $this->get_runtime(),
                )
            );
		  if($city=$result->fetch())
            {
                $city["ID"]=$city["CITY_ID"];
                $city["NAME"]=$city["CITY_NAME"];
                $_SESSION['city']=$city;
                $uriString = $request->getRequestUri();
                $uri = new Uri($uriString);
                $uri->deleteParams(array('set_sity','clear_cache', 'ncc'));
                $redirect = $uri->getUri();
                LocalRedirect($redirect);
            }
        }
    }

    /*
     * Получение списка городов для выпадающего списка
     */
    function GetListSity()
    {
        $Sitys=array();
        $result = Content\OfficesTable::getList(
            array(
                "select" => array(
                    'CITY_NAME',
                    'CITY_ID',
                ),
                'group'=>array('CITY_ID'),
                'order'=>array("CITY_NAME"=>"ASC"),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                    "=BANK_CODE"=>$this->BankZenit,
                    "=SEGMENT_CODE"=>self::TYPE_OFFICE,
                ),
                'runtime' => $this->get_runtime(),
            )
        );
        while($city=$result->fetch())
        {
            $city["ID"]=$city["CITY_ID"];
            $city["NAME"]=$city["CITY_NAME"];
            $key=strtoupper(substr($city["NAME"],0,1));
            $Sitys[$key][]=$city;
        }
        return $Sitys;
    }

    /*
     * Получение списка городов
     */
    function GetSimpleSity()
    {
        $Sitys=array();
        $result = Content\OfficesTable::getList(
            array(
                "select" => array(
                    'CITY_NAME',
                    'CITY_ID',
                ),
                'group'=>array('CITY_ID'),
                'order'=>array("CITY_NAME"=>"ASC"),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                    "=BANK_CODE"=>$this->BankZenit,
                ),
                'runtime' => $this->get_runtime(),
            )
        );
        while($city=$result->fetch())
        {
            $city["ID"]=$city["CITY_ID"];
            $city["NAME"]=$city["CITY_NAME"];
            $Sitys[]=$city;
        }

        return $Sitys;
    }

    /*
     * Получение списка офисов
     */
    function GetSimpleOffices()
    {
        $Sitys=array();
        $result = Content\OfficesTable::getList(
            array(
                "select" => array(
                    "ID",
                    'NAME',
                    'CITY_ID',
                    'SEGMENT_TYPE',
                    'CITY_NAME',
                ),
                'order'=>array("NAME"=>"ASC"),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                    "=SEGMENT_TYPE"=>$this->idOfficeType,
                ),
                'runtime' => array(
                    'SEGMENT_ID' => new Entity\ExpressionField('SEGMENT_ID', '%s', 'PROPERTY_SIMPLE.segmentId'),
                    'CITY_ID' => new Entity\ExpressionField('CITY_ID', '%s', 'PROPERTY_SIMPLE.cityId'),
                    'SEGMENT' => new Entity\ReferenceField(
                        'SEGMENT',
                        Content\SegmentTable::getEntity(),
                        array(
                            '=this.SEGMENT_ID' => 'ref.ID',
                        )
                    ),
                    'CITY' => new Entity\ReferenceField(
                        'CITY',
                        Content\SegmentTable::getEntity(),
                        array(
                            '=this.CITY_ID' => 'ref.ID',
                        )
                    ),
                    'SEGMENT_TYPE' => new Entity\ExpressionField('SEGMENT_TYPE', '%s', 'SEGMENT.PROPERTY_SIMPLE.TYPE'),
                    'CITY_NAME' => new Entity\ExpressionField('CITY_NAME', '%s', 'CITY.NAME'),
                ),
            )
        );
        while($city=$result->fetch())
            $Sitys[]=$city;
        return $Sitys;
    }

    function ChekCook()
    {
        $setcity=(int)Application::getInstance()->getContext()->getRequest()->getCookieRaw("setcity");
        if($setcity==1)
            return true;
        else
            return false;
    }


}
