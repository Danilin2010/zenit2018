<?php


namespace Aic\Bz;


class cEmailConfirmation
{

    private $Confirmation;
    private $passLen=10;
    private $diffTime;
    private $timeFormat='Y-m-d H:i:s';

    function __construct()
    {
        $this->Confirmation=HighLoadsTools::getInstance()->GetAicBzEmailConfirmation();
        $this->diffTime=15*60;
    }

    private function generate_password($number)
    {
        $arr = array('a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }


    public function GetConfirmation($email)
    {
        $class=$this->Confirmation;
        $Confirmation=array(
            "UF_EMAIL"=>$email,
            "UF_ACTIVE"=>"Y",
            'UF_DATETIME'=> new \Bitrix\Main\Type\DateTime(),
            "UF_CODE"=>$this->generate_password($this->passLen)
        );
        $class::Add($Confirmation);

        $arEventFields = array(
            "EMAIL_TO"=>$Confirmation["UF_EMAIL"],
            "CODE"=>$Confirmation["UF_CODE"],
        );
        \CEvent::Send("EMAIL_CONFIRMATION",SITE_ID,$arEventFields);
    }

    public function SetConfirmation($email,$code,$delete=false)
    {
        $class=$this->Confirmation;
        $filter=array(
            '>=UF_DATETIME'=> new \Bitrix\Main\Type\DateTime(date($this->timeFormat,time()-$this->diffTime),$this->timeFormat),
            "UF_EMAIL"=>$email,
            "UF_CODE"=>$code,
        );
        $param=array(
            'limit'=>1,
            'order'=>array("ID"=>"DESC"),
            'filter'=>$filter
        );
        $elres=$class::getList($param);
        if($el = $elres->fetch())
        {
            if($delete)
                $class::Update($el["ID"],array("UF_ACTIVE"=>false));
            return true;
        }
        return false;
    }
}