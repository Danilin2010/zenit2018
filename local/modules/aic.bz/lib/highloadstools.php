<?php

namespace Aic\Bz;

use \Bitrix\Highloadblock;
use \Bitrix\Main;


class HighLoadsTools
{
    protected $AicBzRates=1; //Курсы
    protected $AicBzEmailConfirmation=2; //Подтверждение EMAIL

    private $highloads = array();
    private static $_instance = null;
    static public function getInstance() {
        if(is_null(self::$_instance))
            self::$_instance = new self();
        return self::$_instance;
    }
    private function __construct() {}
    protected function __clone() {}
    private function __sleep(){}
    private function __wakeup(){}

    function GetHighloadblock($id)
    {

        if($this->highloads[$id])
            return $this->highloads[$id];
        $entity_data_class=$this->_GetHighloadblock($id);
        $this->highloads[$id]=$entity_data_class;
        return $entity_data_class;
    }
    function _GetHighloadblock($id)
    {
        $hldata = Highloadblock\HighloadBlockTable::getById($id)->fetch();
        $hlentity = Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $entity_data_class = $hlentity->getDataClass();
        return $entity_data_class;
    }



    //Курсы
    function GetAicBzRates(){return $this->GetHighloadblock($this->AicBzRates);}
    //Подтверждение EMAIL
    function GetAicBzEmailConfirmation(){return $this->GetHighloadblock($this->AicBzEmailConfirmation);}

}
