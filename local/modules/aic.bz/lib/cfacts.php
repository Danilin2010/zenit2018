<?php

namespace Aic\Bz;
use Bitrix\Main\Entity;

class cFacts
{
    public function GetYears()
    {
        $res=array();
        $result = Content\Reporting\FactsTable::getList(
            array(
                "select" => array(
                    "DATA_YEAR",
                ),
                "order" => array(
                    "DATA_YEAR"=>"DESC",
                ),
                'group'=>array(
                    "DATA_YEAR",
                ),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                ),
                'runtime'=>array(
                    new Entity\ExpressionField('DATA_YEAR', 'YEAR(%s)', 'PROPERTY_SIMPLE.DATA'),
                ),
            )
        );
        while($el=$result->fetch())
            $res[]=$el["DATA_YEAR"];
        return $res;
    }
}