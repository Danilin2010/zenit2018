<?php

namespace Aic\Bz;

use \Bitrix\Main\Localization\Loc;

class cOnlineReserve
{
    static $WEB_FORM_ID=3;  //id Формы
    //Триггер ЮЛ/ИП
    static $TypeName="form_dropdown_type";  //Вид юридического лиц
    static $TypeValueIndividual=19;  //id ИП
    static $TypeValueLegal=20;  //id ЮЛ
    //Обязательные для ЮЛ
    static $FullNameOfUlName="form_text_21";  //Полное наименование ЮЛ
    static $AbbreviatedNameName="form_text_22";  //Сокращенное наименование ЮЛ
    static $OrganizationalFormName="form_dropdown_organizational_form";  //Организационно-правовая форма
    static $OgrnName="form_text_32";  //ОГРН
    static $TypeActivityName="form_dropdown_type_activity";  //Тип деятельности
    //Обязательные для ИП
    static $FullNameIndividualEntrepreneurName="form_text_29";  //Полное наименование ИП
    static $ShortNameOfPiName="form_text_30";  //Сокращенное наименование ИП
    static $OgrnipName="form_text_33";  //ОГРНИП

    //Обработчик события
    function onBeforeResultAdd($WEB_FORM_ID, &$arFields, &$arrVALUES)
    {
        if ($WEB_FORM_ID == self::$WEB_FORM_ID)
        {
            $code=$arrVALUES["confirmation"];
            $email=$arrVALUES["form_email_41"];
            if (\Bitrix\Main\Loader::includeModule('aic.bz') && strlen($email)>0 && strlen($code)>0)
            {
                $Confirmation=new \Aic\Bz\cEmailConfirmation();
                $result=$Confirmation->SetConfirmation($email,$code,true);
            }
            switch($arrVALUES[self::$TypeName])
            {
                case self::$TypeValueIndividual:
                    self::ChekFolrm(array(
                        array("error"=>"ErrorFullNameIndividualEntrepreneur","value"=>$arrVALUES[self::$FullNameIndividualEntrepreneurName]),
                        array("error"=>'ErrorShortNameOfPi',"value"=>$arrVALUES[self::$ShortNameOfPiName]),
                        array("error"=>'ErrorOgrnip',"value"=>$arrVALUES[self::$OgrnipName]),
                    ));
                    break;
                case self::$TypeValueLegal:
                    self::ChekFolrm(array(
                        array("error"=>"ErrorFullNameOfUl","value"=>$arrVALUES[self::$FullNameOfUlName]),
                        array("error"=>'ErrorAbbreviatedName',"value"=>$arrVALUES[self::$AbbreviatedNameName]),
                        array("error"=>'ErrorOrganizationalForm',"value"=>$arrVALUES[self::$OrganizationalFormName]),
                        array("error"=>'ErrorOgrn',"value"=>$arrVALUES[self::$OgrnName]),
                        array("error"=>'ErrorTypeActivity',"value"=>$arrVALUES[self::$TypeActivityName]),
                    ));
                    break;
            }

        }
    }

    //Обработка значений и вызов исключения
    function ChekFolrm(array $arr)
    {
        global $APPLICATION;
        $err=array();
        foreach($arr as $val)
            if(strlen($val["value"])<=0)
                $err[]=Loc::getMessage($val["error"]);
        if(count($err)>0)
            $APPLICATION->ThrowException(implode("\n\r",$err));
    }

}