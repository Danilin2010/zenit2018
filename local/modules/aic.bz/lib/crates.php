<?php

namespace Aic\Bz;

use \Aic\Bz\HighLoadsTools;

class cRates
{

    private $file;
    private $AicBzRates;
    private $CachePath;

    function __construct()
    {
        //$this->file=$_SERVER["DOCUMENT_ROOT"]."/upload/rates_import/rates.xml";
        $this->file="https://old.zenit.ru/media/exchange/rates.xml";

        $this->CachePath="/".SITE_ID."/aic.bz/rate.list/";
        $this->AicBzRates=HighLoadsTools::getInstance()->GetAicBzRates();
    }

    private function GetRates()
    {
        $res=array();
        $xmlText = file_get_contents($this->file);
        $obXml = new \SimpleXMLElement($xmlText);
        foreach ($obXml->exchange as $exchange)
        {
            $type=(string)$exchange["type"];
            $arrRates=array(
                'UF_TYPE'=>$type,
                "UF_DATETIME"=>'',
                "UF_EUR_BY"=>0,
                "UF_EUR_SELL"=>0,
                "UF_USD_BY"=>0,
                "UF_USD_SELL"=>0,
                "UF_CROSS_BY"=>0,
                "UF_CROSS_SELL"=>0,
            );
            switch ($type):
                case "PL":
                    foreach ($exchange->ccy as $ccy)
                    {
                        $source=(string)$ccy["source"];
                        $target=(string)$ccy["target"];
                        $rate=(array)$ccy->rate;
                        unset($rate["@attributes"]);
                        if (($timestamp = strtotime((string)$ccy->datetime)) === false){

                        }elseif(strlen($arrRates["UF_DATETIME"])<=0){
                            $arrRates["UF_DATETIME"]=date('Y-m-d H:i:s', $timestamp);
                        }
                        if($source=="RUR" && $target=="EUR")
                        {
                            $arrRates["UF_EUR_BY"]=(float)$rate[0];
                            $arrRates["UF_EUR_SELL"]=(float)$rate[1];
                            $arrRates["UF_CROSS_BY"]=(float)$rate[2];
                        }elseif($source=="RUR" && $target=="USD"){
                            $arrRates["UF_USD_BY"]=(float)$rate[0];
                            $arrRates["UF_USD_SELL"]=(float)$rate[1];
                            $arrRates["UF_CROSS_SELL"]=(float)$rate[2];
                        }
                    }
                    break;
                case "CB":
                    foreach ($exchange->ccy as $ccy)
                    {
                        $source=(string)$ccy["source"];
                        $target=(string)$ccy["target"];
                        $rate=(string)$ccy->rate;
                        if($source=="RUR" && $target=="EUR")
                        {
                            $arrRates["UF_EUR_BY"]=(float)$rate;
                            $arrRates["UF_EUR_SELL"]=(float)$rate;
                        }elseif($source=="RUR" && $target=="USD"){
                            $arrRates["UF_USD_BY"]=(float)$rate;
                            $arrRates["UF_USD_SELL"]=(float)$rate;
                        }
                    }
                    break;
                case "BN":
                    foreach ($exchange->ccy as $ccy)
                    {
                        $source=(string)$ccy["source"];
                        $target=(string)$ccy["target"];
                        $rate=(array)$ccy->rate;
                        unset($rate["@attributes"]);
                        if (($timestamp = strtotime((string)$ccy->datetime)) === false){

                        }elseif(strlen($arrRates["UF_DATETIME"])<=0){
                            $arrRates["UF_DATETIME"]=date('Y-m-d H:i:s', $timestamp);
                        }
                        if($source=="RUR" && $target=="EUR")
                        {
                            $arrRates["UF_EUR_BY"]=(float)$rate[0];
                            $arrRates["UF_EUR_SELL"]=(float)$rate[1];
                        }elseif($source=="RUR" && $target=="USD"){
                            $arrRates["UF_USD_BY"]=(float)$rate[0];
                            $arrRates["UF_USD_SELL"]=(float)$rate[1];
                        }elseif($source=="USD" && $target=="EUR"){
                            $arrRates["UF_CROSS_BY"]=(float)$rate[0];
                            $arrRates["UF_CROSS_SELL"]=(float)$rate[1];
                        }
                    }
                    break;
                default:
                    break;
            endswitch;
            $res[]=$arrRates;
        }
        return $res;

    }

    function ReadRates()
    {
        $class=$this->AicBzRates;
        $Rates=$this->GetRates();
        $useUpdate=false;
        foreach($Rates as $rate)
        {
            if($rate["UF_TYPE"]=="CB")
                continue;
            $filter=array(
                '=UF_DATETIME'=> new \Bitrix\Main\Type\DateTime($rate["UF_DATETIME"],'Y-m-d H:i:s'),
                "UF_TYPE"=>$rate["UF_TYPE"],
            );
            $param=array(
                'limit'=>1,
                'order'=>array("UF_DATETIME"=>"DESC"),
                'filter'=>$filter
            );
            $elres=$class::getList($param);
            if(!$el = $elres->fetch())
            {
                $useUpdate=true;
                $rate["UF_DATETIME"]=new \Bitrix\Main\Type\DateTime($rate["UF_DATETIME"],'Y-m-d H:i:sd');
                $class::Add($rate);
            }
        }
        if($useUpdate)
            \BXClearCache(true, $this->CachePath);
    }

    function GetComponentsRates()
    {
        $class=$this->AicBzRates;
        $arrResult=array();
        $arrTypes=array("BN","PL");
        foreach($arrTypes as $type)
        {
            $filter=array(
                "UF_TYPE"=>$type,
            );
            $param=array(
                'limit'=>1,
                'order'=>array("UF_DATETIME"=>"DESC"),
                'filter'=>$filter
            );
            $elres=$class::getList($param);
            if($el = $elres->fetch())
                $arrResult[]=$el;
        }
        return $arrResult;
    }

}