<?php

namespace Aic\Bz;

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;

class cContactfaces
{

    function __construct()
    {

    }

    function GetList($id)
    {
        $results=array();
        $result = Content\ContactfacesTable::getList(
            array(
                "select" => array(
                    "ID",
                    'NAME',
                    'PHONE'=>'PROPERTY_SIMPLE.PHONE',
                    'EMAIL'=>'PROPERTY_SIMPLE.EMAIL',
                    'CITY'=>'PROPERTY_SIMPLE.CITY',
                    'NOT_USE_NAME'=>'PROPERTY_SIMPLE.NOT_USE_NAME',
                ),
                'order'=>array("SORT"=>"ASC","NAME"=>"ASC"),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                    'PROPERTY_SIMPLE.CITY'=>$id,
                ),
            )
        );
        while($city=$result->fetch())
        {
            $results[]=$city;
        }
        return $results;
    }

}
