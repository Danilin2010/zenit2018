<?php

namespace Aic\Bz;

class CartConditions
{
    public function getConditions($id){

        $Conditions=array();

        $result = Content\CartConditionsTable::getList(
            array(
                "select" => array(
                    'NAME',
                    'PREVIEW_TEXT',
                    'DETAIL_TEXT',
                    'PROPERTY_SIMPLE.PARENT'
                ),
                'order'=>array("SORT"=>"ASC"),
                "filter" => array(
                    "=ACTIVE"=>"Y",
                    "=PROPERTY_SIMPLE.PARENT"=>$id,
                ),
            )
        );

        while($Condition=$result->fetch())
            $Conditions[]=$Condition;

        return $Conditions;

    }
}