<?
IncludeModuleLangFile(__FILE__);
if (class_exists("aic_bz")) return;
Class aic_bz extends CModule
{
    var $MODULE_ID = "aic.bz";

    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    var $MODULE_CSS;

    var $MODULE_GROUP_RIGHTS = "Y";

    var $errors;

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__)."/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("AIC_BZ_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("AIC_BZ_MODULE_DESC");
        $this->PARTNER_NAME = GetMessage("AIC_BZ_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("AIC_BZ_PARTNER_URI");
        //$this->MODULE_DIR=dirname(dirname(__FILE__));
    }

    function DoInstall()
    {
        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();
        $this->InstallEventType();
        RegisterModule($this->MODULE_ID);
        $this->InstallAgent();
    }

    function DoUninstall()
    {
        global $APPLICATION;
        if (!check_bitrix_sessid())
            return false;
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallAgent();
        $this->UnInstallEvents();
        $this->UnInstallEventsType();
        UnRegisterModule($this->MODULE_ID);
    }

    function InstallEvents()
    {

        return true;
    }

    function UnInstallEvents()
    {

        return true;
    }

    function InstallEventType()
    {

        return true;
    }

    function UnInstallEventsType()
    {

        return true;
    }

    function InstallAgent()
    {


        return true;
    }

    function UnInstallAgent()
    {

        return true;
    }

    function InstallDB()
    {

        return true;
    }

    function UnInstallDB($params)
    {

        return true;
    }

    function InstallFiles()
    {

        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function GetModuleRightList()
    {
        $arr = array(
            "reference_id" => array("D","R","W"),
            "reference" => array(
                GetMessage("FORM_DENIED"),
                GetMessage("FORM_OPENED"),
                GetMessage("FORM_FULL")
            ),
        );
        return $arr;
    }

};





