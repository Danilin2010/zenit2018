<?
$MESS ["ErrorFullNameOfUl"] = 'Необходимо указать "Полное наименование ЮЛ"';
$MESS ["ErrorAbbreviatedName"] = 'Необходимо указать "Сокращенное наименование ЮЛ"';
$MESS ["ErrorOrganizationalForm"] = 'Необходимо выбрать "Организационно-правовую форму"';
$MESS ["ErrorOgrn"] = 'Необходимо указать "ОГРН"';
$MESS ["ErrorTypeActivity"] = 'Необходимо выбрать "Тип деятельности"';

$MESS ["ErrorFullNameIndividualEntrepreneur"] = 'Необходимо указать "Полное наименование ИП"';
$MESS ["ErrorShortNameOfPi"] = 'Необходимо указать "Сокращенное наименование ИП"';
$MESS ["ErrorOgrnip"] = 'Необходимо указать "ОГРНИП"';

?>
