<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<!-- Продуктовый хедер-->
<div class="product-header">
    <div class="product-header__wrapper">
        <div class="product-header__inner">
            <div class="product-header__title-wrapper">
                <h1 class="title title_family-secondary-black title_size-h1 title_color-secondary"><?=$arParams["~PRODUCTHEADER_TITLE"]?></h1>
            </div>
            <div class="product-header__image-wrapper">
                <!-- Картинка продуктового хедера-->
                <img class="product-header__image" src="<?=$arParams["PRODUCTHEADER_IMG"]?>">
            </div>
        </div>
    </div>
</div>