<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{

	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'PRODUCTHEADER_TITLE' => Array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_PRODUCTHEADER_TITLE'),
				'TYPE' => 'STRING',
				'DEFAULT' => '',
			),
			'PRODUCTHEADER_IMG' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_PRODUCTHEADER_IMG'),
                'TYPE' => 'STRING',
                'DEFAULT' => '',
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>