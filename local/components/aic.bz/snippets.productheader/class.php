<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class AicBzSnippetsProductHeaderComponent extends \CBitrixComponent
{
	/**
	 * шаблоны путей по умолчанию
	 * @var array
	 */
	protected $defaultUrlTemplates404 = array();
	
	/**
	 * переменные шаблонов путей
	 * @var array
	 */
	protected $componentVariables = array();
	
	/**
	 * определяет переменные шаблонов и шаблоны путей
	 */
	protected function setSefDefaultParams()
	{

	}
	
	/**
	 * получение результатов
	 */
	protected function getResult()
	{

	}
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		try
		{
			$this->setSefDefaultParams();
			$this->getResult();
			$this->includeComponentTemplate();
		}
		catch (Exception $e)
		{
			ShowError($e->getMessage());
		}
	}
}
?>