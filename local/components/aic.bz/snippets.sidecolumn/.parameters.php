<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{
	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
            'SIDECOLUMN_TITLE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_TITLE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_TEXT' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_TEXT'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_IMG' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_IMG'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_BRAND_LIST' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_BRAND_LIST'),
                'TYPE' => 'STRING',
                "MULTIPLE"  =>  "Y",
            ),
			'SIDECOLUMN_TYPE' => Array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_TYPE'),
                "TYPE"      =>  "LIST",
                "VALUES"    =>  array(
                    "product-card_theme-promo" =>  "Синий",
                    "product-card_theme-normal" =>  "Белый",
                    "product-card_theme-normal product-card_disabled" =>  "Серый",
                ),
                "MULTIPLE"  =>  "N",
			),
			'SIDECOLUMN_DATA_FILTER' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_DATA_FILTER'),
                'TYPE' => 'STRING',
                "MULTIPLE"  =>  "Y",
			),
            'SIDECOLUMN_FEATURE1_BEFORE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_BEFORE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE1_TITLE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_TITLE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE1_TITLE_SMALL' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_TITLE_SMALL'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE1_DESCRIPTION' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_DESCRIPTION'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE2_BEFORE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_BEFORE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE2_TITLE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_TITLE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE2_TITLE_SMALL' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_TITLE_SMALL'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE2_DESCRIPTION' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_DESCRIPTION'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE3_BEFORE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_BEFORE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE3_TITLE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_TITLE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE3_TITLE_SMALL' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_TITLE_SMALL'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_FEATURE3_DESCRIPTION' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_FEATURE_DESCRIPTION'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_LINK_ADD_CONTRIBUTION' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_ADD_CONTRIBUTION'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET'),
                'TYPE' => 'CHECKBOX',
            ),
            'SIDECOLUMN_LINK_LEARN_MORE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_LEARN_MORE'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_LINK_LEARN_MORE_TEXT' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_LEARN_MORE_TEXT'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_LINK_LEARN_MORE_TARGET' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_LEARN_MORE_TARGET'),
				'TYPE' => 'CHECKBOX',
            ),
            'SIDECOLUMN_LINK_CART' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_CART'),
                'TYPE' => 'STRING',
            ),
            'SIDECOLUMN_LINK_CART_TARGET' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_SIDECOLUMN_LINK_CART_TARGET'),
				'TYPE' => 'CHECKBOX',
            ),
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>