<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<!-- Добавить product-card_disabled если карточка не попала в фильтр-->
<?//echo "<pre>";print_r($arParams);echo "</pre>";?>
<div <?if($arParams["SIDECOLUMN_LINK_CART_TARGET"]=="Y"){?>data-target="_blank"<?}?> class="product-card product-card_type-small <?=$arParams["SIDECOLUMN_TYPE"]?>" data-filter="<?=htmlspecialchars(json_encode(array_diff($arParams["SIDECOLUMN_DATA_FILTER"], array(''))))?>" <?if(strlen($arParams["SIDECOLUMN_LINK_CART"])>0){?>data-link="<?=$arParams["SIDECOLUMN_LINK_CART"]?>"<?}?>>
    <div class="product-card__wrapper">
        <div class="product-card__text-wrapper">
            <!-- Заголовок-->
            <h2 class="product-card__title"><?=$arParams["SIDECOLUMN_TITLE"]?></h2>
            <!-- Карточка со сменой текста при навеении-->
            <div class="product-card__text">
                <!-- Если есть текст, отображаемый при наведении на карточку-->
                <!-- Добавляем для текста без наведения обертку product-card__text-default-->
                <?if(count(array_diff($arParams["SIDECOLUMN_BRAND_LIST"], array('')))>0){?>
                    <div class="product-card__text-default">
                        <?=$arParams["~SIDECOLUMN_TEXT"]?>
                    </div>
                    <!-- Скрытый текст помещаем в product-card__text-hidden-->
                    <div class="product-card__text-hidden">
                        <ul class="brand-list brand-list_theme-grey">
                            <li><?=implode("</li><li>",array_diff($arParams["SIDECOLUMN_BRAND_LIST"], array('')))?></li>
                        </ul>
                    </div>
                <?}else{?>
                    <?=$arParams["~SIDECOLUMN_TEXT"]?>
                <?}?>
            </div>
        </div>
        <?if(strlen($arParams["SIDECOLUMN_LINK_ADD_CONTRIBUTION"])>0 || strlen($arParams["SIDECOLUMN_LINK_LEARN_MORE"])>0){?>
        <div class="product-card__buttons-wrapper">
            <?if(strlen($arParams["SIDECOLUMN_LINK_ADD_CONTRIBUTION"])>0){?><a <?if($arParams["SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET"]=="Y"){?>target="_blank"<?}?> class="button button_primary product-card__button-action" href="<?=$arParams["SIDECOLUMN_LINK_ADD_CONTRIBUTION"]?>"><?if(strlen($arParams["SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT"])>0){?><?=$arParams["SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT"]?><?}else{?>Оформить вклад<?}?></a><?}?>
            <?if(strlen($arParams["SIDECOLUMN_LINK_LEARN_MORE"])>0){?><a <?if($arParams["SIDECOLUMN_LINK_LEARN_MORE_TARGET"]=="Y"){?>target="_blank"<?}?> class="button button_transparent product-card__button-more" href="<?=$arParams["SIDECOLUMN_LINK_LEARN_MORE"]?>"><?if(strlen($arParams["SIDECOLUMN_LINK_LEARN_MORE_TEXT"])>0){?><?=$arParams["SIDECOLUMN_LINK_LEARN_MORE_TEXT"]?><?}else{?>Подробнее<?}?></a><?}?>
        </div>
        <?}?>
        <?if(
        strlen($arParams["SIDECOLUMN_FEATURE1_TITLE"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE1_TITLE_SMALL"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE1_BEFORE"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE1_DESCRIPTION"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE2_TITLE"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE2_TITLE_SMALL"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE2_BEFORE"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE2_DESCRIPTION"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE3_TITLE"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE3_TITLE_SMALL"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE3_BEFORE"])>0
        || strlen($arParams["SIDECOLUMN_FEATURE3_DESCRIPTION"])>0
        ){?>
        <div class="product-card__features-wrapper">
            <?for ($i=1;$i<=3;$i++){?>
                <?if(strlen($arParams["SIDECOLUMN_FEATURE".$i."_TITLE"])>0
                || strlen($arParams["SIDECOLUMN_FEATURE".$i."_TITLE_SMALL"])>0
                || strlen($arParams["SIDECOLUMN_FEATURE".$i."_BEFORE"])>0
                || strlen($arParams["SIDECOLUMN_FEATURE".$i."_DESCRIPTION"])>0){?>
                <div class="product-card__feature">
                    <?if(strlen($arParams["SIDECOLUMN_FEATURE".$i."_TITLE"])>0
                        || strlen($arParams["SIDECOLUMN_FEATURE".$i."_BEFORE"])>0
                        || strlen($arParams["SIDECOLUMN_FEATURE".$i."_TITLE_SMALL"])>0){?>
                    <span class="product-card__feature-title"><?if(strlen($arParams["SIDECOLUMN_FEATURE".$i."_BEFORE"])>0){?>
                        <span class="product-card__feature-title-small"><?=$arParams["~SIDECOLUMN_FEATURE".$i."_BEFORE"]?></span><?}?>
                        <?=$arParams["~SIDECOLUMN_FEATURE".$i."_TITLE"]?>
                        <span class="product-card__feature-title-small"><?if(strlen($arParams["SIDECOLUMN_FEATURE".$i."_TITLE_SMALL"])>0){?><?=$arParams["~SIDECOLUMN_FEATURE".$i."_TITLE_SMALL"]?><?}?></span>
                    </span>
                    <?}?>
                    <?if(strlen($arParams["SIDECOLUMN_FEATURE".$i."_DESCRIPTION"])>0){?><span class="product-card__feature-description"><?=$arParams["~SIDECOLUMN_FEATURE".$i."_DESCRIPTION"]?></span><?}?>
                </div>
                <?}?>
            <?}?>
        </div>
        <?}?>
    </div>
</div>