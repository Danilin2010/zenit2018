<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{

	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
            'CHECKBOX_ID' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_CHECKBOX_ID'),
                'TYPE' => 'STRING',
                'DEFAULT' => '',
            ),
			'CHECKBOX_CODE' => Array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_CHECKBOX_CODE'),
				'TYPE' => 'STRING',
				'DEFAULT' => '',
			),
            'CHECKBOX_NAME' => Array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_CHECKBOX_NAME'),
                'TYPE' => 'STRING',
                'DEFAULT' => '',
            ),
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>