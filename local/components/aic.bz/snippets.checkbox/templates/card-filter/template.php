<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<div class="products-filter-selection__tag">
	<input type="checkbox" id="<?=$arParams["CHECKBOX_ID"]?>" name="<?=$arParams["CHECKBOX_CODE"]?>">
	<label class="products-filter-selection__tag-control" for="<?=$arParams["CHECKBOX_ID"]?>"><?=$arParams["CHECKBOX_NAME"]?></label>
</div>