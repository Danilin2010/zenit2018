<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<div class="checkbox products-filter__checkbox">
    <input type="checkbox" id="<?=$arParams["CHECKBOX_ID"]?>" name="<?=$arParams["CHECKBOX_CODE"]?>">
    <label class="checkbox__control" for="<?=$arParams["CHECKBOX_ID"]?>">
        <span class="checkbox__control-button">
            <svg class="icon checkbox__icon icon__checkbox-symbol">
                <use xlink:href="#icon__checkbox-symbol"></use>
            </svg>
        </span>
        <span class="checkbox__control-name"><?=$arParams["CHECKBOX_NAME"]?></span>
    </label>
</div>