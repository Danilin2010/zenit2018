<?php
namespace Aic\Bz\Office;


class OfficeData
{

    const
        FILTER_DISTANCE = 5000,
        IB_POINT = 75,
        IB_GROUP = 71,
        IB_BANK = 72,
        IB_SEGMENT = 73,
        IB_SERVICE = 70,
        IB_CITY = 1,
        IB_METRO = 74;

    private $filter = [];
    private $data;
    private $personType;
    private $filterSegment = [];


    public function __construct()
    {
        //Выборки параметров. Выбираем один раз т.е. используются в нескольких местах
        $this->data['segment'] = $this->getSegments();
        $this->data['service'] = $this->getService();
        $this->data['metro'] = $this->getMetro();
        $this->data['bank'] = $this->getBank();
        $this->data['city'] = $this->getCity();
    }


    public function getMetro()
    {
        $result = false;

        //делаем запрос
        $res = \CIBlockElement::GetList(['SORT' => 'ASC'], ['IBLOCK_ID' => self::IB_METRO], false, false);

        while($ob = $res->GetNext())
        {
            $result[$ob['ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }

        return $result;
    }

    public function getBank()
    {
        $result = false;

        //делаем запрос
        $res = \CIBlockElement::GetList(['SORT' => 'ASC'], ['IBLOCK_ID' => self::IB_BANK], false, false);

        while($ob = $res->GetNext())
        {
            $result[$ob['ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }

        return $result;
    }

    /** Выборка сигментов
     * @return bool
     */
    public function getSegments()
    {
        $result = false;
        $filter['IBLOCK_ID'] =  self::IB_SEGMENT;

        //делаем запрос
        $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_SEGMENT], false, false);

        while($ob = $res->GetNext())
        {
            $result[$ob['ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }

        return $result;
    }


    /**
     * @return bool
     */
    public function getServiceSection()
    {
        $sections = [];
        $rsSect = \CIBlockSection::GetList(['SORT' => 'ASC'], ['IBLOCK_ID' => self::IB_SERVICE], false, ["ID", "NAME","UF_*"]);

        while ($section = $rsSect->GetNext())
        {
            $sections[$section['ID']] = [
                'id' => $section['ID'],
                'name' => $section['NAME'],
                'type' => $section['UF_TYPE'],
            ];
        }

        return $sections;
    }

    /**
     * @return bool
     */
    public function getService()
    {
        $result = [];

        $sections = [];
        $rsSect = \CIBlockSection::GetList(['SORT' => 'ASC'], ['IBLOCK_ID' => self::IB_SERVICE], false, ["ID", "NAME","UF_*"]);

        while ($section = $rsSect->GetNext())
        {
            $sections[$section['ID']] = [
                'id' => $section['ID'],
                'name' => $section['NAME'],
                'type' => $section['UF_TYPE'],
            ];
        }

        //делаем запрос
          $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_SERVICE], false, false);

          while($ob = $res->GetNext())
          {
              $result[$ob['ID']] = [
                  'id' => $ob['ID'],
                  'uid' => $ob['XML_ID'],
                  'name' => $ob['NAME'],
                  'typeId' => $sections[$ob['IBLOCK_SECTION_ID']]['id'],
                  'typeName' => $sections[$ob['IBLOCK_SECTION_ID']]['name'],
                  'typeCode' => $sections[$ob['IBLOCK_SECTION_ID']]['type'],
              ];
          }


        return $result;
    }



    public function getCity() {
        $result = [];
        $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_CITY], false, false);

        while($ob = $res->GetNext())
        {
            $result[$ob['ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }
        return $result;
    }


    /** Выбирает элементы по условию банка и группирует по городу чтобы найти те города которые есть для этого банка и типа элемента
     * @param $bankId
     * @return array
     */
    public function getCityBank($bankId) {

        global $DB;
        $list = [];

        if(!empty($this->filterSegment)) {
            $segment = ' AND EP.PROPERTY_191 IN('. implode(',', $this->filterSegment) .')';
        } else {
            $segment = '';
        }

        $results = $DB->Query(
            ' SELECT EP.PROPERTY_192 cityId' .
            ' FROM b_iblock_element as E' .
            ' LEFT JOIN b_iblock_element_prop_s75 as EP ON EP.IBLOCK_ELEMENT_ID = E.ID' .
            ' WHERE E.IBLOCK_ID=' . self::IB_POINT .
            ' AND EP.PROPERTY_190 = ' . $bankId .
            $segment .
            ' GROUP BY cityId '
        );

        while($ob = $results->Fetch())
        {
            if(array_key_exists($ob['cityId'], $this->data['city'])) {
                $list[] = $this->data['city'][$ob['cityId']];
            }
        }

        return $list;
    }


    protected function filterDefault()
    {
        return [
            'type' => 'branch'
        ];
    }


    /** Метод собирает фильтр из полученных параметров
     * @param $params
     */
    protected function buildFilter($params)
    {
        $filter['IBLOCK_ID'] = self::IB_POINT;

        $filterSegment = [
            "LOGIC" => "OR",
        ];
        //найдем идентификаторы банкоматов или офисов
        if ($params['type'] == 'atm') {
            $seg = [];
            foreach ($this->data['segment'] as $item) {
                if (in_array($item['uid'], ['s008', 's007'])) {
                    $filterSegment[] = ['PROPERTY_segmentId' => $item['id']];
                    $this->filterSegment[] = $item['id'];
                }
            }
        } else {
            foreach ($this->data['segment'] as $item) {
                if (!in_array($item['uid'], ['s008', 's007'])) {
                    $filterSegment[] = ['PROPERTY_segmentId' => $item['id']];
                    $this->filterSegment[] = $item['id'];
                }
            }
        }

        $filter[] = $filterSegment;

        //Фильтр по городу
        $filter['PROPERTY_cityId'] = $params['city'];


        if (!empty($params['service'])) {
            $filterService = [
                "LOGIC" => "OR",
            ];
            //Находим по ID раздел услуги чтобы понять для физлиц или юр
            $rsSect = \CIBlockSection::GetList([], ['IBLOCK_ID' => self::IB_SERVICE, 'ID' => $params['service']], false, ["ID", "NAME","UF_*"]);

            if($section = $rsSect->GetNext()) {
                //установим тип
                $this->personType = $section['UF_TYPE'];

                //выбираем элементы чтобы узнать идентификаторы элементов
                $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_SERVICE, 'SECTION_ID' => $params['service']], false, false);

                while($ob = $res->GetNext()) {
                    $filterService[] = ['=PROPERTY_service' => $ob['ID']];
                }

            }
            $filter[] = $filterService;
        }


        //Фильтр по имени или метро
        if(!empty($params['q'])) {
            $filter[] = [
                "LOGIC" => "OR",
                "%PROPERTY_address" => $params['q'],
                "%PROPERTY_metroId.NAME" => $params['q'],
            ];
        }

        //Банк
        if(!empty($params['bank'])) {
            $filter['PROPERTY_bankId.ID'] = (int)$params['bank'];
        }

        $this->filter = $filter;
        //echo '<pre>' . print_r($filter, 1) . '</pre>';
    }

    /** Отсеевание элементов по фильтру после выборки
     * @return bool
     */
    public function itemFilter($item, $params) {
        $result = true;
        //параметры передаются в виде массива
        if(!empty($params['filterParam'])) {
            //круглосуточные банкоматы
            if(in_array('24hours', $params['filterParam']) && $item['type'] === 'atm') {
                //return false;
                $timestamp = strtotime($params['date']);
                $week = date('w', $timestamp) > 0 ? date('w', $timestamp) - 1 : 6;
                $now = (int)date('Hi', $timestamp);
                $timeNow = isset($item['prop']['worktime_atm']['value'][$week]) ? $item['prop']['worktime_atm']['value'][$week] : false;

                if($timeNow === 'Круглосуточно') {
                    return true;
                }
                return false;
            }

            //Открытые сейчас
            if(in_array('now', $params['filterParam'])) {
                $timestamp = strtotime($params['date']);
                $week = date('w', $timestamp) > 0 ? date('w', $timestamp) - 1 : 6;
                $now = (int)date('Hi', $timestamp);

                //Выбираем какое время использовать для проверки работы офисов и банкоматов
                if($params['type'] == 'atm') {
                    $timeNow = isset($item['prop']['worktime_atm']['value'][$week]) ? $item['prop']['worktime_atm']['value'][$week] : false;
                    $timeNowLunch = isset($item['prop']['worktime_atm']['description'][$week]) ? $item['prop']['worktime_atm']['description'][$week] : false;
                } else {
                    //проверим какой сервис выбран, для юр лиц или физ.
                    if($this->personType === 'fiz') {
                        $timeNow = isset($item['prop']['worktime_ret']['value'][$week]) ? $item['prop']['worktime_ret']['value'][$week] : false;
                        $timeNowLunch = isset($item['prop']['worktime_ret']['description'][$week]) ? $item['prop']['worktime_ret']['description'][$week] : false;
                    } else {
                        $timeNow = isset($item['prop']['worktime_corp']['value'][$week]) ? $item['prop']['worktime_corp']['value'][$week] : false;
                        $timeNowLunch = isset($item['prop']['worktime_corp']['description'][$week]) ? $item['prop']['worktime_corp']['description'][$week] : false;
                    }
                }

                if($timeNow === 'Круглосуточно') {
                    return true;
                }

                $timeSplit = explode('-', $timeNow);
                $from = null;
                $to = null;

                if(sizeof($timeSplit) == 2) {
                    $from = (int)preg_replace('/\D/', "", $timeSplit[0]);
                    $to = (int)preg_replace('/\D/', '', $timeSplit[1]);
                }

                //проверяем время работы, если закрыт, вернем false иначе проверим перевыв
                if(($now < $from || $now >= $to) || ($from === null || $to == null)) {
                    return false;
                } else {
                    //Если открытие и закрытие корректно, проверим нет ли перерыва
                    if(!empty($timeNowLunch)) {
                        //разбиваем даты на начало и конец
                        $timeSplitLunch = explode('-', $timeNowLunch);
                        $fromLunch = null;
                        $toLunch = null;

                        //Если 2 элемента, почистим от лишних символов и проверим
                        if(sizeof($timeSplitLunch) == 2) {
                            $fromLunch = (int)preg_replace('/\D/', "", $timeSplitLunch[0]);
                            $toLunch = (int)preg_replace('/\D/', '', $timeSplitLunch[1]);
                            if($now >= $fromLunch && $now < $toLunch) {
                                return false;
                            }
                        }
                    }
                }
            }
        }


        //Прием наличных
        if(in_array('cashIn', $params['filterParam'])) {
            foreach ($item['prop']['service']['value'] as $item) {
                if($item['uid'] == 'atm004') {
                    return true;
                }
            }
            return false;
        }

        if(in_array('exchange', $params['filterParam'])) {
            foreach ($item['prop']['service']['value'] as $item) {
                if($item['uid'] == 'atm005') {
                    return true;
                }
            }
            return false;
        }

        if(in_array('withdrawCurrency', $params['filterParam'])) {
            foreach ($item['prop']['service']['value'] as $item) {
                if(in_array($item['uid'], ['atm002', 'atm003'])) {
                    return true;
                }
            }
            return false;
        }


        if(!empty($params['coord'])) {
            $coord = explode(',', $item['prop']['coord']['value']);

            $dist = self::getDistance((float)$coord[0], (float)$coord[1], (float)$params['coord'][0], (float)$params['coord'][1]);

            if($dist > static::FILTER_DISTANCE) {
                return false;
            }
        }

        return $result;
    }


    public function getItems($params)
    {


        //вызов генерации фильтра
        $this->buildFilter($params);

        $result = [];

        //делаем запрос
        $res = \CIBlockElement::GetList(['name' => 'asc'], $this->filter, false, false);

        $mapProp = [
            'segmentId' => 'segment', 'service' => 'service', 'metroId' => 'metro', 'bankId' => 'bank'
        ];

        $i = 0;
        while($ob = $res->GetNextElement())
        {
            $f = $ob->GetFields();
            $p = $ob->GetProperties();

            $item = [
                'id' => $f['ID'],
                'name' => $f['NAME'],
                'description' => $f['PREVIEW_TEXT'],
                'type' => in_array($this->data['segment'][$p['segmentId']['VALUE']]['uid'], ['s007', 's008']) ? 'atm' : 'office'
            ];

            $property = [];


            foreach ($p as $key => $prop) {
                //если список обработаем иначе строки
                if($prop['PROPERTY_TYPE'] == 'E') {
                    //проверим есть ли справочник
                    if(isset($this->data[$mapProp[$key]])) {
                        //если свойство множественное нужно пройти по всему списку
                        if($prop['MULTIPLE'] == 'Y') {

                            $value = '';
                            foreach ( $prop['VALUE'] as $v) {
                                $value[] = $this->data[ $mapProp[$key] ][$v];
                            }
                        } else {
                            $value = $this->data[$mapProp[$key]][$prop['VALUE']]['name'];
                        }
                    }

                    $property[$key] = [
                        'id' => $prop['ID'],
                        'title' => $prop['NAME'],
                        'value' => $value,
                        'uid' => $this->data[$mapProp[$key]][$prop['VALUE']]['uid']
                    ];

                } else {
                    $property[$key] = [
                        'id' => $prop['ID'],
                        'title' => $prop['NAME'],
                        'value' => $prop['~VALUE'],
                        'description' => $prop['DESCRIPTION'],
                        'uid' => $f['XML_ID']
                    ];
                }



            }

            $item['prop'] = $property;

            if($this->itemFilter($item, $params)) {
                $result[$i] = $item;
            }

            $i++;
        }

        return $result;
    }


    /** Дистанция между координатами
     * @param $φA
     * @param $λA
     * @param $φB
     * @param $λB
     * @return float
     */
    function getDistance ($φA, $λA, $φB, $λB) {

        // перевести координаты в радианы
        $lat1 = $φA * M_PI / 180;
        $lat2 = $φB * M_PI / 180;
        $long1 = $λA * M_PI / 180;
        $long2 = $λB * M_PI / 180;

        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $long2 - $long1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);

        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

        //
        $ad = atan2($y, $x);
        $dist = $ad * 6372795;

        return $dist;
    }

}