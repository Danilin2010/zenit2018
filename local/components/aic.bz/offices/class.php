<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require_once ('OfficeData.php');

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Application;

class OfficesComponent extends CBitrixComponent
{
	/**
	 * кешируемые ключи arResult
	 * @var array()
	 */
	protected $cacheKeys = array();
	
	/**
	 * дополнительные параметры, от которых должен зависеть кеш
	 * @var array
	 */
	protected $cacheAddon = array();
	

    /**
     * вохвращаемые значения
     * @var mixed
     */
	protected $returned;

    /**
     * тегированный кеш
     * @var mixed
     */
    protected $tagCache;

    /** Объект с классом данных
     * @var
     */
    protected $oData;

	/**
	 * подключает языковые файлы
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * подготавливает входные параметры
     * @param array $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $result = array(
            'IBLOCK_TYPE' => trim($params['IBLOCK_TYPE']),
            'IBLOCK_ID' => intval($params['IBLOCK_ID']),
            'IBLOCK_CODE' => trim($params['IBLOCK_CODE']),
            'CACHE_TIME' => intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600,
            'CACHE_TAG_OFF' => $params['CACHE_TAG_OFF'] == 'Y'
        );
        return $result;
    }
	


	/**
	 * получение результатов
	 */
	protected function getResult()
	{

	}
	
	/**
	 * выполняет действия после выполения компонента, например установка заголовков из кеша
	 */
	protected function executeEpilog()
	{
		if ($this->arResult['IBLOCK_ID'] && $this->arParams['CACHE_TAG_OFF'])
            \CIBlock::enableTagCache($this->arResult['IBLOCK_ID']);
	}


    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('aic.bz'))
            throw new Main\LoaderException(Loc::getMessage('AIC_BZ_CITY_LIST_CLASS_AIC_BZ_MODULE_NOT_INSTALLED'));
    }
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
		global $APPLICATION;

        try
		{
            $this->checkModules();

            $request = Application::getInstance()->getContext()->getRequest();

            $this->oData = new \Aic\Bz\Office\OfficeData();
            $filter = [];
            //$this->arResult['service'];
            //$filter = ['service' =>'718,719'];
            if($request->getQuery('type')) {
                $filter['type'] = htmlspecialcharsbx($request->getQuery('type'));
            }

            if($request->getQuery('service')) {
                $filter['service'] = (int)$request->getQuery('service');
            }

            if($request->getQuery('filterParam')) {
                $filter['filterParam'] = $request->getQuery('filterParam');
            }

            if($request->getQuery('q')) {
                $filter['q'] = $request->getQuery('q');
            }


            if($request->getQuery('bank')) {
                $filter['bank'] = $request->getQuery('bank');
            }


            if($request->getQuery('city')) {
                $filter['city'] = $request->getQuery('city');
            } else {
                $filter['city'] = (new \Aic\Bz\cGeo())->get_city()['ID'];
            }



            if($request->getQuery('coord')) {
                $filter['coord'] = $request->getQuery('coord');
            }

            $filter['date'] = $request->getQuery('date');

            $this->arResult['items'] = $this->oData->getItems($filter);
            $this->arResult['service'] = $this->oData->getServiceSection();
            $this->arResult['banks'] = $this->oData->getBank();

            if($request->getQuery('bank')) {
                $this->arResult['city'] = $this->oData->getCityBank($request->getQuery('bank'));
            } else {
            }


            $this->includeComponentTemplate();
		}
		catch (Exception $e)
		{
			$this->abortDataCache();
			ShowError($e->getMessage());
		}
	}
}
