<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
$this->setFrameMode(true);

$result = [];
$arResult['city'] = $arResult['city'];

$arResult['items'] = array_merge($arResult['items']);

foreach ($arResult['items'] as $key => $item) {

    $type = in_array($item['prop']['segmentId']['uid'], ['s007', 's008']) ? 'atm' : 'office';
    ob_start();

    $metro = null;
    if(array_key_exists('id', $item['prop']['metroId']['value'])) {
        $metro = $item['prop']['metroId']['value'];
    } else {
        $metroAr = [];

        foreach ($item['prop']['metroId']['value'] as $m) {
            $metroAr[] = $m['name'];
        }
        $item['prop']['metro']['value'] = implode(', ', $metroAr);
        unset($item['prop']['metroId']);
    }

    if(empty($item['prop']['worktimeAtmText']['value'])) {
        $item['prop']['worktimeAtmText']['value'] = createTimeString($item['prop']['worktime_atm']['value']);
    }


    unset($item['segmentId'], $item['worktime_ret'], $item['worktime_corp'], $item['worktime_ret_cashbox'], $item['worktime_cor_cashbox'], $item['mo'],  $item['mark']);

    $result[$key] = $item;
    $result[$key]['id'] = 'placeholder-' . $item['id'];
    $result[$key]['coord'] = explode(',', $item['prop']['coord']['value']);
    $result[$key]['type'] = $type;
}


/** минимизирует показ расписания
 * @param $data
 * @return string
 */
function createTimeString($data) {
    $dis = [0=> 'пн', 1 => 'вт', 2 => 'ср', 3 => 'чт', 4 => 'пт', 5 => 'сб', 6 => 'вс'];

    foreach ($data as $key => $d) {
        $data[$key] = strtolower(trim($d));
    }

    $check = array_unique($data);
    $text = '';

    //echo '<pre>' . print_r($check, 1) . '</pre>';

    if(sizeof($check) == 1) {
        if(in_array(strtolower(trim($check[0])), ['круглосуточно', 'временно не работает', 'нет данных', 'временно не работает'])) {
            $text = $check[0];
        } else {
            $text = $dis[0] . '.-' . $dis[sizeof($data) - 1] . ': ' . $check[0];
        }
    } else {
        foreach ($data as $key => $item) {
            if($item == 'нет данных') {
                continue;
            }
            $text .= $dis[$key] . '. ' .  $item . '<br>';
        }
    }

    if($text == 'нет данных') {
        return '';
    }
    return $text;
}


echo json_encode([
    'items' => $result,
    'city' => $arResult['city']
], JSON_UNESCAPED_UNICODE);

