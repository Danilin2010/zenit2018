<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
 


foreach ($arResult['items'] as $key => $item) {
    $type = in_array($item['prop']['segmentId']['uid'], ['s007', 's008']) ? 'atm' : 'office';
    ob_start();

?>
 <a type="office" href="#modal_form-list" id="placeholder-<?=$item['id']?>" class="open_modal hover">
            <div class="card-block-list views-row-2">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h3 class="title-ofice"><?=$item['name']?></h3>
                    </div>
                    <div class="col-sm-12 col-mt-4 col-md-4">
                        <div class="adres"><?=$item['address']?></div>
                        <div class="free"></div>
                    </div>
                    <div class="col-sm-12 col-mt-4 col-md-4">
                        <div class="phone-one"><?=$item['phone']?></div>
                        <div class="free"></div>
                    </div>
                    <div class="col-sm-12 col-mt-4 col-md-4">
                        <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                        <div class="weekend">сб. 10:00 – 17:00</div>
                        <div class="free"></div>
                    </div>
                </div>
            </div>
        </a>
<?php
$list = ob_get_contents();
if($item['type'] === 'atm'): ?>
    <a type="atm" href="#modal_form-atm" id="placeholder-<?=$item['id']?>" class="open_modal open_modal_mobile hover">
        <div class="card-block-list views-row-1">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <h3 class="title-ofice"><?=$item['name']?></h3>
                </div>
                <div class="col-sm-12 col-mt-4 col-md-4">
                    <div class="adres"><?=$item['id']?></div>
                    <div class="free"></div>
                </div>
                <div class="col-sm-12 col-mt-4 col-md-4">
                    <div class="round-clock">Круглосуточно</div>
                    <div class="free"></div>
                </div>
                <div class="col-sm-12 col-mt-4 col-md-4">
                    <div class="currency">Рубли РФ</div>
                    <div class="cash-acceptance">Cash-in (прием наличных)</div>
                    <div class="free"></div>
                </div>
            </div>
        </div>
    </a>
<? else: ?>
    <a type="atm" href="#modal_form" id="placeholder-<?=$item['id']?>" class="open_modal marshrut hover">
        <div class="card-block data">
            <h3 class="title-ofice"><?=$item['name']?></h3>
            <p><?=$item['description']?></p>
        </div>
    </a>
<? endif; ?>


<?php
    $map = ob_get_contents();
?>
    <div id="modal_form-atm" class="modal_div container">
        <div class="col-sm-12 col-md-12 modal_fon pb-0">
            <div class="row">
                <div class="col-sm-12 col-md-12 px-5 mobile-title">
                    <h4 class="modal-title"><?=$item['name']?></h4>
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <div class="col-sm-12 col-md-12 mobile-content">
                    <div class="modal-content-top px-4">
                        <div class="adres">
                            <?=$item['address']?>
                        </div>
                        <div class="phone-one"><a><?=$item['phone']?></a></div>
                    </div>
                    <div class="modal-content-bottom">
                        <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                            <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                            </ul>
                            <div class="info-content">
                                <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                    <div class="row px-4 block-line">
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>режим работы</h5>
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                        </div>
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>касса</h5>
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                        </div>
                                    </div>
                                    <div class="row px-4 block-line-bottom">
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>услуги Физ. лицам</h5>
                                            <div class="services">Денежные переводы</div>
                                            <div class="services">Прием и выдача денежных средств</div>
                                        </div>
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>услуги Пенсионерам</h5>
                                            <div class="services">Автокредитование</div>
                                            <div class="services">Ипотечное кредитование</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                    <div class="row px-4 block-line">
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>режим работы</h5>
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                        </div>
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>касса</h5>
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                        </div>
                                    </div>
                                    <div class="row px-4 block-line-bottom">
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>услуги Корпоративным клиентам</h5>
                                            <div class="services">Денежные переводы</div>
                                            <div class="services">Прием и выдача денежных средств</div>
                                            <h5>услуги Инвестиционным банкам</h5>
                                            <div class="services">Автокредитование</div>
                                            <div class="services">Ипотечное кредитование</div>
                                        </div>
                                        <div class="col-sm-12 col-mt-6 col-md-6 pb-3">
                                            <h5>услуги Малому и среднему бизнесу </h5>
                                            <div class="services">Автокредитование</div>
                                            <div class="services">Ипотечное кредитование</div>
                                            <h5>услуги Финансовым институтам</h5>
                                            <div class="services">Автокредитование</div>
                                            <div class="services">Ипотечное кредитование</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row background-linck">
                        <div class="col-sm-12 col-mt-6 col-md-6">
                            <div class="start-marshrut">
                                <a href="#" data-placeholder="placeholder-3" id="marshrut" class="modal_close">Построение маршрута</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    $modal = ob_get_contents();

ob_end_clean();

    $result[$key] = $item;
    $result[$key]['id'] = 'placeholder-' . $item['id'];
    $result[$key]['coord'] = explode(',', $item['prop']['coord']['value']);
    $result[$key]['type'] = $type;
    $result[$key]['html_list'] = $list;
    $result[$key]['html_map'] = $map;
    $result[$key]['html_modal'] = $modal;
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);

