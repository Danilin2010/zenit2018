<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//use Bitrix\Main\Localization\Loc as Loc;
//Loc::loadMessages(__FILE__);
$this->setFrameMode(true);

use \Bitrix\Main\Page\Asset;


$city = (new \Aic\Bz\cGeo())->get_city();

Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
Asset::getInstance()->addCss('/demo/offices/px/px.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/knockout.js');
Asset::getInstance()->addJs('/demo/js/custom-ya.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/offices.js');

foreach ($arResult['banks'] as $b) {
    if($b['uid'] == 'b00005') {
        $bankZenit = $b;
        break;
    }
}

Asset::getInstance()->addString('<script>
var city = ' . CUtil::PhpToJSObject($city) . ';
var bankZenit = ' . CUtil::PhpToJSObject($bankZenit) . ';
</script>');
?>

<div id="filtr-atm-ofice-all" class="main_content">
    <div class="container basic-filter">
        <!-- Фильтр в шапке -->
        <div id="atm-ofice-filtr" class="atm-ofice-full-filtr">
            <div id="filtr-header">
                <div class="row">
                    <div class="col-sm-12 col-md-6 switching-offices-atm">
                        <div class="wr-toggle-light-text text-fixed mobile-offices-cash">
                            <div id="offices-linck" class="toggle-light-text on">
                                <span class="h-1">Офисы</span>
                            </div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N"
                                     data-name="type_cash_list"></div>
                            </div>
                            <div id="cash-dispenser-linck" class="toggle-light-text off">
                                <span class="h-1">Банкоматы</span>
                            </div>
                        </div>
                    </div>
                    <div id="mb-search-adres" class="col-sm-12 col-mb-10 col-mt-11 col-md-3 hidden-lg">
                        <div class="block-filtr-list select">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <input type="text" name="q" class="simple_input" placeholder="" value="" data-control="mobile-search" data-event="false">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                                <div class="text search">Введите адрес</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-2 col-mt-1 col-md-3 hidden-lg">
                        <a href="#modal_form_filtr" class="open_modal open-mobile-filtr">
                            <i class="fa fa-sliders" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-12 col-mb-12 col-mt-12 col-md-3 text-right switch-list-card">
                        <div class="wr-toggle-light-text text-fixed">
                            <div id="tab-1" class="toggle-light-text off">
								<span class="h-1 hidden-xs hidden-sm">списком</span>
                                <i class="fa fa-list hidden-lg" aria-hidden="true"></i>
                            </div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="Y"
                                     data-name="type_visual"></div>
                            </div>
                            <div id="tab-2" class="toggle-light-text on">
								<span class="h-1 hidden-xs hidden-sm">на карте</span>
                                <i class="fa fa-map-o hidden-lg" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 hidden-xs hidden-sm pl-0">
                        <div id="filtr-bank" class="jq-selectbox jqselect formstyle bank">
                            <select data-programm="" autocomplete="off" data-plasholder="Банк группы ЗЕНИТ" data-title="Банк группы ЗЕНИТ"
                                    class="formstyle" name="bank">
                                <?php foreach ($arResult['banks'] as $bank): ?>
										<option value="<?= $bank['id'] ?>" <? if ($bank['name']=='ПАО Банк ЗЕНИТ'){echo 'selected';}?>><?= $bank['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="atm-ofice" class="atm-ofice-full">
        <!-- Отображение на карте -->
        <div id="map-bank" class="offices" data-block="screen">
            <!-- Отображение офисов на карте -->
            <div id="cash-dispenser-map" class="container">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12 col-md-4 col-left">
                        <div class="block-filtr filtr-block-1">
                            <div id="offices-map-filtr">
                                <!-- ko foreach: items -->
                                <a type="atm" href="#modal_form-office" class="open_modal marshrut hover"
                                   data-bind="attr: {id: id}, click: modal">
                                    <div class="card-block data">
                                        <h3 class="title-ofice" data-bind="html: name"></h3>
                                        <p>
                                            <span data-bind="visible: metro">м. <span
                                                        data-bind="text: metro"></span><br/></span>
                                            <span data-bind="html: address"></span>
                                        </p>
                                    </div>
                                </a>
                                <!-- /ko -->
                                <a data-bind="visible: listMessage">
                                    <div class="card-block data"><p class="list-message" data-bind="html: listMessage"></p></div>
                                </a>
                            </div>
                        </div>
                        <div class="block-filtr filtr-block-2">
                            <div class="placeholder">
                                <span class="placeholder-icon"></span>
                                <span class="text">Банк Зенит</span>
                            </div>
                            <div class="placeholder">
                                <span class="placeholder-empty-icon"></span>
                                <span class="text">Банковская группа ЗЕНИТ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-right ymap">

                        <div class="block-filtr filtr-block-3" data-bind="visible: listCity().length > 0">
                            <div class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle" name="city" data-plugin="selectCity" data-bind="
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        optionsCaption: 'Выбрать город'
                                        "></select>
                            </div>
                        </div>

                        <div class="block-filtr filtr-block-3">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div id="map-search-adres" class="complex_input_body">
                                        <input type="text" name="q" class="simple_input" placeholder="" value="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text search">поиск по адресу или метро</div>
                                </div>
                            </div>
                        </div>
                        <div class="block-filtr filtr-block-5">
                            <div id="map-services" class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Услуги" data-title="Услуги" class="formstyle"
                                        name="service">
                                    <option value="">Все услуги</option>
                                    <?php foreach ($arResult['serviceOffice'] as $key => $service): ?>
                                        <option value="<?= $service['id'] ?>"><?= $service['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div id="maps-local-arm" class="block-filtr filtr-block-6">
                            <?php foreach ($arResult['filterBranchParam'] as $key => $fap): ?>
                                <label><input type="checkbox" class="formstyle" name="filterParam"
                                              value="<?= $key ?>"><?= $fap ?></label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Отображение банкоматов на карте -->
            <div id="offices-map" class="container hidden" data-block="screen">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12 col-md-4 col-left">
                        <div class="block-filtr filtr-block-1">
                            <!-- ko foreach: items -->
                            <a type="office" href="#modal_form-atm" class="open_modal marshrut hover"
                               data-bind="attr:{id:id}, click: modal">
                                <div class="card-block data">
                                    <h3 data-bind="html: name"></h3>
                                    <p>
                                        <span data-bind="visible: metro">м. <span
                                                    data-bind="text: metro"></span><br/></span>
                                        <span data-bind="html: address"></span>
                                    </p>
                                </div>
                            </a>
                            <!-- /ko -->
                            <a data-bind="visible: listMessage">
                                <div class="card-block data"><p class="list-message" data-bind="html: listMessage"></p></div>
                            </a>
                        </div>
                        <div class="block-filtr filtr-block-2">
                            <div class="placeholder">
                                <span class="placeholder-icon"></span>
                                <span class="text">Банк Зенит</span>
                            </div>
                            <div class="placeholder">
                                <span class="placeholder-empty-icon"></span>
                                <span class="text">Банковская группа ЗЕНИТ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-right ymap">

                        <div class="block-filtr filtr-block-3" data-bind="visible: listCity().length > 0">
                            <div class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle" name="city" data-plugin="selectCity" data-bind="
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        optionsCaption: 'Выбрать город'
                                        "></select>
                            </div>
                        </div>

                        <div class="block-filtr filtr-block-3">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div id="maps-search-adres" class="complex_input_body">
                                        <input type="text" name="q" class="simple_input" placeholder="" value="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text search">поиск по адресу или метро.</div>
                                </div>
                            </div>
                        </div>
                        <div id="maps-atmstatus" class="block-filtr filtr-block-4">
                            <?php foreach ($arResult['filterAtmParam'] as $key => $fap): ?>
                                <label><input type="checkbox" class="formstyle" name="filterParam"
                                              value="<?= $key ?>"><?= $fap ?></label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Отображение списком -->
        <div id="list-bank" class="offices hidden" data-block="screen">
            <!-- Отображение офисов -->
            <div id="offices" class="container">
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-9 list-bank-mobile ">
                        <div id="offices-list-filtr">
                            <!-- ko foreach: items -->
                            <a type="office" href="#modal_form-office" class="open_modal open_modal_mobile hover"
                               data-bind="attr: {id: id}, click: modal">
                                <div class="card-block-list views-row-2">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice" data-bind="html: name"></h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">
                                                <span data-bind="visible: metro">м. <span
                                                         data-bind="text: metro"></span>, </span>
                                                <span data-bind="html: address"></span>
                                            </div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one" data-bind="html: phone"></div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays" data-bind="html: worktimeRet"></div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!-- /ko -->
                            <a data-bind="visible: listMessage">
                                <div class="card-block data"><p class="list-message" data-bind="html: listMessage"></p></div>
                            </a>
                        </div>
                    </div>
                    <!-- Фильтр офисов -->
                    <div id="form-list-ofis" class="col-sm-12 col-md-3 hidden-xs hidden-sm">
                        <div class="row" data-bind="visible: listCity().length > 0">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div class="jq-selectbox jqselect formstyle">
                                        <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle" name="city" data-plugin="selectCity" data-bind="
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        optionsCaption: 'Выбрать город'
                                        "></select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div class="wr_complex_input complex_input_noicon">
                                        <div class="complex_input_body">
                                            <div id="search-adres" class="complex_input_body">
                                                <input type="text" name="q" class="simple_input" placeholder=""
                                                       value="">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </div>
                                            <div class="text search">поиск по адресу или метро</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div id="user-type" class="jq-selectbox jqselect formstyle">
                                        <select data-programm="" data-plasholder="Услуги" data-title="Услуги"
                                                class="formstyle" name="service">
                                            <option value="">Все услуги</option>
                                            <?php foreach ($arResult['serviceOffice'] as $key => $service): ?>
                                                <option value="<?= $service['id'] ?>"><?= $service['name'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div id="ofisstatus" class="block-filtr-list">
                                    <?php foreach ($arResult['filterBranchParam'] as $key => $fap): ?>
                                        <label><input type="checkbox" class="formstyle" name="filterParam"
                                                      value="<?= $key ?>"><?= $fap ?></label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Отображение банкоматов -->
            <div id="cash-dispenser" class="container hidden" data-block="screen">
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-9 list-bank-mobile">
                        <!-- ko foreach: items -->
                        <a type="atm" href="#modal_form-atm" class="open_modal open_modal_mobile hover"
                               data-bind="attr: {id: id}, click: modal">
                            <div class="card-block-list views-row-1">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice" data-bind="html: name"></h3>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">
                                            <span data-bind="visible: metro">м. <span
                                                        data-bind="text: metro"></span>, </span>
                                            <span data-bind="html: address"></span>
                                        </div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="round-clock" data-bind="html: worktimeAtm"></div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4" data-bind="foreach: serviceAtm">
                                        <div class="currency" data-bind="text: $data"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <!-- /ko -->
                        <a data-bind="visible: listMessage">
                            <div class="card-block data"><p class="list-message" data-bind="html: listMessage"></p></div>
                        </a>
                </div>
                <!-- Фильтр банкоматов -->
                <div id="form-list-arm" class="col-sm-12 col-md-3 hidden-xs hidden-sm">
                    <div class="row" data-bind="visible: listCity().length > 0">
                        <div class="col-sm-12 col-md-12 pl-0">
                            <div class="block-filtr-list select">
                                <div class="jq-selectbox jqselect formstyle">
                                    <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle" name="city" data-plugin="selectCity" data-bind="
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        optionsCaption: 'Выбрать город'
                                        "></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 pl-0">
                            <div class="block-filtr-list select">
                                <div class="wr_complex_input complex_input_noicon">
                                    <div class="complex_input_body">
                                        <div id="search-adres" class="complex_input_body">
                                            <input type="text" name="q" class="simple_input" placeholder="" value="">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </div>
                                        <div class="text search">поиск по адресу или метро</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 pl-0">
                            <div id="atmstatus" class="block-filtr-list">
                                <?php foreach ($arResult['filterAtmParam'] as $key => $fap): ?>
                                    <label><input type="checkbox" class="formstyle" name="filterParam"
                                                  value="<?= $key ?>"><?= $fap ?></label>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- Модальное окно для отображения информации на банкомата -->
<div class="modal-open modal">
    <div id="modal_form-atm" class="modal_div">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header hidden-xs hidden-sm">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel" data-bind="html: $root.dialog.name"></h4>
                </div>
                <div class="modal-header hidden-lg">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel">Выбранный банкомат</h4>
                </div>
                <div id="map-modal-atm" class="map-modal"></div>
                <div class="mobile-modal-content">
                    <div class="modal-body">
                        <div class="modal-content-top">
                            <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel" data-bind="html: $root.dialog.name"></h4>
                            <div class="adres">
                                <span data-bind="visible: $root.dialog.metro">м. <span data-bind="text: $root.dialog.metro"></span>, </span> <span data-bind="html: $root.dialog.address"></span>
                            </div>
                            <div class="phone-one"><a data-bind="html: $root.dialog.phone"></a></div>
                        </div>
                        <div class="modal-content-bottom">
                            <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                <div class="info-content">
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content"
                                         id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel"
                                         aria-hidden="false">
                                        <div class="row block-line" data-bind="visible: $root.dialog.worktimeAtm()">
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="visible: $root.dialog.worktimeAtm">
                                                <h5>режим работы</h5>
                                                <div class="weekdays" data-bind="html: $root.dialog.worktimeAtm"></div>
                                            </div>
                                            <? /*
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="visible: worktimeRetCashbox" >
                                                <h5>касса</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                            </div>
                                            */ ?>
                                        </div>
                                        <div class="row block-line-bottom" data-bind="visible: $root.dialog.serviceAtm().length > 0">
                                            <!-- ko foreach: $root.dialog.serviceAtm -->
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="css: {
                                            'col-sm-12 col-mt-6 col-md-6' : $root.dialog.serviceAtm().length > 1,
                                            'col-sm-12 col-mt-12 col-md-12' : $root.dialog.serviceAtm().length == 1,
                                            }">
                                                <h5 data-bind="text: title"></h5>
                                                <!-- ko foreach: items -->
                                                <div class="services" data-bind="text: $data"></div>
                                                <!-- /ko -->
                                            </div>
                                            <!-- /ko -->
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" id="marshrut" class="modal_close close_marshrut"  data-bind="attr: {'data-placeholder': $root.dialog.id}">Построение
                            маршрута</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Модальное окно для отображения информации офиса -->
<div class="modal-open modal">
    <div id="modal_form-office" class="modal_div">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header hidden-xs hidden-sm">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel" data-bind="html: $root.dialog.name"></h4>
                </div>
                <div class="modal-header hidden-lg">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel">Выбранное отделение</h4>
                </div>
                <div id="map-modal-office" class="map-modal"></div>
                <div class="mobile-modal-content">
                    <div class="modal-body">
                        <div class="modal-content-top">
                            <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel" data-bind="html: $root.dialog.name"></h4>
                            <div class="adres">
                                <span data-bind="visible: $root.dialog.metro">м. <span data-bind="text: $root.dialog.metro"></span>, </span> <span data-bind="html: $root.dialog.address"></span>
                            </div>
                            <div class="phone-one"><a data-bind="html: $root.dialog.phone"></a></div>
                        </div>
                        <div class="modal-content-bottom">
                            <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header"
                                    role="tablist">
                                    <li role="tab" tabindex="0"
                                        class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"
                                        aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true"
                                        aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1"
                                                                class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a>
                                    </li>
                                    <li role="tab" tabindex="-1"
                                        class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
                                        aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false"
                                        aria-expanded="false"><a href="#content-tabs-2" role="presentation"
                                                                 tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим
                                            лицам</a></li>
                                </ul>
                                <div class="info-content">
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content"
                                         id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel"
                                         aria-hidden="false">
                                        <div class="row block-line">
                                            <div class="col-sm-12 col-mt-6 col-md-6"  data-bind="visible: $root.dialog.worktimeRet">
                                                <h5>режим работы</h5>
                                                <div class="weekdays" data-bind="html: $root.dialog.worktimeRet"></div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="visible: $root.dialog.worktimeRetCashbox">
                                                <h5>касса</h5>
                                                <div class="weekdays" data-bind="html: $root.dialog.worktimeRetCashbox"></div>
                                            </div>
                                        </div>
                                        <div class="row block-line-bottom">

                                            <!-- ko foreach: $root.dialog.serviceFiz -->
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="css: {
                                            'col-sm-12 col-mt-6 col-md-6' : $root.dialog.serviceFiz().length > 1,
                                            'col-sm-12 col-mt-12 col-md-12' : $root.dialog.serviceFiz().length == 1,
                                            }">
                                                <h5 data-bind="text: title"></h5>
                                                <!-- ko foreach: items -->
                                                <div class="services" data-bind="text: $data"></div>
                                                <!-- /ko -->
                                            </div>
                                            <!-- /ko -->
                                        </div>
                                    </div>
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content"
                                         id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel"
                                         aria-hidden="true" style="display: none;">
                                        <div class="row block-line">
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="visible: $root.dialog.worktimeCorp">
                                                <h5>режим работы</h5>
                                                <div class="weekdays" data-bind="html: $root.dialog.worktimeCorp"></div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6" data-bind="visible: $root.dialog.worktimeCorCashbox">
                                                <h5>касса</h5>
                                                <div class="weekdays" data-bind="html: $root.dialog.worktimeCorCashbox"></div>
                                            </div>
                                        </div>
                                        <div class="row block-line-bottom">
                                            <!-- ko foreach: $root.dialog.serviceUr -->
                                            <div class="" data-bind="css: {
                                            'col-sm-12 col-mt-6 col-md-6' : $root.dialog.serviceUr().length > 1,
                                            'col-sm-12 col-mt-12 col-md-12' : $root.dialog.serviceUr().length == 1,
                                            }">
                                                <h5 data-bind="text: title"></h5>
                                                <!-- ko foreach: items -->
                                                <div class="services" data-bind="text: $data"></div>
                                                <!-- /ko -->
                                            </div>
                                            <!-- /ko -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" id="marshrut" class="modal_close close_marshrut" data-bind="attr: {'data-placeholder': $root.dialog.id}">Построение
                            маршрута</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Мобильный фильтр офисов и бпнкоматов -->
<div id="modal_form_filtr" class="modal_div mobile">
    <div class="row" >
        <div class="col-sm-12 col-md-12 modal_fon pb-0">
            <div class="row py-3 top-modal">
                <div class="col-sm-12 col-md-12 px-5">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title">Фильтр</h4>
                </div>
            </div>

            <div class="row content-modal">
                <div class="col-sm-12 col-md-12 px-5">
                    <div class="jq-selectbox jqselect formstyle bank" data-control="mobile-city">
                        <select data-programm="" autocomplete="off" data-plasholder="Банк группы ЗЕНИТ" data-title="Банк группы ЗЕНИТ" class="formstyle" name="bank">
                            <option value="" selected="">Все банки</option>
                            <?php foreach ($arResult['banks'] as $bank): ?>
                                <option value="<?= $bank['id'] ?>"><?= $bank['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <!-- мобильный фильтр для офисов -->
                    <div id="mobile_filter_office" data-block="screen">
                        <div data-bind="visible: listCity().length > 0">
                            <div class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle" name="city" data-plugin="selectCity" data-bind="
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        optionsCaption: 'Выбрать город'
                                        "></select>
                            </div>
                        </div>
                        <div id="works" class="block-filtr-list row">
                            <?php foreach ($arResult['filterBranchParam'] as $key => $fap): ?>
                                <div class="col-md-12 col-mt-6">
                                    <label><input type="checkbox" class="formstyle" name="filterParam"
                                                  value="<?= $key ?>"><?= $fap ?></label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <!-- мобильный фильтр для банкоматов -->
                    <div id="mobile_filter_atm" data-block="screen" class="hidden">
                        <div data-bind="visible: listCity().length > 0">
                            <div class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle" name="city" data-plugin="selectCity" data-bind="
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        optionsCaption: 'Выбрать город'
                                        "></select>
                            </div>
                        </div>
                        <div id="works" class="block-filtr-list row">
                            <?php foreach ($arResult['filterAtmParam'] as $key => $fap): ?>
                                <div class="col-md-12 col-mt-6">
                                    <label><input type="checkbox" class="formstyle" name="filterParam"
                                                  value="<?= $key ?>"><?= $fap ?></label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>



                </div>
            </div>


            <div class="row footer-madal">
                <div class="col-sm-12 col-md-12 px-5">
                    <div class="form_application_line">
                        <input class="button bigmaxwidth" value="Применить фильтр" type="submit" data-filter="mobile">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>