<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);

use \Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss('/demo/offices/px/px.css');
Asset::getInstance()->addJs('/demo/js/custom-ya.js');
Asset::getInstance()->addJs('http://api-maps.yandex.ru/2.1/?lang=ru_RU');

?>

<div class="main_content">
    <div class="container basic-filter">
        <!-- Фильтр в шапке -->
        <div id="atm-ofice-filtr" class="atm-ofice-full-filtr">
            <div id="filtr-header">
                <div class="row">
                    <div class="col-sm-12 col-md-6 switching-offices-atm">
                        <div class="wr-toggle-light-text text-fixed mobile-offices-cash">
                            <div id="offices-linck" class="toggle-light-text on">
                                <span class="h-1">Офисы</span>
                            </div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N" data-name="type_cash_list"></div>
                            </div>
                            <div id="cash-dispenser-linck" class="toggle-light-text off">
                                <span class="h-1">Банкоматы</span>
                            </div>
                        </div>
                    </div>
                    <div id="mb-search-adres" class="col-sm-12 col-mb-10 col-mt-11 col-md-3 hidden-lg">
                        <div class="block-filtr-list select">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <input type="text" name="" class="simple_input" placeholder="" value="">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                                <div class="text search">Введите адрес</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-mb-2 col-mt-1 col-md-3 hidden-lg">
                        <a href="#modal_form_filtr" class="open_modal open-mobile-filtr">
                            <i class="fa fa-sliders" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-sm-12 col-mb-12 col-mt-12 col-md-3 text-right switch-list-card">
                        <div class="wr-toggle-light-text text-fixed">
                            <div id="tab-1" class="toggle-light-text on">
                                <span class="h-1 hidden-xs hidden-sm">списком</span>
                                <i class="fa fa-list hidden-lg" aria-hidden="true"></i>
                            </div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-first data-checked="N" data-name="type_visual"></div>
                            </div>
                            <div id="tab-2" class="toggle-light-text off">
                                <span class="h-1 hidden-xs hidden-sm">на карте</span>
                                <i class="fa fa-map-o hidden-lg" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 hidden-xs hidden-sm pl-0">
                        <div id="filtr-bank" class="jq-selectbox jqselect formstyle bank">
                            <select data-programm="" data-plasholder="Банк группы ЗЕНИТ" data-title="Банк группы ЗЕНИТ" class="formstyle" name="programm">
                                <option value="all" selected="">Все банки</option>
                                <?php foreach ($arResult['banks'] as $bank):?>
                                    <option value="<?=$bank['id']?>"><?=$bank['name']?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="atm-ofice" class="atm-ofice-full">
        <!-- Отображение на карте -->
        <div id="map-bank" class="offices hidden">
            <!-- Отображение офисов на карте -->
            <div id="cash-dispenser-map" class="container">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12 col-md-4 col-left">
                        <div class="block-filtr filtr-block-1">
                            <div id="offices-map-filtr">
                                <a type="atm" href="#modal_form-office" id="placeholder-1" class="open_modal marshrut hover">
                                    <div class="card-block data">
                                        <h3 class="title-ofice">Главный офис</h3>
                                        <p>м. Проспект мира<br>
                                            Банный переулок, дом 9</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="block-filtr filtr-block-2">
                            <div class="placeholder">
                                <span class="placeholder-icon"></span>
                                <span class="text">Банк Зенит</span>
                            </div>
                            <div class="placeholder">
                                <span class="placeholder-empty-icon"></span>
                                <span class="text">Банковская группа ЗЕНИТ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-right">
                        <div class="block-filtr filtr-block-3">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div id="map-search-adres"  class="complex_input_body">
                                        <input type="text" name="" class="simple_input" placeholder="" value="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text search">поиск по адресу или метро</div>
                                </div>
                            </div>
                        </div>
                        <div class="block-filtr filtr-block-5">
                            <div id="map-services" class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Услуги" data-title="Услуги" class="formstyle" name="service">
                                    <?php foreach ($arResult['service'] as $key => $service): ?>
                                        <option value="<?=$service?>"><?=$key?></option>
                                    <?php endforeach; ?>

                                    <option value="jur-individuals">Юр. лицам</option>
                                </select>
                            </div>
                        </div>
                        <div id="maps-local-arm" class="block-filtr filtr-block-6">
                            <?php foreach ($arResult['filterBranchParam'] as $key => $fap): ?>
                                <label><input type="checkbox" class="formstyle" name="filterParam" value="<?=$key?>"><?=$fap?></label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Отображение банкоматов на карте -->
            <div id="offices-map" class="container hidden">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12 col-md-4 col-left">
                        <div class="block-filtr filtr-block-1">
                            <a type="office" href="#modal_form-atm" id="placeholder-6" class="open_modal marshrut hover">
                                <div class="card-block data">
                                    <h3>Круглосуточный банкомат</h3>
                                    <p>м. Проспект мира<br>
                                        Банный переулок, дом 9</p>
                                </div>
                            </a>
                        </div>
                        <div class="block-filtr filtr-block-2">
                            <div class="placeholder">
                                <span class="placeholder-icon"></span>
                                <span class="text">Банк Зенит</span>
                            </div>
                            <div class="placeholder">
                                <span class="placeholder-empty-icon"></span>
                                <span class="text">Банковская группа ЗЕНИТ</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3 col-right">
                        <div class="block-filtr filtr-block-3">
                            <div class="wr_complex_input complex_input_noicon">
                                <div class="complex_input_body">
                                    <div id="maps-search-adres" class="complex_input_body">
                                        <input type="text" name="" class="simple_input" placeholder="" value="">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </div>
                                    <div class="text search">поиск по адресу или метро</div>
                                </div>
                            </div>
                        </div>
                        <div id="maps-atmstatus" class="block-filtr filtr-block-4">
                            <?php foreach ($arResult['filterAtmParam'] as $key => $fap): ?>
                                <label><input type="checkbox" class="formstyle" name="filterParam" value="<?=$key?>"><?=$fap?></label>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Отображение списком -->
        <div id="list-bank" class="offices">
            <!-- Отображение офисов -->
            <div id="offices" class="container">
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-9 list-bank-mobile ">
                        <div id="offices-list-filtr">
                            <a type="office" href="#modal_form-office" id="placeholder-1" class="open_modal open_modal_mobile hover">
                                <div class="card-block-list views-row-2">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <h3 class="title-ofice">Дополнительный офис “Европейский”</h3>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="adres">м. Парк Победы МО, Одинцовский район, рабочий поселок Новоивановское, ул. Луговая, д. 1е</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="phone-one">8 (495) 937-07-37,</div>
                                            <div class="phone-two">8 (495) 777-57-07</div>
                                            <div class="free"></div>
                                        </div>
                                        <div class="col-sm-12 col-mt-4 col-md-4">
                                            <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                            <div class="weekend">сб. 10:00 – 17:00</div>
                                            <div class="free"></div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="row full-linck mb-5">
                            <div class="col-sm-12 col-md-12">
                                <a href="#" class="button bigmaxwidth">показать все офисы</a>
                            </div>
                        </div>
                    </div>
                    <!-- Фильтр офисов -->
                    <div id="form-list-ofis" class="col-sm-12 col-md-3 hidden-xs hidden-sm">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div class="wr_complex_input complex_input_noicon">
                                        <div class="complex_input_body">
                                            <div id="search-adres" class="complex_input_body">
                                                <input type="text" name="search-list-ofis" class="simple_input" placeholder="" value="">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </div>
                                            <div class="text search">поиск по адресу или метро</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div id="user-type" class="jq-selectbox jqselect formstyle">
                                        <select data-programm="" data-plasholder="Услуги" data-title="Услуги" class="formstyle" name="service">
                                            <?php foreach ($arResult['service'] as $key => $service): ?>
                                                <option value="<?=$service?>"><?=$key?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div id="ofisstatus" class="block-filtr-list">
                                    <?php foreach ($arResult['filterBranchParam'] as $key => $fap): ?>
                                        <label><input type="checkbox" class="formstyle" name="filterParam" value="<?=$key?>"><?=$fap?></label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Отображение банкоматов -->
            <div id="cash-dispenser" class="container hidden">
                <div class="row mt-4">
                    <div class="col-sm-12 col-md-9 list-bank-mobile ">
                        <a type="atm" href="#modal_form-atm" id="placeholder-6" class="open_modal open_modal_mobile hover">
                            <div class="card-block-list views-row-1">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <h3 class="title-ofice">Круглосуточный банкомат</h3>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="adres">м. Красносельская Москва, Банный перулок, дом 9</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="round-clock">Круглосуточно</div>
                                        <div class="free"></div>
                                    </div>
                                    <div class="col-sm-12 col-mt-4 col-md-4">
                                        <div class="currency">Рубли РФ</div>
                                        <div class="cash-acceptance">Cash-in (прием наличных)</div>
                                        <div class="free"></div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <div class="row full-linck mb-5">
                            <div class="col-sm-12 col-md-12">
                                <a href="#" class="button bigmaxwidth">показать все банкоматы</a>
                            </div>
                        </div>
                    </div>
                    <!-- Фильтр банкоматов -->
                    <div id="form-list-arm" class="col-sm-12 col-md-3 hidden-xs hidden-sm">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div class="block-filtr-list select">
                                    <div class="wr_complex_input complex_input_noicon">
                                        <div class="complex_input_body">
                                            <div id="search-adres" class="complex_input_body">
                                                <input type="text" name="search-adres-arm" class="simple_input" placeholder="" value="">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </div>
                                            <div class="text search">поиск по адресу или метро</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 pl-0">
                                <div id="atmstatus" class="block-filtr-list">
                                    <?php foreach ($arResult['filterAtmParam'] as $key => $fap): ?>
                                        <label><input type="checkbox" class="formstyle" name="filterParam" value="<?=$key?>"><?=$fap?></label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Модальное окно для отображения информации на банкомата -->
<div class="modal-open modal">
    <div id="modal_form-atm" class="modal_div">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header hidden-xs hidden-sm">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel">Круглосуточный банкомат</h4>
                </div>
                <div class="modal-header hidden-lg">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel">Выбранный банкомат</h4>
                </div>
                <div class="mobile-modal-content">
                    <div class="modal-body">
                        <div class="modal-content-top">
                            <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel">Круглосуточный банкомат</h4>
                            <div class="adres">
                                м. Красносельская Москва, Банный перулок, дом 9
                            </div>
                            <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                            <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                        </div>
                        <div class="modal-content-bottom">
                            <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                    <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                    <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                </ul>
                                <div class="info-content">
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                        <div class="row block-line">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>режим работы</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">сб. 10:00 – 17:00</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>касса</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                            </div>
                                        </div>
                                        <div class="row block-line-bottom">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Физ. лицам</h5>
                                                <div class="services">Денежные переводы</div>
                                                <div class="services">Прием и выдача денежных средств</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Пенсионерам</h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                        <div class="row block-line">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>режим работы</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">сб. 10:00 – 17:00</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>касса</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                            </div>
                                        </div>
                                        <div class="row block-line-bottom">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Корпоративным клиентам</h5>
                                                <div class="services">Денежные переводы</div>
                                                <div class="services">Прием и выдача денежных средств</div>
                                                <h5>услуги Инвестиционным банкам</h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Малому и среднему бизнесу </h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                                <h5>услуги Финансовым институтам</h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close close_marshrut">Построение маршрута</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Модальное окно для отображения информации офиса -->
<div class="modal-open modal">
    <div id="modal_form-office" class="modal_div">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header hidden-xs hidden-sm">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel">Торгово-развлекательный центр "Европейский"</h4>
                </div>
                <div class="modal-header hidden-lg">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title" id="exampleModalLabel">Выбранное отделение</h4>
                </div>
                <div class="mobile-modal-content">
                    <div class="modal-body">
                        <div class="modal-content-top">
                            <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel">Торгово-развлекательный центр "Европейский"</h4>
                            <div class="adres">
                                м. Сокольники, м. Комсомольская, Москва, площадь Киевского вокзала, 2
                                Торгово-развлекательный центр "Европейский", первый этаж
                            </div>
                            <div class="phone-one"><a href="tel:84959370737">8 (495) 937-07-37</a>,</div>
                            <div class="phone-two"><a href="tel:84957775707">8 (495) 777-57-07</a></div>
                        </div>
                        <div class="modal-content-bottom">
                            <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                <ul class="content_tab c-container ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                                    <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="content-tabs-1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true"><a href="#content-tabs-1" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1">Физическим лицам</a></li>
                                    <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="content-tabs-2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false"><a href="#content-tabs-2" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2">Юридическим лицам</a></li>
                                </ul>
                                <div class="info-content">
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-1" aria-labelledby="ui-id-1" role="tabpanel" aria-hidden="false">
                                        <div class="row block-line">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>режим работы</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">сб. 10:00 – 17:00</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>касса</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                            </div>
                                        </div>
                                        <div class="row block-line-bottom">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Физ. лицам</h5>
                                                <div class="services">Денежные переводы</div>
                                                <div class="services">Прием и выдача денежных средств</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Пенсионерам</h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content" id="content-tabs-2" aria-labelledby="ui-id-2" role="tabpanel" aria-hidden="true" style="display: none;">
                                        <div class="row block-line">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>режим работы</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">сб. 10:00 – 17:00</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>касса</h5>
                                                <div class="weekdays">пн.–пт.: 09:30 – 20:30</div>
                                                <div class="weekend">тех. перерыв: 13.00 - 13.30</div>
                                            </div>
                                        </div>
                                        <div class="row block-line-bottom">
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Корпоративным клиентам</h5>
                                                <div class="services">Денежные переводы</div>
                                                <div class="services">Прием и выдача денежных средств</div>
                                                <h5>услуги Инвестиционным банкам</h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                            </div>
                                            <div class="col-sm-12 col-mt-6 col-md-6">
                                                <h5>услуги Малому и среднему бизнесу </h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                                <h5>услуги Финансовым институтам</h5>
                                                <div class="services">Автокредитование</div>
                                                <div class="services">Ипотечное кредитование</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" data-placeholder="placeholder-5" id="marshrut" class="modal_close close_marshrut">Построение маршрута</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Мобильный фильтр офисов и бпнкоматов -->
<div id="modal_form_filtr" class="modal_div mobile">
    <div class="row">
        <div class="col-sm-12 col-md-12 modal_fon pb-0">
            <div class="row py-3 top-modal">
                <div class="col-sm-12 col-md-12 px-5">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title">Фильтр</h4>
                </div>
            </div>
            <div class="row content-modal">
                <div class="col-sm-12 col-md-12 px-5">
                    <div id="bank-filtr" class="jq-selectbox jqselect formstyle bank">
                        <select data-programm="" data-plasholder="Банк группы ЗЕНИТ" data-title="Банк группы ЗЕНИТ" class="formstyle" name="programm">
                            <option value="all" selected="">Все банки</option>
                            <?php foreach ($arResult['banks'] as $bank):?>
                                <option value="<?=$bank['id']?>"><?=$bank['name']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div id="works" class="block-filtr-list row">
                        <?php foreach ($arResult['filterAtmParam'] as $key => $fap): ?>
                            <div class="col-md-12 col-mt-6">
                                <label><input type="checkbox" class="formstyle" name="filterParam" value="<?=$key?>"><?=$fap?></label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row footer-madal">
                <div class="col-sm-12 col-md-12 px-5">
                    <div class="form_application_line">
                        <input class="button bigmaxwidth" value="Применить фильтр" type="submit">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>