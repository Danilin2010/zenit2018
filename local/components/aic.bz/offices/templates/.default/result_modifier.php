<?php
$arResult['filterAtmParam'] = [
    'now' => 'Работает сейчас',
    '24hours' => 'Круглосуточно',
    'nearest' => 'Ближайший',
    'cashIn' => 'Прием наличных',
    'exchange' => 'Обмен валюты',
    'withdrawCurrency' => 'Снятие в валюте',
];

$arResult['filterBranchParam'] = [
    //'now' => 'Работает сейчас',
    'nearest' => 'Ближайший',
];


$serviceTemp = [];

foreach ($arResult['service'] as $key => $item) {

    if(in_array($item['type'], ['fiz', 'ur'])) {
        $arResult['serviceOffice'][] = $item;
    }
    if($item['type'] == 'atm') {
        $arResult['serviceAtm'][] = $item;
    }

}
