<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<div class="recommended-card">
    <a class="recommended-card__wrapper" href="<?=$arParams["RECOMMENDED_LINK"]?>"<?php if ($arParams["RECOMMENDED_TARGET"] == "Y") { ?>target="_blank"<?php } ?>>
        <div class="recommended-card__inner">
            <div class="recommended-card__image-wrapper">
                <img class="recommended-card__image" src="<?=$arParams["RECOMMENDED_IMG"]?>">
            </div>
            <div class="recommended-card__title"><?=$arParams["~RECOMMENDED_TITLE"]?></div>
            <div class="recommended-card__text"><?=$arParams["~RECOMMENDED_TEXT"]?></div>
        </div>
    </a>
</div>