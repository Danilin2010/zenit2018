<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{

	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'RECOMMENDED_TITLE' => Array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_RECOMMENDED_TITLE'),
				'TYPE' => 'STRING',
				'DEFAULT' => '',
			),
            'RECOMMENDED_TEXT' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_RECOMMENDED_TEXT'),
                'TYPE' => 'STRING',
            ),
            'RECOMMENDED_LINK' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_RECOMMENDED_LINK'),
                'TYPE' => 'STRING',
                'DEFAULT' => '',
            ),
			'RECOMMENDED_TARGET' => array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_RECOMMENDED_TARGET'),
				'TYPE' => 'CHECKBOX',
			),
			'RECOMMENDED_IMG' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_RECOMMENDED_IMG'),
                'TYPE' => 'STRING',
                'DEFAULT' => '',
			),
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>