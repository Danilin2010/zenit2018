<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Онлайн-резервирование номера счета</title>

    <link rel="icon" type="image/png" href="/local/templates/bz/img/favicon.ico" />
    <meta name="theme-color" content="#1da9af">
    <link rel="icon" sizes="192x192" href="/local/templates/bz/img/icon/android-chrome-192.png">
    <meta content="app-id=1187159866" name="apple-itunes-app">
    <meta content="app-id=ru.zenit.zenitonline" name="google-play-app">

    <link rel="manifest" href="/manifest/manifest.json">



    <!--<link rel="stylesheet" type="text/css" href="pixel/pixel1.css" />-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="index, follow" />
    <meta name="description" content="Онлайн-резервирование номера счета" />
    <script type="text/javascript" data-skip-moving="true">(function(w, d, n) {var cl = "bx-core";var ht = d.documentElement;var htc = ht ? ht.className : undefined;if (htc === undefined || htc.indexOf(cl) !== -1){return;}var ua = n.userAgent;if (/(iPad;)|(iPhone;)/i.test(ua)){cl += " bx-ios";}else if (/Android/i.test(ua)){cl += " bx-android";}cl += (/(ipad|iphone|android|mobile|touch)/i.test(ua) ? " bx-touch" : " bx-no-touch");cl += w.devicePixelRatio && w.devicePixelRatio >= 2? " bx-retina": " bx-no-retina";var ieVersion = -1;if (/AppleWebKit/.test(ua)){cl += " bx-chrome";}else if ((ieVersion = getIeVersion()) > 0){cl += " bx-ie bx-ie" + ieVersion;if (ieVersion > 7 && ieVersion < 10 && !isDoctype()){cl += " bx-quirks";}}else if (/Opera/.test(ua)){cl += " bx-opera";}else if (/Gecko/.test(ua)){cl += " bx-firefox";}if (/Macintosh/i.test(ua)){cl += " bx-mac";}ht.className = htc ? htc + " " + cl : cl;function isDoctype(){if (d.compatMode){return d.compatMode == "CSS1Compat";}return d.documentElement && d.documentElement.clientHeight;}function getIeVersion(){if (/Opera/i.test(ua) || /Webkit/i.test(ua) || /Firefox/i.test(ua) || /Chrome/i.test(ua)){return -1;}var rv = -1;if (!!(w.MSStream) && !(w.ActiveXObject) && ("ActiveXObject" in w)){rv = 11;}else if (!!d.documentMode && d.documentMode >= 10){rv = 10;}else if (!!d.documentMode && d.documentMode >= 9){rv = 9;}else if (d.attachEvent && !/Opera/.test(ua)){rv = 8;}if (rv == -1 || rv == 8){var re;if (n.appName == "Microsoft Internet Explorer"){re = new RegExp("MSIE ([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}else if (n.appName == "Netscape"){rv = 11;re = new RegExp("Trident/.*rv:([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}}return rv;}})(window, document, navigator);</script>


    <link href="/local/templates/bz/components/bitrix/form.result.new/reserve/style.css?15120510096356" type="text/css"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/css/core.min.css?15115207592854" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/slick/slick.css?15120509101733" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/jquery-ui/jquery-ui.structure.css?15120509103469" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/baraja/baraja.css?1512050909670" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/smartbanner/jquery.smartbanner.css?15120509114064" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/toggles/css/toggles.css?1512050995484" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/toggles/css/themes/toggles-modern.css?15120510621452" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/toggles/css/themes/toggles-light.css?1512051061815" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/slider-pips/jquery-ui-slider-pips.css?15120509119292" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/vendors/jslider/css/jquery.jslider-all.css?15120509876627" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/font/Lato/font.css?15120507882163" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/font/ProximaNova/font.css?151205078813882" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/font-awesome.css?151205075937512" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/toggle.css?1512050759282" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/icon.css?15120507583365" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/gui.css?151205075810013" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/_sky.css?15120507591410" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/slider.css?15120507591143" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/main.css?1513871375123621" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/1600.css?15120507572121" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/900.css?151274379733167" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/mobile.css?151205075918819" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/offices-mobile.css?151272141024741" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/style.css?151205075925644" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/about.css?15123978854398" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/about-mobile.css?15120507571319" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/offices.css?15120507595447" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/css/search.css?15120507595237" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/panel/main/popup.min.css?151152075120704" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/components/bitrix/form.result.new/contact/style.css?1512051009100" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/css/core_popup.min.css?151558750713169" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/css/core_date.min.css?15115207599657" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/css/core_panel.min.css?151152075962290" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/panel/main/admin-public.min.css?151558749873050" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/panel/main/hot_keys.min.css?15115207513150" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/themes/.default/pubstyles.min.css?151152075746822" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="/local/templates/bz/template_styles.css?151205075129" type="text/css"  data-template-style="true"  rel="stylesheet" />
    <link href="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/fileman/sticker.min.css?151152075924594" type="text/css"  data-template-style="true"  rel="stylesheet" />




</head>

<body
    class="to_page ">
<div id="panel">
    <!--[if lte IE 7]>
    <style type="text/css">#bx-panel {display:none !important;}</style>
    <div id="bx-panel-error">Административная панель не поддерживает Internet Explorer версии 7 и ниже. Установите современный браузер <a href="http://www.firefox.com">Firefox</a>, <a href="http://www.google.com/chrome/">Chrome</a>, <a href="http://www.opera.com">Opera</a> или <a href="http://www.microsoft.com/windows/internet-explorer/">Internet Explorer 8</a>.</div><![endif]-->
    <div style="display:none; overflow:hidden;" id="bx-panel-back"></div>
    <div id="bx-panel" class="bx-panel-folded">
        <div id="bx-panel-top">
            <div id="bx-panel-top-gutter"></div>
            <div id="bx-panel-tabs">

                <a id="bx-panel-menu" href="" onmouseover="BX.hint(this, 'Меню', 'Быстрый переход на любую страницу Административной части.')"><span id="bx-panel-menu-icon"></span><span id="bx-panel-menu-text">Меню</span></a><a id="bx-panel-view-tab"><span>Сайт</span></a><a id="bx-panel-admin-tab" href="/bitrix/admin/index.php?lang=ru&amp;back_url_pub=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F"><span>Администрирование</span></a><a class="adm-header-notif-block" id="adm-header-notif-block" onclick="return BX.adminInformer.Toggle(this);" href="" title="Просмотр уведомлений"><span class="adm-header-notif-icon"></span><span id="adm-header-notif-counter" class="adm-header-notif-counter">4</span></a><a id="bx-panel-clear-cache" href="" onclick="BX.clearCache(); return false;"><span id="bx-panel-clear-cache-icon"></span><span id="bx-panel-clear-cache-text">Сбросить кеш</span></a>
            </div>
            <div id="bx-panel-userinfo">
                <a href="/bitrix/admin/user_edit.php?lang=ru&ID=1" id="bx-panel-user" onmouseover="BX.hint(this, 'Быстрый переход на страницу редактирования параметров пользователя.')"><span id="bx-panel-user-icon"></span><span id="bx-panel-user-text">admin</span></a><a href="/businesses/rko/account/online-reserve/?logout=yes" id="bx-panel-logout" onmouseover="BX.hint(this, 'Выход пользователя из системы. (&amp;nbsp;Ctrl+Alt+O&amp;nbsp;) ')">Выйти</a><a href="/businesses/rko/account/online-reserve/?bitrix_include_areas=Y" id="bx-panel-toggle" class="bx-panel-toggle-off" onmouseover="BX.hint(this, 'Режим правки', 'Включение режима контекстного редактирования сайта. При включенном режиме наведите курсор на выбранный блок информации и используйте всплывающие панели инструментов. (&amp;nbsp;Ctrl+Alt+D&amp;nbsp;) ')"><span id="bx-panel-switcher-gutter-left"></span><span id="bx-panel-toggle-indicator"><span id="bx-panel-toggle-icon"></span><span id="bx-panel-toggle-icon-overlay"></span></span><span class="bx-panel-break"></span><span id="bx-panel-toggle-caption">Режим правки</span><span class="bx-panel-break"></span><span id="bx-panel-toggle-caption-mode"><span id="bx-panel-toggle-caption-mode-off">выключен</span><span id="bx-panel-toggle-caption-mode-on">включен</span></span><span id="bx-panel-switcher-gutter-right"></span></a><a href="" id="bx-panel-expander" onmouseover="BX.hint(this, 'Развернуть', 'Развернуть Панель управления. (&amp;nbsp;Ctrl+Alt+E&amp;nbsp;) ')"><span id="bx-panel-expander-text">Развернуть</span><span id="bx-panel-expander-arrow"></span></a><a id="bx-panel-hotkeys" href="javascript:void(0)" onclick="BXHotKeys.ShowSettings();" onmouseover="BX.hint(this, 'Настройки горячих клавиш (&amp;nbsp;Ctrl+Alt+P&amp;nbsp;) ')"></a><a href="javascript:void(0)" id="bx-panel-pin" class="bx-panel-pin-fixed" onmouseover="BX.hint(this, 'Фиксация Панели управления в верхней части экрана.')"></a>			</div>
        </div>
        <div id="bx-panel-site-toolbar"><div id="bx-panel-buttons-gutter"></div><div id="bx-panel-switcher"><a href="" id="bx-panel-hider" onmouseover="BX.hint(this, 'Свернуть', 'Свернуть Панель управления. (&amp;nbsp;Ctrl+Alt+E&amp;nbsp;) ')">Свернуть<span id="bx-panel-hider-arrow"></span></a></div><div id="bx-panel-buttons"><div id="bx-panel-buttons-inner"><span class="bx-panel-button-group" data-group-id="0"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_file_new.php?lang=ru&amp;site=s1&amp;templateID=bz&amp;path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;siteTemplateId=bz','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_create"><span class="bx-panel-button-icon bx-panel-create-page-icon"></span></a><a id="bx_topmenu_btn_create_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Создать<span class="bx-panel-break"></span>страницу&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_file_new.php?lang=ru&amp;site=s1&amp;templateID=bz&amp;newFolder=Y&amp;path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;siteTemplateId=bz','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_create_section"><span class="bx-panel-button-icon bx-panel-create-section-icon"></span></a><a id="bx_topmenu_btn_create_section_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Создать<span class="bx-panel-break"></span>раздел&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="1"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CEditorDialog({'content_url':'/bitrix/admin/public_file_edit.php?lang=ru&amp;path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&amp;site=s1&amp;back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;templateID=bz&amp;siteTemplateId=bz','width':'1169','height':'452','min_width':'780','min_height':'400'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_edit"><span class="bx-panel-button-icon bx-panel-edit-page-icon"></span></a><a id="bx_topmenu_btn_edit_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Изменить<span class="bx-panel-break"></span>страницу&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_folder_edit.php?lang=ru&amp;site=s1&amp;path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;siteTemplateId=bz','width':'','height':'','min_width':'450','min_height':'250'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_edit_section"><span class="bx-panel-button-icon bx-panel-edit-section-icon"></span></a><a id="bx_topmenu_btn_edit_section_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Изменить<span class="bx-panel-break"></span>раздел&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="2"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_menus"><span class="bx-panel-small-button-icon bx-panel-menu-icon"></span><span class="bx-panel-small-button-text">Меню</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CDialog({'content_url':'/bitrix/admin/public_structure.php?lang=ru&amp;site=s1&amp;path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&amp;templateID=bz&amp;siteTemplateId=bz','width':'350','height':'470'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_structure"><span class="bx-panel-small-button-icon bx-panel-site-structure-icon"></span><span class="bx-panel-small-button-text">Структура</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_structure_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CAdminDialog({'content_url':'/bitrix/admin/public_seo_tools.php?lang=ru&amp;bxpublic=Y&amp;from_module=seo&amp;site=s1&amp;path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&amp;title_final=0J7QvdC70LDQudC9LdGA0LXQt9C10YDQstC40YDQvtCy0LDQvdC40LUg0L3QvtC80LXRgNCwINGB0YfQtdGC0LA%3D&amp;title_changer_name=&amp;title_changer_link=&amp;title_win_final=&amp;title_win_changer_name=&amp;title_win_changer_link=&amp;sessid=391a1a78c5e0808db9c4928c4f11c92c&amp;back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;siteTemplateId=bz','width':'920','height':'400'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-active')" id="bx_topmenu_btn_seo"><span class="bx-panel-small-button-icon bx-panel-seo-icon"></span><span class="bx-panel-small-button-text">SEO</span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="3"><span class="bx-panel-button"><span class="bx-panel-button-inner"><a href="javascript:void(0)" onclick="BX.clearCache();BX.removeClass(this.parentNode.parentNode, 'bx-panel-button-icon-active');" id="bx_topmenu_btn_0"><span class="bx-panel-button-icon bx-panel-clear-cache-icon"></span></a><a id="bx_topmenu_btn_0_menu" href="javascript:void(0)"><span class="bx-panel-button-text">Сбросить<span class="bx-panel-break"></span>кеш&nbsp;<span class="bx-panel-button-arrow"></span></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="4"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_components"><span class="bx-panel-small-button-icon bx-panel-components-icon"></span><span class="bx-panel-small-button-text">Компоненты</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_1"><span class="bx-panel-small-button-icon bx-panel-site-template-icon"></span><span class="bx-panel-small-button-text">Шаблон сайта</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="/businesses/rko/account/online-reserve/?show_page_exec_time=Y&amp;show_include_exec_time=Y&amp;show_sql_stat=Y" onclick=";BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_2"><span class="bx-panel-small-button-icon bx-panel-performance-icon"></span><span class="bx-panel-small-button-text">Отладка</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_2_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span></span><span class="bx-panel-button-separator"></span><span class="bx-panel-button-group" data-group-id="5"><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" id="bx_topmenu_btn_3"><span class="bx-panel-small-button-icon bx-panel-statistics-icon"></span><span class="bx-panel-small-button-text">Статистика</span><span class="bx-panel-small-single-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="(new BX.CAdminDialog({'content_url':'/bitrix/admin/short_uri_edit.php?lang=ru&amp;public=Y&amp;bxpublic=Y&amp;str_URI=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;site=s1&amp;back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&amp;siteTemplateId=bz','width':'770','height':'270'})).Show();BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_4"><span class="bx-panel-small-button-icon bx-panel-short-url-icon"></span><span class="bx-panel-small-button-text">Короткий URL</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_4_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span><span class="bx-panel-break"></span><span class="bx-panel-small-button"><span class="bx-panel-small-button-inner"><a href="javascript:void(0)" onclick="if (window.oBXSticker){window.oBXSticker.AddSticker();};BX.removeClass(this.parentNode.parentNode, 'bx-panel-small-button-text-active')" id="bx_topmenu_btn_5"><span class="bx-panel-small-button-icon bx-panel-small-stickers-icon"></span><span class="bx-panel-small-button-text">Стикеры</span></a><a href="javascript:void(0)" class="bx-panel-small-button-arrow" id="bx_topmenu_btn_5_menu"><span class="bx-panel-small-button-arrow"></span></a></span></span></span></div>
            </div>
        </div>
    </div>
    <div class="adm-informer" id="admin-informer" style="display: none; top:48px; left:316px;" onclick="return BX.adminInformer.OnInnerClick(event);">
        <div class="adm-informer-header">Новые уведомления</div>
        <div class="adm-informer-item adm-informer-item-green" style="display:block">
            <div class="adm-informer-item-title">
                Оцените решение Маркетплейс
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-0">
                    Вы используете решение <b>reCaptcha 2.0 Google Free</b> (twim.recaptchafree).<br />Поделитесь своими впечатлениями.<br />
                    Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-0">
                    <a href="javascript:void(0)" onclick="hideMpAnswer(this, 'twim.recaptchafree')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/twim.recaptchafree/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'twim.recaptchafree')">Оставить отзыв</a>
                </div>
            </div>
        </div>
        <div class="adm-informer-item adm-informer-item-green" style="display:block">
            <div class="adm-informer-item-title">
                Оцените решение Маркетплейс
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-1">
                    Вы используете решение <b>Быстрая подписка на рассылки</b> (asd.subscribequick).<br />Поделитесь своими впечатлениями.<br />
                    Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-1">
                    <a href="javascript:void(0)" onclick="hideMpAnswer(this, 'asd.subscribequick')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/asd.subscribequick/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'asd.subscribequick')">Оставить отзыв</a>
                </div>
            </div>
        </div>
        <div class="adm-informer-item adm-informer-item-green" style="display:block">
            <div class="adm-informer-item-title">
                Оцените решение Маркетплейс
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-2">
                    Вы используете решение <b>Субэлементы</b> (sokrat.subelement).<br />Поделитесь своими впечатлениями.<br />
                    Что понравилось?<br />Есть ли замечания?<br />Ваш отзыв очень важен для нас!
                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-2">
                    <a href="javascript:void(0)" onclick="hideMpAnswer(this, 'sokrat.subelement')" style="float: right !important; font-size: 0.8em !important;">Скрыть</a><a href="http://marketplace.1c-bitrix.ru/solutions/sokrat.subelement/#tab-rating-link" target="_blank" onclick="hideMpAnswer(this, 'sokrat.subelement')">Оставить отзыв</a>
                </div>
            </div>
        </div>
        <div class="adm-informer-item adm-informer-item-peach" style="display:block">
            <div class="adm-informer-item-title">
                Резервное копирование
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-3">

                    <div class="adm-informer-item-section">
					<span class="adm-informer-item-l">
						<span class="adm-informer-strong-text">Всего:</span> 6 ГБ
					</span>
                        <span class="adm-informer-item-r">
							<span class="adm-informer-strong-text">Доступно:</span> 6 ГБ
					</span>
                    </div>
                    <div class="adm-informer-status-bar-block" >
                        <div class="adm-informer-status-bar-indicator" style="width:0%; "></div>
                        <div class="adm-informer-status-bar-text">0%</div>
                    </div>
                    <span class="adm-informer-strong-text">Не выполнялось.</span>
                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-3">
                    <a href="/bitrix/admin/dump.php?lang=ru">Рекомендуется выполнить</a>
                </div>
            </div>
        </div>
        <div class="adm-informer-item adm-informer-item-gray" style="display:none">
            <div class="adm-informer-item-title">
                Обновления
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-4">
                    <span class="adm-informer-strong-text">Версия системы 17.5.8</span><br>Последнее обновление<br>10.01.2018 15:35
                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-4">
                    <a href="/bitrix/admin/update_system.php?refresh=Y&lang=ru">Проверить обновления</a>
                </div>
            </div>
        </div>
        <div class="adm-informer-item adm-informer-item-blue" style="display:none">
            <div class="adm-informer-item-title">
                Проактивная защита
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-5">

                    <div class="adm-informer-item-section">
	<span class="adm-informer-item-l">
		<span class="adm-informer-strong-text">Защита включена.<br></span>
		<span>Попыток вторжения не обнаружено</span>
	</span>
                    </div>

                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-5">
                    <a href="/bitrix/admin/security_filter.php?lang=ru">Настроить защиту</a>
                </div>
            </div>
        </div>
        <div class="adm-informer-item adm-informer-item-green" style="display:none">
            <div class="adm-informer-item-title">
                Ускорение сайта (CDN)
            </div>
            <div class="adm-informer-item-body">
                <div class="adm-informer-item-html" id="adm-informer-item-html-6">

                    <div class="adm-informer-item-section">
						<span class="adm-informer-item-l">
							<span class="adm-informer-strong-text">Всего:</span> 30 ГБ
						</span>
                        <span class="adm-informer-item-r">
								<span class="adm-informer-strong-text">Доступно:</span> 30 ГБ
						</span>
                    </div>
                    <div class="adm-informer-status-bar-block" >
                        <div class="adm-informer-status-bar-indicator" style="width:0%; "></div>
                        <div class="adm-informer-status-bar-text">0%</div>
                    </div>

                    <span class="adm-informer-icon"></span>
                </div>
                <div class="adm-informer-item-footer" id="adm-informer-item-footer-6">
                    <a href="/bitrix/admin/bitrixcloud_cdn.php?lang=ru">Настроить CDN</a>
                </div>
            </div>
        </div>
        <a href="javascript:void(0);" class="adm-informer-footer adm-informer-footer-collapsed" hidefocus="true" id="adm-informer-footer" onclick="return BX.adminInformer.ToggleExtra();" >Показать все (7) </a>
        <span class="adm-informer-arrow"></span>
    </div>

</div>
<!--select_city-->
<div class="wr_select_city" data-wr-city>
    <div class="select_city">
        <div class="select_city_fr">
            <div class="select_city_close" data-close-city></div>
            <div class="select_city_title">Выберите свой или ближайший город</div>
            <div class="select_city_body">
                <div class="select_city_block">
                    <div class="select_city_left">
                        А                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=763" data-set-cook-city
                                >Альметьевск</a></li>
                            <li><a
                                    href="?set_sity=764" data-set-cook-city
                                >Арзамас</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        В                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=766" data-set-cook-city
                                >Воронеж</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        Г                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=768" data-set-cook-city
                                >Горно-Алтайск</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        Е                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=772" data-set-cook-city
                                >Екатеринбург</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        И                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=775" data-set-cook-city
                                >Ижевск</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        К                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=776" data-set-cook-city
                                >Казань</a></li>
                            <li><a
                                    href="?set_sity=8" data-set-cook-city
                                >Калининград</a></li>
                            <li><a
                                    href="?set_sity=9" data-set-cook-city
                                >Кемерово</a></li>
                            <li><a
                                    href="?set_sity=10" data-set-cook-city
                                >Курск</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        М                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=11" data-set-cook-city
                                    data-set-city class="select"                                   >Москва и МО</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        Н                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=12" data-set-cook-city
                                >Нижнекамск</a></li>
                            <li><a
                                    href="?set_sity=13" data-set-cook-city
                                >Нижний Новгород</a></li>
                            <li><a
                                    href="?set_sity=14" data-set-cook-city
                                >Новокузнецк</a></li>
                            <li><a
                                    href="?set_sity=15" data-set-cook-city
                                >Новосибирск</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        О                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=16" data-set-cook-city
                                >Омск</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        П                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=17" data-set-cook-city
                                >Пермь</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        Р                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=18" data-set-cook-city
                                >Ростов-на-Дону</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        С                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=19" data-set-cook-city
                                >Самара</a></li>
                            <li><a
                                    href="?set_sity=20" data-set-cook-city
                                >Санкт-Петербург</a></li>
                            <li><a
                                    href="?set_sity=21" data-set-cook-city
                                >Саратов</a></li>
                            <li><a
                                    href="?set_sity=22" data-set-cook-city
                                >Саров</a></li>
                            <li><a
                                    href="?set_sity=785" data-set-cook-city
                                >Сургут</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        Т                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=23" data-set-cook-city
                                >Таганрог</a></li>
                            <li><a
                                    href="?set_sity=24" data-set-cook-city
                                >Тольятти</a></li>
                        </ul>
                    </div>
                </div>
                <div class="select_city_block">
                    <div class="select_city_left">
                        Ч                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <li><a
                                    href="?set_sity=25" data-set-cook-city
                                >Челябинск</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="wr_bottom_city">
                <div class="bottom_city_title">
                    Банковская Группа ЗЕНИТ                </div>
                <div class="bottom_city_list">
                    <div class="bottom_city_item bottom_city_item001">

                    </div>
                    <div class="bottom_city_item bottom_city_item002">

                    </div>
                    <div class="bottom_city_item bottom_city_item003">

                    </div>
                    <div class="bottom_city_item bottom_city_item004">

                    </div>
                    <div class="bottom_city_item bottom_city_item005">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--select_sity-->
<div class="main_wrapper">

    <div id="header-modal" class="wr_fix_top">
        <div class="fix_top">
            <!--top_line-->
            <div class="wr_top_line">
                <div class="top_line z-container">
                    <div class="top_line_right">
                        <ul class="ul_top_line_right">
                            <li><a class="" href="/rus/about_bank/">О Банке</a></li>
                            <li class="b_select_city open">
                                <a href="#" title="Выбор города" data-select-city>Москва и МО</a>
                                <div class="wr_select_city_in">
                                    <div class="select_city">
                                        <div class="select_city_text">
                                            Ваш город
                                        </div>
                                        <div class="select_city_name">
                                            Москва и МО                                        </div>
                                        <table>
                                            <tr>
                                                <td><a class="button maxwidth" data-close-city data-set-cook-city href="javascript:void(0)">Да</a></td>
                                                <td><a href="#" title="Выбор города" data-select-city>выбрать другой</a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            <li><a href="/offices/?type=atm"><div class="ico map"></div>Банкоматы</a> и <a href="/offices/">офисы</a></li>
                            <li class="menu_contact"><a href="" class="cd-dropdown-trigger"><div class="ico note"></div>Связаться с нами</a>
                                <div class="container-menu">
                                    <div class="cd-dropdown" id="menu_contact">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="title-cont">Бесплатно по россии</div>
                                                <div class="phone">8 (800) 500-66-77</div>
                                                <div class="content">Для физических лиц</div>
                                                <div class="phone">8 (800) 500-40-82</div>
                                                <div class="content">Для юридических лиц</div>
                                                <div class="row icon">
                                                    <div class="col-sm-6 col-md-6 icon-linck">
                                                        <a href="skype:zenitgroup.ru?call">
                                                            <i class="fa fa-skype" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                    <!--<div class="col-sm-6 col-md-6 icon-linck">
                                                        <a href="#">
                                                            <i class="fa fa-weixin" aria-hidden="true"></i>
                                                        </a>
                                                    </div>-->
                                                    <div class="col-sm-12 col-md-12 linck">
                                                        <a href="#modal_form-contact" class="open_modal">Обратная связь</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="search wr_js_form" title="Поиск по сайту">
                            <div class="ico_search"></div>
                            <div class="wr_block_search">
                                <div class="block_search">
                                    <form class="form_search" action="/search/">
                                        <input name="s" type="hidden" value="Поиск" />
                                        <div class="block_search_close"></div>
                                        <div class="block_search_input">
                                            <input type="text" name="q" value="" autocomplete="off" placeholder="Я ищу..." size="15" maxlength="50" />                </div>
                                    </form>
                                </div>
                                <div class="wr_block_search_result">
                                    <div class="block_search_result z-container">

                                    </div>
                                </div>
                            </div>
                        </div>                    </div>
                    <!--ul_top_line-->
                    <ul class="ul_top_line">
                        <li class="">
                            <a class="" href="/">Частным лицам</a>
                        </li>
                        <li class="button_select_top_menu">
                            <a class="select" >Малому и среднему бизнесу</a>
                            <ul>
                                <li><a class="selected" href="/businesses/" >Малому и среднему бизнесу</a></li>
                                <li><a class="" href="/big-business/" >Крупному бизнесу</a></li>
                                <li><a class="" href="/corp-and-fin/" >Финансовым институтам</a></li>
                            </ul>
                        </li>
                        <li class="">
                            <a class="" href="/private-banking/">Private Banking</a>
                        </li>
                    </ul>
                    <!--ul_top_line-->
                </div>
            </div>
            <!--top_line-->
            <!--header-->
            <div class="wr_header">
                <div class="mobail_bottom_menu">
                    <div class="ul_mobail_bottom_menu">
                        <div class="ul_mobail_bottom_menu_item">
                            <a data-show="burger_menu">Частным лицам</a>
                        </div><div class="ul_mobail_bottom_menu_item">
                            <a class="select"data-show="mcb_menu">Малому и среднему бизнесу</a>
                        </div><div class="ul_mobail_bottom_menu_item">
                            <a data-show="corp_menu">Крупному бизнесу</a>
                        </div><div class="ul_mobail_bottom_menu_item">
                            <a data-show="fin_menu">Финансовым институтам</a>
                        </div><div class="ul_mobail_bottom_menu_item">
                            <a >Private Banking</a>
                        </div>        </div>
                </div>
                <div class="b_wr_header">
                    <div class="header z-container">
                        <div class="wr_mobail_min_menu wr_js_form">
                            <ul class="mobail_min_menu">
                                <!--<li><a href="#" data-select-city class="map"></a></li>-->
                                <li><a href="/offices/" class="map"></a></li>
                                <li><a href="#modal_form-contact" class="open_contact_form note"></a></li>
                                <li><a href="#" class="menu"></a></li>
                            </ul>
                            <div class="container-menu glyphicon-menu-down">
                                <div class="cd-dropdown" id="menu_contact">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="title-cont">Бесплатно по россии</div>
                                            <a href="tel:88005006677" class="phone">8 (800) 500-66-77</a>
                                            <div class="content">Для физических лиц</div>
                                            <a href="tel:88005004082" class="phone">8 (800) 500-40-82</a>
                                            <div class="content">Для юридических лиц</div>
                                            <div class="row icon">
                                                <div class="col-sm-6 col-md-6 col-mb-6 col-mt-6 icon-linck">
                                                    <a href="skype:zenitgroup.ru?call">
                                                        <i class="fa fa-skype" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-mb-12 col-mt-12 linck">
                                                    <a href="#modal_form-contact" class="open_contact_form open_modal">Обратная связь</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mobail_show_menu">
                                <div class="mobail_show_menu_close"></div>
                                <a href="#" class="mobail_show_menu_castle"></a>
                                <div class="mobail_show_menu_search">
                                    <form class="form_search" action="/search/">
                                        <input name="s" type="hidden" value="Поиск" />
                                        <div class="block_search_input">
                                            <div class="block_search_ico"></div>
                                            <div class="block_search_input_wr">
                                                <input type="text" name="q" value="" autocomplete="off" placeholder="Поиск" size="15" maxlength="50" />            </div>
                                        </div>
                                    </form>
                                </div>                            </div>
                            <div class="wr_block_search_result">
                                <div class="block_search_result z-container">

                                </div>
                            </div>                        </div>
                        <a class="logo" href="/"></a>
                        <a href="tel:88005006677" class="mobile-header-phone">8 (800) 500-66-77</a>
                        <!--top_menu-->

                        <ul class="top_menu ">
                            <li><a
                                    href="#"
                                    data-show="businesses_rko"                                >РКО</a></li>

                            <li><a
                                    href="#"
                                    data-show="businesses_loans"                                >Кредиты и гарантии</a></li>

                            <li><a
                                    href="#"
                                    data-show="businesses_placement_funds"                                >Размещение средств</a></li>

                            <li><a
                                    href="#"
                                    data-show="businesses_international_settlements"                                >Международные расчеты</a></li>

                        </ul>
                        <!--top_menu-->
                        <a href="#" data-show="menu_net_bank" class="net_bank"><div class="ico close"></div>Интернет-банк</a>
                    </div>
                </div>
                <!--menu_credit-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_rko">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Открытие и ведение счета</span>
                                        <ul>
                                            <li><a href="https://old.zenit.ru/rus/businesss/operations/reserve_account/">Онлайн-резервирование номера счета</a></li>
                                            <li><a href="/businesses/rko/account/opening-of-accounts/">Открытие счетов</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Тарифные планы</span>
                                        <ul>
                                            <li><a href="/businesses/rko/tariff-plans/main-tariff-plans/">Основные тарифные планы</a></li>
                                            <!--<li><a href="/businesses/rko/tariff-plans/tariff-plan-importer/">ТП Импортер</a></li>-->
                                        </ul>
                                    </li>
                                    <li><span>Безналичные переводы</span>
                                        <ul>
                                            <li><a href="/businesses/rko/transfers/rub/">Переводы в рублях</a></li>
                                            <li><a href="/businesses/rko/transfers/currency/">Переводы в иностранной валюте</a></li>
                                            <!--<li><a href="/businesses/rko/transfers/time/">Операционное время</a></li>-->
                                        </ul>
                                    </li>
                                    <li><span>Дистанционное обслуживание</span>
                                        <ul>
                                            <li><a href="/businesses/rko/remote/clientbank/">Система Клиент-Банк</a></li>
                                            <li><a href="/businesses/rko/remote/1c/">Клиент-Банк из 1С: Предприятие</a></li>
                                            <li><a href="/businesses/rko/remote/mobail/">Мобильный банк</a></li>
                                            <li><a href="/businesses/rko/remote/sms/">SMS - информирование</a></li>
                                            <li><a href="/businesses/rko/remote/indicator/">Индикатор</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Банковские карты</span>
                                        <ul>
                                            <li><a href="/businesses/rko/bank-card/customs-card/">Таможенная карта</a></li>
                                            <li><a href="/businesses/rko/bank-card/salary-project/">Зарплатный проект</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Эквайринг</span>
                                        <ul>
                                            <li><a href="/businesses/rko/acquiring/trade-aquaring/">Торговый экваринг</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/businesses/rko/collection-and-delivery-of-cash/">Инкассация и доставка наличных</a></li>
                                    <li><span>Валютный контроль</span>
                                        <ul>
                                            <li><a href="/businesses/rko/currency-control/description-and-documents/">Описание и документы</a></li>
                                            <li><a href="/businesses/rko/currency-control/violation-of-currency-legislation/">Нарушения валютного законодательства</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Дополнительные продукты</span>
                                        <ul>
                                            <li><a href="/businesses/rko/additional/check/">Прием чеков на инкассо</a></li>
                                            <li><a href="/businesses/rko/additional/cells/">Сейфовые ячейки</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Специальные предложения и акции</span>
                                        <ul>
                                            <li><a href="/businesses/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks/">Льготные тарифы для клиентов проблемных банков</a></li>
                                            <li><a href="/businesses/rko/special-offers-and-promotions/promotions/">Акции</a></li>
                                        </ul>
                                    </li>
                                    <!--<li><span>Тарифы</span>
                                        <ul>
                                            <li><a href="/businesses/rko/rates/collection-of-tariffs/">Сборник тарифов</a></li>
                                        </ul>
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!--menu_credit-->
                <!--menu_cards-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_loans">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_block">
                                <div class="menu_bottom_block_title">Кредиты</div>
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/loans/working-capital-loan/">Оборотное кредитование</a></li>
                                    <li><a href="/businesses/loans/overdraft/">Овердрафт</a></li>
                                    <li><a href="/businesses/loans/tender-loan/">Тендерный кредит</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="menu_bottom_block_title">Инвестиционное кредитование</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/loans/investment-lending/real-estate/">Недвижимость</a></li>
                                    <li><a href="/businesses/loans/investment-lending/equipment-and-transport/">Оборудование и
                                            транспорт</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="menu_bottom_block_title">Государственная поддержка субъектов МСБ</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/loans/state-support-of-smes/program-6-5-jsc-corporation-smes/">Программа
                                            6,5 АО "Корпорации "МСП"</a></li>
                                    <li>
                                        <a href="/businesses/loans/state-support-of-smes/warranty-support-of-jsc-corporation-smes-jsc-sme-bank/">Гарантийная
                                            поддержка АО "Корпорация "МСП/АО "МСП Банк"</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="menu_bottom_block_title">Гарантии</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/warranty/electronic-guarantee-for-the-participants-of-public-procurement/">Электронная
                                            гарантия для участников госзакупок</a></li>
                                    <li><a href="/businesses/warranty/commercial-warranty/">Коммерческие гарантии</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <!--menu_cards-->
                <!--menu_payments-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_placement_funds">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">

                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li>
                                        <a href="/businesses/allocation-of-funds/how-to-place-funds-in-the-bank/five-steps-to-income/" class="">Как разместить средства в Банке?</a>
                                    </li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="menu_bottom_block_title">Депозитные продукты</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/allocation-of-funds/deposit-products/term-deposits/">Срочные депозиты</a></li>
                                    <li><a href="/businesses/allocation-of-funds/deposit-products/dual-currency-deposit/">Бивалютный депозит</a></li>
                                    <li><a href="/businesses/allocation-of-funds/deposit-products/minimum-balance-on-the-account/">Неснижаемый остаток на расчетном счете</a></li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="menu_bottom_block_title">Ценные бумаги</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">

                                    <li><a href="/businesses/allocation-of-funds/securities/certificates-of-deposit/">Депозитные сертификаты</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!--menu_payments-->
                <!--menu_insurance-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="businesses_international_settlements">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">

                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/international-payments/conversion-operations/">Конверсионные операции</a></li>
                                    <li><a href="/businesses/international-payments/documentary-operations/">Документарные операции</a></li>
                                </ul>
                            </div>

                        </div><div class="burger_menu_item">

                        </div>
                    </div>
                </div>

                <!--menu_insurance-->                <!--wr_burger_menu-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="burger_menu">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Ипотека</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Новостройки</span>
                                        <ul >
                                            <li
                                                id="bx_3218110189_39">
                                                <a href="/personal/mortgage/new/new/">Ипотека на новостройку</a> 		</li>
                                            <li
                                                id="bx_3218110189_82">
                                                <a href="/personal/mortgage/new/objects/">Ипотека в объектах, финансируемых Банком</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Военная ипотека</span>
                                        <ul class="burger_menu_menu">
                                            <li><span>Новостройки</span>
                                                <ul >
                                                    <li
                                                        id="bx_651765591_163">
                                                        <a href="/personal/mortgage/military-new/military-mortgage-new8079/">Новостройки</a> 		</li>
                                                    <li
                                                        id="bx_651765591_164">
                                                        <a href="/personal/mortgage/military-new/military-mortgage-new-family/">Кредит &quot;Семейный&quot; на новостройку</a> 		</li>
                                                    <li
                                                        id="bx_651765591_166">
                                                        <a href="/personal/mortgage/military-new/military-mortgage-sampo/">Квартиры в  ЖК SAMPO</a> 		</li>
                                                    <li
                                                        id="bx_651765591_167">
                                                        <a href="/personal/mortgage/military-new/military-mortgage-aijk/">Военная ипотека АИЖК</a> 		</li>
                                                </ul>							</li>
                                            <li><span>Вторичное жилье</span>
                                                <ul >
                                                    <li
                                                        id="bx_1373509569_74">
                                                        <a href="/personal/mortgage/military-secondary/military-mortgage-secondary/">На вторичном рынке</a> 		</li>
                                                    <li
                                                        id="bx_1373509569_165">
                                                        <a href="/personal/mortgage/military-secondary/military-mortgage-secondary-family/">Кредит &quot;Семейный&quot; на вторичном рынке</a> 		</li>
                                                </ul>							</li>
                                        </ul>
                                    </li>

                                    <li><span>Вторичное жилье</span>
                                        <ul >
                                            <li
                                                id="bx_3485106786_73">
                                                <a href="/personal/mortgage/the-apartment-on-the-secondary-market/the-apartment-on-the-secondary-market/">Ипотека на вторичное жильё</a> 		</li>
                                            <li
                                                id="bx_3485106786_132">
                                                <a href="/personal/mortgage/the-apartment-on-the-secondary-market/property-from-the-bank-s-balance-sheet/">Ипотека на объекты с баланса Банка</a> 		</li>
                                            <li
                                                id="bx_3485106786_135">
                                                <a href="/personal/mortgage/the-apartment-on-the-secondary-market/room-in-the-secondary-market/">Ипотека на комнату</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Дом с землей</span>
                                        <ul >
                                            <li
                                                id="bx_3099439860_134">
                                                <a href="/personal/mortgage/house-with-land/property-in-kp-primavera/">Недвижимость в КП «Примавера»</a> 		</li>
                                            <li
                                                id="bx_3099439860_84">
                                                <a href="/personal/mortgage/house-with-land/balance/">Недвижимость на балансе Банка</a> 		</li>
                                            <li
                                                id="bx_3099439860_75">
                                                <a href="/personal/mortgage/house-with-land/house-with-land/">Дома с земельными участками</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Специальные предложения</span>
                                        <ul class="burger_menu_menu">
                                            <li><a href="/personal/mortgage/stock/sampo_action/">Кредит на приобретение объектов недвижимости в Жилом комплексе «SAMPO»</a></li>
                                            <li><a href="/personal/mortgage/special-offers/">Акция «Ипотека по двум документам»</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/personal/mortgage/documents-and-supporting-information/">Документы и дополнительная информация</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Кредиты</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Потребительское кредитование</span>
                                        <ul >
                                            <li
                                                id="bx_565502798_4862">
                                                <a href="/personal/credits/personal-loans/na-lyubye-tseli-bez-zaloga-i-poruchiteley/">На любые цели без залога и поручительства</a> 		</li>
                                            <li
                                                id="bx_565502798_258">
                                                <a href="/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/">Под поручительство физлиц и залог автомобиля</a> 		</li>
                                            <li
                                                id="bx_565502798_260">
                                                <a href="/personal/credits/personal-loans/pod-zalog-nedvizhimosti/">Под залог недвижимости</a> 		</li>
                                            <li
                                                id="bx_565502798_361">
                                                <a href="/personal/credits/personal-loans/dlya-voennosluzhashchikh/">Для военнослужащих</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Рефинансирование</span>
                                        <ul class="burger_menu_menu">
                                            <li><a href="/personal/credits/personal-loans/refinancing/">Рефинансирование кредитов</a> </li>
                                        </ul>
                                    </li>

                                    <li><span> Автокредитование</span>
                                        <ul class="burger_menu_menu">
                                            <li><span>Новый автомобиль</span>
                                                <ul >
                                                    <li
                                                        id="bx_1454625752_162">
                                                        <a href="/personal/credits/carloans/new-car/">Новый автомобиль</a> 		</li>
                                                    <li
                                                        id="bx_1454625752_233">
                                                        <a href="/personal/credits/carloans/a-car-loan-with-a-delayed-payment/">Автокредит с остаточным платежом</a> 		</li>
                                                </ul>							</li>
                                            <li><span>Автомобиль с пробегом</span>
                                                <ul >
                                                    <li
                                                        id="bx_3322728009_227">
                                                        <a href="/personal/credits/carloans/car-old/">Автомобиль с пробегом</a> 		</li>
                                                </ul> </li>
                                        </ul>
                                    </li>

                                    <li><span> Специальные предложения</span>
                                        <ul class="burger_menu_menu">
                                            <li>
                                                <a href="/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/">Потребительский кредит по фиксированной ставке</a> 							</li>
                                            <!--li>
                                                <a href="/personal/credits/carloans/buying-a-new-car-without-a-down-payment_action/">Кредит на покупку нового автомобиля</a> 							</li-->
                                            <!--li>
                                                <a href="/personal/credits/carloans/buying-a-used-car-without-a-down-payment_action/">Кредит на покупку подержанного автомобиля</a> 							</li-->
                                            <li>
                                                <a href="/personal/credits/special-offers/">Программа льготного автокредитования</a> 							</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Карты</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Кредитные карты</span>
                                        <ul >
                                            <li
                                                id="bx_2970353375_4787">
                                                <a href="/personal/cards/credit-card/credit-card-with-cash-back/">Кредитная карта с кэшбэк</a> 		</li>
                                            <li
                                                id="bx_2970353375_4792">
                                                <a href="/personal/cards/credit-card/classic-credit-card-the-world/">Кредитная карта МИР</a> 		</li>
                                            <li
                                                id="bx_2970353375_4804">
                                                <a href="/personal/cards/credit-card/visa-platinum-credit-card/">Кредитная карта Visa Platinum</a> 		</li>
                                            <li
                                                id="bx_2970353375_4808">
                                                <a href="/personal/cards/credit-card/credit-card-mastercard-platinum/">Кредитная карта Mastercard Platinum</a> 		</li>
                                            <li
                                                id="bx_2970353375_4812">
                                                <a href="/personal/cards/credit-card/credit-card-unionpay-platinum/">Кредитная карта UnionPay Platinum</a> 		</li>
                                            <li
                                                id="bx_2970353375_4828">
                                                <a href="/personal/cards/credit-card/credit-card-world-of-travel/">Кредитная карта iGlobe</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Дебетовые карты</span>
                                        <ul >
                                            <li
                                                id="bx_719294866_4714">
                                                <a href="/personal/cards/debit-card/card-profitable-balance/">Дебетовая карта «Доходный остаток»</a> 		</li>
                                            <li
                                                id="bx_719294866_4715">
                                                <a href="/personal/cards/debit-card/card-with-cash-back/">Дебетовая карта с кэшбэк</a> 		</li>
                                            <li
                                                id="bx_719294866_4717">
                                                <a href="/personal/cards/debit-card/classical-map-the-world/">Дебетовая карта МИР</a> 		</li>
                                            <li
                                                id="bx_719294866_4722">
                                                <a href="/personal/cards/debit-card/card-unionpay-platinum/">Дебетовая карта UnionPay Platinum</a> 		</li>
                                            <li
                                                id="bx_719294866_4735">
                                                <a href="/personal/cards/debit-card/map-initiative/">Дебетовая карта «Инициатива»</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Предоплаченные карты</span>
                                        <ul >
                                        </ul>					</li>
                                </ul>
                            </div>
                        </div>

                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Вклады</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Максимальный доход</span>
                                        <ul >
                                            <li
                                                class="contribution"         id="bx_3302092990_246">
                                                <a href="/personal/deposits/high-profit/">Вклад «Высокий доход»</a> 		</li>
                                            <li
                                                id="bx_3302092990_4499">
                                                <a href="/personal/deposits/leadership-strategy/">Вклад «Стратегия лидерства»</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Накопление</span>
                                        <ul >
                                            <li
                                                id="bx_3017195560_4860">
                                                <a href="/personal/deposits/the-contribution-of-the-growing-income/">Вклад «Пополняемый доход»</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Максимальная гибкость</span>
                                        <ul >
                                            <li
                                                id="bx_766662027_248">
                                                <a href="/personal/deposits/great-opportunity/">Вклад «Большие возможности»</a> 		</li>
                                            <li
                                                id="bx_766662027_4861">
                                                <a href="/personal/deposits/the-account-savings-online/">Счёт «Накопительный Онлайн»</a> 		</li>
                                        </ul>					</li>

                                    <li><span>Акции</span>
                                        <ul >
                                            <li
                                                class="contribution"         id="bx_1521837341_4859">
                                                <a href="/personal/deposits/the-contribution-of-zenit-plyus/">Вклад «ЗЕНИТ Плюс»</a> 		</li>
                                        </ul>					</li>
                                </ul>
                            </div>
                        </div>

                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Платежи и переводы</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/personal/payments/payment">Оплата услуг</a></li>
                                    <li><span>Переводы</span>
                                        <ul>
                                            <li>
                                                <a href="/personal/payments/transfers/card-to-card/">Переводы с карты на карту</a> 	</li>
                                            <li>
                                                <a href="/personal/payments/transfers/perevody-v-sisteme-gorod/">Переводы через систему «Город»</a> 	</li>
                                            <li>
                                                <a href="/personal/payments/transfers/perevody-bez-otkrytiya-scheta/">Переводы без открытия счета</a> 	</li>
                                            <li>
                                                <a href="/personal/payments/transfers/sistema-zolotaya-korona/">Переводы через платежную систему «Золотая Корона»</a> 	</li>
                                            <li>
                                                <a href="/personal/payments/transfers/sistema-westrn-union/">Переводы через платежную систему Western Union</a> 	</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Страхование</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Страхование жизни и здоровья</span>
                                        <ul>
                                            <li>
                                                <a href="/personal/insurance/life/investment/">Инвестиционное страхование жизни</a> 	</li>
                                            <li>
                                                <a href="/personal/insurance/life/card/">Защита карты и здоровья</a> 	</li>
                                            <li>
                                                <a href="/personal/insurance/life/credit/">Финансовая защита заёмщиков</a> 	</li>
                                            <li>
                                                <a href="/personal/insurance/life/programma-iszh-kapital-v-plyus/">Программа ИСЖ «Капитал в плюс»</a> 	</li>
                                            <li>
                                                <a href="/personal/insurance/life/programma-iszh-upravlenie-kapitalom/">Программа ИСЖ «Управление капиталом+» </a> 	</li>
                                        </ul>
                                    </li>
                                    <li><span>Страхование имущества</span>
                                        <ul>
                                            <li>
                                                <a href="/personal/insurance/property/estate/">Страхование квартиры и домашнего имущества</a> 	</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Пенсионное обеспечение</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li>
                                        <a href="/personal/pension/personal/">Личные пенсионные программы</a> 	</li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Дистанционное банковское обслуживание</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li>
                                        <a href="/personal/remote-service/internet-bank/">Интернет-банк и мобильное приложение<br> «ЗЕНИТ Онлайн»</a> 				</ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Индивидуальные сейфы</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li>
                                        <a href="/personal/safes/secure/">Ответственное хранение</a> 	</li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Инвестиционные услуги</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li>
                                        <a href="http://invest.zenit.ru/" target="_blank">ОФБУ (Общие фонды банковского управления)</a>
                                    </li>
                                    <li>
                                        <a href="/personal/invest/individual-trust-management.php">Индивидуальное доверительное управление</a> 					</li>
                                    <li>
                                        <a href="/personal/invest/individual-investment-accounts.php">Индивидуальные инвестиционные счета</a> 					</li>
                                    <li><span>Брокерское обслуживание</span>
                                        <ul>
                                            <li>
                                                <a href="/personal/brokerage-service/about-the-service/">Об услуге</a>
                                            </li>
                                            <li>
                                                <a href="/personal/brokerage-service/one-account/">Единый торговый счет</a>
                                            </li>
                                            <li>
                                                <a href="/personal/brokerage-service/repo-transactions/">Сделки РЕПО с ценными бумагами</a>
                                            </li>
                                            <li>
                                                <a href="/personal/brokerage-service/margin-trading/">Маржинальная торговля</a>
                                            </li>
                                            <li>
                                                <a href="/personal/brokerage-service/trading-systems/">Торговые системы</a>
                                            </li>
                                            <li>
                                                <a href="/personal/brokerage-service/rates/">Тарифы</a>
                                            </li>
                                            <li>
                                                <a href="/personal/brokerage-service/documents/">Документы</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><span>Депозитарные операции</span>
                                        <ul>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-information/">Информация о депозитарии</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-services/">Депозитарные услуги</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-rates/">Тарифы депозитария</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-documents/">Документы депозитария</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-securities/">Обслуживаемые ценные бумаги</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-operations/">Проведение операций</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/depositary-edoc/">Электронный документооборот</a>
                                            </li>
                                            <li>
                                                <a href="/personal/invest/depositary-operations/registrars-and-depositaries/">Перечень регистраторов и депозитариев</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Тарифы</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li>
                                        <a href="/personal/tariffs/">Тарифы для частных лиц</a> 					</li>
                                </ul>
                            </div>
                        </div>
                        <div class="burger_menu_item">

                        </div>


                    </div>
                </div><!--wr_burger_menu-->
                <!--wr_mcb_menu-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="mcb_menu">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Расчетно-кассовое обслуживание</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Открытие и ведение счета</span>
                                        <ul>
                                            <li><a href="/businesses/rko/account/online-reserve">Онлайн-резервирование номера счета</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Тарифные планы</span>
                                        <ul>
                                            <li><a href="/businesses/rko/tariff-plans/main-tariff-plans/">Основные тарифные планы</a></li>
                                            <li><a href="/businesses/rko/tariff-plans/tariff-plan-importer/">ТП Импортер</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Безналичные переводы</span>
                                        <ul>
                                            <li><a href="/businesses/rko/transfers/rub">Переводы в рублях</a></li>
                                            <li><a href="/businesses/rko/transfers/currency">Переводы в иностранной валюте</a></li>
                                            <li><a href="/businesses/rko/transfers/time">Операционное время</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Дистанционное обслуживание</span>
                                        <ul>
                                            <li><a href="/businesses/rko/remote/clientbank">Система Клиент-Банк</a></li>
                                            <li><a href="/businesses/rko/remote/1c">Клиент-Банк из 1С: Предприятие</a></li>
                                            <li><a href="/businesses/rko/remote/mobail">Мобильный банк</a></li>
                                            <li><a href="/businesses/rko/remote/sms">SMS - информирование</a></li>
                                            <li><a href="/businesses/rko/remote/indicator">Индикатор</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Банковские карты</span>
                                        <ul>
                                            <li><a href="/businesses/rko/bank-card/customs-card">Таможенная карта</a></li>
                                            <li><a href="/businesses/rko/bank-card/salary-project">Зарплатный проект</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Эквайринг</span>
                                        <ul>
                                            <li><a href="/businesses/rko/acquiring/trade-aquaring/">Торговый экваринг</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/businesses/rko/collection-and-delivery-of-cash/">Инкассация и доставка наличных</a></li>
                                    <li><span>Валютный контроль</span>
                                        <ul>
                                            <li><a href="/businesses/rko/currency-control/description-and-documents">Описание и документы</a></li>
                                            <li><a href="/businesses/rko/currency-control/violation-of-currency-legislation">Нарушения валютного законодательства</a></li>
                                        </ul>
                                    </li>

                                    <li><span>Дополнительные продукты</span>
                                        <ul>
                                            <li><a href="/businesses/rko/additional/check">Прием чеков на инкассо</a></li>
                                            <li><a href="/businesses/rko/additional/cells">Сейфовые ячейки</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Специальные предложения и акции</span>
                                        <ul>
                                            <li><a href="/businesses/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks">Льготные тарифы для клиентов проблемных банков</a></li>
                                            <li><a href="/businesses/rko/special-offers-and-promotions/promotions">Акции</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Тарифы</span>
                                        <ul>
                                            <li><a href="/businesses/rko/rates/collection-of-tariffs">Сборник тарифов</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="burger_menu_title">Кредиты и гарантии</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Кредиты</span>
                                        <ul>
                                            <li><a href="/businesses/loans/working-capital-loan">Оборотное кредитование</a></li>
                                            <li><a href="/businesses/loans/overdraft">Овердрафт</a></li>
                                            <li><a href="/businesses/loans/tender-loan">Тендерный кредит</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Инвестиционное кредитование</span>
                                        <ul>
                                            <li><a href="/businesses/loans/investment-lending/real-estate">Недвижимость</a></li>
                                            <li><a href="/businesses/loans/investment-lending/equipment-and-transport">Оборудование и транспорт</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Государственная поддержка субъектов МСБ</span>
                                        <ul>
                                            <li><a href="/businesses/loans/state-support-of-smes/program-6-5-jsc-corporation-smes">Программа 6,5 АО "Корпорации "МСП"</a></li>
                                            <li><a href="/businesses/loans/state-support-of-smes/warranty-support-of-jsc-corporation-smes-jsc-sme-bank">Гарантийная поддержка АО "Корпорация "МСП/АО "МСП Банк"</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="burger_menu_title">Гарантии</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/warranty/electronic-guarantee-for-the-participants-of-public-procurement">Электронная гарантия для участников госзакупок</a></li>
                                    <li><a href="/businesses/warranty/commercial-warranty">Коммерческие гарантии</a></li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="burger_menu_title">Размещение средств</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/allocation-of-funds/how-to-place-funds-in-the-bank/five-steps-to-income">Как разместить средства в Банке?</a></li>
                                    <li><span>Депозитные продукты</span>
                                        <ul>
                                            <li><a href="/businesses/allocation-of-funds/deposit-products/term-deposits">Срочные депозиты</a></li>
                                            <li><a href="/businesses/allocation-of-funds/deposit-products/dual-currency-deposit">Бивалютный депозит</a></li>
                                            <li><a href="/businesses/allocation-of-funds/deposit-products/minimum-balance-on-the-account">Неснижаемый остаток на расчетном счете</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Ценные бумаги</span>
                                        <ul>

                                            <li><a href="/businesses/allocation-of-funds/securities/certificates-of-deposit/">Депозитные сертификаты</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="burger_menu_title">Международные расчеты</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/businesses/international-payments/conversion-operations">Конверсионные операции</a></li>
                                    <li><a href="/businesses/international-payments/documentary-operations">Документарные операции</a></li>
                                </ul>
                            </div>

                        </div><div class="burger_menu_item">


                        </div>
                    </div>
                </div><!--wr_mcb_menu-->
                <!--wr_corp_menu-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="corp_menu">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Финансирование</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/big-business/funding/overdraft/">Овердрафт</a></li>
                                    <li><a href="/big-business/funding/loans-for-working-capital/">Кредитование на пополнение оборотных средств</a></li>
                                    <li><a href="/big-business/funding/investment-lending/">Инвестиционное кредитование</a></li>
                                    <li><a href="/big-business/funding/project-financing/">Проектное финансирование</a></li>
                                    <li><a href="/big-business/funding/trade-finance/">Торговое финансирование</a></li>
                                    <li><a href="/big-business/funding/foreign-trade-investment/">Финансирование  по программам Банка ЗЕНИТ</a></li>
                                    <li><a href="/big-business/funding/exiar-coverage/">Финасирование под страховое покрытие АО ЭКСАР</a></li>
                                    <li><a href="/big-business/funding/foreign-trade-investment/">Финансирование импорта под страховое покрытие ЭКА</a></li>
                                    <li><a href="/big-business/funding/organization-of-debt-financing/promissory-notes-program/">Выпуск вексельной программы</a></li>
                                    <li><a href="/big-business/funding/organization-of-debt-financing/bonded-loan-issue/">Выпуск облигационного займа</a></li>
                                    <li><a href="/big-business/funding/organization-of-debt-financing/syndicated-lending/">Синдицированное кредитование</a></li>
                                    <li><a href="/big-business/funding/organization-of-debt-financing/issue-of-eurobonds/">Выпуск еврооблигаций и кредитных нот (CLN, LPN)</a></li>
                                    <li><a href="/big-business/funding/organization-of-debt-financing/debt-and-distressed-assets/">Услуги по реструктуризации долга и по работе с проблемными активами</a></li>
                                    <li><a href="/big-business/funding/organization-of-debt-financing/support-issuers/">Поддержка эмитентов при прохождении оферт, погашения долга.</a></li>
                                    <li><a href="/big-business/funding/factoring/">Факторинг</a></li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="burger_menu_title">Расчетно-кассовое обслуживание</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Тарифные планы</span>
                                        <ul>
                                            <li><a href="/big-business/rko/tariff-plans/main-tariff-plans/">Основные тарифные планы</a></li>
                                            <li><a href="/big-business/rko/tariff-plans/tariff-plan-importer/">ТП Импортер</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Безналичные переводы</span>
                                        <ul>
                                            <li><a href="/big-business/rko/transfers/rub">Переводы в рублях</a></li>
                                            <li><a href="/big-business/rko/transfers/currency">Переводы в иностранной валюте</a></li>
                                            <li><a href="/big-business/rko/transfers/time">Операционное время</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Банковские карты</span>
                                        <ul>
                                            <li><a href="/big-business/rko/bank-card/customs-card">Таможенная карта</a></li>
                                            <li><a href="/big-business/rko/bank-card/salary-project">Зарплатный проект</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Эквайринг</span>
                                        <ul>
                                            <li><a href="/big-business/rko/acquiring/trade-aquaring/">Торговый экваринг</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/big-business/rko/conversion-operations/">Конверсионные операции</a></li>
                                    <li><span>Специальные предложения и акции</span>
                                        <ul>
                                            <li><a href="/big-business/rko/special-offers-and-promotions/preferential-rates-for-clients-of-troubled-banks">Льготные тарифы для клиентов проблемных банков</a></li>
                                            <li><a href="/big-business/rko/special-offers-and-promotions/promotions">Акции</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Дистанционное обслуживание</span>
                                        <ul>
                                            <li><a href="/big-business/rko/remote/clientbank">Клиент-Банк</a></li>
                                            <li><a href="/big-business/rko/remote/1c">iBank2 для 1С:Предприятие</a></li>
                                            <li><a href="/big-business/rko/remote/sms">SMS - информирование</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div><div class="burger_menu_item">
                            <div class="burger_menu_title">Размещение денежных средств</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><span>Срочное размещение</span>
                                        <ul>
                                            <li><a href="/big-business/allocation-of-funds/deposit-products/term-deposits">Депозиты</a></li>
                                            <li><a href="/big-business/allocation-of-funds/deposit-products/dual-currency-deposit">Бивалютный депозит</a></li>
                                            <li><a href="/big-business/allocation-of-funds/deposit-products/minimum-balance-on-the-account">Неснижаемый остаток на расчетном счете</a></li>
                                        </ul>
                                    </li>
                                    <li><span>Ценные бумаги</span>
                                        <ul>

                                            <li><a href="/big-business/allocation-of-funds/securities/certificates-of-deposit/">Депозитные сертификаты</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/big-business/documentary-operations/">Документарные операции</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!--wr_corp_menu-->
                <!--wr_fin_menu-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="fin_menu">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_title">Конверсионный и межбанковский рынки</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/corp-and-fin/conversion-and-interbank-markets/operations-with-derivative-financial-instruments/">Операции с ПФИ</a></li>
                                    <li><a href="/corp-and-fin/conversion-and-interbank-markets/conversion-operations/">Конверсионные операции</a></li>
                                </ul>
                            </div>
                            <a href="/corp-and-fin/correspondent-relations/" class="menu_bottom_block_title">Корреспондентские отношения</a>

                        </div><div class="burger_menu_item">
                            <div class="burger_menu_title">Операции на рынке ценных бумаг</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/corp-and-fin/trading-operations/">Торговые операции</a></li>
                                    <li><a href="/corp-and-fin/operations-on-the-securities-market/repo-transactions-with-securities/">Операции РЕПО с ценными бумагами</a></li>
                                    <li><a href="/corp-and-fin/operations-on-the-securities-market/custody-services/">Депозитарное обслуживание</a></li>
                                    <li><a href="/corp-and-fin/operations-on-the-securities-market/brokerage-service/">Брокерское обслуживание</a></li>
                                </ul>
                            </div>

                            <div class="burger_menu_title">Организация проектов по привлечению долгового финансирования</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/corp-and-fin/organization-of-debt-financing/bonded-loan-issue/">Выпуск облигационного займа</a></li>
                                    <li><a href="/corp-and-fin/organization-of-debt-financing/syndicated-lending/">Синдицированное кредитование</a></li>
                                    <li><a href="/corp-and-fin/organization-of-debt-financing/support-issuers/">Поддержка эмитентов при прохождении оферт, погашения долга.</a></li>
                                    <li><a href="/corp-and-fin/organization-of-debt-financing/promissory-notes-program/">Выпуск вексельной программы</a></li>
                                </ul>
                            </div>

                        </div><div class="burger_menu_item">
                            <div class="burger_menu_title">Другие продукты</div>
                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/corp-and-fin/program-for-bank-cards-for-partner-banks/">Программы по банковским картам для банков-партнеров</a></li>
                                    <li><a href="/corp-and-fin/details-of-pjsc-bank-zenit/">Реквизиты ПАО Банк ЗЕНИТ</a></li>


                                </ul>
                            </div>
                        </div><div class="burger_menu_item">


                        </div>
                    </div>
                </div><!--wr_fin_menu-->
                <!--wr_private_banking-->
                <div class="wr_menu_bottom wr_burger_menu" style="display: none" id="private_banking_menu">
                    <div class="burger_menu z-container">
                        <div class="burger_menu_item">
                            <div class="burger_menu_block">
                                <ul  class="burger_menu_menu">
                                    <li
                                        id="bx_3283891367_1458">
                                        <a href="/private-banking/investment-insurance-tools/produkt-investitsionnoe-strakhovanie-zhizni/">Инвестиционное страхование жизни</a> 		</li>
                                    <li
                                        id="bx_3283891367_1457">
                                        <a href="/private-banking/investment-insurance-tools/produkt-nakopitelnoe-strakhovanie-zhizni/">Накопительное страхование жизни</a> 		</li>
                                    <li
                                        id="bx_3283891367_1456">
                                        <a href="/private-banking/investment-insurance-tools/produkt-fiksirovannyy-dokhod/">Фиксированный доход</a> 		</li>
                                </ul>            </div>
                        </div><div class="burger_menu_item">

                            <div class="burger_menu_block">
                                <ul  class="burger_menu_menu">
                                    <li
                                        id="bx_3032155185_1464">
                                        <a href="/private-banking/savings-and-investments/contribution/srochnyy-v-kitayskikh-yuanyakh/">Срочный в китайских юанях</a> 		</li>
                                    <li
                                        id="bx_3032155185_1463">
                                        <a href="/private-banking/savings-and-investments/contribution/investitsionnyy/">Инвестиционный</a> 		</li>
                                    <li
                                        id="bx_3032155185_589">
                                        <a href="/private-banking/savings-and-investments/contribution/nakopitelnyy-premium/">Накопительный премиум</a> 		</li>
                                    <li
                                        id="bx_3032155185_588">
                                        <a href="/private-banking/savings-and-investments/contribution/srochnyy-premium/">Срочный премиум</a> 		</li>
                                </ul>            </div>



                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/private-banking/savings-and-investments/investment-management/">Управление инвестициями</a></li>
                                    <li><a href="/private-banking/savings-and-investments/investment-management/brokerage-service/">Брокерское обслуживание</a></li>
                                    <li><a href="/private-banking/savings-and-investments/investment-management/collective-trust-management/">Коллективное доверительное управление</a></li>
                                    <li><a href="/private-banking/savings-and-investments/investment-management/depositary-services/">Услуги депозитария</a></li>
                                </ul>
                            </div>

                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/private-banking/savings-and-investments/investment-management/individual-trust-management/">Индивидуальное доверительное управление активами Клиента</a></li>
                                    <li><a href="/private-banking/savings-and-investments/investment-management/investment-portfolio/">Структурирование инвестиционного портфеля Клиента</a></li>
                                    <li><a href="/private-banking/savings-and-investments/investment-management/private-consultant-advisory/">Частный консультант Advisory</a></li>
                                </ul>
                            </div>

                        </div><div class="burger_menu_item">


                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/private-banking/traditional-banking-products/cash-management-services/">Расчетно-кассовое обслуживание</a></li>
                                    <li><a href="/private-banking/traditional-banking-products/documentary-and-international-operations/">Документарные и международные операции</a></li>
                                    <li><a href="/private-banking/traditional-banking-products/elite-credit-card/">Элитные банковские карты</a></li>
                                </ul>
                            </div>

                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/private-banking/traditional-banking-products/individual-credit-program/">Индивидуальные кредитные программы</a></li>
                                    <li><a href="/private-banking/traditional-banking-products/operations-of-precious-metals/">Операции на рынке драгоценных металлов и обезличенные металлические счета</a></li>
                                </ul>
                            </div>

                            <div class="burger_menu_block">
                                <ul class="burger_menu_menu">
                                    <li><a href="/private-banking/traditional-banking-products/safe-deposit-boxes/">Сейфовые ячейки</a></li>
                                    <li><a href="/private-banking/traditional-banking-products/the-collection-of-funds/">Инкассирование денежных средств</a></li>
                                </ul>
                            </div>


                        </div>
                    </div>
                </div><!--wr_private_banking-->                <!--menu_net_bank-->
                <div class="wr_menu_bottom" id="menu_net_bank" style="display: none;">
                    <div class="menu_bottom z-container">
                        <div class="menu_bottom_block">
                            <a href="https://my.zenit.ru/wb/" class="menu_bottom_snippet">
                                <div class="menu_bottom_snippet_pict simpl"></div>
                                <div class="menu_bottom_snippet_text">
                                    <b>ЗЕНИТ Онлайн</b><br>
                                    для личных финансов
                                </div>
                                <div class="button transparent min_height">Войти</div>
                                <div class="bottom_blank">
                                    Мобильный банк
                                    <ul class="mobile_banking_line_icon">
                                        <li><div class="mobile_banking_line_icon apple"><div></div></div></li>
                                        <li><div class="mobile_banking_line_icon android"><div></div></div></li>
                                    </ul>
                                </div>
                            </a>
                        </div><div class="menu_bottom_block">
                            <a href="https://old.zenit.ru/cb/" class="menu_bottom_snippet">
                                <div class="menu_bottom_snippet_pict bisness"></div>
                                <div class="menu_bottom_snippet_text">
                                    <b>ЗЕНИТ Бизнес</b><br>
                                    для компаний
                                </div>
                                <div class="button transparent min_height">Войти</div>
                                <div class="bottom_blank">
                                    Мобильный банк-клиент
                                    <ul class="mobile_banking_line_icon">
                                        <li><div class="mobile_banking_line_icon apple"><div></div></div></li>
                                        <li><div class="mobile_banking_line_icon android"><div></div></div></li>
                                    </ul>
                                </div>
                            </a>
                        </div><div class="menu_bottom_block">
                            <div class="menu_bottom_block_paggin">
                            </div>
                        </div>
                    </div>
                </div>                <!--menu_net_bank-->
            </div>
            <!--header-->
        </div>
    </div>








    <div class="page-promo-banner">
        <img class="banner-background" src="/local/templates/bz/img/online-reserve/fon.png" alt="">
        <div class="blur-wrap">
            <div class="img-container">
                <img class="blur-image" src="/local/templates/bz/img/online-reserve/fon.png" alt="">
            </div>
            <div class="blur-content">
                <div class="c-container">
                    <div class="title">
                        Онлайн-резервирование номера счета                        </div>
                    <div class="text">
                        <p>Получите номер счета прямо сейчас</p>                        </div>
                </div>
            </div>
        </div>
    </div>








    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <h1>Как проходит  резервирование</h1>
            <div class="block_type_right">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">
                            <div class="right_top_line">
                                <h2>Контакты</h2>
                                <div class="form_application_line" data-showid="11" >
                                    <div class="contacts_block">
                                        +7 (495) 777-57-04                                </div>
                                </div>
                                <div class="form_application_line" data-showid="763" style="display:none;">
                                    <div class="right_bank_title">
                                        Асылгараева Альбина                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.gainetdinova@zenit.ru">a.gainetdinova@zenit.ru</a><br>                                    +7 (8553) 37-75-90, 37-75-95, доб. 422                                </div>
                                </div>
                                <div class="form_application_line" data-showid="763" style="display:none;">
                                    <div class="right_bank_title">
                                        Савдиярова Наталья                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:+7 (8553) 37-75-90, 37-75-95, доб. 444">+7 (8553) 37-75-90, 37-75-95, доб. 444</a><br>                                    n.savdiyarova@zenit.ru                                </div>
                                </div>
                                <div class="form_application_line" data-showid="764" style="display:none;">
                                    <div class="right_bank_title">
                                        Малышева Инна                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:i.malysheva@zenit.ru">i.malysheva@zenit.ru</a><br>                                    +7 (83147) 3-38-38, доб. 351                                </div>
                                </div>
                                <div class="form_application_line" data-showid="764" style="display:none;">
                                    <div class="right_bank_title">
                                        Пылкова Марина                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:m.pylkova@zenit.ru">m.pylkova@zenit.ru</a><br>                                    +7 (83147) 3-38-38, доб. 353                                </div>
                                </div>
                                <div class="form_application_line" data-showid="766" style="display:none;">
                                    <div class="right_bank_title">
                                        Анищенко Ольга                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:o.anishchenko@zenit.ru">o.anishchenko@zenit.ru</a><br>                                    +7 (473) 276-58-68, доб. 334                                </div>
                                </div>
                                <div class="form_application_line" data-showid="766" style="display:none;">
                                    <div class="right_bank_title">
                                        Кузякин Денис                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:d.kuzyakin@zenit.ru">d.kuzyakin@zenit.ru</a><br>                                    +7 (473) 276-58-68, доб. 338                                </div>
                                </div>
                                <div class="form_application_line" data-showid="766" style="display:none;">
                                    <div class="right_bank_title">
                                        Сидоров Вадим                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:vv.sidorov@zenit.ru">vv.sidorov@zenit.ru</a><br>                                    +7 (473) 276-58-68, доб. 333                                </div>
                                </div>
                                <div class="form_application_line" data-showid="768" style="display:none;">
                                    <div class="right_bank_title">
                                        Волкова Анна                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.volkova@zenit.ru">a.volkova@zenit.ru</a><br>                                    +7 (38822) 2-50-18, доб. 201                                </div>
                                </div>
                                <div class="form_application_line" data-showid="768" style="display:none;">
                                    <div class="right_bank_title">
                                        Климов Марк                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:m.klimov@zenit.ru">m.klimov@zenit.ru</a><br>                                    +7 (38822) 2-50-18, доб. 222                                </div>
                                </div>
                                <div class="form_application_line" data-showid="772" style="display:none;">
                                    <div class="right_bank_title">
                                        Вахтель Елена                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:e.vahtel@zenit.ru">e.vahtel@zenit.ru</a><br>                                    +7 (343)310-32-70, доб. 134                                </div>
                                </div>
                                <div class="form_application_line" data-showid="772" style="display:none;">
                                    <div class="right_bank_title">
                                        Колесникова Наталья                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:na.kolesnikova@zenit.ru">na.kolesnikova@zenit.ru</a><br>                                    +7 (343)310-32-70, доб. 135                                </div>
                                </div>
                                <div class="form_application_line" data-showid="775" style="display:none;">
                                    <div class="right_bank_title">
                                        Булдакова Татьяна                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:ta.buldakova@zenit.ru">ta.buldakova@zenit.ru</a><br>                                    +7 (3412)799-755, доб. 312                                </div>
                                </div>
                                <div class="form_application_line" data-showid="775" style="display:none;">
                                    <div class="right_bank_title">
                                        Кириченко Степан                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:s.kirichenko@zenit.ru">s.kirichenko@zenit.ru</a><br>                                    +7 (3412)799-755, доб. 301                                </div>
                                </div>
                                <div class="form_application_line" data-showid="8" style="display:none;">
                                    <div class="right_bank_title">
                                        Анисимов Семён                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:s.anisimov@zenit.ru">s.anisimov@zenit.ru</a><br>                                    +7 (4012) 92-00-01, доб. 342                                </div>
                                </div>
                                <div class="form_application_line" data-showid="8" style="display:none;">
                                    <div class="right_bank_title">
                                        Лисина Марина                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:m.lisina@zenit.ru">m.lisina@zenit.ru</a><br>                                    +7 (4012) 92-00-01, доб. 306                                </div>
                                </div>
                                <div class="form_application_line" data-showid="9" style="display:none;">
                                    <div class="right_bank_title">
                                        Любушкин Станислав                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:s.lubushkin@zenit.ru">s.lubushkin@zenit.ru</a><br>                                    +7 (3842)58-12-71, доб. 127                                </div>
                                </div>
                                <div class="form_application_line" data-showid="9" style="display:none;">
                                    <div class="right_bank_title">
                                        Разумов Владислав                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:v.razumov@zenit.ru">v.razumov@zenit.ru</a><br>                                    +7 (3842)58-12-71, доб. 103                                </div>
                                </div>
                                <div class="form_application_line" data-showid="10" style="display:none;">
                                    <div class="right_bank_title">
                                        Жикулин Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.zhikulin@zenit.ru">a.zhikulin@zenit.ru</a><br>                                    +7 (4712) 51-26-56, доб. 121                                </div>
                                </div>
                                <div class="form_application_line" data-showid="10" style="display:none;">
                                    <div class="right_bank_title">
                                        Майданов Олег                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:o.maydanov@zenit.ru">o.maydanov@zenit.ru</a><br>                                    +7 (4712) 51-26-56, доб. 136                                </div>
                                </div>
                                <div class="form_application_line" data-showid="13" style="display:none;">
                                    <div class="right_bank_title">
                                        Крамков Владимир                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:v.kramkov@zenit.ru">v.kramkov@zenit.ru</a><br>                                    +7 (8312) 278-97-77, доб.221                                </div>
                                </div>
                                <div class="form_application_line" data-showid="13" style="display:none;">
                                    <div class="right_bank_title">
                                        Матющенко Юлия                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:y.matyushchenko@zenit.ru">y.matyushchenko@zenit.ru</a><br>                                    +7 (8312) 278-97-77, доб. 127                                </div>
                                </div>
                                <div class="form_application_line" data-showid="13" style="display:none;">
                                    <div class="right_bank_title">
                                        Хлебин Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.hlebin@zenit.ru">a.hlebin@zenit.ru</a><br>                                    +7 (8312) 278-97-77, доб. 222                                </div>
                                </div>
                                <div class="form_application_line" data-showid="14" style="display:none;">
                                    <div class="right_bank_title">
                                        Волков Николай                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:n.volkov@zenit.ru">n.volkov@zenit.ru</a><br>                                    +7 (3843)46-60-11, доб. 103                                </div>
                                </div>
                                <div class="form_application_line" data-showid="14" style="display:none;">
                                    <div class="right_bank_title">
                                        Чупахин Вадим                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:v.chupahin@zenit.ru">v.chupahin@zenit.ru</a><br>                                    +7 (3843)46-60-11, доб. 122                                </div>
                                </div>
                                <div class="form_application_line" data-showid="15" style="display:none;">
                                    <div class="right_bank_title">
                                        Дегтярева Саида                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:s.degtyareva@zenit.ru">s.degtyareva@zenit.ru</a><br>                                    +7 (383) 298-94-37, доб. 106                                </div>
                                </div>
                                <div class="form_application_line" data-showid="15" style="display:none;">
                                    <div class="right_bank_title">
                                        Харчёв Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.kharchev@zenit.ru">a.kharchev@zenit.ru</a><br>                                    +7 (383) 298-94-30, доб. 171                                </div>
                                </div>
                                <div class="form_application_line" data-showid="16" style="display:none;">
                                    <div class="right_bank_title">
                                        Кабанцова Оксана                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:o.kabancova@zenit.ru">o.kabancova@zenit.ru</a><br>                                    +7 (3812)35-64-41, доб. 315                                </div>
                                </div>
                                <div class="form_application_line" data-showid="16" style="display:none;">
                                    <div class="right_bank_title">
                                        Юрин Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.yurin@zenit.ru">a.yurin@zenit.ru</a><br>                                    +7 (3812) 21-04-17 доб. 320                                </div>
                                </div>
                                <div class="form_application_line" data-showid="17" style="display:none;">
                                    <div class="right_bank_title">
                                        Бушмакина Ирина                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:i.bushmakina@zenit.ru">i.bushmakina@zenit.ru</a><br>                                    +7 (342) 211-12-20, доб. 218                                </div>
                                </div>
                                <div class="form_application_line" data-showid="17" style="display:none;">
                                    <div class="right_bank_title">
                                        Пятков Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.pyatkov@zenit.ru">a.pyatkov@zenit.ru</a><br>                                    +7 (342) 211-12-19, доб. 236                                </div>
                                </div>
                                <div class="form_application_line" data-showid="18" style="display:none;">
                                    <div class="right_bank_title">
                                        Козинцев Артём                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:A.Kozincev@zenit.ru">A.Kozincev@zenit.ru</a><br>                                    +7 (863) 201-80-23, доб. 141                                </div>
                                </div>
                                <div class="form_application_line" data-showid="18" style="display:none;">
                                    <div class="right_bank_title">
                                        Мартыненко Галина                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:G.Martinenko@zenit.ru">G.Martinenko@zenit.ru</a><br>                                    +7 (863) 201-78-07, доб. 121                                </div>
                                </div>
                                <div class="form_application_line" data-showid="19" style="display:none;">
                                    <div class="right_bank_title">
                                        Балашов Михаил                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:m.balashov@zenit.ru">m.balashov@zenit.ru</a><br>                                    +7 (846) 310 28 60, доб. 295                                </div>
                                </div>
                                <div class="form_application_line" data-showid="19" style="display:none;">
                                    <div class="right_bank_title">
                                        Плешаков Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.pleshakov@zenit.ru">a.pleshakov@zenit.ru</a><br>                                    +7 (846) 310 28 55, доб. 115                                </div>
                                </div>
                                <div class="form_application_line" data-showid="20" style="display:none;">
                                    <div class="right_bank_title">
                                        Владимир Павленко                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:V.Pavlenko@zenit.ru">V.Pavlenko@zenit.ru</a><br>                                    +7 (812) 324-69-24 доб. 214                                </div>
                                </div>
                                <div class="form_application_line" data-showid="20" style="display:none;">
                                    <div class="right_bank_title">
                                        Ситюков Александр                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:A.Situkov@zenit.ru">A.Situkov@zenit.ru</a><br>                                    +7 (812) 324-69-24 доб. 224                                </div>
                                </div>
                                <div class="form_application_line" data-showid="21" style="display:none;">
                                    <div class="right_bank_title">
                                        Андреев Константин                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:k.andreev@zenit.ru">k.andreev@zenit.ru</a><br>                                    +7 (8452) 66 92 11, доб. 241                                </div>
                                </div>
                                <div class="form_application_line" data-showid="21" style="display:none;">
                                    <div class="right_bank_title">
                                        Доронина Елена                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:e.doronina@zenit.ru">e.doronina@zenit.ru</a><br>                                    +7 (8452) 66 92 11, доб. 247                                </div>
                                </div>
                                <div class="form_application_line" data-showid="22" style="display:none;">
                                    <div class="right_bank_title">
                                        Гулыга Анастасия                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.gulyga@zenit.ru">a.gulyga@zenit.ru</a><br>                                    +7 (83130) 7-98-58, доб. 362                                </div>
                                </div>
                                <div class="form_application_line" data-showid="22" style="display:none;">
                                    <div class="right_bank_title">
                                        Усов Дмитрий                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:d.usov@zenit.ru">d.usov@zenit.ru</a><br>                                    +7 (83130) 7-98-58, доб. 361                                </div>
                                </div>
                                <div class="form_application_line" data-showid="23" style="display:none;">
                                    <div class="right_bank_title">
                                        Петров Дмитрий                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:d.petrov@zenit.ru">d.petrov@zenit.ru</a><br>                                    +7 (8634) 611-996, 611-157, доб. 312                                </div>
                                </div>
                                <div class="form_application_line" data-showid="23" style="display:none;">
                                    <div class="right_bank_title">
                                        Юникова Юлия                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:y.yunikova@zenit.ru">y.yunikova@zenit.ru</a><br>                                    +7 (8634) 611-996, 611-460, доб. 314                                </div>
                                </div>
                                <div class="form_application_line" data-showid="24" style="display:none;">
                                    <div class="right_bank_title">
                                        Заволковский Михаил                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:m.zavolkovskiy@zenit.ru">m.zavolkovskiy@zenit.ru</a><br>                                    +7 (8452) 31 99 50, доб. 222                                </div>
                                </div>
                                <div class="form_application_line" data-showid="25" style="display:none;">
                                    <div class="right_bank_title">
                                        Баженов Денис                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:d.bazhenov@zenit.ru">d.bazhenov@zenit.ru</a><br>                                    +7 247-91-65, доб. 209                                </div>
                                </div>
                                <div class="form_application_line" data-showid="25" style="display:none;">
                                    <div class="right_bank_title">
                                        Палаус Павел                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:p.palaus@zenit.ru">p.palaus@zenit.ru</a><br>                                    +7 247-91-65, доб. 132                                </div>
                                </div>
                                <div class="form_application_line" data-showid="25" style="display:none;">
                                    <div class="right_bank_title">
                                        Шевякова Анастасия                                </div>
                                    <div class="contacts_block">
                                        <a href="mailto:a.shevyakova@zenit.ru">a.shevyakova@zenit.ru</a><br>                                    +7 247-91-65, доб. 210                                </div>
                                </div>
                                <div class="right_bank_title">Телефон прямой линии</div>
                                <div class="form_application_line">
                                    <div class="contacts_block">
                                        8 (800) 500-66-77                    </div>
                                    <div class="note_text">
                                        звонок по России бесплатный
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block_type_center">
                            <!--h2>Ваш город</h2>
                            <div class="form_application_line">
                                <select data-programm data-plasholder="Ваш город" data-title="Ваш город" class="formstyle" name="code_sity">
                                                        <option value="11" selected>Москва и МО</option>
                                                        <option value="763" >Альметьевск</option>
                                                        <option value="764" >Арзамас</option>
                                                        <option value="766" >Воронеж</option>
                                                        <option value="768" >Горно-Алтайск</option>
                                                        <option value="772" >Екатеринбург</option>
                                                        <option value="775" >Ижевск</option>
                                                        <option value="776" >Казань</option>
                                                        <option value="8" >Калининград</option>
                                                        <option value="9" >Кемерово</option>
                                                        <option value="10" >Курск</option>
                                                        <option value="12" >Нижнекамск</option>
                                                        <option value="13" >Нижний Новгород</option>
                                                        <option value="3430" >Новая Адыгея</option>
                                                        <option value="14" >Новокузнецк</option>
                                                        <option value="15" >Новосибирск</option>
                                                        <option value="16" >Омск</option>
                                                        <option value="17" >Пермь</option>
                                                        <option value="18" >Ростов-на-Дону</option>
                                                        <option value="19" >Самара</option>
                                                        <option value="20" >Санкт-Петербург</option>
                                                        <option value="21" >Саратов</option>
                                                        <option value="22" >Саров</option>
                                                        <option value="23" >Таганрог</option>
                                                        <option value="24" >Тольятти</option>
                                                        <option value="25" >Челябинск</option>
                                                    </select>
                            </div-->


                            <h2>Тарифы</h2>
                            <!--doc-->
                            <div class="doc_list">
                                <a href="/upload/iblock/365/tariffs_corp_moscow_20171201.pdf" class="doc_item" target="_blank" data-showid="11" >
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            506.93 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/de2/tariffs_corp_tatarstan_20180101.pdf" class="doc_item" target="_blank" data-showid="763" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            479.64 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/903/tariffs_corp_nnov_20180101.pdf" class="doc_item" target="_blank" data-showid="764" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            406.52 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/d24/tariffs_corp_kursk_20180101.pdf" class="doc_item" target="_blank" data-showid="766" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            563.27 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/8ae/tariffs_corp_g_alt_20180101.pdf" class="doc_item" target="_blank" data-showid="768" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            566.33 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/d0f/tariffs_corp_ektrb_20180101.pdf" class="doc_item" target="_blank" data-showid="772" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            491.65 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/96e/tariffs_corp_perm_20180101.pdf" class="doc_item" target="_blank" data-showid="775" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            488.89 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/b96/tariffs_corp_tatarstan_20180101.pdf" class="doc_item" target="_blank" data-showid="776" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            479.64 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/145/tariffs_corp_kaliningrad_20180101.pdf" class="doc_item" target="_blank" data-showid="8" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            480.57 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/52c/tariffs_corp_kemerovo_20180101.pdf" class="doc_item" target="_blank" data-showid="9" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            491.03 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/86f/tariffs_corp_kursk_20180101.pdf" class="doc_item" target="_blank" data-showid="10" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            563.27 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/e9b/tariffs_corp_tatarstan_20180101.pdf" class="doc_item" target="_blank" data-showid="12" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            479.64 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/9c5/tariffs_corp_nnov_20180101.pdf" class="doc_item" target="_blank" data-showid="13" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            406.52 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/fa7/empty.txt" class="doc_item" target="_blank" data-showid="3430" style="display:none;">
                                    <div class="doc_pict txt"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            3 Б                                </div>
                                    </div>
                                </a>
                                <h2 style="display:none;"><!--Бланки Заявлений--></h2>
                                <a href="" target="_blank" class="doc_item" data-showid="3430" style="display:none;">
                                    <div class="doc_pict "></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                        </div>
                                        <div class="doc_note">
                                        </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/561/tariffs_corp_kemerovo_20180101.pdf" class="doc_item" target="_blank" data-showid="14" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            491.03 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/25a/tariffs_corp_nsib_20180101.pdf" class="doc_item" target="_blank" data-showid="15" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            494.43 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/530/tariffs_corp_omsk_20180101.pdf" class="doc_item" target="_blank" data-showid="16" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            564.9 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/7f3/tariffs_corp_perm_20180101.pdf" class="doc_item" target="_blank" data-showid="17" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            488.89 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/762/tariffs_corp_rostov_20180101.pdf" class="doc_item" target="_blank" data-showid="18" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            568.15 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/55d/tariffs_corp_samara_20180101.pdf" class="doc_item" target="_blank" data-showid="19" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            566.21 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/6d6/tariffs_corp_spb_20180101.pdf" class="doc_item" target="_blank" data-showid="20" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            486.59 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/055/tariffs_corp_samara_20180101.pdf" class="doc_item" target="_blank" data-showid="21" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            566.21 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/b46/tariffs_corp_nnov_20180101.pdf" class="doc_item" target="_blank" data-showid="22" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            406.52 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/6df/tariffs_corp_taganrog_20180101.pdf" class="doc_item" target="_blank" data-showid="23" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            478.29 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/769/tariffs_corp_samara_20180101.pdf" class="doc_item" target="_blank" data-showid="24" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            566.21 Кб                                </div>
                                    </div>
                                </a>
                                <a href="/upload/iblock/74a/tariffs_corp_chelyab_20180101.pdf" class="doc_item" target="_blank" data-showid="25" style="display:none;">
                                    <div class="doc_pict pdf"></div>
                                    <div class="doc_body">
                                        <div class="doc_text">
                                            Тарифы за услуги для юридических лиц и индивидуальных предпринимателей за расчетно-кассовое обслуживание

                                        </div>
                                        <div class="doc_note">
                                            490.43 Кб                                </div>
                                    </div>
                                </a>
                            </div>
                            <!--doc-->
                        </div>
                    </div>
                </div>        </div>
            <div class="block_type_center">
                <div class="text_block">
                    <!--promo_benefits-->
                    <div class="promo_benefits big_element">
                        <div class="promo_benefits_item" id="bx_1392716086_1415">
                            <div class="pict " style="background-image: url('//opt-1125008.ssl.1c-bitrix-cdn.ru/upload/iblock/c4c/001.png?1511520787832');"></div>
                            <div class="body">
                                <div class="title">
                                    Оперативно            </div>
                                <div class="text">
                                    Получение номера счета до предоставления в Банк документов и заключения договора банковского счета.            </div>
                            </div>
                        </div><div class="promo_benefits_item" id="bx_1392716086_1416">
                            <div class="pict " style="background-image: url('//opt-1125008.ssl.1c-bitrix-cdn.ru/upload/iblock/a18/002.png?1511520823801');"></div>
                            <div class="body">
                                <div class="title">
                                    До 10 рабочих дней            </div>
                                <div class="text">
                                    Срок на который Банк резервирует номер расчетного счета            </div>
                            </div>
                        </div><div class="promo_benefits_item" id="bx_1392716086_1417">
                            <div class="pict " style="background-image: url('//opt-1125008.ssl.1c-bitrix-cdn.ru/upload/iblock/869/003.png?1511520791779');"></div>
                            <div class="body">
                                <div class="title">
                                    Бесплатно            </div>
                                <div class="text">
                                    Резервирование номера расчетного счета осуществляется бесплатно.            </div>
                            </div>
                        </div></div>
                    <!--promo_benefits-->
                </div>
                <!--form_application-->
                <div class="big_wr_form_application">
                    <form name="account_number" action="/businesses/rko/account/online-reserve/" method="POST" enctype="multipart/form-data"><input type="hidden" name="sessid" id="sessid" value="391a1a78c5e0808db9c4928c4f11c92c" /><input type="hidden" name="WEB_FORM_ID" value="3" />        <input type="hidden" name="web_form_apply" value="Y"/>
                        <div class="snippet-online-reserve">
                            <div class="snippet-online-reserve-body">
                                <div class="snippet-online-reserve-text">
                                    Для того чтобы воспользоваться
                                    сервисом Вам необходимо:
                                </div>
                                <div class="snippet-online-reserve-ul">
                                    <div class="snippet-online-reserve-li">
                                        ознакомиться с условиями предоставления
                                        сервиса
                                    </div>
                                    <div class="snippet-online-reserve-li">
                                        подтвердить согласие с условиями
                                        предоставления сервиса;
                                    </div>
                                    <div class="snippet-online-reserve-li">
                                        заполнить краткую анкету.
                                    </div>
                                </div>
                                <div class="snippet-online-reserve-chek">
                                    <label>
                                        <input type="checkbox" class="formstyle" id="42" name="form_checkbox_agreement[]" value="42"><label for="42"> </label>                                                Согласен с <a class="open_modal" href="#modal_form-agreement">условиями</a>
                                    </label>
                                </div>
                                <div class="snippet-online-reserve-button">
                                    <input class="button" value="Зарезервировать" type="submit">
                                </div>
                            </div>
                        </div>
                        <div class="form_errors_text"></div>
                        <div class="wr_form_application online-reserve-applicatio">
                            <div class="step_form_application first_step">
                                <div class="form_application">
                                    <h3>Выберите вид юридического лица</h3>
                                    <div class="form_application_line">
                                        <input type="hidden"
                                               name="form_dropdown_type"
                                               value="19">
                                        <div class="wr-toggle-light-text">
                                            <div class="toggle-light-text on">Индивидуальный предприниматель</div>
                                            <div class="toggle-light-wr">
                                                <div class="toggle toggle-light"
                                                     data-toggle data-toggle-type
                                                     data-checked-n="19" data-checked-y="20"
                                                     data-checked="N" data-name="form_dropdown_type"></div>
                                            </div>
                                            <div class="toggle-light-text off">Юридическое лицо-резидент РФ</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_application">
                                    <h3>О компании</h3>
                                    <div class="form_application_line" data-type-line="legal">
                                        <div class="wr_complex_input complex_input_noicon ">
                                            <div class="complex_input_body">
                                                <div class="text">Полное наименование ЮЛ</div>
                                                <input type="text" class="simple_input" autocomplete="off" name="form_text_21" value="" size="0" />            </div>
                                        </div>
                                    </div><div class="form_application_line" data-type-line="legal">
                                        <div class="wr_complex_input complex_input_noicon ">
                                            <div class="complex_input_body">
                                                <div class="text">Сокращенное наименование ЮЛ</div>
                                                <input type="text" class="simple_input" autocomplete="off" name="form_text_22" value="" size="0" />            </div>
                                        </div>
                                    </div><div class="form_application_line" data-type-line="legal">
                                        <select data-plasholder="Организационно-правовая форма" data-title="Организационно-правовая форма" class="formstyle" name="form_dropdown_organizational_form" id="form_dropdown_organizational_form"><option value="23">Акционерное общество</option><option value="24">Закрытое акционерное общество</option><option value="25">Открытое акционерное общество</option><option value="26">Публичное акционерное общество</option><option value="27">Производственный кооператив</option><option value="28">Общество с ограниченной ответственностью</option></select>        </div>                    <div class="form_application_line" data-type-line="ip">
                                        <div class="wr_complex_input complex_input_noicon ">
                                            <div class="complex_input_body">
                                                <div class="text">Полное наименование ИП</div>
                                                <input type="text" class="simple_input" autocomplete="off" name="form_text_29" value="" size="0" />            </div>
                                        </div>
                                    </div><div class="form_application_line" data-type-line="ip">
                                        <div class="wr_complex_input complex_input_noicon ">
                                            <div class="complex_input_body">
                                                <div class="text">Сокращенное наименование ИП</div>
                                                <input type="text" class="simple_input" autocomplete="off" name="form_text_30" value="" size="0" />            </div>
                                        </div>
                                    </div>                    <h3>Данные компании</h3>
                                    <div class="form_application_line">
                                        <div class="form_application_to">
                                            <div class="wr_complex_input complex_input_noicon ">
                                                <div class="complex_input_body">
                                                    <div class="text">ИНН</div>
                                                    <input type="text" class="simple_input" autocomplete="off" name="form_text_31" value="" size="0" />            </div>
                                            </div>
                                        </div>
                                        <div class="form_application_to" data-type-line="legal">
                                            <div class="wr_complex_input complex_input_noicon ">
                                                <div class="complex_input_body">
                                                    <div class="text">ОГРН</div>
                                                    <input type="text" class="simple_input" autocomplete="off" name="form_text_32" value="" size="0" />            </div>
                                            </div>
                                        </div>
                                        <div class="form_application_to" data-type-line="ip">
                                            <div class="wr_complex_input complex_input_noicon ">
                                                <div class="complex_input_body">
                                                    <div class="text">ОГРНИП</div>
                                                    <input type="text" class="simple_input" autocomplete="off" name="form_text_33" value="" size="0" />            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_application_line">
                                        <div class="form_application_to" data-type-line="legal">
                                            <select data-plasholder="Тип деятельности" data-title="Тип деятельности" class="formstyle" name="form_dropdown_type_activity" id="form_dropdown_type_activity"><option value="34">Внебюджетная</option><option value="35">Коммерческая</option></select>                        </div>
                                        <div class="form_application_to">
                                            <select data-plasholder="Объем годовой выручки" data-title="Объем годовой выручки" class="formstyle" name="form_dropdown_annual_revenue_volume" id="form_dropdown_annual_revenue_volume"><option value="36">менее 120 млн. руб.</option><option value="37">120 млн. - 800 млн. руб.</option><option value="38">более 800 млн. руб.</option></select>                        </div>
                                    </div>
                                    <div class="form_application_line">
                                        <div class="form_application_to">
                                            <input type="hidden"
                                                   name="form_text_type"
                                                   id="form_text_type"
                                                   value="Москва и МО">
                                            <select id="select_form_text_43"
                                                    data-id="form_text_43"
                                                    name="form_text_43"
                                                    data-plasholder="Город"
                                                    data-title="Город" class="formstyle">
                                                <option  value="id_3397">Аксай</option>
                                                <option  value="id_763">Альметьевск</option>
                                                <option  value="id_764">Арзамас</option>
                                                <option  value="id_765">Бабяково</option>
                                                <option  value="id_766">Воронеж</option>
                                                <option  value="id_767">Выкса</option>
                                                <option  value="id_768">Горно-Алтайск</option>
                                                <option  value="id_769">Городец</option>
                                                <option  value="id_770">Дзержинск</option>
                                                <option  value="id_771">Дивеево</option>
                                                <option  value="id_772">Екатеринбург</option>
                                                <option  value="id_773">Жигулевск</option>
                                                <option  value="id_774">Зеленовка</option>
                                                <option  value="id_775">Ижевск</option>
                                                <option  value="id_776">Казань</option>
                                                <option  value="id_8">Калининград</option>
                                                <option  value="id_777">Катунь</option>
                                                <option  value="id_9">Кемерово</option>
                                                <option  value="id_778">Копейск</option>
                                                <option  value="id_779">Кстово</option>
                                                <option  value="id_10">Курск</option>
                                                <option  value="id_780">Кызыл-Озек</option>
                                                <option  value="id_781">Майма</option>
                                                <option selected value="id_11">Москва и МО</option>
                                                <option  value="id_12">Нижнекамск</option>
                                                <option  value="id_13">Нижний Новгород</option>
                                                <option  value="id_14">Новокузнецк</option>
                                                <option  value="id_15">Новосибирск</option>
                                                <option  value="id_16">Омск</option>
                                                <option  value="id_17">Пермь</option>
                                                <option  value="id_3436">Покровское</option>
                                                <option  value="id_783">Ревда</option>
                                                <option  value="id_18">Ростов-на-Дону</option>
                                                <option  value="id_19">Самара</option>
                                                <option  value="id_20">Санкт-Петербург</option>
                                                <option  value="id_21">Саратов</option>
                                                <option  value="id_22">Саров</option>
                                                <option  value="id_784">Сосновка</option>
                                                <option  value="id_785">Сургут</option>
                                                <option  value="id_23">Таганрог</option>
                                                <option  value="id_786">Тверь</option>
                                                <option  value="id_24">Тольятти</option>
                                                <option  value="id_25">Челябинск</option>
                                                <option  value="id_787">Южноуральск</option>
                                            </select>
                                        </div>
                                        <div class="form_application_to">
                                            <input type="hidden"
                                                   name="form_text_type"
                                                   id="form_text_type"
                                                   value="">
                                            <select id="select_form_text_44"
                                                    data-id="form_text_44"
                                                    name="form_text_44"
                                                    data-plasholder="Офис"
                                                    data-title="Офис" class="formstyle">
                                                <option  value="id_5149" data-chained="id_10">"Курский" филиал</option>
                                                <option  value="id_5153" data-chained="id_18">"Ростовский" филиал</option>
                                                <option  value="id_5491" data-chained="id_3396">Азнакаевский филиал  </option>
                                                <option  value="id_5495" data-chained="id_3401">Бугульминский филиал   </option>
                                                <option  value="id_5487" data-chained="id_763">Головной офис  </option>
                                                <option  value="id_5124" data-chained="id_11">Головной офис</option>
                                                <option  value="id_5455" data-chained="id_3445">Головной офис </option>
                                                <option  value="id_5462" data-chained="id_3424">Головной офис </option>
                                                <option  value="id_5483" data-chained="id_3439">Головной офис</option>
                                                <option  value="id_5488" data-chained="id_763">Департамент сбережений вкладов населения  </option>
                                                <option  value="id_5145" data-chained="id_20">ДО "Суворовский" Филиала «Банковский центр «БАЛТИКА»</option>
                                                <option  value="id_5456" data-chained="id_3407">Донской филиал  </option>
                                                <option  value="id_5489" data-chained="id_763">Дополнительный офис  </option>
                                                <option  value="id_5493" data-chained="id_3400">Дополнительный офис </option>
                                                <option  value="id_5496" data-chained="id_3401">Дополнительный офис  </option>
                                                <option  value="id_5499" data-chained="id_3405">Дополнительный офис </option>
                                                <option  value="id_5454" data-chained="id_763">Дополнительный офис</option>
                                                <option  value="id_5506" data-chained="id_3428">Дополнительный офис  </option>
                                                <option  value="id_5484" data-chained="id_3399">Дополнительный офис "Анапа"</option>
                                                <option  value="id_5389" data-chained="id_11">Дополнительный офис "Водный"</option>
                                                <option  value="id_5126" data-chained="id_11">Дополнительный офис "Европейский"</option>
                                                <option  value="id_5485" data-chained="id_3417">Дополнительный офис "Краснодар"</option>
                                                <option  value="id_5129" data-chained="id_11">Дополнительный офис "Мытищи"</option>
                                                <option  value="id_5156" data-chained="id_763">Дополнительный офис "На Белоглазова" в г. Альметьевске филиала "Банковский центр ТАТАРСТАН"</option>
                                                <option  value="id_5150" data-chained="id_10">Дополнительный офис "На Ленина" "Курского" филиала</option>
                                                <option  value="id_5157" data-chained="id_12">Дополнительный офис "Нижнекамский" филиала "Банковский центр ТАТАРСТАН"</option>
                                                <option  value="id_5130" data-chained="id_11">Дополнительный офис "Одинцово"</option>
                                                <option  value="id_5127" data-chained="id_11">Дополнительный офис "Пресненский"</option>
                                                <option  value="id_5128" data-chained="id_11">Дополнительный офис "Семеновский"</option>
                                                <option  value="id_5131" data-chained="id_11">Дополнительный офис "Химки-2"</option>
                                                <option  value="id_5486" data-chained="id_3439">Дополнительный офис «Адлер»</option>
                                                <option  value="id_5152" data-chained="id_764">Дополнительный офис «Арзамасский» Филиала «Банковский центр «ВОЛГА»</option>
                                                <option  value="id_5804" data-chained="id_3439">Дополнительный офис «Донской» ЗАО Банк ЗЕНИТ Сочи</option>
                                                <option  value="id_5802" data-chained="id_776">Дополнительный офис «Магеллан»</option>
                                                <option  value="id_5270" data-chained="id_19">Дополнительный офис «Октябрьский» Филиала «Банковский центр «ПОВОЛЖЬЕ»</option>
                                                <option  value="id_6042" data-chained="id_763">Дополнительный офис «Панорама»</option>
                                                <option  value="id_6034" data-chained="id_3439">Дополнительный офис «Ривьера» ЗАО Банк ЗЕНИТ Сочи</option>
                                                <option  value="id_5269" data-chained="id_22">Дополнительный офис «Саровский» Филиала «Банковский центр «ВОЛГА»</option>
                                                <option  value="id_5271" data-chained="id_23">Дополнительный офис «Таганрогский» «Ростовского» филиала ПАО Банк ЗЕНИТ</option>
                                                <option  value="id_5159" data-chained="id_24">Дополнительный офис «Тольяттинский» Филиала «Банковский центр «ПОВОЛЖЬЕ»</option>
                                                <option  value="id_5457" data-chained="id_3431">Дополнительный офис №1 Донского филиала </option>
                                                <option  value="id_5458" data-chained="id_3445">Дополнительный офис №2 </option>
                                                <option  value="id_5460" data-chained="id_3445">Дополнительный офис №4 </option>
                                                <option  value="id_5476" data-chained="id_3403">Дополнительный офис Грязинское отделение </option>
                                                <option  value="id_5477" data-chained="id_3404">Дополнительный офис Данковское отделение </option>
                                                <option  value="id_5478" data-chained="id_3421">Дополнительный офис Лебедянское отделение </option>
                                                <option  value="id_5806" data-chained="id_3424">Дополнительный офис ЛКБ ПЛЮС "9 мая"</option>
                                                <option  value="id_6037" data-chained="id_3424">Дополнительный офис ЛКБ ПЛЮС "Октябрьское"</option>
                                                <option  value="id_6036" data-chained="id_3424">Дополнительный офис ЛКБ ПЛЮС "Университетское"</option>
                                                <option  value="id_5463" data-chained="id_3424">Дополнительный офис Новолипецкое отделение </option>
                                                <option  value="id_5467" data-chained="id_3424">Дополнительный офис отделение Вермишева </option>
                                                <option  value="id_5474" data-chained="id_3424">Дополнительный офис Отделение Гагарина </option>
                                                <option  value="id_5471" data-chained="id_3424">Дополнительный офис Отделение Газина </option>
                                                <option  value="id_5470" data-chained="id_3424">Дополнительный офис Отделение Катукова </option>
                                                <option  value="id_5466" data-chained="id_3424">Дополнительный офис отделение Победы </option>
                                                <option  value="id_5473" data-chained="id_3424">Дополнительный офис Отделение Расковой </option>
                                                <option  value="id_5468" data-chained="id_3424">Дополнительный офис Отделение Семашко </option>
                                                <option  value="id_5469" data-chained="id_3424">Дополнительный офис Отделение Строителей </option>
                                                <option  value="id_5472" data-chained="id_3424">Дополнительный офис отделение Титова  </option>
                                                <option  value="id_5475" data-chained="id_3424">Дополнительный офис отделение Тракторозаводское </option>
                                                <option  value="id_5465" data-chained="id_3424">Дополнительный офис Площадь Победы </option>
                                                <option  value="id_5464" data-chained="id_3424">Дополнительный офис Советское отделение </option>
                                                <option  value="id_6035" data-chained="id_3442">Дополнительный офис Тербунское отделение</option>
                                                <option  value="id_5500" data-chained="id_3408">Елабужский филиал  </option>
                                                <option  value="id_5479" data-chained="id_3409">Елецкий филиал </option>
                                                <option  value="id_5502" data-chained="id_3411">Заинский филиал  </option>
                                                <option  value="id_5503" data-chained="id_776">Казанский филиал  </option>
                                                <option  value="id_5125" data-chained="id_11">Клиентский центр "Садовая Слобода"</option>
                                                <option  value="id_5161" data-chained="id_785">Кредитно-кассовый офис «Сургутский» Филиала «Банковский центр «УРАЛ»</option>
                                                <option  value="id_5504" data-chained="id_3423">Лениногорский филиал   </option>
                                                <option  value="id_5480" data-chained="id_11">Московский филиал </option>
                                                <option  value="id_5505" data-chained="id_3428">Набережночелнинский филиал  </option>
                                                <option  value="id_5507" data-chained="id_12">Нижнекамский филиал  </option>
                                                <option  value="id_5508" data-chained="id_3434">Нурлатский филиал  </option>
                                                <option  value="id_5497" data-chained="id_3414">ОКВКУ (Опер касса вне кассового узла)</option>
                                                <option  value="id_5501" data-chained="id_3408">Операционная касса  </option>
                                                <option  value="id_5490" data-chained="id_763">Операционная касса  </option>
                                                <option  value="id_6041" data-chained="id_3398">Операционная касса  </option>
                                                <option  value="id_5492" data-chained="id_3396">Операционная касса  </option>
                                                <option  value="id_5494" data-chained="id_3400">Операционная касса  </option>
                                                <option  value="id_5498" data-chained="id_3401">Операционная касса № 3  </option>
                                                <option  value="id_5803" data-chained="id_3439">Операционная касса №2 вне кассового узла</option>
                                                <option  value="id_5260" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_6068" data-chained="id_3439">Операционная касса вне кассового узла</option>
                                                <option  value="id_5311" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5272" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5314" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5273" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5315" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5339" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5132" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5274" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5340" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5133" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5278" data-chained="id_10">Операционная касса вне кассового узла</option>
                                                <option  value="id_5397" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5134" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5280" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5408" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5135" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5281" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5412" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5136" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5413" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5137" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5414" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5138" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5415" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5139" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5416" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5140" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5417" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5461" data-chained="id_3431">Операционная касса вне кассового узла </option>
                                                <option  value="id_5141" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5451" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5142" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_5143" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_6066" data-chained="id_3439">Операционная касса вне кассового узла</option>
                                                <option  value="id_5306" data-chained="id_20">Операционная касса вне кассового узла</option>
                                                <option  value="id_5258" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_6067" data-chained="id_3439">Операционная касса вне кассового узла</option>
                                                <option  value="id_5309" data-chained="id_11">Операционная касса вне кассового узла</option>
                                                <option  value="id_6033" data-chained="id_3439">Операционная касса вне кассового узла №1</option>
                                                <option  value="id_5154" data-chained="id_25">Операционний офис «Челябинский» Филиала «Банковский центр «УРАЛ»</option>
                                                <option  value="id_5293" data-chained="id_775">Операционный офис "Ижевский" Филиала "Банковский центр "ВОЛГА"</option>
                                                <option  value="id_5146" data-chained="id_8">Операционный офис "Калининградский" Филиала «Банковский центр «БАЛТИКА»</option>
                                                <option  value="id_5801" data-chained="id_3417">Операционный офис "Карасунский"</option>
                                                <option  value="id_5147" data-chained="id_9">Операционный офис "Кемеровский" Филиала Банковский центр  СИБИРЬ</option>
                                                <option  value="id_5148" data-chained="id_14">Операционный офис "Новокузнецкий"  Филиала Банковский центр СИБИРЬ</option>
                                                <option  value="id_5267" data-chained="id_766">Операционный офис «Воронежский» «Курского» филиала ПАО Банк ЗЕНИТ</option>
                                                <option  value="id_5164" data-chained="id_768">Операционный офис «Горно-Алтайский» Филиала «Банковский центр «СИБИРЬ»</option>
                                                <option  value="id_5268" data-chained="id_16">Операционный офис «Омский» Филиала «Банковский центр «СИБИРЬ»</option>
                                                <option  value="id_5162" data-chained="id_17">Операционный офис «Пермский» Филиала "Банковский центр "ВОЛГА"</option>
                                                <option  value="id_5160" data-chained="id_21">Операционный офис «Саратовский» Филиала «Банковский центр «ПОВОЛЖЬЕ»</option>
                                                <option  value="id_5481" data-chained="id_3440">Старооскольский филиал </option>
                                                <option  value="id_5482" data-chained="id_3443">Туапсинский филиал  </option>
                                                <option  value="id_5155" data-chained="id_776">Филиал "Банковский центр ТАТАРСТАН"</option>
                                                <option  value="id_5144" data-chained="id_20">Филиал «Банковский центр «БАЛТИКА»</option>
                                                <option  value="id_5151" data-chained="id_13">Филиал «Банковский центр «ВОЛГА»</option>
                                                <option  value="id_5158" data-chained="id_19">Филиал «Банковский центр «ПОВОЛЖЬЕ»</option>
                                                <option  value="id_5163" data-chained="id_15">Филиал «Банковский центр «СИБИРЬ»</option>
                                                <option  value="id_5289" data-chained="id_772">Филиал «Банковский центр «УРАЛ»</option>
                                                <option  value="id_5509" data-chained="id_3446">Чебоксарский филиал  </option>
                                                <option  value="id_5510" data-chained="id_3448">Чистопольский филиал  </option>
                                            </select>
                                        </div>
                                    </div>
                                    <h3>Контактная информация</h3>
                                    <div class="form_application_line">
                                        <div class="form_application_to">
                                            <div class="wr_complex_input complex_input_noicon ">
                                                <div class="complex_input_body">
                                                    <div class="text">Номер телефона</div>
                                                    <input type="text" class="simple_input" autocomplete="off" name="form_text_39" value="" size="0" />            </div>
                                            </div>
                                        </div>
                                        <div class="form_application_to">
                                            <div class="wr_complex_input complex_input_noicon ">
                                                <div class="complex_input_body">
                                                    <div class="text">Адрес электронной почты</div>
                                                    <input type="text" class="simple_input" autocomplete="off" name="form_email_41" value="" size="0" />            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_application_line">
                                        <div class="wr_complex_input complex_input_noicon ">
                                            <div class="complex_input_body">
                                                <div class="text">Комментарии</div>
                                                <textarea name="form_textarea_40" cols="40" rows="5" class="simple_input" autocomplete="off"></textarea>            </div>
                                        </div>
                                    </div>
                                    <div class="form_application_line form_text">
                                        Для резервирования номера счета, необходимо получить код подтверждения,
                                        который будет направлен на указанный Вами адрес электронной почты.
                                        Код действителен в течение 15 минут!
                                    </div>
                                    <div class="form_application_line confirmation_block">
                                        <div class="form_application_to">
                                            <div class="wr_complex_input complex_input_noicon ">
                                                <div class="complex_input_body">
                                                    <div class="text">Код подтверждения</div>
                                                    <input type="text" class="simple_input" autocomplete="off" name="confirmation"
                                                           value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_application_to">
                                            <input data-setcode class="button" value="Получить код" type="button">
                                        </div>
                                    </div>
                                    <div class="form_application_line buttom_line">
                                        <div class="form_application_line buttom_line_captcha">
                                            <input type="hidden"
                                                   name="captcha_sid"
                                                   value="03ed190dcb6ce92d8230c90f4eb1d62d"/>
                                            <div class="g-recaptcha" data-theme="light" data-sitekey="6LeK6igUAAAAAN2AGbpkOOpsC4i5xHhLx4YGovuq"  data-size="normal"  data-badge="bottomright" data-callback="RecaptchafreeSubmitForm"></div>

                                            <input type="text" name="captcha_word" style="display:none" value="6LeK6" size="30" maxlength="50" value=""
                                                   class="inputtext"/>
                                            <div id="recaptchaErrop" class="diverror">Подтвердите, что вы не робот</div>
                                        </div>
                                        <div class="form_application_line buttom_line_button">
                                            <input class="button" value="Продолжить" type="submit">
                                        </div>
                                    </div>
                                </div>
                    </form>            </div>
                <div class="step_form_application to_step">
                    <div class="form_application">
                        <div class="form_application_ok"></div>
                        <div class="form_application_title">
                            Ваша заявка принята!
                        </div>
                        <div class="form_application_text">
                            Номер заявки <b data-result></b>. Подтверждение отправлено на e-mall. Срок<br/>
                            рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
                            свяжется с Вами в ближайшее время.
                        </div>
                    </div>
                </div>
            </div>
            </form>        <!--form_application-->
        </div>


    </div>
</div>

</div>






<!--footer-->
<div class="footer">
    <div class="first_line z-container">
        <div class="call">
            <div class="call_title">
                Бесплатный звонок
            </div>
            <div class="call_phone">
                8 (800) 500-66-77            </div>
            <div class="call_phone min">
                +7 (495) 967-11-11            </div>
            <div class="call_button">
                <a class="button footer" href="mailto:info@zenit.ru">написать нам</a>
                <a class="skype" href="skype:zenitgroup.ru?call"></a>
            </div>
        </div>



        <ul class="block_menu">

            <li><a href="/rus/about_bank/" class="">О Банке</a></li>

            <li><a href="/about/general/information-social-projects/social-projects/" class="">Социальные проекты</a></li>

            <li><a href="https://old.zenit.ru/group/" class="">Банковская группа ЗЕНИТ</a></li>

            <li><a href="/rus/about_bank/disclosure/" class="">Раскрытие информации</a></li>

            <li><a href="/rus/about_bank/disclosure/equity-structure/" class="">Структура акционерного капитала</a></li>

        </ul><ul class="block_menu">			<li><a href="/rus/about_bank/general/property-sale/" class="">Залоговое имущество</a></li>

            <li><a href="https://old.zenit.ru/rus/about_bank/bank_in_press/" class="">Публикации в СМИ</a></li>

            <li><a href="#modal_form-contact" class="open_modal">Задать вопрос</a></li>

            <li><a href="/about/general/trust-line/" class="">Линия доверия</a></li>


        </ul>
        <div class="links">
            <ul class="fuuter_net">
                <li><a href="https://vk.com/bank_zenit" class="vk" id="bx_32686673_33"></a></li> <li><a href="https://www.facebook.com/BankZENIT/" class="f" id="bx_32686673_34"></a></li> <li><a href="https://twitter.com/bank_zenit" class="tw" id="bx_32686673_35"></a></li> </ul>            <div class="call_title">
                Мобильный банк
            </div>
            <ul class="fuuter_app">
                <li><a href="https://itunes.apple.com/ru/app/id1187159866" class="apple" id="bx_1995829959_31"></a></li> <li><a href="https://play.google.com/store/apps/details?id=ru.zenit.zenitonline" class="android" id="bx_1995829959_32"></a></li> </ul>        </div>
    </div>
    <div class="to_line">
        <div class="z-container">
            <div class="license">
                ©2017 ПАО Банк ЗЕНИТ. Лицензия<br>
                Банка России №3255 от 16.12.2014            </div>
            <div class="text_block">
                <div class="st"></div>
                <div class="text">
                    Максимальные <a href="/about/disclosure/disclosure-information/information-rates/">процентные ставки</a><br>
                    по вкладам физических лиц
                </div>
            </div>            <div class="text_block min">
                <div class="text">
                    Страница раскрытия информации<br>
                    на сайте <a href="http://www.e-disclosure.ru/portal/company.aspx?id=538" target="_blank">ООО "Интерфакс-ЦРКИ"</a>                </div>
            </div>
        </div>
    </div>
</div>
<!--footer-->
</div>

<!-- Модальное окно для отображения формы обратной связи -->


<div class="modal-open modal form">
    <div id="modal_form-contact" class="modal_div">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="col-sm-12 col-md-12 modal_fon px-4">
                    <div class="row first_step">
                        <div class="col-sm-12 col-md-12">
                            <div class="form_errors_text">
                            </div>
                        </div>
                        <form name="SIMPLE_FORM_2" action="/businesses/rko/account/online-reserve/" method="POST" enctype="multipart/form-data"><input type="hidden" name="sessid" id="sessid_1" value="391a1a78c5e0808db9c4928c4f11c92c" /><input type="hidden" name="WEB_FORM_ID" value="2" />                                                        <input type="hidden" name="web_form_apply" value="Y"/>
                            <input type="hidden" name="form__"
                                   value="">

                            <div class="col-sm-12 col-md-12">
                                <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <h3>Задать вопрос</h3>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <select class="formstyle" name="topic" data-programm="" data-plasholder="выберите тему обращения" data-title="выберите тему обращения" name="form_dropdown_topic" id="form_dropdown_topic"><option value="9">Услуги и тарифы</option><option value="15">Офисы и банкоматы</option><option value="16">Предложение о сотрудничестве</option><option value="17">Пожелания</option><option value="18">Претензия</option><option value="77">Отзыв о сайте</option></select>                                </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="wr_complex_input complex_input_noicon ">
                                    <div class="complex_input_body">
                                        <div class="text">Фамилия, имя и отчество</div>
                                        <input type="text" class="simple_input user-neme" autocomplete="off" name="form_text_10" value="" size="0" />            </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="wr_complex_input complex_input_noicon ">
                                    <div class="complex_input_body">
                                        <div class="text">Телефон для связи</div>
                                        <input type="text" class="simple_input" autocomplete="off" name="form_text_11" value="" size="0" />            </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="wr_complex_input complex_input_noicon ">
                                    <div class="complex_input_body">
                                        <div class="text">E-mail для ответа</div>
                                        <input type="text" class="simple_input" autocomplete="off" name="form_email_12" value="" size="0" />            </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="wr_complex_input complex_input_noicon ">
                                    <div class="complex_input_body">
                                        <div class="text">Ваш вопрос</div>
                                        <textarea name="form_textarea_13" cols="40" rows="10" rows="10" cols="45" name="text" class="simple_input"></textarea>            </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-8">
                                <div class="form_application_button">
                                    <label><input type="checkbox" class="formstyle" id="14" name="form_checkbox_agreement[]" value="14"><label for="14"> </label>Согласен с <a target="_blank" href="https://www.zenit.ru/media/rus/content/pdf/personal/pd_confirm.pdf">условиями</a> обработки данных</label>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="form_application_button_right">
                                    <input class="button" value="отправить" type="submit">
                                </div>
                            </div>
                        </form>                        </div>
                    <div class="step_form to_step">
                        <div class="block_type to_column">
                            <div class="block_type_center">
                                <div class="wr_form_application">
                                    <div class="form_application">
                                        <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        <div class="form_application_ok alt"></div>
                                        <div class="form_application_title">
                                            Ваш вопрос принят!
                                        </div>
                                        <div class="form_application_text">
                                            Представитель банка свяжется с Вами в ближайшее время.<br/>
                                            Благодарим за обращение.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Модальное окно с условиями онлайн-резервирования счета -->

<div class="modal-open modal form">
    <div id="modal_form-agreement" class="modal_div">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="col-sm-12 col-md-12 modal_fon px-4">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <h3>Условия предоставления сервиса онлайн-резервирования номера расчетного счета</h3>
                            <div class="text-block">
                                <p>Сервис Онлайн-резервирования номера расчетного счета предоставляется юридическим лицам – резидентам РФ (за исключением финансовых, некоммерческих, государственных и муниципальных унитарных предприятий) и индивидуальным предпринимателям.
                                </p>
                                <h4>Сервис позволяет зарезервировать следующие номера расчетных счетов:</h4>
                                <ul>
                                    <li>40702* – для юридических лиц;</li>
                                    <li>40802* – для индивидуальных предпринимателей.</li>
                                </ul>
                                <p>
                                    Для резервирования номера расчетного счета необходимо заполнить электронную анкету, отразив в ней действительную и достоверную информацию о юридическом лице или индивидуальном предпринимателе.
                                </p>
                                <h4>Используя сервис Онлайн-резервирования номера расчетного счета на сайте ПАО Банк ЗЕНИТ, Вы соглашаетесь с тем, что:</h4>
                                <ol>
                                    <li>Номер расчетного счета резервируется на срок не более чем 10 рабочих дней.</li>
                                    <li>Валютой зарезервированного расчетного счета является российский рубль.</li>
                                    <li>Один электронный адрес, указанный в анкете, позволяет зарезервировать не более 3 номеров расчетных счетов в течение одного календарного дня.</li>
                                    <li>Для открытия расчетного счета Вам необходимо в указанный в п.1 срок предоставить в выбранное при заполнении электронной анкеты структурное подразделение Банка полный комплект документов, необходимый для открытия расчетного счета в установленном порядке.</li>
                                    <li>До момента открытия расчетного счета номер зарезервированного расчетного счета носит исключительно информационный характер. Никакие банковские операции по данному счету в течение срока резервирования осуществляться не могут.</li>
                                    <li>При нарушении срока предоставления полного комплекта документов для открытия расчетного счета в установленном Банком порядке, резервирование указанного номера счета аннулируется.</li>
                                    <li>Банк не гарантирует возможность дальнейшего использования номера расчетного счета, резервирование которого аннулировано.</li>
                                </ol>
                                <p>ПАО Банк ЗЕНИТ оставляет за собой право отказать в заключении Договора банковского счета и(или) открытии расчетного счета в случаях, предусмотренных действующим законодательством Российской Федерации.</p>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Yandex.Metrika counter -->
<noscript><div><img src="https://mc.yandex.ru/watch/46786158" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div id="overlay"> <!-- пoдлoжкa, дoлжнa быть oднa нa стрaнице --> </div>
<div class="wr_mobail_city open">
    <div class="wr_select_city_in mobail">
        <div class="select_city">
            <div class="select_city_text">
                Ваш город
            </div>
            <div class="select_city_name">
                Москва и МО            </div>
            <table>
                <tr>
                    <td><a class="button maxwidth" data-close-city data-set-cook-city href="javascript:void(0)">Да</a></td>
                    <td><a href="#" title="Выбор города" data-select-city>выбрать другой</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">if(!window.BX)window.BX={};if(!window.BX.message)window.BX.message=function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;};</script>
<script type="text/javascript">(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'WEEK_START':'1','AMPM_MODE':false});(window.BX||top.BX).message({'MONTH_1':'Январь','MONTH_2':'Февраль','MONTH_3':'Март','MONTH_4':'Апрель','MONTH_5':'Май','MONTH_6':'Июнь','MONTH_7':'Июль','MONTH_8':'Август','MONTH_9':'Сентябрь','MONTH_10':'Октябрь','MONTH_11':'Ноябрь','MONTH_12':'Декабрь','MONTH_1_S':'Января','MONTH_2_S':'Февраля','MONTH_3_S':'Марта','MONTH_4_S':'Апреля','MONTH_5_S':'Мая','MONTH_6_S':'Июня','MONTH_7_S':'Июля','MONTH_8_S':'Августа','MONTH_9_S':'Сентября','MONTH_10_S':'Октября','MONTH_11_S':'Ноября','MONTH_12_S':'Декабря','MON_1':'Янв','MON_2':'Фев','MON_3':'Мар','MON_4':'Апр','MON_5':'Май','MON_6':'Июн','MON_7':'Июл','MON_8':'Авг','MON_9':'Сен','MON_10':'Окт','MON_11':'Ноя','MON_12':'Дек','DAY_OF_WEEK_0':'Воскресенье','DAY_OF_WEEK_1':'Понедельник','DAY_OF_WEEK_2':'Вторник','DAY_OF_WEEK_3':'Среда','DAY_OF_WEEK_4':'Четверг','DAY_OF_WEEK_5':'Пятница','DAY_OF_WEEK_6':'Суббота','DOW_0':'Вс','DOW_1':'Пн','DOW_2':'Вт','DOW_3':'Ср','DOW_4':'Чт','DOW_5':'Пт','DOW_6':'Сб','FD_SECOND_AGO_0':'#VALUE# секунд назад','FD_SECOND_AGO_1':'#VALUE# секунду назад','FD_SECOND_AGO_10_20':'#VALUE# секунд назад','FD_SECOND_AGO_MOD_1':'#VALUE# секунду назад','FD_SECOND_AGO_MOD_2_4':'#VALUE# секунды назад','FD_SECOND_AGO_MOD_OTHER':'#VALUE# секунд назад','FD_SECOND_DIFF_0':'#VALUE# секунд','FD_SECOND_DIFF_1':'#VALUE# секунда','FD_SECOND_DIFF_10_20':'#VALUE# секунд','FD_SECOND_DIFF_MOD_1':'#VALUE# секунда','FD_SECOND_DIFF_MOD_2_4':'#VALUE# секунды','FD_SECOND_DIFF_MOD_OTHER':'#VALUE# секунд','FD_MINUTE_AGO_0':'#VALUE# минут назад','FD_MINUTE_AGO_1':'#VALUE# минуту назад','FD_MINUTE_AGO_10_20':'#VALUE# минут назад','FD_MINUTE_AGO_MOD_1':'#VALUE# минуту назад','FD_MINUTE_AGO_MOD_2_4':'#VALUE# минуты назад','FD_MINUTE_AGO_MOD_OTHER':'#VALUE# минут назад','FD_MINUTE_DIFF_0':'#VALUE# минут','FD_MINUTE_DIFF_1':'#VALUE# минута','FD_MINUTE_DIFF_10_20':'#VALUE# минут','FD_MINUTE_DIFF_MOD_1':'#VALUE# минута','FD_MINUTE_DIFF_MOD_2_4':'#VALUE# минуты','FD_MINUTE_DIFF_MOD_OTHER':'#VALUE# минут','FD_MINUTE_0':'#VALUE# минут','FD_MINUTE_1':'#VALUE# минуту','FD_MINUTE_10_20':'#VALUE# минут','FD_MINUTE_MOD_1':'#VALUE# минуту','FD_MINUTE_MOD_2_4':'#VALUE# минуты','FD_MINUTE_MOD_OTHER':'#VALUE# минут','FD_HOUR_AGO_0':'#VALUE# часов назад','FD_HOUR_AGO_1':'#VALUE# час назад','FD_HOUR_AGO_10_20':'#VALUE# часов назад','FD_HOUR_AGO_MOD_1':'#VALUE# час назад','FD_HOUR_AGO_MOD_2_4':'#VALUE# часа назад','FD_HOUR_AGO_MOD_OTHER':'#VALUE# часов назад','FD_HOUR_DIFF_0':'#VALUE# часов','FD_HOUR_DIFF_1':'#VALUE# час','FD_HOUR_DIFF_10_20':'#VALUE# часов','FD_HOUR_DIFF_MOD_1':'#VALUE# час','FD_HOUR_DIFF_MOD_2_4':'#VALUE# часа','FD_HOUR_DIFF_MOD_OTHER':'#VALUE# часов','FD_YESTERDAY':'вчера','FD_TODAY':'сегодня','FD_TOMORROW':'завтра','FD_DAY_AGO_0':'#VALUE# дней назад','FD_DAY_AGO_1':'#VALUE# день назад','FD_DAY_AGO_10_20':'#VALUE# дней назад','FD_DAY_AGO_MOD_1':'#VALUE# день назад','FD_DAY_AGO_MOD_2_4':'#VALUE# дня назад','FD_DAY_AGO_MOD_OTHER':'#VALUE# дней назад','FD_DAY_DIFF_0':'#VALUE# дней','FD_DAY_DIFF_1':'#VALUE# день','FD_DAY_DIFF_10_20':'#VALUE# дней','FD_DAY_DIFF_MOD_1':'#VALUE# день','FD_DAY_DIFF_MOD_2_4':'#VALUE# дня','FD_DAY_DIFF_MOD_OTHER':'#VALUE# дней','FD_DAY_AT_TIME':'#DAY# в #TIME#','FD_MONTH_AGO_0':'#VALUE# месяцев назад','FD_MONTH_AGO_1':'#VALUE# месяц назад','FD_MONTH_AGO_10_20':'#VALUE# месяцев назад','FD_MONTH_AGO_MOD_1':'#VALUE# месяц назад','FD_MONTH_AGO_MOD_2_4':'#VALUE# месяца назад','FD_MONTH_AGO_MOD_OTHER':'#VALUE# месяцев назад','FD_MONTH_DIFF_0':'#VALUE# месяцев','FD_MONTH_DIFF_1':'#VALUE# месяц','FD_MONTH_DIFF_10_20':'#VALUE# месяцев','FD_MONTH_DIFF_MOD_1':'#VALUE# месяц','FD_MONTH_DIFF_MOD_2_4':'#VALUE# месяца','FD_MONTH_DIFF_MOD_OTHER':'#VALUE# месяцев','FD_YEARS_AGO_0':'#VALUE# лет назад','FD_YEARS_AGO_1':'#VALUE# год назад','FD_YEARS_AGO_10_20':'#VALUE# лет назад','FD_YEARS_AGO_MOD_1':'#VALUE# год назад','FD_YEARS_AGO_MOD_2_4':'#VALUE# года назад','FD_YEARS_AGO_MOD_OTHER':'#VALUE# лет назад','FD_YEARS_DIFF_0':'#VALUE# лет','FD_YEARS_DIFF_1':'#VALUE# год','FD_YEARS_DIFF_10_20':'#VALUE# лет','FD_YEARS_DIFF_MOD_1':'#VALUE# год','FD_YEARS_DIFF_MOD_2_4':'#VALUE# года','FD_YEARS_DIFF_MOD_OTHER':'#VALUE# лет','CAL_BUTTON':'Выбрать','CAL_TIME_SET':'Установить время','CAL_TIME':'Время','FD_LAST_SEEN_TOMORROW':'завтра в #TIME#','FD_LAST_SEEN_NOW':'только что','FD_LAST_SEEN_TODAY':'сегодня в #TIME#','FD_LAST_SEEN_YESTERDAY':'вчера в #TIME#','FD_LAST_SEEN_MORE_YEAR':'более года назад'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'ADMIN_INCLAREA_DBLCLICK':'Двойной щелчок','ADMIN_SHOW_MODE_ON':'включен','ADMIN_SHOW_MODE_OFF':'выключен','AMDIN_SHOW_MODE_TITLE':'Режим правки','ADMIN_SHOW_MODE_ON_HINT':'Режим правки включен. На странице доступны элементы управления компонентами и включаемыми областями.','ADMIN_SHOW_MODE_OFF_HINT':'Режим правки выключен'});</script>
<script type="text/javascript">(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'DD.MM.YYYY','FORMAT_DATETIME':'DD.MM.YYYY HH:MI:SS','COOKIE_PREFIX':'BITRIX_SM','SERVER_TZ_OFFSET':'10800','SITE_ID':'s1','SITE_DIR':'/','USER_ID':'1','SERVER_TIME':'1516007001','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'391a1a78c5e0808db9c4928c4f11c92c'});</script><script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/twim.recaptchafree/script.js?15115207584421"></script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadRecaptchafree&render=explicit&hl=ru"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core.min.js?151558767377571"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_db.min.js?15115207598699"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_ajax.min.js?151558766521471"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/json/json2.min.js?15115207583467"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_ls.min.js?15115207597365"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_fx.min.js?15115207599768"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_frame_cache.min.js?151152075911191"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/session.min.js?15115207592511"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_window.min.js?151558766575173"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_popup.min.js?151558751540695"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_date.min.js?151558750737561"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/fileman/sticker.min.js?151152075948559"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/core/core_admin.min.js?151152075921881"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/utils.min.js?151152075919858"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/admin_tools.min.js?151152075845484"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/popup_menu.min.js?15115207599738"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/admin_search.min.js?15115207594820"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/hot_keys.min.js?151152075812333"></script>
<script type="text/javascript" src="//opt-1125008.ssl.1c-bitrix-cdn.ru/bitrix/js/main/public_tools.min.js?151152075924131"></script>

<script type="text/javascript">
    var phpVars = {
        'ADMIN_THEME_ID': '.default',
        'LANGUAGE_ID': 'ru',
        'FORMAT_DATE': 'DD.MM.YYYY',
        'FORMAT_DATETIME': 'DD.MM.YYYY HH:MI:SS',
        'opt_context_ctrl': false,
        'cookiePrefix': 'BITRIX_SM',
        'titlePrefix': 'Банк ЗЕНИТ - ',
        'bitrix_sessid': '391a1a78c5e0808db9c4928c4f11c92c',
        'messHideMenu': 'Скрыть меню',
        'messShowMenu': 'Показать меню',
        'messHideButtons': 'Уменьшить кнопки',
        'messShowButtons': 'Увеличить кнопки',
        'messFilterInactive': 'Поиск не используется - показаны все записи',
        'messFilterActive': 'Используется поиск - показаны только найденные записи',
        'messFilterLess': 'Скрыть условие поиска',
        'messLoading': 'Загрузка...',
        'messMenuLoading': 'Загрузка...',
        'messMenuLoadingTitle': 'Загружаются пункты меню...',
        'messNoData': '- Нет данных -',
        'messExpandTabs': 'Развернуть все вкладки на одну страницу',
        'messCollapseTabs': 'Свернуть вкладки',
        'messPanelFixOn': 'Зафиксировать панель на экране',
        'messPanelFixOff': 'Открепить панель',
        'messPanelCollapse': 'Скрыть панель',
        'messPanelExpand': 'Показать панель'
    };
</script>




<script type="text/javascript" src="/local/templates/bz/vendors/jquery.min.js?151205076495786"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/slick/slick.min.js?151205091141953"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.scroolly.js?151205076445188"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery-ui/jquery-ui.min.js?151205091034477"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.number.min.js?15120507646285"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.formstyler.min.js?151205076419287"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/baraja/modernizr.custom.79639.js?15120509109201"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/baraja/jquery.baraja.js?151205090917277"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/vivus/pathformer.js?15120509127787"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/vivus/vivus.js?151205091221977"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/smartbanner/_jquery.smartbanner.js?151205091116248"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/toggles/toggles.min.js?15120509122398"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.validate.min.js?151205076421090"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/additional-methods.min.js?151205076317229"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/localization/messages_ru.min.js?15120509101928"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/inputmask/jquery.inputmask.bundle.min.js?151205091086007"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/slider-pips/jquery-ui-slider-pips.js?151205091124533"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.ui.touch-punch.min.js?15120507641291"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.mousewheel.min.js?15120507641392"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.kinetic.min.js?15120507638372"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.smoothDivScroll-1.3.js?151205076446839"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.cookie.js?15120507633140"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jslider/jquery.slider-bundled.js?1512050910112731"></script>
<script type="text/javascript" src="/local/templates/bz/js/toggle.js?15120507621767"></script>
<script type="text/javascript" src="/local/templates/bz/js/city.js?15125736451671"></script>
<script type="text/javascript" src="/local/templates/bz/js/slider.js?15138707243874"></script>
<script type="text/javascript" src="/local/templates/bz/js/top_banner.js?1512050763171"></script>
<script type="text/javascript" src="/local/templates/bz/js/main.js?151574300015838"></script>
<script type="text/javascript" src="/local/templates/bz/js/custom.js?15120507622090"></script>
<script type="text/javascript" src="/local/templates/bz/js/form_search.js?1512050762550"></script>
<script type="text/javascript" src="/local/templates/bz/components/bitrix/form.result.new/contact/script.js?151205100911925"></script>
<script type="text/javascript" src="/local/templates/bz/components/bitrix/news.list/rates/script.js?1512051053207"></script>
<script type="text/javascript" src="/local/templates/bz/components/bitrix/form.result.new/reserve/script.js?15120510099884"></script>
<script type="text/javascript" src="/local/templates/bz/vendors/jquery.chained.min.js?15120507631035"></script>
<script type="text/javascript">
    bxSession.Expand(900, '391a1a78c5e0808db9c4928c4f11c92c', false, 'a57ba3eb4d6c6a077612810d7ad2cfbf');
</script>
<script type="text/javascript">var _ba = _ba || []; _ba.push(["aid", "084338cf6147abbfca438bf6bb2b3337"]); _ba.push(["host", "zenit.ru"]); (function() {var ba = document.createElement("script"); ba.type = "text/javascript"; ba.async = true;ba.src = (document.location.protocol == "https:" ? "https://" : "http://") + "bitrix.info/ba.js";var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ba, s);})();</script>


<script type="text/javascript">
    $(function() {
        $.smartbanner({
            title: 'ЗЕНИТ Онлайн',
            author: 'в мобильном удобнее',
            price: null,
            icon: '/local/templates/bz/img/icon/android-chrome-192.png',
            button: 'Открыть',
            scale: $('body').width() / window.screen.width,
            //scale: 2,
            speedIn: 300,
            speedOut: 400,
            daysHidden: 15,
            daysReminder: 90,
            force: null
        });
    });
</script>

<script type="text/javascript">BX.admin.dynamic_mode=false; BX.admin.dynamic_mode_show_borders = false;</script>
<script type="text/javascript">BX.message({MENU_ENABLE_TOOLTIP: false}); new BX.COpener({'DIV':'bx-panel-menu','ACTIVE_CLASS':'bx-pressed','MENU_URL':'/bitrix/admin/get_start_menu.php?lang=ru&back_url_pub=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&sessid=391a1a78c5e0808db9c4928c4f11c92c','MENU_PRELOAD':true});</script><script type="text/javascript"> BXHotKeys.Add("", "var d=BX(\"bx-panel-menu\"); if (d) d.click();", 91, 'Меню', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "var d=BX(\'bx-panel-admin-tab\'); if (d) location.href = d.href;", 92, 'Администрирование', 0); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+79", "var d=BX(\'bx-panel-logout\'); if (d) location.href = d.href;", 133, 'Выход пользователя из системы.', 19); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+68", "location.href=\"/businesses/rko/account/online-reserve/?bitrix_include_areas=Y\";", 117, 'Режим правки', 16); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+69", "var d=BX(\'bx-panel-expander\'); if (d) BX.fireEvent(d, \'click\');", 118, 'Развернуть/Свернуть', 21); </script>
<script type="text/javascript"> BXHotKeys.Add("", "", 111, '<b>Создать страницу</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=bz&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 60, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Создать страницу', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Создать страницу','TEXT':'Создание страниц с помощью мастера.'}, GROUP_ID : 0, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_menu', TYPE: 'BIG', MENU: [{'TEXT':'Создать страницу','TITLE':'Мастер создания новой страницы','ICON':'panel-new-file','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=bz&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','DEFAULT':true,'SORT':'10','HK_ID':'top_panel_create_page'},{'TEXT':'По шаблону','TITLE':'Мастер создания страницы по выбранному шаблону','ICON':'panel-new-file-template','MENU':[{'TEXT':'Стандартная страница','TITLE':'Шаблон standard.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?page_template=standard.php&lang=ru&site=s1&templateID=bz&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'TEXT':'Включаемая область для страницы','TITLE':'Шаблон inc.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?page_template=inc.php&lang=ru&site=s1&templateID=bz&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'}],'SORT':'20'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Создать новую страницу','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_html_edit.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&new=Y&templateID=bz&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Создать страницу','TEXT':'Нажмите на эту кнопку, чтобы запустить мастер или шаблон создания страниц или включаемых областей для страниц. С помощью этих мастеров, можно создать страницу или включаемую область, последовательно внося данные на каждом шаге мастера.'}, GROUP_ID : 0, TEXT :  "Создать страницу"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 112, '<b>Создать раздел</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=bz&newFolder=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 62, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Создать раздел', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_section', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Создать раздел','TEXT':'Создание разделов с помощью мастера.'}, GROUP_ID : 0, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_create_section_menu', TYPE: 'BIG', MENU: [{'TEXT':'Создать раздел','TITLE':'Мастер создания нового раздела','ICON':'panel-new-folder','DEFAULT':true,'ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?lang=ru&site=s1&templateID=bz&newFolder=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'10','HK_ID':'top_panel_create_folder'},{'TEXT':'По шаблону','TITLE':'Мастер создания нового раздела с индексной страницей по шаблону','ICON':'panel-new-folder-template','MENU':[{'TEXT':'Форум (ЧПУ)','TITLE':'Шаблон forum\nСтраница с форумами в режиме ЧПУ','ICON':'','IMAGE':'/bitrix/themes/.default/start_menu/forum/forum.gif','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&wiz_template=forum&lang=ru&site=s1&templateID=bz&newFolder=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'SEPARATOR':true},{'TEXT':'Стандартная страница','TITLE':'Шаблон standard.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&page_template=standard.php&lang=ru&site=s1&templateID=bz&newFolder=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'},{'TEXT':'Включаемая область для страницы','TITLE':'Шаблон inc.php','ICON':'panel-new-file-template','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_new.php?newFolder=Y&page_template=inc.php&lang=ru&site=s1&templateID=bz&newFolder=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()'}],'SORT':'20'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Создать новый раздел','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_newfolder.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F\')'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Создать раздел','TEXT':'Нажмите на эту кнопку, чтобы запустить мастер или шаблон создания разделов или включаемых областей для разделов. С помощью этих мастеров, можно создать страницу или включаемую область, последовательно внося данные на каждом шаге мастера.'}, GROUP_ID : 0, TEXT :  "Создать раздел"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 107, '<b>Изменить страницу</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&templateID=bz&siteTemplateId=bz\',\'width\':\'1169\',\'height\':\'452\',\'min_width\':\'780\',\'min_height\':\'400\'})).Show()", 63, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В визуальном редакторе', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_property.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 64, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заголовок и свойства страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&noeditor=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'1169\',\'height\':\'452\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 65, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В режиме HTML-кода', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&templateID=bz&siteTemplateId=bz\',\'width\':\'1456\',\'height\':\'458\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 67, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В режиме PHP-кода', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_delete.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&siteTemplateId=bz\',\'width\':\'440\',\'height\':\'180\',\'min_width\':\'250\',\'min_height\':\'180\'})).Show()", 68, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Удалить страницу', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Изменить страницу','TEXT':'Изменение содержимого страницы.'}, GROUP_ID : 1, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_menu', TYPE: 'BIG', MENU: [{'TEXT':'В визуальном редакторе','TITLE':'Редактировать страницу в визуальном редакторе','ICON':'panel-edit-visual','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&templateID=bz&siteTemplateId=bz\',\'width\':\'1169\',\'height\':\'452\',\'min_width\':\'780\',\'min_height\':\'400\'})).Show()','DEFAULT':true,'SORT':'10','HK_ID':'top_panel_edit_page'},{'TEXT':'Заголовок и свойства страницы','TITLE':'Изменить заголовок и другие свойства страницы','ICON':'panel-file-props','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_property.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'20','HK_ID':'top_panel_page_prop'},{'SEPARATOR':true,'SORT':'29'},{'TEXT':'Доступ к странице','TITLE':'Установить права доступа к странице','ICON':'panel-file-access','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'30','HK_ID':'top_panel_access_page_new'},{'ID':'delete','ICON':'icon-delete','ALT':'Удалить страницу','TEXT':'Удалить страницу','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_file_delete.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&siteTemplateId=bz\',\'width\':\'440\',\'height\':\'180\',\'min_width\':\'250\',\'min_height\':\'180\'})).Show()','SORT':'40','HK_ID':'top_panel_del_page'},{'SEPARATOR':true,'SORT':'49'},{'TEXT':'В режиме HTML-кода','TITLE':'Редактировать страницу в режиме HTML','ICON':'panel-edit-text','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit.php?lang=ru&noeditor=Y&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'1169\',\'height\':\'452\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()','SORT':'50','HK_ID':'top_panel_edit_page_html'},{'TEXT':'В режиме PHP-кода','TITLE':'Редактировать полный PHP-код страницы','ICON':'panel-edit-php','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&templateID=bz&siteTemplateId=bz\',\'width\':\'1456\',\'height\':\'458\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()','SORT':'60','HK_ID':'top_panel_edit_page_php'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Редактировать текущую страницу','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_html_edit.php?lang=ru&site=s1&templateID=bz&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Изменить страницу','TEXT':'Нажмите на эту кнопку, чтобы вызвать диалог редактирования страницы, в том числе через документооборот, просмотреть историю изменений страницы, изменить свойства страницы, ограничить доступ к ней.'}, GROUP_ID : 1, TEXT :  "Изменить страницу"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 96, '<b>Изменить раздел</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "javascript:(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_folder_edit.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 69, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Свойства раздела', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()", 70, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Доступ к разделу', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_section', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Изменить раздел','TEXT':'Изменение свойств раздела.'}, GROUP_ID : 1, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_edit_section_menu', TYPE: 'BIG', MENU: [{'TEXT':'Свойства раздела','TITLE':'Изменить название и другие свойства раздела','ICON':'panel-folder-props','DEFAULT':true,'ACTION':'javascript:(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_folder_edit.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'10','HK_ID':'top_panel_folder_prop'},{'TEXT':'Доступ к разделу','TITLE':'Установить права доступа к разделу','ICON':'panel-folder-access','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_access_edit.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\',\'min_width\':\'450\',\'min_height\':\'250\'})).Show()','SORT':'30','HK_ID':'top_panel_access_folder_new'},{'SEPARATOR':true,'SORT':'99'},{'TEXT':'В панели управления','TITLE':'Редактировать свойства раздела','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_folder.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F\')','SORT':'100'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Изменить раздел','TEXT':'Нажмите на эту кнопку, чтобы изменить свойства раздела и ограничить доступ к нему.'}, GROUP_ID : 1, TEXT :  "Изменить раздел"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_menus', TYPE: 'SMALL', MENU: [{'TEXT':'Редактировать \"Верхняя линия слева\"','TITLE':'Редактировать пункты меню \"Верхняя линия слева\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2F&name=toplineleft&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"Верхняя линия справа\"','TITLE':'Редактировать пункты меню \"Верхняя линия справа\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2F&name=toplineright&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"Главное меню\"','TITLE':'Редактировать пункты меню \"Главное меню\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2Fbusinesses%2F&name=top&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Редактировать \"Нижнее меню\"','TITLE':'Редактировать пункты меню \"Нижнее меню\"','SORT':'100','ICON':'menu-edit','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2F&name=bottom&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'SEPARATOR':'Y','SORT':'150'},{'TEXT':'Создать \"Верхняя линия слева\"','TITLE':'Создать меню \"Верхняя линия слева\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve&name=toplineleft&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"Верхняя линия справа\"','TITLE':'Создать меню \"Верхняя линия справа\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve&name=toplineright&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"Главное меню\"','TITLE':'Создать меню \"Главное меню\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve&name=top&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false},{'TEXT':'Создать \"Нижнее меню\"','TITLE':'Создать меню \"Нижнее меню\" в текущем разделе','SORT':'200','ICON':'menu-add','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_menu_edit.php?new=Y&lang=ru&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve&name=bottom&siteTemplateId=bz\',\'width\':\'\',\'height\':\'\'})).Show()','DEFAULT':false}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Меню','TEXT':'Вызов формы редактирования меню. Нажмите на стрелку, чтобы отредактировать все меню данной страницы или создать новое меню.','TARGET':'parent'}, GROUP_ID : 2, TEXT :  "Меню"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 97, '<b>Структура</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F\')", 71, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;В панели управления', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_structure', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Структура','TEXT':'Вызов диалога Структура сайта с возможностью добавления, редактирования, перемещения, удаления, изменения свойств разделов и страниц.','TARGET':'parent'}, GROUP_ID : 2, SKIP : true, LINK: "javascript:void(0)", ACTION : "(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_structure.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&templateID=bz&siteTemplateId=bz\',\'width\':\'350\',\'height\':\'470\'})).Show()",TEXT :  "Структура" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_structure_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Управление структурой','TITLE':'Управление структурой сайта','ACTION':'(new BX.CDialog({\'content_url\':\'/bitrix/admin/public_structure.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&templateID=bz&siteTemplateId=bz\',\'width\':\'350\',\'height\':\'470\'})).Show()','DEFAULT':true,'HK_ID':'main_top_panel_struct'},{'SEPARATOR':true},{'TEXT':'В панели управления','TITLE':'Перейти в управление структурой в административном разделе','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F\')','HK_ID':'main_top_panel_struct_panel'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 2, TEXT :  "Структура"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_seo', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'SEO','TEXT':'Вызов инструментов оптимизации сайта с целью повышения положения сайта в выдаче поисковых машин.'}, GROUP_ID : 2, SKIP : false, LINK: "javascript:void(0)", ACTION : "(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/public_seo_tools.php?lang=ru&bxpublic=Y&from_module=seo&site=s1&path=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2Findex.php&title_final=0J7QvdC70LDQudC9LdGA0LXQt9C10YDQstC40YDQvtCy0LDQvdC40LUg0L3QvtC80LXRgNCwINGB0YfQtdGC0LA%3D&title_changer_name=&title_changer_link=&title_win_final=&title_win_changer_name=&title_win_changer_link=&sessid=391a1a78c5e0808db9c4928c4f11c92c&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'920\',\'height\':\'400\'})).Show()",TEXT :  "SEO" })</script><script type="text/javascript"> BXHotKeys.Add("", "", 98, '<b>Сбросить кеш</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "BX.clearCache()", 72, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Обновить кеш страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?clear_cache_session=Y\');", 74, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Не использовать кеш', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_0', TYPE: 'BIG', ACTIVE_CSS: 'bx-panel-button-icon-active', HOVER_CSS: 'bx-panel-button-icon-hover', HINT: {'TITLE':'Сбросить кеш','TEXT':'Обновление кеша страницы.'}, GROUP_ID : 3, SKIP : true }); BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_0_menu', TYPE: 'BIG', MENU: [{'TEXT':'Обновить кеш страницы','TITLE':'Обновить закешированные данные текущей страницы','ICON':'panel-page-cache','ACTION':'BX.clearCache()','DEFAULT':true,'HK_ID':'top_panel_cache_page'},{'SEPARATOR':true},{'TEXT':'Не использовать кеш','TITLE':'Отключить кеширование для текущего сеанса пользователя','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?clear_cache_session=Y\');','HK_ID':'top_panel_cache_not'}], ACTIVE_CSS: 'bx-panel-button-text-active', HOVER_CSS: 'bx-panel-button-text-hover', HINT: {'TITLE':'Сбросить кеш','TEXT':'Нажмите на эту кнопку, чтобы обновить кеш страницы или компонентов, или отключить кеш для данной страницы вообще.'}, GROUP_ID : 3, TEXT :  "Сбросить кеш"})</script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], BX(\'bx-panel-toggle\').href);", 75, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Перейти в режим правки', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_components', TYPE: 'SMALL', MENU: [{'TEXT':'Перейти в режим правки','TITLE':'Перейдите в режим правки для отображения списка компонентов','ACTION':'jsUtils.Redirect([], BX(\'bx-panel-toggle\').href);','HK_ID':'top_panel_edit_mode'}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Компоненты','TEXT':'Выбор компонента для редактирования его параметров.','TARGET':'parent'}, GROUP_ID : 4, TEXT :  "Компоненты"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 99, '<b>Шаблон сайта</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fbz%2Fstyles.css&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'1456\',\'height\':\'458\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 76, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изменить стили сайта', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fbz%2Ftemplate_styles.css&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'1456\',\'height\':\'458\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()", 77, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изменить стили шаблона', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/site_edit.php?lang=ru&LID=s1\')", 78, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Редактировать сайт', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_1', TYPE: 'SMALL', MENU: [{'TEXT':'Изменить стили сайта','TITLE':'Редактировать основные стили сайта (styles.css)','ICON':'panel-edit-text','HK_ID':'top_panel_templ_site_css','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fbz%2Fstyles.css&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'1456\',\'height\':\'458\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()'},{'TEXT':'Изменить стили шаблона','TITLE':'Редактировать стили шаблона (template_styles.css)','ICON':'panel-edit-text','HK_ID':'top_panel_templ_templ_css','ACTION':'(new BX.CEditorDialog({\'content_url\':\'/bitrix/admin/public_file_edit_src.php?lang=ru&path=%2Flocal%2Ftemplates%2Fbz%2Ftemplate_styles.css&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'1456\',\'height\':\'458\',\'min_width\':\'700\',\'min_height\':\'400\'})).Show()'},{'SEPARATOR':'Y'},{'TEXT':'В панели управления','MENU':[{'TEXT':'Редактировать шаблон','TITLE':'Редактировать шаблон сайта в панели управления','ICON':'icon-edit','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/template_edit.php?lang=ru&ID=bz\')','DEFAULT':false,'HK_ID':'top_panel_templ_edit'},{'TEXT':'Редактировать сайт','TITLE':'Редактировать настройки сайта в панели управления','ICON':'icon-edit','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/site_edit.php?lang=ru&LID=s1\')','DEFAULT':false,'HK_ID':'top_panel_templ_site'}]}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Шаблон сайта','TEXT':'Вызов форм редактирования стилей сайта и стилей шаблона.','TARGET':'parent'}, GROUP_ID : 4, TEXT :  "Шаблон сайта"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 100, '<b>Отладка</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_sql_stat=Y\');", 83, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика SQL-запросов', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_include_exec_time=Y\');", 82, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика включаемых областей', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_page_exec_time=Y\');", 81, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Время исполнения страницы', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?compress=Y\');", 84, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Статистика компрессии', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_2', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Отладка','TEXT':'Включение режима отображения суммарной статистики. Нажмите на стрелку, чтобы Просмотреть статистику запросов базы данных, статистику включаемых областей, время исполнение страницы и статистику компрессии.','TARGET':'parent'}, GROUP_ID : 4, SKIP : true, LINK: "/businesses/rko/account/online-reserve/?show_page_exec_time=Y&show_include_exec_time=Y&show_sql_stat=Y", ACTION : "",TEXT :  "Отладка" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_2_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Суммарная статистика','TITLE':'Показывать статистику SQL-запросов, включаемых областей, времени исполнения','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_page_exec_time=Y&show_include_exec_time=Y&show_sql_stat=Y\');','DEFAULT':true,'HK_ID':'top_panel_debug_summ'},{'SEPARATOR':true},{'TEXT':'Статистика SQL-запросов','TITLE':'Показывать статистику SQL-запросов, исполняющихся на странице','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_sql_stat=Y\');','HK_ID':'top_panel_debug_sql'},{'TEXT':'Детальная статистика кеша','TITLE':'Внимание! Может сильно влиять на время исполнения страницы.','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_cache_stat=Y\');','HK_ID':'top_panel_debug_cache'},{'TEXT':'Статистика включаемых областей','TITLE':'Показывать статистику включаемых областей (время, запросы)','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_include_exec_time=Y\');','HK_ID':'top_panel_debug_incl'},{'TEXT':'Время исполнения страницы','TITLE':'Показывать время исполнения страницы','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_page_exec_time=Y\');','HK_ID':'top_panel_debug_time'},{'SEPARATOR':true},{'TEXT':'Статистика компрессии','TITLE':'Показывать статистику модуля компрессии','CHECKED':false,'ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?compress=Y\');','HK_ID':'top_panel_debug_compr'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 4, TEXT :  "Отладка"})</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_3', TYPE: 'SMALL', MENU: [{'TEXT':'График посещаемости страницы','TITLE':'График посещаемости страницы','IMAGE':'/bitrix/images/statistic/page_traffic.gif','ACTION':'javascript:window.open(\'/bitrix/admin/section_graph_list.php?lang=ru&public=Y&width=650&height=650&section=https%3A%2F%2Fzenit.ru%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&set_default=Y\',\'\',\'target=_blank,scrollbars=yes,resizable=yes,width=650,height=650,left=\'+Math.floor((screen.width - 650)/2)+\',top=\'+Math.floor((screen.height- 650)/2))'},{'TEXT':'Показать статистику переходов по ссылкам с данной страницы','TITLE':'Показать статистику переходов по ссылкам с данной страницы','IMAGE':'/bitrix/images/statistic/link_stat_show.gif','ACTION':'jsUtils.Redirect([], \'/businesses/rko/account/online-reserve/?show_link_stat=Y\')'}], ACTIVE_CSS: 'bx-panel-small-button-active', HOVER_CSS: 'bx-panel-small-button-hover', HINT: {'TITLE':'Статистика','TEXT':'Просмотр графика посещаемости страницы или статистики переходов.','TARGET':'parent'}, GROUP_ID : 5, TEXT :  "Статистика"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 101, '<b>Короткий URL</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "javascript:(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'770\',\'height\':\'270\'})).Show()", 85, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Добавить короткую ссылку', 0); </script><script type="text/javascript"> BXHotKeys.Add("", "jsUtils.Redirect([], \'/bitrix/admin/short_uri_admin.php?lang=ru\');", 86, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список ссылок', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_4', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Короткая ссылка','TEXT':'Настройка короткой ссылки для текущей страницы','TARGET':'parent'}, GROUP_ID : 5, SKIP : true, LINK: "javascript:void(0)", ACTION : "(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'770\',\'height\':\'270\'})).Show()",TEXT :  "Короткий URL" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_4_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Добавить короткую ссылку','TITLE':'Добавить короткую ссылку на текущую страницу','ACTION':'javascript:(new BX.CAdminDialog({\'content_url\':\'/bitrix/admin/short_uri_edit.php?lang=ru&public=Y&bxpublic=Y&str_URI=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&site=s1&back_url=%2Fbusinesses%2Frko%2Faccount%2Fonline-reserve%2F&siteTemplateId=bz\',\'width\':\'770\',\'height\':\'270\'})).Show()','DEFAULT':true,'HK_ID':'MTP_SHORT_URI1'},{'TEXT':'Список ссылок','TITLE':'Список коротких ссылок','ACTION':'jsUtils.Redirect([], \'/bitrix/admin/short_uri_admin.php?lang=ru\');','HK_ID':'MTP_SHORT_URI_LIST'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 5, TEXT :  "Короткий URL"})</script><script type="text/javascript"> BXHotKeys.Add("", "", 102, '<b>Стикеры</b>', 0); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+83", "if (window.oBXSticker){window.oBXSticker.AddSticker();}", 87, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Наклеить стикер', 22); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+88", "if (window.oBXSticker){window.oBXSticker.ShowAll();}", 88, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Показать стикеры (0)', 23); </script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Shift+76", "if (window.oBXSticker){window.oBXSticker.ShowList(\'current\');}", 89, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список стикеров страницы', 24); </script><script type="text/javascript"> BXHotKeys.Add("", "if (window.oBXSticker){window.oBXSticker.ShowList(\'all\');}", 90, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Список всех стикеров', 0); </script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_5', TYPE: 'SMALL', ACTIVE_CSS: 'bx-panel-small-button-text-active', HOVER_CSS: 'bx-panel-small-button-text-hover', HINT: {'TITLE':'Стикеры','TEXT':'Наклеить новый стикер на страницу.','TARGET':'parent'}, GROUP_ID : 5, SKIP : true, LINK: "javascript:void(0)", ACTION : "if (window.oBXSticker){window.oBXSticker.AddSticker();}",TEXT :  "Стикеры" })</script><script type="text/javascript">BX.admin.panel.RegisterButton({ID: 'bx_topmenu_btn_5_menu', TYPE: 'SMALL', MENU: [{'TEXT':'Наклеить стикер','TITLE':'Наклеить новый стикер на страницу ( Ctrl+Shift+S ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.AddSticker();}','DEFAULT':true,'HK_ID':'FMST_PANEL_STICKER_ADD'},{'SEPARATOR':true},{'ID':'bxst-show-sticker-icon','TEXT':'Показать стикеры (0)','TITLE':'Отобразить все стикеры на странице ( Ctrl+Shift+X ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowAll();}','HK_ID':'FMST_PANEL_STICKERS_SHOW'},{'TEXT':'Список стикеров страницы','TITLE':'Показать список всех стикеров на текущей странице ( Ctrl+Shift+L ) ','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowList(\'current\');}','HK_ID':'FMST_PANEL_CUR_STICKER_LIST'},{'TEXT':'Список всех стикеров','TITLE':'Показать список всех стикеров на сайте','ICON':'','ACTION':'if (window.oBXSticker){window.oBXSticker.ShowList(\'all\');}','HK_ID':'FMST_PANEL_ALL_STICKER_LIST'}], ACTIVE_CSS: 'bx-panel-small-button-arrow-active', HOVER_CSS: 'bx-panel-small-button-arrow-hover', GROUP_ID : 5, TEXT :  "Стикеры"})</script><script type="text/javascript">
    BX.admin.panel.state = {
        fixed: true,
        collapsed: true
    };
    BX.admin.moreButton.init({ buttonTitle : "Еще"});
</script><script type="text/javascript"> BXHotKeys.Add("Ctrl+Alt+75", "location.href=\'/bitrix/admin/hot_keys_list.php?lang=ru\';", 106, 'Перейти в раздел управления горячими клавишами', 18);  BXHotKeys.Add("Ctrl+Alt+80", "BXHotKeys.ShowSettings();", 17, 'Список горячих клавиш на странице', 14);  BXHotKeys.Add("Ctrl+Alt+85", "location.href=\'/bitrix/admin/user_admin.php?lang=\'+phpVars.LANGUAGE_ID;", 139, 'Перейти в раздел управления пользователями', 13); </script><script type='text/javascript'>
    BXHotKeys.MesNotAssign = 'не назначена';
    BXHotKeys.MesClToChange = 'Кликните мышкой, чтобы изменить';
    BXHotKeys.MesClean = 'Очистить';
    BXHotKeys.MesBusy = 'Данная комбинация клавиш уже задействована. Вы уверены, что хотите её использовать повторно?';
    BXHotKeys.MesClose = 'Закрыть';
    BXHotKeys.MesSave = 'Сохранить';
    BXHotKeys.MesSettings = 'Настройка горячих клавиш';
    BXHotKeys.MesDefault = 'По умолчанию';
    BXHotKeys.MesDelAll = 'Удалить все';
    BXHotKeys.MesDelConfirm = 'Вы уверены, что хотите удалить все назначенные сочетания горячих клавиш?';
    BXHotKeys.MesDefaultConfirm = 'Вы уверены, что хотите удалить все назначенные сочетания горячих клавиш и установить сочетания горячих клавиш по умолчанию?';
    BXHotKeys.MesExport = 'Экспорт';
    BXHotKeys.MesImport = 'Импорт';
    BXHotKeys.MesExpFalse = 'Экспортировать горячие клавиши не удалось.';
    BXHotKeys.MesImpFalse = 'Импортировать горячие клавиши не удалось.';
    BXHotKeys.MesImpSuc = 'Успешно импортировано горячих клавиш: ';
    BXHotKeys.MesImpHeader = 'Импорт горячих клавиш';
    BXHotKeys.MesFileEmpty = 'Для импорта горячих клавиш необходимо указать файл.';
    BXHotKeys.MesDelete = 'Удалить';
    BXHotKeys.MesChooseFile = 'Выбрать файл';
    BXHotKeys.uid = 1;
</script><script type="text/javascript">BX.ready(function(){var BXST_MESS =
        {
            Public : "общий",
            Personal : "личный",
            Close : "Отклеить",
            Collapse : "Свернуть",
            UnCollapse : "Восстановить",
            UnCollapseTitle : "Двойной клик для восстановления стикера",
            SetMarkerArea : "Привязать к области на странице",
            SetMarkerEl : "Привязать к элементу на странице",
            Color : "цвет",
            Add : "добавить",
            PersonalTitle : "Только вы можете видеть этот стикер. Нажмите, чтобы изменить.",
            PublicTitle : "Этот стикер могут видеть другие. Нажмите, чтобы изменить.",
            CursorHint : "Выделите область",
            Yellow : "Желтый",
            Green : "Зеленый",
            Blue : "Синий",
            Red : "Красный",
            Purple : "Пурпурный",
            Gray : "Серый",
            StickerListTitle : "Список стикеров",
            CompleteLabel : "выполнено",
            DelConfirm : "Вы уверены, что хотите удалить выбранные стикеры?",
            CloseConfirm : "Этот стикер наклеил #USER_NAME# Вы уверенны, что хотите его отклеить?",
            Complete : "Пометить как завершенное",
            CloseNotify : "Вы можете вернуть отклеенные стикеры на экран из #LINK#списка стикеров#LINK#."
        }; window.oBXSticker = new BXSticker({'access':'W','sessid_get':'sessid=391a1a78c5e0808db9c4928c4f11c92c','start_width':'350','start_height':'200','min_width':'280','min_height':'160','start_color':'0','zIndex':'5000','curUserName':'admin','curUserId':'1','pageUrl':'/businesses/rko/account/online-reserve/','pageTitle':'Онлайн-резервирование номера счета','bShowStickers':false,'listWidth':'800','listHeight':'450','listNaviSize':'5','useHotkeys':false,'filterParams':{'type':'all','colors':'all','status':'opened','page':'all'},'bHideBottom':true,'focusOnSticker':'0','strDate':'15 Января','curPageCount':'0','site_id':'s1'}, [], BXST_MESS);});</script>
<script type="text/javascript">
    function hideMpAnswer(el, module)
    {
        if(el.parentNode.parentNode.parentNode)
            BX.hide(el.parentNode.parentNode.parentNode);
        BX.ajax({
            'method': 'POST',
            'dataType': 'html',
            'url': '/bitrix/admin/partner_modules.php',
            'data': 'module='+module+'&sessid=391a1a78c5e0808db9c4928c4f11c92c&act=unnotify',
            'async': true,
            'processData': false

        });
    }
</script>
<script type="text/javascript">
    function hideMpAnswer(el, module)
    {
        if(el.parentNode.parentNode.parentNode)
            BX.hide(el.parentNode.parentNode.parentNode);
        BX.ajax({
            'method': 'POST',
            'dataType': 'html',
            'url': '/bitrix/admin/partner_modules.php',
            'data': 'module='+module+'&sessid=391a1a78c5e0808db9c4928c4f11c92c&act=unnotify',
            'async': true,
            'processData': false

        });
    }
</script>
<script type="text/javascript">
    function hideMpAnswer(el, module)
    {
        if(el.parentNode.parentNode.parentNode)
            BX.hide(el.parentNode.parentNode.parentNode);
        BX.ajax({
            'method': 'POST',
            'dataType': 'html',
            'url': '/bitrix/admin/partner_modules.php',
            'data': 'module='+module+'&sessid=391a1a78c5e0808db9c4928c4f11c92c&act=unnotify',
            'async': true,
            'processData': false

        });
    }
</script>
<script type="text/javascript">
    BX.ready( function(){BX.adminInformer.Init(4); } );
</script><script async src="https://www.googletagmanager.com/gtag/js?id=UA-110204788-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-110204788-1');
</script><script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46786158 = new Ya.Metrika({
                    id:46786158,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
</body>
