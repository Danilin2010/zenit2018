var SendingForm = (function () {
    'use strict';

    var self = {};

    var captchaResponse = null;
    var sendCode = false;

    function onEvent() {
        $('[data-setcode]').on('click', self.code);
    }


    /**
     *
     * @param captcha response каптчи
     */
    self.code = function () {
        var $form = $("form[name=account_number]");
        var $mail = $form.find('[name="mailbox"]');
        var domain = 'https://q3.zenit.ru/wzr_accreserve/checkemailaddress';


        var mail = $mail.val();
        $mail.prop('readonly', true).addClass('disabled');

        $('[data-server-error]').addClass('hidden');


        if ($form.valid()) {
            $.ajax({
                type: 'post',
                url: domain,
                data: '<form1><mail>' + mail + '</mail><response>' + captchaResponse + '</response></form1>',
                dataType: 'json',
                success: function (msg) {
                    if (msg == null) {
                        $('[server-error="2"]').removeClass('hidden');
                    } else {
                        switch (msg.return_status.return_code) {
                            case -1:
                                $('[data-server-error]').text(msg.return_status.return_text).removeClass('hidden');
                                break;
                            case 0:
                                //TODO: показать кнопку "Зарезервировать счет";
                                $("[data-setcode]").addClass('hidden');
                                $("#recaptcha").addClass('hidden');
                                $("[data-block-confirmation]").removeClass('confirmation_block');
                                sendCode = true;
                                break;
                            case 1:
                                $('[data-server-error]').text(msg.return_status.return_text).show();
                                break;
                            case 2:
                                $('[server-error="2"]').removeClass('hidden');
                                break;
                        }
                    }
                },
                error: function () {
                    alert('Извините, сервис временно недоступен.');
                },
            });

        }


    };


    self.confirm = function (form) {
        var domain = 'https://q3.zenit.ru/wzr_accreserve/reserveaccnumber';
        var type = $('[name="documentType"]').val();

        if (type == 'ИП') {
            var fields = ['documentType', 'city', 'phone', 'comments', 'mailbox', 'mailCode', 'ip_full', 'ip_short', 'ip_inn', 'ogrnip', 'revenues_ip'];
        } else {
            var fields = ['documentType', 'city', 'company', 'company_short', 'justice_type', 'inn', 'ogrn', 'work_type', 'revenues', 'phone', 'mailbox', 'comments', 'mailCode'];
        }

        var xml = "<reserve>";

        form.serializeArray().forEach(function (o) {
            if ($.inArray(o.name, fields) != -1) {
                xml += "<" + o.name + ">" + o.value + "</" + o.name + ">";
            }
        });

        xml += '<office>' + $('[name="office"] option:selected').val() + '|' + $('[name="office"] option:selected').text() + '</office>';
        xml += '</reserve>';

        console.log(xml);

        $.ajax({
            type: 'post',
            url: domain,
            data: xml,
            dataType: 'json',
            success: function (msg) {
                if (msg == null) {
                    $('[server-error="4"]').removeClass('hidden');
                }
                else {
                    var text = msg.return_status.return_text;
                    switch (msg.return_status.return_code) {
                        case -1:
                            $('[server-error="2"]').removeClass('hidden');
                            break;
                        case 0:
                            $('[data-server-error="1"]').text(text).removeClass('hidden');
                            break;
                        case 1:
                            $('[server-error="2"]').removeClass('hidden');
                            break;
                        case 2:
                            if (text != 0) {
                                $('[data-server-error="3"]').find('span').text(text).removeClass('hidden');
                            }
                            else {
                                $('[data-server-error="1"]')
                                    .text('Лимит попыток ввода неверного кода подтверждения исчерпан. Повторное обращение к сервису возможно через 15 минут.')
                                    .removeClass('hidden');
                            }
                            break;
                    }
                }
            },
            error: function () {
                $('[data-server-error="1"]').text('Извините, сервис временно недоступен.').removeClass('hidden');
            }
        });
    };

    self.verifyCallback = function (response) {
        if (response != null) {
            captchaResponse = response;

            $("[data-setcode]").removeClass('hidden');
        }
    };


    self.hasSentCode = function () {
        return sendCode;
    }

    onEvent();
    return self;

})();


$(document).ready(function () {

    var left_to_step = '100%';
    var first_to_step = '0px';
    var timer = 500;
    var padding = 40;
    padding = 0;

    function MarginResize() {
        var to_step = $('.to_step');
        var first_step = $('.first_step');
        var position_to_step = $('.wr_form_application').offset();
        var to_step_ = $(window).width() - position_to_step.left - padding + 100;
        left_to_step = to_step_ + 'px';
        first_to_step = '-' + (position_to_step.left + first_step.width() - padding) + 'px';
        if (!to_step.hasClass('win'))
            to_step.css('margin-left', left_to_step);
        if (first_step.hasClass('win'))
            first_step.css('left', first_to_step);
    }

    setTimeout(function () {
        MarginResize();
    }, 500);

    $(window).resize(function () {
        MarginResize();
    });


    function ChekType(active) {
        if (active) {
            $("[data-type-line=legal]").show();
            $("[data-type-line=ip]").hide();
        } else {
            $("[data-type-line=legal]").hide();
            $("[data-type-line=ip]").show();
        }
    }

    $("[data-toggle-type]").on('toggle', function (e, active) {
        var name = $(this).data('name');
        var N = $(this).data('checkedN');
        var Y = $(this).data('checkedY');
        if (active)
            $('input[name=' + name + ']').val(Y);
        else
            $('input[name=' + name + ']').val(N);
        ChekType(active);
    });

    ChekType($("[data-toggle-type]").data('toggles').active);


    function TriggerForm() {
        $('#select_form_text_43, #select_form_text_44').trigger('refresh');
        $('#select_form_text_43, #select_form_text_44').each(function () {
            var title = $(this).data('title');
            if (typeof (title) != 'undefined')
                $(this).next('.jq-selectbox__select').prepend('<div class="select_title">' + title + '</div>');
            else
                $(this).next('.jq-selectbox__select').prepend('<div class="select_title"></div>');
            var id = $(this).data('id');
            var val = $(this).find('option:selected').text();
            $('#' + id).val(val);
        });
    }

    $("#select_form_text_44").chained("#select_form_text_43");
    $(document).on('change', '#select_form_text_43, #select_form_text_44', function () {
        TriggerForm();
    });

    TriggerForm();

    $('input[name="mailbox]').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
        showMaskOnFocus: false,
        showMaskOnHover: false,
        'placeholder': ''
    });

    $('input[name="phone"]').inputmask({
        mask: "+7 (999) 999-99-99",
        showMaskOnHover: false,
        //showMaskOnFocus: false,
        placeholder: '+7 (   )    -  -  '
    });

    $('input[name="ogrn"]').inputmask({
        mask: "9",
        repeat: 13,
        greedy: false,
        showMaskOnHover: false,
        clearIncomplete: true,
        placeholder: ''
    });

    $('input[name="ip_inn"]').inputmask({
        mask: "9",
        repeat: 12,
        greedy: false,
        showMaskOnHover: false,
        clearIncomplete: true,
        placeholder: ''
    });

    $('input[name="inn"]').inputmask({
        mask: "9",
        repeat: 10,
        greedy: false,
        showMaskOnHover: false,
        clearIncomplete: true,
        placeholder: ''
    });


    $('input[name="eserve.ogrnip"]').inputmask({
        mask: "9",
        repeat: 15,
        greedy: false,
        showMaskOnHover: false,
        clearIncomplete: true,
        placeholder: ''
    });


    $('.snippet-online-reserve-chek input[type=checkbox]').on('change', function (e) {
        if ($(this).prop('checked')) {
            $('.online-reserve-applicatio').stop().slideDown();
        } else {
            $('.online-reserve-applicatio').stop().slideUp();
        }
    });

    $.validator.addMethod("type_ip", function (value, element) {
        return value.length || $('[name="documentType"]').val() == 'ЮЛ';
    }, "Это поле необходимо заполнить.");

    $.validator.addMethod("type_legal", function (value, element) {
        return value.length || $('[name="documentType"]').val() == 'ИП';
    }, "Это поле необходимо заполнить.");

    function addCity() {
        var data = '<option value="">Выбрать город</option>';
        reserve_city.forEach(function (o) {
            data += '<option value="' + o.branch + '">' + o.name + '</option>';
        });

        $('[name="city"]').html(data);
        TriggerForm();
    }

    function addOffice(office) {
        var data = '<option value="">Выбрать офис</option>';
        office.forEach(function (o) {
            data += '<option value="' + o.code + '">' + o.name + '</option>';
        });

        $('[name="office"]').html(data);
        $('[name="office"]').prop('disabled', false);
        TriggerForm();
    }


    addCity();

    $('[name="city"]').on('change', function () {
        var citySelect = $('[name="city"] option:selected').text();

        var office = null;
        reserve_city.forEach(function (o) {
            if (o.name == citySelect) {
                office = o.office;
            }
        });

        addOffice(office);
    });


    $("form[name=account_number]").validate({
        errorPlacement: function (error, element) {
            var parent = element.parents('.wr_complex_input');
            var parentcheckbox = element.parents('.jq-checkbox');
            var parentto = element.parents('.form_application_to');
            if (parent.length > 0) {
                parent.addClass('error');
                error.insertAfter(parent);
            } else if (parentcheckbox.length > 0) {
                parentcheckbox.addClass('error');
                error.insertAfter(parentcheckbox);
            } else if (parentto.length > 0) {
                parentto.append(error);
            } else {
                error.insertAfter(element);
            }
        },
        success: function (error) {
            error.prev('.jq-checkbox').removeClass('error');
            error.prev('.wr_complex_input').removeClass('error');
            error.remove();
        },
        messages: {
            confirmation: {
                required: 'Введите полученный код',
                remote: 'Код неверный или его действие истекло'
            },
            'form_checkbox_agreement[]': {
                required: 'Необходимо дать согласие'
            },
        },
        submitHandler: function (form) {
            var $form = $(form);

            SendingForm.confirm($form);
            return false;
        }
    });
    $.validator.addClassRules({
        reserve_documentType: {
            required: true
        },
        reserve_ip_full: {
            type_ip: true
        },
        reserve_ip_short: {
            type_ip: true
        },
        reserve_inn: {
            required: true,
            inn: true
        },
        reserve_ogrnip: {
            type_ip: true,
            maxlength: 15,
            minlength: 15
        },
        reserve_revenues: {
            required: true
        },
        reserve_city: {
            required: true
        },
        reserve_office: {
            required: true
        },
        reserve_phone: {
            required: true,
            pattern: /^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/
        },
        reserve_mailbox: {
            required: true,
            email2: true
        },
        reserve_work_type: {
            type_legal: true
        },
        reserve_ogrn: {
            type_legal: true,
            maxlength: 13,
            minlength: 13
        },
        reserve_justice_type: {
            type_legal: true
        },
        reserve_company_short: {
            type_legal: true
        },
        reserve_company: {
            type_legal: true
        },
        reserve_mailCode: {
            required: true
        }
    });


});

var onloadRecaptchafree = function () {
    grecaptcha.render('recaptcha', {
        'sitekey': '6Lc1yA8TAAAAAMgly-tXwBR6jHmnU7I_Od_yiD-2',
        'callback': SendingForm.verifyCallback,
        'theme': 'light'
    });
};








