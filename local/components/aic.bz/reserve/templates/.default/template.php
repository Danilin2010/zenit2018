
<div class="big_wr_form_application">
    <form name="account_number" action="" method="POST"
          enctype="multipart/form-data">
        <div class="snippet-online-reserve">
            <div class="snippet-online-reserve-body">
                <div class="snippet-online-reserve-text">
                    Для того чтобы воспользоваться
                    сервисом Вам необходимо:
                </div>
                <div class="snippet-online-reserve-ul">
                    <div class="snippet-online-reserve-li">
                        ознакомиться с условиями предоставления
                        сервиса
                    </div>
                    <div class="snippet-online-reserve-li">
                        подтвердить согласие с условиями
                        предоставления сервиса;
                    </div>
                    <div class="snippet-online-reserve-li">
                        заполнить краткую анкету.
                    </div>
                </div>
                <div class="snippet-online-reserve-chek">
                    <label>
                        <input type="checkbox" class="formstyle checkbox_agreement" id="42"
                               value="1"><label for="42"> </label>Согласен с <a class="open_modal"
                                                                                 href="#modal_form-agreement">условиями</a>
                    </label>
                </div>
                <!--<div class="snippet-online-reserve-button">
                    <input class="button" value="Зарезервировать" type="submit">
                </div>-->
            </div>
        </div>
        <div class="form_errors_text"></div>
        <div class="wr_form_application online-reserve-applicatio">
            <div class="step_form_application first_step">
                <div class="form_application">
                    <h3>Выберите вид юридического лица</h3>
                    <div class="form_application_line">
                        <input type="hidden"
                               style="reserve_documentType"
                               name="documentType"
                               value="ИП">
                        <div class="wr-toggle-light-text">
                            <div class="toggle-light-text on">Индивидуальный предприниматель</div>
                            <div class="toggle-light-wr">
                                <div class="toggle toggle-light" data-toggle data-toggle-type data-checked-n="ИП" data-checked-y="ЮЛ" data-checked="N" data-name="documentType"></div>
                            </div>
                            <div class="toggle-light-text off">Юридическое лицо-резидент РФ</div>
                        </div>
                    </div>
                </div>
                <div class="form_application">
                    <h3>О компании</h3>
                    <div class="form_application_line" data-type-line="legal">
                        <div class="wr_complex_input complex_input_noicon ">
                            <div class="complex_input_body">
                                <div class="text">Полное наименование ЮЛ</div>
                                <input type="text" class="simple_input reserve_company" autocomplete="off" name="company"
                                       value="" size="0"/></div>
                        </div>
                    </div>
                    <div class="form_application_line" data-type-line="legal">
                        <div class="wr_complex_input complex_input_noicon ">
                            <div class="complex_input_body">
                                <div class="text">Сокращенное наименование ЮЛ</div>
                                <input type="text" class="simple_input reserve_company_short" autocomplete="off" name="company_short"
                                       value="" size="0"/></div>
                        </div>
                    </div>
                    <div class="form_application_line" data-type-line="legal">
                        <select data-plasholder="Организационно-правовая форма" data-title="Организационно-правовая форма" class="formstyle reserve_justice_type" name="justice_type" id="form_dropdown_organizational_form">
                            <option value="Акционерное общество">Акционерное общество</option>
                            <option value="Закрытое акционерное общество">Закрытое акционерное общество</option>
                            <option value="Открытое акционерное общество">Открытое акционерное общество</option>
                            <option value="Публичное акционерное общество">Публичное акционерное общество</option>
                            <option value="Производственный кооператив">Производственный кооператив</option>
                            <option value="Общество с ограниченной ответственностью">Общество с ограниченной ответственностью</option>
                        </select>
                    </div>
                    <div class="form_application_line" data-type-line="ip">
                        <div class="wr_complex_input complex_input_noicon ">
                            <div class="complex_input_body">
                                <div class="text">Полное наименование ИП</div>
                                <input type="text" class="simple_input reserve_ip_full" autocomplete="off" name="ip_full" value=""
                                       size="0"/></div>
                        </div>
                    </div>
                    <div class="form_application_line" data-type-line="ip">
                        <div class="wr_complex_input complex_input_noicon ">
                            <div class="complex_input_body">
                                <div class="text">Сокращенное наименование ИП</div>
                                <input type="text" class="simple_input reserve_ip_short" autocomplete="off" name="ip_short" value=""
                                       size="0"/></div>
                        </div>
                    </div>
                    <h3>Данные компании</h3>
                    <div class="form_application_line">
                        <div class="form_application_to">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">ИНН</div>
                                    <input type="text" class="simple_input reserve_inn" autocomplete="off" name="ip_inn" value="" size="0" data-type-line="ip" />
                                    <input type="text" class="simple_input reserve_inn hidden" autocomplete="off" name="inn" value="" size="0" data-type-line="legal" />
                                </div>
                            </div>
                        </div>
                        <div class="form_application_to" data-type-line="legal">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">ОГРН</div>
                                    <input type="text" class="simple_input reserve_ogrn" autocomplete="off" name="ogrn"
                                           value="" size="0"/></div>
                            </div>
                        </div>
                        <div class="form_application_to" data-type-line="ip">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">ОГРНИП</div>
                                    <input type="text" class="simple_input reserve_ogrnip" autocomplete="off" name="ogrnip"
                                           value="" size="0"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_to" data-type-line="legal">
                            <select data-plasholder="Тип деятельности" data-title="Тип деятельности" class="formstyle reserve_work_type"
                                    name="work_type" id="form_dropdown_type_activity">
                                <option value="Внебюджетная">Внебюджетная</option>
                                <option value="Коммерческая">Коммерческая</option>
                            </select>
                        </div>
                        <div class="form_application_to"  data-type-line="legal">
                            <select data-plasholder="Объем годовой выручки"
                                    data-title="Объем годовой выручки"
                                    class="formstyle hidden reserve_revenues"
                                    name="revenues"
                                    id="form_dropdown_annual_revenue_volume">
                                <option value="менее 120 млн. руб.">менее 120 млн. руб.</option>
                                <option value="120 млн. - 800 млн. руб.">120 млн. - 800 млн. руб.</option>
                                <option value="более 800 млн. руб.">более 800 млн. руб.</option>
                            </select>
                        </div>

                        <div class="form_application_to" data-type-line="ip">
                            <select data-plasholder="Объем годовой выручки"
                                    data-title="Объем годовой выручки"
                                    class="formstyle reserve_revenues"
                                    name="revenues_ip"
                                    id="form_dropdown_annual_revenue_volume">
                                <option value="50 млн. - 100 млн. руб.">50 млн. - 100 млн. руб.</option>
                                <option value="более 100 млн. руб.">более 100 млн. руб.</option>

                            </select>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="form_application_to">
                            <input type="hidden"  id="form_text_type" value="">
                            <select id="select_form_text_43" data-id="form_text_43" name="city" data-plasholder="Город" data-title="Город" class="formstyle reserve_city">
                                <option value="">Выбрать город</option>
                            </select>
                        </div>
                        <div class="form_application_to">
                            <input type="hidden" id="form_text_type" value="">
                            <select id="select_form_text_44" data-id="form_text_44" name="office" data-plasholder="Офис" data-title="Офис" class="formstyle reserve_office">
                                <option value="">Выбрать офис</option>
                            </select>
                        </div>
                    </div>

                    <h3>Контактная информация</h3>
                    <div class="form_application_line">
                        <div class="form_application_to">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">Номер телефона</div>
                                    <input type="text" class="simple_input reserve_phone" autocomplete="off" name="phone" value="" size="0"/></div>
                            </div>
                        </div>
                        <div class="form_application_to">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">Адрес электронной почты</div>
                                    <input type="text" class="simple_input reserve_mailbox" autocomplete="off" name="mailbox" value="" size="0"/></div>
                            </div>
                        </div>
                    </div>
                    <div class="form_application_line">
                        <div class="wr_complex_input complex_input_noicon ">
                            <div class="complex_input_body">
                                <div class="text">Комментарии</div>
                                <textarea name="comments" cols="40" rows="5" class="simple_input" autocomplete="off"></textarea></div>
                        </div>
                    </div>

                    <div id="recaptcha"></div>

                    <div class="form_application_line form_text">
                        Для резервирования номера счета, необходимо получить код подтверждения,
                        который будет направлен на указанный Вами адрес электронной почты.
                        Код действителен в течение 15 минут!
                    </div>


                    <noindex>
                        <div class="server-error error hidden" data-server-error="1"></div>

                        <div class="server-error error hidden" data-server-error="2">
                            <p class="title">Ошибка обработки.</p>
                            <p class="text">К сожалению, на текущий момент сервис &laquo;Онлайн-резервирование номера расчетного счета&raquo; недоступен.<br>Для открытия расчетного счета, пожалуйста, обратитесь в ближайшее отделение Банка либо по телефону +7 (495) 937-09-94, 8-800-500-66-77 (звонок по России бесплатный).</p>
                            <p>С уважением,<br>Банк ЗЕНИТ.</p>
                        </div>

                        <div class="server-error error hidden" data-server-error="3">
                            <p class="title">Неверный код подтверждения.</p>
                            <p class="text">Количество оставшихся попыток: <span>x</span>.</p>
                            <p>Необходимо проверить правильность введенного кода и <a href="#" data-resend-code>повторить попытку</a>.</p>
                        </div>

                        <div class="server-error error hidden" data-server-error="4">
                            <p class="title">Удаленный сервис не отвечает.</p>
                            <p>Извините. Возникли технические проблемы.<br>Попробуйте повторить попытку позже.</p>
                        </div>

                        <div class="server-success success hidden" data-server-success="1">
                            <p class="title">Запрос обработан.</p>
                            <p class="text">Спасибо, что выбрали Банк ЗЕНИТ.<br>
                            </p>
                            <p>На указанный Вами адрес электронной почты отправлен список необходимых документов и порядок открытия расчетного счета.</p>
                            <p><a href="#">ЕЩЕ ОДИН НОМЕР СЧЕТА</a></p>
                        </div>
                    </noindex>

                    <div class="form_application_to">
                        <input data-setcode class="button hidden" value="Получить код" type="button">
                    </div>

                    <div class="form_application_line confirmation_block" data-block-confirmation>
                        <div class="form_application_to">
                            <div class="wr_complex_input complex_input_noicon ">
                                <div class="complex_input_body">
                                    <div class="text">Код подтверждения</div>
                                    <input type="text" class="simple_input reserve_mailCode" autocomplete="off" name="mailCode" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form_application_to">
                            <input data-confirm-code class="button" value="Зарезервировать счет" type="submit">
                        </div>
                    </div>
                </div>
    </form>
</div>


<? /**/ ?>
<div class="step_form_application to_step">
    <div class="form_application">
        <div class="form_application_ok"></div>
        <div class="form_application_title">
            Ваша заявка принята!
        </div>
        <div class="form_application_text">
            Номер заявки <b data-result></b>. Подтверждение отправлено на e-mall. Срок<br/>
            рассмотрения заявки составляет 2 рабочих дня. Представитель банка<br/>
            свяжется с Вами в ближайшее время.
        </div>
    </div>
</div>


</div>

