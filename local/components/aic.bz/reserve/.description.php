<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage('AIC_BZ_CITY_LIST_NAME'),
	"DESCRIPTION" => Loc::getMessage('AIC_BZ_CITY_LIST_DESCRIPTION'),
	"SORT" => 20,
	"PATH" => array(
		"ID" => 'aicbz',
		"NAME" => Loc::getMessage('AIC_BZ_DESCRIPTION_GROUP'),
		"SORT" => 10,
	),
);



?>