<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(false);
?>
<?if(count($arResult['ITEMS'])){?>
<div class="wr_select_city" data-wr-city>
    <?/*<div class="select_city_substrate" data-substrate-city></div>*/?>
    <div class="select_city">
        <div class="select_city_fr">
            <div class="select_city_close" data-close-city></div>
            <div class="select_city_title"><?=Loc::getMessage('AIC_BZ_CITY_LIST_TEMPLATE_TITLE');?></div>
            <div class="select_city_body">
                <?foreach($arResult['ITEMS'] as $key=>$item){?>
                <div class="select_city_block">
                    <div class="select_city_left">
                        <?=$key?>
                    </div>
                    <div class="select_city_right">
                        <ul class="select_city_list">
                            <?foreach($item as $sity){?>
                            <li><a
                                   href="?set_sity=<?=$sity["ID"]?>" data-set-cook-city
                                   <?if($sity["ID"]==$arResult['CITY']["ID"]){?>data-set-city class="select"<?}?>
                                   ><?=$sity["NAME"]?></a></li>
                            <?}?>
                        </ul>
                    </div>
                </div>
                <?}?>
            </div>
            <div class="wr_bottom_city">
                <div class="bottom_city_title">
                    <?=Loc::getMessage('AIC_BOTTOM_CITY_TITLE');?>
                </div>
                <div class="bottom_city_list">
                    <div class="bottom_city_item bottom_city_item001">

                    </div>
                    <div class="bottom_city_item bottom_city_item002">

                    </div>
                    <div class="bottom_city_item bottom_city_item003">

                    </div>
                    <div class="bottom_city_item bottom_city_item004">

                    </div>
                    <div class="bottom_city_item bottom_city_item005">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?}?>
