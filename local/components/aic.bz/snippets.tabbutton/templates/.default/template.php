<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<!-- Кнопки табов. Активная кнопка - tab-control_state-active  В href - id таба с контентом-->
<!-- Для внешней ссылки просто указать URL вместо ID таба. target="_blank" опционально-->
<a class="tabs-controls__tab-control <?if($arParams["PRODUCTHEADER_ACTIVE"]=="Y"){?>tabs-controls__tab-control_state-active<?}?> <?if($arParams["PRODUCTHEADER_STAR"]=="Y"){?>tabs-controls__tab-control_star<?}?>"
   href="<?=$arParams["PRODUCTHEADER_SRC"]?>"
    <?if($arParams["PRODUCTHEADER_TITLE"]=="Y"){?> target="_blank"<?}?>>
    <span class="tabs-controls__text"><?=$arParams["PRODUCTHEADER_TITLE"]?></span>
</a>