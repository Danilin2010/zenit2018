<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{

	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'PRODUCTHEADER_TITLE' => Array(
				'PARENT' => 'BASE',
				'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_TITLE'),
				'TYPE' => 'STRING',
				'DEFAULT' => '',
			),
			'PRODUCTHEADER_SRC' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_SRC'),
                'TYPE' => 'STRING',
                'DEFAULT' => '',
			),
            'PRODUCTHEADER_TARGET' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_TARGET'),
                'TYPE' => 'CHECKBOX',
            ),
            'PRODUCTHEADER_ACTIVE' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_ACTIVE'),
                'TYPE' => 'CHECKBOX',
            ),
            'PRODUCTHEADER_STAR' => array(
                'PARENT' => 'BASE',
                'NAME' => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_STAR'),
                'TYPE' => 'CHECKBOX',
            ),
        )
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>