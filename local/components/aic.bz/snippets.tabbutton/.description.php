<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
	"NAME" => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_NAME'),
	"DESCRIPTION" => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING_TABBUTTON_DESCRIPTION'),
	"SORT" => 10,
	"PATH" => array(
		"ID" => 'mscoder',
		"NAME" => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS'),
		"SORT" => 10,
		"CHILD" => array(
			"ID" => 'standard',
			"NAME" => Loc::getMessage('AIC_BZ_SNIPPETS_CONTENT_BLOCKS_BREEDING'),
			"SORT" => 10
		)
	),
);

?>