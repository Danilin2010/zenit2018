<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
use Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
$this->setFrameMode(false);


$cityDefault = (new \Aic\Bz\cGeo())->get_city();

Asset::getInstance()->addJs('//api-maps.yandex.ru/2.1/?lang=ru_RU');
//Asset::getInstance()->addCss('/demo/offices/px/px.css');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/realty.css');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/knockout.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/realty.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/realty.map.js');

$arCity = $this->__component->oData->getCityRealtyGroup();
$city = json_encode($arCity, JSON_UNESCAPED_UNICODE);

$mortgage = json_encode([
    ['code' => 'classic', 'name' => 'Ипотека на новостройки'],
    ['code' => 'vi', 'name' => 'Военная ипотека']
],
    JSON_UNESCAPED_UNICODE
);

Asset::getInstance()->addString("
<script>
var realtyCity ='" . $city . "'; 
var realtyCityDefaultId = '" . $cityDefault['ID'] . "';
var realtyCityDefaultName = '" . $cityDefault['NAME'] . "';
var realtyMortgage = '" . $mortgage . "'
</script>");

?>

<div id="filtr-atm-ofice-all" class="main_content realty">
    <div class="container realty-title">
        <div class="row">
            <div class="col-md-9"><h1>Карта новостроек в ипотеку от Банка ЗЕНИТ</h1></div>
            <div class="col-md-3 pl-0">

                <div class="jq-selectbox jqselect formstyle city-select hidden-xs hidden-sm">
                    <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle"
                            name="city" data-plugin="selectCity" data-bind="
                                        event: {change: function(data, event){ selectCity(data,event) } },
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        selectedOptions: $root.chosenCity
                                        "></select>
                </div>
            </div>
        </div>

    </div>

    <div class="container basic-filter">
        <!-- Фильтр в шапке -->
        <div id="atm-ofice-filtr" class="atm-ofice-full-filtr">
            <div id="filtr-header">
                <div class="row">
                    <div class="jq-selectbox jqselect formstyle">
                        <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle"
                                name="city" data-plugin="selectCity" data-bind="
                                        event: {change: function(data, event){ selectCity(data,event) } },
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        selectedOptions: $root.chosenCity
                                        "></select>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="atm-ofice" class="atm-ofice-full">

        <div class="container">
            <!-- Отображение на карте -->
            <div id="map-bank" class="offices " data-block="screen">
                <!-- Отображение офисов на карте -->
                <div id="cash-dispenser-map" class="container">
                    <div class="row hidden-xs hidden-sm">


                        <div class="col-sm-12 col-md-4 col-left">
                            <div class="block-filtr filtr-block-1">
                                <div id="offices-map-filtr">
                                    <!-- ko foreach: items -->
                                    <a type="atm" href="#modal_form-office" class="open_modal marshrut hover"
                                       data-bind="attr: {id: id}, click: modal">
                                        <div class="card-block data">
                                            <h3 class="title-ofice" data-bind="html: name"></h3>
                                            <p>
                                                <span data-bind="html: address"></span>
                                            </p>
                                        </div>
                                    </a>
                                    <!-- /ko -->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3 col-right ymap">
                            <div class="block-filtr filtr-block-3">
                                <div class="wr_complex_input complex_input_noicon">
                                    <div class="complex_input_body">
                                        <div id="maps-search-adres" class="complex_input_body">
                                            <input name="q" class="simple_input" placeholder="" value="" type="text"
                                                   data-bind="event: {change: function(data,event) {term(data, event)} }">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </div>
                                        <div class="text search">поиск</div>
                                    </div>
                                </div>
                            </div>


                            <div id="maps-atmstatus" class="block-filtr filtr-block-4">
                                <label>
                                    <input type="checkbox" class="formstyle" name="filterParam" value="classic" data-bind="checked: $root.chosenMortgage, event: {change: function(data, event) { $root.checkMortgage(data, event); } }">
                                    Ипотека на новостройки
                                </label>
                                <label>
                                    <input type="checkbox" class="formstyle" name="filterParam" value="vi" data-bind="checked: $root.chosenMortgage, event: {change: function(data, event) { $root.checkMortgage(data, event); } }">
                                    Военная ипотека
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>


</div>


<!-- Модальное окно для отображения информации -->
<div class="modal-open modal">
    <div id="modal_form-office" class="modal_div">
        <table class="modal_form-office-table">
            <td>
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header hidden-xs hidden-sm">
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            <h4 class="modal-title" id="exampleModalLabel" data-bind="html: $root.dialog.name"></h4>
                        </div>
                        <div class="modal-header hidden-lg">
                            <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                            <h4 class="modal-title" id="exampleModalLabel">Выбранный адрес</h4>
                        </div>
                        <div id="map-modal" class="map-modal object-picture" data-bind="style: {backgroundImage: 'url(' + $root.dialog.object() + ')'}">
                        </div>
                        <div class="mobile-modal-content">
                            <div class="modal-body">
                                <div class="modal-content-top">
                                    <h4 class="modal-title-mobile hidden-lg" id="exampleModalLabel"
                                        data-bind="html: $root.dialog.name"></h4>
                                </div>
                                <div class="modal-content-bottom">
                                    <div class="content_rates_tabs ui-tabs ui-corner-all ui-widget ui-widget-content">
                                        <div class="info-content">
                                            <div class="content_body ui-tabs-panel ui-corner-bottom ui-widget-content">
                                                <div class="row block-line">
                                                    <div class="col-sm-12 col-mt-6 col-md-6">
                                                        <h5>Адрес</h5>
                                                        <div class="weekdays"
                                                             data-bind="html: $root.dialog.address"></div>
                                                    </div>

                                                    <div class="col-sm-12 col-mt-6 col-md-6">
                                                        <h5>Застройщик/продавец</h5>
                                                        <div class="row">
                                                            <div class="col-sm-8 col-mt-8 col-md-8">
                                                                <div style="font-weight: 800" data-bind="html: $root.dialog.gk"></div>
                                                                <div class="" data-bind="html: $root.dialog.ul"></div>
                                                            </div>
                                                            <div class="col-sm-4 col-mt-4 col-md-4">
                                                                <!-- ko if: $root.dialog.logo().length > 0 -->
                                                                <img data-bind="attr: {src: $root.dialog.logo}" class="company-logo">
                                                                <!-- /ko -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row block-line">
                                                    <div class="col-sm-12 col-mt-6 col-md-6">
                                                        <h5>Доступная ипотека</h5>
                                                        <div class="weekdays"
                                                             data-bind="html: $root.dialog.mortgage"></div>
                                                    </div>

                                                    <div class="col-sm-12 col-mt-6 col-md-6">
                                                        <h5>Сайт застройщика</h5>
                                                        <div class=""><a
                                                                    data-bind="html: $root.dialog.link, attr: {'href': $root.dialog.link}"
                                                                    target="_blank"></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a id="marshrut" class="modal_close close_marshrut"
                                   data-bind="attr: {'href': $root.dialog.pageUrl}">Отправить заявку</a>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </table>
    </div>
</div>


<!-- Мобильный фильтр офисов и бпнкоматов -->
<div id="modal_form_filtr" class="modal_div mobile">
    <div class="row">
        <div class="col-sm-12 col-md-12 modal_fon pb-0">
            <div class="row py-3 top-modal">
                <div class="col-sm-12 col-md-12 px-5">
                    <span class="modal_close"><i class="fa fa-times" aria-hidden="true"></i></span>
                    <h4 class="modal-title">Фильтр</h4>
                </div>
            </div>

            <div class="row content-modal">
                <div class="col-sm-12 col-md-12 px-5">
                    <!-- мобильный фильтр для офисов -->
                    <div id="mobile_filter_office" data-block="screen">
                        <div>
                            <div class="jq-selectbox jqselect formstyle">
                                <select data-programm="" data-plasholder="Город" data-title="Город" class="formstyle"
                                        name="city" data-plugin="selectCity" data-bind="
                                        event: {change: function(data, event){ selectCity(data,event) } },
                                        options: listCity,
                                        optionsValue: 'id',
                                        optionsText: 'name',
                                        selectedOptions: $root.chosenCity
                                        "></select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>