<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
$this->setFrameMode(true);

$result = [];


//$arResult['items'] = array_merge($arResult['items']);
$result = $this->__component->oData->getItems();

echo json_encode([
    'status' => 200,
    'items' => $result,
], JSON_UNESCAPED_UNICODE);

