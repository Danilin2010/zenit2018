<?php
namespace Aic\Bz\Realty;


class RealtyData
{

    private $filter = [];
    private $arParams = [];
    private $data;


    const
        IB_REALTY = 149,
        IB_CITY = 1;


    public function __construct($arParams)
    {
        $this->arParams = $arParams;
    }


    public function buildFilter($params)
    {
        if (array_key_exists('IBLOCK_ID', $params)) {

            if((int)$params['IBLOCK_ID'] <= 0) {
                throw new \Exception('IBLOCK ID не верный');
                die;
            }
        } else {
            throw new \Exception('IBLOCK ID не найден');
        }

        $this->filter['IBLOCK_ID'] = $params['IBLOCK_ID'];
        $this->filter['ACTIVE'] = 'Y';
        $this->filter['PROPERTY_city'] = $params['city'];



        //Фильтр по имени или метро
        if(!empty($params['term'])) {
            $this->filter[] = [
                "LOGIC" => "OR",
                "%PROPERTY_address" => $params['term'],
                "%PROPERTY_gk" => $params['term'],
                "%PROPERTY_ul" => $params['term'],
                "%NAME" => $params['term'],
            ];
        }

/*        $this->filter[] = [
            "LOGIC" => "OR",
            "PROPERTY_vi_XML_ID" => 'yes',
        ];*/

        if(!empty($params['mortgage'])) {
            $mortgage['LOGIC'] = "OR";

            if(in_array('classic', $params['mortgage'])) {
                //TODO: сделать по челочечески
                $mortgage['PROPERTY_classic'] = 54;
            }

            if(in_array('vi', $params['mortgage'])) {
                //TODO: сделать по челочечески
                $mortgage['PROPERTY_vi'] = 55;
            }
            $this->filter[] = $mortgage;
        }

        //$this->filter['PROPERTY_classic_VALUE'] = 'Ипотека на новостройки';
        //s$this->filter['PROPERTY_vi_VALUE'] = 'Военная ипотека';


       // echo '<pre>' . print_r( $this->filter, 1) . '</pre>';
    }


    public function getItems()
    {
        try {
            //вызов генерации фильтра
            $this->buildFilter($this->arParams);


            $result = [];
            $res = \CIBlockElement::GetList(['name' => 'asc'], $this->filter, false, false);

            while ($ob = $res->GetNextElement()) {
                $f = $ob->GetFields();
                $p = $ob->GetProperties();
                $props = [];

                $mortgage = [];

                foreach ($p as $prop) {
                    if($prop['CODE'] == 'coord') {
                        $props[$prop['CODE']] = explode(',', str_replace(['[', ']'], '', $prop['VALUE']));
                    }
                    elseif($prop['CODE'] == 'classic' || $prop['CODE'] == 'vi'){
                        if(!empty($prop['VALUE'])) {
                            $mortgage[] = $prop['VALUE'];
                        }
                    }
                    elseif($prop['CODE'] == 'logo' || $prop['CODE'] == 'object'){
                        $props[$prop['CODE']] = \CFile::GetPath($prop['VALUE']);
                    }
                    else {
                        $props[$prop['CODE']] = $prop['VALUE'];
                    }
                }

                $props['mortgage'] = implode(', ', $mortgage);

                $result[] = [
                    'id' => $f['ID'],
                    'name' => $f['NAME'],
                    'props' => $props,
                ];
            }
            return $result;

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }


    public function getCityFilter()
    {
        $allCity = $this->getCity();
        $cityId = $this->getCityRealtyGroupId();

        $result = [];

        foreach ($cityId as $item) {
            if($item) {
                $result[] = $allCity[$item];
            }
        }

        return $result;
    }


    protected function getCity()
    {
        $result = [];
        $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_CITY], false, false);

        while ($ob = $res->GetNext()) {
            $result[$ob['ID']] = [
                'id' => $ob['ID'],
                'uid' => $ob['XML_ID'],
                'name' => $ob['NAME'],
            ];
        }
        return $result;
    }


    /** Выбирает элементы по условию банка и группирует по городу чтобы найти те города которые есть для этого банка и типа элемента
     * @param $bankId
     * @return array
     */
    protected function getCityRealtyGroupId()
    {
        global $DB;
        $list = [];

        $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => self::IB_REALTY], ['PROPERTY_CITY'], false,
            ['ID', 'PROPERTY_CITY']);

        while ($ob = $res->GetNext()) {
            if(!empty($ob['PROPERTY_CITY_VALUE'])) {
                $list[] = $ob['PROPERTY_CITY_VALUE'];
            }
        }

        return $list;
    }



    public function getCityRealtyGroup()
    {
        global $DB;
        $list = [];

        $res = \CIBlockElement::GetList(['PROPERTY_CITY' =>'ASC'], ['IBLOCK_ID' => self::IB_REALTY], ['PROPERTY_CITY'], false,
            ['ID', 'PROPERTY_CITY']);

        while ($ob = $res->GetNext()) {
            if(!empty($ob['PROPERTY_CITY_VALUE'])) {
                if($ob['PROPERTY_CITY_VALUE'] == 'Москва и МО') {
                    array_unshift($list, $ob['PROPERTY_CITY_VALUE']);
                } else {
                    $list[] = $ob['PROPERTY_CITY_VALUE'];
                }
            }
        }

        return $list;
    }


}