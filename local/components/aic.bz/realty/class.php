<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require_once ('RealtyData.php');

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;
use Bitrix\Main\Application;

class RealtyComponent extends CBitrixComponent
{
	/**
	 * кешируемые ключи arResult
	 * @var array()
	 */
	protected $cacheKeys = array();
	
	/**
	 * дополнительные параметры, от которых должен зависеть кеш
	 * @var array
	 */
	protected $cacheAddon = array();
	

    /**
     * вохвращаемые значения
     * @var mixed
     */
	protected $returned;

    /**
     * тегированный кеш
     * @var mixed
     */
    protected $tagCache;

    /** Объект с классом данных
     * @var
     */
    public $oData;

	/**
	 * подключает языковые файлы
	 */
	public function onIncludeComponentLang()
	{
		$this->includeComponentLang(basename(__FILE__));
		Loc::loadMessages(__FILE__);
	}
	
    /**
     * подготавливает входные параметры
     * @param array $params
     * @return array
     */
    public function onPrepareComponentParams($params)
    {
        $result = array(
            'IBLOCK_TYPE' => trim($params['IBLOCK_TYPE']),
            'IBLOCK_ID' => intval($params['IBLOCK_ID']),
            'IBLOCK_CODE' => trim($params['IBLOCK_CODE']),
            'CACHE_TIME' => intval($params['CACHE_TIME']) > 0 ? intval($params['CACHE_TIME']) : 3600,
            'CACHE_TAG_OFF' => $params['CACHE_TAG_OFF'] == 'Y'
        );
        return $result;
    }
	


	/**
	 * получение результатов
	 */
	protected function getResult()
	{

	}
	
	/**
	 * выполняет действия после выполения компонента, например установка заголовков из кеша
	 */
	protected function executeEpilog()
	{
		if ($this->arResult['IBLOCK_ID'] && $this->arParams['CACHE_TAG_OFF'])
            \CIBlock::enableTagCache($this->arResult['IBLOCK_ID']);
	}


    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules()
    {
        if (!Main\Loader::includeModule('aic.bz'))
            throw new Main\LoaderException(Loc::getMessage('AIC_BZ_CITY_LIST_CLASS_AIC_BZ_MODULE_NOT_INSTALLED'));
    }
	
	/**
	 * выполняет логику работы компонента
	 */
	public function executeComponent()
	{
        try
		{
			$this->checkModules();

			$request = Application::getInstance()->getContext()->getRequest();

			$filter = [];

			if ($request->getQuery('filterParam'))
			{
				$filter['filterParam'] = $request->getQuery('filterParam');
			}

			if ($request->getPost('city'))
			{
				$this->arParams['city'] = $request->getPost('city');
			}
			else
			{
				$this->arParams['city'] = (new \Aic\Bz\cGeo())->get_city()['NAME'];
			}

			if ($request->getPost('term'))
			{
				$this->arParams['term'] = $request->getPost('term');
			}

			if ($request->getPost('mortgage'))
			{
				$this->arParams['mortgage'] = $request->getPost('mortgage');
			}

			if ($request->getQuery('coord'))
			{
				$filter['coord'] = $request->getQuery('coord');
			}

			$this->oData = new \Aic\Bz\Realty\RealtyData($this->arParams);

            $this->includeComponentTemplate();
		}
		catch (Exception $e)
		{
			$this->abortDataCache();
			ShowError($e->getMessage());
		}
	}
}
