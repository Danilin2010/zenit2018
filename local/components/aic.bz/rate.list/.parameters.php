<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

try
{
	if (!Main\Loader::includeModule('aic.bz'))
		throw new Main\LoaderException(Loc::getMessage('AIC_BZ_CITY_LIST_PARAMETERS_AIC_BZ_MODULE_NOT_INSTALLED'));


	$arComponentParameters = array(
		'GROUPS' => array(
		),
		'PARAMETERS' => array(
			'CACHE_TIME' => array(
				'DEFAULT' => 3600
			)
		)
	);
}
catch (Main\LoaderException $e)
{
	ShowError($e->getMessage());
}
?>