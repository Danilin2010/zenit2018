<?
$MESS['UF_TYPE_BN'] = 'Безналичные';
$MESS['UF_TYPE_PL'] = 'По карточным счетам';
$MESS['TEMP_TITLE'] = 'Курсы валют';
$MESS['TEMP_BY'] = 'Продаем';
$MESS['TEMP_SELL'] = 'Покупаем';
$MESS['TEMP_CURRENCY'] = 'Валюта';
$MESS['TEMP_USD'] = 'USD';
$MESS['TEMP_EUR'] = 'EUR';
$MESS['TEMP_EUR_USD'] = 'EUR > USD';
$MESS['TEMP_USD_USD'] = 'USD > EUR';
$MESS['TEMP_CROSS'] = 'Кросс-курс';
?>