<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])>0){?>
<div class="exchange_rates">
    <div class="exchange_title">
        <div class="exchange_title_t">
            <?=Loc::getMessage('TEMP_TITLE')?>
        </div>
        <div data-totime="Y" class="exchange_title_p">
            <?=$arResult["ITEMS"][0]["UF_DATETIME"]?>
        </div>
    </div>
    <div class="exchange_rates_tabs">
        <ul class="exchange_tab">
            <?foreach($arResult["ITEMS"] as $key=>$item){?>
                <li data-settime="<?=$item["UF_DATETIME"]?>"><a href="#tabs-<?=$item["ID"]?>"><?=Loc::getMessage('UF_TYPE_'.$item['UF_TYPE'])?></a></li>
            <?}?>
        </ul>
        <?foreach($arResult["ITEMS"] as $key=>$item){?>
            <div class="exchange_body" id="tabs-<?=$item["ID"]?>">
                <div class="exchange_block">
                    <div class="exchange_line">
                        <div class="exchange_value exchange_grey">
                            <?=Loc::getMessage('TEMP_BY')?>
                        </div>
                        <div class="exchange_value exchange_grey">
                            <?=Loc::getMessage('TEMP_SELL')?>
                        </div>
                        <div class="exchange_name exchange_grey">
                            <?=Loc::getMessage('TEMP_CURRENCY')?>
                        </div>
                    </div>
                    <div class="exchange_line">
                        <div class="exchange_value">
                            <?=$item["UF_USD_SELL"]?>
                        </div>
                        <div class="exchange_value">
                            <?=$item["UF_USD_BY"]?>
                        </div>
                        <div class="exchange_name">
                            <?=Loc::getMessage('TEMP_USD')?>
                        </div>
                    </div>
                    <div class="exchange_line">
                         <div class="exchange_value">
                            <?=$item["UF_EUR_SELL"]?>
                        </div>
                        <div class="exchange_value">
                            <?=$item["UF_EUR_BY"]?>
                        </div>
                        <div class="exchange_name">
                            <?=Loc::getMessage('TEMP_EUR')?>
                        </div>
                    </div>
                    <div class="exchange_line">
                        <div class="exchange_value">
                            <?=$item["UF_CROSS_SELL"]?>
                            <div class="exchange_grey">
                                <?=Loc::getMessage('TEMP_USD_USD')?>
                            </div>
                        </div>
                        <div class="exchange_value">
                            <?=$item["UF_CROSS_BY"]?>
                            <div class="exchange_grey">
                                <?=Loc::getMessage('TEMP_EUR_USD')?>
                            </div>
                        </div>
                        <div class="exchange_name">
                            <?=Loc::getMessage('TEMP_CROSS')?>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>
<?}?>