<table class="tb">
<thead>
<tr>
	<td>
		Банк-корреспондент
	</td>
	<td>
		SWIFT&nbsp;
	</td>
	<td>
		№ Счета
	</td>
</tr>
</thead>
<tbody>
<tr>
	<td colspan="3">
		AUD
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113
	</td>
</tr>
<tr>
	<td colspan="3">
		BYR
	</td>
</tr>
<tr>
	<td>
		Belvnesheconombank Open Joint Stock Company, Minsk
	</td>
	<td>
		BELBBY2X
	</td>
	<td>
		1702 795072004
	</td>
</tr>
<tr>
	<td colspan="3">
		CAD
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-111
	</td>
</tr>
<tr>
	<td colspan="3">
		CHF
	</td>
</tr>
<tr>
	<td>
		Banque de Commerce et de Placements SA, Geneva
	</td>
	<td>
		BPCPCHGG
	</td>
	<td>
		10.603414.0.100 CHF
	</td>
</tr>
<tr>
	<td colspan="3">
		CNY
	</td>
</tr>
<tr>
	<td>
		Bank of China (ELUOSI), Moscow
	</td>
	<td>
		BKCHRUMM
	</td>
	<td>
		30109156600000000001
	</td>
</tr>
<tr>
	<td>
		Bank of China (Hong Kong) Limited, Hong Kong
	</td>
	<td>
		BKCHHKHH
	</td>
	<td>
		012-875-60-11756-0
	</td>
</tr>
<tr>
	<td>
		Industrial and Commercial Bank of China (Moscow) ZAO, Moscow
	</td>
	<td>
		ICBKRUMM
	</td>
	<td>
		30109156100000000272
	</td>
</tr>
<tr>
	<td colspan="3">
		DKK
	</td>
</tr>
<tr>
	<td>
		NG Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-101
	</td>
</tr>
<tr>
	<td colspan="3">
		EUR
	</td>
</tr>
<tr>
	<td>
		Commerzbank AG, Frankfurt am Main
	</td>
	<td>
		COBADEFF
	</td>
	<td>
		400888055100 EUR
	</td>
</tr>
<tr>
	<td>
		Deutsche Bank AG, Frankfurt am Main
	</td>
	<td>
		DEUTDEFF
	</td>
	<td>
		100 9476045 10 00
	</td>
</tr>
<tr>
	<td>
		Raiffeisen Bank International AG, Vienna
	</td>
	<td>
		RZBAATWW
	</td>
	<td>
		55 040 919
	</td>
</tr>
<tr>
	<td>
		VTB Bank (Europe) SE, Frankfurt am Main
	</td>
	<td>
		OWHBDEFF
	</td>
	<td>
		0103571014
	</td>
</tr>
<tr>
	<td colspan="3">
		GBP
	</td>
</tr>
<tr>
	<td>
		VTB Bank (Europe) SE, Frankfurt am Main
	</td>
	<td>
		OWHBDEFF
	</td>
	<td>
		0103571428
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB&nbsp;
	</td>
	<td>
		301010343113-031
	</td>
</tr>
<tr>
	<td colspan="3">
		HKD
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-975
	</td>
</tr>
<tr>
	<td>
		Industrial and Commercial Bank of China (Moscow) ZAO, Moscow
	</td>
	<td>
		ICBKRUMM
	</td>
	<td>
		30109344200000000272
	</td>
</tr>
<tr>
	<td>
		Bank of China (ELUOSI), Moscow
	</td>
	<td>
		BKCHRUMM
	</td>
	<td>
		30109344700000000001
	</td>
</tr>
<tr>
	<td colspan="3">
		INR
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301-0103431-13INR
	</td>
</tr>
<tr>
	<td colspan="3">
		JPY
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-831
	</td>
</tr>
<tr>
	<td colspan="3">
		KRW
	</td>
</tr>
<tr>
	<td>
		Korea Exchange Bank, Seoul
	</td>
	<td>
		KOEXKRSE
	</td>
	<td>
		0963-FRW-001000075
	</td>
</tr>
<tr>
	<td colspan="3">
		KZT
	</td>
</tr>
<tr>
	<td>
		Kazkommertsbank Joint-Stock Company, Almaty
	</td>
	<td>
		KZKOKZKX
	</td>
	<td>
		KZ679260001000347000
	</td>
</tr>
<tr>
	<td colspan="3">
		NOK
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-091
	</td>
</tr>
<tr>
	<td colspan="3">
		NZD
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113
	</td>
</tr>
<tr>
	<td colspan="3">
		PLN
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113/PLN
	</td>
</tr>
<tr>
	<td colspan="3">
		SEK
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-081
	</td>
</tr>
<tr>
	<td colspan="3">
		SGD
	</td>
</tr>
<tr>
	<td>
		ING Belgium SA/NV, Brussels
	</td>
	<td>
		BBRUBEBB
	</td>
	<td>
		301010343113-851
	</td>
</tr>
<tr>
	<td>
		Industrial and Commercial Bank of China (Moscow) ZAO, Moscow
	</td>
	<td>
		ICBKRUMM
	</td>
	<td>
		30109702800000000272
	</td>
</tr>
<tr>
	<td colspan="3">
		UAH
	</td>
</tr>
<tr>
	<td>
		Raiffeisen Bank Aval Public Joint Stock Company, Kyiv
	</td>
	<td>
		AVALUAUK
	</td>
	<td>
		16005324
	</td>
</tr>
<tr>
	<td colspan="3">
		USD
	</td>
</tr>
<tr>
	<td>
		Citibank NA, New York
	</td>
	<td>
		CITIUS33
	</td>
	<td>
		36312321
	</td>
</tr>
<tr>
	<td>
		JPMorgan Chase Bank National Association, New York
	</td>
	<td>
		CHASUS33
	</td>
	<td>
		400 941 228
	</td>
</tr>
<tr>
	<td colspan="3">
		VND
	</td>
</tr>
<tr>
	<td>
		Vietnam-Russia Joint Venture Bank, Hanoi
	</td>
	<td>
		VRBAVNVX
	</td>
	<td>
		999010000000037
	</td>
</tr>
</tbody>
</table>