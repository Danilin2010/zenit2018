<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корреспондентские отношения");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/the-financial-institutions/korrespondent.png');
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p>
					 Банк ЗЕНИТ имеет давние деловые и партнерские отношения с крупнейшими российскими и международными финансовыми институтами. Это позволяет нам предоставлять широкий спектр услуг на высоком качественном уровне. При этом корреспондентская сеть Банка ЗЕНИТ предоставляет клиентам Банка возможности для максимально эффективного проведения расчетов и осуществления переводов в различные точки мира.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Расчетное<br>
			 обслуживание</a></li>
			<li><a href="#content-tabs-2">Корреспондентские<br>
			 счета НОСТРО</a></li>
			<li><a href="#content-tabs-3">Зарубежные Банки<br>
			 - корреспонденты</a></li>
			<li><a href="#content-tabs-4">Анкета ПАО<br>
			 Банк ЗЕНИТ</a></li>
			<li><a href="#content-tabs-5">Тарифы</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						 Банк ЗЕНИТ осуществляет расчётное обслуживание счетов Лоро кредитных организаций (резидентов и нерезидентов) в российских рублях и иностранной валюте (в том числе в валютах стран ближнего зарубежья). Обслуживание осуществляется на следующих условиях:
						<ul>
							<li>бесплатное открытие/закрытие и ведение счета;</li>
							<li>начисление процентов на остаток на корреспондентском счете;</li>
							<li>использование следующих видов электронной связи - SWIFT, TELEX, системы iBank.</li>
						</ul>
						<p>
							 Расчеты осуществляются в соответствии со следующим регламентом:
						</p>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "rub_transfer",
		"EDIT_TEMPLATE" => ""
	)
);?> <!--table-->
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "dol_transfer",
		"EDIT_TEMPLATE" => ""
	)
);?> <!--table-->
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "eur_transfer",
		"EDIT_TEMPLATE" => ""
	)
);?> <!--table-->
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					 <!--table--> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "corr_table",
		"EDIT_TEMPLATE" => ""
	)
);?> <!--table-->
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					 <!--table--> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "swift_table",
		"EDIT_TEMPLATE" => ""
	)
);?> <!--table-->
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-5">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="right_top_line">
							<div class="block_top_line">
							</div>
							<div class="right_bank_block">
								<div class="wr_max_block">
									<div class="form_application_line">
										<div class="contacts_block">
											 Илья Елистратов&nbsp; <a href="mailto:ilya.elistratov@zenit.ru">ilya.elistratov@zenit.ru</a><br>
											 +7 (495) 937-07-37, доб. 2515
											<div class="note_text">
												 Начальник Управления развития отношений с финансовыми институтами
											</div>
										</div>
										<div class="contacts_block">
											 Эльвира Бургарт&nbsp;&nbsp; <a href="mailto:burgart@zenit.ru">burgart@zenit.ru</a><br>
											 +7 (495) 937-07-37, доб. 2814
											<div class="note_text">
												 Начальник Отдела развития отношений с банками РФ, СНГ и Балтии
											</div>
										</div>
										<div class="contacts_block">
											 Елена Колесникова&nbsp; <a href="mailto:elena.kolesnikova@zenit.ru">elena.kolesnikova@zenit.ru</a><br>
											 +7 (495) 937-07-37, доб. 2560
											<div class="note_text">
												 Начальник Отдела развития отношений с международными финансовыми институтами
											</div>
											 &nbsp;
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="block_type_center">
						 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doc",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "doc",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "63",
		"IBLOCK_TYPE" => "rko",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "127",
		"PARENT_SECTION_CODE" => "Документы для скачивания",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"FILE",2=>"",3=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "NAME",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="right_top_line">
						</div>
					</div>
					<div class="block_type_center">
						 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"doc",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "doc",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "63",
		"IBLOCK_TYPE" => "rko",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "128",
		"PARENT_SECTION_CODE" => "Документы для скачивания ",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"FILE",2=>"",3=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "NAME",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>