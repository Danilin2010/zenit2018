<table class="tb">
							<thead>
							<tr>
								<td>Bank</td>
								<td>S.W.I.F.T. code</td>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td colspan="2">USA</td>
							</tr>
							<tr>
								<td>Citibank NA, New York</td>
								<td>CITIUS33</td>
							</tr>
							<tr>
								<td>JP Morgan Chase Bank</td>
								<td>CHASUS33</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">Germany</td>
							</tr>
							<tr>
								<td>Deutsche Bank AG</td>
								<td>DEUTDEFF</td>
							</tr>
							<tr>
								<td>BHF-BANK AG</td>
								<td>BHFBDEFF</td>
							</tr>
							<tr>
								<td>BHF-BANK AG</td>
								<td>BHFBDEFF</td>
							</tr>
							<tr>
								<td>Commerzbank AG</td>
								<td>COBADEFF</td>
							</tr>
							<tr>
								<td>UniCredit Bank AG</td>
								<td>HYVEDEMM</td>
							</tr>
							<tr>
								<td>Landesbank Berlin AG</td>
								<td>BELADEBE</td>
							</tr>
							<tr>
								<td>VTB Bank (Deutschland) AG</td>
								<td>OWHBDEFF</td>
							</tr>
							<tr>
								<td>DZ Bank AG</td>
								<td>GENODEFF</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">France</td>
							</tr>
							<tr>
								<td>BNP Paribas SA</td>
								<td>BNPAFRPP</td>
							</tr>
							<tr>
								<td>Societe Generale</td>
								<td>SOGEFRPP</td>
							</tr>
							<tr>
								<td>Natixis</td>
								<td>CCBPFRPP</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">Austria</td>
							</tr>
							<tr>
								<td>Raiffeisen Bank International AG</td>
								<td>RZBAATWW</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">United Kingdom</td>
							</tr>
							<tr>
								<td>Barclays Bank Plc.</td>
								<td>BARCGB22</td>
							</tr>
							<tr>
								<td>VTB Capital Plc</td>
								<td>MNBLGB2L</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">Italy</td>
							</tr>
							<tr>
								<td>Intesa SanPaolo SpA </td>
								<td>BCITITMM</td>
							</tr>
							<tr>
								<td>Unicredito Italiano SpA</td>
								<td>UNCRITMM</td>
							</tr>
							<tr>
								<td>Banca Monte dei Paschi di Siena SpA</td>
								<td>PASCITMM</td>
							</tr>
							<tr>
								<td>Banco Popolare Group </td>
								<td>BAPPIT22</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">Netherlands</td>
							</tr>
							<tr>
								<td>ING Bank N.V.</td>
								<td>INGBNL2A</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">Switzerland</td>
							</tr>
							<tr>
								<td>UBS AG</td>
								<td></td>
							</tr>
							<tr>
								<td>Credit Suisse (Switzerland) AG</td>
								<td></td>
							</tr>
							<tr>
								<td>Banque de Commerce et de Placements</td>
								<td>BPCPCHGG</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">Luxembourg</td>
							</tr>
							<tr>
								<td>Clearstream Banking SA</td>
								<td>CEDELULL</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">India</td>
							</tr>
							<tr>
								<td>State Bank of India</td>
								<td>SBININBB</td>
							</tr>
							<tr>
								<td>Canara Bank</td>
								<td>CNRBINBB</td>
							</tr>
							<tr>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="2">China</td>
							</tr>
							<tr>
								<td>Bank of China Limited </td>
								<td>BKCHCNBJ</td>
							</tr>
							<tr>
								<td>Industrial and Commercial Bank of China</td>
								<td>ICBKCNBJ</td>
							</tr>
							<tr>
								<td>Agricultural Bank of China</td>
								<td>ABOCCNBJ</td>
							</tr>
							</tbody>
						</table>