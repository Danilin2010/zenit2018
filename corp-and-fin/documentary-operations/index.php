<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Документарные операции");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/all.png');
?>

	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">
				<div class="wr_max_block">
					<h2>Контакты:</h2>
					<div class="form_application_line">
						<div class="contacts_block">
							Юлия Викторовна Демонтович <br>
							+7 (495) 937-07-37, доб. 2564<br>
							e-mail: <a href="mailto:j.demontovich@zenit.ru">j.demontovich@zenit.ru</a>
						</div>
						<div class="note_text">
							Заместитель начальника Департамента финансового консалтинга и международных проектов
						</div>
					</div>
					<div class="form_application_line">
						<div class="contacts_block">
							Бармин Андрей Леонидович <br>
							 +7 (495) 937-07-37, доб. 2294<br>
						</div>
						<div class="note_text">
							Начальник Отдела документарных операций
						</div>
					</div>
				</div>
			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						Банк ЗЕНИТ имеет значительный опыт в сфере проведения операций с использованием документарных
						аккредитивов и банковских гарантий, а также прочные контакты и доверие со стороны многих западных
						банков. Мы имеем возможность предложить Вам содействие в проведении Вашими клиентами документарных
						операций и операций по финансированию международной торговли и предлагаем следующие услуги:
					</p>
					<ul>Аккредитивы:
						<li>
							Авизование, исполнение и подтверждение экспортных аккредитивов, в т.ч. аккредитивов, открытых
							банками стран СНГ;
						</li>
						<li>
							Выпуск рамбурсных обязательств (IRU).
						</li>
					</ul>
					<ul>Гарантии:
						<li>
							Выдача банковских гарантий и контргарантий в пользу российских и зарубежных организаций;
						</li>
						<li>
							Банковские гарантии в пользу таможенных органов России;
						</li>
						<li>
							Авизование гарантий.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>