<ul>
	<li>Банк ЗЕНИТ (публичное акционерное общество) / ПАО Банк ЗЕНИТ</li>
	<li>Адрес: 129110, Москва, Банный переулок, д.9</li>
</ul>
<p>
	к/с № 30101810000000000272 в ГУ Банка России по ЦФО;<br>
	 БИК 044525272
</p>
<p>
	 ИНН 7729405872<br>
	 КПП 997950001
</p>
 К/с в долларах США:
<ul>
	<li>№36312321 в Citibank NA, New York, SWFT: CITIUS33.</li>
</ul>
 К/с в ЕВРО:
<ul>
	<li>№&nbsp;400888055100 EUR&nbsp;в Commerzbank AG, Frankfurt am Main, SWIFT: COBADEFF;</li>
	<li>№ 0103571014 в VTB Bank (Deutschland) AG, Frankfurt am Main, SWIFT: OWHBDEFF.</li>
</ul>
<p>
	 Тел: (495) 937-0737; 777-5707 <br>
	 Факс: (495) 937-07-36; 777-5706
</p>
<p>
	 ОКПО 29325987 ОКОГУ 49011 ОКАТО 45286570000<br>
	 ОКВЭД 65.12 ОКФС 16 ОКОПФ 47
</p>
<p>
	 TELEX: 485506 ZENT RU <br>
	 S.W.I.F.T.: ZENIRUMM <br>
	 REUTER-DEALING: ZENT
</p>