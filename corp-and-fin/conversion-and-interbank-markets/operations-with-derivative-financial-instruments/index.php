<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Операции с ПФИ");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/the-financial-institutions/convers.png');
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
			<div class="wr_max_block">
				<h2>Контакты</h2>
				<div class="form_application_line">
					<div class="contacts_block">
						 Александр Валканов
					</div>
					<div class="note_text">
						+7 (495) 937-07-37 доб. 3404
					</div>
				</div>
				<div class="form_application_line">
					<div class="contacts_block">
						Юлия Паршина
					</div>
					<div class="note_text">
						+7 (495) 937-07-37 доб. 2856
					</div>
				</div>
			</div>
		</div>
		<div class="block_type_center">
			<h1></h1>
			<div class="text_block">
				<p>
					 ПАО Банк ЗЕНИТ осуществляет операции с валютными и процентными деривативами на межбанковском рынке. В качестве договорной базы выступает типовая документация ISDA/RISDA.
				</p>
			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>