<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Программы по банковским картам для банков-партнеров");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/all.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Оказываемые услуги</a></li>
			<li><a href="#content-tabs-2">Дополнительные предложения</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<ul class="big_list">
							<li>
							<p>
								 Содействие по вступлению в Международные платежные системы Visa International и MasterCard Worldwide.&nbsp;
							</p>
							<p>
								 Банк ЗЕНИТ предлагает Вам рассмотреть возможность вступления в МПС Visa International в качестве Associate Member и/или в МПС MasterCard Worldwide в качестве Affiliate Member.
							</p>
 </li>
							<li>
							<p>
								 Организация выпуска банковских карт Международных платежных систем Visa International &nbsp; и MasterCard Worldwide.&nbsp;
							</p>
							<p>
								 Банк ЗЕНИТ предлагает программу эмиссии карт МПС Visa International и MasterCard Worldwide.
							</p>
 </li>
							<li>
							<p>
								 Эквайринговая программа.&nbsp;
							</p>
							<p>
								 Банк ЗЕНИТ, являясь принципиальным членом международных платежных систем Visa International, MasterCard Worldwide, UnionPay International и платежной системы "Мир", предоставляет Вашему банку возможность обслуживать в торговых точках банковские карты международных платежных систем: Visa International, MasterCard Worldwide, UnionPay International, American Express и платежной системы "Мир".
							</p>
							<p>
 <!--a href="/upload/docs/corp/card bank partner/acquiring_referal_corp_20170518.pdf" target="_blank"-->
<a href="/upload/medialibrary/84a/acquiring_corp_20180115.pdf" target="_blank">Правила&nbsp;оказания</a> ПАО Банк ЗЕНИТ юридическим лицам и индивидуальным предпринимателям,&nbsp;привлеченным партнером&nbsp;Услуги «Торговый эквайринг»
<br>
 <!--a href="/upload/medialibrary/84a/acquiring_corp_20180115.pdf" target="_blank">Правила</a> вступают в действие с 15.01.2018г.-->
							</p>
 </li>
							<li>
							<p>
								 Содействие по вступлению в платежную систему "Мир"&nbsp;
							</p>
							<p>
								 Банк ЗЕНИТ предлагает Вам рассмотреть возможность вступления в платежную систему "Мир" в качестве Косвенного участника и организовать выпуск и обслуживание платежных карт "Мир".
							</p>
 </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<ul class="big_list">
							<li>
							<p>
								 SMS-информирование клиентов Банка-партнера о проведении операции.&nbsp;
							</p>
							<p>
								 Клиент получает SMS-оповещение при каждой операции снятия наличных, пополнения карточного счета и при совершении операции в ТСП
							</p>
 </li>
							<li>
							<p>
								 Организацию приема платежей в адрес сторонних получателей в банкоматах Вашего банка.
							</p>
 </li>
							<li>
							<p>
								 Предоставление сотрудникам Банка-партнера удаленных рабочих мест для доступа в режиме on-line к информации о своих клиентах, специальных карточных счетах, картах, устройствах и операциях по ним.
							</p>
 </li>
						</ul>
						<p>
 <a href="/offices/?type=atm" target="_parent">Банкоматная сеть Банка ЗЕНИТ</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>