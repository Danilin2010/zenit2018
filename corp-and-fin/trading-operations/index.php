<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Торговые операции");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/the-financial-institutions/operation.png');
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
			<div class="wr_max_block">
				<h2>Контакты</h2>
				<div class="form_application_line">
					<div class="contacts_block">
						 Александр Валканов<br>
						<a href="mailto:a.valkanov@zenit.ru">a.valkanov@zenit.ru</a><br>
						+7 (495) 937-07-37 доб. 3404
						<div class="note_text">
							 Управление продаж
						</div>
					</div>
					<div class="contacts_block">
						Юлия Паршина<br>
						<a href="mailto:у.parshina@zenit.ru">у.parshina@zenit.ru</a><br>
						+7 (495) 937-07-37 доб. 2856
						<div class="note_text">
							 Управление продаж
						</div>
					</div>
 <a href="#modal_form-contact" class="button bigmaxwidth open_modal">задать вопрос</a>
				</div>
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p>
					 Банк ЗЕНИТ является одним из ключевых участников российского рынка ценных бумаг.
				</p>
				<ul>
					 Наши специалисты помогут вам осуществить различные виды операций с ценными бумагами:
					<li>покупку и продажу рублевых облигаций;</li>
					<li>покупку и продажу еврооблигаций российских и иностранных эмитентов;</li>
					<li>покупку облигаций и евробондов для клиентов на первичных размещениях;</li>
					<li>покупку и продажу американских и европейских депозитарных расписок (ADR, GDR).</li>
				</ul>
				<p>
					 Кроме того, Банк ЗЕНИТ является одним из крупнейших операторов российского вексельного рынка и осуществляет торговые операции как с собственными векселями, так и с векселями сторонних векселедателей.
				</p>
				<p>
					 Банк размещает собственные векселя со сроками погашения от 3 мес. до 1 года. Ставки размещения зависят от рыночной конъюнктуры и необходимости в финансировании на текущий момент.
				</p>
				<p>
					 <!-- Более подробную информацию об эмитентах и облигациях можно получить в разделе <a href="#" target="_blank">Аналитика по рынку ценных бумаг</a>. Также дополнительно можно ознакомиться с информацией о выпусках <a href="#" target="_blank">облигаций</a>, подготовленных Банком ЗЕНИТ .-->
				</p>
			</div>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>