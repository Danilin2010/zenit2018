<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Операции РЕПО с ценными бумагами");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/the-financial-institutions/operation.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

				<div class="wr_max_block">
					<h2>Контакты</h2>
					<div class="form_application_line">
						<div class="contacts_block">
							Александр Валканов
							<a href="mailto:a.valkanov@zenit.ru">a.valkanov@zenit.ru</a><br>
							+7 (495) 937-07-37 доб. 3404
							<div class="note_text">
								Управление продаж
							</div>
						</div>
						<div class="contacts_block">
							Юлия Паршина
							<a href="mailto:у.parshina@zenit.ru">у.parshina@zenit.ru</a><br>
							+7 (495) 937-07-37 доб. 2856
							<div class="note_text">
								Управление продаж
							</div>
						</div>
						<a href="#" class="button bigmaxwidth">задать вопрос</a>
					</div>
				</div>

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						Банк ЗЕНИТ входит в группу ведущих в России операторов по финансированию под обеспечение ценных
						бумаг (операции РЕПО).
					</p>
					<ul>Банк проводит операции РЕПО со следующими ценными бумагами:
						<li>государственные, муниципальные, корпоративные и банковские облигации, номинированные в рублях; </li>
						<li>суверенные еврооблигации; </li>
						<li>корпоративные и банковские еврооблигации; </li>
						<li>векселя; </li>
						<li>акции российских предприятий (голубые фишки) и ADR на них. </li>
					</ul>
					<p>
						Операции РЕПО проводятся сроком от 1 дня с возможностью пролонгации в рамках установленных
						лимитов на ценные бумаги, являющихся обеспечением по сделке.
					</p>
					<p>
						Операции РЕПО с рублевыми облигациями заключаются на ПАО "Московская Биржа".
					</p>
					<p>
						Внебиржевые операции РЕПО осуществляются в рамках Генерального соглашения об общих условиях
						заключения договоров РЕПО на рынке ценных бумаг.
					</p>
					<p>
						Операции РЕПО с облигациями, номинированными в иностранной валюте, заключаются в валюте облигаций.
					</p>
					<p>
						Размер процентной ставки по операциям РЕПО оговариваются при заключении сделки.
					</p>
				</div>
			</div>
		</div>
	</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>