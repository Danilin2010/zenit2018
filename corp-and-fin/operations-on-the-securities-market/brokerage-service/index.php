<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Брокерское обслуживание");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/the-financial-institutions/operation.png');
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p>
					 Банк ЗЕНИТ предлагает российским и западным инвесторам (в т.ч. финансовым институтам) полный спектр услуг на российском и международном фондовых рынках.
				</p>
				<p>
					 Банк ЗЕНИТ является универсальным брокером, осуществляющим операции на всех основных торговых площадках России: ПАО "Московская Биржа", FORTS, а также на ведущих мировых торговых площадках - NASDAQ, NYSE, LSE и др., что позволяет клиентам Банка совершать сделки на наиболее выгодных условиях с минимальными затратами времени и средств.
				</p>
				<p>
					 Банк ЗЕНИТ предлагает ежедневные обзоры фондового рынка на сайте в Интернете с возможностью рассылки обзоров по электронной почте и консультационную поддержку специалистов Аналитического Управления Банка.
				</p>
				<p>
					<!-- Более подробную информацию можно получить <a href="#" target="_blank">здесь</a>-->
				</p>
			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>