<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Индивидуальные инвестиционные счета");
?>
<div class="wr_block_type">
<div class="block_type to_column c-container">
	<div class="block_type_center" style="text-align:justify">
<p>С 1 января 2015 года клиентам Банка ЗЕНИТ доступна новая услуга &mdash; открытие Индивидуальных инвестиционных счетов (ИИС). Эти счета открываются в соответствии с недавними изменениями в законодательстве, которые позволяют физическим лицам воспользоваться налоговой льготой при инвестировании средств на фондовом рынке.</p>
<p>Инвестор может выбрать два варианта получения налогового вычета: с сумм, первоначально зачисленных на ИИС, или при закрытии ИИС &mdash; с полученной прибыли. <!--a onclick="Hider('ur'); return false" href="#">показать/скрыть</a--></p>
<!--div id="ur" style="display: none;"-->
<blockquote>
<p><strong>1 вариант. Вычет предоставляется в размере сумм, зачисленных на ИИС.</strong></p>
<p>Инвестор может уменьшить свою налогооблагаемую базу не более чем на 400 000 рублей в год, и вернуть из бюджета 13% от этой суммы - 52 000 рублей. Для этого инвестор должен самостоятельно подать налоговую декларацию и заявление о возврате налога в налоговую инспекцию по месту жительства.&nbsp;</p>
<p><strong>2 вариант. Вычет предоставляется в размере положительного финансового результата на момент закрытия счета.</strong></p>
<p>Инвестор не платит налог с прибыли при закрытии ИИС. Вычет предоставляет брокер или доверительный управляющий, то есть инвестору не нужно подавать декларацию в налоговую инспекцию самостоятельно.</p>
<p><strong>Как определиться с типом налогового вычета</strong></p>
<p>При условии, что инвестор вносит единовременно на счет 400 тыс. руб. и больше его не пополняет, можно ориентироваться на &laquo;правило 100%&raquo;: если инвестор предполагает, что по итогам 3-х лет инвестирования он заработает более 400 тыс. руб. прибыли, то выгоднее использовать вариант 2 &ndash; вычет в размере всей суммы полученной прибыли. Если ожидания инвестора в части прибыли более скромные, выгоднее использовать вариант 1 &ndash; ежегодный вычет в размере 400 тыс. руб., подавать документы в налоговую инспекцию ежегодно и реинвестировать суммы возврата.</p>
<p>При расчете эффективности вложений и реинвестировании сумм возврата налога необходимо учитывать, что вернуть налог можно примерно в мае года, следующего за отчетным, при условии подачи налоговой декларации в январе. То есть если в течение 2015 года на ИИС внести 400 000 рублей, то вернуть 52 000 рублей из бюджета получится в середине 2016 года.</p>
<p>Следует отметить, что инвестору необязательно определятся с выбором налоговой льготы сразу при открытии ИИС. Это можно сделать и позже, оценив фактическую доходность операций по счету, но при условии, что инвестор ранее не подавал заявление на возврат налога, то есть не воспользовался первым вариантом налогового вычета. Выбор лучше сделать не позже трех лет с момента открытия ИИС, так как в соответствии с Налоговым Кодексом РФ возместить налог из бюджета можно только за последние три года.</p>
</blockquote>
<!--/div-->
<p>В соответствии с законодательством, максимальная сумма, которую можно зачислить на ИИС, ограничена 400 000 руб. в год. Налоговая льгота действует при условии, что инвестор не закрывает ИИС в течение 3-лет с момента открытия. Допускается наличие у инвестора только одного ИИС.</p>
<p>В рамках услуги по открытию и сопровождению ИИС Банк ЗЕНИТ предоставит инвесторам несколько опций:</p>
<ul class="big_list">
<li>клиенты могут самостоятельно определять стратегию инвестирования и управлять своими средствами в рамках&nbsp;брокерского обслуживания.</li>
<li>передача средств в доверительное управление Банку ЗЕНИТ.&nbsp;</li>
</ul>
<p><strong><a title="index" href="/personal/brokerage-service/about-the-service/">Брокерское обслуживание</a></strong></p>
<p>В рамках брокерского ИИС инвестор самостоятельно определяет стратегию инвестирования. В отличие от доверительного управления, где управляющий принимает решение о том, куда инвестировать денежные средства инвестора, размещенные на ИИС, при брокерском обслуживании инвестор имеет возможность самостоятельно выбрать как консервативную стратегию для инвестиций, так и более рискованную с более высоким ожидаемым доходом. В любой момент времени инвестор по брокерскому обслуживанию может изменить свою стратегию. При желании, инвестору устанавливается система интренет-трейдинга для непосредственного доступа к торгам и возможности самостоятельного совершения сделок. Так же инвестор может получать информацию о текущей стоимости своих активов, помощь в совершении сделок позвонив брокеру по телефону.</p>
<p><strong><a title="index" href="/personal/invest/individual-trust-management.php">Доверительное управление</a></strong></p>
<p>В рамках индивидуального инвестиционного счета мы предлагаем инвесторам консервативную стратегию управления. 100% денежных средств размещаются в облигации. При этом предпочтение будет отдаваться государственным и субфедеральным облигациям. Наряду с высокой надежностью эмитента, плюсом таких ценных бумаг является необлагаемый налогом купонный доход. Это увеличивает эффективность инвестирования по сравнению с корпоративными бумагами.</p>

    </div>
   </div>
  </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>