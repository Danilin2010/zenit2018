<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стратегия «Сбалансированная»");
?>			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<h3>Контакты:</h3>
<div class="contacts_block">
	Анатолий Жарков<br><a href="mailto:a.zharkov@zenit.ru">a.zharkov@zenit.ru</a><br>
	+7 (495) 933-03-62
	<div class="note_text">
	</div>
</div>
					</div>
					<div class="block_type_center">
<p>Больше прибыль &mdash; больше риск. Эта стратегия потенциально принесет большую прибыль, чем &laquo;Защитная&raquo;, но при этом доходность портфеля в течение периода управления может значительно колебаться. Основная часть средств, также как и при защитной стратегии, вкладывается в надежные облигации: государственные, муниципальные, корпоративные. Доля более рискованных ценных бумаг может составлять до 30% от общего объема портфеля.</p>
<p>Вознаграждение доверительного управляющего &mdash; 1,50% годовых от стоимости имущества в управлении.</p>
<p>Премия доверительного управляющего &mdash; зависит от прироста стоимости имущества в управлении и рассчитывается согласно таблице:</p>
<table class="nums" style="width: 100%; border: #cdcdcd 0px solid;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="cap">
<td valign="top">Доходность вложений, % годовых</td>
<td valign="top">Премия, % от величины прироста стоимости имущества</td>
</tr>
<tr>
<td valign="top">До 6</td>
<td valign="top">0</td>
</tr>
<tr>
<td valign="top">От 6 до 15</td>
<td valign="top">10% от дохода свыше 6% годовых</td>
</tr>
<tr>
<td valign="top">Свыше 15</td>
<td valign="top">10% от дохода свыше 6% годовых + 20% от дохода свыше 15% годовых</td>
</tr>
</tbody>
</table>
					</div>
				</div>
			</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>