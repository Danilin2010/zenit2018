<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Стратегия «Активная»");
?>			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<h3>Контакты:</h3>
<div class="contacts_block">
	Анатолий Жарков<br><a href="mailto:a.zharkov@zenit.ru">a.zharkov@zenit.ru</a><br>
	+7 (495) 933-03-62
	<div class="note_text">
	</div>
</div>
					</div>
					<div class="block_type_center">
<p>Лучший вариант для тех, кто готов рискнуть, чтобы получить весомую прибыль. Ваши средства вкладываются в акции стабильно растущих предприятий, значительное внимание уделяется повышению доходности портфеля за счет краткосрочных операций. Структура портфеля постоянно изменяется в соответствии с изменениями обстановки на рынке. Наша работа &mdash; обратить эти колебания в ваш доход.</p>
<p>Вознаграждение доверительного управляющего &mdash; 1,50% годовых от стоимости имущества в управлении.</p>
<p>Премия доверительного управляющего &mdash; зависит от прироста стоимости имущества в управлении и рассчитывается согласно таблице:</p>
<table class="nums" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="cap">
<td valign="top">Доходность вложений, % годовых</td>
<td valign="top">Премия, % от величины прироста стоимости имущества</td>
</tr>
<tr>
<td valign="top">До 6</td>
<td valign="top">0</td>
</tr>
<tr>
<td valign="top">От 6 до 25</td>
<td valign="top">15% от дохода свыше 6% годовых</td>
</tr>
<tr>
<td valign="top">От 25 до 35</td>
<td valign="top">15% от дохода свыше 6% годовых + 25% от дохода свыше 25% годовых</td>
</tr>
<tr>
<td valign="top">Свыше 35</td>
<td valign="top">15% от дохода свыше 6% годовых + 25% от дохода свыше 25% годовых + 30% от дохода свыше 35% годовых</td>
</tr>
</tbody>
</table>
<p>Базовые параметры доверительного управления активами.</p>
<p>Рекомендуемый срок управления &mdash; минимум 12 месяцев.</p>
<p>Минимальная сумма, передаваемая в управление &mdash; 10 миллионов рублей.</p>
					</div>
				</div>
			</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>