<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Страховые и пенсионные программы | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Страховка и пенсия",
		"PRODUCTHEADER_IMG" => "/personal/pension-insurance/img/pensionCatalogIconBig.png"
	)
);?> 
<div class="tab-content tab-content_state-active" id="classic"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Пенсионные программы",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет максимально выгодно использовать накопительную часть пенсии",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/pension/personal/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Узнать больше",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/personal/pension/personal/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Инвестиционное страхование",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет защитить себя от непредвиденных ситуаций и получить дополнительный доход",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array("Страхование жизни и инвестиционный доход","Гарантированный возврат стоимости полиса"),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/insurance/life/investment/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Узнать больше",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/personal/insurance/life/investment/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Страхование здоровья",
		"SIDECOLUMN_TEXT" => "Для тех, кто любит себя и своих близких, предпочитая страховать здоровье от непредвиденных ситуаций",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/insurance/life/card/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Узнать больше",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/personal/insurance/life/card/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Страхование жилья",
		"SIDECOLUMN_TEXT" => "Для тех, кто беспокоится за сохранность и безопасность своего дома или&nbsp;квартиры",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/insurance/life/card/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Узнать больше",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/personal/insurance/life/card/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Страховка для карты",
		"SIDECOLUMN_TEXT" => "Для тех, кто выбирает повышенную безопасность, желая обеспечить максимальную защиту средств на&nbsp;карте",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/insurance/life/card/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Узнать больше",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/personal/insurance/life/card/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Страховка для кредита",
		"SIDECOLUMN_TEXT" => "Финансовая защита для тех, кто планирует взять кредит и&nbsp;хочет застраховаться от&nbsp;непредвиденных ситуаций",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/insurance/life/credit/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Узнать больше",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/personal/insurance/life/credit/"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>