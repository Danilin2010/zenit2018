<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Брокерское и депозитарное обслуживание, доверительное управление | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Иинвестиции",
		"PRODUCTHEADER_IMG" => "/personal/investment/img/investmentIconBig.png"
	)
);?> 
<div class="tab-content tab-content_state-active" id="classic"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 </div>
         				</div>
       			</div>
     
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 							 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Брокерское<br>обслуживание",
		"RECOMMENDED_TEXT" => "",
		"RECOMMENDED_LINK" => "/personal/brokerage-service/",
		"RECOMMENDED_IMG" => "/personal/investment/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Депозитарное<br>обслуживание",
		"RECOMMENDED_TEXT" => "",
		"RECOMMENDED_LINK" => "/personal/invest/depositary-operations/",
		"RECOMMENDED_IMG" => "/personal/investment/img/information.svg"
	)
);?> 					 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Доверительное<br>управление",
		"RECOMMENDED_TEXT" => "",
		"RECOMMENDED_LINK" => "/personal/invest/individual-trust-management.php",
		"RECOMMENDED_IMG" => "/personal/investment/img/additional.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Инвестиционные<br>счета",
		"RECOMMENDED_TEXT" => "",
		"RECOMMENDED_LINK" => "/personal/invest/individual-investment-accounts.php",
		"RECOMMENDED_IMG" => "/personal/investment/img/additional.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Общие фонды<br>банковского управления",
		"RECOMMENDED_TEXT" => "",
		"RECOMMENDED_LINK" => "http://invest.zenit.ru/ofbu/",
		"RECOMMENDED_IMG" => "/personal/investment/img/realty.svg"
	)
);?> </div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>