<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Хранение денежных средств в сейфовых ячейках");
/*BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');*/
?><?
use \Bitrix\Main\Page\Asset;
?>
<div class="wr_block_type">
     <!--content_tab-->
    <div class="content_rates_tabs">
	
        <ul class="content_tab c-container">
            <li><a href="#content-tabs-1">Описание</a></li>
            <li><a href="#content-tabs-2">Где оформить</a></li>
            <li><a href="#content-tabs-3">Документы</a></li>
        </ul>
		
        <div class="content_body" id="content-tabs-1">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
                        <div class="text_block">
							<ul class="big_list">
								<li>Помещение денежных средств в ячейку осуществляется клиентом в присутствии работника Банка.</li>
								<li>Составляется двусторонний акт приема-передачи, в котором отражаются все передаваемые на хранение денежные средства.</li>
								<li>Доступ к ячейке  возможен только при одновременном присутствии клиента и работника Банка.</li>
								<li>Проверка и пересчет денежных средств клиента осуществляется кассовым работником Банка в присутствии клиента.</li>
								<li>Банк предоставляет технические средства контроля для пересчета и проверки (установления подлинности) денежных средств.</li>
								<li>Возможность предоставления права пользования ячейкой доверенному лицу.</li>
								<li>Возможность предоставления доступа к ячейке третьих лиц совместно с клиентом или его доверенным лицом.</li>
								<li>Договор заключается на срок от 7-ми до 365-ти дней.</li>
								<li>Возможность выбора размера ячейки.</li>
								<li>Ключ от ячейки хранится у клиента.</li>
							</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="content_body" id="content-tabs-2">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
                        <div class="text_block">
							<p>Сейфовые ячейки расположены в специально оборудованных хранилищах Банка по следующим адресам:</p>
							<ul class="big_list">
								<li>Головной офис, г. Москва, Банный пер., д. 9, тел.: +7 (495) 937-07-37, доб. 2954.</li>
								<li>Дополнительный офис «Пресненский», г. Москва, ул.Пресненский Вал, д.16, стр.3, тел.: +7 (495) 937-36-64.</li>
								<li>Клиентский центр «Садовая Слобода», г. Москва, ул. Садовническая, д. 69, тел.: +7 (495) 980-75-78.</li>
								<li>Дополнительный офис «Мытищи», Московская область, г. Мытищи, ул. Мира, д. 24/5, тел.: +7 (495) 940-91-14.</li>
							</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		        <div class="content_body" id="content-tabs-3">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
						<h2></h2>						
						<div class="text_block">
							<div class="doc_list">
								<a href="/upload/iblock/8fb/tariffs_storage_20151026.pdf" target="_blank">
									<div class="doc_pict pdf">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Тарифы по хранению денежных средств с использованием индивидуальных сейфовых ячеек (в Головном офисе и дополнительных офисах Банка, расположенных в Москве и Московской области)
										</div>
										<div class="doc_note">
										</div>
									</div>
								</a>
							</div>
							<div class="doc_list">
								<a href="/media/doc/safe/tariffs_storage_20180420.pdf" target="_blank">
									<div class="doc_pict pdf">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Тарифы вступающие в действие с 20.04.2018
										</div>
										<div class="doc_note">
										</div>
									</div>
								</a>
							</div>
							<br />
                        <!--/div>
						<div class="text_block"-->
							<div class="doc_list">
								<a href="/upload/iblock/39a/tariffs_storage_kursk_20180326.pdf" target="_blank">
									<div class="doc_pict pdf">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Тарифы ПАО Банк ЗЕНИТ по хранению денежных средств с использованиеминдивидуальных сейфовых ячеек в Операционном офисе «На Ленина» (г. Курск)
										</div>
										<div class="doc_note">
										</div>
									</div>
								</a>
							</div>
							<div class="doc_list">
								<a href="/media/doc/safe/tariffs_storage_kursk_20180420.pdf" target="_blank">
									<div class="doc_pict pdf">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Тарифы вступающие в действие с 20.04.2018
										</div>
										<div class="doc_note">
										</div>
									</div>
								</a>
							</div>
							<br />
                        <!--/div>
						<div class="text_block"-->

                        </div>
						<!--div class="text_block">
							<div class="doc_list">
								<a href="" target="_blank">
									<div class="doc_pict pdf">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											
										</div>
										<div class="doc_note">
										</div>
									</div>
								</a>
							</div>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
		
    </div>
     <!--content_tab-->	
</div>						
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>