<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Интернет-банк, платежи, переводы с карты на карту | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Сервисы и услуги",
		"PRODUCTHEADER_IMG" => "/personal/services/img/servicesCatalogIconBig.png"
	)
);?> 
<div class="tab-content tab-content_state-active" id="classic"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Интернет-банк и&nbsp;приложения",
		"SIDECOLUMN_TEXT" => "Для просмотра информации по картам, анализа расходов, удобных платежей, переводов и&nbsp;оформления продуктов",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "https://my.zenit.ru/wb/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Войти в интернет-банк",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/remote-service/internet-bank/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/remote-service/internet-bank/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Переводы с&nbsp;карты на&nbsp;карту",
		"SIDECOLUMN_TEXT" => "Быстрые и безопасные переводы между картами любых банков, защищенные 3D Secure",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => " ",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => " ",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "https://zenit.dengisend.ru/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Сделать перевод",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/payments/transfers/card-to-card/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Тарифы и условия",
		"SIDECOLUMN_LINK_CART" => "/personal/payments/transfers/card-to-card/"
	)
);?> 				</div>
         				</div>
       			</div>
     
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Золотая Корона",
		"RECOMMENDED_TEXT" => "Быстрые переводы по&nbsp;России и&nbsp;за&nbsp;рубеж",
		"RECOMMENDED_LINK" => "/personal/payments/transfers/sistema-zolotaya-korona/",
		"RECOMMENDED_IMG" => "/personal/services/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Western Union",
		"RECOMMENDED_TEXT" => "Мировой лидер в сфере переводов",
		"RECOMMENDED_LINK" => "/personal/payments/transfers/sistema-westrn-union/",
		"RECOMMENDED_IMG" => "/personal/services/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Система &laquo;Город&raquo;",
		"RECOMMENDED_TEXT" => "Быстрые переводы и&nbsp;оплата услуг",
		"RECOMMENDED_LINK" => "/personal/payments/transfers/perevody-v-sisteme-gorod/",
		"RECOMMENDED_IMG" => "/personal/services/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Налоговый вычет",
		"RECOMMENDED_TEXT" => "Возможность получить налоговый вычет при покупке жилой недвижимости",
		"RECOMMENDED_LINK" => "/personal/tax-deduction/",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> 	<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Хранение денег",
		"RECOMMENDED_TEXT" => "Использование хранилища банка для хранения крупных сумм денег",
		"RECOMMENDED_LINK" => "/personal/safes/storing-cash-in-safe-deposit-boxes/",
		"RECOMMENDED_IMG" => "/personal/services/img/information.svg"
	)
);?> 	<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Аренда ячейки",
		"RECOMMENDED_TEXT" => "Хранение ценностей с полной конфиденциальностью о&nbsp;вложениях",
		"RECOMMENDED_LINK" => "/personal/safes/safe-deposit-box-rental/",
		"RECOMMENDED_IMG" => "/personal/services/img/information.svg"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>