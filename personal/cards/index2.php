<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Карты");
?>


		<main class="content">
			<!-- Продуктовый хедер-->
			<div class="product-header">
				<div class="product-header__wrapper">
					<div class="product-header__inner">
						<div class="product-header__title-wrapper">
							<h1 class="title title_family-secondary-black title_size-h1 title_color-secondary">Карты</h1>
						</div>
						<div class="product-header__image-wrapper">
							<!-- Картинка продуктового хедера-->
							<img class="product-header__image" src="./static/product-header/card-image-1.svg">
						</div>
					</div>
				</div>
				<div class="tabs-controls" id="tabs-controls-app">
					<div class="tabs-controls__wrapper">
						<nav class="tabs-controls__inner">
							<!-- Кнопки табов. Активная кнопка - tab-control_state-active  В href - id таба с контентом-->
							<a class="tabs-controls__tab-control tabs-controls__tab-control_state-active" href="#debit-cards">
								<span class="tabs-controls__text">Дебетовые</span>
							</a>
							<a class="tabs-controls__tab-control" href="#credit-cards">
								<span class="tabs-controls__text">Кредитные</span>
							</a>
							<a class="tabs-controls__tab-control" href="#one-more-link">
								<span class="tabs-controls__text">Тест скролла много текста</span>
							</a>
						</nav>
					</div>
				</div>
			</div>
			<!-- Активный таб tab-content_state-active Поставить id из href кнопки-->
			<div class="tab-content tab-content_state-active" id="debit-cards">
				<!-- Фон подложки таба-->
				<div class="tab-content__theme tab-content__theme_grey">
					<div class="tab-content__wrapper">
						<div class="tab-content__inner">
							<div class="col col_size-8">
								<div class="product-cards">
									<!-- Для промо карточки класс product-cart_type-promo-->
									<div class="product-card product-card_type-promo">
										<div class="product-card__text-wrapper">
											<!-- Заголовок-->
											<h2 class="product-card__title">Доходный остаток</h2>
											<p class="product-card__text">Карта с начислением до 7.5% годовых на остаток средств, доходность сравнима с доходностью по вкладам</p>
										</div>
										<div class="product-card__image-wrapper">
											<img class="product-card__image" src="./static/product-card/card-1.png">
										</div>
										<div class="product-card__features-wrapper">
											<div class="product-card__feature">
												<span class="product-card__feature-title">12.9
													<span class="product-card__feature-title-small">%</span>
												</span>
												<span class="text text_size-14 product-card__feature-description">минимальная<br>ставка
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">500
													<span class="product-card__feature-title-small">тыс ₽</span>
												</span>
												<span class="text text_size-14 product-card__feature-description">максимальная<br>сумма
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">3
													<span class="product-card__feature-title-small">года</span>
												</span>
												<span class="text text_size-14 product-card__feature-description">минимальный<br>срок
												</span>
											</div>
										</div>
										<div class="product-card__buttons-wrapper">
											<div class="button button_primary product-card__button-action">Заказать карту</div>
											<div class="button button_transparent product-card__button-more">Подробнее</div>
										</div>
									</div>
									<!-- Для обычной белой карточки класс product-cart_type-normal-->
									<div class="product-card product-card_type-normal">
										<div class="product-card__text-wrapper">
											<!-- Заголовок-->
											<h2 class="product-card__title">Автокарта Platinum</h2>
											<p class="product-card__text">Возврат по карте от суммы покупок на АЗС, оплаты парковок, платных дорог, услуг автомоек</p>
										</div>
										<div class="product-card__image-wrapper">
											<img class="product-card__image" src="./static/product-card/card-2.png">
										</div>
										<div class="product-card__features-wrapper">
											<div class="product-card__feature">
												<span class="product-card__feature-title">12.9
													<span class="product-card__feature-title-small">%</span>
												</span>
												<span class="product-card__feature-description">минимальная<br>ставка
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">500
													<span class="product-card__feature-title-small">тыс ₽</span>
												</span>
												<span class="product-card__feature-description">максимальная<br>сумма
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">3
													<span class="product-card__feature-title-small">года</span>
												</span>
												<span class="product-card__feature-description">минимальный<br>срок
												</span>
											</div>
										</div>
										<div class="product-card__buttons-wrapper">
											<div class="button button_primary product-card__button-action">Заказать карту</div>
											<div class="button button_transparent product-card__button-more">Подробнее</div>
										</div>
									</div>
									<!-- Для обычной белой карточки класс product-cart_type-normal-->
									<div class="product-card product-card_type-normal">
										<div class="product-card__text-wrapper">
											<!-- Заголовок-->
											<h2 class="product-card__title">Автокарта Gold</h2>
											<p class="product-card__text">Карта с начислением до 7.5% годовых на остаток средств, доходность сравнима с доходностью по вкладам</p>
										</div>
										<div class="product-card__image-wrapper">
											<img class="product-card__image" src="./static/product-card/card-3.png">
										</div>
										<div class="product-card__features-wrapper">
											<div class="product-card__feature">
												<span class="product-card__feature-title">12.9
													<span class="product-card__feature-title-small">%</span>
												</span>
												<span class="text text_size-14 product-card__feature-description">минимальная<br>ставка
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">500
													<span class="product-card__feature-title-small">тыс ₽</span>
												</span>
												<span class="text text_size-14 product-card__feature-description">максимальная<br>сумма
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">3
													<span class="product-card__feature-title-small">года</span>
												</span>
												<span class="text text_size-14 product-card__feature-description">минимальный<br>срок
												</span>
											</div>
										</div>
										<div class="product-card__buttons-wrapper">
											<div class="button button_primary product-card__button-action">Заказать карту</div>
											<div class="button button_transparent product-card__button-more">Подробнее</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col col_size-4">
								<asside class="products-asside">
									<!-- Фильтр продуктов-->
									<affix class="products-filter" :class="{&quot;products-filter_state-open&quot;: filterStateOpen}" relative-element-selector="#debit-cards">
										<button class="products-filter__control" @click="toggleFilterState()">
											<svg class="icon icon__filter-symbol">
												<use xlink:href="#icon__filter-symbol"></use>
											</svg>
										</button>
										<div class="products-filter__wrapper">
											<div class="products-filter__inner">
												<div class="products-filter__title">Подбор карты</div>
												<button class="products-filter__close" @click="toggleFilterState()">
													<svg class="icon icon__close-symbol">
														<use xlink:href="#icon__close-symbol"></use>
													</svg>
												</button>
												<div class="products-filter__list">
													<div class="checkbox">
														<input type="checkbox" id="501">
														<label class="checkbox__control" for="501">
															<span class="checkbox__control-button">
																<svg class="icon checkbox__icon icon__checkbox-symbol">
																	<use xlink:href="#icon__checkbox-symbol"></use>
																</svg>
															</span>
															<span class="checkbox__control-name">Кэшбэк</span>
														</label>
													</div>
													<div class="checkbox">
														<input type="checkbox" id="502">
														<label class="checkbox__control" for="502">
															<span class="checkbox__control-button">
																<svg class="icon checkbox__icon icon__checkbox-symbol">
																	<use xlink:href="#icon__checkbox-symbol"></use>
																</svg>
															</span>
															<span class="checkbox__control-name">Для накоплений</span>
														</label>
													</div>
													<div class="checkbox">
														<input type="checkbox" id="503">
														<label class="checkbox__control" for="503">
															<span class="checkbox__control-button">
																<svg class="icon checkbox__icon icon__checkbox-symbol">
																	<use xlink:href="#icon__checkbox-symbol"></use>
																</svg>
															</span>
															<span class="checkbox__control-name">Скидки на бензин</span>
														</label>
													</div>
												</div>
											</div>
										</div>
									</affix>
								</asside>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-content" id="credit-cards">
				<!-- Фон подложки таба-->
				<div class="tab-content__theme tab-content__theme_grey">
					<div class="tab-content__wrapper">
						<div class="tab-content__inner">
							<div class="col col_size-8">
								<div class="product-cards">
									<!-- Для обычной белой карточки класс product-cart_type-normal-->
									<div class="product-card product-card_type-normal">
										<div class="product-card__text-wrapper">
											<!-- Заголовок-->
											<h2 class="product-card__title">Автокарта Platinum</h2>
											<p class="product-card__text">Возврат по карте от суммы покупок на АЗС, оплаты парковок, платных дорог, услуг автомоек</p>
										</div>
										<div class="product-card__image-wrapper">
											<img class="product-card__image" src="./static/product-card/card-2.png">
										</div>
										<div class="product-card__features-wrapper">
											<div class="product-card__feature">
												<span class="product-card__feature-title">12.9
													<span class="product-card__feature-title-small">%</span>
												</span>
												<span class="product-card__feature-description">минимальная<br>ставка
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">500
													<span class="product-card__feature-title-small">тыс ₽</span>
												</span>
												<span class="product-card__feature-description">максимальная<br>сумма
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">3
													<span class="product-card__feature-title-small">года</span>
												</span>
												<span class="product-card__feature-description">минимальный<br>срок
												</span>
											</div>
										</div>
										<div class="product-card__buttons-wrapper">
											<div class="button button_primary product-card__button-action">Заказать карту</div>
											<div class="button button_transparent product-card__button-more">Подробнее</div>
										</div>
									</div>
									<div class="product-card product-card_type-normal">
										<div class="product-card__text-wrapper">
											<!-- Заголовок-->
											<h2 class="product-card__title">Автокарта Platinum</h2>
											<p class="product-card__text">Возврат по карте от суммы покупок на АЗС, оплаты парковок, платных дорог, услуг автомоек</p>
										</div>
										<div class="product-card__image-wrapper">
											<img class="product-card__image" src="./static/product-card/card-2.png">
										</div>
										<div class="product-card__features-wrapper">
											<div class="product-card__feature">
												<span class="product-card__feature-title">12.9
													<span class="product-card__feature-title-small">%</span>
												</span>
												<span class="product-card__feature-description">минимальная<br>ставка
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">500
													<span class="product-card__feature-title-small">тыс ₽</span>
												</span>
												<span class="product-card__feature-description">максимальная<br>сумма
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">3
													<span class="product-card__feature-title-small">года</span>
												</span>
												<span class="product-card__feature-description">минимальный<br>срок
												</span>
											</div>
										</div>
										<div class="product-card__buttons-wrapper">
											<div class="button button_primary product-card__button-action">Заказать карту</div>
											<div class="button button_transparent product-card__button-more">Подробнее</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col col_size-4">
								<asside class="products-asside">
									<!-- Фильтр продуктов-->
									<affix class="products-filter" :class="{&quot;products-filter_state-open&quot;: filterStateOpen}" relative-element-selector="#credit-cards">
										<button class="products-filter__control" @click="toggleFilterState()">
											<svg class="icon icon__filter-symbol">
												<use xlink:href="#icon__filter-symbol"></use>
											</svg>
										</button>
										<div class="products-filter__wrapper">
											<div class="products-filter__inner">
												<div class="products-filter__title">Подбор карты</div>
												<button class="products-filter__close" @click="toggleFilterState()">
													<svg class="icon icon__close-symbol">
														<use xlink:href="#icon__close-symbol"></use>
													</svg>
												</button>
												<div class="products-filter__list">
													<div class="checkbox">
														<input type="checkbox" id="519">
														<label class="checkbox__control" for="519">
															<span class="checkbox__control-button">
																<svg class="icon checkbox__icon icon__checkbox-symbol">
																	<use xlink:href="#icon__checkbox-symbol"></use>
																</svg>
															</span>
															<span class="checkbox__control-name">Кэшбэк</span>
														</label>
													</div>
													<div class="checkbox">
														<input type="checkbox" id="520">
														<label class="checkbox__control" for="520">
															<span class="checkbox__control-button">
																<svg class="icon checkbox__icon icon__checkbox-symbol">
																	<use xlink:href="#icon__checkbox-symbol"></use>
																</svg>
															</span>
															<span class="checkbox__control-name">Для накоплений</span>
														</label>
													</div>
													<div class="checkbox">
														<input type="checkbox" id="521">
														<label class="checkbox__control" for="521">
															<span class="checkbox__control-button">
																<svg class="icon checkbox__icon icon__checkbox-symbol">
																	<use xlink:href="#icon__checkbox-symbol"></use>
																</svg>
															</span>
															<span class="checkbox__control-name">Скидки на бензин</span>
														</label>
													</div>
												</div>
											</div>
										</div>
									</affix>
								</asside>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-content" id="one-more-link">
				<!-- Фон подложки таба-->
				<div class="tab-content__theme tab-content__theme_grey">
					<div class="tab-content__wrapper">
						<div class="tab-content__inner">
							<div class="col col_size-8">
								<div class="product-cards">
									<!-- Для обычной белой карточки класс product-cart_type-normal-->
									<div class="product-card product-card_type-promo">
										<div class="product-card__text-wrapper">
											<!-- Заголовок-->
											<h2 class="product-card__title">Автокарта Platinum</h2>
											<p class="product-card__text">Возврат по карте от суммы покупок на АЗС, оплаты парковок, платных дорог, услуг автомоек</p>
										</div>
										<div class="product-card__image-wrapper">
											<img class="product-card__image" src="./static/product-card/card-2.png">
										</div>
										<div class="product-card__features-wrapper">
											<div class="product-card__feature">
												<span class="product-card__feature-title">12.9
													<span class="product-card__feature-title-small">%</span>
												</span>
												<span class="product-card__feature-description">минимальная<br>ставка
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">500
													<span class="product-card__feature-title-small">тыс ₽</span>
												</span>
												<span class="product-card__feature-description">максимальная<br>сумма
												</span>
											</div>
											<div class="product-card__feature">
												<span class="product-card__feature-title">3
													<span class="product-card__feature-title-small">года</span>
												</span>
												<span class="product-card__feature-description">минимальный<br>срок
												</span>
											</div>
										</div>
										<div class="product-card__buttons-wrapper">
											<div class="button button_primary product-card__button-action">Заказать карту</div>
											<div class="button button_transparent product-card__button-more">Подробнее</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>