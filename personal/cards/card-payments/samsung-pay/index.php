<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Платите свободно везде смартфоном или устройством  Gear S3 по картам Mastercard");
$APPLICATION->SetTitle("Сервис Samsung Pay");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/inner/smg_pay.png'); 
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">О сервисе Samsung Pay</a></li>
			<li><a href="#content-tabs-2">Где и как оплачивать покупки</a></li>
			<li><a href="#content-tabs-3">Поддержка пользователей</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="banners c-container">
					<h2>Преимущества платежного сервиса Samsung Pay</h2>
					<div class="wr_banners_item">
						<div class="banners_item">
							<div class="banners_title">
								 Просто
							</div>
							<div class="banners_text">
								Запустите Samsung Pay, проведя по экрану смартфона снизу вверх. Выберите карту и авторизуйтесь по отпечатку пальца или PIN-коду приложения. Поднесите устройство к терминалу для оплаты.
								<br><br><br><br><br><br>
							</div>
							<div class="banners_pict" style="background-image: url('//opt-1125008.ssl.1c-bitrix-cdn.ru/upload/img/easy.png?151921488617248');"></div>
						</div>
					</div>
					<div class="wr_banners_item">
						<div class="banners_item">
							<div class="banners_title">
								Безопасно
							</div>
							<div class="banners_text">
								Вместо номера карты используется специальный цифровой код – токен. Для каждой покупки требуется авторизация по отпечатку пальца или PIN-коду. Специальная встроенная система безопасности устройства Samsung KNOX обеспечивает защиту <br>данных Samsung Pay <br>независимо от операцион-<br>ной системы.
							</div>
							<div class="banners_pict" style="background-image: url('//opt-1125008.ssl.1c-bitrix-cdn.ru/upload/img/secure.png?151921525514643');"></div>
						</div>
					</div>
					<div class="wr_banners_item">
						<div class="banners_item">
							<div class="banners_title">
								Повсеместно
							</div>
							<div class="banners_text">
								Samsung Pay принимается к оплате везде, где можно оплатить покупку картой Mastercard Банка ЗЕНИТ – по бесконтактной технологии или магнитной полосе.
								<br><br><br><br><br><br><br>
							</div>
							<div class="banners_pict" style="background-image: url('//opt-1125008.ssl.1c-bitrix-cdn.ru/upload/img/anywhere.png?151921534514569');"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<img width="200px" src="/upload/img/smart.png" />
					</div>
					<div class="block_type_center">
						<h2>Как добавить карту в Samsung Pay на смартфоне</h2>
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									 1
								</div>
								<div class="step_body">
									<div class="step_title">
										Укажите Вашу действующую учетную запись Samsung Account (e-mail) и настройте удобный для Вас способ авторизации: по отпечатку пальца или по PIN-коду.
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 2
								</div>
								<div class="step_body">
									<div class="step_title">
										Отсканируйте карту с помощью камеры смартфона или введите ее данные вручную. Примите Условия оказания услуг сервиса Samsung Pay и соглашение Банка ЗЕНИТ.
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 3
								</div>
								<div class="step_body">
									<div class="step_title">
										 Нажмите на кнопку SMS для прохождения идентификации держателя карты банка. Введите код, полученный в SMS, и нажмите «Отправить».
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 4
								</div>
								<div class="step_body">
									<div class="step_title">
										Введите подпись с помощью пальца или стилуса. Карта добавлена! На одно устройство можно добавить максимум 10 банковских карт.
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="doc_list">
							<a href="http://images.samsung.com/is/content/samsung/p5/ru/apps/mobile/samsungpay/pdf/instruction_pay-smart.pdf" target="_blank" class="doc_item">
                            	<div class="doc_pict pdf"></div>
                            	<div class="doc_body">
                                	<div class="doc_text">
                                    	Подробная инструкция для пользователя смартфона Samsung Galaxy
									</div>
	                            </div>
    	                    </a>
						</div>

					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<img width="200px" src="/upload/img/gearS3.png" />
					</div>
					<div class="block_type_center">
						<h2>Как добавить карту в Samsung Pay на устройстве GearS3</h2>
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									1
								</div>
								<div class="step_body">
									<div class="step_title">
										Подключите устройство Gear к совместимому смартфону с помощью мобильного приложения Samsung Gear.
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									2
								</div>
								<div class="step_body">
									<div class="step_title">
										В приложении Samsung Gear нажмите «Открыть Samsung Pay»,  войдите в учетную запись Samsung Account и нажмите «Добавить карту».
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									3
								</div>
								<div class="step_body">
									<div class="step_title">
										 На устройстве Gear настройте блокировку экрана по PIN-коду.
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									4
								</div>
								<div class="step_body">
									<div class="step_title">
										Откройте Samsung Pay на устройстве Gear, удерживая клавишу «Назад».
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									5
								</div>
								<div class="step_body">
									<div class="step_title">
										На смартфоне в приложении Samsung Gear отсканируйте банковскую карту либо введите её данные вручную, примите Условия обслуживания Банка.
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									6
								</div>
								<div class="step_body">
									<div class="step_title">
										Нажмите на кнопку SMS для прохождения идентификации держателя карты с помощью одноразового кода по SMS. 
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									7
								</div>
								<div class="step_body">
									<div class="step_title">
										Готово!
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="doc_list">
							<a href="http://images.samsung.com/is/content/samsung/p5/ru/apps/mobile/samsungpay/pdf/instruction.pdf" target="_blank" class="doc_item">
                            	<div class="doc_pict pdf"></div>
                            	<div class="doc_body">
                                	<div class="doc_text">
                                    	Подробная инструкция для пользователя устройства Samsung GearS3
									</div>
	                            </div>
    	                    </a>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<img src="/upload/img/samsung_pay_logo.png" />
						 <!-- <a href="#" class="button mt-3" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Заказать карту</a>-->
					</div>
					<div class="block_type_center">
						<h2>Где платить с Samsung Pay</h2>
						<p>
							Samsung Pay работает везде - даже там, где терминал НЕ поддерживает оплату по "бесконтакту" (NFC).
						</p>
						<p>
							Секрет в том, что устройства Samsung могут создавать магнитное поле, схожее с сигналом магнитной полосы банковской карты. Обычно даже сами продавцы не знают, что такое возможно в их магазинах, но это реально работает.
						</p>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
						<h2>Как платить смартфоном</h2>
						<img width="100%" src="/upload/img/pay_smart.png">
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<h2>Как платить устройством GearS3</h2>
					<img width="100%" src="/upload/img/pay_s3.png">
					<!--<div class="video">
						 <ifr ame title="Samsung Pay | Советы при оплате" width="480" height="270" src="//www.youtube.com/embed/whljLCksgvM?feature=oembed" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
					</div>-->
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						 <!-- <a href="#" class="button mt-3" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Заказать карту</a>-->
					</div>
					<div class="block_type_center">
						 <!--h2>Есть вопросы</h2-->
						<p>
							 Подробнее о сервисе и совместимых устройствах на <a href="http://www.samsung.com/ru/apps/mobile/samsungpay/" target="_blank">сайте Samsung Pay</a>.
						</p>
						<p>
							 Остались вопросы? Обращайтесь в <a href="https://help.content.samsung.com/csweb/faq/searchFaq.do" target="_blank">Службу поддержки Samsung</a> или Контакт-центр Банка по телефону 8 (800) 500 - 66 - 77 (звонок по России бесплатный).
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>