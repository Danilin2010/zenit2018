<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Карты с кэшбэком и процентом на остаток &#151; оформление онлайн | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Карты");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	".default",
	Array(
		"PRODUCTHEADER_TITLE" => "Карты",
		"PRODUCTHEADER_IMG" => "/personal/cards/img/cardsCatalogIconBig.png"
	)
);?> 
<div class="tabs-controls" id="tabs-controls-app"> 	 
  <div class="tabs-controls__wrapper"> <nav class="tabs-controls__inner"> 		<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_ACTIVE" => "Y",
		"PRODUCTHEADER_SRC" => "#debit",
		"PRODUCTHEADER_STAR" => "N",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_TITLE" => "Дебетовые"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_SRC" => "#credit",
		"PRODUCTHEADER_STAR" => "N",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_TITLE" => "Кредитные"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Samsung Pay",
		"PRODUCTHEADER_SRC" => "#mobile",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_STAR" => "N"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Акции",
		"PRODUCTHEADER_SRC" => "#actions",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_STAR" => "Y"
	)
);?> </nav> 	</div>
 </div>
 
<!-- Активный таб tab-content_state-active Поставить id из href кнопки-->
 
<div class="tab-content tab-content_state-active" id="debit"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Колонка с контентом-->
 				 
        <div class="col col_size-8"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта с кэшбэком",
		"SIDECOLUMN_TEXT" => "Дебетовая карта для тех, кто посещает кино, концерты, кафе, рестораны и&nbsp;хочет получать кэшбэк за&nbsp;развлечения",
		"SIDECOLUMN_IMG" => "/personal/cards/img/cashbackdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array("entertainment","cafe","dohod"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>за развлечения",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "36",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "кэшбэка<br>за год",
		"SIDECOLUMN_FEATURE3_BEFORE" => "до",
		"SIDECOLUMN_FEATURE3_TITLE" => "6",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "на остаток<br>средств",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/card-with-cash-back/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET" => "N",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/card-with-cash-back/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_LEARN_MORE_TARGET" => "N",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/card-with-cash-back/",
		"SIDECOLUMN_LINK_CART_TARGET" => "N"
	)
);?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Автокарта Platinum",
		"SIDECOLUMN_TEXT" => "Дебетовая карта для тех, кто активно ей&nbsp;пользуется и&nbsp;хочет получать кэшбэк за&nbsp;топливо, парковки, мойки",
		"SIDECOLUMN_IMG" => "/personal/cards/img/autoplatinumdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array("auto"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>на Татнефть",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "за мойки, парковки<br>и другие заправки",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1,5",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кэшбэк<br>за покупки",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/debit-autocard-platinum/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/debit-autocard-platinum/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/debit-autocard-platinum/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array("auto",""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>на Татнефть",
		"SIDECOLUMN_FEATURE1_TITLE" => "7",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "за мойки, парковки<br>и другие заправки",
		"SIDECOLUMN_FEATURE2_TITLE" => "5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кэшбэк<br>за покупки",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "%",
		"SIDECOLUMN_IMG" => "/personal/cards/img/autogolddebetCard.png",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/debit-autocard-gold/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/debit-autocard-gold/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/debit-autocard-gold/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Дебетовая карта для тех, кто менее активно ей&nbsp;пользуется и&nbsp;хочет получать кэшбэк за&nbsp;топливо, парковки, мойки",
		"SIDECOLUMN_TITLE" => "Автокарта Gold",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Доходный остаток",
		"SIDECOLUMN_TEXT" => "Дебетовая карта с повышенными процентами на остаток средств &#151; для тех, кто привык хранить деньги на карте",
		"SIDECOLUMN_IMG" => "/personal/cards/img/dohoddebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array("dohod"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "до",
		"SIDECOLUMN_FEATURE1_TITLE" => "7",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "на остаток<br>средств",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "0",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "1 год<br>обслуживания",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "0",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "2 месяца<br>SMS-сервиса",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/card-profitable-balance/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/card-profitable-balance/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/card-profitable-balance/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта МИР",
		"SIDECOLUMN_TEXT" => "Дебетовая карта национальной платежной системы &#151; удобная оплата товаров и&nbsp;услуг по&nbsp;всей России",
		"SIDECOLUMN_IMG" => "/personal/cards/img/mirdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/classical-map-the-world/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/classical-map-the-world/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/classical-map-the-world/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта Unionpay",
		"SIDECOLUMN_TEXT" => "Дебетовая карта китайской платёжной системы для тех, кто планирует поездку в&nbsp;Китай или&nbsp;другие цели",
		"SIDECOLUMN_IMG" => "/personal/cards/img/unionpaydebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/card-unionpay-platinum/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/card-unionpay-platinum/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/card-unionpay-platinum/"
	)
);?> 
<!--?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта Инициатива",
		"SIDECOLUMN_TEXT" => "Дебетовая карта для перечисления взносов в&nbsp;Национальный негосударственный пенсионный фонд",
		"SIDECOLUMN_IMG" => "/personal/cards/img/iniciativadebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/debit-card/map-initiative/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/debit-card/map-initiative/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/debit-card/map-initiative/"
	)
);?-->
 					</div>
         				</div>
       				 
<!-- Боковая колонка-->
 				 
        <div class="col col_size-4"> <asside class="products-asside"> 
<!-- Фильтр продуктов-->
 
            <div class="products-filter" :class="{&quot;products-filter_state-open&quot;: filterStateOpen}"> <button class="button products-filter__control" @click="toggleFilterState()"> <svg class="icon icon__filter-symbol"> <use xlink:href="#icon__filter-symbol"></use> </svg> <span class="text">Подобрать карту</span> </button> <form class="products-filter__wrapper" @change="onFilterChange($event)"> 
                <div class="products-filter__inner"> 
                  <div class="products-filter__title">Подбор карты</div>
                 <button class="products-filter__close" type="button" @click.prevent="toggleFilterState()"> <svg class="icon icon__close-symbol"> <use xlink:href="#icon__close-symbol"></use> </svg> </button> 
                  <div class="products-filter__list"> 									 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "1",
		"CHECKBOX_CODE" => "dohod",
		"CHECKBOX_NAME" => "Проценты на остаток"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "2",
		"CHECKBOX_CODE" => "auto",
		"CHECKBOX_NAME" => "Кэшбэк за топливо"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "3",
		"CHECKBOX_CODE" => "entertainment",
		"CHECKBOX_NAME" => "Кэшбэк за развлечения"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "4",
		"CHECKBOX_CODE" => "cafe",
		"CHECKBOX_NAME" => "Кэшбэк за кафе"
	)
);?> </div>
                 
                  <div class="products-filter__controls"> 
                    <div class="button button_primary products-filter__apply-params" @click.prevent="onFilterApply()"> Применить фильтр </div>
                   
                    <div class="button button_transparent button_dark-transparent products-filter__reset-params" @click.prevent="showAllCards()"> Сбросить </div>
                   </div>
                 </div>
               </form> </div>
           </asside> 				</div>
       			</div>
     
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 
            <div class="recommended-cards__title"> 							 Обратите внимание 						</div>
           <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
		"RECOMMENDED_TEXT" => "Зарплатная карта с&nbsp;начислением кэшбэка и&nbsp;процентов на&nbsp;остаток средств",
		"RECOMMENDED_LINK" => "/personal/tatneft/#сards",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Samsung Pay",
		"RECOMMENDED_TEXT" => "Удобный и безопасный сервисы бесконтактных мобильных платежей",
		"RECOMMENDED_LINK" => "/personal/cards/?start-tab=mobile",
		"RECOMMENDED_IMG" => "/personal/cards/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Акции",
		"RECOMMENDED_TEXT" => "Оплачивая картами Банка Зенит, вам могут быть доступны дополнительные выгоды",
		"RECOMMENDED_LINK" => "/personal/cards/?start-tab=actions",
		"RECOMMENDED_IMG" => "/personal/cards/img/information.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Пополнение карт",
		"RECOMMENDED_TEXT" => "Вы можете пополнять карты наличными в&nbsp;банкоматах, терминалах и&nbsp;переводами",
		"RECOMMENDED_LINK" => "/personal/cards/methods-of-deposit/",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<!-- Активный таб tab-content_state-active Поставить id из href кнопки-->
 
<div class="tab-content" id="credit"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Колонка с контентом-->
 				 
        <div class="col col_size-8"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта с кэшбэком",
		"SIDECOLUMN_TEXT" => "Кредитная карта для тех, кто посещает кино, концерты, кафе, рестораны и&nbsp;хочет получать кэшбэк за&nbsp;развлечения",
		"SIDECOLUMN_IMG" => "/personal/cards/img/cashbackdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array("entertainment", "cafe"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>за развлечения",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "36",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "кэшбэка<br>за год",
		"SIDECOLUMN_FEATURE3_BEFORE" => "до",
		"SIDECOLUMN_FEATURE3_TITLE" => "50",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "дней",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "льготный<br>период",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/credit-card-with-cash-back/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET" => "N",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/credit-card-with-cash-back/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_LEARN_MORE_TARGET" => "N",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/credit-card-with-cash-back/",
		"SIDECOLUMN_LINK_CART_TARGET" => "N"
	)
);?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Автокарта Platinum",
		"SIDECOLUMN_TEXT" => "Кредитная карта для тех, кто активно ей&nbsp;пользуется и&nbsp;хочет получать кэшбэк за&nbsp;топливо, парковки, мойки",
		"SIDECOLUMN_IMG" => "/personal/cards/img/autoplatinumdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array("auto"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>на Татнефть",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "за мойки, парковки<br>и другие заправки",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1,5",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кэшбэк<br>за покупки",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/credit-autocard-platinum/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/credit-autocard-platinum/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/credit-autocard-platinum/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Автокарта Gold",
		"SIDECOLUMN_TEXT" => "Кредитная карта для тех, кто менее активно ей&nbsp;пользуется и&nbsp;хочет получать кэшбэк за&nbsp;топливо, парковки, мойки",
		"SIDECOLUMN_IMG" => "/personal/cards/img/autogolddebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array("auto"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "7",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>на Татнефть",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "за мойки, парковки<br>и другие заправки",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кэшбэк<br>за покупки",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/credit-autocard-gold/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/credit-autocard-gold/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/credit-autocard-gold/"
	)
);?> 
<!--?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта iGolbe",
		"SIDECOLUMN_TEXT" => "Кредитная карта для тех, кто часто путешествует и копит мили для бронирования авиабилетов и отелей",
		"SIDECOLUMN_IMG" => "/personal/cards/img/iglobecreditCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array("travel"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "1",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "миля",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "за каждые<br>30 рублей",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "50",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "дней",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "льготный<br>период",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кредитный <br>лимит",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/credit-card-world-of-travel/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/credit-card-world-of-travel/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/credit-card-world-of-travel/"
	)
);?-->
 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта Visa Platinum",
		"SIDECOLUMN_TEXT" => "Премиальная кредитная карта для тех, кто выбирает Visa &#151; удобная оплата товаров и&nbsp;услуг по&nbsp;всему миру",
		"SIDECOLUMN_IMG" => "/personal/cards/img/visacreditCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_TITLE" => "17",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "50",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "дней",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "льготный<br>период",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>лимит",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/visa-platinum-credit-card/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/visa-platinum-credit-card/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/visa-platinum-credit-card/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта MC Platinum",
		"SIDECOLUMN_TEXT" => "Премиальная кредитная карта для тех, кто выбирает Mastercard &#151; удобная оплата товаров и&nbsp;услуг по&nbsp;всему миру",
		"SIDECOLUMN_IMG" => "/personal/cards/img/mastercreditCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_TITLE" => "17",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "50",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "дней",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "льготный<br>период",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>лимит",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/visa-platinum-credit-card/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/visa-platinum-credit-card/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/visa-platinum-credit-card/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта МИР",
		"SIDECOLUMN_TEXT" => "Кредитная карта национальной платежной системы&nbsp;&#151;удобная оплата товаров и&nbsp;услуг по&nbsp;всей России",
		"SIDECOLUMN_IMG" => "/personal/cards/img/mirdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_TITLE" => "17",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "50",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "дней",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "льготный<br>период",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>лимит",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/classic-credit-card-the-world/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/classic-credit-card-the-world/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/classic-credit-card-the-world/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Карта Unionpay",
		"SIDECOLUMN_TEXT" => "Кредитная карта китайской платежной системы для тех, кто планирует поездку в&nbsp;Китай или&nbsp;другие цели",
		"SIDECOLUMN_IMG" => "/personal/cards/img/visacreditCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array("travel"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_TITLE" => "17",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "50",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "дней",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "льготный<br>период",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>лимит",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/credit-card/credit-card-unionpay-platinum/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/credit-card/credit-card-unionpay-platinum/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/credit-card/credit-card-unionpay-platinum/"
	)
);?>					</div>
         				</div>
       				 
<!-- Боковая колонка-->
 				 
        <div class="col col_size-4"> <asside class="products-asside"> 
<!-- Фильтр продуктов-->
 
            <div class="products-filter" :class="{&quot;products-filter_state-open&quot;: filterStateOpen}"> <button class="button products-filter__control" @click="toggleFilterState()"> <svg class="icon icon__filter-symbol"> <use xlink:href="#icon__filter-symbol"></use> </svg> <span class="text">Подобрать карту</span> </button> <form class="products-filter__wrapper" @change="onFilterChange($event)"> 
                <div class="products-filter__inner"> 
                  <div class="products-filter__title">Подбор карты</div>
                 <button class="products-filter__close" type="button" @click.prevent="toggleFilterState()"> <svg class="icon icon__close-symbol"> <use xlink:href="#icon__close-symbol"></use> </svg> </button> 
                  <div class="products-filter__list"> 									 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "5",
		"CHECKBOX_CODE" => "auto",
		"CHECKBOX_NAME" => "Кэшбэк за топливо"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "6",
		"CHECKBOX_CODE" => "entertainment",
		"CHECKBOX_NAME" => "Кэшбэк за развлечения"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "7",
		"CHECKBOX_CODE" => "cafe",
		"CHECKBOX_NAME" => "Кэшбэк за кафе"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.checkbox",
	"",
	Array(
		"CHECKBOX_ID" => "8",
		"CHECKBOX_CODE" => "travel",
		"CHECKBOX_NAME" => "Для путешествий"
	)
);?> </div>
                 
                  <div class="products-filter__controls"> 
                    <div class="button button_primary products-filter__apply-params" @click.prevent="onFilterApply()"> Применить фильтр </div>
                   
                    <div class="button button_transparent button_dark-transparent products-filter__reset-params" @click.prevent="showAllCards()"> Сбросить </div>
                   </div>
                 </div>
               </form> </div>
           </asside> 				</div>
       			</div>
     
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 
            <div class="recommended-cards__title"> 							 Обратите внимание 						</div>
           <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
		"RECOMMENDED_TEXT" => "Зарплатная карта с&nbsp;начислением кэшбэка и&nbsp;процентов на&nbsp;остаток средств",
		"RECOMMENDED_LINK" => "/personal/tatneft/#сards",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Samsung Pay",
		"RECOMMENDED_TEXT" => "Удобный и безопасный сервис бесконтактных мобильных платежей",
		"RECOMMENDED_LINK" => "/personal/cards/?start-tab=mobile",
		"RECOMMENDED_IMG" => "/personal/cards/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Акции",
		"RECOMMENDED_TEXT" => "Оплачивая картами Банка Зенит, вам могут быть доступны дополнительные выгоды",
		"RECOMMENDED_LINK" => "/personal/cards/?start-tab=actions",
		"RECOMMENDED_IMG" => "/personal/cards/img/information.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Пополнение карт",
		"RECOMMENDED_TEXT" => "Вы можете пополнять карты наличными в&nbsp;банкоматах, терминалах и&nbsp;переводами",
		"RECOMMENDED_LINK" => "/personal/cards/methods-of-deposit/",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<div class="tab-content" id="mobile"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Сервис Samsung Pay",
		"SIDECOLUMN_TEXT" => "Платите свободно везде смартфоном или&nbsp;устройством Gear&nbsp;S3 по&nbsp;картам Mastercard",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/cards/card-payments/samsung-pay/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подключить",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET" => "N",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/cards/card-payments/samsung-pay/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_LEARN_MORE_TARGET" => "N",
		"SIDECOLUMN_LINK_CART" => "/personal/cards/card-payments/samsung-pay/",
		"SIDECOLUMN_LINK_CART_TARGET" => "N"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<div class="tab-content" id="actions"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Шоу Cirque du Soleil OVO ",
		"SIDECOLUMN_TEXT" => "Погрузитесь в мир чудес! Оплачивайте картой Mastercard Банка Зенит билеты на&nbsp;шоу и&nbsp;получайте скидку&nbsp;5%",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "http://www.cds-show.ru/ovo/tickets",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Выбрать билеты",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET" => "Y",
		"SIDECOLUMN_LINK_LEARN_MORE" => "http://www.cds-show.ru/ovo/show",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее о шоу",
		"SIDECOLUMN_LINK_LEARN_MORE_TARGET" => "Y",
		"SIDECOLUMN_LINK_CART" => "http://www.cds-show.ru/ovo/show",
		"SIDECOLUMN_LINK_CART_TARGET" => "Y"
	)
);?> 					 
<!--?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Акций пока нет",
		"SIDECOLUMN_TEXT" => "Но они обязательно появятся через некоторое время. Возвращайтесь позже!",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "#",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => " &#128528; Я негодую!",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => ""
	)
);?-->
 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>