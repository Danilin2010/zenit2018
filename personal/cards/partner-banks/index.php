<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Банки-партнеры");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/street.png');
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h1>Банки-партнеры</h1>
				<ul class="big_list">
					<li>АО «АК Банк»</li>
					<li>АО БАНК ЗЕНИТ СОЧИ</li>
					<li>АО КБ «ЗЛАТКОМБАНК»</li>
					<li>КБ «МКБ» ("Международный коммерческий банк") ПАО</li>
					<li>АО «ПроБанк»</li>
					<li>ПАО «Спиритбанк»</li>
					<li>ПАО «Липецккомбанк»</li>
					<li>АБ «Девон-Кредит» (ПАО)</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>