<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Способы пополнения");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/top_banner/street.png');
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h2>Пополнить карту</h2>
				<p>
					 Пополнить карту Банка ЗЕНИТ очень просто. Это можно сделать одним из следующих способов:
				</p>
				<div class="faq">
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict">
								<div class="faq_arr">
								</div>
							</div>
							<div class="faq_top_text">
								 Наличными:
							</div>
						</div>
						<div class="faq_text">
							<ul class="big_list">
								<li>через <a href="/offices/?type=atm">банкоматы Банка ЗЕНИТ</a> с функцией приёма наличных (cash-in)</li>
								<li>через банкоматы и терминалы БИНБАНКА:
								<ul>
									<li>Найти <a href="https://www.binbank.ru/branches/atms/map/" target="_blank">терминал</a></li>
									<li><a href="/media/doc/personal/card/binbank_instruction.pdf" target="_blank">Инструкция</a> по пополнению</li>
								</ul>
 </li>
								<li>через терминалы МОСКОВСКОГО КРЕДИТНОГО БАНКА:
								<ul>
									<li>Найти <a href="https://mkb.ru/about/address/terminal" target="_blank">терминал</a></li>
									<li><a target="_blank" href="/media/doc/personal/card/mkb_instruction.pdf">Инструкция</a> по пополнению</li>
								</ul>
 </li>
								<li>через <a href="https://elecsnet.ru/terminals/addresses" target="_blank">терминалы «Элекснет»</a></li>
								<li>в кассах <a href="/offices/" target="_blank">офисов Банка ЗЕНИТ</a></li>
							</ul>
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict">
								<div class="faq_arr">
								</div>
							</div>
							<div class="faq_top_text">
								 Переводом:
							</div>
						</div>
						<div class="faq_text">
							<ul class="big_list">
								<li>в интернет-банке и мобильном приложении <a href="https://my.zenit.ru" target="_blank">«ЗЕНИТ Онлайн»</a></li>
								<li><a href="https://zenit.dengisend.ru/" target="_blank">с карты на карту</a></li>
								<li>через сервис <a href="https://koronapay.com/credit/pages/gde-oplatit.aspx#type:all/method:bounds/bounds:55.36479318400174%2C36.98867913281249%2C56.13926616926466%2C38.25210686718749/" target="_blank">«Золотая Корона – Погашение кредитов»</a></li>
							</ul>
						</div>
					</div>
				</div><br>
				<p>Карта пополняется в соответствии с <a href="/personal/tariffs/#content-tabs-2">тарифами</a>.</p>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>