<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Налоговый вычет под ключ");
?>
<div class="wr_block_type">
     <!--content_tab-->
    <div class="content_rates_tabs">
	
        <ul class="content_tab c-container">
            <li><a href="#content-tabs-1">Налоговые вычеты</a></li>
            <li><a href="#content-tabs-2">Как это работает</a></li>
            <li><a href="#content-tabs-3">Стоимость</a></li>
        </ul>
		
        <div class="content_body" id="content-tabs-1">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
                        <div class="text_block">
							<ul class="big_list">
								<li>При покупке квартиры – <b>до 260 000 рублей</b></li>
								<li>При выплате ипотеки – <b>до 390 000 рублей</b></li>
								<li>При открытии индивидуального инвестиционного счёта – <b>до 52 000 рублей</b></li>
								<li>При оплате собственного обучения/ лечения – <b>до 15 600 рублей</b></li>
								<li>При оплате обучения ребёнка – <b>до 6500 рублей</b></li>
								<li>При оплате дорогостоящего лечения – <b>без ограничений</b></li>								
							</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="content_body" id="content-tabs-2">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
                        <div class="text_block">
							<p>Для получения налогового вычета требуется совершить всего три шага:</p>
							<ol>
								<li>Покупка сертификата в <a href="/offices/">любом офисе</a> Банка ЗЕНИТ.</li>
								<li>Активация сертификата <a href="https://consultant365.ru/" target="_blank">на сайте</a>.</li>
								<li>Загрузка комплекта документов и оформление заявления на получение электронной цифровой подписи (ЭЦП).</li>
							</ol><br>
							<p>Всё остальное за Вас сделают квалифицированные юристы:</p>
							<ol>
								<li>Проверка комплектности и содержания документов.</li>
								<li>Составление документов от Вашего имени (заполнение декларации 3-НДФЛ и оформление заявления на получение налогового вычета).</li>
								<li>Отправка комплекта документов в налоговую инспекцию в электронном виде (после Вашего подписания их с использованием ЭЦП).</li>
							</ol><br>							
							<p>Также в Личном кабинете в режиме онлайн Вы всегда сможете получить правовую консультацию (включая разъяснения по документам) и отслеживать статус налогового вычета после отправки документов в налоговую инспекцию</p>
							<p>Сертификат действует бессрочно. Это значит, что воспользоваться им Вы сможете <b>в любое время</b>.</p>
							<p>Услуга предоставляется компанией «Онлайн-юрист». Правовая поддержка клиентов осуществляется по телефону 8 (800) 100-11-10 (круглосуточно, звонок по России бесплатный).</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		        <div class="content_body" id="content-tabs-3">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
						<h2></h2>
                        <div class="text_block">
							<p>Стоимость сертификата составляет <b>3000 рублей</b>.<br>
							Один сертификат может использоваться для получения одного налогового вычета.</p>
                    </div>
                </div>
            </div>
        </div>
		
    </div>
     <!--content_tab-->	
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>