<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Архивные тарифы");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/about/header-bg.png');
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				Архивные тарифы (для карт, выпущенных&nbsp;с 22.03.2018г. )<br>
				<ul>
					<!--li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_61_20161227.pdf" target="_blank">Тарифный план 6.1</a></li-->
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_71_20180322.pdf" target="_blank">Тарифный план 7.1</a></li>
				</ul>
				 <br>
				Архивные тарифы (для карт, выпущенных&nbsp;с 27.12.2017г. до 22.03.2018 г.)<br>
				<ul>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_61_20161227.pdf" target="_blank">Тарифный план 6.1</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_62_20161227.pdf" target="_blank">Тарифный план 6.2</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_71_20161227.pdf" target="_blank">Тарифный план 7.1</a></li>
				</ul>
				 <br>
				Архивные тарифы (для карт, выпущенных&nbsp;с 22.12.2015г. до 27.12.2017 г.)<br>
				<ul>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_61_20151222.pdf" target="_blank">Тарифный план 6.1</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_71_20151222.pdf" target="_blank">Тарифный план 7.1</a></li>
				</ul>
				 <br>
				 Архивные тарифы (для карт, выпущенных c 10.08.2015 г. до 21.12.2015 г.)<br>
				<ul>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_6.1_20150810.pdf" target="_blank">Тарифный план 6.1</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_7.1_20150810.pdf" target="_blank">Тарифный план 7.1</a></li>
				</ul>
				 <br>
				 Архивные тарифы (для карт, выпущенных c 10.06.2015 г. до 09.08.2015 г.)<br>
				<ul>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_61_20150610.pdf" target="_blank">Тарифный план 6.1</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_71_20150610.pdf" target="_blank">Тарифный план 7.1</a></li>
				</ul>
				 <br>
				 Архивные тарифы (для карт, выпущенных до 10.06.2015 г.)<br>
				<ul>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tariffs_8.pdf" target="_blank">Тарифный план 8</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/Cc_tarifs_6.1_20140701.pdf" target="_blank">Тарифный план 6.1</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/Cc_tarifs_7.1_20140701.pdf" target="_blank">Тарифный план 7.1</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_6_20130805.pdf" target="_blank">Тарифный план 6</a></li>
					<li><a href="/media/doc/personal/card/archived-rates/cc_tarifs_7_20130805.pdf" target="_blank">Тарифный план 7</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>