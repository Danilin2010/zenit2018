<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Потребительский кредит");
?>    
<div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>
                    Потребительские кредиты
                    <div class="note_text">
                        
                    </div>
                </h1>
                <!--ipoteka_list-->
                <div class="ipoteka_list">
                    <div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/consumer-credit/consumer-loan-for-any-purpose" class="ipoteka_item_title">
                                Потребительский кредит на любые цели
                            </a>
                            <div class="ipoteka_item_text">
                                На неотложные нужды без залога и поручительства
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/consumer-credit/consumer-credit-on-vacation" class="ipoteka_item_title">
                                Потребительский кредит на отдых
                            </a>
                            <div class="ipoteka_item_text">
                                На отдых без хлопот
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a  href="/personal/tatneft/consumer-credit/a-consumer-loan-secured-by-real-estate" class="ipoteka_item_title">
                                Потребительский кредит под залог недвижимости
                            </a>
                            <div class="ipoteka_item_text">
                                Большие деньги на реализацию больших целей
                            </div>
                        </div>
                    </div>
                </div>
                <!--ipoteka_list-->
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>