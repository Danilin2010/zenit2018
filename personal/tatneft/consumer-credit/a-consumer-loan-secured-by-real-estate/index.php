<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Потребительский кредит под залог недвижимости");
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Условия и документы</a></li>
			<li><a href="#content-tabs-4">Страхование</a></li>
			<li><a href="#content-tabs-2">Погашение</a></li>
			 <!--li><a href="#content-tabs-3">Документы</a></li--> <!--li><a href="#content-tabs-5">FAQ</a></li-->
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						 <!--conditions-->
						<div class="conditions">
							<div class="conditions_item norm_width">
								<div class="conditions_item_title">
									<span>до </span>14<span> млн рублей</span>                         
								</div>
								<div class="conditions_item_text">
									сумма                       
								</div>
							</div>
							<div class="conditions_item norm_width">
								<div class="conditions_item_title">
									<span>до </span>15<span> лет</span>
								</div>
								<div class="conditions_item_text">
									срок                      
								</div>
							</div>
							<div class="conditions_item norm_width">
								<div class="conditions_item_title">
									<span></span>15,5%<span> годовых</span>              
								</div>
								<div class="conditions_item_text">
									ставка                        
								</div>
							</div>            
						</div>
						 <!--conditions-->
						<div class="text_block">
							<h2>Условия кредитования</h2>
							<p>
							</p>
							<ul class="big_list">
								<li>Кредит на любые цели, не связанные с осуществлением предпринимательской деятельности</li>
								<li>Получение денег единоразово либо в форме невозобновляемой кредитной линии</li>
								<li>Срок рассмотрения заявки – не более пяти рабочих дней</li>
								<li>Сумма кредита:
								<ul>
									<li>от 540 000 до 14 млн рублей в Московском регионе;</li>
									<li>от 270 000 до 10,5 млн рублей в других регионах присутствия Банка </li>
								</ul>
 </li>
								<li>Процентная ставка – 15,5% годовых</li>
								<li>Срок кредита – до 15 лет (кратно одному году)</li>
								<li>Залог недвижимого имущества (квартира, комната, апартаменты, домовладения, земельный участок, гараж/ машиноместо, иная недвижимость), находящегося в собственности заёмщика или иного физ. лица</li>
								<li>Досрочное погашение без моратория и комиссии</li>
							</ul>
							<p>
							</p>
							<h2>Требования к заёмщику</h2>
							<p>
							</p>
							<ul class="big_list">
								<li>Регистрация по месту жительства на территории РФ*</li>
								<li>Работа в компании Группы «Татнефть»</li>
								<li>Гражданство РФ</li>
							</ul>
							<p>
							</p>
							 <!--p class="note_text">*<em>За исключением Республики Адыгея, Карачаево-Черкесской Республики, Кабардино-Балкарской Республики, Республики Дагестан, Республики Ингушетия, Чеченской Республики, Республики Калмыкия, Мурманской области, Архангельской области, Республики Коми, Ненецкого автономного округа, Ямало-Ненецкого автономного округа, Красноярского края, Забайкальского края, Республики Тыва, Республики Бурятия, Иркутской области, Амурской области, Магаданской области, Сахалинской области, Хабаровского края, Приморского края, Камчатской края, Еврейской автономной области, Республики Саха (Якутия) и Чукотского автономного округа.</em>
							</p-->
							<h2>Необходимые документы*</h2>
							<ul class="big_list">
								<li>Паспорт РФ</li>
								<li>Справка о доходах по форме 2-НДФЛ или по форме Банка</li>
								<li>Копия трудовой книжки, заверенная работодателем</li>
								<li>Копия свидетельства о заключении брака/ копия брачного договора/ копия свидетельства о расторжении брака (при наличии)</li>
								<li>Документы по залогу (перечень документов зависит от вида закладываемого имущества).</li>
							</ul>
							<p>
								 Для рассмотрения заявки необходимо заполнить <a href="/media/doc/personal/mortgage/anket_loan.pdf">заявление-анкету</a> на получение кредита.
							</p>
							<p class="note_text">
								 *<em>При необходимости Банк может запросить дополнительные документы</em>.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p align="justify">
								 Погашение кредита осуществляется ежемесячно <b>равными (аннуитетными) платежами</b>.
							</p>
							<p align="justify">
								 При желании Вы сможете погасить кредит досрочно без штрафов и комиссий. Полное досрочное погашение возможно в любой рабочий день, частичное – в дату внесения планового платежа. Для осуществления полного или частичного досрочного погашения необходимо обратиться в Банк с заявлением не позднее чем за 3 (Три) календарных дня до даты досрочного погашения.
							</p>
							<p align="justify">
								 В случае возникновения просроченной задолженности начисляется неустойка в размере не более 0,06% от суммы задолженности за каждый календарный день просрочки.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		 <!--div class="content_body" id="content-tabs-3">
            <div class="wr_block_type">
                <div class="block_type to_column c-container">
                    <div class="block_type_right">
                    </div>
                    <div class="block_type_center">
                        <div class="text_block">
							<p>Для оформления ипотечного кредита необходимы следующие документы*:
								<ul class="big_list">
									<li>паспорт РФ;</li>
									<li>справка о доходах по форме 2-НДФЛ или по форме Банка;</li>
									<li>копия трудовой книжки, заверенная работодателем;</li>
									<li>копия свидетельства о заключении брака/ копия брачного договора/ копия свидетельства о расторжении брака (при наличии);</li>
									<li>сертификат на материнский капитал или его копия (при уменьшении первоначального взноса на сумму материнского капитала);</li>
									<li>справка из Пенсионного фонда РФ об остатке средств материнского капитала, заверенная печатью территориального отделения Пенсионного фонда РФ (при уменьшении первоначального взноса на сумму материнского капитала).</li>
								<ul>
							</p>
							<p>Для рассмотрения заявки необходимо заполнить <a href="/media/doc/personal/mortgage/anket_loan.pdf">заявление-анкету</a> на получение кредита.</p>

							<p class="note_text">*<em>При необходимости Банк может запросить дополнительные документы</em>.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div-->
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<ul class="big_list">
								<li>Страхование риска утраты и повреждения недвижимого имущества</li>
								<li>Страхование риска утраты права собственности на передаваемое в залог недвижимое имущество (титула) в течение трёх лет с даты оформления права собственности на недвижимое имущество по Вашему желанию</li>
								<li>Страхование жизни и потери трудоспособности (личное) по Вашему желанию</li>
							</ul>
							<p>
								 При отсутствии личного страхования и/или титульного страхования процентная ставка увеличивается на 3 процентных пункта.
							</p>
							<p>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "universal",
		"SOURCE_TREATMENT" => "",
		"RIGHT_TEXT" => "",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>