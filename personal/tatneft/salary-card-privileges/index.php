<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Зарплатная карта привилегий");
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>
                    Зарплатная карта
                    <div class="note_text">
                        Получайте зарплату и бонусы!
                    </div>
                </h1>
                <!--ipoteka_list-->
                <div class="ipoteka_list">
                    <div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/salary-card-privileges/tariff-optimal" class="ipoteka_item_title">
                                Тариф «Оптимальный»
                            </a>
                            <div class="ipoteka_item_text">
                                Карта Visa Gold/ Mastercard Gold/ классическая карта «Мир»
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/salary-card-privileges/tariff-premium" class="ipoteka_item_title">
                                Тариф «Премиальный»
                            </a>
                            <div class="ipoteka_item_text">
                                Карта Visa Platinum/ Mastercard Platinum/ классическая карта «Мир»
                            </div>
                        </div>
                    </div>
                </div>
                <!--ipoteka_list-->
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>