<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тариф «Оптимальный»");
?><div class="wr_block_type">
	<div class="block_type to_column c-container" style="overflow: hidden;">
		<div class="block_type_right">
			<div class="right_top_line">
				<h2>Вам может понадобиться</h2>
 <a href="/personal/cards/methods-of-deposit/" class="button">Пополнение карт</a>
				<!--<div class="block_top_line"></div>-->
			</div>
		</div>
		<div class="block_type_center">
			 <!--conditions-->
			<div class="conditions">
				<div class="conditions_item norm_width">
					<div class="conditions_item_title">
						<span>до </span>5,5%<span> годовых</span>
					</div>
					<div class="conditions_item_text">
						 на остаток по карте
					</div>
				</div>
				<div class="conditions_item norm_width">
					<div class="conditions_item_title">
						<span>до </span>3%
					</div>
					<div class="conditions_item_text">
						 кэшбэк
					</div>
				</div>
				<div class="conditions_item norm_width">
					<div class="conditions_item_title">
						 0 %
					</div>
					<div class="conditions_item_text">
						 за снятие в любых банкоматах
					</div>
				</div>
			</div>
			 <!--conditions-->
			<div class="text_block">
				<p align="justify">
 <b>Преимущества</b>
				</p>
				<ul class="big_list">
					<li>
					<p>
						 0% за снятие в любых банкоматах
					</p>
 </li>
					<li>
					<p>
						 Кэшбэк 1% за все покупки
					</p>
 </li>
					<li>
					<p>
						 Повышенный кэшбэк до 3% за покупки в категории «Развлечения»
					</p>
 </li>
					<li>
					<p>
						 До 5,5% годовых на остаток по карте
					</p>
 </li>
					<li>
					<p>
						 Бесплатное обслуживание карты
					</p>
 </li>
				</ul>
			</div>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p align="justify">
 <b>Мы Вам предоставим</b>
				</p>
				<ul class="big_list">
					<li>Карта Visa Gold/ Mastercard Gold/ классическая карта «Мир»</li>
					<li>Возврат 2% от суммы покупок в кафе, барах, ресторанах, спортивных магазинах и аптеках (при оплате основной и дополнительными картами товаров и услуг на сумму не менее 10 000 рублей в месяц)*</li>
					<li>Возврат 1% от суммы любых других покупок (при оплате основной и дополнительными картами товаров и услуг на сумму не менее 10 000 рублей в месяц)*</li>
					<li>Начисление 5,5% годовых** на остаток средств на счёте (при оплате основной и дополнительными картами товаров и услуг на сумму не менее 10 000 рублей в месяц)</li>
					<li>Годовое обслуживание – бесплатно </li>
					<li>Стоимость услуги «SMS-информирование»: бесплатно в течение первых двух месяцев, впоследствии – 59 рублей в месяц</li>
					<li>Доступ к удобным интернет-банку и мобильному приложению</li>
				</ul>
				<p>
					 *<em>Максимальная сумма возврата по обеим категориям составляет 1000 рублей в месяц</em>.
				</p>
				<p>
					 **<em>Проценты начисляются на часть остатка в размере 100 000 рублей</em>.
				</p>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"universal",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>