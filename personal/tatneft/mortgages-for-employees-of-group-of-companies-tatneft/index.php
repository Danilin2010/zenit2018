<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ипотека для сотрудников ГК «Татнефть»");
?><div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>
                    Ипотека
                    <div class="note_text">
                        
                    </div>
                </h1>
                <!--ipoteka_list-->
                <div class="ipoteka_list">
                    <div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-new-property" class="ipoteka_item_title">
                                Ипотека на новостройку
                            </a>
                            <div class="ipoteka_item_text">
                                Кредит на покупку квартиры на первичном рынке
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgages-for-second-homes" class="ipoteka_item_title">
                                Ипотека на вторичное жильё
                            </a>
                            <div class="ipoteka_item_text">
                                Кредит на покупку квартиры на вторичном рынке
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a  href="/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-house-to-ground/" class="ipoteka_item_title">
                               Ипотека на дом с землёй
                            </a>
                            <div class="ipoteka_item_text">
                                Кредит на покупку дома с земельным участком
                            </div>
                        </div>
                    </div><div class="ipoteka_item">
                        <div class="wr_ipoteka_item">
                            <div class="wr_pict_ipoteka_item" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/f_snippet.png');"></div>
                            <a href="/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-room" class="ipoteka_item_title">
                                Ипотека на комнату
                            </a>
                            <div class="ipoteka_item_text">
                                Кредит на покупку комнаты в квартире
                            </div>
                        </div>
                    </div>
                </div>
                <!--ipoteka_list-->
            </div>
        </div>
    </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>