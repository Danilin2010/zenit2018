<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ипотека на дом с землёй");
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Условия и требования</a></li>
			<li><a href="#content-tabs-2">Погашение</a></li>
			<li><a href="#content-tabs-3">Документы</a></li>
			<li><a href="#content-tabs-4">Страхование</a></li>
			<li><a href="#content-tabs-5">FAQ</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						 <!--conditions-->
						<div class="conditions">
							<div class="conditions_item norm_width">
								<div class="conditions_item_title">
									 до 16 млн рублей
								</div>
								<div class="conditions_item_text">
									 размер кредита
								</div>
							</div>
							<div class="conditions_item norm_width">
								<div class="conditions_item_title">
									 от 30%
								</div>
								<div class="conditions_item_text">
									 первый взнос
								</div>
							</div>
							<div class="conditions_item norm_width">
								<div class="conditions_item_title">
									 10,9%
								</div>
								<div class="conditions_item_text">
									 ставка
								</div>
							</div>
						</div>
						 <!--conditions-->
						<div class="text_block">
							<h2>Условия кредитования</h2>
							<ul class="big_list">
								<li><b>Кредит на приобретение домов с земельными участками</b>: выбирайте квартиру в любом регионе присутствия Банка</li>
								<li><b>Срок рассмотрения заявки – не более пяти рабочих дней</b>: долго ждать не придётся!</li>
								<li><b>Первоначальный взнос – от 30% стоимости жилья</b>.</li>
								<li><b>Уменьшение первоначального взноса на сумму материнского капитала</b>: за счёт средств материнского капитала Вы можете снизить первоначальный взнос на сумму, не превышающую 5% от стоимости приобретаемой квартиры*</li>
								<li><b>Процентная ставка – 10,9% годовых</b>: ипотека не ударит по Вашему бюджету</li>
								<li><b>Сумма кредита – до 16 млн рублей</b>: не отказывайте себе в квадратных метрах!
								<ul>
									<li>От 600 000 до 16 млн рублей для Московского региона</li>
									<li>От 300 000 до 12 млн рублей для других регионов </li>
								</ul>
 </li>
								<li><b>Срок кредита – от года до 25 лет (кратно 12-ти месяцам)</b></li>
								<li><b>Досрочное погашение без моратория и комиссии</b>: Вы можете погасить кредит в любое время</li>
							</ul>
							<p class="note_text">
								 *<em>В случае ненаправления средств материнского капитала на частичное досрочное погашение ипотечного кредита в течение трёх месяцев с даты его оформления Банк вправе повысить процентную ставку по кредиту на 1 процентный пункт</em>.
							</p>
							<h2>Вы можете получить кредит, если:</h2>
							<ul class="big_list">
								<li>Вы зарегистрированы по месту жительства на территории РФ</li>
								<li>Вы работаете в компании Группы «Татнефть»</li>
								<li>У Вас есть гражданство РФ</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p align="justify">
								 Погашение кредита осуществляется ежемесячно <b>равными (аннуитетными) платежами</b>.
							</p>
							<p align="justify">
								 При желании Вы сможете погасить кредит <b>досрочно без штрафов и комиссий</b>. Полное досрочное погашение возможно в любой рабочий день, частичное – в дату внесения планового платежа. Для осуществления полного или частичного досрочного погашения необходимо обратиться в Банк с заявлением не позднее чем за 3 (Три) календарных дня до даты досрочного погашения.
							</p>
							<p align="justify">
								 В случае возникновения просроченной задолженности начисляется неустойка в размере не более 0,06% от суммы задолженности за каждый календарный день просрочки.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Для оформления ипотечного кредита необходимы следующие документы*:
							</p>
							<ul class="big_list">
								<li>паспорт РФ;</li>
								<li>справка о доходах по форме 2-НДФЛ или по форме Банка;</li>
								<li>копия трудовой книжки, заверенная работодателем;</li>
								<li>копия свидетельства о заключении брака/ копия брачного договора/ копия свидетельства о расторжении брака (при наличии);</li>
								<li>сертификат на материнский капитал или его копия (при уменьшении первоначального взноса на сумму материнского капитала);</li>
								<li>справка из Пенсионного фонда РФ об остатке средств материнского капитала, заверенная печатью территориального отделения Пенсионного фонда РФ (при уменьшении первоначального взноса на сумму материнского капитала).</li>
								<ul>
									<p>
									</p>
									<p>
										 Для рассмотрения заявки необходимо заполнить <a href="/media/doc/personal/mortgage/anket_loan.pdf">заявление-анкету</a> на получение кредита.
									</p>
									<p class="note_text">
										 *<em>При необходимости Банк может запросить дополнительные документы</em>.
									</p>
								</ul>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<ul class="big_list">
								<li>Cтрахование риска утраты и повреждения предмета залога.</li>
								<li>Страхование риска утраты права собственности (титула) в течение первых трёх лет с даты оформления права собственности по Вашему желанию.</li>
								<li>Cтрахование жизни и потери трудоспособности (личное) – по Вашему желанию.</li>
							</ul>
							<p>
								 При отсутствии личного страхования и/или титульного страхования процентная ставка увеличивается на 1,5 процентных пункта.
							</p>
							<p>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-5">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Часто задаваемые вопросы</h1>
						<div class="text_block">
							 <!--faq-->
							<div class="faq">
								<div class="faq_item">
									<div class="faq_top">
										<div class="faq_pict">
											<div class="faq_arr">
											</div>
										</div>
										<div class="faq_top_text">
											 Если меня интересует ипотека в Банке ЗЕНИТ, куда я могу подъехать, чтобы проконсультироваться со специалистом?
										</div>
									</div>
									<div class="faq_text">
										 Для получения консультации, проведения предварительных расчетов Вы можете обратиться в любой офис Банка и/или в круглосуточную Службу дистанционного обслуживания Банка по телефонам: (495) 967-1111 – для Москвы и Московской области, 8-800-500-66-77 – для всех регионов России (звонок по России бесплатный).
									</div>
								</div>
								<div class="faq_item">
									<div class="faq_top">
										<div class="faq_pict">
											<div class="faq_arr">
											</div>
										</div>
										<div class="faq_top_text">
											 Как я могу подать заявку на получение ипотечного кредита?
										</div>
									</div>
									<div class="faq_text">
										 Чтобы подать заявку в Банк на получение ипотечного кредита, необходимо подготовить пакет требуемых Банком документов в соответствии со&nbsp;списком&nbsp;и обратиться в один из офисов Банка.<br>
										 Список документов размещен на странице сайта:&nbsp;<a href="/personal/mortgage/documents-and-supporting-information/">Документы и дополнительная информация</a>
									</div>
								</div>
								<div class="faq_item">
									<div class="faq_top">
										<div class="faq_pict">
											<div class="faq_arr">
											</div>
										</div>
										<div class="faq_top_text">
											 Могу ли я передать в Банк заявку и документы на получение ипотечного кредита с курьером или обязательно мое личное присутствие?
										</div>
									</div>
									<div class="faq_text">
										 Документы подаются в Банк заемщиками лично, либо третьими лицами при наличии нотариально удостоверенной доверенности. В таком случае, Заявление-анкета подписывается доверенным лицом с указанием реквизитов доверенности. Одновременно с копиями документов должны быть предоставлены и оригиналы этих документов для сверки специалистом Банка.
									</div>
								</div>
								<div class="faq_item">
									<div class="faq_top">
										<div class="faq_pict">
											<div class="faq_arr">
											</div>
										</div>
										<div class="faq_top_text">
											 Как я узнаю об одобрении ипотечного кредита?
										</div>
									</div>
									<div class="faq_text">
										 По результатам принятия Банком решения с Вами свяжется специалист по указанным в анкете контактам. В случае положительного решения Банка о кредитовании, Вам на электронную почту, которую Вы указали в Заявлении-анкете, придет Уведомление о параметрах одобренного кредита.
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"universal",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>