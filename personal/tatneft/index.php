<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Специальные предложения для сотрудников Татнефть | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	".default",
	Array(
		"PRODUCTHEADER_TITLE" => "Предложения",
		"PRODUCTHEADER_IMG" => "/personal/tatneft/img/tatneft.png"
	)
);?> 
<div class="tabs-controls" id="tabs-controls-app"> 	 
  <div class="tabs-controls__wrapper"> <nav class="tabs-controls__inner"> 		<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Кредиты",
		"PRODUCTHEADER_SRC" => "#credits",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_ACTIVE" => "Y",
		"PRODUCTHEADER_STAR" => "N"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Ипотека",
		"PRODUCTHEADER_SRC" => "#mortgage",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_STAR" => "N"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Зарплатные карты",
		"PRODUCTHEADER_SRC" => "#cards",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_STAR" => "Y"
	)
);?> </nav> 	</div>
 </div>
 
<div class="tab-content tab-content_state-active" id="credits"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Кредит на любые цели",
		"SIDECOLUMN_TEXT" => "Потребительский кредит наличными без залога и&nbsp;поручителей на&nbsp;любые неотложные нужды",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array("Для оформления достаточно двух документов","Заявка рассматривается не более 2 дней"),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от ",
		"SIDECOLUMN_FEATURE1_TITLE" => "8,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "25",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "7",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/consumer-credit/consumer-loan-for-any-purpose/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET" => "N",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/consumer-credit/consumer-loan-for-any-purpose/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_LEARN_MORE_TARGET" => "N",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/consumer-credit/consumer-loan-for-any-purpose/",
		"SIDECOLUMN_LINK_CART_TARGET" => "N"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Кредит на отдых",
		"SIDECOLUMN_TEXT" => "Кредит на оплату путёвок в санатории-профилактории, базы отдыха, детские оздоровительные лагеря Татнефть",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array("Для оформления достаточно двух документов","Заявка рассматривается не более 2 дней"),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "25",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "5",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/consumer-credit/consumer-credit-on-vacation/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/consumer-credit/consumer-credit-on-vacation/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/consumer-credit/consumer-credit-on-vacation/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Под залог недвижимости",
		"SIDECOLUMN_TEXT" => "Если вам необходима крупная сумма денег и вы готовы предоставить в залог недвижимость",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "15,5",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "14",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "15",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/consumer-credit/a-consumer-loan-secured-by-real-estate/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/consumer-credit/a-consumer-loan-secured-by-real-estate/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/consumer-credit/a-consumer-loan-secured-by-real-estate/"
	)
);?> 							</div>
         				</div>
       			</div>
     			 		</div>
   	</div>
 </div>
 
<div class="tab-content" id="mortgage"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Квартира в&nbsp;новостройке",
		"SIDECOLUMN_TEXT" => "Ипотека на&nbsp;приобретение квартиры в&nbsp;строящемся или&nbsp;готовом доме",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "100",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "новостроек<br>на&nbsp;выбор",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-new-property/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-new-property/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-new-property/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Квартира на&nbsp;вторичном рынке",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;квартиру в&nbsp;многоэтажном доме на&nbsp;вторичном рынке ",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "15",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "5",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "дней<br>на заявку",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgages-for-second-homes/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgages-for-second-homes/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgages-for-second-homes/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Комната на&nbsp;вторичном рынке",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет приобрести одну или&nbsp;несколько комнат в&nbsp;квартире на&nbsp;вторичном рынке",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "300",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-room/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-room/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-room/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Дом с&nbsp;землей",
		"SIDECOLUMN_TEXT" => "Если вы мечтаете жить за&nbsp;городом в&nbsp;собственном доме с&nbsp;участком земли",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "30",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "16",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-house-to-ground/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-house-to-ground/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/mortgages-for-employees-of-group-of-companies-tatneft/mortgage-on-house-to-ground/"
	)
);?> 	</div>
         				</div>
       			</div>
     			 
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 
            <div class="recommended-cards__title"> 							 Обратите внимание 						</div>
           						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Подбор недвижимости",
		"RECOMMENDED_TEXT" => "Подбор новостроек на&nbsp;карте и&nbsp;интересные спецпредложения на выгодных условиях",
		"RECOMMENDED_LINK" => "/personal/mortgage/realty/",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Справочная информация",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/information.svg",
		"RECOMMENDED_LINK" => "/personal/mortgage/documents-and-supporting-information/",
		"RECOMMENDED_TEXT" => "Требования, условия, документы и&nbsp;информация по&nbsp;обслуживанию ипотеки",
		"RECOMMENDED_TITLE" => "Справочная информация"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<div class="tab-content" id="cards"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Колонка с контентом-->
 				 
        <div class="col col_size-8"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Тариф &laquo;Премиальный&raquo;",
		"SIDECOLUMN_TEXT" => "Зарплатная карта для тех, кто активно ей пользуется, хочет получать повышенный кэшбэк и&nbsp;проценты на&nbsp;остаток",
		"SIDECOLUMN_IMG" => "/personal/cards/img/tatneftdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "3",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "кэшбэк<br>за покупки",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "5,75",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "на остаток<br>средств",
		"SIDECOLUMN_FEATURE3_BEFORE" => "до",
		"SIDECOLUMN_FEATURE3_TITLE" => "24",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кэшбэка<br>за год",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/salary-card-privileges/tariff-premium/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TARGET" => "N",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/salary-card-privileges/tariff-premium/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_LEARN_MORE_TARGET" => "N",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/salary-card-privileges/tariff-premium/",
		"SIDECOLUMN_LINK_CART_TARGET" => "N"
	)
);?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"big",
	Array(
		"SIDECOLUMN_TITLE" => "Тариф &laquo;Оптимальный&raquo;",
		"SIDECOLUMN_TEXT" => "Карта для тех, кто использует её для снятия наличных, а&nbsp;также хочет получать кэшбэк за&nbsp;покупки и&nbsp;проценты на&nbsp;остаток",
		"SIDECOLUMN_IMG" => "/personal/cards/img/tatneftdebetCard.png",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array("auto"),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "0",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "снятие в любых<br>банкоматах",
		"SIDECOLUMN_FEATURE2_BEFORE" => "до",
		"SIDECOLUMN_FEATURE2_TITLE" => "5,5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "на остаток<br>средств",
		"SIDECOLUMN_FEATURE3_BEFORE" => "до",
		"SIDECOLUMN_FEATURE3_TITLE" => "12",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "кэшбэка<br>за год",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/tatneft/salary-card-privileges/tariff-optimal/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Заказать карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/tatneft/salary-card-privileges/tariff-optimal/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/tatneft/salary-card-privileges/tariff-optimal/"
	)
);?> </div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>