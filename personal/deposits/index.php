<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Выгодные вклады с возможностью пополнения, снятия и досрочного расторжения | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	"",
	Array(
		"PRODUCTHEADER_IMG" => "/personal/deposits/img/depositsCatalogIconBig.png",
		"PRODUCTHEADER_TITLE" => "Вклады"
	)
);?> 
<div class="tab-content tab-content_state-active" id="classic"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array("В подарок карта с кэшбэком и процентом на остаток","При оформлении в интернет-банке ставка выше на 0,25%","",""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "до",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "ставка<br>по&nbsp;вкладу",
		"SIDECOLUMN_FEATURE1_TITLE" => "8,5",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "срок<br>вклада",
		"SIDECOLUMN_FEATURE2_TITLE" => "6",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "мес.",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "30",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/deposits/the-contribution-of-dessert/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Хочу оформить",
		"SIDECOLUMN_LINK_CART" => "/personal/deposits/the-contribution-of-dessert/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/deposits/the-contribution-of-dessert/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Успейте оформить до 30 апреля! Вклад в&nbsp;рублях на&nbsp;6&nbsp;месяцев с&nbsp;выплатой процентов в&nbsp;конце срока",
		"SIDECOLUMN_TITLE" => "Вклад &laquo;Десерт&raquo;",
		"SIDECOLUMN_TYPE" => "product-card_theme-promo"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Вклад &laquo;Высокий доход&raquo;",
		"SIDECOLUMN_TEXT" => "Вклад от&nbsp;1 месяца до&nbsp;2 лет без&nbsp;возможности пополнения и&nbsp;снятия, с&nbsp;выплатой процентов в&nbsp;конце срока",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array("Нельзя пополнять и снимать","При оформлении в интернет-банке ставка выше на 0,25%"),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "до",
		"SIDECOLUMN_FEATURE1_TITLE" => "6,75",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "ставка<br>по&nbsp;вкладу",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "1",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "мес.",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальный<br>срок",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "30",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/deposits/high-profit/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Хочу оформить",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/deposits/high-profit/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/deposits/high-profit/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Вклад &laquo;Пополняемый доход&raquo;",
		"SIDECOLUMN_TEXT" => "Вклад от&nbsp;3 месяцев до&nbsp;1 года с&nbsp;возможностью пополнения и&nbsp;ежемесячной выплатой процентов",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array("При увеличении суммы можно увеличить ставку","При оформлении в интернет-банке ставка выше на 0,25%"),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "до",
		"SIDECOLUMN_FEATURE1_TITLE" => "6,15",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "3",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "мес.",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальный<br>срок",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "30",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/deposits/the-contribution-of-the-growing-income/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Хочу открыть",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/deposits/the-contribution-of-the-growing-income/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/deposits/the-contribution-of-the-growing-income/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Вклад &laquo;Управляемый доход&raquo;",
		"SIDECOLUMN_TEXT" => "Вклад от&nbsp;3 месяцев до&nbsp;2 лет с&nbsp;возможностью пополнения, снятия и&nbsp;ежемесячной выплатой процентов",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array("При увеличении суммы можно увеличить ставку","При оформлении в интернет-банке ставка выше на 0,25%"),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "до",
		"SIDECOLUMN_FEATURE1_TITLE" => "6",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "3",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "мес.",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальный<br>срок",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "30",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/deposits/manage-profit/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Хочу открыть",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/deposits/manage-profit/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/deposits/manage-profit/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array("можно пополнять и снимать деньги","проценты выплачиваются ежемесячно",""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "6",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "срок<br>не ограничен",
		"SIDECOLUMN_FEATURE2_TITLE" => "∞",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/deposits/the-account-savings-online/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Хочу оформить",
		"SIDECOLUMN_LINK_CART" => "/personal/deposits/the-account-savings-online/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/deposits/the-account-savings-online/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Накопительный счёт, который можно открыть в интернет-банке или приложении. Позволяет накопить на цель",
		"SIDECOLUMN_TITLE" => "Счёт &laquo;Накопительный онлайн&raquo;",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "до",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,25",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "дней<br>срок вклада",
		"SIDECOLUMN_FEATURE2_TITLE" => "300",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "100",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/deposits/leadership-strategy/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Хочу оформить",
		"SIDECOLUMN_LINK_CART" => "/personal/deposits/leadership-strategy/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/deposits/leadership-strategy/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Вклад на 300 дней с&nbsp;выплатой процентов в&nbsp;конце срока, оформляемый с&nbsp;полисом ИСЖ",
		"SIDECOLUMN_TITLE" => "Вклад &laquo;Стратегия лидерства&raquo;",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> 					</div>
         				</div>
       
<!--div class="col col_size-8"> 					 
          <div class="product-cards"> 
            <div class="recommended-cards__title">Вас может заинтересовать</div>
           <img style="cursor: default;" id="bxid_815887" src="/bitrix/images/fileman/htmledit2/component.gif" /> 	</div>
         		</div-->
 			</div>
     		</div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>