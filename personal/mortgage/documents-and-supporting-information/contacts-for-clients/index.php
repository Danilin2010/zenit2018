<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Контакты для клиентов в Москве и Московской области");
?>

<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
							<h2>Связаться с банком</h2>
							<div class="form_application_line">
								<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
									 +7 (495) 967-11-11<br>
									 8 (800) 500-66-77
								</div>
								<div class="note_text">
									 звонок по России бесплатный
								</div>
							</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
		</div>
		<div class="block_type_center">
			<div class="text_block">
<p>По вопросам сопровождения ипотечных кредитов необходимо обращаться на электронную почту: oik@zenit.ru.</p>
<p>Телефон: +7 (495) 937-07-37.<br />Факс: +7 (495) 937-07-36.</p>
<p>Дополнительные контакты сотрудников:</p>
<ul class="big_list">
<li>для Заемщиков, фамилии которых начинаются с А по К (включительно) - доб. 3722;</li>
<li>для Заемщиков, фамилии которых начинаются с Л по Я (включительно) - доб. 3756.</li>
</ul>
<p>По вопросам оформления закладных, выдачи закладных, снятия обременений необходимо обращаться на электронную почту: <a href="mailto:zalogifl@zenit.ru">zalogifl@zenit.ru</a>.</p>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>