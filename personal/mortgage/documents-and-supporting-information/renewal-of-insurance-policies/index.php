<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Ежегодная пролонгация страховых полисов");
?>

<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
							<h2>Связаться с банком</h2>
							<div class="form_application_line">
								<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
									 +7 (495) 967-11-11<br>
									 8 (800) 500-66-77
								</div>
								<div class="note_text">
									 звонок по России бесплатный
								</div>
							</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
		</div>
		<div class="block_type_center">
			<div class="text_block">
<p><strong>Уважаемый Заемщик!</strong></p>
<p>В день предоставления кредитных средств Вы оформили договор(ы) страхования, который(ые) предусматривает(ют) страхование следующих рисков:</p>
<ul class="big_list">
<li>риски, связанные с причинением вреда жизни и здоровью в результате несчастного случая и/или болезни (заболевания), потеря трудоспособности (личное страхование);</li>
<li>риски утраты и повреждения недвижимости, связанные с владением, пользованием и распоряжением застрахованным имуществом (имущественное страхование);</li>
<li>риск утраты права собственности на недвижимость (титульное страхование).</li>
</ul>
<p>Договор страхования оформляется на весь период действия кредитного договора с ежегодной оплатой страховой премии. Выгодоприобретателем по договору страхования согласно условиям кредитного договора является кредитор, а именно:&nbsp;ПАО Банк ЗЕНИТ.</p>
<p>Обратите внимание на то, что осуществить оплату страховой премии необходимо до даты окончания оплаченного периода по договору страхования. Несвоевременная оплата, либо отсутствие оплаты ежегодной страховой премии приводит к потере защиты от вышеуказанных рисков.</p>
<p>Настоятельно рекомендуем Вам своевременно оплачивать ежегодные страховые премии. Для оплаты страховой премии Вам необходимо обратиться в ту же страховую компанию, где был заключен Договор страхования.</p>
<p>В соответствии с кредитным договором Заемщик обязан предоставить копии документов, подтверждающих оплату очередной страховой премии. В случае оплаты страховой премии частями своевременно вносить платежи, причитающиеся по договору страхования, а также предоставлять копии документов, подтверждающих оплату указанных платежей. Копии документы необходимо направить на адрес электронной почты: oik@zenit.ru.</p>
<p>В случае неисполнения или неполного исполнения обязательств по оплате страховой премии по договору(ам) страхования кредитор вправе досрочно истребовать всю сумму задолженности по кредитному договору, увеличить ставку по кредиту, либо применить иные штрафные санкции, предусмотренные кредитным договором.</p>
<p>Обращаем Ваше внимание на то, что осуществить страхование Вы можете только в тех страховых компаниях, которые аккредитованы Банком.</p>
				<p>По всем вопросам, связанным с пролонгацией договоров страхования / оплатой страховой премии/заключением договора страхования/предоставлением контактов страховых компаний и т.д., Вы можете обращаться по следующим контактам: e-mail: oik@zenit.ru (либо тел. +7 (495) 777-57-07, доб. 3294, 3722, 3756).</p>
<!--p>По всем вопросам, связанным с пролонгацией договоров страхования / оплатой страховой премии/заключением договора страхования и т.д., Вы можете обращаться по следующим контактам: e-mail: oik@zenit.ru (либо тел. +7 (495) 777-57-07, доб. 3294, 3722, 3756).</p-->
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>