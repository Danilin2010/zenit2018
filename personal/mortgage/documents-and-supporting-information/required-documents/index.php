<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Необходимые документы");
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
							<h2>Связаться с банком</h2>
							<div class="form_application_line">
								<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
									 +7 (495) 967-11-11<br>
									 8 (800) 500-66-77
								</div>
								<div class="note_text">
									 звонок по России бесплатный
								</div>
							</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
		</div>
		<div class="block_type_center">
		 <div class="text_block">
			 <!--h2>Необходимые документы</h2>
 <br>
 <a href="" target="_blank">Заявление-анкета</a> на получение ипотечного кредита, в том числе по программе "Военная ипотека" <br>
 <a href="" target="_blank">Заявление-анкета</a> для Солидарного заемщика/Поручителя <br>
 <a href="" target="_blank">Заявление-анкета</a> Продавца <br>
 <a href="" target="_blank">Заявление-анкета</a> Залогодателя <br>
 <a href="" target="_blank">Список документов</a> необходимых для подачи заявки Рекомендовано предоставление оригиналов документов для сверки Банком предоставленных копий <br>
 <a href="" target="_blank">Информация</a> о компании-работодателе <br>
 <a href="" target="_blank">Информация</a> по компании (для собственников бизнеса) <br>
 <a href="" target="_blank">Справка</a> о заработной плате и иных доходах<br>
			<h2>Документы, предоставляемые по недвижимости</h2>
			 Документы, предоставляемые по приобретаемой недвижимости: <br>
 <a href="" target="_blank">Список документов</a> по приобретаемой квартире <br>
 <a href="" target="_blank">Список документов</a> по приобретаемым Дому и земельному участку <br>
 <a href="" target="_blank">Список документов</a>, предоставляемый, в случае если собственником приобретаемого за счет кредитных средств недвижимого имущества является юридическое лицо<br>
			<br>
			 Документы, предоставляемые при залоге недвижимости, имеющейся в собственности: <br>
 <a href="" target="_blank">Список документов</a> по закладываемой квартире <br>
 <a href="" target="_blank">Список документов</a> по закладываемому земельному участку <br>
 <a href="" target="_blank">Список документов</a> по закладываемому Дому и земельному участку -->
			<h2>Необходимые документы</h2>
			<ul class="big_list">
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/credits/anket_loan.pdf" target="_blank">Заявление-анкета</a>&nbsp;на получение ипотечного кредита, в том числе по программе "Военная ипотека"&nbsp;</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/credits/guarantor.pdf" target="_blank">Заявление-анкета для Солидарного заемщика/Поручителя</a>&nbsp;</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/credits/seller.pdf" target="_blank">Заявление-анкета Продавца</a></li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/mortgagor.pdf" target="_blank">Заявление-анкета</a>&nbsp;Залогодателя</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/mortgage_set_docs.pdf" target="_blank">Список</a> документов необходимых для подачи заявки<br>
 <em>Рекомендовано предоставление оригиналов документов для сверки Банком предоставленных копий</em></li>
				<li><a href="https://old.zenit.ru/media/rus/content/docs/list_employer.doc" target="_blank">Информация</a>&nbsp;о компании-работодателе</li>
				<li><a href="https://old.zenit.ru/media/rus/content/docs/list_owner.doc" target="_blank">Информация</a>&nbsp;по компании (для собственников бизнеса)</li>
				<li><a href="https://old.zenit.ru/media/rus/content/docs/morgage_finance.doc" target="_blank">Справка</a>&nbsp;о заработной плате и иных доходах</li>
			</ul>
			<h2>Документы, предоставляемые по недвижимости</h2>
			<p>
 <strong><em>Документы, предоставляемые по приобретаемой недвижимости:</em></strong>
			</p>
			<ul class="big_list">
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/pawn_flat.pdf" target="_blank">Список</a> документов по приобретаемой квартире</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/pawn_house.pdf" target="_blank">Список</a>&nbsp;документов по приобретаемым Дому и земельному участку</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/pawn_ur.pdf" target="_blank">Список</a>&nbsp;документов, предоставляемый, в случае если собственником приобретаемого за счет кредитных средств недвижимого имущества является юридическое лицо</li>
			</ul>
			<p>
 <strong><em>Документы, предоставляемые при залоге недвижимости, имеющейся в собственности:</em></strong>
			</p>
			<ul class="big_list">
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/list_pledge1.pdf" target="_blank">Список</a> документов по закладываемой квартире</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/list_pledge2.pdf" target="_blank">Список</a>&nbsp;документов по закладываемому земельному участку</li>
				<li><a href="https://old.zenit.ru/media/rus/content/pdf/list_pledge3.pdf" target="_blank">Список</a>&nbsp;документов по закладываемому Дому и земельному участку</li>
			</ul>
		</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>