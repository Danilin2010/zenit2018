<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Документы и дополнительная  информация");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Документы и дополнительная информация");

?><div class="wr_block_type">
	<div class="content_rates_tabs" style="background: none;">
		<div class="content_tab_wrapper" style="background: #fff;">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Ипотека</a></li>
				<li><a href="#content-tabs-2">Военная ипотека</a></li>
				<li><a href="#content-tabs-3">Обслуживание кредита</a></li>
			</ul>
		</div>
		<div class="container about-page">
			<div class="row">
				 <!-- Правый блок мобилка и планшет -->
				<div class="col-sm-12 col-md-4 hidden-lg">
					<div class="row note_text manager-top">
						<div class="block-card right note_text">
							<h2>Связаться с банком</h2>
							<div class="form_application_line">
								<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
									 +7 (495) 967-11-11<br>
									 8 (800) 500-66-77
								</div>
								<div class="note_text">
									 звонок по России бесплатный
								</div>
							</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
						</div>
					</div>
				</div>
				 <!-- Содержимое -->
				<div class="col-sm-12 col-mt-0 col-md-8">
					<div class="row">
						<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mt-3 main-content-top">
							<div class="text_block block-card right note_text" style="color: black; font-size: large;">
								<div class="content_body" id="content-tabs-1">
									<h3>Ипотека:</h3>
									<div class="step_block">
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/requirements-for-mortgage-customer/" class="doc_item">
													Требования к клиенту по ипотеке </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/required-documents/" class="doc_item">
													Необходимые документы </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/the-main-stages-of-obtaining-a-mortgage-loan/" class="doc_item">
													Основные этапы получения ипотечного кредита </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/partners-bank-mortgage-loans/" class="doc_item">
													Партнеры Банка по Программам ипотечного кредитования </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/accredited-objects/" target="_blank" class="doc_item">
													Перечень аккредитованных Банком объектов по программе "Кредит на покупку квартир в новостройках" </a>
												</div>
											</div>
										</div>
									</div>
									<!--div class="step_block">
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/requirements-to-insurance-companies/" title="Требования к страховым компаниям" class="doc_item">Требования к страховым компаниям </a>
												</div>
											</div>
										</div>
									</div-->
									<!--div class="doc_list">
 <a href="/upload/medialibrary/1fb/mortgage_insurance.pdf" title="Перечень страховых компаний, соответствующих требованиям ПАО Банк ЗЕНИТ (по ипотечному кредитованию)" target="_blank" class="doc_item">
										<div class="doc_pict pdf">
										</div>
										<div class="doc_body">
											<div class="doc_text">
												 Перечень страховых компаний по ипотечному кредитованию
											</div>
											<div class="doc_note">
											</div>
										</div>
 </a>
									</div-->
									 <!--div class="doc_list">
 <a href="/upload/medialibrary/bbb/rules.pdf" title="Информация об условиях предоставления, использования и возврата кредита,обязательства заемщика, по которому обеспечены ипотекой" target="_blank" class="doc_item">
							<div class="doc_list">
								<div class="doc_pict pdf">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										Информация об условиях предоставления, использования и возврата кредита, обязательства заемщика по которому обеспечены ипотекой
									</div>
									<div class="doc_note">
									</div>
								</div>
							</div>
 </a>
						</div-->
									<div class="doc_list">
 <a href="/upload/medialibrary/d41/rules_20171227.pdf" title="Информация об условиях предоставления, использования и возврата кредита,обязательства заемщика, по которому обеспечены ипотекой" target="_blank">
										<div class="doc_list">
											<div class="doc_pict pdf">
											</div>
											<div class="doc_body">
												<div class="doc_text">
													 Информация об условиях предоставления, использования и возврата кредита, обязательства заемщика по которому обеспечены ипотекой<br>
												</div>
												<div class="doc_note">
												</div>
											</div>
										</div>
 </a>
									</div>
									<br>
								</div>
								<div class="content_body" id="content-tabs-2">
									<h3>Военная ипотека:</h3>
									<div class="doc_list">
										<a href="/media/doc/personal/mortgage/memo_army_mortgage.pdf" title="Памятка заемщику - военнослужащему «Шаги по оформлению права собственности»" target="_blank" class="doc_item">
										<div class="doc_list">
											<div class="doc_pict pdf">
											</div>
											<div class="doc_body">
												<div class="doc_text">
													 Памятка заемщику - военнослужащему «Шаги по оформлению права собственности»<!--(Первичный рынок жилья)-->
												</div>
												<div class="doc_note">
												</div>
											</div>
										</div>
 </a>
									</div>
									<div class="doc_list">
 <a href="/upload/medialibrary/14d/anketa_army_aizhk.pdf" title="Анкета-заявление на получение ипотечного кредита по программе Военная ипотека АИЖК" target="_blank" class="doc_item">
										<div class="doc_list">
											<div class="doc_pict pdf">
											</div>
											<div class="doc_body">
												<div class="doc_text">
													 Анкета-заявление на получение ипотечного кредита по программе "Военная ипотека АИЖК"
												</div>
												<div class="doc_note">
												</div>
											</div>
										</div>
 </a> <br>
									</div>
									<div class="doc_list">
 <a href="/media/doc/personal/mortgage/anket_loan.pdf" title="Заявление-анкета Заемщика" target="_blank" class="doc_item">
										<div class="doc_list">
											<div class="doc_pict pdf">
											</div>
											<div class="doc_body">
												<div class="doc_text">
													 Заявление-анкета Заемщика
												</div>
												<div class="doc_note">
												</div>
											</div>
										</div>
 </a> <br>
									</div>
									<div class="doc_list">
 <a href="/media/doc/personal/mortgage/guarantor.pdf" title="Заявление-анкета Созаемщика" target="_blank" class="doc_item">
										<div class="doc_list">
											<div class="doc_pict pdf">
											</div>
											<div class="doc_body">
												<div class="doc_text">
													 Заявление-анкета Созаемщика
												</div>
												<div class="doc_note">
												</div>
											</div>
										</div>
 </a> <br>
									</div>
									<div class="step_block">
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/accredited-military/" title="Перечень Застройщиков и объектов, аккредитованных Банком по программе Военная ипотека на первичном рынке" target="_parent" class="doc_item">Перечень Застройщиков и объектов аккредитованных Банком по программе "Военная ипотека" на первичном рынке</a>
												</div>
											</div>
										</div>
									</div>
									<div class="step_block">
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/claim-against-the-estate-acquired-under-the-program-military-mortgage/" title="Требования к недвижимости приобретаемой по программе Военная ипотека" class="doc_item">Требования к недвижимости приобретаемой по программе "Военная ипотека"</a>
												</div>
											</div>
										</div>
 <br>
									</div>
								</div>
								<div class="content_body" id="content-tabs-3">
									<h3>Обслуживание кредита:</h3>
									<div class="step_block">
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/loan-restructuring/" class="doc_item">
													Реструктуризация кредита </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/documents-for-early-repayment/" class="doc_item">
													Документы для досрочного погашения </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/the-removal-of-encumbrances-from-the-property/" class="doc_item">
													Снятие обременения с объекта недвижимости </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/application-forms-for-the-loan/" class="doc_item">
													Формы заявлений по действующим кредитам </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/service-mortgage-loans-of-other-banks" class="doc_item">
													Обслуживание ипотечных кредитов других банков </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/renewal-of-insurance-policies" class="doc_item">
													Ежегодная пролонгация страховых полисов </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/tax-deduction" class="doc_item">
													Налоговый вычет </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/maternity-capital" class="doc_item">
													Материнский капитал </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/mortgage/documents-and-supporting-information/contacts-for-clients" target="_blank" class="doc_item">
													Контакты сотрудников, занимающихся сопровождением ипотечных кредитов </a>
												</div>
											</div>
										</div>
										<div class="step_item">
											<div class="step_num2">
												 i
											</div>
											<div class="step_body">
												<div class="step_title">
 <a href="/personal/tariffs/" target="_blank" class="doc_item">
													Тарифы комиссионного вознаграждения за обслуживание действующих кредитов, выданных физическим лицам </a>
												</div>
											</div>
										</div>
									</div>
		<div class="doc_list">
			<a href="/media/doc/personal/mortgage/rules_refinans_2018.pdf" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						 Информация об условиях предоставления, использования и возврата кредита, выданного на цели погашения (рефинансирования) ипотечных кредитов сторонних банков
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div> <br>
								</div>
								<p>
 <b>При регистрации права собственности на квартиру, приобретенную на этапе строительства</b>:<br>
									 Согласно действующему законодательству РФ, а также Административному Регламенту федеральной службы государственной регистрации, кадастра и картографии по предоставлению государственной услуги по государственной регистрации прав на недвижимое имущество и сделок с ними, утвержденный Приказом Минэкономразвития России от 09.12.2014г. № 789, ПАО Банк ЗЕНИТ НЕ предоставляет второй экземпляр (оригинал) кредитного договора, ксерокопия кредитного договора НЕ должна быть заверена Банком.
								</p>
								<!--hr-->
								<!--p>
 <b>Уважаемые клиенты Банка!</b><br>
 <b>Порядок определения процентной ставки зафиксирован в кредитном договоре/оферте, заключенном/ой с ПАО Банк ЗЕНИТ. Если условием договора/оферты предусмотрена фиксированная ставка, то она действует весь срок кредитования и не подлежит пересмотру.</b>
								</p-->
							</div>
						</div>
					</div>
				</div>
				 <!-- Правый блок для ПС -->
				<div class="col-md-4 hidden-xs hidden-sm">
					<div class="row">
						<div class="col-sm-12">
							<div class="block-card right note_text">
								<h2>Связаться с банком</h2>
								<div class="form_application_line">
									<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
										 +7 (495) 967-11-11<br>
										 8 (800) 500-66-77
									</div>
									<div class="note_text">
										 звонок по России бесплатный
									</div>
								</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>