<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Требования к клиенту по ипотеке");
?>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
<div class="text_block">
<h2><strong>Требования к заемщикам/созаемщикам</strong></h2>
<table class="responsive-stacked-table tb" style="width: 100%;" cellspacing="0" cellpadding="0">
<tbody>
<tr>
	<td>
		 Гражданство
	</td>
	<td>
		 Наличие гражданства РФ
	</td>
</tr>

<tr>
	<td>
		 Регистрация по месту жительства или пребывания
	</td>
	<td>
		 Регистрация по месту жительства - на территории РФ.<br>
		 В случае, если адрес постоянной регистрации находится не в регионе присутствия Банка, необходимо оформление регистрации по месту пребывания в одном из регионов присутствия Банка.
	</td>
</tr>
<tr>
	<td>
		 Регистрация по месту жительства
	</td>
	<td>
		 На территории РФ
	</td>
</tr>
</tbody>
</table>
					</div>
					</div>
				</div>
			</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>