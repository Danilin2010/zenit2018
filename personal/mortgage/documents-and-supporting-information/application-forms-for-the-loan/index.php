<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Формы заявлений по действующим кредитам");
?>

<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
							<h2>Связаться с банком</h2>
							<div class="form_application_line">
								<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
									 +7 (495) 967-11-11<br>
									 8 (800) 500-66-77
								</div>
								<div class="note_text">
									 звонок по России бесплатный
								</div>
							</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
		</div>
		<div class="block_type_center">
			<div class="text_block">
<!--ul>
	<li--><a href="/media/doc/personal/mortgage/statement_on_the_provision_of_services.pdf" target="_blank">Заявление</a> на предоставление услуги<!--/li>
	<!--li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/replan.pdf" target="_blank">Заявление на перепланировку в Объекте недвижимости</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/3registr.pdf" target="_blank">Заявление на регистрацию третьих лиц в Объекте недвижимости</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/rent.pdf" target="_blank">Заявление на сдачу в аренду Объекта недвижимости</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/spr_pens.pdf" target="_blank">Заявление на справку в Пенсионный Фонд</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/percent.pdf" target="_blank">Заявление на справку о выплаченных процентах</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/close.pdf" target="_blank">Заявление на справку о закрытии кредита</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/ostat.pdf" target="_blank">Заявление на справку об остатке ссудной задолженности</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/copies.pdf" target="_blank">Заявление o предоставлении копий кредитно-обеспечительной документации</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/changes.pdf" target="_blank">Заявление о внесении изменений</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/object_bay.pdf" target="_blank">Заявление на реализацию предмета залога</a></li>
<li><a href="https://old.zenit.ru/media/rus/content/pdf/mortgage/exclude.pdf" target="_blank">Заявление для военнослужащих об исключении информации из кредитной истории</a></li-->
</ul-->
<br>
				<p>Заявление можно передать на рассмотрение следующими способами:</p>
				<ul class="big_list">
					<li>направить в сканированном виде на электронную почту oik@zenit.ru <div class="doc_note">(в теме письма необходимо обязательно указать ФИО Заемщика и номер Кредитного договора);</div></li>
					<li>направить почтовым отправлением по адресу: <div class="doc_note">129110, г. Москва, Банный пер., д. 9;</div></li>
					<li>передать в Дополнительный офис Банка с пометкой "В отдел сопровождения ЦИК".</li>
<!--p>Вы можете воспользоваться <a title="index" target="_blank" href="https://old.zenit.ru/rus/retail/catalog/ipotek_credit/use/forms/order_documents/index.wbp">online-заказом</a> документов Банка. Запрос принимается только от Заемщиков Банка, обслуживающих кредит в г. Москве, Московской области!</p-->
<p>Срок обработки запроса составляет ориентировочно 5 рабочих дней.</p>
<p>О готовности документов Вас проинформирует сотрудник Банка.</p>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>