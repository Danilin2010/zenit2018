<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
$APPLICATION->SetTitle("Партнеры Банка по Программам ипотечного кредитования");
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
							<h2>Связаться с банком</h2>
							<div class="form_application_line">
								<div class="contacts_block">
 <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
									 +7 (495) 967-11-11<br>
									 8 (800) 500-66-77
								</div>
								<div class="note_text">
									 звонок по России бесплатный
								</div>
							</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<h2>Риэлторские компании</h2>
				<ul class="big_list">
					<li><a href="http://www.ipoteka-miel.ru/">Агенство «МИЭЛЬ»</a><br>
					Телефон: (495) 777-33-77</li>
					<li><a href="http://new.miel.ru/" target="_blank">Компания «МИЭЛЬ-Новостройки»<br>
					</a>Телефон: (495) 777-33-33</li>
					<li><a href="http://www.incom.ru/" target="_blank">Корпорация «ИНКОМ-НЕДВИЖИМОСТЬ»</a><br>
					Телефон: (495) 363-10-38<br>
					Е-mail: <a href="mailto:ipoteka@incom.ru">ipoteka@incom.ru</a></li>
					<li><a href="http://www.npetelina.ru" target="_blank">Агентство недвижимости Петелиной</a><br>
					Телефон: (499) 518-15-15</li>
				</ul>
				<h2>Инвестиционные компании</h2>
				<ul class="big_list">
					<li><a href="http://s-a-m-p-o.ru" target="_blank">ООО «Микрорайон «Кантри» ЖК «SAMPO»</a><br>
					Телефон: (495) 125-46-36</li>
					<li><a href="http://primavera-kazan.ru" target="_parent">ЗАО «Коттеджный поселок «Загородная усадьба»</a><br>
					Телефон: (843) 22-55-007, (843) 22-66-007</li>
					<li><a href="http://www.more-plaza.ru/index.php" target="_blank">ЗАО «М.О.Р.Е. - ПЛАЗА»</a><br>
					Телефон: (495) 755-77-40<br>
					Контактные лица: Ломтева Екатерина Вячеславовна,<br>
					Власова Виктория Валерьевна.</li>
					<li><a href="http://su155.ru/" target="_blank">Инвестиционно-строительная группа компаний «СУ-155»<br>
					</a>Телефоны: (495) 967-14-92</li>
					<li><a href="http://konti.ru/" target="_blank">ГК «КОНТИ»</a><br>
					Москва, ул. Герасима Курина, д. 10. корп. 2.<br>
					Телефон: (495) 933-35-35</li>
					<li><a href="http://www.morton.ru/">ГК "МОРТОН"</a><br>
					г. Москва, ул. Авиамоторная, д. 19.<br>
					Телефоны: (495) 921 22 21, (495) 921 10 20</li>
					<li><a href="http://www.tatimmobilien.ru/" target="_blank">ООО «ТАТ иммобилен»<br>
					</a>420126, Республика Татарстан, г. Казань, ул. Чистопольская, д. 42.<br>
					Телефон: (843) 525-75-32</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>