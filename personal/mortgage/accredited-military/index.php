<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Аккредитованные застройщики и объекты");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
?>
<div class="wr_block_type">
	<div class="block_type to_column z-container">
		<div class="block_type_center">
			<div class="text_block">
				<h2>Московский регион</h2>
				<div class="faq">
					<!--div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ЖК «SAMPO»</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="left" valign="top"> <p>ООО «Микрорайон «Кантри»</p> </td>
										<td align="left" valign="top"> <p>Московская область, Истринский муниципальный район, сельское поселение Обушковское, вблизи деревни Красный поселок, дом 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,&nbsp;24,&nbsp;25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44</p> </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div-->
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК Гранель Групп</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> <p>ООО «Гранель»&nbsp;</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, Ленинский район, сельское поселение Булатниковское, вблизи д. Лопатино, поз. 1, 2, 3, 4, 5, 6, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 (ЖК "Государев дом")</li>
												<li>Московская область, Мытищинский район, городское поселение Мытищи, восточнее д. Погорелки, жилые дома №13,<!-- №14,--> №15 корп. 1, №15 корп. 2, <!--№16, -->№18<!--, №19, №20 -->(ЖК «Императорские Мытищи»)</li>
												<li>Московская область, г.Королев, мкр. Болшево, ул.Спартаковская, д.1А, корпус 4 ( ЖК «Театральный парк»)</li>
												<li>Московская область, г. Королев, мкр. Болшево, ул. Полевая, д. 9, поз. 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 27, 28, 31, 32, 41 (ЖК «Театральный парк»)</li>
												<li>Московская область, Красногорский муниципальный район, городское поселение Нахабино, вблизи р.п. Нахабино, к. 5.1, 5.3, 5.5 (ЖК «Малина»)</li>												
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «Гранель Девелопмент»</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, г. Балашиха, севернее улицы Лукино, корпуса 7, 8, 9, 10, 11, 12, 13, 14,&nbsp;15&nbsp;(квартал «Алексеевская роща»)</li>
												<li>Московская область, город Королев, улица Горького, дом 79, корпуса 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,&nbsp;12, 13, 14, 15,&nbsp;16, 17, 18,&nbsp;19, 20, 21, 22, 23&nbsp;(ЖК «Валентиновка парк»)</li>
											</ul>
										</td> 
									</tr>
									<tr> 
										<td> <p>ООО «Лэнд Юг»</p> </td> 
										<td> <p>г. Москва, НАО, пос. Сосенское, вблизи дер. Столбово, участок 2, дом № 4, дом № 6 (ЖК «Москвичка»)</p> </td>
									</tr>
								</tbody>
							</table>    
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК ПИК</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> <p>ООО «ЗемПроектСтрой»</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, Балашихинский район, 28-й км автомагистрали М7 «Волга»,  микрорайон «Сакраменто», корпуса №  17, 23, 28, 34, 41, 42</li>
												<li>Московская область, г.о. Балашиха, 28-й км. автомагистрали М7 «Волга», северо-восточная часть квартала «Новский», квартал «А», поз. 1, 2, 4, 5</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «ПрометейСити»</p> </td>
										<td>
											<p>
												Москва, ЗелАО, Крюково, д. Андреевка, поз. 1, 2, 3, 8, 10, корп. 1,2 (мкрн "Жемчужина Зеленограда")
											</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО«ГрадОлимп»</p> </td>
										<td>
											<p>
												Московская область, Красногорский муниципальный район, с/п Ильинское, вблизи поселка «Ильинское-Усово», корпус 36, корпус 37, корпус 38, корпус 39, корпус 40 (ЖК "Новорижские кварталы")
											</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «Мортон-РСО»</p> </td>
										<td>
											<p>
												Московская область, г. Лобня, микрорайон «Катюшки», корпус 5, 6
											</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «Лотан»</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, Ленинский муниципальный район, сельское поселение Булатниковское, вблизи д. Дрожжино, позиция 4, 5, 7, 8, 8.1, 11, 13, 14.1, 15, 15.1, 15.2, 17 (микрорайон «Дрожжино 2»)»), уч.-21-ю, поз. 12.2, 32, 33, 34</li>
												<li>Московская область, Ленинский район, сельское поселение Булатниковское, в районе д. Боброво, уч.2, "Восточное Бутово", поз. 9, 10, 11, 12, 28, 29, 31, 32, 33, 34, 35, 36, 37, 26, 27</li>
												<li>Московская область, Ленинский район, Булатниковский с/о, д. Боброво, мкр. «Боброво» (Восточное Бутово), ж.д поз.17,18,19,19а</li>
												<li>Московская область, Ленинский район, с/о Булатниковский, поз.30</li>
												<li>Московская область, Ленинский район, Булатниковское сельское поселение, д. Боброво, мкр-н МортонГрад "Боброво" поз.22, 23, 24, 25</li>
												<li>Московская область, с/п Булатниковское, д. Дрожжино, ж.к. "Дрожжино-2", поз. 12.1, 28.1, 31,31.1</li>
												<li>Московская область, Ленинский район, Булатниковское сельское поселение, д. Дрожжино, уч. 2 поз. 1, поз. 35</li>
												<li>Московская область, Ленинский район, с/о Булатиновское, поз. 29, 30 (мкр. Бутово, Дрожжино-2)</li>
												<li>Московская область, Булатниковское сельское поселение, вблизи д. Дрожжино, жилой комплекс «Дрожжино-2», позиция 28</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «РИВАС МО»</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, Красногорский район, вблизи дер. Путилково, корпус 1, 2, 4, 7, 8, 9, 10, 12, 13, 19, 23, 26 30, позиция 31</li>
												<li>Московская область, Красногорский муниципальный район, городское поселение Нахабино, р.п. Нахабино, ул. Красноармейская, стр.корп.57, вблизи д.49 (ЖК «Нахабино»)</li>
												<li>Московская область, Красногорский муниципальный район, сельское поселение Отрадненское, дер. Путилково, поз. 41, корп. 41.2, 41.3</li>
												<!--li>Московская область, Красногорский муниципальный район, сельское поселение Отрадненское, дер. Путилково, поз. 41, корп. 41.3</li-->
											</ul>
										</td> 
									</tr>
									<tr> <td> <p>ЗАО «СТ-Инжиниринг»</p> </td> <td> <p>Московская область, Ленинский муниципальный район, сельское поселение Булатниковское, вблизи д. Бутово, позиция 15&nbsp; (микрорайон «Бутово Парк»), позиция 22</p> </td> </tr>
		 							<tr> <td> <p>ООО «НИКП»</p> </td> <td> <p>Московская область, г. Лобня, ул. Юности, дом 1, 3, 5, 9, 13, 15, 17 (микрорайон «Катюшки 2»), «Катюшки-Юг», поз. 20</p> </td> </tr>
		 							<tr> <td> <p>НИПП Ассоциация «Народное домостроение» (некоммерческая организация)</p> </td> <td> <p>Москва, СВАО, Северный, пос. Северный, микрорайон I-Б.1, I-Б.2, I-Б.3 и II, корпуса К-1,К-5, К-6, К-7, К-8, поз. 2,9,10</p> </td> </tr>
		 							<tr> <td> <p>ООО «Брод-Эстейт»</p> </td> <td> <p>г. Москва, г. Щербинка, ул. Овражная, дом 3, 4,&nbsp;5,&nbsp;6,&nbsp;7</p> </td> </tr>
		 							<tr> <td> <p>ООО «Агентство недвижимости «Ключ»</p> </td> <td> <p>Московская область, г. Балашиха, 19-й км Щелковского шоссе, мкрн. «Щитниково», позиция 29 (ЖК «Янтарный»)</p> <p>Московская область, г. Балашиха, 19-й км Щелковского шоссе, квартал Б, поз.25</p> </td> </tr>
									<tr> <td> <p>ООО «РусСтройГарант»</p> </td> 
										<td>
											<ul class="big_list">
												<li>Московская область, Люберецкий район, г. Люберцы, мкр. 12 «Красная горка», корпус 1,&nbsp;2, 2а, 3, 10, 11 (ЖК «Люберецкий»)</li>
												<li>Московская область, Люберецкий район, г. Люберцы, район Красная горка, мкр.12, корпус 4.1</li>
											</ul> 
										</td> 
									</tr>
		 							<tr> <td> <p>ООО «Ковчег»</p> </td> <td> <p>Московская область, г. Мытищи, ул. Рождественская, корп. 1, 2 (ЖК «Рождественский»)</p> </td> </tr>
		 							<tr> 
										<td> <p>ООО «ТехноСтрой»</p> </td> 
										<td> 
											<ul class="big_list">
												<li>г. Москва, ул. Красноказарменная, вл.14А, жилой корпус №1, жилой корпус №3, (ЖК «Петр I»)</li>
												<li>г. Москва, ул. Красноказарменная, вл. 14А, корп. 8, 9, 10 (ЖК «Лефортово Парк»)</li>
										</td> 
									</tr> 
		 							<tr> <td> <p>АО «Межрегионоптторг»</p> </td> <td> <p>г. Москва, ЮВАО, район Люблино, ул. Цимлянская, вл. 3, поз. К-1 (ЖК "Влюблино")</p> </td> </tr>
									<tr>
										<td> <p>АО «Первая Ипотечная Компания-Регион»</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, Одинцовский район, городское поселение Одинцово, г. Одинцово-1, корпус 1.12, 1.13, 1.14, 1.15, 1.16, 1.7 (Жилой район «Одинцово-1»)</li>
												<li>Московская область, г. Химки, 6,7,8 микрорайоны, корпус 1,2, 47В, 47Г, 8, 1.1, 1.2, 1.3, 66, 67 (Жилой район «Новокуркино»)</li>
												<li>Московская область, г. Химки, микрорайон Левобережный, улица Совхозная, корпус 1.1, 1.2, 7.1, 7.2</li>
												<li>Московская область, Красногорский район,г. Красногорск, коммунальная зона «Красногорск-Митино», корп.1</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ПАО "Группа Компаний ПИК"</p> </td>
										<td>
											<ul class="big_list">
												<li>г. Москва, НАО, п. Сосенское, вблизи д. Столбово, поз. 1.3, 1.4/1, 1.4/2, 1.5/1, 1.5/2, 1.5/3, 1.6/1, 1.6/2, 1.7/1, 1.7/2, 1.8, 1.9.1, 1.9/2, 1.10 (ЖК «Бунинские Луга»)</li>
												<li>г. Москва, ЮАО, Чертаново Южное, Варшавское шоссе, многофункциональный жилой комплекс, корп.5,6,7,8 (ЖК «Варшавское шоссе, 141)</li>
												<li>г. Москва, Южный административный округ, район Чертаново Южное, Варшавское шоссе, владение 141, корпус 2, строение 1,2,3,4,5</li>
												<li>г. Москва, СВАО, Останкинский район, Сельскохозяйственная улица, вл. 35, стр.100, этапы 1,2,3 (ЖК «Грин Парк»)</li>
												<li>г. Москва, СВАО, район Отрадное, Высоковольный проезд, корп.1, 2, 3, 4 (ЖК Римского-Корсакова 11)</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>АО "ПИК-Регион"</p> </td>
										<td>
											<ul class="big_list">
												<li>Московская область, Красногорский район, г. Красногорск, коммунальная зона «Красногорск-Митино», корпус 2</li>
												<li>Московская область, Одинцовский район, г. Одинцово-1 (бывший военный городок №315), городское поселение Одинцово, корпус 1.8, 1.9, 1.10</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>АО "Завод железобетонных изделий №23"</p> </td>
										<td>
											<ul class="big_list">
												<li>г. Москва, САО, ул. Черняховского, д.19, корп.1</li>
												<li>Москва, САО, район Аэропорт, ул. Черняховского, вл.19, Большой Коптевский проезд, вл. 16, корп. 2</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «Вейстоун»</p> </td>
										<td>
											<p>г. Москва, улица Маршала Захарова вл.7</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ЗАО "МОНЕТЧИК"</p> </td>
										<td>
											<p>г. Москва, ЗАО, район Кунцево, квартал 20, корп.38 (Рублевское ш., вл.105), квартал 20, корп.28Б (Рублевское шоссе, вл.101) (ЖК "Vander Park")</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО "Загородная усадьба"</p> </td>
										<td>
											<p>Московская область, г. Мытищи, мкр.16, корп.27, 30, 38, 39.1, 39.2, 42.1, 42.2 (ЖК «Ярославский»)</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «Тирон»</p> </td>
										<td>
											<p>г. Москва, НАО, поселение Московский, в районе д. Саларьево, уч.22/1, корп.1,2,3,4,5, корп.7 стр.1, корп.7 стр.2, корп.8,9 (ЖК «Саларьево Парк»)</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО "РегионИнвест"</p> </td>
										<td>
											<p>Московская область, г. Котельники, мкр. Опытное поле, вл 10/2, корп.1,2,3,4,5 (ЖК «Оранж Парк»)</p>
										</td> 
									</tr>
									<tr>
										<td> <p>АО «ПИК-Индустрия»</p> </td>
										<td>
											<ul class="big_list">
												<li>г.Москва, ЗАО, район Солнцево, Боровское шоссе, вл. 2, корп. 1.1, 1.2, 2.1, 2.2, 3.1, 3.2 (ЖК «Мещерский лес»)</li>
												<li>Москва, ЗАО, район Солнцево, Боровское шоссе, вл.2-2, корп. 4.1, 4.2, 4.3, 4.4 (ЖК «Мещерский лес»)</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО "Ривьера Парк"</p> </td>
										<td>
											<p>Московская область, городской округ Балашиха, 16 км автодороги М7 «Волга», 1 этап – жилой дом корпус 1.1, 1.2</p>
										</td> 
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ООО «Кутузовское-1»</div>
						</div>
						<div class="faq_text" style="display: none;">
							<ul class="big_list">
								<li>Московская область, Солнечногорский район, с.п. Кутузовское, д. Рузино, жилые дома IV.06, IV.07</li>
							</ul>    
						</div>
					</div>
					<!--div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ООО «СФД»</div>
						</div>
						<div class="faq_text" style="display: none;">
							<ul class="big_list">
								<li>Московская область, Мытищинский район, городское поселение Мытищи, юго-западнее деревни Болтино, корп. 1, 9, 12 (ЖК «Мытищи Lite»)</li>
							</ul>    
						</div>
					</div-->
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК ОПИН</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> <p>ООО «Мартемьяново»</p> </td>
										<td>
											<p>Московская область, Наро-Фоминский район, г.п. Апрелевка, город Апрелевка, микрорайон «Мартемьяново-7», корп. 5,6,7,8 (ЖК VESNA)</p>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО «Павловский квартал»</p> </td>
										<td>
											<p>Московская область, Истринский район, с.п. Павлово-Слободское, д. Лобаново, ул. Новая, жилой дом №№9-20 (ЖК «Павловский квартал»)</p>
										</td> 
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК Основа</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> <p>ООО «С-СтройПерспектива»</p> </td>
										<td>
											<p>Московская область, г. Королев, ул. Академика Легостаева, д. 8 (ЖК «Парад Планет»)</p>
										</td> 
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ФСК "ЛИДЕР"</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> <p>ООО "Акколада Лэнд"</p> </td>
										<td>
											<ul class="big_list">
												<li>МО, г/о Мытищи, д. Бородино, дом (корпус) 3,4(UP-квартал «Скандинавский» )</li>
											</ul>
										</td> 
									</tr>
									<tr>
										<td> <p>ООО "Балашиха- Сити"</p> </td>
										<td>
											<ul class="big_list">
												<li>МО, г. Балашиха, Западная коммунальная зона, ш. Энтузиастов, дом (корпус) 1,2 (ЖК «Новогиреевский»)</li>
											</ul>
										</td> 
									</tr>
									<tr> 
										<td> <p>ООО "Сигналстройгрупп"</p> </td> 
										<td> <p>г. Москва, СВАО, Сигнальный пр., вл.5, корпус 2,3 (ЖК «Поколение»)</p> </td>
									</tr>
									<tr> 
										<td> <p>ООО "СоюзАГРО"</p> </td> 
										<td> <p>Московская область, Красногорский район, деревня Путилково, дом №1. (UP-квартал «Новое Тушино»)</p> </td>
									</tr>
									<tr> 
										<td> <p>ООО "ФСК "Лидер"</p> </td> 
										<td> <p>Московская область, Одинцовский район, г. Одинцово ул. Чистяковой, корп.7,10,11,12 (UP-квартал «Сколковский»)</p> </td>
									</tr>									
								</tbody>
							</table>    
						</div>
					</div>
				</div>
				<h2>г. Санкт- Петербург</h2>
				<div class="faq">
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК ПИК</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="left" valign="top"> <p>ЗАО "ПрофСервис" </p> </td>
										<td align="left" valign="top"> <p>г. Санкт-Петербург, Дальневосточный проспект, в районе дома 15, литера А, участок 1</p> </td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК ЛСР</div>
						</div>
						<div class="faq_text" style="display: none;">
						<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="left" valign="top"> <p>ООО "ЛСР.Недвижимость-Северо-Запад"</p> </td>
										<td>
											<ul class="big_list">
												<li>г.Санкт-Петербург, пр-т Маршала Блюхера, дом 12, литера АШ, корпус 1, корпус 2, корпус 3, корпус 4, корпус 5, корпус 6</li> 
												<li>ЖК "Новая Охта. На речке", д. 1, г.Санкт-Петербург, территория предприятия "Ручьи", участок 116.10 - 11 этап строительства</li>
												<li>ЖК "Новая Охта. На речке", дом 2 (корп. 50.1), - г.Санкт-Петербург, территория предприятия "Ручьи", участок 116.12 - 12 этап строительства</li>
												<li>ЖК "Новая Охта. На речке" дом 3 (корп. 54.1) - г.Санкт-Петербург, территория предприятия "Ручьи", участок 116.16 - 14 этап строительства</li>
												<li>ЖК "Новая Охта. На речке" дом 4 (корп. 55.1) - г.Санкт-Петербург, территория предприятия "Ручьи", участок 116.19 - 16 этап строительства</li>
												<li>ЖК "Новая Охта. На речке" дома  5, 6 (корп. 58.1, 58.2) - г.Санкт-Петербург, территория предприятия "Ручьи", участок 116.20 - 17, 18 этапы строительства</li>												
												<li>ЖК "Цветной город" д. 1, 2 (корп. 198.1, 198.2) - г.Санкт-Петербург, территория предприятия "Ручьи", участок 8, квартал 17, - 1, 2 этапы строительства</li>
												<li>ЖК "Цветной город" д. 5, 6 (корп. 207.1, 207.2) - г.Санкт-Петербург, территория предприятия "Ручьи", участок 8, квартал 17, - 15, 16 этапы строительства</li>												
												<li>ЖК "Цивилизация" д. 8,9,10 г.Санкт-Петербург, Октябрьская наб., участок 54, (юго-восточнее дома 40, корпус 2, литера Г2), участок 4(по ППТ)</li>
												<li>ЖК "Шуваловский" дома 13, 14 (корп. 410.2, 410.1)  - г.Санкт-Петербург, Пригородный, участок 410</li>
												<li>ЖК "Шуваловский" дома 19, 20 (корп. 412.1, 412.2)  - г.Санкт-Петербург, Пригородный, участок 412</li>												
											</ul>
										</td>
									</tr>
								</tbody>
							</table>    
						</div>
					</div>
				</div>
				<h2>Пермский край</h2>
				<div class="faq">
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК ПИК</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="left" valign="top"> <p>АО "Первая Ипотечная Компания - Регион"</p> </td>
										<td>
											<ul class="big_list">
												<li>г. Пермь, Мотовилихинский район, жилой район ИВА, ул. Уинская, 43 (поз.7)</li>
												<li>г. Пермь, Мотовилихинский район, жилой район ИВА, ул. Грибоедова, 72 (поз.8)</li>
											</ul>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<h2>Ростовская область</h2>
				<div class="faq">
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict"><div class="faq_arr"></div></div>
							<div class="faq_top_text">ГК ПИК</div>
						</div>
						<div class="faq_text" style="display: none;">
							<table cellpadding="0" cellspacing="0" class="responsive-stacked-table tb">
								<thead>
									<tr>
										<td>Наименование застройщика</td>
										<td>Строительные адреса объектов недвижимости</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> <p>ООО "Ростовкапстрой"</p> </td>
										<td>
											<ul class="big_list">
												<li>Ростовская область, г. Ростов-на-Дону, Ворошиловский район, ул. Орбитальная, дом № 3, дом № 4</li>
												<li>Ростовская область, г. Ростов-на-Дону, застройка севернее ул. Орбитальной,  ж.д. 6 (ЖК «НОРД»)</li>
											</ul>
										</td>
									</tr>
									<tr>
										<td> <p>АО "ПИК-Регион"</p> </td>
										<td>
											<p>Ростовская область, г. Ростов-на-Дону, Ленинский р-н, пер. Доломановский, 15А, 17, 19, 21, 23, 25, 27, 27А, корпус 1.1</p>
										</td>
									</tr>
									<tr>
										<td> <p>ООО "Ростовкапстрой"</p> </td>
										<td>
											<p>Ростовская область, Аксайский район, п. Верхнетемерницкий, строительный квартал 1, дом № 12</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?> 