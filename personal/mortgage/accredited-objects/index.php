<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Перечень аккредитованных Банком объектов недвижимости");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
	<div class="block_type_center">
    <div class="text_block">
			<ul class="big_list">
                    <li class="faq_text"><a href="http://s-a-m-p-o.ru" target="_blank">Жилой комплекс SAMPO</a></li>
					<li class="faq_text"><a href="http://primavera-kazan.ru" target="_blank">Коттеджный поселок «Загородная усадьба»</a></li>
			</ul>
		<div class="faq">			
			<h2>Московский регион</h2>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Est-a-Tet</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://www.estatet.ru/catalog_new/zhk_mir_mitino/descr/" target="_blank">ЖК "Мир Митино"</a> по адресу г. Москва, внутригородское муниципальное образование Митино, вблизи с. Рождествено, корп.7,8,9,10,10.1</li>
						<li><a href="https://www.estatet.ru/catalog_new/zapadnyy_port/descr/" target="_blank">МФК  "Западный порт. Кварталы на набережной"</a> по адресу г. Москва, внутригородское муниципальное образование Филевский парк, ул. Заречная, вл. 2/1, 1 этап строительства, 1 квартал.</li>
						<li><a href="https://www.estatet.ru/catalog_new/festival_park/descr/" target="_blank">Фестиваль парк</a> по адресу г.Москва, САО, район Левобережный, мкр.2, уч.2Е, корпус 17,18,19,28</li>
						<li><a href="https://www.estatet.ru/catalog_new/vavilova_27_31/descr/" target="_blank">Вавилов дом</a> по адресу г.Москва, ЮЗАО, район Академический, ул. Вавилова, вл.27-31</li>
						<li><a href="https://www.estatet.ru/catalog_new/1147/descr/" target="_blank">1147</a> по адресу г.Москва, СВАО, район Алексеевский, ул. Маломосковская, вл.14</li>
						<li><a href="https://www.estatet.ru/catalog_new/tushino-2018/descr/" target="_blank">Город на реке Тушино 2018</a> по адресу г.Москва, СЗАО, Покровское-Стрешнево, Волоколамское шоссе, вл.67, корп. 1,2,3,4</li>
						<li><a href="https://www.estatet.ru/catalog_new/zhk_domashniy/descr/" target="_blank">микрорайон Домашний</a> по адресу г.Москва, ул.Донецкая, вл.30, 1-я оч.строительства, к.1,2, 2-я оч.стрительства, к.1,2</li>						
						<li><a href="https://www.estatet.ru/catalog_new/zhk_pozitiv/descr/" target="_blank">ЖК "Позитив"</a> по адресу г.Москва, НАО, п. Московский, вблизи д. Румянцево, уч.3/2, корп. 3, 4, 5, 6</li>
						<li><a href="https://www.estatet.ru/catalog_new/eko_vidnoe_2_0/descr/" target="_blank">Эко Видное 2.0</a> по адресу г. Московская область, Ленинский район, восточнее д. Ермолино, 1-й этап строительства, корпус 1, 2-й этап строительства, корпус 2</li>
						<li><a href="https://www.estatet.ru/catalog_new/seliger_city/descr/" target="_blank">Селигер Сити</a> по адресу  Москва, САО, Западное Дегунино, Ильменский проезд, вл.14, К-1,2, корп.В, корп. А.</li>
						<li><a href="https://www.estatet.ru/catalog_new/savelovskiy_city/descr/" target="_blank">Савеловский Сити</a> по адресу г. Москва, Северо-восточный административный округ, район Бутырский, улица Складочная, вл. 1, строение 3, 19, 20, 21, 29, 30, 34, 36, 37, 1-й пусковой комплекс – корпус В1, 2-й пусковой комплекс - корпус С1, 5-й пусковой комплекс – корпус С2, 4-й пусковой комплекс - корпус В2.</li>
						<li><a href="https://www.estatet.ru/catalog_new/apartamenti_v_mfk_vodniy/descr/" target="_blank">МФК Водный</a> по адресу г. Москва, Головинское шоссе, вл. 5</li>
						<li><a href="https://www.estatet.ru/catalog_new/zhk_yasnyiy/descr/" target="_blank">ЖК Ясный</a> по адресу г. Москва, Орехово-Борисово Южное, Каширское шоссе, вл. 65</li>
						<li><a href="https://www.estatet.ru/catalog_new/tsarskaya_ploshchad_leningradskiy_prospekt_31/descr/" target="_blank">ЖК «Царская площадь»</a> по адресу г. Москва, Ленинградский проспект, вл. 31</li>
					</ul>    
				</div>
            </div>	
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Villagio Estate</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://villagio.ru/villages/futuropark/" target="_blank">ЖК «Футуро Парк»</a> по адресу Московская область, Истринский район, с.п. Обушковское, д. Покровское, блокированный жилой дом №29,30,36,37,31, 34, 33,35,39,41,42,44,45,46, 48,49,50,51,76,77,78,79,40,52,53,60</li>
						<li><a href="https://www.villagio.ru/villages/parkfonte/" target="_blank">ЖК «Парк Фонтэ»</a> по адресу Московская область, Истринский м/р, с.п. Обушковское, д. Красный поселок, д.7, корпуса 1,2,3,4,5,6,7,8,9,10,11,12,15,16,17,19</li>						
					</ul>    
				</div>
			</div>						
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">БЕСТ-Новострой</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/szao/zhk_mir_mitino/" target="_blank">ЖК "Мир Митино"</a> по адресу г. Москва, внутригородское муниципальное образование Митино, вблизи с. Рождествено, корп.7,8,9,10,10.1 </li>
						<li><a href="http://best-novostroy.ru/novostroyki_podmoskovya/novokraskovo/" target="_blank">ЖК "Новокрасково"</a> по адресу Московская область, Люберецкий р-н, г.п. Красково, д.п. Красково, Егорьевское шоссе в районе д.1 (на пересечении МЕТК и автомобильной дороги Красково-Коренево-Торбеево), корпус 1, 2, 3, 4</li>
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/zao/zhk_zapadnyy_port/" target="_blank">МФК  "Западный порт. Кварталы на набережной"</a> по адресу г.Москва, внутригородское муниципальное образование Филевский парк, ул. Заречная, вл. 2/1, 1 этап строительства, 1 квартал.</li>
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/sao/zhk_festivalnoy/" target="_blank">Фестиваль парк</a> по адресу г.Москва, САО, район Левобережный, мкр.2, уч.2Е, корпус 17,18,19,28</li>
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/uzao/zhk_na_vavilova/" target="_blank">Вавилов дом</a> по адресу г.Москва, ЮЗАО, район Академический, ул. Вавилова, вл.27-31</li>
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/szao/zhk_tushino_2018/" target="_blank">Город на реке Тушино 2018</a> по адресу г.Москва, СЗАО, Покровское-Стрешнево, Волоколамское шоссе, вл.67, корп. 1,2,3,4</li>
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/uvao/zhk_domashniy/#" target="_blank">микрорайон Домашний</a> по адресу г.Москва, ул.Донецкая, вл.30, 1-я оч.строительства, к.1,2, 2-я оч.стрительства, к.1,2</li>						
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/new_moscow/zhk_pozitiv/" target="_blank">ЖК "Позитив"</a> по адресу г.Москва, НАО, п. Московский, вблизи д. Румянцево, уч.3/2, корп. 3, 4, 5, 6</li>
					</ul>    
				</div>
            </div>
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Группа компаний Гранель</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://www.granelle.ru/objects/tpark/" target="_blank">ЖК «Театральный парк»</a> по адресу: Московская область, г. Королев, мкр. Болшево, ул. Полевая, д. 9, поз. 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 27, 28, 31, 32; ул.Спартаковская, д.1А, корпус 4</li>
						<li><a href="https://www.granelle.ru/objects/leninsky-district/" target="_blank">ЖК «Государев дом»</a> по адресу: Московская область, Ленинский район, сельское поселение Булатниковское, вблизи д. Лопатино поз. 1, 2, 3, 4, 5, 6, 11, 12, 14, 23, 24, 25, 26, 28, 29</li>
						<li><a href="https://www.granelle.ru/objects/leninsky-district/" target="_blank">ЖК «Государев дом»</a> по адресу: Московская область, Ленинский муниципальный район, с.п. Булатниковское, вблизи д. Лопатино, 1 этап строительства, жилые дома 3-й очереди, поз.16</li>
						<li><a href="https://www.granelle.ru/objects/mytishi/" target="_blank">ЖК «Императорские Мытищи»</a> по адресу: Московская область, Мытищинский район, городское поселение Мытищи, восточнее д. Погорелки, жилой дом №15.1, 15.2,18</li>
						<li><a href="https://www.granelle.ru/objects/moskvichka/" target="_blank">ЖК «Мовсквичка»</a> по адресу: г. Москва, НАО, пос. Сосенское, вблизи дер. Столбово, участок 2, дом №2,4,6</li>
						<li><a href="https://www.granelle.ru/objects/malina/" target="_blank">ЖК «Малина»</a> по адресу: Московская область, Красногорский муниципальный район, городское поселение Нахабино, вблизи р.п. Нахабино, к. 5.1, 5.3, 5.5</li>
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ГРУППА МГ</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
                    <ul class="big_list">
						<li><a href="http://pirogovo-dialekt.ru/" target="_blank">ЖК "Диалект"</a> по адресу Московская область, Мытищинский район, рабочий поселок Пироговский</li>
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Группа компаний ОПИН</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://vesnadom.ru/" target="_blank">ЖК «VESNA»</a> по адресу Московская область, Наро-Фоминский р-н, городское поселение Апрелевка, микрорайон «Мартемьяново-7», корпуса №№ 5 – 8, застройщик ООО "Мартемьяново"</li>
						<!--li><a href="http://pavkvartal.ru/" target="_blank">ЖК «Павловский квартал»</a> по адресу Московская область, Истринский район, сельское поселение Павло-Слободское, д. Лобаново, ул. Новая, дома №№ 9 – 20, застройщик ООО "Павловский квартал"</li-->
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ДОНСТРОЙ</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://simvol.com/" target="_blank">ЖК Символ (квартал Свобода)</a> по адресу г.Москва, ЮВАО, Золоторожский Вал, вл.11, очередь "1Б" корп. 27, 28, 29а, 29б, 33</li>
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">КРОСТ</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://www.krost.ru/live/jk-novaya-zvezda/" target="_blank">ЖК "Новая звезда"</a> по адресу г. Москва, поселение Сосенское, в районе пос. Газопровод, корп. 5</li>
					</ul>    
				</div>
            </div>			
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Компания MR Group</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://filigrad.ru" target="_blank">Жилой комплекс «Фили Град»</a></li>
						<li><a href="http://vodny2.ru" target="_blank">Многофункциональный комплекс «Водный»</a></li>
						<li><a href="http://vorobiev-dom.ru" target="_blank">Многофункциональный жилой комплекс «Воробьев дом»</a></li>
						<li><a href="http://yasny-dom.ru/" target="_blank">ЖК Ясный</a></li>
						<li><a href="http://царская-площадь.рф/" target="_blank">ЖК «Царская площадь»</a></li>
						<li><a href="http://savcity.ru/" target="_blank">Савеловский Сити</a> по адресу г. Москва, Северо-восточный административный округ, район Бутырский, улица Складочная, вл. 1, строение 3, 19, 20, 21, 29, 30, 34, 36, 37, 1-й пусковой комплекс – корпус В1, 2-й </li>
						<li><a href="http://www.seliger-city.ru/" target="_blank">Селигер Сити</a> по адресу г.Москва, САО, Западное Дегунино, Ильменский проезд, вл.14, К-1,2, корп.В, корп. А.</li>
						<li><a href="http://ecovidnoe2.ru/" target="_blank">Эко Видное 2.0</a> по адресу Московская область, Ленинский район, восточнее д. Ермолино, 1-й этап строительства, корпус 1, 2-й этап строительства, корпус 2</li>
						<li><a href="http://cvet32.ru/" target="_blank">Цвет 32</a> по адресу г. Москва, ЦАО, район Мещанский, Цветной бульвар, владение 32, строение 4</li>
					</ul>
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">КОРТРОС</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://kortros.ru/projects/moscow/zhk-lyublinsky/" target="_blank">ЖК "Люблинский дом у сквера"</a> по адресу г. Москва, проспект 40 лет Октября, вл.36</li>
					</ul>    
				</div>
            </div>	
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ЛИДЕР-ИНВЕСТ</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://www.domnashodnenskoy.ru/" target="_blank">Дом на Сходненской</a> по адресу: г.Москва, ул. Фабрициуса вл. 18, стр. 1, 2</li>
						<li><a href="http://lidervtushino.ru/" target="_blank">Лидер в Тушино</a> по адресу: г.Москва, бульвар Яна Райниса, вл. 4, корп. 3</li>
						<li><a href="http://domvkuzminkah.ru/" target="_blank">Дом в Кузьминках</a> по адресу: г. Москва, ул. Зеленодольская, вл. 41, корп. 2, стр. 2, корп. 2</li>
						<li><a href="http://domnaabramcevskoy.ru/" target="_blank">Дом на Абрамцевской</a> по адресу: г.Москва, улица Абрамцевская, владение 10</li>
						<li><a href="http://www.vsevolozhskiy5.ru/" target="_blank">Резиденция на Всеволожском</a> по адресу: г.Москва, Всеволожский переулок, вл. 5</li>
						<li><a href="http://domvmnevnikah.ru/" target="_blank">Дом в Мневниках</a> по адресу: г.Москва, улица Демьяна Бедного, владение 15</li>
						<li><a href="http://domvolimpiyskoyderevne.ru/" target="_blank">Дом в Олимпийской деревне</a> по адресу: г.Москва, Мичуринский проспект, Олимпийская деревня, вл. 10, корпус 1</li>
						<li><a href="http://domnaveshnyakovskoy18.ru" target="_blank">Дом на Вешняковской</a> по адресу: г.Москва, улица Вешняковская, владение 18Г</li>
						<li><a href="http://lidernadmitrovskom.ru/" target="_blank">Лидер на Дмитровском - Софьин-дом</a> по адресу: г.Москва, улица Софьи Ковалевской, вл. 20</li>
						<li><a href="http://lidernachertanovskoy.ru/" target="_blank">Лидер на Чертановской - Дом Притяжение</a> по адресу: г.Москва, ул. Чертановская, вл.59</li>
						<li><a href="http://lidervcaricyno.ru/" target="_blank">Лидер в Царицыно</a> по адресу: г.Москва, Кавказский бульвар, владение 27 корпус 2</li>
						<li><a href="http://domnaleninskom154.ru/" target="_blank">Лидер на Ленинском - Консул-дом</a> по адресу: г.Москва, Ленинский проспект, вл.154, корп.2</li>						
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ЛСР</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://luchi.moscow/" target="_blank">«Лучи»</a> по адресу г. Москва, ЗАО, район Солнцево, ул. Производственная, вл. 6, корп.1,2</li>
					</ul>    
				</div>
			</div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ООО «Кутузовское-1»</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li>ЖК «Новый Зеленоград» по адресу Московская обл., Солнечногорский р-н, с.п. Кутузовское, д. Рузино, жилые дома IV.06, IV.07</li>
					</ul>    
				</div>
			</div>			
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ПИК</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://www.pik.ru/vavilova" target="_blank">ЖК «Вавилова 4»</a> по адресу: г.Москва, р-н Донской, ул. Вавилова, вл.4</li>
						<li><a href="https://www.pik.ru/yasen" target="_blank">ЖК «Ясеневая 14»</a> по адресу: г. Москва, ЮАО, Орехово-Борисово Южное, ул. Ясеневая, корп.1</li>
						<li><a href="https://www.pik.ru/polar" target="_blank">ЖК «Полярная 25»</a> по адресу: г.Москва, СВАО, Южное Медведково, ул. Полярная, вл. 25, корп. 1.1, 1.2</li>
						<li><a href="https://www.pik.ru/141" target="_blank">Варшавское шоссе 141</a> по адресу: г.Москва, ЮАО, район Чертаново Южное, Варшавское шоссе, вл.141, корп.2, строение 1,2,3,4,5</li>
						<li><a href="https://www.pik.ru/les" target="_blank">Мещерский лес</a> по адресу: г.Москва, ЗАО, район Солнцево, Боровское шоссе, вл.2-2: корпус 3.1,3.2,4.1,4.2,4.3,4.4; вл.2-1: корп.5.1</li>
						<li><a href="https://www.pik.ru/gp" target="_blank">Грин Парк</a> по адресу: г. Москва, СВАО, Останкинский р-н, ул. Сельскохозяйственная, вл. 35, стр.100, 2,3 этап строительства</li>
						<li><a href="https://www.pik.ru/luga" target="_blank">Бунинские Луга</a> по адресу: г. Москва, НАО, поселение Сосенское, вблизи д. Столбово, уч. № 27, корп. 1.7/1,1.7/2, 1.8, 1.3, корп. 1.2/1, корп. 1.2/2</li>
						<li><a href="https://www.pik.ru/park" target="_blank">Оранж парк</a> по адресу: Московская область, г. Котельники, мкр. Опытное поле, вл.10/2, 2 этап строительства, корпус 3,4,5</li>
						<li><a href="https://www.pik.ru/zhiloi-raion-yaroslavskii" target="_blank">Ярославский</a> по адресу: Московская область, Мытищинский район, г. Мытищи, микрорайон 16, корп.39 (по ГП), Блок 39.1,39.2; корп.42 (по ГП), блок 42.1, 42.2.; корп. 43,44</li>
						<li><a href="https://www.pik.ru/odin" target="_blank">Одинцово-1</a> по адресу: Московская область, Одинцовский муниципальный р-н, г. Одинцово, (бывший военный городок №315), г.п. Одинцово, корпус 1.15,1.16,1.7,1.8,1.9,1.10</li>
						<li><a href="https://www.pik.ru/raion-novokurkino" target="_blank">ЖР Новокуркино</a> по адресу: Московская область, Химкинский район, г. Химки, 6, 7, 8 мкр., корпус 8, 1.1,1.2,1.3,66,67</li>
						<li><a href="https://www.pik.ru/raion-levoberezhnyi" target="_blank">ЖР Левобережный</a> по адресу: Московская область, г. Химки, микрорайон Левобережный, улица Совхозная, корпус 1.1,1.2,1.3,7.1,7.2</li>
						<li><a href="https://www.pik.ru/putilkovo" target="_blank">ЖР Путилково</a> по адресу: Московская область, Красногорский муниципальный район, сельское поселение Отрадненское, д. Путилково, позиция 1,2; корп. 41.1 </li>
						<li><a href="https://www.pik.ru/butovo-park" target="_blank">ЖК «Бутово парк»</a> по адресу: Московская область, Ленинский район, сельское поселение Булатниковское, д. Бутово, корп. 4а</li>
						<li><a href="https://www.pik.ru/bp2" target="_blank">Бутово парк-2</a> по адресу: Московская область, Ленинский район, сельское поселение Булатниковское, д. Дрожжино, уч.2,  позиция 1, 28,28.1,30,31,31.1</li>
						<li><a href="https://www.pik.ru/mkr-vostochnoe-butovo" target="_blank">Восточное Бутово</a> по адресу: Московская область, Ленинский район, Булатниковский с/о, д. Боброво мкр. «Боброво»  (Восточное Бутово),  позиция 17, 18,19,19а,27,30,36</li>
						<li><a href="https://www.pik.ru/pearl-zelenograd" target="_blank">Жемчужина Зеленограда</a> по адресу: г.Москва, ЗелАО, Крюково, д. Андреевка, позиция 8,9,10 корп.1, 10 корп.2, поз.11 корп. 1,2</li>
						<li><a href="https://www.pik.ru/luberecky" target="_blank">Люберецкий</a> по адресу: Московская область, Люберецкий район, г. Люберцы, район Красная горка, мкр.12, позиция 1,3, корпус 4.1, 4.2</li>
						<li><a href="https://www.pik.ru/severnyj" target="_blank">Северный</a> по адресу: г.Москва, СВАО, Северный, пос.Северный, мкр. I-Б.1, I-Б.2, I-Б.З и II, корп. К-1, К-2,К-5,К-6, поз.9,10</li>
						<li><a href="https://www.pik.ru/vanderpark" target="_blank">Vander Park</a> по адресу: г.Москва, р-н Кунцево, Рублевское шоссе, вл. 101, 105 (кв. 20, корп. 28Б, 38)</li>
						<li><a href="https://www.pik.ru/sp" target="_blank">ЖР Саларьево Парк</a> по адресу: г. Москва, НАО, поселение Московский, в районе д.  Саларьево, уч.22/1, корпус 1,2,3,4,5,7 стр.1, 7 стр.2,8,9, уч.22/4 корп.13 стр.1,2,3</li>
						<li><a href="https://www.pik.ru/rk11" target="_blank">Римского-Корсакова 11</a> по адресу: г. Москва, СВАО, район Отрадное, Высоковольтный проезд, корпус 1,2,3,4,5</li>
						<li><a href="https://www.pik.ru/che19/bulks/" target="_blank">Черняховского 19</a> по адресу: г.Москва, САО, ул. Черняховского, вл. 19, корпус 1,2,3</li>						
						<li><a href="https://www.pik.ru/il-luga" target="_blank">Ильинские Луга</a> по адресу: Московская область, Красногорский Муниципальный район, Сельское поселение  Ильинское, вблизи поселка «Ильинское-Усово» поз. 36,37,38,39,40</li>
						<li><a href="https://www.pik.ru/petr" target="_blank">Петр 1</a> по адресу: г. Москва, ЮВАО, ул. Красноказарменная, вл. 14А,  корпус № 1,3</li>
						<li><a href="https://www.pik.ru/lp" target="_blank">Лефортово Парк</a> по адресу: г. Москва, ЮВАО, ул. Красноказарменная, вл. 14А, корпус  № 8,9,10</li>
						<li><a href="https://www.pik.ru/vlublino" target="_blank">Влюблино</a> по адресу: г. Москва, ЮВАО, район Люблино, ул. Цимлянская, вл. 3, позиция К-1, К-2 (стр. 2.1 и 2.2)</li>
						<li><a href="https://www.pik.ru/izum" target="_blank">Щитниково Изумрудный</a> по адресу: Московская область, г. Балашиха, мкр. на 19 км Щелковского шоссе, квартал Б,  Блок 1, Блок 2, позиция 25</li>
						<li><a href="https://www.pik.ru/mitino" target="_blank">Митино Парк</a> по адресу: Московская область, Красногорский район, г. Красногорск, коммунальная зона «Красногорск-Митино»,  корпус 1,2</li>
						<li><a href="https://www.pik.ru/i-les" target="_blank">Измайловский лес</a> по адресу: Московская область, городской округ Балашиха, 16 км автодороги М7 «Волга», 1 этап, корпус 1.1,1.2,2.1,2.4</li>
						<li><a href="https://www.pik.ru/apavlova" target="_blank">Академика Павлова</a> по адресу: г. Москва, р-н Кунцево, ул. Ак. Павлова, д. 28,30,32,34, кв. 7 (на месте сноса жилых домов)</li>
						<li><a href="https://www.pik.ru/spolyany" target="_blank">Столичные Поляны</a> по адресу: г. Москва, ул. Поляны, пересечение с ул. Скобелевская, корп.1,2,3</li>
						<li><a href="https://www.pik.ru/bp2" target="_blank">Бутово парк 2</a> по адресу: Московская область, Ленинский муниципальный район, сельское поселение Булатниковское, д. Дрожжино, жилой комплекс «Дрожжино-2», жилой дом поз. 23-25</li>						
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ПИОНЕР</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://botsad.life/" target="_blank">Жилой квартал LIFE Ботанический сад</a> по адресу г.Москва, СВАО, Свиблово, Лазоревый проезд, вл.3, стр.2,3, К8, К9</li>
						<li><a href="http://kutuzovsky.life/" target="_blank">Жилой квартал LIFE Кутузовский</a> по адресу г.Москва. ЗАО, раон Можайский, ул. Гжатская, вл.9, 1 этап строительства, корп. 1,2,3,4; 2 этап строительства, корп.7</li>
						<!--li><a href="http://kutuzovsky.life/" target="_blank">Жилой квартал LIFE Кутузовский</a> по адресу г.Москва, ЗАО, р-н Можайский, ул. Гжатская, вл.9, 2 этап строительства, корп.7</li-->
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">СТОУНХЕДЖ</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://storydom.ru/" target="_blank">Клубный дом STORY</a> по адресу г.Москва, ЮАО, 3-й Автозаводский пр-д, вл.13</li>
					</ul>    
				</div>
            </div>			
			<!--div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">НДВ-Недвижимость</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://novostroyki.ndv.ru/baza/23052-about/" target="_blank">ЖК «Мытищи Lite»</a> по адресу Московская область, Мытищинский район, городское поселение Мытищи, юго-западнее деревни Болтино, корп. 1, 9, 12, застройщик ООО «СФД»</li>
						<li><a href="http://novostroyki.ndv.ru/baza/6746-about/" target="_blank">ЖК «Ленинградский»</a> по адресу Московская область, г. Химки, ул. 9 мая, мкр. 8, дома №№ 1 – 3, застройщик ООО «Агиасма»</li>
					</ul>    
				</div>
            </div-->

	




			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ФСК «Лидер»</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://fsk-lider.ru/offers/skandinavskiy/" target="_blank">UP-квартал «Скандинавский»</a> по адресу Московская область, г/о Мытищи, д. Бородино, жилые дома №2,3,4</li>
						<li><a href="https://fsk-lider.ru/offers/novogireevskiy/" target="_blank">ЖК «Новогиреевский»</a> по адресу Московская область, г. Балашиха, Западная коммунальная зона, ш. Энтузиастов, корп. 1, корп. 2</li>
						<li><a href="https://fsk-lider.ru/offers/pokolenie/" target="_blank">ЖК «Поколение»</a> по адресу г. Москва, СВАО, Сигнальный пр., вл. 5, жилые дома корп. 2,3</li>
						<li><a href="https://fsk-lider.ru/offers/novoe-tushino/" target="_blank">UP-квартал «Новое Тушино»</a> по адресу Московская область, Красногорский район, деревня Путилково UP-квартал «Новое Тушино», дом №1</li>
						<li><a href="https://fsk-lider.ru/offers/skolkovsky/" target="_blank">UP-квартал «Сколковский»</a> по адресу Московская область, Одинцовский район, г. Одинцово ул. Чистяковой, жилые дома корп.7,10,11,12</li>
					</ul>    
				</div>
            </div>



			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Частная компания с ограниченной ответственностью «C&amp;T ИНВЕСТМЕНТС ЛИМИТЕД»</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://snegiri-eco.ru/?utm_source=yandex&utm_medium=cpc&utm_campaign=brend_poisk&yclid=894234961300437897" target="_blank">ЖК «Снегири ЭКО»</a> по адресу город Москва, ул. Минская, корп.1-26, район Раменки (ЗАО гор. Москвы) 2-я очереди, корп.1-12</li>
					</ul>    
				</div>
            </div>			
        <!--/div-->
		   <h2>г. Санкт-Петербург</h2>
		<!--div class="faq"-->
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ПИК</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://www.pik.ru/spb/dalnevostochny" target="_blank">ЖК «Дальневосточный»</a> по адресу: г. Санкт-Петербург, Дальневосточный проспект, в районе дома 15, литера А, участок 1, 1А, 2А, 3А </li>
					</ul>    
				</div>
            </div>
        <!--/div-->			
		<h2>г. Кемерово</h2>
        <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ИФК "Мера"</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://i-mera.ru/projects/southern/" target="_blank">ЖК "ЮЖНЫЙ"</a> по адресу: г. Кемерово, ул. Дружбы в Заводском районе, д. 3,4</li>
					</ul>    
				</div>
            </div>
		   <h2>Ростовская область</h2>
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ПИК</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="https://www.pik.ru/rostov-na-donu/zhk-nord" target="_blank">ЖК «НОРД»</a> по адресу: Ростовская область, г. Ростов-на-Дону, Ворошиловский район, ул. Орбитальная, дом № 3, дом № 4, дом №6</li>
						<li><a href="https://www.pik.ru/rostov-na-donu/zhk-nord" target="_blank">ЖК «НОРД»</a> по адресу: Ростовская область, Аксайский район, п. Верхнетемерницкий, строительный квартал 1, дом № 12</li>
						<li><a href="https://www.pik.ru/rostov-na-donu/dol" target="_blank">ЖК «Доломановский»</a> по адресу: Ростовская область, г. Ростов-на-Дону, Ленинский р-н, пер. Доломановский, 15А, 17, 19, 21, 23, 25, 27, 27А</li>						
					</ul>    
				</div>
            </div>
		<h2>г. Калининград</h2>
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Мегаполис</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://www.megapolis-rielt.ru/ob-ekty/1-zhiloj-rajon-vostok/10-dom-17" target="_blank">Восток</a> по адресу: Калининградская область, город Калининград, улица Асакова - дорога Окружная, жилой дом №17</li>
					</ul>    
				</div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>