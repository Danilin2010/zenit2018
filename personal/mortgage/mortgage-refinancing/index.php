<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("use_page_frame", "Y");
$APPLICATION->SetPageProperty("html_text_for_caps", "<p>Программа для тех, кто хочет рефинансировать ипотечный кредит</p><ul class=\"big_list\"><li>До 25 млн рублей на срок до 30 лет</li><li>От 9,6% годовых</li><ul>");
$APPLICATION->SetPageProperty("title", "Рефинансирование ипотеки");
$APPLICATION->SetPageProperty("keywords", "Рефинансирование ипотеки");
$APPLICATION->SetPageProperty("description", "Рефинансирование ипотеки");
$APPLICATION->SetTitle("Рефинансирование ипотеки");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
?>
<div class="wr_block_type">
<!--content_tab-->
<div class="content_rates_tabs">
	<ul class="content_tab c-container">
		<li><a href="#content-tabs-2">Условия и требования</a></li>
		<li><a href="#content-tabs-4">Погашение</a></li>
		<li><a href="#content-tabs-5">Документы</a></li>
		<li><a href="#content-tabs-3">Страхование</a></li>
		<li><a href="#content-tabs-6">FAQ</a></li>
	</ul>
	<div class="content_body" id="content-tabs-2">
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
					<div class="form_application_line">
						<div class="right_top_line">
							<div class="block_top_line">
							</div>
						</div>
					</div>
					<div class="form_application_line">
						<div class="note_text">Рефинансируйте свой ипотечный кредит на привлекательных условиях – и платите меньше!</div>
 <a href="#" class="button mt-3" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
					</div>
				</div>
				<div class="block_type_center">
					 <!--conditions-->
					<div class="conditions">
						<div class="conditions_item">
							<div class="conditions_item_title">
								 до 25 млн.
							</div>
							<div class="conditions_item_text">
								 размер кредита
							</div>
						</div>
						<div class="conditions_item">
							<div class="conditions_item_title">
								 до 30 лет
							</div>
							<div class="conditions_item_text">
								 срок
							</div>
						</div>
						<div class="conditions_item">
							<div class="conditions_item_title">
								 от 9.6%
							</div>
							<div class="conditions_item_text">
								 ставка
							</div>
						</div>
					</div>
					 <!--conditions-->
					<div class="text_block">
						<h2>Условия кредитования</h2>
						<ul>
						</ul>
						<ul>
							<li><b>Кредит на рефинансирование ипотечного кредита стороннего банка:</b> Ваша возможность получить более привлекательные условия кредитования </li>
							<li><b>Срок рассмотрения заявки – не более пяти рабочих дней:</b> долго ждать не придётся! </li>
							<li><b>Процентная ставка – от 9,6% годовых:</b> рефинансирование кредита поможет сократить расходы </li>
							<li><b>Сумма кредита – до 25 млн рублей</b>
							<ul>
								<li>От 600 тысяч до 25 млн рублей для Московского региона </li>
								<li>От 300 тысяч до 15 млн рублей для других регионов </li>
							</ul>
 </li>
							<li><b>Срок кредита – от года до 30 лет (кратно 12-ти месяцам)</b> </li>
							<li><b>Досрочное погашение без моратория и комиссии:</b> Вы можете погасить кредит в любое время </li>
						</ul>
						<p>
 <br>
 <b>Требования к рефинансируемому кредиту</b>
						</p>
						<ul>
							<li>Ипотечный кредит на: </li>
							<ul>
								<li> приобретение недвижимости на вторичном рынке жилья (в том числе на осуществление неотделимых улучшений) или на первичном рынке жилья (при условии регистрации права собственности на недвижимость на момент рефинансирования); </li>
								<li>рефинансирование кредита, ранее предоставленного на приобретение недвижимости на вторичном рынке жилья (в том числе на осуществление неотделимых улучшений) или на первичном рынке жилья (при условии регистрации права собственности на недвижимость на момент рефинансирования). </li>
							</ul>
							<li>Валюта – рубли РФ. </li>
							<li>Срок с даты предоставления кредита – не менее шести месяцев. </li>
							<li>Срок до полного погашения кредита – не менее 36 месяцев. </li>
							<li>Отсутствие просрочек длительностью более 30 дней каждая за весь срок действия кредита. </li>
							<li>Отсутствие просрочек за последние шесть месяцев погашения. </li>
							<li>Отсутствие текущей просрочки. </li>
							<li>Отсутствие фактов реструктуризации (за исключением реструктуризации в виде снижения процентной ставки по рефинансируемому кредиту). </li>
						</ul>
						<p>
 <b>Не подлежат рефинансированию</b> кредиты, выданные Банком ЗЕНИТ и другими банками, входящими в Банковскую группу ЗЕНИТ, небанковскими организациями и физическими лицами.
						</p>
						<p>
 <b>Вы можете получить кредит, если:</b>
						</p>
						<ul>
							<li>У Вас есть гражданство РФ </li>
							<li>Вы зарегистрированы по месту жительства или пребывания в регионе присутствия Банка </li>
							<li>Вы работаете на территории РФ </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content_body" id="content-tabs-3">
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
				</div>
				<div class="block_type_center">
					<div class="text_block">
						<p>
							 Обязательным является только страхование риска утраты и повреждения приобретённой квартиры.
						</p>
						<p>
							 Хотите <b>снизить Вашу процентную ставку на 1,5 процентных пункта?</b> Оформите страхование жизни и риска потери трудоспособности заёмщика/ созаёмщика/ поручителей с учитываемым доходом и страхование риска утраты права собственности в течение первых трёх лет с даты оформления права собственности.&nbsp;
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content_body" id="content-tabs-4">
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
				</div>
				<div class="block_type_center">
					<div class="text_block">
						<p>
							 Погашение кредита осуществляется ежемесячно <b>равными (аннуитетными) платежами</b>.
						</p>
						<p>
							 В любой момент Вы можете погасить кредит <b>досрочно без штрафов и комиссий</b>. Полное досрочное погашение возможно в любой рабочий день, частичное – в дату внесения планового платежа.
						</p>
						<p>
							 В случае возникновения просроченной задолженности начисляются пени в размере не более 0,06% от её суммы за каждый календарный день просрочки.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content_body" id="content-tabs-5">
		<div class="wr_block_type white">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
				</div>
				<div class="block_type_center">
					<h2>Для получения ипотечного кредита необходимы следующие документы: </h2>
					<ul>
						<li>Паспорт РФ;</li>
						<li>Cправка о доходах по форме 2-НДФЛ или по форме Банка;</li>
						<li>Копия трудовой книжки, заверенная работодателем;</li>
						<li>копия свидетельства о заключении брака/ копия брачного договора/ копия свидетельства о расторжении брака (при наличии);</li>
						<li>Копия военного билета (для мужчин до 27 лет);</li>
						<!--li>Копии документов об образовании;</li>
						<li>Информация о компании-работодателе (по форме Банка).</li-->
					</ul>
					<p>
						 Дополнительно Вы можете предоставить:
					</p>
					<ul>
						<li>Загранпаспорт;</li>
						<li>Водительское удостоверение;</li>
						<li>СНИЛС;</li>
						<li>ИНН.</li>
					</ul>
					<p>
						 Документы по рефинансируемому кредиту:
					</p>
					<ul>
						<li>кредитная документация: кредитный договор/ индивидуальные условия договора и график платежей, а также (при наличии) договор ипотеки, дополнительные соглашения к кредитному договору/ договору ипотеки, текущий график платежей после досрочного погашения;</li>
						<li>договор купли-продажи недвижимости с актом приёма-передачи и документами, подтверждающими оплату по договору купли-продажи.</li>
					</ul>
					<p>
						 Банк оставляет за собой право после принятия решения о кредитовании дополнительно запросить у заёмщика сведения о рефинансируемом кредите/ документы на недвижимость.
					</p>
					<p>
						 Для рассмотрения заявки необходимо заполнить <a href="/upload/iblock/0d9/anket_loan.pdf" target="_blank">заявление-анкету </a> на получение кредита.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="content_body" id="content-tabs-6">
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
				</div>
				<div class="block_type_center">
					<h1>Часто задаваемые вопросы </h1>
					<div class="text_block">
						 <!--faq-->
						<div class="faq">
							<div class="faq_item">
								<div class="faq_top">
									<div class="faq_pict">
										<div class="faq_arr">
										</div>
									</div>
									<div class="faq_top_text">
										 Если меня интересует ипотека в Банке ЗЕНИТ, куда я могу подъехать, чтобы проконсультироваться со специалистом?
									</div>
								</div>
								<div class="faq_text">
									 Для получения консультации, проведения предварительных расчетов Вы можете обратиться в любой офис Банка и/или в круглосуточную Службу дистанционного обслуживания Банка по телефонам: (495) 967-1111 – для Москвы и Московской области, 8-800-500-66-77 – для всех регионов России (звонок по России бесплатный).
								</div>
							</div>
							<div class="faq_item">
								<div class="faq_top">
									<div class="faq_pict">
										<div class="faq_arr">
										</div>
									</div>
									<div class="faq_top_text">
										 Как я могу подать заявку на получение ипотечного кредита?
									</div>
								</div>
								<div class="faq_text">
									 Чтобы подать заявку в Банк на получение ипотечного кредита, необходимо подготовить пакет требуемых Банком документов в соответствии со&nbsp;списком&nbsp;и обратиться в один из офисов Банка.<br>
									 Список документов размещен на странице сайта:&nbsp;<a href="/personal/mortgage/documents-and-supporting-information/">Документы и дополнительная информация</a>
								</div>
							</div>
							<div class="faq_item">
								<div class="faq_top">
									<div class="faq_pict">
										<div class="faq_arr">
										</div>
									</div>
									<div class="faq_top_text">
										 Могу ли я передать в Банк заявку и документы на получение ипотечного кредита с курьером или обязательно мое личное присутствие?
									</div>
								</div>
								<div class="faq_text">
									 Документы подаются в Банк заемщиками лично, либо третьими лицами при наличии нотариально удостоверенной доверенности. В таком случае, Заявление-анкета подписывается доверенным лицом с указанием реквизитов доверенности. Одновременно с копиями документов должны быть предоставлены и оригиналы этих документов для сверки специалистом Банка.
								</div>
							</div>
							<div class="faq_item">
								<div class="faq_top">
									<div class="faq_pict">
										<div class="faq_arr">
										</div>
									</div>
									<div class="faq_top_text">
										 Как я узнаю об одобрении ипотечного кредита?
									</div>
								</div>
								<div class="faq_text">
									 По результатам принятия Банком решения с Вами свяжется специалист по указанным в анкете контактам. В случае положительного решения Банка о кредитовании, Вам на электронную почту, которую Вы указали в Заявлении-анкете, придет Уведомление о параметрах одобренного кредита.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

     <?$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "universal",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "result_edit.php",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "result_list.php",
        "RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
        "SEF_MODE" => "N",
        "SOURCE_TREATMENT" => "Рефинансирование ипотеки",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "Y",
        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
        "WEB_FORM_ID" => "1"
    )
);?>


    <!--content_tab-->
    <div class="block_type to_column">
        <!--banners-->
        <div class="banners c-container">
	<div class="wr_banners_item">
		<div class="banners_item">
			<div class="banners_title">
				 Вклад "ЗЕНИТ Плюс"
			</div>
			<div class="banners_text">
				 До 9% годовых + <br/>
                суперкарта в подарок!
			</div>
			<div class="banners_button">
 <a href="/personal/deposits/the-contribution-of-zenit-plyus/" class="button min_height">подробнее</a>
			</div>
		</div>
		<div class="banners_pict" style="background-image: url('/local/templates/bz/img/banners/deposit.png');">
		</div>
	</div>
	<div class="wr_banners_item">
		<div class="banners_item">
			<div class="banners_title">
				 Карта с Cash Back
			</div>
			<div class="banners_text">
				 Возврат 10% суммы, <br>
				потраченной на развлечения!
			</div>
			<div class="banners_button">
				<a href="/personal/cards/credit-card/credit-card-with-cash-back/" class="button min_height">подробнее</a>
			</div>
		</div>
		<div class="banners_pict" style="background-image: url('/local/templates/bz/img/banners/card.png');">
		</div>
	</div>
	<div class="wr_banners_item">
		<div class="banners_item">
			<div class="banners_title">
				 Мобильное приложение
			</div>
			<div class="banners_text">
				 Доступ к счетам и картам 24/7<br>
				всегда под рукой
			</div>
			<div class="banners_button">
				<ul class="mobile_banking_line_icon">
					<li><a href="#" class="mobile_banking_line_icon apple">
					<div>
					</div>
					</a></li>
					<li><a href="#" class="mobile_banking_line_icon android">
					<div>
					</div>
					</a></li>
				</ul>
			</div>
		</div>
		<div class="banners_pict" style="background-image: url('/local/templates/bz/img/banners/003.png');">
		</div>
	</div>
</div>
<br>        <!--banners-->
    </div>


</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>