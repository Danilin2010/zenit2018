<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Семейная ипотека");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/builders.png');
?>
<div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Условия и требования</a></li>
			<li><a href="#content-tabs-2">Погашение</a></li>
			<li><a href="#content-tabs-3">Документы</a></li>
			<li><a href="#content-tabs-4">Страхование</a></li>
			<li><a href="#content-tabs-5">FAQ</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						 <div class="form_application_line">
						<div class="right_top_line">
							<div class="block_top_line">
							</div>
						</div>
					</div>
						<div class="form_application_line">
							<div class="note_text">С программой «Семейная ипотека» Вы можете получить ипотечный кредит всего под 6% годовых.</div>
 <a href="#" class="button mt-3" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
						</div>
					</div>
					<div class="block_type_center">
						<div class="conditions">
							<div class="conditions_item">
								<div class="conditions_item_title">
									<span>до</span> 8 <span>млн.</span>
								</div>
								<div class="conditions_item_text">
									Размер кредита
								</div>
							</div><div class="conditions_item">
								<div class="conditions_item_title">
									<span>от</span> 20%
								</div>
								<div class="conditions_item_text">
									первый взнос
								</div>
							</div><div class="conditions_item">
								<div class="conditions_item_title">
																<span>от</span> 6%
																</div>
								<div class="conditions_item_text">
									ставка
								</div>
							</div>
						</div>
						<div class="text_block">
							<h2>Условия кредитования</h2>
							<p>
								<b>Вы можете получить кредит, если:</b>
							</p>
							<ul class="big_list">
								<li>У Вас есть гражданство РФ</li>
								<li>Вы зарегистрированы по месту жительства или пребывания в регионе присутствия Банка </li>
								<li>Вы работаете на территории РФ</li>
								<li>В период с 1 января 2018 года по 31 декабря 2022 года у Вас родится второй или третий ребёнок</li>
							</ul>
							<h2>Варианты кредитования</h2>
							<!--table cellspacing="0" cellpadding="0" class="non" style="font-weight: normal;">
							<tbody>
							<tr>
								<td style="font-weight: normal;">
									<p>
										 Кредит на покупку жилья
									</p>
								</td>
								<td>
									<p>
										 Кредит на рефинансирование кредита стороннего банка
									</p>
								</td>
							</tr>
							<tr>
								<td style="font-weight: normal;">
									<p>
										 Кредит на приобретение квартиры на первичном или вторичном рынке: выбирайте жильё по душе!
									</p>
									<p>
										 Первоначальный взнос – от 20% стоимости жилья (может быть оплачен полностью или частично за счёт средств материнского капитала): всего 1/5 стоимости – и квартира Ваша!
									</p>
								</td>
								<td>
									<p>
										 Кредит на рефинансирование ипотечного кредита стороннего банка: Ваша возможность получить более привлекательные условия кредитования
									</p>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-weight: normal;">
									<p align="center">
										 Процентная ставка – 6% годовых*
									</p>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-weight: normal;">
									<p align="center">
										 Срок рассмотрения заявки – не более пяти рабочих дней:<br>
										 долго ждать не придётся!
									</p>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-weight: normal;">
									<p align="center">
										 Сумма кредита – до 8 млн рублей:<br>
										 не отказывайте себе в квадратных метрах!
									</p>
									<p>
										 - От 600 тысяч до 8 млн рублей для Москвы, Московской области, Санкт-Петербурга и Ленинградской области
									</p>
									<p>
										 - От 600 тысяч до 3 млн рублей для других регионов
									</p>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-weight: normal;">
									<p align="center">
										 Срок кредита – от года до 30 лет (кратно 12-ти месяцам)
									</p>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="font-weight: normal;">
									<p align="center">
										 Досрочное погашение без моратория и комиссии:<br>
										 Вы можете погасить кредит в любое время
									</p>
								</td>
							</tr>
							<tr>
								<td style="font-weight: normal;">
									<p>
										 &nbsp;
									</p>
								</td>
								<td>
									<p>
										 Требования к рефинансируемому кредиту
									</p>
									<ul>
										<li>
										Ипотечный кредит на приобретение недвижимости на первичном или вторичном рынке жилья. </li>
										<li>
										Валюта – рубли РФ. </li>
										<li>
										Срок с даты предоставления кредита – не менее 12 месяцев. </li>
										<li>
										Срок до полного погашения кредита – не менее 36 месяцев. </li>
										<li>
										Отсутствие просрочек длительностью более 30 дней каждая за весь срок действия кредита. </li>
										<li>
										Отсутствие просрочек за последние шесть месяцев погашения. </li>
										<li>
										Отсутствие текущей просрочки. </li>
										<li>
										Отсутствие фактов реструктуризации (за исключением реструктуризации в виде снижения процентной ставки по рефинансируемому кредиту). </li>
									</ul>
								</td>
							</tr>
							</tbody>
							</table-->
				<div class="faq">
				
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict">
								<div class="faq_arr">
								</div>
							</div>
							<div class="faq_top_text">
								 Кредит на покупку жилья
							</div>
						</div>
						<div class="faq_text">
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Кредит на приобретение квартиры на первичном или вторичном рынке</b>: выбирайте жильё по душе!</p>
											<p><b>Первоначальный взнос – от 20% стоимости жилья</b> (может быть оплачен полностью или частично за счёт средств материнского капитала): всего 1/5 стоимости – и квартира Ваша!</p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Процентная ставка – 6% годовых*</b></p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Срок рассмотрения заявки – не более 5-ти рабочих дней</b>: долго ждать не придётся!</p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Сумма кредита – до 8 млн рублей</b>: не отказывайте себе в квадратных метрах!</p>
											<p>
												 - От 600 тысяч до 8 млн рублей для Москвы, Московской области, Санкт-Петербурга и Ленинградской области
											</p>
											<p>
												 - От 600 тысяч до 3 млн рублей для других регионов
											</p>											 
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Срок кредита – от года до 30 лет (кратно 12-ти месяцам)</b></p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Досрочное погашение без моратория и комиссии</b>: Вы можете погасить кредит в любое время</p>
										</div>
									</div>
								</div>
							</div>
							<br/>
							<p align="justify">
								 *При рождении второго ребёнка ставка в размере 6% годовых действует три года, при рождении третьего ребёнка – пять лет. Если в период действия программы в семье с двумя детьми родится третий ребёнок, действие льготной ставки будет продлено на пять лет. По окончании льготного периода стоимость кредита будет рассчитываться по формуле: ключевая ставка ЦБ РФ на дату предоставления кредита + 2 процентных пункта.
							</p>
						</div>
					</div>	
					
					<div class="faq_item">
						<div class="faq_top">
							<div class="faq_pict">
								<div class="faq_arr">
								</div>
							</div>
							<div class="faq_top_text">
								Рефинансирование кредита стороннего банка
							</div>
						</div>
						<div class="faq_text">
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Кредит на рефинансирование ипотечного кредита стороннего банка</b>: Ваша возможность получить более привлекательные условия кредитования</p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Процентная ставка – 6% годовых*</b></p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Срок рассмотрения заявки – не более пяти рабочих дней</b>: долго ждать не придётся!</p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Сумма кредита – до 8 млн рублей</b>: не отказывайте себе в квадратных метрах!</p>
											<p>
												 - От 600 тысяч до 8 млн рублей для Москвы, Московской области, Санкт-Петербурга и Ленинградской области
											</p>
											<p>
												 - От 600 тысяч до 3 млн рублей для других регионов
											</p>											 
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Срок кредита – от года до 30 лет (кратно 12-ти месяцам)</b></p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Досрочное погашение без моратория и комиссии</b>: Вы можете погасить кредит в любое время</p>
										</div>
									</div>
								</div>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 i
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											<p><b>Требования к рефинансируемому кредиту</b>:</p>
											 <ul class="big_list">
												<li>Ипотечный кредит на приобретение недвижимости на первичном или вторичном рынке жилья.</li>
												<li>Валюта – рубли РФ.</li>
												<li>Срок с даты предоставления кредита – не менее 12 месяцев.</li>
												<li>Срок до полного погашения кредита – не менее 36 месяцев.</li>
												<li>Отсутствие просрочек длительностью более 30 дней каждая за весь срок действия кредита.</li>
												<li>Отсутствие просрочек за последние шесть месяцев погашения.</li>
												<li>Отсутствие текущей просрочки.</li>
												<li>Отсутствие фактов реструктуризации (за исключением реструктуризации в виде снижения процентной ставки по рефинансируемому кредиту).</li>
										</div>
									</div>
								</div>
							</div>
								<br/>
							<p align="justify">
								 *При рождении второго ребёнка ставка в размере 6% годовых действует три года, при рождении третьего ребёнка – пять лет. Если в период действия программы в семье с двумя детьми родится третий ребёнок, действие льготной ставки будет продлено на пять лет. По окончании льготного периода стоимость кредита будет рассчитываться по формуле: ключевая ставка ЦБ РФ на дату предоставления кредита + 2 процентных пункта.
							</p>
						</div>
					</div>
				</div>
					<br/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Погашение кредита осуществляется ежемесячно <b>равными (аннуитетными) платежами</b>.
							</p>
							<p>
								 В любой момент Вы можете погасить кредит <b>досрочно без штрафов и комиссий</b>. Полное досрочное погашение возможно в любой рабочий день, частичное – в дату внесения планового платежа.
							</p>
							<p>
								 В случае возникновения просроченной задолженности начисляются пени в размере не более 0,06% от её суммы за каждый календарный день просрочки.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Для получения ипотечного кредита необходимы следующие документы:
							</p>
							<ul class="big_list">
								<li>паспорт РФ;</li>
								<li>справка о доходах по форме 2-НДФЛ или по форме Банка;</li>
								<li>копия трудовой книжки, заверенная работодателем;</li>
								<li>копия свидетельства о заключении брака/ копия брачного договора/ копия свидетельства о расторжении брака (при наличии);</li>
								<li>копия свидетельства о рождении/ усыновлении ребёнка (детей);</li>
								<li>сертификат на материнский капитал (при использовании средств материнского капитала для внесения первоначального взноса);</li>
								<li>справка из Пенсионного фонда РФ об остатке средств материнского капитала (при использовании средств материнского капитала для внесения первоначального взноса);</li>
							</ul>
							<p>
								 Документы по рефинансируемому кредиту:
							</p>
							<ul class="big_list">
								<li>кредитная документация: кредитный договор/ индивидуальные условия договора и график платежей, а также (при наличии) договор ипотеки, дополнительные соглашения к кредитному договору/ договору ипотеки, текущий график платежей после досрочного погашения;</li>
								<li>договор купли-продажи недвижимости с актом приёма-передачи и документами, подтверждающими оплату по договору купли-продажи.</li>
							</ul>
							<p>
								 Для рассмотрения заявки необходимо заполнить <a href="/upload/iblock/0d9/anket_loan.pdf" target="_blank">заявление-анкету</a> на получение кредита.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Обязательным условием является страхование риска утраты и повреждения приобретаемой (приобретённой) квартиры.
							</p>
							<p>
								 Страхование рисков потери трудоспособности заёмщика/ созаёмщиков/ поручителей с учитываемым доходом и риска утраты права собственности на предмет залога не является обязательным условием предоставления кредита и остаётся на усмотрение заёмщика.
							</p>
							<p>
								 В случае отсутствия страхования жизни и потери трудоспособности заёмщика/ созаёмщиков/ поручителей с учитываемым доходом и/ или риска утраты права собственности на предмет залога процентная ставка по кредиту увеличивается на 5 процентных пунктов в период предоставления субсидии и на 1,5 процентного пункта – по окончании периода предоставления субсидии.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-5">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<h1>Часто задаваемые вопросы </h1>
							<div class="text_block">
								 <!--faq-->
								<div class="faq">
									<div class="faq_item">
										<div class="faq_top">
											<div class="faq_pict">
												<div class="faq_arr">
												</div>
											</div>
											<div class="faq_top_text">
												 Если меня интересует ипотека в Банке ЗЕНИТ, куда я могу подъехать, чтобы проконсультироваться со специалистом?
											</div>
										</div>
										<div class="faq_text">
											 Для получения консультации, проведения предварительных расчетов Вы можете обратиться в любой офис Банка и/или в круглосуточную Службу дистанционного обслуживания Банка по телефонам: (495) 967-1111 – для Москвы и Московской области, 8-800-500-66-77 – для всех регионов России (звонок по России бесплатный).
										</div>
									</div>
									<div class="faq_item">
										<div class="faq_top">
											<div class="faq_pict">
												<div class="faq_arr">
												</div>
											</div>
											<div class="faq_top_text">
												 Как я могу подать заявку на получение ипотечного кредита?
											</div>
										</div>
										<div class="faq_text">
											 Чтобы подать заявку в Банк на получение ипотечного кредита, необходимо подготовить пакет требуемых Банком документов в соответствии со&nbsp;списком&nbsp;и обратиться в один из офисов Банка.<br>
											 Список документов размещен на странице сайта:&nbsp;<a href="/personal/mortgage/documents-and-supporting-information/">Документы и дополнительная информация</a>
										</div>
									</div>
									<div class="faq_item">
										<div class="faq_top">
											<div class="faq_pict">
												<div class="faq_arr">
												</div>
											</div>
											<div class="faq_top_text">
												 Могу ли я передать в Банк заявку и документы на получение ипотечного кредита с курьером или обязательно мое личное присутствие?
											</div>
										</div>
										<div class="faq_text">
											 Документы подаются в Банк заемщиками лично, либо третьими лицами при наличии нотариально удостоверенной доверенности. В таком случае, Заявление-анкета подписывается доверенным лицом с указанием реквизитов доверенности. Одновременно с копиями документов должны быть предоставлены и оригиналы этих документов для сверки специалистом Банка.
										</div>
									</div>
									<div class="faq_item">
										<div class="faq_top">
											<div class="faq_pict">
												<div class="faq_arr">
												</div>
											</div>
											<div class="faq_top_text">
												 Как я узнаю об одобрении ипотечного кредита?
											</div>
										</div>
										<div class="faq_text">
											 По результатам принятия Банком решения с Вами свяжется специалист по указанным в анкете контактам. В случае положительного решения Банка о кредитовании, Вам на электронную почту, которую Вы указали в Заявлении-анкете, придет Уведомление о параметрах одобренного кредита.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"universal",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Семейная ипотека",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>