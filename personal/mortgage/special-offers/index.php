<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Акция «Ипотека по двум документам»");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/sale.png');
?><?
use \Bitrix\Main\Page\Asset;
?>
<div class="container about-page">
	<div class="row">
		 <!-- Правый блок мобилка и планшет -->
		<div class="col-sm-12 col-md-4 hidden-lg">
			<div class="row note_text manager-top">
				<div class="block-card right note_text">
					<h2>Связаться с банком</h2>
					<div class="form_application_line">
						<div class="contacts_block">
 <a href="mailto:email@zenit.ru">info@zenit.ru</a><br>
							 +7 (495) 967-11-11<br>
							 8 (800) 500-66-77
						</div>
						<div class="note_text">
							 звонок по России бесплатный
						</div>
					</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
				</div>
			</div>
		</div>
		 <!-- Содержимое -->
		<div class="col-sm-12 col-mt-0 col-md-8">
			<div class="row  wr_block_type">
				<div class="text_block block-card right note_text" style="color: black;">
					<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mt-3 main-content-top">
<p><strong>Специальные условия ипотечного кредитования физических лиц действуют до 31.12.2018 г.</strong></p><br>
<p>Действие акции по ипотеке, по условиям которой заёмщики с первоначальным взносом не менее 50% стоимости жилья могут получить кредит по двум документам (паспорт РФ и один дополнительный документ), продлено до 31 декабря 2018 года. Процентная ставка по всем базовым программам для таких заёмщиков составляет 9,9% годовых.
	В рамках акции выдаются кредиты на приобретение готового и строящегося жилья, а также загородной недвижимости.</p>
<table  cellspacing="0" cellpadding="0" style="font-size:inherit;
">
<tbody>
<tr>
<td >
<p>Цель</p>
</td>
<td align="left" valign="top">
<p>Приобретение недвижимости по программам:</p>
<ul style="list-style-type: square;">
<li>Программа &laquo;<a title="index" href="/personal/mortgage/the-apartment-on-the-secondary-market/the-apartment-on-the-secondary-market/">Ипотечный кредит на приобретение квартиры на вторичном рынке жилья</a>&raquo;;</li>
<li>Программа &laquo;<a title="index" href="/personal/mortgage/new/new/">Ипотечный кредит на приобретение квартир в домах - новостройках</a>&raquo;;</li>
<li>Программа &laquo;<a title="index" href="/personal/mortgage/house-with-land/house-with-land/" >Ипотечный кредит на приобретение земельного участка с жилым домом</a>&raquo;;</li>
<li>Программа &laquo;<a title="index" href="/personal/mortgage/the-apartment-on-the-secondary-market/room-in-the-secondary-market/">Ипотечный кредит на приобретение комнаты</a>&raquo;.</li>
</ul>
</td>
</tr>
<tr>
<td>
<p>Валюта кредита</p>
</td>
<td align="left" valign="top">
<p>Рубли РФ</p>
</td>
</tr>
<tr>
<td>
<p>Комиссия за выдачу кредита</p>
</td>
<td align="left" valign="top">
<p>Не взимается</p>
</td>
</tr>
<tr>
<td class="th">
<p>Срок кредита</p>
</td>
<td align="left" valign="top">
<p>От 1 года до&nbsp;20 лет</p>
</td>
</tr>
<tr>
<td>
<p>Первоначальный взнос</p>
</td>
<td align="left" valign="top">
<p>Не менее <strong>50%</strong> от стоимости приобретаемой недвижимости</p>
</td>
</tr>
<tr>
<td>
<p>Процентная ставка</p>
</td>
<td align="left" valign="top">
<p>9,9%*</p>
</td>
</tr>
<tr>
<td>
<p>Перечень обязательных документов Заемщика/ Созаемщика для рассмотрения кредитной заявки:</p>
</td>
<td align="left" valign="top"><ol>
<li>Паспорт;</li>
<li>Один из следующих дополнительных документов: водительское удостоверение, страховое свидетельство государственного пенсионного страхования, свидетельство о присвоении ИНН, загранпаспорт.<br /><br /><em>Для принятия решения о предоставлении кредита Банк может запросить дополнительные документы</em></li>
</ol></td>
</tr>
</tbody>
</table>
<p><em>* К процентной ставке применяется надбавка 1.5 п.п. при отсутствии дополнительного страхового обеспечения по кредитному продукту.</em></p>
<p><em>К процентной ставке применяется надбавка 1 п.п. при непредоставлении Заемщиком Банку документов, подтверждающих государственную регистрацию права собственности Заемщика на объект долевого строительства и ипотеки объекта долевого строительства в силу закона в пользу Банка, по истечении 12 (Двенадцати) месяцев с установленной Договором участия в долевом строительстве даты передачи Застройщиком Заемщику объекта долевого строительства.</em></p>
<p><strong>Диапазоны значений полной стоимости кредита:&nbsp;11,762&ndash; 12,990% годовых.</strong></p>

					</div>
				</div>
			</div>
		</div>
		 <!-- Правый блок для ПС -->
		<div class="col-md-4 hidden-xs hidden-sm">
			<div class="row">
				<div class="col-sm-12">
					<div class="block-card right note_text">
						<h2>Связаться с банком</h2>
						<div class="form_application_line">
							<div class="contacts_block">
 <a href="mailto:email@zenit.ru">info@zenit.ru</a><br>
								 +7 (495) 967-11-11<br>
								 8 (800) 500-66-77
							</div>
							<div class="note_text">
								 звонок по России бесплатный
							</div>
						</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>