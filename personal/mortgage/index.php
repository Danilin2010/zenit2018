<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Ипотека — новостройки, вторичный рынок, военная ипотека | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"PRODUCTHEADER_IMG" => "/personal/mortgage/img/mortgageCatalogIconBig.png",
		"PRODUCTHEADER_TITLE" => "Ипотека"
	)
);?> 
<div class="tabs-controls" id="tabs-controls-app"> 	 
  <div class="tabs-controls__wrapper"> <nav class="tabs-controls__inner"> 		<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_ACTIVE" => "Y",
		"PRODUCTHEADER_SRC" => "#classic",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_TITLE" => "Классическая"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_SRC" => "#military",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_TITLE" => "Военная"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_SRC" => "#realty",
		"PRODUCTHEADER_STAR" => "Y",
		"PRODUCTHEADER_TARGET" => "Y",
		"PRODUCTHEADER_TITLE" => "Подбор недвижимости"
	)
);?> </nav> 	</div>
 </div>
 
<div class="tab-content tab-content_state-active" id="classic"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "6",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "8",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/family-mortgage/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/family-mortgage/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/family-mortgage/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит для&nbsp;семей с&nbsp;детьми по&nbsp;программе государственного субсидирования ипотеки",
		"SIDECOLUMN_TITLE" => "Семейная ипотека",
		"SIDECOLUMN_TYPE" => "product-card_theme-promo"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,6",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE2_TITLE" => "300",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_FEATURE3_TITLE" => "30",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/mortgage-refinancing/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/mortgage-refinancing/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/mortgage-refinancing/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет улучшить условия кредита&nbsp;&#151; сократить срок, уменьшить платёж или&nbsp;ставку",
		"SIDECOLUMN_TITLE" => "Рефинансирование ипотеки",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "новостроек<br>на&nbsp;выбор",
		"SIDECOLUMN_FEATURE3_TITLE" => "100",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/new/new/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/new/new/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/new/new/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Ипотека на&nbsp;приобретение квартиры в&nbsp;строящемся или&nbsp;готовом доме",
		"SIDECOLUMN_TITLE" => "Квартира в&nbsp;новостройке",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "15",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "дней<br>на заявку",
		"SIDECOLUMN_FEATURE3_TITLE" => "5",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " ",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/the-apartment-on-the-secondary-market/the-apartment-on-the-secondary-market/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/the-apartment-on-the-secondary-market/the-apartment-on-the-secondary-market/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/the-apartment-on-the-secondary-market/the-apartment-on-the-secondary-market/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;квартиру в&nbsp;многоэтажном доме на&nbsp;вторичном рынке ",
		"SIDECOLUMN_TITLE" => "Квартира на&nbsp;вторичном рынке",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "300",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/the-apartment-on-the-secondary-market/room-in-the-secondary-market/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/the-apartment-on-the-secondary-market/room-in-the-secondary-market/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/the-apartment-on-the-secondary-market/room-in-the-secondary-market/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет приобрести одну или&nbsp;несколько комнат в&nbsp;квартире на&nbsp;вторичном рынке",
		"SIDECOLUMN_TITLE" => "Комната на&nbsp;вторичном рынке",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "10,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "30",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "16",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/house-with-land/house-with-land/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/house-with-land/house-with-land/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/house-with-land/house-with-land/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Если вы мечтаете жить за&nbsp;городом в&nbsp;собственном доме с&nbsp;участком земли",
		"SIDECOLUMN_TITLE" => "Дом с&nbsp;землей",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> 					</div>
         				</div>
       			</div>
     			 
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 
            <div class="recommended-cards__title"> 							 Обратите внимание 						</div>
           						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Подбор недвижимости",
		"RECOMMENDED_TEXT" => "Подбор новостроек на&nbsp;карте и&nbsp;интересные спецпредложения на выгодных условиях",
		"RECOMMENDED_LINK" => "/personal/mortgage/?start-tab=realty",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Сотрудникам Татнефть",
		"RECOMMENDED_TEXT" => "Ипотечные программы и&nbsp;потребительские кредиты на&nbsp;специальных условиях",
		"RECOMMENDED_LINK" => "/personal/tatneft/#mortgage",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Справочная информация",
		"RECOMMENDED_TEXT" => "Требования, условия, документы и&nbsp;информация по&nbsp;обслуживанию ипотеки",
		"RECOMMENDED_LINK" => "/personal/mortgage/documents-and-supporting-information/",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/information.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Налоговый вычет",
		"RECOMMENDED_TEXT" => "Возможность получить налоговый вычет при покупке жилой недвижимости",
		"RECOMMENDED_LINK" => "/personal/tax-deduction/",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<div class="tab-content" id="military"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "новостроек<br>на выбор",
		"SIDECOLUMN_FEATURE3_TITLE" => "100",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/military-new/military-mortgage-new8079/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/military-new/military-mortgage-new8079/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/military-new/military-mortgage-new8079/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Ипотека на&nbsp;приобретение квартиры в&nbsp;строящемся или&nbsp;готовом доме",
		"SIDECOLUMN_TITLE" => "Квартира в&nbsp;новостройке для&nbsp;военнослужащего",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "6",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/military-new/military-mortgage-new-family/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/military-new/military-mortgage-new-family/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/military-new/military-mortgage-new-family/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Ипотека для военнослужащих, являющихся супругами",
		"SIDECOLUMN_TITLE" => "Квартира в новостройке для&nbsp;супругов",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "3",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/military-secondary/military-mortgage-secondary/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать зявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/military-secondary/military-mortgage-secondary/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/military-secondary/military-mortgage-secondary/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;квартиру в&nbsp;многоэтажном доме на&nbsp;вторичном рынке ",
		"SIDECOLUMN_TITLE" => "Квартира на&nbsp;вторичном рынке для&nbsp;военнослужащего",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "6",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/military-secondary/military-mortgage-secondary-family/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/military-secondary/military-mortgage-secondary-family/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/military-secondary/military-mortgage-secondary-family/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;квартиру в&nbsp;многоэтажном доме на&nbsp;вторичном рынке для супругов",
		"SIDECOLUMN_TITLE" => "Квартира на&nbsp;вторичном рынке для&nbsp;супругов",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE2_TITLE" => "300",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "до достижения<br>возраста",
		"SIDECOLUMN_FEATURE3_TITLE" => "50",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/refinance-military-mortgage/refinance-military-mortgage/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/refinance-military-mortgage/refinance-military-mortgage/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/refinance-military-mortgage/refinance-military-mortgage/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет улучшить условия кредита&nbsp;&#151; сократить срок, уменьшить платёж или&nbsp;ставку",
		"SIDECOLUMN_TITLE" => "Рефинансирование ипотеки",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> 
<!--?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "20",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "2,41",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/military-new/military-mortgage-aijk/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/military-new/military-mortgage-aijk/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/military-new/military-mortgage-aijk/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;новостройку или&nbsp;квартиру на&nbsp;вторичном рынке совместно с&nbsp;АИЖК",
		"SIDECOLUMN_TITLE" => "Ипотека АИЖК",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?-->
 					</div>
         				</div>
       			</div>
     			 
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 
            <div class="recommended-cards__title"> 							 Обратите внимание 						</div>
           						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Подбор недвижимости",
		"RECOMMENDED_TEXT" => "Подбор новостроек на&nbsp;карте и&nbsp;интересные спецпредложения на выгодных условиях",
		"RECOMMENDED_LINK" => "/personal/mortgage/?start-tab=realty",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/realty.svg"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Справочная информация",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/information.svg",
		"RECOMMENDED_LINK" => "/personal/mortgage/documents-and-supporting-information/",
		"RECOMMENDED_TEXT" => "Требования, условия, документы и&nbsp;информация по&nbsp;обслуживанию ипотеки",
		"RECOMMENDED_TITLE" => "Справочная информация"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<div class="tab-content" id="realty"> 	 
<!-- Фон подложки таба-->
 	 
  <div class="tab-content__theme tab-content__theme_grey"> 		 
    <div class="tab-content__wrapper"> 			 
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				 
        <div class="col col_size-12"> 					 
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Карта новостроек",
		"SIDECOLUMN_TEXT" => "Воспользуйтесь картой для&nbsp;поиска новостроек, в&nbsp;которых доступна ипотека Банка Зенит",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-promo",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/realty/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Открыть карту",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_LINK_CART" => "/realty/"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE1_TITLE" => "0",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_TITLE" => "10",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "8",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/stock/sampo_action/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/stock/sampo_action/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/stock/sampo_action/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;квартиру в&nbsp;современном жилом комплексе в&nbsp;престижном районе Подмосковья на&nbsp;Новорижском шоссе",
		"SIDECOLUMN_TITLE" => "Квартиры в&nbsp;ЖК&nbsp;SAMPO",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE1_TITLE" => "20",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_TITLE" => "9,5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "3",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/military-new/military-mortgage-sampo/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/military-new/military-mortgage-sampo/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/military-new/military-mortgage-sampo/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;квартиру в&nbsp;современном жилом комплексе в&nbsp;престижном районе Подмосковья на&nbsp;Новорижском шоссе",
		"SIDECOLUMN_TITLE" => "Квартиры в&nbsp;ЖК&nbsp;SAMPO для&nbsp;военнослужащих",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Дом в&nbsp;поселке Примавера",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;приобретение домов и&nbsp;таунхаусов с&nbsp;земельными участками в&nbsp;коттеджном посёлке Примавера",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "10",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "10,5",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "25",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/mortgage/house-with-land/property-in-kp-primavera/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/mortgage/house-with-land/property-in-kp-primavera/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/mortgage/house-with-land/property-in-kp-primavera/"
	)
);?> 					</div>
         				</div>
       			</div>
     		 
      <div class="tab-content__inner"> 				 
        <div class="col col_size-12"> 					 
          <div class="recommended-cards"> 						 
            <div class="recommended-cards__title"> 							 Вас может заинтересовать</div>
           						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Недвижимость банка",
		"RECOMMENDED_TEXT" => "Кредит на объекты недвижимости, которые находятся в&nbsp;собствености банка",
		"RECOMMENDED_LINK" => "/personal/credits/carloans/?start-tab=salons",
		"RECOMMENDED_IMG" => "/personal/credits/carloans/img/information.svg"
	)
);?> 					</div>
         				</div>
       			</div>
     </div>
   	</div>
 </div>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>