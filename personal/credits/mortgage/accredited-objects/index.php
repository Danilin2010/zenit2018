<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Аккредитованные объекты");
?><div class="block_type_center">
    <div class="text_block">
		<div class="faq">
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Группа компаний Гранель</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li>ЖК «Театральный парк» по адресу Московская область, г. Королев, мкр. Болшево, ул. Полевая, д. 9, поз. 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 27, 28; ул.Спартаковская, д.1А, корпус 4</li>
						<li>ЖК «Государев дом» по адресу Московская область, Ленинский район, сельское поселение Булатниковское, вблизи д. Лопатино поз. 1, 2, 3, 4, 5, 6, 11, 12, 14, 23, 24, 25, 26, 28, 29</li>
						<!--li>ЖК «Императорские Мытищи» по адресу: Московская область, Мытищинский район, городское поселение Мытищи, восточнее д. Погорелки дом № 14, 16, 19, 20</li-->
					</ul>    
				</div>
            </div>
			<!--div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Группа Компаний ПИК</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li>Московская область, г.о. Балашиха, 28-й км. автомагистрали М7 «Волга», северо-восточная часть квартала «Новский», квартал «А», поз. 1, 2, 4, 5</li>
						<li>г. Москва, ЗелАО, Крюково, д. Андреевка, поз. 1, 2, 3, 8, 9</li>
						<li>Московская область, Красногорский муниципальный район, с/п Ильинское, вблизи поселка «Ильинское-Усово», корпус 36, корпус 37, корпус 38, корпус 39, корпус 40</li>
						<li>г. Москва, СВАО, Северный, пос. Северный, микрорайон I-Б.1, I-Б.2, I-Б.3 и II, корпуса К-1,К-5, К-6, К-7, К-8</li>
						<li>г. Москва, ул. Красноказарменная, вл.14А, жилой корпус №1, жилой корпус №3 (ЖК «Петр I»)</li>
						<li>г. Москва, ЮВАО, район Люблино, ул. Цимлянская, вл. 3, поз. К-1</li>
						<li>Московская область, Люберецкий район, г. Люберцы, мкр. 12 «Красная горка», корпус 1, 2, 2а, 10, 11</li>
						<li>Московская область, Ленинский муниципальный район, сельское поселение Булатниковское, вблизи д. Бутово, позиция 15 (микрорайон «Бутово Парк»), позиция 22</li>
						<li>Московская область, Ленинский район, Булатниковское сельское поселение, д. Боброво, мкр-н МортонГрад "Боброво" поз.22, 23, 24, 25</li>
						<li>Московская область, Ленинский муниципальный район, сельское поселение Булатниковское, вблизи д. Дрожжино, позиция 4, 5, 7, 8, 8.1, 11, 13, 14.1, 15, 15.1, 15.2, 17 (микрорайон «Дрожжино 2»), уч.-21-ю, поз. 12.2, 32, 33, 34</li>
						<li>г. Москва, г. Щербинка, ул. Овражная, дом 3, 4, 5, 6, 7</li>
					</ul>    
				</div>
            </div-->
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Компания MR Group</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://filigrad.ru" target="_blank">Жилой комплекс «Фили Град»</a></li>
						<li><a href="http://vodny2.ru" target="_blank">Многофункциональный комплекс «Водный»</a></li>
						<li><a href="http://vorobiev-dom.ru" target="_blank">Многофункциональный жилой комплекс «Воробьев дом»</a></li>
						<li><a href="http://yasny-dom.ru/" target="_blank">ЖК Ясный</a></li>
					</ul>
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Группа компаний ОПИН</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://vesnadom.ru/" target="_blank">ЖК «VESNA»</a> по адресу Московская область, Наро-Фоминский р-н, городское поселение Апрелевка, микрорайон «Мартемьяново-7», корпуса №№ 5 – 8, застройщик ООО "Мартемьяново"</li>
						<!--li><a href="http://pavkvartal.ru/" target="_blank">ЖК «Павловский квартал»</a> по адресу Московская область, Истринский район, сельское поселение Павло-Слободское, д. Лобаново, ул. Новая, дома №№ 9 – 20, застройщик ООО "Павловский квартал"</li-->
					</ul>    
				</div>
            </div>
			<!--div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">НДВ-Недвижимость</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
						<li><a href="http://novostroyki.ndv.ru/baza/23052-about/" target="_blank">ЖК «Мытищи Lite»</a> по адресу Московская область, Мытищинский район, городское поселение Мытищи, юго-западнее деревни Болтино, корп. 1, 9, 12, застройщик ООО «СФД»</li>
						<li><a href="http://novostroyki.ndv.ru/baza/6746-about/" target="_blank">ЖК «Ленинградский»</a> по адресу Московская область, г. Химки, ул. 9 мая, мкр. 8, дома №№ 1 – 3, застройщик ООО «Агиасма»</li>
					</ul>    
				</div>
            </div-->
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">БЕСТ-Новострой</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
                    <ul class="big_list">
						<li><a href="http://best-novostroy.ru/realty/novostroyki_moscow/szao/zhk_mir_mitino/" target="_blank">ЖК "Мир Митино"</a> по адресу г. Москва, внутригородское муниципальное образование Митино, вблизи с. Рождествено, корп.7,8,9,10,10.1 </li>
					</ul>    
				</div>
            </div>	
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Est-a-Tet</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
                    <ul class="big_list">
						<li><a href="https://www.estatet.ru/catalog_new/zhk_mir_mitino/descr/" target="_blank">ЖК "Мир Митино"</a> по адресу г. Москва, внутригородское муниципальное образование Митино, вблизи с. Рождествено, корп.7,8,9,10,10.1</li>
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">КОРТРОС</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
                    <ul class="big_list">
						<li><a href="https://kortros.ru/projects/moscow/zhk-lyublinsky/" target="_blank">ЖК "Люблинский дом у сквера"</a> по адресу г. Москва, проспект 40 лет Октября, вл.36</li>
					</ul>    
				</div>
            </div>
			<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">ГРУППА МГ</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ul class="big_list">
                    <ul class="big_list">
						<li><a href="http://pirogovo-dialekt.ru/" target="_blank">ЖК "Диалект"</a> по адресу Московская область, Мытищинский район, рабочий поселок Пироговский</li>
					</ul>    
				</div>
            </div>			
        </div>
    </div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>