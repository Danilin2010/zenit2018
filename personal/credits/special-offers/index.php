<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Специальные предложения");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/sale.png');
?><?
use \Bitrix\Main\Page\Asset;
?>
<?/*<div class="container about-page">
	<div class="row">
		 <!-- Правый блок мобилка и планшет -->
		<div class="col-sm-12 col-md-4 hidden-lg">
			<div class="row note_text manager-top">
				<div class="block-card right note_text">
					<h2>Связаться с банком</h2>
					<div class="form_application_line">
						<div class="contacts_block">
 <a href="mailto:email@zenit.ru">info@zenit.ru</a><br>
							 +7 (495) 967-11-11<br>
							 8 (800) 500-66-77
						</div>
						<div class="note_text">
							 звонок по России бесплатный
						</div>
					</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
				</div>
			</div>
		</div>
		 <!-- Содержимое -->
		<div class="col-sm-12 col-mt-0 col-md-8">
			<div class="row">
				<div class="col-sm-12 col-mb-pr-0 col-mt-pr-0 pr-4 mt-3 main-content-top">
					<div class="text_block block-card right note_text" style="color: black; font-size: large;">
						<p>
							 В октябре Банк ЗЕНИТ стал участником государственной программы субсидирования автокредитов и запустил программу льготного автокредитования.<br>
							 Теперь наши заёмщики могут получить автокредит с пониженной процентной ставкой <b>9%</b> годовых (при наличии полиса КАСКО).&nbsp;
						</p>
						<p>
							 Для получения кредита на приобретение автомобилей всех марок (кроме ГАЗ и коммерческих автомобилей) не требуется первоначальный взнос. При приобретении автомобилей марки ГАЗ и коммерческих автомобилей других марок предусмотрен первоначальный взнос в размере <b>30%</b> от стоимости транспортного средства.&nbsp;
						</p>
						<ul>
							<li>
							<p>
								 Срок кредита – от года до трёх лет.&nbsp;
							</p>
 </li>
							<li>
							<p>
								 Минимальная сумма – <b>100 000</b> рублей.
							</p>
 </li>
							<li>Действие программы распространяется на приобретение новых автомобилей <b>2016</b> и <b>2017</b> годов выпуска, произведённых на территории РФ, стоимостью не более <b>1,45 млн </b>рублей. </li>
						</ul>
 <br>
						 Действие программы продлится до <b>31 декабря 2017 </b>года.
						<p>
						</p>
					</div>
				</div>
			</div>
		</div>
 <br>
		 <!-- Правый блок для ПС -->
		<div class="col-md-4 hidden-xs hidden-sm">
			<div class="row">
				<div class="col-sm-12">
					<div class="block-card right note_text">
						<h2>Связаться с банком</h2>
						<div class="form_application_line">
							<div class="contacts_block">
 <a href="mailto:email@zenit.ru">info@zenit.ru</a><br>
								 +7 (495) 967-11-11<br>
								 8 (800) 500-66-77
							</div>
							<div class="note_text">
								 звонок по России бесплатный
							</div>
						</div>
 <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>*/?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>