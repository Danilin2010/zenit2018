<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Автокредит — онлайн-заявка на новый или подержанный автомобиль  | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	"",
	Array(
		"PRODUCTHEADER_IMG" => "/personal/credits/carloans/img/autocreditCatalogIconBig.png",
		"PRODUCTHEADER_TITLE" => "Автокредит"
	)
);?> 
<div class="tabs-controls" id="tabs-controls-app"> 	
  <div class="tabs-controls__wrapper"> <nav class="tabs-controls__inner"> 		<?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_ACTIVE" => "Y",
		"PRODUCTHEADER_SRC" => "#credits",
		"PRODUCTHEADER_STAR" => "N",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_TITLE" => "Кредиты"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.tabbutton",
	"",
	Array(
		"PRODUCTHEADER_ACTIVE" => "N",
		"PRODUCTHEADER_SRC" => "#salons",
		"PRODUCTHEADER_STAR" => "Y",
		"PRODUCTHEADER_TARGET" => "N",
		"PRODUCTHEADER_TITLE" => "Автосалоны"
	)
);?> </nav> 	</div>
 </div>
 
<div class="tab-content tab-content_state-active" id="credits"> 	 
<!-- Фон подложки таба-->
 	
  <div class="tab-content__theme tab-content__theme_grey"> 		
    <div class="tab-content__wrapper"> 			
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				
        <div class="col col_size-12"> 					
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "12,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "0",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "100",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "тыс. ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/new-car/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/new-car/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/new-car/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет купить иномарку или&nbsp;отечественный автомобиль у&nbsp;официального диллера",
		"SIDECOLUMN_TITLE" => "Новый автомобиль",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array("если ожидаете получение крупной суммы в перспективе","например &#151; от продажи недвижимости или с премии",""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "14",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "15",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_FEATURE3_TITLE" => "6,5",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/a-car-loan-with-a-delayed-payment/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит для&nbsp;тех, кто хочет уменьшить платеж по&nbsp;кредиту за&nbsp;счет переноса части кредита на&nbsp;последний платеж",
		"SIDECOLUMN_TITLE" => "Новый автомобиль с&nbsp;остаточным платежом",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array("оригинал ПТС не нужно сдавать в банк","заявку на кредит можно подать в автосалоне-партнере",""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "13,9",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "первоначальный<br>взнос",
		"SIDECOLUMN_FEATURE2_TITLE" => "0",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "100",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "тыс ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/car-old/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/car-old/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/carloans/car-old/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Кредит на&nbsp;приобретение понравившегося подержанного автомобиля в&nbsp;салоне-партнере банка",
		"SIDECOLUMN_TITLE" => "Автомобиль с&nbsp;пробегом",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> 					</div>
         				</div>
       			</div>
     			
      <div class="tab-content__inner"> 				
        <div class="col col_size-12"> 					
          <div class="recommended-cards"> 						
            <div class="recommended-cards__title"> 							 Вам может понадобиться 						</div>
           						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"RECOMMENDED_TITLE" => "Автосалоны",
		"RECOMMENDED_TEXT" => "Партнеры банка, в которых можно приобрести автомобиль в кредит",
		"RECOMMENDED_LINK" => "/personal/credits/carloans/?start-tab=salons",
		"RECOMMENDED_IMG" => "/personal/credits/carloans/img/information.svg"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<div class="tab-content" id="salons"> 	 
<!-- Фон подложки таба-->
 	
  <div class="tab-content__theme tab-content__theme_grey"> 		
    <div class="tab-content__wrapper"> 			
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				
        <div class="col col_size-12"> 					
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/carloans/dealers/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Выбрать салон",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/carloans/dealers/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "",
		"SIDECOLUMN_TEXT" => "Партнеры банка, у&nbsp;которых можно приобрести новую иномарку или&nbsp;отечественный автомобиль, в&nbsp;том числе с&nbsp;пробегом ",
		"SIDECOLUMN_TITLE" => "Автосалоны",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>