<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Перечень автосалонов-партнеров Банка");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/car.png');
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
            <!-- Правый блок мобилка и планшет -->
            <div class="col-sm-12 col-md-4 hidden-lg">
                <div class="row note_text manager-top">
                    <div class="block-card right note_text">
                        <h2>Связаться с банком</h2>
                        <div class="form_application_line">
                            <div class="contacts_block">
                                <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
                                +7 (495) 967-11-11<br>
                                8 (800) 500-66-77
                            </div>
                            <div class="note_text">
                                звонок по России бесплатный
                            </div>
                        </div>
                        <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
                    </div>
                </div>
            </div>
		</div>
		<div class="block_type_center">
        <div class="text_block">
	       <div class="faq">
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Москва</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ol>
<li>ТЦ Кунцево: <a href="http://www.kuntsevo.com" target="_blank">www.kuntsevo.com</a></li>
<li>Независимость: <a href="http://www.indep.ru" target="_blank">www.indep.ru</a></li>
<li>Группа Компаний АвтоСпецЦентр: <a href="http://www.ascgroup.ru" target="_blank">www.ascgroup.ru</a></li>
<li>АвтоПассаж: <a href="http://www.autopassage.ru" target="_blank">www.autopassage.ru</a></li>
<li>Азимут СП: <a href="http://www.bmw-azimutsp.ru" target="_blank">www.bmw-azimutsp.ru</a></li>
<li>Автолейманн: <a href="http://www.autolehmann.ru" target="_blank">www.autolehmann.ru</a></li>
<li>Вист Авто: <a href="http://www.vistauto.kia.ru">www.vistauto.kia.ru</a></li>
<li>Группа компаний «Авто-М»: <a href="http://www.auto-mo.ru" target="_blank">www.auto-mo.ru</a></li>
<li>ТоргМаш: <a href="http://www.torgmash-avto.ru" target="_blank">www.torgmash-avto.ru</a></li>
<li>РУС-ЛАН: <a href="http://www.rus-lan.ru" target="_blank">www.rus-lan.ru</a></li>
<li>Петровский Автоцентр: <a href="http://www.petrovskiy.ru" target="_blank">www.petrovskiy.ru</a></li>
<li>Автоцентр ОВОД: <a href="http://www.ovod-nissan.ru">www.ovod-nissan.ru</a></li>
<li>ААРОН АВТО: <a href="http://www.aaron-auto.ru" target="_blank">www.aaron-auto.ru</a></li>
<li>Маршал Авто: <a href="http://www.marshalauto.ru" target="_blank">www.marshalauto.ru</a></li>
<li>Форд Центр Кутузовский: <a href="http://www.ford-kutuzovskiy.ru" target="_blank">www.ford-kutuzovskiy.ru</a></li>
<li>Авиньон: <a href="http://www.avignon-auto.ru" target="_blank">www.avignon-auto.ru</a></li>
<li>Автономия: <a href="http://www.vw-autonomy.ru" target="_blank">www.vw-autonomy.ru</a></li>
<li>АвтоГермес: <a href="http://www.avtogermes.ru" target="_blank">www.avtogermes.ru</a></li>
<li>АвтоГАЗ: <a href="http://www.avto-gaz.ru" target="_blank">www.avto-gaz.ru</a></li>
<li>Ю.С.Импекс: <a href="http://www.ford-usimpex.ru" target="_blank">www.ford-usimpex.ru</a></li>
<li>Группа Компаний Автоцентр Сити: <a href="http://www.avto-city.ru">www.avto-city.ru</a></li>
<li>Авалюкс: <a href="http://www.avalux.ru" target="_blank">www.avalux.ru</a></li>
<li>Ирбис: <a href="http://www.irbis-auto.ru">www.irbis-auto.ru</a></li>
</ol>   
				</div>
            </div>
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Самара</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ol>
<li>Субару Центр Самара: <a href="http://www.samara.subaru.ru/" target="_blank">www.samara.subaru.ru</a></li>
<li>Арго, сеть автосалонов: <a href="http://www.avtosalonargo.ru" target="_blank">avtosalonargo.ru</a></li>
<li>Ауди Центр Самара: <a href="http://www.audi-samara.ru/" target="_blank">www.audi-samara.ru</a></li>
<li>Автомир Самара: <a href="http://www.samara.ford-avtomir.ru" target="_blank">www.samara.ford-avtomir.ru</a></li>
</ol>  
				</div>
            </div>
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Кемерово</div>
                </div>
                <div class="faq_text" style="display: none;">
                    <ol>
<li>«Сибинпэкс», автосалон Cadillac, Chevrolet, Opel: <a href="http://www.sibinpex.ru/" target="_blank">www.sibinpex.ru</a></li>
<li>«СВ-Авто», официальный дилер Subaru: <a href="http://www.kemerovo.subaru.ru/" target="_blank">www.kemerovo.subaru.ru</a></li>
<li>«СВ-Авто», официальный дилер Geely: <a href="http://www.geely-kemerovo.ru/" target="_blank">www.geely-kemerovo.ru</a></li>
<li>«Картель», дилерский центр NISSAN: <a href="http://www.cartelauto.ru/" target="_blank">www.cartelauto.ru</a></li>
<li>«ФЦ Кемерово», автосалон Volkswagen: <a href="http://www.vwkemerovo.ru/" target="_blank">www.vwkemerovo.ru</a></li>
<li>«АС Кемерово», автосалон Audi: <a href="http://www.audi-kemerovo.ru/" target="_blank">www.audi-kemerovo.ru</a></li>
<li>«Автоцентр ДЮК и К», дилерский центр LADA: <a href="http://www.avtoduk.lada.ru/" target="_blank">www.avtoduk.lada.ru</a></li>
<li>ООО «Авто-С» дилерский центр SKODA: <a href="http://www.avtos-kem.ru">www.avtos-kem.ru</a></li>
<li>ЗАО «Нью-Йорк Моторс Сибирь»: 
<ul style="list-style-type: square;">
<li>Дилерский центр Ford - <a href="http://www.fordkem.ru/">www.fordkem.ru</a></li>
</ul>
</li>
</ol>
				</div>
            </div>
            <div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Новокузнецк</div>
                </div>
                <div class="faq_text" style="display: none;">
                   <ol>
<li>&laquo;Золотой дюйм&raquo;, официальный дилер HYUNDAI в Новокузнецке: <a href="http://www.golden-i.ru/" target="_blank">www.golden-i.ru</a></li>
<li>&laquo;НВК Моторс&raquo;: <a href="http://bmw-nvkmotors.ru/" target="_blank">bmw-nvkmotors.ru</a></li>
<li>Картель Авто: <a href="http://www.mitsubishi.cartelauto.ru/" target="_blank">www.mitsubishi.cartelauto.ru</a></li>
<li>СибМоторс, Opel, Chevrolet, автосалон: <a href="http://www.gmnk.ru/" target="_blank">www.gmnk.ru</a></li>
<li>&laquo;Автомобильный центр Новокузнецк&raquo;: <a href="http://www.acn-nk.ru/" target="_blank">www.acn-nk.ru</a></li>
<li>&laquo;Олимп Моторс&raquo;, официальный дилер Volkswagen: <a href="http://www.olimpmotors-vw.ru/" target="_blank">www.olimpmotors-vw.ru</a></li>
<li>&laquo;Арена Моторс&raquo;: <a href="http://www.arena-motors.ru/" target="_blank">www.arena-motors.ru</a></li>
<li>&laquo;KIA Центр на Запсибе&raquo;: <a href="http://ofk.kia.ru/" target="_blank">ofk.kia.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Новосибирск</div>
                </div>
                <div class="faq_text" style="display: none;">
  <ol>
<li>Автосалон Сармат - официальный дилер KIA, ГАЗ: <a href="http://www.sarmat.kia.ru/" target="_blank">www.sarmat.kia.ru</a>,&nbsp;<a href="http://www.sarmat.org" target="_blank">www.sarmat.org</a></li>
</ol>

				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Саров</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>Саров Моторс</li>
<li>Интерн:&nbsp;<a href="http://www.internavto.ru" target="_blank">www.internavto.ru</a></li>
<li>Автоцентр на Солнечной, 3: <a href="http://www.avto-sarov.ru/" target="_blank">www.avto-sarov.ru</a></li>
</ol>

				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Нижний Новгород</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>Автоцентр &laquo;Плаза&raquo;: <a href="http://www.mercedes-benz.nn.ru/" target="_blank">www.mercedes-benz.nn.ru</a></li>
<li>Автоцентр &laquo;Орион-сервис&raquo;: <a href="http://www.orion-nn.ru/" target="_blank">www.orion-nn.ru</a></li>
<li>Автоцентр &laquo;Артан&raquo;: <a href="http://www.artan.nnov.ru/" target="_blank">www.artan.nnov.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Дзержинск</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>Автосалон &laquo;Премио&raquo;: <a href="http://www.premio-nissan.ru/" target="_blank">www.premio-nissan.ru</a></li>
<li>Автоцентр &laquo;Юникор&raquo;:&nbsp;<a href="http://www.unikorauto.ru" target="_blank">www.unikorauto.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Ростов-на-Дону</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>ГК Гедон: <a href="http://www.gedon.ru/" target="_blank">www.gedon.ru</a></li>
<!--li>Экспресс-моторс:&nbsp;<a href="http://www.expressmotors.ru" target="_blank">www.expressmotors.ru</a></li-->
<li>ГК Сокол Моторс:&nbsp;<a href="http://www.sokolmotors.ru" target="_blank">www.sokolmotors.ru</a></li>
<!--li>Орбита-Леон-Авто:&nbsp;<a href="http://www.orbita-peugeot.ru" target="_blank">www.orbita-peugeot.ru</a></li-->
<!--li>Орбита:&nbsp;<a href="http://www.orbita-nissan.ru" target="_blank">www.orbita-nissan.ru</a></li-->
	<li>ГК ААА Моторс: <a href="http://www.aaa-motors.ru" target="_blank">www.aaa-motors.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Санкт-Петребург</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>ООО "ПКРФ": <a href="http://yesfin.ru" target="_blank">yesfin.ru</a></li>
<li>&laquo;Атлант-М Балтика&raquo;: <a href="http://www.atlant-m.spb.ru/" target="_blank">www.atlant-m.spb.ru</a></li>
<li>ГК АВТОПРОДИКС: <a href="http://www.autoprodix.ru/" target="_blank">www.autoprodix.ru</a></li>
<li>ГК А-Сервис: <a href="http://www.axis.spb.ru/" target="_blank">www.axis.spb.ru</a></li>
<li>ГК РАЛЬФ: <a href="http://www.autoralf.ru/" target="_blank">www.autoralf.ru</a></li>
<li>ГК "МАКСИМУМ": <a href="http://maximum-auto.ru" target="_blank">maximum-auto.ru</a></li>
<li>ГК "АВРОРА": <a href="http://autocenter-aurore.ru" target="_blank">autocenter-aurore.ru</a></li>
<li>Автобиография:&nbsp;<a href="http://www.a-b-g.spb.ru" target="_blank">www.a-b-g.spb.ru</a></li>
<li>&laquo;Классика&raquo;: <a href="http://www.vw-klassika.ru/" target="_blank">www.vw-klassika.ru</a></li>
<li>ГК ИАТ: <a href="http://toyotacenter.ru" target="_blank">toyotacenter.ru</a></li>
<li>ООО "Инчкейп Олимп": <a href="http://www.inchcape.ru/" target="_blank">www.inchcape.ru</a></li>
<li>ООО "Хавейл СПБ-ЮГ": <a href="http://haval-spb.ru" target="_blank">haval-spb.ru</a></li>
<li>ООО "АВТОПОЛИС КУДРОВО 2": <a href="http://www.autopole.ru">www.autopole.ru</a></li>
<li>ООО "ЛАПИН КАРС": <a href="http://lapincars.ru" target="_blank">lapincars.ru</a></li>
<li>ООО "АРКОМ": <a href="http://elan-motors.ru" target="_blank">elan-motors.ru</a></li>
<li>ООО "ШУВАЛОВО-АВТОКРЕДО": <a href="http://s-m.kia.ru" target="_blank">s-m.kia.ru</a></li>
<li>ООО "АВМ-МОТОРС": <a href="http://abm-centre.ru" target="_blank">abm-centre.ru</a></li>
<li>ООО "АВТОМОБИЛЬНОЕ БЮРО": <a href="http://august-otto-cars.ru" target="_blank">august-otto-cars.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Калининград</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>"ДО-КАР": <a href="http://do-car.kia.ru/" target="_blank">do-car.kia.ru</a></li>
<li>"ОТТО КАР": <a href="http://www.otto-car.ru" target="_blank">www.otto-car.ru</a></li>
<li>"АС Калининград": <a href="http://www.audi-kaliningrad.ru" target="_blank">www.audi-kaliningrad.ru</a></li>
<li>"Автомобильный дом": <a href="http://www.avtomobilnydom.ru" target="_blank">www.avtomobilnydom.ru</a></li>
<li>"К-Авто": <a href="http://www.ford-kld.ru" target="_blank">www.ford-kld.ru</a></li>
<li>"Франц Мобиль": <a href="http://kaliningrad.citroen.ru/" target="_blank">kaliningrad.citroen.ru</a></li>
<li>"Плеяды": <a href="http://www.oil-midland.ru/" target="_blank">www.oil-midland.ru</a>, <a href="http://www.subaru39.ru">www.subaru39.ru</a></li>
<li>"Динамика Калининград Хендэ": <a href="http://www.hyundai-kaliningrad.ru" target="_blank">www.hyundai-kaliningrad.ru</a></li>
<li>"АСКАР-СЕРВИС", "Балтийский автоцентр": <a href="http://www.ascar.su/" target="_blank">www.ascar.su</a></li>
<li>"Юки Моторс": <a href="http://www.suzuki39.com" target="_blank">www.suzuki39.com</a></li>
<li>"АСКАР", "АСКАР-АВТО": <a href="http://www.avtograd-kld.ru" target="_blank">www.avtograd-kld.ru</a></li>
<li>"Рус Моторс Трейд": <a href="http://bmw-rusmotors.ru/ru/ru/" target="_blank">www.bmw-rusmotors.ru</a></li>
<li>"Евролак": <a href="http://www.mercedes-kaliningrad.ru" target="_blank">www.mercedes-kaliningrad.ru</a></li>
<li>БРУТОКАР</li>
<li>Риэл-авто: <a href="http://www.riel-auto.ru/">www.riel-auto.ru</a></li>
<li>АВТОТЕМА: <a href="http://www.avtotema39.ru/">www.avtotema39.ru</a></li>
<li>"Автобиография"</li>
<li>"Юто Карс"</li>
<li>СМАРТ</li>
	<li>Автобиография плюс: <a href="http://www.abg-auto.ru/">www.abg-auto.ru </a></li>
	<li>Инсайд финанс: <a href="http://www.avtocredit39.ru/">www.avtocredit39.ru</a></li>
	<li>АВТОГРАД: <a href="http://www.avtograd39.ru/">www.avtograd39.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Челябинск</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>&laquo;СЕЙХО-МОТОРС&raquo;: <a href="http://toyotachel.ru" target="_blank">toyotachel.ru</a>, <a href="http://lexus-chel.ru" target="_blank">lexus-chel.ru</a></li>
<li>ГК Автоальянс: <a href="http://fiat.aalyans.ru" target="_blank">fiat.aalyans.ru</a>, <a href="http://ssangyong.aalyans.ru" target="_blank">ssangyong.aalyans.ru</a>, <a href="http://uaz.aalyans.ru" target="_blank">uaz.aalyans.ru</a></li>
<li>ГК Джемир: <a href="http://www.gemir.ru/" target="_blank">www.gemir.ru</a></li>
<li>&laquo;ДС Авто&raquo;: <a href="http://www.ds-avto.citroen.ru/" target="_blank">ds-avto.citroen.ru</a></li>
<li>&laquo;Легион-Моторс&raquo;: <a href="http://www.legion-motors.ru/" target="_blank">legion-motors.ru</a></li>
<li>&laquo;Леонар Авто&raquo;: <a href="http://dealer.peugeot.ru/leonar" target="_blank">dealer.peugeot.ru/leonar</a></li>
<li>&laquo;Омега&raquo;: <a href="http://www.omegachel.ru/" target="_blank">www.omegachel.ru</a>, <a href="http://www.mercedes-omega.ru/" target="_blank">www.mercedes-omega.ru</a></li>
<li>ООО Автоград74: <a href="http://www.avtograd74.com">www.avtograd74.com</a></li>
<li>&laquo;УралАвтоХаус&raquo;: <a href="http://www.uah.ru/" target="_blank">www.uah.ru</a></li>
<li>&laquo;Шелтре Моторс&raquo;: <a href="http://vk.com/sheltremotors" target="_blank">vk.com/sheltremotors</a></li>
<li>Чеснок авто: <a href="http://chesnokauto.ru/" target="_blank">chesnokauto.ru</a></li>
<li>ООО ЗБС-Авто: <a href="http://zbs-auto.ru/" target="_blank">zbs-auto.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Курск</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>Тойота Центр Курск: <a href="http://www.toyotabc-kursk.ru" target="_blank">toyotabc-kursk.ru</a></li>
<li>Орион Авто: <a href="http://hyundai.orion-kursk.ru/" target="_blank">hyundai.orion-kursk.ru</a></li>
</ol>
				</div>
</div>

<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Сургут</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
	<li>&laquo;ВОСТОК МОТОРС СУРГУТ&raquo;: <a href="http://www.mazda-surgut.ru" target="_blank">www.mazda-surgut.ru</a>, <a href="http://www.vmsurgut-skoda.ru" target="_blank">www.vmsurgut-skoda.ru</a></li>
	<li>&laquo;Техцентр Сургут&raquo;: <a href="http://www.volvocarsurgut.ru" target="_blank">www.volvocarsurgut.ru</a></li>
	<li>&laquo;АВТО-МОТОРС&raquo;: <a href="http://surgut.lada.ru" target="_blank">surgut.lada.ru</a></li>
	<li>&laquo;АвтоПартнер&raquo;: <a href="http://sk-motors.net" target="_blank">sk-motors.net</a></li>
	<li>ПКФ &laquo;Новотех&raquo;: <a href="http://www.novotekh-surgut.ru" target="_blank">www.novotekh-surgut.ru</a></li>
</ol>
				</div>
</div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Екатеринбург</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>ГК Независимость: <a href="http://www.ekb.indep.ru" target="_blank">www.ekb.indep.ru</a></li>
<li>ООО &laquo;Майя Моторс&raquo;: <a href="http://www.mayamotors.ru" target="_blank">www.mayamotors.ru</a></li>
<li>ООО &laquo;ШТЕРН&raquo;: <a href="http://www.mercedes-stern.ru" target="_blank">www.mercedes-stern.ru</a></li>
<li>ООО "Автобан-Запад": <a href="http://opel.avtoban.biz/" target="_blank">opel.avtoban.biz</a></li>
<li>ООО "Автобан": <a href="http://opel.avtoban.biz/" target="_blank">opel.avtoban.biz</a></li>
<li>ООО "Автобан-Запад-Плюс": <a href="http://vw-avtoban.ru/" target="_blank">vw-avtoban.ru</a></li>
<li>ООО "Автобан-Юг": <a href="http://vw-avtoban.ru/" target="_blank">vw-avtoban.ru</a></li>
<li>&laquo;Автобан-Renault&raquo;: <a href="http://www.avtoban-renault.ru" target="_blank">www.avtoban-renault.ru</a></li>
</ol>

				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Пермь</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>ЭКС АВТО: <a href="http://www.eks-auto.ru/" target="_blank">www.eks-auto.ru</a></li>
<li>"Нэкст авто": <a href="http://www.nekst-auto.ru/" target="_blank">www.nekst-auto.ru</a></li>
<li>Дав-Авто: <a href="http://www.dav-auto.ru/" target="_blank">www.dav-auto.ru</a></li>
<li>"Премьер": <a href="http://www.audi-perm.ru/" target="_blank">www.audi-perm.ru</a></li>
<li>Дав-авто-прокат: <a href="http://www.comcars.ru/" target="_blank">www.comcars.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Ижевск</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>ООО "Аспэк- Авто" - <a href="http://aspec-auto.ru" target="_blank">aspec-auto.ru</a>: 
<ul style="list-style-type: square;">
<li>Дилерский центр Skoda - <a href="http://www.aspec-lider.ru" target="_blank">www.aspec-lider.ru</a></li>
<li>Дилерский центр Honda - <a href="http://www.honda-aspec.ru" target="_blank">www.honda-aspec.ru</a></li>
<li>Дилерский центр Lexus - <a href="http://lexus.aspec-auto.ru" target="_blank">lexus.aspec-auto.ru</a></li>
<li>Дилерский центр Nissan - <a href="http://nissan.aspec-auto.ru" target="_blank">nissan.aspec-auto.ru</a></li>
<li>Дилерский центр Toyota - <a href="http://toyota.aspec-auto.ru" target="_blank">toyota.aspec-auto.ru</a></li>
<li>Дилерский центр Kia - <a href="http://aspec.kia.ru" target="_blank">aspec.kia.ru</a></li>
<li>Дилерский центр Ford - <a href="http://ford.aspec-auto.ru" target="_blank">ford.aspec-auto.ru</a></li>
<li>Дилерский центр Datsun - <a href="http://datsun.aspec-auto.ru" target="_blank">datsun.aspec-auto.ru</a></li>
</ul>
</li>
<li>"ИТС-Авто": 
<ul style="list-style-type: square;">
<li>Официальный дилер Volkswagen <a href="http://www.its-auto.ru" target="_blank">www.its-auto.ru</a></li>
<li>Официальнй дилер Mazda - <a href="http://www.mazda-izhevsk.ru" target="_blank">www.mazda-izhevsk.ru</a></li>
</ul>
</li>
<li>Автосалон "Авто-Центр": <a href="http://www.av18.ru" target="_blank">www.av18.ru</a></li>
</ol>
				</div>
            </div>
<div class="faq_item">
                <div class="faq_top">
                    <div class="faq_pict"><div class="faq_arr"></div></div>
                    <div class="faq_top_text">Бийск</div>
                </div>
                <div class="faq_text" style="display: none;">
<ol>
<li>ТЕРМИНАЛ-МОТОРС Автоцентр: <a href="http://terminal-motors.ru/" target="_blank">terminal-motors.ru</a></li>
</ol>
				</div>
            </div>
            </div>
         </div>
      </div>
            <!-- Правый блок для ПС -->
            <!--div class="col-md-4 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="block-card right note_text">
                            <h2>Связаться с банком</h2>
                            <div class="form_application_line">
                                <div class="contacts_block">
                                    <a href="mailto:info@zenit.ru">info@zenit.ru</a><br>
                                    +7 (495) 967-11-11<br>
                                    8 (800) 500-66-77
                                </div>
                                <div class="note_text">
                                    звонок по России бесплатный
                                </div>
                            </div>
                            <a href="#modal_form-contact" class="open_modal button bigmaxwidth">задать вопрос</a>
                        </div>
                    </div>
                </div>
            </div-->
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>