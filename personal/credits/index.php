<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Потребительский кредит наличными - заявка на кредит без залога и поручителей | Банк ЗЕНИТ");
$APPLICATION->SetTitle("Title");
?><?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.productheader",
	"",
	Array(
		"PRODUCTHEADER_IMG" => "/personal/credits/img/creditsCatalogIconBig.png",
		"PRODUCTHEADER_TITLE" => "Кредиты"
	)
);?> 
<div class="tab-content tab-content_state-active" id="classic"> 	 
<!-- Фон подложки таба-->
 	
  <div class="tab-content__theme tab-content__theme_grey"> 		
    <div class="tab-content__wrapper"> 			
      <div class="tab-content__inner"> 				 
<!-- Контент без боковой колонки-->
 				
        <div class="col col_size-12"> 					
          <div class="product-cards"> 						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array("не нужен залог или поручители","можно подтвердить доход по справке банка",""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "11,5",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальный<br>срок",
		"SIDECOLUMN_FEATURE2_TITLE" => "2",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "года",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_TITLE" => "3",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/personal-loans/potrebitelskiy-kredit-po-fiksirovannoy-stavke/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Потребительский кредит наличными с&nbsp;фиксированной процентной ставкой на&nbsp;весь срок",
		"SIDECOLUMN_TITLE" => "Специальный от&nbsp;200&nbsp;000&nbsp;₽",
		"SIDECOLUMN_TYPE" => "product-card_theme-promo"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array("можно объединить до 5 кредитов и кредитных карт","можно получить дополнительную сумму денег",""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "12,5",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE2_TITLE" => "100",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_FEATURE3_TITLE" => "7",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/personal-loans/refinancing/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/personal-loans/refinancing/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/personal-loans/refinancing/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет получить более выгодные условия по&nbsp;кредитам других банков, уменьшить платёж или&nbsp;ставку",
		"SIDECOLUMN_TITLE" => "Рефинансирование кредита",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "14,5",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE2_TITLE" => "30",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "минимальный<br>срок",
		"SIDECOLUMN_FEATURE3_TITLE" => "1",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "год",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/personal-loans/na-lyubye-tseli-bez-zaloga-i-poruchiteley/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/personal-loans/na-lyubye-tseli-bez-zaloga-i-poruchiteley/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/personal-loans/na-lyubye-tseli-bez-zaloga-i-poruchiteley/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Потребительский кредит наличными на&nbsp;любые цели без&nbsp;залога и&nbsp;поручителей",
		"SIDECOLUMN_TITLE" => "Наличными до 200 000 ₽",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "14",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE2_TITLE" => "60",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_FEATURE3_TITLE" => "7",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " лет",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет повысить вероятность одобрения заявки на&nbsp;кредит наличными, предоставив в&nbsp;залог свой автомобиль",
		"SIDECOLUMN_TITLE" => "Под залог автомобиля",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_BRAND_LIST" => array(""),
		"SIDECOLUMN_DATA_FILTER" => array(""),
		"SIDECOLUMN_FEATURE1_BEFORE" => "от",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "процентная<br>ставка",
		"SIDECOLUMN_FEATURE1_TITLE" => "14",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "минимальная<br>сумма",
		"SIDECOLUMN_FEATURE2_TITLE" => "60",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "000 ₽",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_FEATURE3_TITLE" => "7",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => " лет",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/personal-loans/pod-poruchitelstvo-fizlits-i-zalog-avtomobilya/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_TEXT" => "Для тех, кто хочет повысить вероятность одобрения заявки на&nbsp;кредит наличными, предоставив поручителя",
		"SIDECOLUMN_TITLE" => "Под поручительство",
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.sidecolumn",
	"",
	Array(
		"SIDECOLUMN_TITLE" => "Под залог недвижимости",
		"SIDECOLUMN_TEXT" => "Если вам необходима крупная сумма денег и вы готовы предоставить в залог недвижимость",
		"SIDECOLUMN_IMG" => "",
		"SIDECOLUMN_BRAND_LIST" => array(),
		"SIDECOLUMN_TYPE" => "product-card_theme-normal product-card_disabled",
		"SIDECOLUMN_DATA_FILTER" => array(),
		"SIDECOLUMN_FEATURE1_BEFORE" => "",
		"SIDECOLUMN_FEATURE1_TITLE" => "15,5",
		"SIDECOLUMN_FEATURE1_TITLE_SMALL" => "%",
		"SIDECOLUMN_FEATURE1_DESCRIPTION" => "минимальная<br>ставка",
		"SIDECOLUMN_FEATURE2_BEFORE" => "",
		"SIDECOLUMN_FEATURE2_TITLE" => "14",
		"SIDECOLUMN_FEATURE2_TITLE_SMALL" => "млн ₽",
		"SIDECOLUMN_FEATURE2_DESCRIPTION" => "максимальная<br>сумма",
		"SIDECOLUMN_FEATURE3_BEFORE" => "",
		"SIDECOLUMN_FEATURE3_TITLE" => "15",
		"SIDECOLUMN_FEATURE3_TITLE_SMALL" => "лет",
		"SIDECOLUMN_FEATURE3_DESCRIPTION" => "максимальный<br>срок",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION" => "/personal/credits/personal-loans/pod-zalog-nedvizhimosti/",
		"SIDECOLUMN_LINK_ADD_CONTRIBUTION_TEXT" => "Подать заявку",
		"SIDECOLUMN_LINK_LEARN_MORE" => "/personal/credits/personal-loans/pod-zalog-nedvizhimosti/",
		"SIDECOLUMN_LINK_LEARN_MORE_TEXT" => "Подробнее",
		"SIDECOLUMN_LINK_CART" => "/personal/credits/personal-loans/pod-zalog-nedvizhimosti/"
	)
);?> 					</div>
         				</div>
       			</div>
     			
      <div class="tab-content__inner"> 				
        <div class="col col_size-12"> 					
          <div class="recommended-cards"> 						
            <div class="recommended-cards__title"> 							 Обратите внимание 						</div>
           						 <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Подбор недвижимости",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/realty.svg",
		"RECOMMENDED_LINK" => "/personal/credits/personal-loans/dlya-voennosluzhashchikh/",
		"RECOMMENDED_TEXT" => "Кредит наличными на любые цели без залога и&nbsp;поручителей до&nbsp;3&nbsp;млн&nbsp;рублей",
		"RECOMMENDED_TITLE" => "Для военнослужащих"
	)
);?> <?$APPLICATION->IncludeComponent(
	"aic.bz:snippets.recommended",
	"",
	Array(
		"PRODUCTHEADER_TITLE" => "Справочная информация",
		"RECOMMENDED_IMG" => "/personal/mortgage/img/additional.svg",
		"RECOMMENDED_LINK" => "/personal/tatneft/#credits",
		"RECOMMENDED_TEXT" => "Специальные условия по кредитам на&nbsp;различные потребительские цели",
		"RECOMMENDED_TITLE" => "Сотрудникам Татнефть"
	)
);?> 					</div>
         				</div>
       			</div>
     		</div>
   	</div>
 </div>
 
<br />
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>