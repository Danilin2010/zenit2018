<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("html_text_for_caps", "&lt;p&gt;Акция до 31 мая 2018 года! Процентная ставка по кредиту не зависит от срока кредитования.&lt;/p&gt;&lt;p&gt;&nbsp;&lt;/p&gt;");
$APPLICATION->SetTitle("Потребительский кредит «Специальный»");
BufferContent::SetTitle('top_breadcrumbs_images',SITE_TEMPLATE_PATH.'/img/inner/inner-personal.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Условия кредитования</a></li>
			<li><a href="#content-tabs-2">Страхование</a></li>
			<li><a href="#content-tabs-3">Погашение</a></li>
			<li><a href="#content-tabs-4">Документы</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="wr_block_type">
					<div class="block_type to_column z-container">
						<div class="block_type_right">
							<div class="form_application_line">
								<div class="right_top_line">
									<div class="right_bank_block">
										<div class="right_bank_title">
											 Рубли
										</div>
										<div class="right_bank_text">
											 Валюта кредита
										</div>
									</div>
									<div class="right_bank_block">
										<div class="right_bank_title">
											 2 дня
										</div>
										<div class="right_bank_text">
											 срок рассмотрения заявки
										</div>
									</div>
								</div>
							</div>
							<div class="form_application_line">
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Оставить заявку</a>
							</div>
						</div>
						<div class="block_type_center">
							<h1>Условия кредитования</h1>
							 <!--conditions-->
							<div class="conditions">
								<div class="conditions_item">
									<div class="conditions_item_title">
										<span>от</span> 11,5 %
									</div>
									<div class="conditions_item_text">
										 ставка
									</div>
								</div>
								<div class="conditions_item">
									<div class="conditions_item_title">
										<span>до</span> 7 лет
									</div>
									<div class="conditions_item_text">
										 срок кредита
									</div>
								</div>
							</div>
							 <!--conditions-->
							<div class="text_block">
								<ul class="big_list">
									<li>Фиксированная процентная ставка: от 11,5% годовых</li>
									<li>
									Сумма кредита – до 3 млн рублей</li>
									<li>
									Срок – от двух до семи лет</li>
									<li>
									Досрочное погашение без штрафов и комиссий</li>
									<li>Возможность подтверждения дохода справкой по форме Банка</li>
									<li>Бесплатное оформление кредитной карты</li>
<li>Диапазоны значений полной стоимости кредита: 11,500 – 21,950% годовых</li>
								</ul>
 <br>
								<h1>Ставки</h1>
								<table border="1" cellspacing="0" cellpadding="0">
								<tbody>
								<tr>
									<td rowspan="2">
										<p align="center">
											 Сумма (руб.)
										</p>
										<p>
										</p>
									</td>
									<td colspan="2">
										<p align="center">
											 Ставка (% годовых)*
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>
											 Зарплатные клиенты, военнослужащие
										</p>
									</td>
									<td>
										<p>
											 Клиенты иных категорий
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>
											 от 2 млн до 3 млн
										</p>
									</td>
									<td>
										<p>
											 11,5%
										</p>
									</td>
									<td>
										<p>
											 12,5%
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>
											 от 500 000 до 1 999 999
										</p>
									</td>
									<td>
										<p>
											 13,5%
										</p>
									</td>
									<td>
										<p>
											 14,5%
										</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>
											 от 200 000 до 499 000
										</p>
									</td>
									<td>
										<p>
											 14,5%
										</p>
									</td>
									<td>
										<p>
											 15,5%
										</p>
									</td>
								</tr>
								</tbody>
								</table>
								<div class="note_text">
									*При отсутствии финансовой защиты заёмщика процентная ставка увеличивается на 6 процентных пунктов.
								</div>
								<p>Указанные процентные ставки действуют при подаче заявки на кредит через интернет-банк или мобильное приложение «ЗЕНИТ Онлайн». При подаче заявки в офисе Банка ставка увеличивается на 0,4 процентного пункта.<br>
Для подключения интернет-банка и мобильного приложения необходима карта Банка ЗЕНИТ. В любом офисе Банка Вы можете оформить моментальную карту Visa Quick Welcome с бесплатным обслуживанием. С ней Вы сможете не только получить пониженную ставку по кредиту, но и легко вносить платежи.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="wr_block_type">
					<div class="block_type to_column z-container">
						<div class="block_type_right">
						</div>
						<div class="block_type_center">
							<h1>Требования к заемщику</h1>
							<ul class="big_list">
								<li>
								Регистрация по месту жительства в регионе присутствия Банка </li>
								<li>
								Место работы на территории РФ </li>
								<li>
								Гражданство РФ</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Страховой полис избавит Вас и Ваших близких от исполнения обязательств по кредиту в случае возникновения непредвиденной ситуации – это сделает за Вас страховая компания.
							</p>
							<p>
								 Полис действует во всех странах мира.
							</p>
							<p>
								 Страховые риски: смерть в результате несчастного случая или болезни, утрата трудоспособности в связи с установлением инвалидности I или II группы, недобровольная потеря работы (опционально).
							</p>
							<p>
								 Стоимость полиса:
							</p>
							<ul class="big_list">
								<li>3,6% от суммы кредита ежегодно с покрытием трёх рисков (смерть, утрата трудоспособности в связи с установлением инвалидности I или II группы, недобровольная потеря работы);</li>
								<li>2% от суммы кредита ежегодно с покрытием двух рисков (смерть, утрата трудоспособности в связи с установлением инвалидности I или II группы).</li>
							</ul>
							<p>
								 Услуга предоставляется страховой компанией «РГС-Жизнь» и Страховым Домом «ВСК» (по Вашему выбору).
							</p>
						</div>
					</div>
				</div>
			</div>
			</div>
				<div class="content_body" id="content-tabs-3">
					<div class="wr_block_type">
						<div class="block_type to_column c-container">
							<div class="block_type_right">
							</div>
							<div class="block_type_center">
								<div class="text_block">
									<p>
										 Погашение кредита осуществляется Банком из денежных средств, размещённых на Вашем счёте для погашения (реквизиты счёта указаны в Вашем кредитном договоре). 
									</p>
									<p>
										 Вам необходимо самостоятельно контролировать состояние счёта для погашения кредита. При недостатке средств пополняйте счёт как минимум до суммы, указанной в графике погашения кредита. 
									</p>
									<h3>
										Досрочное погашение </h3>
									<p>
										 Вы можете погашать кредит досрочно. Для этого необходимо направить в Банк заявление. Частичное досрочное погашение кредита осуществляется не ранее чем через 30 дней с момента получения Банком заявления-уведомления или в дату ближайшего платежа. При этом уменьшается размер ежемесячного платежа, изменение срока кредита не производится.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="content_body" id="content-tabs-4">
					<div class="wr_block_type">
						<div class="block_type to_column c-container">
							<div class="block_type_right">
		<div class="doc_list">
			<a href="/media/doc/personal/credit/credit_main_rules_201804.pdf" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						 Информация об условиях предоставления, использования и возврата потребительского кредита
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div>
							</div>
							<div class="block_type_center">
								<div class="text_block">
									<p>
										 Для оформления кредита необходимы следующие документы:
									</p>
									<ul class="big_list">
										<li><a target="_blank" href="/upload/iblock/741/anket_loan.pdf">заявление-анкета</a>;</li>
										<li>паспорт РФ;</li>
										<li>один из дополнительных документов: СНИЛС, ИНН, водительское удостоверение, загранпаспорт;</li>
										<li>копия документа, подтверждающего занятость;</li>
										<li>документы, подтверждающие величину доходов.</li>
									</ul>
									<p>
										 Если заёмщик – лицо мужского пола в возрасте до 27 лет, Банк вправе запросить документы, подтверждающие прохождение воинской службы/ отсрочку от прохождения воинской службы/ увольнение в запас.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
	</div>
	<div class="block_type to_column c-container reversal">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Потребительский кредит \"Специальный\"",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "universal",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>