<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Удаленное обслуживание");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/internet_bank.png');
?><div class="content_rates_tabs">
	<ul class="content_tab c-container">
		<li><a href="#content-tabs-1">Зачем Вам «ЗЕНИТ Онлайн»</a></li>
		<li><a href="#content-tabs-2">Как подключить</a></li>
	</ul>
	<div class="content_body" id="content-tabs-1">
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_right">
					<div class="doc_list">
 <a href="/media/doc/personal/dbo/dbo_memory.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Памятка для клиентов
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <a href="/media/doc/personal/dbo/rules_dbo_20180215.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Правила дистанционного банковского обслуживания физических лиц в ПАО Банк ЗЕНИТ <!--(вступают в действие с 15.01.2018 г.)-->
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <!--a href="/media/doc/personal/dbo/rules_dbo_20180215.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Правила ДБО, вступают в действие с 15.02.2018 г.
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br-->
 <a href="/media/doc/personal/dbo/dbo_use_instruction.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Инструкция по использованию интернет-банка ЗЕНИТ Онлайн <!--(вступают в действие с 15.01.2018 г.)-->
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <!--a href="/media/doc/personal/dbo/dbo_use_instruction.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Инструкция по использованию интернет-банка ЗЕНИТ Онлайн
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a-->
 <a href="/media/doc/personal/dbo/tariffs_fiz/dbo_tariffs_20180226.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Тарифы комиссионного вознаграждения, взимаемого ПАО Банк ЗЕНИТ за дистанционное банковское обслуживание физических лиц
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <!--a href="/media/doc/personal/dbo/tariffs_fiz/dbo_tariffs_20180226.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Тарифы, вступают в действие с 26.02.2018 г.
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br-->
 <a href="/media/doc/personal/card2card/c2c_20170828.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Правила и тарифы ПАО Банк ЗЕНИТ на оказание физическим лицам Услуги «Перевод с карты на карту»
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <a href="/media/doc/personal/card2card/online_moneysend.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Информация о переводах с карты на карту
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <a href="/media/doc/personal/card2card/cardsave.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Как "привязать" карту стороннего банка в "ЗЕНИТ Онлайн"
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a><br>
 <a href="/media/doc/personal/dbo/online_safety_notice_01072017.pdf" target="_blank">
						<div class="doc_pict pdf">
						</div>
						<div class="doc_body">
							<div class="doc_text">
								 Памятка и рекомендации по безопасному использованию сервисов дистанционного обслуживания
							</div>
							<div class="doc_note">
							</div>
						</div>
 </a>
					</div>
				</div>
				<div class="block_type_center">
					<h1>Интернет-банк и мобильное приложение «ЗЕНИТ Онлайн»</h1>
					<h2>Зачем Вам «ЗЕНИТ Онлайн»</h2>
					<ul class="big_list">
						<li>
						Информация о балансе карточных и текущих счетов, суммах вкладов, остатках по кредитам; </li>
						<li>
						История операций; </li>
						<li>
						Шаблоны и автоплатежи; </li>
						<li>
						Даты блажайших платежей по кредитам и кредитным картам; </li>
						<li>
						Информативный сервис анализа расходов; </li>
						<li>
						Онлайн-заявки на карты и кредиты; </li>
						<li>
						Открытие вкладов с повышенными процентными ставками. </li>
						<li>
						Поиск и оплата штрафов ГИБДД и налогов; </li>
						<li>
						Переводы с карты на карту; </li>
						<li>
						«Привязка» карт сторонних банков; </li>
						<li>
						Переводы клиентам Банка ЗЕНИТ по номеру мобильного телефона; </li>
						<li>
						Оплата более 500 видов услуг (ЖКХ, мобильная связь и др.); </li>
						<li>
						Погашение кредитов (в том числе в других банках); </li>
						<li>
						И многое другое!</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="content_body" id="content-tabs-2">
		<div class="wr_block_type">
			<div class="block_type to_column c-container">
				<div class="block_type_center">
					<h1>Как подключить:</h1>
					 <!--step_block-->
					<div class="step_block">
						<div class="step_item">
							<div class="step_num">
								1
							</div>
							<div class="step_body">
								<div class="step_text">
									 Позвоните в Контакт-центр Банка по телефону 8 (800) 500-66-77 (звонок бесплатный) и сообщите о своём намерении подключить интернет-банк.
								</div>
							</div>
						</div>
						<div class="step_item">
							<div class="step_num">
								2
							</div>
							<div class="step_body">
								<div class="step_text">
									 Завершите регистрацию на сайте <a href="http://my.zenit.ru" target="_blank">my.zenit.ru</a>
									или в мобильном приложении (для <a href="https://itunes.apple.com/ru/app/zenit-onlajn/id1187159866?mt=8" target="_blank">iOS;</a> для <a href="https://play.google.com/store/apps/details?id=ru.zenit.zenitonline" target="_blank">Android</a>). Владельцам устройств на Windows Phone доступна мобильная версия <a href="http://m.zenit.ru" target="_blank">интернет-банка</a>.
								</div>
							</div>
						</div>
						<div class="step_item">
							<div class="step_num">
								3
							</div>
							<div class="step_body">
								<div class="step_text">
									 Если Вы хотите проверить, зарегистрированы ли Вы в интернет-банке, позвоните в Контакт-центр. Также специалисты Контакт-центра помогут Вам восстановить доступ, в случае если Вы забудете логин или пароль.
								</div>
							</div>
						</div>
						 <!--step_block-->
					</div>
				</div>
				<p>
					 Для входа в интернет-банк и мобильное приложение используются одинаковые логин и пароль, поэтому пройти процедуру регистрации Вам потребуется только один раз.
				</p>
			</div>
		</div>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>