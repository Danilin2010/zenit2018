<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Private Banking");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/family.png');
BufferContent::SetTitle('to_page_class','private-banking');
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/private-banking.css");
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
			<div class="text_block">
				<p>
					 Private Banking Банка ЗЕНИТ– это эффективное управление вашим капиталом. Мы готовы обеспечить профессиональное решение финансовых вопросов с учетом ваших индивидуальных пожеланий.
				</p>
				<p>
					Персональный менеджер предложит вам оптимальную программу для сохранения и приумножения вашего капитала. <br>
				</p>
				<p>
					Мы работаем с российскими и иностранными гражданами с положительной деловой репутацией, готовыми разместить и поддерживать в различных инструментах Банка сумму не менее 15 млн рублей или ее эквивалент в иностранной валюте. <br>
				</p>
				<p>
					Private Banking Банка ЗЕНИТ соблюдает основные принципы частного банковского обслуживания: безопасность и конфиденциальность, профессионализм и компетентность, индивидуальный подход, оперативность, комфортные условия обслуживания.
				</p>
			</div>
		</div>
	</div>
</div>
<br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>