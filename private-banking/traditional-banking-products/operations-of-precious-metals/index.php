<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Операции на рынке драгоценных металлов и обезличенные металлические счета");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						Покупка и продажа золота и серебра для Клиентов с зачислением на обезличенные металлические счета (не слитки)
					</p>
					<p>
						Покупка Клиентом слитков золота и серебра у Банка.
					</p>
				</div>
			</div>
		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>