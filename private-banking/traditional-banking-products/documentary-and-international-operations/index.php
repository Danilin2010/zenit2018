<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Документарные и международные операции");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <p>
                        Документарные (аккредитив, банковская гарантия, инкассо) и международные (валютные переводы,
                        операции и консультации по инвестициям на зарубежных фондовых рынках, фидуциарные сделки,
                        консультации по открытию счетов в иностранных банках).
                    </p>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>