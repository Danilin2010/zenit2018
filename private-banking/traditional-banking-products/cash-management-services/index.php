<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Расчетно-кассовое обслуживание");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>

    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <h1>В рамках расчетно-кассового обслуживания нашим клиентам доступен широкий спектр услуг:</h1>
                <ul class="big_list">
                    <li>
                        Расчетно-кассовые операции, конверсионные операции, операции с использованием банковских карт
                    </li>
                    <li>
                        Управление счетами и средствами на этих счетах через персонального менеджера
                    </li>
                    <li>
                        Управление текущими операциями Клиента в соответствии с согласованным с Клиентом финансовым планом
                    </li>
                    <li>
                        Оптимизация наличных и безналичных расчетных и конверсионных операций Клиента
                    </li>
                    <li>
                        Сопровождение операций по банковским картам, в том числе контроль списания расходов по банковским картам
                    </li>
                    <li>
                        Обеспечение расходных операций Клиента за счет его активов в ПАО Банк ЗЕНИТ
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>