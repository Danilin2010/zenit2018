<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сейфовые ячейки");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>

    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <p>
                        Клиентам Private Banking мы предлагаем воспользоваться индивидуальными сейфовыми ячейками,
                        расположенными в специальных хранилищах Банка и предоставляемыми в аренду на любой удобный срок.
                    </p>
                    <p>
                        Индивидуальные ячейки – это самый безопасный способ хранения денежной наличности и других
                        ценностей, конфиденциальных документов, драгоценных камней и металлов, ценных бумаг.
                    </p>
                    <p>
                        Сейфовые ячейки могут использоваться при наличных расчетах по сделкам купли-продажи недвижимости,
                        автомобилей.
                    </p>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>