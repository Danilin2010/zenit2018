<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Элитные банковские карты");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <p>
                        Нашим клиентам доступны элитные банковские карты крупнейших международных платежных систем Visa
                        и Mastercard.
                    </p>
                    <p>
                        Мы выпускаем: Visa Platinum, Visa Infinite, MasterCard Platinum, MasterCard World Elite.
                    </p>
                    <p>
                        Для держателей данных карт предусмотрены специальные программы привилегий, с которыми Вы можете
                        ознакомиться на сайтах Visa и MasterCard или задать вопрос Вашему персональному менеджеру.
                        Эти привилегиями Вы можете воспользоваться практически в любой точке мира.
                    </p>
                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>