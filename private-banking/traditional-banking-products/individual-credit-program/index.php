<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Индивидуальные кредитные программы");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						Для решения возникающих потребностей в финансировании, Банк предлагает клиентам краткосрочное и
						долгосрочное кредитование в соответствии с их индивидуальными планами. Такие программы включают
						потребительские кредиты и кредиты на приобретение движимого и недвижимого имущества, инвестиционное
						кредитование на приобретение ценных бумаг, «овердрафтное» кредитование под текущие операции клиентов.
					</p>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>