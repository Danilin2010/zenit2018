<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Инкассирование денежных средств");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/traditions.png');
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <p>
                        Для клиентов Private Banking Банк организовывает инкассацию денежных средств или ценностей.
                        Услуга доступна в городах расположения головного офиса и филиалов Банка.
                        Перевозка ценностей Клиентов (денежных средств) осуществляется из Банка до места
                        назначения Клиента и/или наоборот.
                    </p>
                </div>
            </div>
        </div>
    </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>