<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Структурирование инвестиционного портфеля Клиента");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/rules.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						Структурирование персонального инвестиционного портфеля, состоящего из акций, производных
						финансовых инструментов, инструментов с фиксированной доходностью (депозиты, облигации) и других
						активов, который будет отвечать инвестиционным потребностям клиента с учетом его индивидуальных
						особенностей, таких как отношение к риску, инвестиционный горизонт, ликвидность капитала и т.д.
						Данная услуга предоставляется лицам со значительными размещениями.
					</p>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>