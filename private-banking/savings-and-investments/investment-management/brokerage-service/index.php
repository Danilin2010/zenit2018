<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Брокерское обслуживание");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/rules.png');
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <p>
                        Банк ЗЕНИТ является универсальным брокером, осуществляющим операции на всех основных торговых
                        площадках России, что позволяет Клиентам Банка совершать сделки на наиболее выгодных условиях
                        с минимальными затратами времени и средств.
                    </p>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>