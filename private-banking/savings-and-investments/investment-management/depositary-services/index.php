<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Услуги депозитария");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/rules.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>Депозитарий ПАО Банк ЗЕНИТ предлагает своим клиентам полный спектр услуг по учету и удостоверению
						прав на ценные бумаги, включая:</p>
					<ul>
						<li>Хранение и учет прав на ценные бумаги любых форм выпусков </li>
						<li>
							Перерегистрацию прав на ценные бумаги в реестрах владельцев именных ценных бумаг и других
							депозитариях
						</li>
						<li>Оформление залога или заклада ценных бумаг на счетах «депо» Клиентов </li>
						<li>Консультации по вопросам обращения и учета прав на ценные бумаги </li>
						<li>Контроль дивидендной и процентной политики эмитентов. </li>
					</ul>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


