<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Коллективное доверительное управление");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/rules.png');
?>

    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <div class="block_type_right">

            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <p>
                        Общие Фонды Банковского Управления являются формой коллективного инвестирования, позволяющей
                        сохранять и приумножать капитал инвесторов. Все функции, связанные с управлением активами,
                        выполняет профессиональный управляющий Банка ЗЕНИТ.
                    </p>
                    <p>
                        Клиент покупает определенное количество паев в Фонде, что подтверждает сертификат долевого
                        участия, который выдается Банком ЗЕНИТ. Каждый Инвестор владеет частью всего Фонда, в
                        зависимости
                        от того, сколько паев он приобрел.
                    </p>
                </div>
            </div>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>