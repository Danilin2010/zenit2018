<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Индивидуальное доверительное управление активами Клиента");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/rules.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						В большинстве случаев Клиент, принявший решение о размещении личного капитала на финансовых рынках,
						не обладает необходимым временем и опытом для взвешенной оценки постоянно меняющейся рыночной ситуации.
						Эту проблему помогает решить механизм доверительного управления.
					</p>
					<p>
						В рамках Договора доверительного управления Клиент доверяет профессиональному управляющему
						–Банку ЗЕНИТ – денежные средства, а Банк ЗЕНИТ, используя все имеющиеся у него технологии и опыт
						квалифицированных управляющих, в соответствии с согласованными стратегиями самостоятельно
						осуществляет операции на финансовых рынках в интересах клиента.
					</p>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>