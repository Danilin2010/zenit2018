<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Частный консультант Advisory");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/rules.png');
?>
	<div class="wr_block_type">
		<div class="block_type to_column c-container">
			<div class="block_type_right">

			</div>
			<div class="block_type_center">
				<div class="text_block">
					<p>
						Услуга предоставляется Клиентам Private Banking Банка ЗЕНИТ, которые предпочитает держать руку
						на пульсе инвестиций и следить за движением своих активов. Advisory - это возможность самостоятельно
						принимать инвестиционные решения и обсуждать свои действия с профессиональным инвестиционным
						советником. Персональный консультант предоставит рекомендации по перспективности той или иной
						ценной бумаги, оценит состояние портфеля и поможет разобраться в общей ситуации на рынке.
					</p>
				</div>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>