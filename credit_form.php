<?
define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"]="N";
$APPLICATION->ShowIncludeStat = false;?>

<?
CModule::IncludeModule("form");



$FORM_ID = 19;
$arValues = array (
    "form_text_340"                 => $_POST['NAME'],       // "Фамилия, имя, отчество"
    "form_text_341"                 => $_POST['REGION'],     // "Регион/город"
    "form_text_342"                 => $_POST['PHONE'],      // "Телефон"
    "form_text_343"                 => $_POST['EMAIL']       // "Адрес почты"
);

if ($RESULT_ID = CFormResult::Add($FORM_ID, $arValues))
{
    echo "Результат #".$RESULT_ID." успешно создан";
    $name = isset($_POST['NAME']) ? $_POST['NAME'] : '';
    $email = isset($_POST['EMAIL']) ? $_POST['EMAIL'] : '';
    $phone = isset($_POST['PHONE']) ? $_POST['PHONE'] : '';
    $region = isset($_POST['REGION']) ? $_POST['REGION'] : '';
    $subject = isset($_POST['subj']) ? $_POST['subj'] : 'credit.zenit.ru';

    $headers = 'From: noreply@zenit.ru' . "\r\n" .
        "Content-Type: text/html; charset=UTF-8" . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $message = '
        <table border="1">
            <tr>
                <td>Эта заявка с лендинга "Потребительский кредит"</td>
            </tr>
            <tr>
                <td>Имя:</td>
                <td>' . $name . '</td>
            </tr>
            <tr>
                <td>Регион:</td>
                <td>' . $region . '</td>
               
            </tr>
            <tr>
                <td>Телефон:</td>
                <td>' . $phone . '</td>
            </tr>
             <tr>
                 <td>Email:</td>
                <td>' . $email . '</td>
            </tr>
        </table>
    ';

    mail( "credit@zenit.ru", $subject , $message, $headers );
}


?>