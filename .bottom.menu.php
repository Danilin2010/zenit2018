<?
$aMenuLinks = Array(
	Array(
		"О Банке", 
		"/rus/about_bank/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Подробнее о банке", 
		"#", 
		Array(), 
		Array(), 
		"false" 
	),
	Array(
		"Социальные проекты", 
		"/about/general/information-social-projects/social-projects/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Банковская группа ЗЕНИТ", 
		"https://old.zenit.ru/group/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Раскрытие информации", 
		"rus/about_bank/disclosure/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Структура акционерного капитала", 
		"/rus/about_bank/disclosure/equity-structure/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Залоговое имущество", 
		"/rus/about_bank/general/property-sale/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Публикации в СМИ", 
		"/rus/about_bank/bank_in_press/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Задать вопрос", 
		"#modal_form-contact", 
		Array(), 
		Array("class"=>"open_modal"), 
		"" 
	),
	Array(
		"Линия доверия", 
		"/about/general/trust-line/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"<!--Структура акционерного капитала-->", 
		"/rus/about_bank/disclosure/equity-structure/", 
		Array(), 
		Array(), 
		"" 
	)
);
?>