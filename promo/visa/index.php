<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Вы мечтаете о чемпионате мира по футболу FIFA™");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/inner/visa_fifa.png');
?><div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_center">
			<div class="text_block">
				<p>
					 Вступайте в Команду болельщиков Visa — единственную команду, которая точно окажется на финале Чемпионата. Оплачивайте свои ежедневные покупки с Visa, ведь каждая транзакция — это Ваш шанс выиграть поездку на финал и множество других призов каждую неделю.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
			<div class="form_application_line">
 <a href="#" class="button" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Заказать карту</a>
			</div>
		</div>
		<div class="block_type_center">
			<h2>Как вступить в Команду болельщиков Visa и выиграть призы?</h2>
			<p>
				 С 5 февраля по 5 апреля оплачивайте картой Visa свои ежедневные покупки от 300 руб. Регистрируйте чеки на <a target="_blank" href="http://fifa.visa.ru">fifa.visa.ru</a> и получайте шансы: 1 чек = 1 шанc. Получайте 2 шанса за каждый чек из магазинов-партнёров «Пятёрочка», «Карусель», «М.Видео»
			</p>
			<p>
				 Ловите шансы на выигрыш каждую неделю!
			</p>
			<div class="warning balls">
				<div class="pict ball">
				</div>
				<div class="text">
 <b>500 сетов болельщика. Розыгрыши — каждую неделю!</b>
					Всё, что нужно для домашнего просмотра Чемпионата с доставкой на дом.
				</div>
			</div>
			<div class="warning balls">
				<div class="pict ball">
				</div>
				<div class="text">
 <b>50 поездок на матчи Чемпионата мира по футболу FIFA™. Розыгрыши — каждый месяц!</b>
					<ul>
						<li>Азарт футбольной борьбы и романтика белых ночей в Санкт-Петербурге</li>
						<li>Жаркие футбольные моменты и свежесть морского бриза в Сочи</li>
						<li>Футбол мирового уровня и бодрящий воздух Балтики в Калининграде</li>
						<li>Красивая игра и рассвет с видом на Волгу в Самаре</li>
					</ul>
				</div>
			</div>
			<div class="warning balls">
				<div class="pict ball">
				</div>
				<div class="text">
 <b>5 уникальных поездок на финал</b> Чемпионата мира по футболу FIFA 2018™ с туром по стадиону и выходом на футбольное поле. Розыгрыш — в финале промоакции!<br>
					 Победителей ждут самые грандиозные впечатления от главного футбольного турнира.
					<ul>
						<li>2 билета на финальный матч + полное транспортное сопровождение + двухдневное проживание в 4-х звездочном отеле + программа гостеприимства Visa + эксклюзивный тур по стадиону для двоих с возможностью выйти на футбольное поле. </li>
					</ul>
				</div>
			</div>
			<p>
				 Вступайте в Команду болельщиков Visa на <a target="_blank" href="http://fifa.visa.ru">fifa.visa.ru!</a>
			</p>
			<div class="note_text">
				<p>
					 Срок проведения акции: с 05.02.2018 по 30.06.2018 включительно. Срок совершения покупок: с 05.02.2018 по 05.04.2018. Организатор акции — ООО «Интеджер» (ОГРН 1107746108337, ИНН 7709848474).
				</p><a name="form_fifa"></a>
				<p>
					 Подробнее об организаторе акции, правилах её проведения, количестве призов, сроках, месте и порядке их получения читайте на сайте <a target="_blank" href="http://fifa.visa.ru">fifa.visa.ru</a>. Количество призов ограничено. Корпорация «Виза Интернешнл Сервис Ассосиэйшн» (Visa International Service Association) (США). 2018 FIFA World Cup Russia™ — Чемпионат мира по футболу FIFA 2018 в России™.
				</p>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"universal",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Команда болельщиков Visa",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>