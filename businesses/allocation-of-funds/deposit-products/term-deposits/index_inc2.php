<h1>Как разместить средства</h1>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 1
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											Достаточно один раз обратиться за консультацией в Банк (по телефону или лично, в удобный для Вас офис Банка) или заполнить <a href="#" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0" style="width:100%">Онлайн-заявку</a> и за Вами будет закреплен персональный менеджер, который поможет разместить денежные средства в Банке.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										2
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											Позвоните Вашему персональному менеджеру, который подберет наиболее выгодный для Вас продукт под требуемые условия.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										3
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											Ваш персональный менеджер направит Вам по электронной почте перечень всех документов, необходимых для размещения денежных средств в Банке.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										4
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											После заполнения и подписания всех документов Вам необходимо перечислить денежные средства на указанные в договоре счета в Банке ЗЕНИТ.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										5
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											Ваш персональный менеджер проверит поступление денежных средств на счет и уведомит Вас об этом. 
										</div>
									</div>
								</div>
</div>
