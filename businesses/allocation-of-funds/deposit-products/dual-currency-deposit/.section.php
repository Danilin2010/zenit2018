<?
$sSectionName = "Бивалютный депозит";
$arDirProperties = Array(
   "description" => "Бивалютный депозит",
   "title" => "Бивалютный депозит",
   "html_text_for_caps" => "<p>Бивалютный депозит подходит для компаний, осуществляющих экспортно-импортные операции, компаний с 	мультивалютным балансом, а также для тех, кто заинтересован в получении повышенного дохода и конвертации 	валюты на выгодных условиях.</p> <ul class=\"big_list\"> 	<li>Повышенная процентная ставка.</li> 	<li>Возможность открытия депозита в рублях и в иностранной валюте.</li> 	<li>Заранее известный курс конвертации валюты.</li> </ul>"
);
?>