<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Электронные Банковские гарантии");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/inner/all.png');
/*BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/common-image.jpg');*/
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Преимущества и условия</a></li>
			<li><a href="#content-tabs-2">Как получить </a></li>
			<li><a href="#content-tabs-3">Калькулятор</a></li>
			<li><a href="#content-tabs-4">Верификация гарантий </a></li>
			<li><a href="#content-tabs-5">Партнерам</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
<a href="#" class="button mt-3" data-yourapplication="" data-classapplication=".wr_form_application" data-formrapplication=".content_rates_tabs" data-formrapplicationindex="0">Заполнить заявку</a>
					</div>
					<div class="block_type_center">
						<h1>Преимущества нашего продукта:</h1>
						<ul class="big_list">
                                                        <li>Гарантии по 44-ФЗ; 223-ФЗ; 185-ФЗ</li>
							<li>Не требуется открытие расчетного счета, залог, поручительство </li>
							<li>Принятие решения за 1 час </li>
							<li>Минимальный пакет документов </li>
							<li>Дистанционная выдача </li>
							<li>Внесение банковской гарантии в реестр </li>
							<li>Возможность работы с крупнейшими Заказчиками по 223-ФЗ (Татнефть, Транснефть, РЖД, Росатом и т.д.) </li>
							<li>Предварительный расчет комиссии до подачи заявки </li>
							<li>Строгое соблюдение 44 ФЗ и 223 ФЗ </li>
							<li>Электронный документооборот </li>
							<li>Стоимость банковской гарантии от 2 990 руб. </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="wr_max_block">
							<h2>Контактное лицо</h2>
							<div class="form_application_line">
								<div class="contacts_block">
									 Фролов Сергей Викторович <br>
									 +7 495 937 07 37 доб. 33-25
								</div>
							</div>
							<div class="doc_list">
 <a href="/upload/medialibrary/cf4/conditions_bank_guarantees.pdf" target="_blank" class="doc_item">
								<div class="doc_pict pdf">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Общие условия Соглашения о предоставлении банковских гарантий в рамках кредитного продукта «Электронная банковская гарантия»
									</div>
									 <!--div class="doc_note">
                                    323.28 Кб                                </div-->
								</div>
 </a>
							</div>
						</div>
					</div>
					<div class="block_type_center">
						<h1>Мы предоставляем продукт на следующих условиях: </h1>
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Форма предоставления банковской гарантии
							</td>
							<td>
								<ul>
									<li>Электронная форма</li>
									<li>На бумажном носителе</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Валюта банковской гарантии
							</td>
							<td>
								 Рубли
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок банковской гарантии
							</td>
							<td>
								 До 1129 дней
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Максимальная сумма электронной банковской гарантии
							</td>
							<td>
								 60 000 000 руб.
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Процентная ставка
							</td>
							<td>
								 Рассчитать стоимость гарантии можно в разделе «Предварительный расчет стоимости»
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Электронная банковская гарантия обеспечивает:
							</td>
							<td>
								<ul>
									<li>исполнение обязательств по контракту</li>
									<li>возврат аванса</li>
									<li>исполнение гарантийных обязательств</li>
									<li>участие в закупке</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок рассмотрения банковской гарантии
							</td>
							<td>
								<ul>
									<li>Принятие решения в течении 1 часа.</li>
									<li>Выдача банковской гарантии в течение 1 дня.</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Наличие расчетного счета, залог, поручительство
							</td>
							<td>
								<ul>
									<li>Без залога и поручительства при сумме банковской гарантии до 15 млн. руб.</li>
									<li>Без открытия расчетного счета при сумме банковской гарантии до 15 млн руб</li>
									<li>Свыше 15 млн. рублей индивидуальное рассмотрение</li>
								</ul>
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="wr_max_block">
							<h2>Контактное лицо</h2>
							<div class="form_application_line">
								<div class="contacts_block">
									 Фролов Сергей Викторович <br>
									 +7 495 937 07 37 доб. 33-25
								</div>
							</div>
						</div>
					</div>
					<div class="block_type_center">
						<h1>Для получения электронной гарантии необходимо</h1>
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									 1
								</div>
								<div class="step_body">
									<div class="step_title">
										 Заполнить анкету-заявку в Excel формате
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 2
								</div>
								<div class="step_body">
									<div class="step_title">
										 Направить на почту <a href="mailto:bg_sme@zenit.ru">bg_sme@zenit.ru</a>
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 3
								</div>
								<div class="step_body">
									<div class="step_title">
										 Согласовать проект банковской гарантии и соглашение
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 4
								</div>
								<div class="step_body">
									<div class="step_title">
										 Оплатить комиссию
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 5
								</div>
								<div class="step_body">
									<div class="step_title">
										 Получить скан банковской гарантии по электронной почте
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 6
								</div>
								<div class="step_body">
									<div class="step_title">
										 Получить оригинал гарантии службой экспресс-доставки
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Рассчитайте стоимость для вас:</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="wr_max_block">
							<h2>Контактное лицо</h2>
							<div class="form_application_line">
								<div class="contacts_block">
									 Лунина Светлана Юрьевна <br>
									 +7 495 937 07 37 доб. 34-35
								</div>
							</div>
						</div>
					</div>
					<div class="block_type_center">
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-5">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						<div class="wr_max_block">
							<h2>Контактное лицо</h2>
							<div class="form_application_line">
								<div class="contacts_block">
									 Фролов Сергей Викторович <br>
									 +7 495 937 07 37 доб. 33-25
								</div>
							</div>
						</div>
					</div>
					<div class="block_type_center">
						<h1>Приглашаем к сотрудничеству агентов</h1>
						<div class="step_block">
							<h2>Для заключения Агентского договора нужно:</h2>
							<div class="step_item">
								<div class="step_num">
									 1
								</div>
								<div class="step_body">
									<div class="step_title">
										 оставить заявку на сайте
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 2
								</div>
								<div class="step_body">
									<div class="step_title">
										 дождаться звонка сотрудника Банка и получить необходимые ответы
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 3
								</div>
								<div class="step_body">
									<div class="step_title">
										 согласовать условия сотрудничества
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 4
								</div>
								<div class="step_body">
									<div class="step_title">
										 ознакомиться с тарифами
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 5
								</div>
								<div class="step_body">
									<div class="step_title">
										 получить необходимые инструкции
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 6
								</div>
								<div class="step_body">
									<div class="step_title">
										 подписать агентский договор
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
    <div class="wr_block_type">
<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal_inn", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 2 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Заявка на эл. Банковскую гарантию",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "18",
		"COMPONENT_TEMPLATE" => "universal_inn",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>