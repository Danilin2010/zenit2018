<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Агентская сеть");
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Стать агентом</a></li>
			<li><a href="#content-tabs-2">Типовые формы соглашений</a></li>
			<li><a href="#content-tabs-3">Список документов</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
						 <!--<div class="form_application_line">
 							<a class="button" href="#" data-yourapplication="" data-classapplication=".wr_form_application">Оставить заявку</a>
						</div>-->
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Банк ЗЕНИТ расширяет свою агентскую сеть и приглашает к сотрудничеству риэлтерские компании, компании – застройщики, консалтинговые компании, фонды поддержки малого бизнеса и другие организации и физические лица, заинтересованные во взаимовыгодном сотрудничестве. Достаточно заключить агентский договор, в соответствии с которым Банк будет выплачивать вознаграждение за оказываемые агентом услуги.
							</p>
							<h3>Важные преимущества сотрудничества с Банком ЗЕНИТ</h3>
							<ul class="big_list">
								<li>Партнерство с одним из самых надежных банков страны&nbsp;</li>
								<li>Неограниченный доход в виде агентского вознаграждения&nbsp;</li>
								<li>Индивидуальный подход к клиентам&nbsp;</li>
								<li>Личный менеджер&nbsp;</li>
								<li>Дополнительный высококачественный сервис для ваших клиентов&nbsp;</li>
								<li>Оперативное принятие решений по продуктам Банка&nbsp;</li>
								<li>Своевременные выплаты агентского вознаграждения </li>
							</ul>
						</div>
					</div>
					<?$APPLICATION->IncludeComponent(
						"bitrix:form.result.new",
						"agent",
						Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"CHAIN_ITEM_LINK" => "",
							"CHAIN_ITEM_TEXT" => "",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_URL" => "result_edit.php",
							"IGNORE_CUSTOM_TEMPLATE" => "N",
							"LIST_URL" => "result_list.php",
							"RIGHT_TEXT" => "",
							"SEF_MODE" => "N",
							"SOURCE_TREATMENT" => "",
							"SUCCESS_URL" => "",
							"USE_EXTENDED_ERRORS" => "N",
							"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
							"WEB_FORM_ID" => "7"
						)
					);?>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<h3>По кредитным продуктам</h3>
							<div class="doc_list">
								<a href="/upload/docs/agency/loans_agreement_ur.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для юридических лиц
										</div>
									</div>
								</a>
							</div>
							<br>
							<div class="doc_list">
								<a href="/upload/docs/agency/loans_agreement_ip.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для индивидуальных предпринимателей
										</div>
									</div>
								</a>
							</div>
							<br>
							<div class="doc_list">
								<a href="/upload/docs/agency/loans_agreement_fiz.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для физических лиц
										</div>
									</div>
								</a>
							</div>
							<br>
							<h3>По расчетно-кассовому обслуживанию</h3>
							<div class="doc_list">
								<a href="/upload/docs/agency/rko_agreement_ur.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для юридических лиц
										</div>
									</div>
								</a>
							</div>
							<br>
							<div class="doc_list">
								<a href="/upload/docs/agency/rko_agreement_ip.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для индивидуальных предпринимателей
										</div>
									</div>
								</a>
							</div>
							<br>
							<div class="doc_list">
								<a href="/upload/docs/agency/rko_agreement_fiz.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для физических лиц
										</div>
									</div>
								</a>
							</div>
							<br>
							<h3>По электронным банковским гарантиям</h3>
							<div class="doc_list">
								<a href="/upload/docs/agency/ebg_agreement_ur.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для юридических лиц
										</div>
									</div>
								</a>
							</div>
							<br>
							<div class="doc_list">
								<a href="/upload/docs/agency/ebg_agreement_ip.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для индивидуальных предпринимателей
										</div>
									</div>
								</a>
							</div>
							<br>
							<div class="doc_list">
								<a href="/upload/docs/agency/ebg_agreement_fiz.docx" target="_blank">
									<div class="doc_pict doc">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Агентский договор
										</div>
										<div class="doc_note">
											для физических лиц
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<div class="doc_list">
								<a href="/upload/docs/agency/ebg_agreement_ip.docx" target="_blank">
									<div class="doc_pict pdf">
									</div>
									<div class="doc_body">
										<div class="doc_text">
											Перечень юридических/правоустанавливающих документов
										</div>
										<div class="doc_note">
										</div>
									</div>
								</a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>