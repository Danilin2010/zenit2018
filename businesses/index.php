<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Малому и среднему бизнесу");
?>
    <!--main_slider-->
<?$APPLICATION->IncludeComponent("bitrix:news.list", "main_slider", Array(
    "COMPONENT_TEMPLATE" => ".default",
    "IBLOCK_TYPE" => "banners",
    "IBLOCK_ID" => "2",
    "NEWS_COUNT" => "20",
    "SORT_BY1" => "SORT",
    "SORT_ORDER1" => "ASC",
    "SORT_BY2" => "NAME",
    "SORT_ORDER2" => "ASC",
    "FILTER_NAME" => "",
    // Фильтр
    "FIELD_CODE" => array(    // Поля
        0 => "DETAIL_TEXT",
        1 => "",
    ),
    "PROPERTY_CODE" => array(    // Свойства
        0 => "PERCENT",
        1 => "LINK",
        2 => "SUFFIX",
        3 => "",
    ),
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "PREVIEW_TRUNCATE_LEN" => "",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "SET_TITLE" => "N",
    "SET_BROWSER_TITLE" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_LAST_MODIFIED" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "148",
    "PARENT_SECTION_CODE" => "",
    "INCLUDE_SUBSECTIONS" => "N",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "PAGER_TEMPLATE" => ".default",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "SET_STATUS_404" => "N",
    "SHOW_404" => "N",
    "MESSAGE_404" => "",
), false); ?>

    <!--main_tile-->
<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/template/businesses_tile.php",
    Array(),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>
    <!--main_tile-->
    <!--footer_banner-->
<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/template/businesses_banner.php",
    Array(),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>
    <!--footer_banner-->

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>