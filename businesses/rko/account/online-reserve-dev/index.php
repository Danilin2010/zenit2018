<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Онлайн-резервирование номера счета");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/online-reserve/fon.png');
?>
    <div class="wr_block_type">
        <div class="block_type to_column c-container">
            <h1>Как проходит резервирование</h1>
            <div class="block_type_right">
                <? $APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH . "/inc/block/rates.php",
                    Array(
                        "template" => "rates_right",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "N",
                        "show_blanks" => "N",
                    ),
                    Array("MODE" => "txt", "SHOW_BORDER" => false)
                ); ?>
            </div>
            <div class="block_type_center">
                <div class="text_block">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "promo_benefits",
                        array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "64",
                            "IBLOCK_TYPE" => "rko",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "100",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "132",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "SORT",
                            "SORT_BY2" => "NAME",
                            "SORT_ORDER1" => "ASC",
                            "SORT_ORDER2" => "ASC",
                            "STRICT_SECTION_CHECK" => "N",
                            "COMPONENT_TEMPLATE" => "promo_benefits",
                            "SHOW_TITLE" => "N",
                            "BIG_ELEMENT" => "Y"
                        ),
                        false
                    ); ?>
                </div>

                <?
                $APPLICATION->IncludeComponent("aic.bz:reserve", "", Array(
                    "CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "CACHE_TYPE" => "A",    // Тип кеширования
                ),
                    false
                );

                /* $APPLICATION->IncludeComponent("bitrix:form.result.new", "reserve", Array(
                     "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                     "CACHE_TYPE" => "A",	// Тип кеширования
                     "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                     "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                     "EDIT_URL" => "result_edit.php",	// Страница редактирования результата
                     "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                     "LIST_URL" => "result_list.php",	// Страница со списком результатов
                     "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                     "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                     "USE_EXTENDED_ERRORS" => "N",	// Использовать расширенный вывод сообщений об ошибках
                     "WEB_FORM_ID" => "3",	// ID веб-формы
                     "COMPONENT_TEMPLATE" => ".default",
                     "VARIABLE_ALIASES" => array(
                         "WEB_FORM_ID" => "WEB_FORM_ID",
                         "RESULT_ID" => "RESULT_ID",
                     )
                 ),
                     false
                 );
                 */ ?>
            </div>
        </div>

    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>