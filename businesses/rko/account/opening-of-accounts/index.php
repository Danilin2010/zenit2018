<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Открытие счетов");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/rko.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Как открыть<br>
 </a></li>
			<li><a href="#content-tabs-2">Документы</a></li>
			<li><a href="#content-tabs-3">Договоры</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							 <?php if (false): ?>/*закрываем код ниже*/ <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"simple_benefits",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "warning",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "64",
		"IBLOCK_TYPE" => "rko",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "134",
		"PARENT_SECTION_CODE" => " ",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "NAME",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>/*выше закрытый код*/ <?php endif; ?> <!--/*добавляем наш код*/-->
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 1
									</div>
									<div class="step_body">
										<div class="step_title">
											 Подготовьте пакет документов
										</div>
										<div class="step_text">
											 Все документы должны быть действительными и достоверными на дату их предъявления. Также предлагаем Вам ознакомиться с <a href="/businesses/rko/account/opening-of-accounts/#content-tabs-3" target="_blank">Комплексным договором и приложениями</a> к нему.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 2
									</div>
									<div class="step_body">
										<div class="step_title">
											 Ознакомьтесь с тарифами
										</div>
										<div class="step_text">
											 Выберите подходящий тарифный план или <a href="#tarif">воспользуйтесь стандартными тарифами</a>. <!--С любыми возникшими вопросами Вы можете обратиться к нашим менеджерам по телефону +7(495)777-57-04.-->
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 3
									</div>
									<div class="step_body">
										<div class="step_title">
											 Передайте пакет документов в <a href="/offices/" target="_blank">отделение Банка</a>
										</div>
										<div class="step_text">
											 Сотрудник отделения ответит на все возникшие вопросы и примет пакет документов. Предварительно Вы можете <a href="#modal_online-reserv" class="open_modal">зарезервировать номер счета</a>, чтобы указать его реквизиты в необходимых документах (договора, счета и т.д.).
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 4
									</div>
									<div class="step_body">
										<div class="step_title">
											 Счет открыт
										</div>
										<div class="step_text">
											 Менеджер банка свяжется с Вами и пригласит в отделение для подписания Заявления о присоединении к Правилам комплексного банковского обслуживания и Заявления на подключение всех необходимых продуктов/услуг.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 5
									</div>
									<div class="step_body">
										<div class="step_title">
											 Подключение системы ДБО Клиент-Банк
										</div>
										<div class="step_text">
											 На своем компьютере Вы, следуя инструкции, генерируете сертификат и предоставляете его в отделение Банка в распечатанном виде. После активации системы ДБО Клиент-Банк Вы сможете использовать все преимущества удаленного доступа к продуктам Банка.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<div class="doc_list">
 <a href="/media/doc/business/rko/accounts_docs_firms_20171218.rar" target="_blank">
								<div class="doc_pict rar">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Юридические лица - резиденты и нерезиденты
									</div>
									<div class="doc_note">
									</div>
								</div>
 </a>
							</div>
 <br>
							<div class="doc_list">
 <a href="/media/doc/business/rko/accounts_docs_ip_20171218.rar" target="_blank">
								<div class="doc_pict rar">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Индивидуальные предприниматели
									</div>
									<div class="doc_note">
									</div>
								</div>
 </a>
							</div>
 <br>
							<div class="doc_list">
 <a href="/media/doc/business/rko/accounts_docs_private_practice_20171218.rar" target="_blank">
								<div class="doc_pict rar">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Физические лица, занимающиеся частной практикой в установленном законодательством РФ порядке
									</div>
									<div class="doc_note">
									</div>
								</div>
 </a>
							</div>
 <br>
						</div>
						<div class="text_block">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Перечень документов</h1>
						<div class="text_block">
							<div class="doc_list">
 <a href="/media/doc/business/rko/complex_contract_20171218.pdf" target="_blank">
								<div class="doc_pict pdf">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Комплексный договор
									</div>
									<div class="doc_note">
									</div>
								</div>
 </a>
							</div>
 <br>
							<div class="doc_list">
 <a href="/media/doc/business/rko/Application_to_complex_contract_20171218.rar" target="_blank">
								<div class="doc_pict pdf">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Приложения к Комплексному договору
									</div>
									<div class="doc_note">
									</div>
								</div>
 </a>
							</div>
							 <!--br>

						<div class="doc_list">
							<a href="/upload/medialibrary/32a/dbo_contract_moscow.pdf" target="_blank">
								<div class="doc_pict pdf">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Договор оказания услуг дистанционного банковского обслуживания для Москвы и Московской области
									</div>
									<div class="doc_note">
									</div>
								</div>
							</a>
						</div>
 <br>
						<div class="doc_list">
							<a href="/upload/medialibrary/b77/dbo_contract_regions.pdf" target="_blank">
								<div class="doc_pict pdf">
								</div>
								<div class="doc_body">
									<div class="doc_text">
										 Договор оказания услуг дистанционного банковского обслуживания для региональных филиалов
									</div>
									<div class="doc_note">
									</div>
								</div>
							</a>
						</div-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<a name="tarif"></a>
<?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/rates.php",
                    Array(
                        "template"=>"rates",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "Y",
                        "show_blanks" => "N",
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>

</div>
<?$APPLICATION->IncludeFile("/local/templates/bz/inc/page-clone/online-reservations-account/index.php", Array(), Array(
    "MODE"      => "html",                                           // будет редактировать в веб-редакторе
    "NAME"      => "Редактирование включаемой области раздела",      // текст всплывающей подсказки на иконке
    "TEMPLATE"  => "section_include_template.php"                    // имя шаблона для нового файла
    ));
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>