<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Торговый эквайринг");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/rko.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Наши преимущества</a></li>
			<li><a href="#content-tabs-2">Как оформить</a></li>
			<li><a href="#content-tabs-3">Документы</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type c-container">
					 <?php if (false): ?>/*закрываем код ниже*/ <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"simple_benefits",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "warning",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "64",
		"IBLOCK_TYPE" => "rko",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "134",
		"PARENT_SECTION_CODE" => " ",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "NAME",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?>/*закрытый код*/ <?php endif; ?> <!--/*добовляем наш код*/-->
					<ul class="big_list">
						<li>бесконтактные платежи: Apple Pay, Samsung Pay, Android Pay, VISA PayWave и MasterCard PayPass</li>
						<li>привлечение новых платежеспособных клиентов</li>
						<li>участие в дисконтных программах</li>
						<li>увеличение объема продаж на 20—30%</li>
					</ul>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Преимущества эквайринга Банка ЗЕНИТ</h1>
 <img width="640" alt="logo_pay_3.png" src="/upload/medialibrary/e2f/logo_pay_3.png" height="50" title="logo_pay_3.png"><br>
 <br>
						<ul class="big_list">
							<li>Прием карт самых популярных платежных систем - Visa, MasterCard, UnionPay, Мир, American Express.</li>
							<li>
							Бесплатная установка любых типов терминалов, как стационарных, так и переносных (GРRS, Wi-Fi), интеграция с кассовым ПО; </li>
							<li>
							Обслуживание карт по технологии PayPass и payWave (оплата в одно касание); </li>
							<li>
							Минимальная комиссия за организацию расчетов и сжатые сроки перечисления денежных средств, отсутствие каких-либо дополнительных затрат; </li>
							<li>
							Зачисление денежных средств на р/счет в любой банк; </li>
							<li>
							Собственный процессинговый центр мирового стандарта; </li>
							<li>
							Личный менеджер с опытом решения задач; </li>
							<li>
							Бесплатное обучение персонала; </li>
							<li>
							Круглосуточная служба поддержки клиентов; </li>
							<li>
							Банк бесплатно предоставляет удобный детализированный отчет по операциям с использованием банковских карт; </li>
							<li>
							Возмещение сумм операций оплаты с использованием карт происходит в кратчайшие сроки (от 1 дня); </li>
							<li>
							Участие в программе лояльности </li>
							<li>
							Ставка на Торговый эквайринг предоставляется от 1,5%. </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<div>
								 <!--<div style="padding-left: 35px;">-->
								<h2 style="line-height: initial;">Мы ценим время клиента, поэтому процесс заключения договора на эквайринг максимально упрощен и состоит из таких этапов: </h2>
							</div>
							<div class="step_block">
								<div class="step_item" id="">
									<div class="step_num">
										 1
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											 Необходимо подать <a href="#" data-yourapplication="" data-classapplication=".wr_form_application">онлайн-заявку</a> на сайте или позвонить в Банк ЗЕНИТ менеджеру по указанным контактам или заполнить «Заявление о предоставлении Услуги «Торговый эквайринг» и выслать менеджеру.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 2
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											 Менеджер банка связывается с клиентом, обсуждает модель пос-терминала для установки и приезжает для заключения договора.
										</div>
									</div>
								</div>
								<div class="step_item" id="">
									<div class="step_num">
										 3
									</div>
									<div class="step_body">
										<div class="step_title">
										</div>
										<div class="step_text">
											 После заключения договора Банк ЗЕНИТ доставляет и настраивает пос-терминал, проводит обучение по работе с пос-терминалом и оставляет инструкцию.
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="text_block">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
		<div class="doc_list">
 			<a href="/upload/medialibrary/bbf/acquiring_assert_20171213.doc.pdf" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						 Заявление о предоставлении Услуги «Торговый эквайринг»
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div>
		 <br>
		<div class="doc_list">
 			<a href="/upload/medialibrary/84a/acquiring_corp_20171213.pdf" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						 Правила оказания ПАО Банк ЗЕНИТ юридическим лицам и индивидуальным предпринимателям Услуги «Торговый эквайринг»
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div><!--br>
		<div class="doc_list">
 			<a href="/upload/medialibrary/84a/acquiring_corp_20180115.pdf" target="_blank">
				<div class="doc_pict pdf">
				</div>
				<div class="doc_body">
					<div class="doc_text">
						 Правила, вступают в действие с 15.01.2018г.
					</div>
					<div class="doc_note">
					</div>
				</div>
 			</a>
		</div-->
					</div>
					<div class="block_type_center">
						<h1>Перечень документов</h1>
						<div class="text_block">
							<h2>Для юридических лиц:</h2>
							<ul class="big_list">
								<li>
								Учредительный документ юридического лица со всеми изменениями (копия, заверенная нотариально, либо органом, осуществившим государственную регистрацию); </li>
								<li>
								Копии документов (протоколов, приказов, решений), подтверждающих избрание/назначение единоличного исполнительного органа юридического лица, а также документов, подтверждающих полномочия лица, подписавшего договор (в случае подписания договора не руководителем), заверенные нотариально;</li>
								<li>
								Копии лицензий (разрешений), выданных юридическому лицу в установленном законодательством Российской Федерации порядке на право осуществления деятельности, подлежащей лицензированию, заверенные нотариально;</li>
								<li>
								Копия документа, удостоверяющего личность единоличного исполнительного органа, уполномоченного лица (в случае подписания договора не руководителем), заверенная нотариально;</li>
								<li>
								Договор(ы) аренды/субаренды помещения, занимаемого юридическим лицом, либо свидетельство(а) о государственной регистрации права собственности юридического лица на занимаемое помещение (при наличии), в котором(ых) Банк устанавливает ЭТ (копия(и), заверенная нотариально);</li>
								<li>
								Опросная Анкета юридического лица (на бланке Банка), подписанная руководителем (уполномоченным лицом, действующим на основании доверенности, и главным бухгалтером (при отсутствии в штате юридического лица главного бухгалтера – только руководителем), скрепленная печатью юридического лица.</li>
								<!--li>
								Договор аренды/субаренды помещения, в котором Банком устанавливается оборудование; </li-->
							</ul>
						</div>
						<div class="text_block">
							<h2>Для индивидуального предпринимателя:</h2>
							<ul class="big_list">
								<li>
								Копия документа, удостоверяющего личность индивидуального предпринимателя, заверенная нотариально;</li>
								<li>
								Нотариально удостоверенная доверенность, выданная уполномоченному лицу, и копия документа, удостоверяющего личность уполномоченного лица, заверенная нотариально (в случае подписания договора уполномоченным лицом);</li>
								<li>
								Копии лицензий (патентов), выданных индивидуальному предпринимателю на право осуществления деятельности, подлежащей лицензированию (регулированию путем выдачи патента), заверенные нотариально;</li>
								<li>
								Договор(ы) аренды/субаренды помещения, занимаемого индивидуальным предпринимателем, либо свидетельство(а) о государственной регистрации права собственности индивидуального предпринимателя на недвижимое имущество (занимаемое помещение) (при наличии), в котором(ых) Банк устанавливает ЭТ (копия(и), заверенная нотариально);</li>
								<li>
								Опросная Анкета индивидуального предпринимателя (на бланке Банка), подписанная индивидуальным предпринимателем (иным уполномоченным лицом, действующим на основании надлежащим образом оформленной доверенности).</li>
							</ul>
						</div>
						<div class="text_block">
							<h2>Для физического лица, занимающегося в установленном законодательством Российской Федерации порядке частной практикой:</h2>
							<ul class="big_list">
								<li>
								Копия документа, удостоверяющего личность физического лица, занимающегося в установленном законодательством РФ порядке частной практикой, заверенная нотариально;</li>
								<li>
								Нотариально удостоверенная доверенность, выданная уполномоченному лицу (в случае подписания договора уполномоченным лицом) и копия документа, удостоверяющего личность уполномоченного лица, заверенная нотариально;</li>
								<li>
								Свидетельство о постановке физического лица, занимающегося в установленном законодательством РФ порядке частной практикой, на учет в налоговом органе;</li>
								<li>
								Документ, подтверждающий наделение нотариуса полномочиями (назначение на должность), выдаваемый органами юстиции субъектов Российской Федерации, в соответствии с законодательством Российской Федерации (предоставляет Клиент - нотариус);</li>
								<li>
								Документ, удостоверяющий регистрацию адвоката в реестре адвокатов, а также документ, подтверждающий учреждение адвокатского кабинета, заверенные нотариально (предоставляет Клиент - адвокат);</li>
								<li>
								Договор(ы) аренды/субаренды помещения, занимаемого физическим лицом, занимающемся в установленном законодательством РФ порядке частной практикой, либо свидетельство(а) о государственной регистрации права собственности физического лица, занимающегося в установленном законодательством РФ порядке частной практикой, на недвижимое имущество (занимаемое помещение) (при наличии), в котором(ых) Банк устанавливает ЭТ (копия(и), заверенная нотариально).</li>
								<li>
								Опросная Анкета физического лица, занимающего в установленном законодательством РФ порядке частной практикой (на бланке Банка), подписанная физическим лицом, занимающимся в установленном законодательством РФ порядке частной практикой (иным уполномоченным лицом, действующим на основании надлежащим образом оформленной доверенности).</li>
							</ul>
						</div>
 <br>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"universal", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Торговый эквайринг",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"WEB_FORM_ID" => "1",
		"COMPONENT_TEMPLATE" => "universal",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>