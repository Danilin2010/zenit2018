<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Безналичные переводы в рублях");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/rko.png');
?>


    <div class="wr_block_type">
        <!--content_tab-->
        <div class="content_rates_tabs">
            <ul class="content_tab c-container">
                <li><a href="#content-tabs-1">Описание</a></li>
                <li><a href="#content-tabs-2">Операционное время</a></li>
                <li><a href="#content-tabs-3">Дополнительная информация</a></li>
            </ul>
            <div class="content_body" id="content-tabs-1">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <h1>Проведение платежей</h1>
                            <!--conditions-->
<!-- закрыли вывод таблицы тарифа-->
							<!--/* <div class="conditions">
                                <div class="conditions_item">
                                    <div class="conditions_item_title">
<?/* $APPLICATION->IncludeFile(
                                            "/businesses/rko/transfers/rub/files/conditions_item_title_1.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
										);?>
                                    </div>
                                    <div class="conditions_item_text">
                                        <?$APPLICATION->IncludeFile(
                                            "/businesses/rko/transfers/rub/files/conditions_item_text_1.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
                                        );?>
                                    </div>
                                </div><div class="conditions_item">
                                    <div class="conditions_item_title">
                                        <?$APPLICATION->IncludeFile(
                                            "/businesses/rko/transfers/rub/files/conditions_item_title_2.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
                                        );?>
                                    </div>
                                    <div class="conditions_item_text">
                                        <?$APPLICATION->IncludeFile(
                                            "/businesses/rko/transfers/rub/files/conditions_item_text_2.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
                                        );?>
                                    </div>
                                </div><div class="conditions_item">
                                    <div class="conditions_item_title">
                                        <?$APPLICATION->IncludeFile(
                                            "/businesses/rko/transfers/rub/files/conditions_item_title_3.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
                                        );?>
                                    </div>
                                    <div class="conditions_item_text">
                                        <?$APPLICATION->IncludeFile(
                                            "/businesses/rko/transfers/rub/files/conditions_item_text_3.php",
                                            Array(),
                                            Array("MODE"=>"txt","SHOW_BORDER"=>true)
);*/?>
                                    </div>
                                </div>
                            </div>-->
                            <!--conditions-->
                            <div class="text_block">
                                <?$APPLICATION->IncludeFile(
                                    "/businesses/rko/transfers/rub/files/big_list.php",
                                    Array(),
                                    Array("MODE"=>"txt","SHOW_BORDER"=>true)
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_body" id="content-tabs-2">
                <div class="wr_block_type">
                    <div class="block_type c-container">
                        <div class="block_type_center">
                            <!--table-->
							<?/*$APPLICATION->IncludeFile(
                                "/businesses/rko/transfers/rub/files/table.php",
                                Array(),
                                Array("MODE"=>"txt","SHOW_BORDER"=>true)
);*/?>
             <?$GLOBALS["arrFilter"] = array(
                    "PROPERTY_region" => GetSityID(),
                );?> 
                <?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "operational_time",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(0=>"",1=>"",),
        "FILTER_NAME" => "arrFilter",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "76",
        "IBLOCK_TYPE" => "rko_main",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "1",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0=>"blank_verification",1=>"copy_verification",2=>"tariffs_table",3=>"currency_account",4=>"external_transfer",5=>"currency_control_discount",6=>"region",7=>"operation_time",8=>"",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "STRICT_SECTION_CHECK" => "N"
    )
);?>

                            <!--table-->

                            <?$APPLICATION->IncludeFile(
                                "/businesses/rko/transfers/rub/files/note.php",
                                Array(),
                                Array("MODE"=>"txt","SHOW_BORDER"=>true)
                            );?>



                        </div>
                    </div>
                </div>
            </div>
            <div class="content_body" id="content-tabs-3">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <?$APPLICATION->IncludeComponent("bitrix:news.list", "warning", Array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "62",	// Код информационного блока
		"IBLOCK_TYPE" => "rko",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "100",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "28",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "TYPE_ELEMENT",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
		"SORT_BY2" => "NAME",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
                        </div>
                    </div>
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <?$APPLICATION->IncludeComponent("bitrix:news.list", "warning", Array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                "AJAX_MODE" => "N",	// Включить режим AJAX
                                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                "CACHE_TIME" => "36000",	// Время кеширования (сек.)
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                "DISPLAY_DATE" => "Y",	// Выводить дату элемента
                                "DISPLAY_NAME" => "Y",	// Выводить название элемента
                                "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
                                "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
                                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                "FIELD_CODE" => array(	// Поля
                                    0 => "",
                                    1 => "",
                                ),
                                "FILTER_NAME" => "",	// Фильтр
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                "IBLOCK_ID" => "62",	// Код информационного блока
                                "IBLOCK_TYPE" => "rko",	// Тип информационного блока (используется только для проверки)
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                                "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
                                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                                "NEWS_COUNT" => "100",	// Количество новостей на странице
                                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                "PAGER_TITLE" => "Новости",	// Название категорий
                                "PARENT_SECTION" => "29",	// ID раздела
                                "PARENT_SECTION_CODE" => "",	// Код раздела
                                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                "PROPERTY_CODE" => array(	// Свойства
                                    0 => "TYPE_ELEMENT",
                                    1 => "",
                                ),
                                "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                                "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                                "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                                "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                "SHOW_404" => "N",	// Показ специальной страницы
                                "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
                                "SORT_BY2" => "NAME",	// Поле для второй сортировки новостей
                                "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
                                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
                                "COMPONENT_TEMPLATE" => ".default"
                            ),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--content_tab-->
    </div>


<?$APPLICATION->IncludeFile(
    SITE_TEMPLATE_PATH."/inc/block/rates.php",
    Array(
        "template"=>"rates",
        "all_phone" => "8 (800) 500-66-77",
        "use_clientbank" => "N",
        "show_blanks" => "N",
    ),
    Array("MODE"=>"txt","SHOW_BORDER"=>false)
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>