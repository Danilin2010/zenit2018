<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Зарплатные проекты");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/rko.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Описание</a></li>
			<li><a href="#content-tabs-2">Преимущества</a></li>
			<li><a href="#content-tabs-3">Как оформить </a></li>
			<li><a href="#content-tabs-4">Документы</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<div class="text_block">
							<p>
								 Организация перечисления зарплаты сотрудникам на&nbsp;зарплатные карты, открытые в&nbsp;Банке, позволит работодателю сократить накладные расходы, а&nbsp;сотрудникам предприятия получать заработную плату в&nbsp;любое удобное время в&nbsp;банкомате либо просто оплачивать банковской зарплатной картой свои покупки. Получение зарплаты через банк удобно и&nbsp;для работников, и&nbsp;для работодателей.
							</p>
							<h2>Простота</h2>
							<ul>
								<li>
								Для выплаты заработной платы достаточно перечислить общую сумму зарплаты одним платежным поручением и передать в Банк в электронном виде платежную ведомость, содержащую список сотрудников и сумму заработной платы на каждого из них. </li>
								<h2>Удобство&nbsp;</h2>
								<ul>
									<li>
									Зачисление зарплаты осуществляется без визита в офис Банка благодаря системе «Клиент-Банк». </li>
									<li>
									Выдача зарплаты осуществляется через бакноматы, работающие в круглосуточном режиме.</li>
									<li>
									Компании не нужно депонировать невыплаченные сотрудникам суммы заработной платы или возвращать полученные предприятием в Банке и неизрасходованные денежные средства.</li>
									<li>
									Отсутствие необходимости в инкассации и охране наличных средств.</li>
									<!--li>
									Банк ЗЕНИТ осуществляет доставку и установку Банкомата на территорию предприятия, обеспечивает его бесперебойную работу и обслуживание. </li-->
								</ul>
								<h2>Экономическая выгода&nbsp;</h2>
								<ul>
									<li>Отсутствие комиссий за подключение и обслуживание зарплатного проекта;</li>
									<li>Отсутствие расходов по доставке денежной наличности в кассу предприятия;</li>
									<li>Сокращение непроизводительных потерь рабочего времени сотрудников на получение заработной платы;</li>
									<li>Отсутствие расходов на содержание кассы предприятия и кассира;</li>
									<li>Бесплатный выпуск и обслуживание международных банковских карт;</li>
									<li>Бесплатная установка и обслуживание банкомата (оговаривается индивидуально).</li>
								</ul>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<ul class="big_list">
							<li>
							Выдача наличных денежных средств в рублях в сети банкоматов и пунктах выдачи наличных Банка ЗЕНИТ производится без взимания комиссий; </li>
							<li>
							При утере или краже карты денежные средства сохраняются на счете; </li>
							<li>
							Есть возможность открыть кредитную линию в размере до 90% от зачисляемых на банковский счет денежных средств; </li>
							<li>
							Можно оформить дополнительные карты к банковскому счету для членов семьи, родственников или уполномоченных лиц; </li>
							<li>
							К счетам выпускаются банковские карты платежной системы МИР и международные банковские карты платежных систем Visa International и MasterCard International со встроенным микропроцессором (стандарт EMV) всех категорий (карты с чипом высшей категории безопасности); </li>
							<li>
							Банк ЗЕНИТ регулярно предоставляет держателям своих карт информацию о движении и остатке денежных средств на банковских счетах по операциям с использованием Банковских карт. </li>
							<li>
							Кредитование сотрудников компании по индивидуальным условиям. </li>
							<li>
							Широкий спектр банковских продуктов (вкладов и сберегательных счетов) и индивидуальных инвестиционных решений. </li>
							<li>
							Бесплатное подключение мобильного и интернет-банка. </li>
							<li>
							Бесплатное финансовое образование (тематические семинары) на рабочем месте. </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						 <!--step_block-->
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									1
								</div>
								<div class="step_body">
									<div class="step_title">
										 заказать <a href="#">обратный звонок в Банке</a> или обратиться в удобное для Вас <a href="#">отделение Банка ЗЕНИТ</a>
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									2
								</div>
								<div class="step_body">
									<div class="step_title">
										 заполнить Заявление о предоставлении услуги "Зарплатный проект"
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									3
								</div>
								<div class="step_body">
									<div class="step_title">
										 предоставить запрашиваемый пакет документов
									</div>
								</div>
							</div>
						</div>
						 <!--step_block-->
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h2>Для заключения договора зарплатного проекта юридические лица и ИП предоставляют следующий пакет документов:</h2>
						<ul class="big_list">
							<li>
							Заявление по <a href="/upload/docs/rko/bank-cards/zproject_claim.pdf" target="_blank">форме Банка</a> </li>
							<li><a href="/upload/medialibrary/2dd/rules_salary_project.pdf" target="_blank">Правила</a> оказания услуги "Зарплатный проект"</li>
							<li>
							Учредительные документы и/или иные правоустанавливающие документы </li>
							<li>
							Штатное расписание </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>