<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кредит на оборудование и транспорт");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/credits.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Описание продукта, преимущества</a></li>
			<li><a href="#content-tabs-2">Условия кредитования</a></li>
			<li><a href="#content-tabs-3">Требования к заемщику</a></li>
			<li><a href="#content-tabs-4">Государственная поддержка</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Преимущества Банка ЗЕНИТ:</h1>
						<ul class="big_list">
							<li>
							Предмет залога - приобретаемое оборудование или транспорт либо иное движимое или недвижимое имущество; </li>
							<li>
							Возможность использования кредита для ремонта и/или модернизации имеющегося оборудования и транспорта; </li>
							<li>
							Доля собственного участия в проекте может быть заменена залогом имеющегося имущества; </li>
							<li>
							Расходы, связанные с оценкой залога - отсутствуют, </li>
							<li>
							Возможно принятие гарантий от региональных фондов поддержки МСБ </li>
							<li>
							Предусмотрены льготные тарифы за расчетно-кассовое обслуживание, эквайринг, зарплатный проект. </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1> Как получить кредит:</h1>
						 <!--step_block-->
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									 1
								</div>
								<div class="step_body">
									<div class="step_title">
										 Заполните заявку на сайте Банка и дождитесь звонка менеджера
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 2
								</div>
								<div class="step_body">
									<div class="step_title">
										 Пройдите кредитное интервью по телефону или договоритесь о встрече
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 3
								</div>
								<div class="step_body">
									<div class="step_title">
										 Принести документы в банк
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 4
								</div>
								<div class="step_body">
									<div class="step_title">
										 Подготовьте финансовые и юридические документы в соответствии со списком, который составит для Вас менеджер Банка по итогам переговоров
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 5
								</div>
								<div class="step_body">
									<div class="step_title">
										 После принятия решения на Кредитном Комитете Вас пригласят для открытия расчетного счета и подписания кредитно-обеспечительной документации
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
						</div>
						 <!--step_block-->
					</div>
				</div>
			</div>
			 <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/rates.php",
                    Array(
                        "template"=>"rates",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "Y",
                        "show_blanks" => "N",
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Условия кредитования</h1>
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Цели кредитования
							</td>
							<td>
								 Приобретение<a href="#s1" class="footnote" id="l1"><sup>1</sup></a> оборудования, транспорта, спецтехники отечественного и иностранного производства, а также ремонт, модернизация и подготовка к эксплуатации оборудования/транспорта/спецтехники, в том числе приобретаемого
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Форма предоставления кредита
							</td>
							<td>
								<ul>
									<li>Единовременная выдача</li>
									<li>Невозобновляемая кредитная линия</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Валюта кредита
							</td>
							<td>
								 Рубли
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок кредита
							</td>
							<td>
								 Не более 60 месяцев
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Максимальная сумма кредита
							</td>
							<td>
								 150&nbsp;000 000 руб.
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Процентная ставка
							</td>
							<td>
								 устанавливается индивидуально
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Первоначальный взнос (доля собственного участия в проекте) в случае приобретения оборудования / транспорта / спецтехники
							</td>
							<td>
								 Не менее 50% от стоимости приобретаемого оборудования / не менее 20% от стоимости приобретаемого транспорта / спецтехники<a href="#s2" class="footnote" id="l2"><sup>2</sup></a>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Порядок погашения основного долга и процентов
							</td>
							<td>
								<ul>
									<li>Аннуитетные /дифференцированные платежи (ежемесячно) </li>
									<li>По индивидуальному графику</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Отсрочка погашения основного долга
							</td>
							<td>
								 Не более 6 месяцев
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Досрочное погашение кредита
							</td>
							<td>
								 Запрещено в течение первых 6 месяцев действия кредита, начиная с 7-го месяца без ограничений
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Обеспечение
							</td>
							<td>
								 Залог приобретаемого оборудования, спецтехники или транспорта либо иного движимого или недвижимого имущества, находящегося в собственности залогодателя, а также поручительство юридических лиц – связанных компаний (при наличии таких компаний) и/или физических лиц - собственников (учредителей).<a href="#s3" class="footnote" id="l3"><sup>3</sup></a>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Страхование
							</td>
							<td>
								<ul>
									<li>
									Страхование риска утраты (гибели)/повреждения предмета залога в страховой компании, соответствующей требованиям Банка. В качестве выгодоприобретателя должен быть указан Банк. </li>
									<li>
									Для залога недвижимого имущества, находящегося в собственности залогодателя менее трех лет - страхование риска утраты права собственности (титула) в течение срока действия кредитного договора, но не более трех лет от даты возникновения права собственности залогодателя на объект недвижимости. </li>
									<li>
									При отсутствии поручительств третьих лиц должно быть оформлено страхование жизни учредителя заемщика в пользу Банка </li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок рассмотрения заявки
							</td>
							<td>
								<ul>
									<li>При залоге движимого имущества не более 5 рабочих дней. </li>
									<li>При залоге недвижимого имущества не более 10 рабочих дней.</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Наличие расчетного счета и поддержание оборотов
							</td>
							<td>
								<ul>
									<li>Открытие расчетного счета обязательно после принятия положительного решения. </li>
									<li>Поддержание ежемесячных кредитовых оборотов в размере не менее 30% от суммы кредита.</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Проверка кредитной истории
							</td>
							<td>
								 Согласие физических лиц - собственников (учредителей) бизнеса на получение информации из бюро кредитных историй
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Прочее
							</td>
							<td>
								 В случае заключения договора поручительства / договора залога с физическим лицом (собственником / учредителем бизнеса, залогодателем и/или третьим лицом) необходимо нотариально заверенное согласие супруги (а) на совершение сделки или брачный контракт
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
						<div class="note_text" id="s1">
 <a href="#l1" class="footnote"><sup>1</sup></a>&nbsp;Ограничения при кредитовании на приобретение транспорта и оборудования: Приобретение новых транспортных средств и спецтехники, а также транспортных средств и спецтехники с пробегом (только иностранного производства и возрастом не старше 3-х лет).&nbsp; Приобретение только нового оборудования отечественного и иностранного производства.
						</div>
						<div class="note_text" id="s2">
 <a href="#l2" class="footnote"><sup>2</sup></a>
							В случае финансирования 100% суммы сделки по приобретению оборудования/транспорта за счет кредитных средств необходимо предоставление дополнительного обеспечения.
						</div>
						<div class="note_text" id="s3">
 <a href="#l3" class="footnote"><sup>3</sup></a>&nbsp; 1. Рассматривается в индивидуальном порядке с окончательным утверждением списка поручителей коллегиальным органом Банка.<br>
							 2. Поручительство региональных Фондов содействия кредитованию малого бизнеса может быть принято в качестве частичного обеспечения.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Требования к заемщикам</h1>
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Фактический срок существования бизнеса
							</td>
							<td>
								 Не менее 12 месяцев
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Категория заемщиков&nbsp; / принципалов
							</td>
							<td>
								 Юридические лица, индивидуальные предприниматели
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Место регистрации
							</td>
							<td>
								 г. Москва, Московская область, г. Санкт-Петербург, Ленинградская область, регионы присутствия филиалов/операционных офисов Банка
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Состав учредителей / акционеров
							</td>
							<td>
								 Доля участия в уставном капитале государственных, общественных и религиозных организаций (объединений), благотворительных и иных фондов – не более 25 %
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Прочие требования
							</td>
							<td>
								 Руководители или акционеры данного предприятия ранее не были владельцами или руководили предприятиями, признанными банкротами или имевшими длительный период неплатежеспособности. Предприятие не имеет существенных налоговых нарушений или претензий со стороны государственных и правоохранительных органов.
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Финансовые результаты
							</td>
							<td>
								 Деятельность прибыльна (с учетом показателей бухгалтерской отчетности), либо Убыточная деятельность оценивается как несущественная (полученные убытки привели к снижению чистых активов менее чем на 25% от максимального значения за последние 4 отчетные даты).
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h2>Организации оказывающие государственную поддержку:</h2>
						<ul>
							<li> <a href="http://www.mosgarantfund.ru/" target="_blank">Фонд содействия кредитованию малого бизнеса Москвы</a><a href="http://fs-credit.mbm.ru/" target="_blank"></a> </li>
							<li> <a href="http://www.dmpmos.ru/" target="_blank">Департамент поддержки и развития малого и среднего предпринимательства города Москвы</a> </li>
							<li> <a href="http://gfppko.ru/pages/98.html" target="_blank">Государственный фонд поддержки предпринимательства Кемеровской области</a><a href="http://fs-credit.mbm.ru/" target="_blank"></a><a href="http://www.fpmp39.ru/services/garanty_fond">«Фонд поддержки предпринимательства» Калининградской области</a> </li>
							<li> <a href="http://www.gfso.ru/" target="_blank">Государственное унитарное предприятие Самарской области «Гарантийный фонд поддержки предпринимательства Самарской области»</a><a href="http://fs-credit.mbm.ru/" target="_blank">,</a><a href="http://fs-credit.mbm.ru/" target="_blank"></a> </li>
							<li> <a href="http://fondmsp.ru/" target="_blank">Фонд развития малого и среднего предпринимательства Новосибирской области</a><a href="http://fs-credit.mbm.ru/" target="_blank">,</a><a href="http://fs-credit.mbm.ru/" target="_blank"></a> </li>
							<li> <a href="http://www.fond-omsk.ru/" target="_blank">Омский региональный фонд поддержки и развития малого предпринимательства</a><a href="http://www.saratov.gov.ru/government/structure/mineconom/gf.php" target="_blank">,</a> </li>
							<li> <a href="http://www.sofp.ru/" target="_blank">Свердловский областной фонд поддержки малого предпринимательства</a><a href="http://fs-credit.mbm.ru/" target="_blank">,</a> </li>
							<li> <a href="http://aijk-ra.ru/index.php/garantijnyj-fond-respubliki-altaj" target="_blank">Агентство по ипотечному жилищному кредитованию Республики Алтай</a><a href="http://fs-credit.mbm.ru/" target="_blank">,</a> </li>
							<li> <a href="http://tida.tatarstan.ru/rus/info.php?id=386679" target="_blank">Гарантийный фонд Республики Татарстан</a><a href="http://fs-credit.mbm.ru/" target="_blank">,</a> </li>
							<li> <a href="http://sb-ugra.ru/" target="_blank">Фонд поддержки предпринимательства Югры</a><a href="http://fs-credit.mbm.ru/" target="_blank">,</a> </li>
							<li> <a href="http://credit-fond.ru/" target="_blank">Фонд содействия кредитованию малого и среднего бизнеса Санкт-Петербурга</a>, </li>
							<li> <a href="http://www.dongarant.ru/" target="_blank">Гарантийный фонд Ростовской области</a>, </li>
							<li> <a href="http://www.garantnn.ru/" target="_blank">Агентство по развитию системы гарантий для субъектов малого предпринимательства Нижегородской области</a>, </li>
							<li> <a href="http://www.gfskur.ru/" target="_blank">Гарантийный фонд содействия кредитованию малого и среднего предпринимательства Удмуртской Республики</a>, </li>
							<li> <a href="http://www.fond74.ru/" target="_blank">Фонд содействия кредитованию малого предпринимательства Челябинской области</a>. </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"universal",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Заявка на кредит на оборудование и транспорт.",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>