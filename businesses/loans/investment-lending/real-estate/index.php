<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Кредит на приобретение недвижимости");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/credits.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Описание продукта, преимущества</a></li>
			<li><a href="#content-tabs-2">Условия кредитования</a></li>
			<li><a href="#content-tabs-3">Требования к заемщику</a></li>
			<li><a href="#content-tabs-4">Партнеры Банка по Программе</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<h2>С помощью оборотного Кредита Предприятие может:</h2>
						<ul class="big_list">
							<li>Приобрести любую коммерческую недвижимость или земельный участок,</li>
							<li>Осуществить строительство, реконструкцию или ремонт.</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<h2>Преимущества Банка ЗЕНИТ:</h2>
						<ul class="big_list">
							<li>Длительные сроки кредитования,</li>
							<li>Возможность кредитования без первоначального взноса,</li>
							<li>Использование кредита на строительство, реконструкцию и ремонт,</li>
							<li>Отсутствие расходов, связанных с оценкой объекта,</li>
							<li>Возможность получения длительной отсрочки погашения основного долга.</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<h2>Как получить кредит:</h2>
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									 1
								</div>
								<div class="step_body">
									<div class="step_title">
										 Заполните заявку на сайте Банка и дождитесь звонка менеджера
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 2
								</div>
								<div class="step_body">
									<div class="step_title">
										 Пройдите кредитное интервью по телефону или договоритесь о встрече
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 3
								</div>
								<div class="step_body">
									<div class="step_title">
										 Подготовьте финансовые и юридические документы в соответствии со списком, который составит для Вас менеджер Банка по итогам переговоров
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									 4
								</div>
								<div class="step_body">
									<div class="step_title">
										 После принятия решения на Кредитном Комитете Вас пригласят для открытия расчетного счета и подписания кредитно-обеспечительной документации
									</div>
									<div class="step_text">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			 <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/rates.php",
                    Array(
                        "template"=>"rates",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "Y",
                        "show_blanks" => "N",
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Условия кредитования</h1>
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Цели кредитования
							</td>
							<td>
								 Приобретение недвижимости и земельных участков, а также ремонт, модернизация и подготовка к эксплуатации объектов недвижимости, в том числе приобретаемых
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Форма предоставления кредита
							</td>
							<td>
								<ul>
									<li>Единовременная выдача </li>
									<li>Невозобновляемая кредитная линия</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Валюта кредита
							</td>
							<td>
								 Рубли
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок кредита
							</td>
							<td>
								 Не более 120 месяцев
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Максимальная сумма кредита
							</td>
							<td>
								 150&nbsp;000 000 руб.
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Первоначальный взнос (доля собственного участия в проекте)
							</td>
							<td>
								 Не менее 30% от стоимости приобретаемого объекта недвижимости<a href="#s1" class="footnote" id="l1"><sup>1</sup></a>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Порядок погашения основного долга и процентов
							</td>
							<td>
								<ul>
									<li>Аннуитетные /дифференцированные платежи (ежемесячно) </li>
									<li>По индивидуальному графику</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Отсрочка погашения основного долга
							</td>
							<td>
								 Не более 12 месяцев
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Досрочное погашение кредита
							</td>
							<td>
								 Запрещено в течение первых 6 месяцев действия кредита, начиная с 7-го месяца без ограничений
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Обеспечение
							</td>
							<td>
								<ul>
									<li>Основное обеспечение – приобретаемый / имеющийся объект недвижимости; </li>
									<li>
									Дополнительное обеспечение: поручительство юридических лиц – связанных компаний (при наличии таких компаний) и физических лиц – собственников (учредителей).<a href="#s2" class="footnote" id="l2"><sup>2</sup></a> </li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Страхование:
							</td>
							<td>
								<ul>
									<li>риска утраты (гибели)/повреждения предмета залога; </li>
									<li>
									для залога недвижимого имущества, находящегося в собственности залогодателя менее трех лет - страхование риска утраты права собственности (титула) в течение срока действия кредитного договора, но не более трех лет от даты возникновения права собственности залогодателя на объект недвижимости </li>
									<li>
									При отсутствии поручительств третьих лиц должно быть оформлено страхование жизни учредителя заемщика в пользу Банка </li>
									<li>
									в страховой компании, соответствующей требованиям Банка. В качестве выгодоприобретателя&nbsp; должен быть указан Банк. </li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок рассмотрения заявки
							</td>
							<td>
								 Не более 10 рабочих дней.
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Наличие расчетного счета и поддержание оборотов
							</td>
							<td>
								<ul>
									<li>Открытие расчетного счета обязательно после принятия положительного решения. </li>
									<li>Поддержание ежемесячных кредитовых оборотов в размере не менее 10% от суммы кредита.</li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Проверка кредитной истории
							</td>
							<td>
								 Согласие физических лиц - собственников (учредителей) бизнеса на получение информации из бюро кредитных историй
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Прочее
							</td>
							<td>
								 В случае заключения договора поручительства / договора залога с физическим лицом (собственником / учредителем бизнеса, залогодателем и/или третьим лицом) необходимо нотариально заверенное согласие супруги (а) на совершение сделки или брачный контракт
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
						<div class="note_text" id="s1">
 <a href="#l1" class="footnote"><sup>1</sup></a>.В случае финансирования 100% суммы сделки по приобретению недвижимости за счет кредитных средств необходимо предоставление дополнительного обеспечения.
						</div>
						<div class="note_text" id="s2">
 <a href="#l2" class="footnote"><sup>2</sup></a>2.1.Рассматривается в индивидуальном порядке с окончательным утверждением списка поручителей коллегиальным органом Банка. 2. Поручительство региональных Фондов содействия кредитованию малого бизнеса может быть принято в качестве частичного обеспечения .
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Требования к заемщикам</h1>
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Фактический срок существования бизнеса
							</td>
							<td>
								 Не менее 12 месяцев
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Категория заемщиков&nbsp; / принципалов
							</td>
							<td>
								 Юридические лица, индивидуальные предприниматели
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Место регистрации
							</td>
							<td>
								 г. Москва, Московская область, г. Санкт-Петербург, Ленинградская область, регионы присутствия филиалов/операционных офисов Банка
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Состав учредителей / акционеров
							</td>
							<td>
								 Доля участия в уставном капитале государственных, общественных и религиозных организаций (объединений), благотворительных и иных фондов – не более 25 %
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Прочие требования
							</td>
							<td>
								 Руководители или акционеры данного предприятия ранее не были владельцами или руководили предприятиями, признанными банкротами или имевшими длительный период неплатежеспособности. Предприятие не имеет существенных налоговых нарушений или претензий со стороны государственных и правоохранительных органов.
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Финансовые результаты
							</td>
							<td>
								 Деятельность прибыльна (с учетом показателей бухгалтерской отчетности), либо Убыточная деятельность оценивается как несущественная (полученные убытки привели к снижению чистых активов менее чем на 25% от максимального значения за последние 4 отчетные даты).
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_center">
						<h2>Партнеры Банка по Программе «Кредит на недвижимость»:</h2>
						 <!--ul class="big_list">
								<li>
									<a href="http://www.incom-realty.ru/sale-realty/commercesell/" target="_blank">Департамент коммерческой недвижимости компании «ИНКОМ-Недвижимость»,</a>
								</li>
								<li>
									<a href="http://www.konti.ru/index.php?razdel_id=9&group_id=71" target="_blank">Группа компаний «КОНТИ»,</a>
								</li>
								<li>
									<a href="http://www.absrealty.ru/commercial/" target="_blank">Инвестиционная группа «Абсолют»,</a>
								</li>
							</ul-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wr_block_type">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"universal",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
		"SEF_MODE" => "N",
		"SOURCE_TREATMENT" => "Заявка на кредит на приобретение недвижимости.",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "1"
	)
);?>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>