<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тендерный кредит");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/credits.png');
?>
	<div class="wr_block_type">
		<!--content_tab-->
		<div class="content_rates_tabs">
			<ul class="content_tab c-container">
				<li><a href="#content-tabs-1">Описание продукта, преимущества</a></li>
				<li><a href="#content-tabs-2">Условия кредитования</a></li>
				<li><a href="#content-tabs-3">Требования к заемщику</a></li>
			</ul>
			<div class="content_body" id="content-tabs-1">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
						</div>
						<div class="block_type_center">
							<h1>Преимущества сотрудничества с Банком ЗЕНИТ:</h1>
							<ul class="big_list">
								<li>Обеспечение в форме залога - не требуется,</li>
								<li>Комиссия за досрочное погашение – не взимается,</li>
								<li>Комиссия за неиспользование лимита (при кредитовании в форме возобновляемой линии) – не взимается,
								<li>
								<li>Предусмотрены льготные тарифы за расчетно-кассовое обслуживание, эквайринг, зарплатный проект. </li>
							</ul>
							<div class="note_text">
								Конечные условия кредитования определяются индивидуально, в зависимости от финансового
								состояния клиента и потребностей бизнеса.
							</div>
						</div>
					</div>
				</div>
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<h1>Как получить кредит:</h1>
							<!--step_block-->
							<div class="step_block">
								<div class="step_item">
									<div class="step_num">1</div>
									<div class="step_body">
										<div class="step_title">
											Заполните заявку на сайте Банка и дождитесь звонка менеджера
										</div>
										<div class="step_text">
										</div>
									</div>
								</div>
								<div class="step_item">
									<div class="step_num">2</div>
									<div class="step_body">
										<div class="step_title">
											Пройдите кредитное интервью по телефону или договоритесь о встрече
										</div>
										<div class="step_text">
										</div>
									</div>
								</div>
								<div class="step_item">
									<div class="step_num">3</div>
									<div class="step_body">
										<div class="step_title">
											Подготовьте финансовые и юридические документы в соответствии со списком,
											который составит для Вас менеджер Банка по итогам переговоров
										</div>
										<div class="step_text">
										</div>

									</div>
								</div>
								<div class="step_item">
									<div class="step_num">4</div>
									<div class="step_body">
										<div class="step_title">
											После принятия решения на Кредитном Комитете Вас пригласят для открытия
											расчетного счета и подписания кредитно-обеспечительной документации
										</div>
										<div class="step_text">
										</div>
									</div>
								</div>
							</div>
							<!--step_block-->
						</div>
					</div>
				</div>
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/rates.php",
                    Array(
                        "template"=>"rates",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "Y",
                        "show_blanks" => "N",
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
			</div>
			<div class="content_body" id="content-tabs-2">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">

						</div>
						<div class="block_type_center">
							<!--table-->
							<table class="tb min_table">
								<tbody>
								<tr style="width: 1863px;">
									<td>
										Цели кредитования
									</td>
									<td>
										Обеспечение заявки для участия в конкурсе, проводимом государственными и
										муниципальными учреждениями с целью заключения контракта.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Форма предоставления кредита
									</td>
									<td>
										<ul>
											<li>Единовременная выдача </li>
											<li>Возобновляемая кредитная линия</li>
										</ul>
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Валюта кредита
									</td>
									<td>
										Рубли
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Срок кредитования
									</td>
									<td>
										<ul>
											<li>Не более 3 месяцев при предоставлении кредита; </li>
											<li>Не более 12 месяцев при предоставлении возобновляемой линии.</li>
										</ul>
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Срок траншей
									</td>
									<td>
										Не более 3 месяцев при предоставлении возобновляемой линии.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Максимальная сумма кредита
									</td>
									<td>
										150 000 000 руб. Сумма кредита/транша не должна быть более суммы обеспечения заявки
										на участие в аукционе, установленной в документации /электронном аукционе.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Процентная ставка
									</td>
									<td>
										устанавливается индивидуально
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Порядок погашения кредита
									</td>
									<td>
										<ul>
											<li>
												Единовременно в конце срока действия договора (при предоставлении кредита);
											</li>
											<li>
												Единовременно в дату погашения каждого транша (при предоставлении возобновляемой
												кредитной линии).
											</li>
										</ul>
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Порядок погашения процентов по кредиту
									</td>
									<td>
										Ежемесячно
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Досрочное погашение кредита
									</td>
									<td>
										Разрешено без ограничений.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Обеспечение
									</td>
									<td>
										Поручительство физических лиц (собственников бизнеса / супруга (супруги)
										индивидуального предпринимателя либо третьих лиц) и /или юридических лиц -
										связанных компаний
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Срок рассмотрения заявки
									</td>
									<td>
										Не более 5 рабочих дней
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Наличие расчетного счета и поддержание оборотов
									</td>
									<td>
										Открытие расчетного счета обязательно после принятия положительного решения.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Проверка кредитной истории
									</td>
									<td>
										Согласие юридического лица (заемщик/поручитель), согласие физических лиц -
										поручителей на получение информации из бюро кредитных историй.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Прочее
									</td>
									<td>
										В случае заключения договора поручительства с физическим лицом (собственником /
										учредителем бизнеса и/или третьим лицом) необходимо нотариально заверенное согласие
										супруги (а) на совершение сделки или брачный контракт
									</td>
								</tr>
								</tbody>
							</table>
							<!--table-->
						</div>
					</div>
				</div>
			</div>
			<div class="content_body" id="content-tabs-3">
				<div class="wr_block_type">
					<div class="block_type to_column c-container">
						<div class="block_type_right">
						</div>
						<div class="block_type_center">
							<!--table-->
							<table class="tb min_table">
								<tbody>
								<tr style="width: 1863px;">
									<td>
										Фактический срок существования бизнеса
									</td>
									<td>
										Не менее 12 месяцев
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Категория заемщиков  / принципалов
									</td>
									<td>
										Юридические лица, индивидуальные предприниматели
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Место регистрации
									</td>
									<td>
										г. Москва, Московская область, г. Санкт-Петербург, Ленинградская область,
										регионы присутствия филиалов/операционных офисов Банка
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Состав учредителей / акционеров
									</td>
									<td>
										Доля участия в уставном капитале государственных, общественных и религиозных
										организаций (объединений), благотворительных и иных фондов – не более 25 %
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Прочие требования
									</td>
									<td>
										Руководители или акционеры данного предприятия ранее не были владельцами или
										руководили предприятиями, признанными банкротами или имевшими длительный период
										неплатежеспособности. Предприятие не имеет существенных налоговых нарушений или
										претензий со стороны государственных и правоохранительных органов.
									</td>
								</tr>
								<tr style="width: 1863px;">
									<td>
										Финансовые результаты
									</td>
									<td>
										Деятельность прибыльна (с учетом показателей бухгалтерской отчетности), либо
										Убыточная деятельность оценивается как несущественная (полученные убытки привели
										к снижению чистых активов менее чем на 25% от максимального значения за последние
										4 отчетные даты).
									</td>
								</tr>
								</tbody>
							</table>
							<!--table-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="wr_block_type">
		<? $APPLICATION->IncludeComponent("bitrix:form.result.new", "universal", array(
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "N",
			"CHAIN_ITEM_LINK" => "",
			"CHAIN_ITEM_TEXT" => "",
			"EDIT_URL" => "",
			"IGNORE_CUSTOM_TEMPLATE" => "N",
			"LIST_URL" => "",
			"SEF_MODE" => "N",
			"SUCCESS_URL" => "",
			"USE_EXTENDED_ERRORS" => "Y",
			"WEB_FORM_ID" => "1",
			"COMPONENT_TEMPLATE" => "universal",
			"SOURCE_TREATMENT" => "Заявка на Тендерный кредит: " . $arResult["NAME"],
			"RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более  10 минут.",
			"VARIABLE_ALIASES" => array(
				"WEB_FORM_ID" => "WEB_FORM_ID",
				"RESULT_ID" => "RESULT_ID",
			)
		), false); ?>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>