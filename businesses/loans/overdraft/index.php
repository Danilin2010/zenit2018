<? 
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Овердрафт");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/credits.png');
?><div class="wr_block_type">
        <!--content_tab-->
        <div class="content_rates_tabs">
            <ul class="content_tab c-container">
                <li><a href="#content-tabs-1">Описание продукта, преимущества</a></li>
                <li><a href="#content-tabs-2">Условия кредитования</a></li>
                <li><a href="#content-tabs-3">Требования к заемщику</a></li>
            </ul>
            <div class="content_body" id="content-tabs-1">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">
                        </div>
                        <div class="block_type_center">
                            <h1>С помощью оборотного Кредита Предприятие может:</h1>
                            <ul class="big_list">
                                <li>Приобрести товарно-материальные ценности,</li>
                                <li>Пополнить запасы,</li>
                                <li>Приобрести расходные материалы и сырье,
                                </li><li>
                                </li><li>Расширить ассортиментный ряд,</li>
                                <li>Выплатить заработную плату,</li>
                                <li>Заплатить налоги.</li>
                                <li>Оплатить аренду</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">
                        </div>
                        <div class="block_type_center">
                            <h1>Преимущества сотрудничества с Банком ЗЕНИТ:</h1>
                            <ul class="big_list">
                                <li>отсутствие необходимости предоставлять обеспечение;</li>
                                <li>Комиссия за неиспользование лимита не взимается;,</li>
                                <!--li>возможность финансирования кассовых разрывов сроком 1-2 месяца;
                                </li--><li>
                                </li><li>возможность оперативного привлечения кредитных средств;</li>
                                <li>
                                    оптимизация стоимости кредитных средств за счет оперативного привлечения кредитов в
                                    объеме, требуемом для оплаты расчетных документов и их автоматического погашения за
                                    счет поступлений на счета;
                                </li>
                                <li>
                                    возможность определения лимита овердрафта на основании данных по оборотам в других
                                    кредитных организациях, а также на основании оборотов компаний, входящих в группу
                                    компаний заемщика;
                                </li>
                                <li>возможность самостоятельного распределения общего лимита овердрафта между компаниями группы</li>
                            </ul>
                            <div class="note_text">
                                Конечные условия кредитования определяются индивидуально, в зависимости от финансового
                                состояния клиента и потребностей бизнеса.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <h1>Как получить кредит:</h1>
                            <!--step_block-->
                            <div class="step_block">
                                <div class="step_item">
                                    <div class="step_num">1</div>
                                    <div class="step_body">
                                        <div class="step_title">
                                            Заполните заявку на сайте Банка и дождитесь звонка менеджера
                                        </div>
                                        <div class="step_text">
                                        </div>
                                    </div>
                                </div>
                                <div class="step_item">
                                    <div class="step_num">2</div>
                                    <div class="step_body">
                                        <div class="step_title">
                                            Пройдите кредитное интервью по телефону или договоритесь о встрече
                                        </div>
                                        <div class="step_text">
                                        </div>
                                    </div>
                                </div>
                                <div class="step_item">
                                    <div class="step_num">3</div>
                                    <div class="step_body">
                                        <div class="step_title">
                                            Подготовьте финансовые и юридические документы в соответствии со списком,
                                            который составит для Вас менеджер Банка по итогам переговоров
                                        </div>
                                        <div class="step_text">
                                        </div>

                                    </div>
                                </div>
                                <div class="step_item">
                                    <div class="step_num">4</div>
                                    <div class="step_body">
                                        <div class="step_title">
                                            После принятия решения на Кредитном Комитете Вас пригласят для открытия
                                            расчетного счета и подписания кредитно-обеспечительной документации
                                        </div>
                                        <div class="step_text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--step_block-->
                        </div>
                    </div>
                </div>
                <?$APPLICATION->IncludeFile(
                    SITE_TEMPLATE_PATH."/inc/block/rates.php",
                    Array(
                        "template"=>"rates",
                        "all_phone" => "8 (800) 500-66-77",
                        "use_clientbank" => "Y",
                        "show_blanks" => "N",
                    ),
                    Array("MODE"=>"txt","SHOW_BORDER"=>false)
                );?>
            </div>
            <div class="content_body" id="content-tabs-2">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <!--table-->
                            <table class="tb min_table">
                                <tbody>
                                <tr style="width: 1863px;">
                                    <td>
                                        Цели кредитования
                                    </td>
                                    <td>
                                        Оплата расчетных документов при отсутствии/недостаточности средств на счете
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Форма предоставления кредита
                                    </td>
                                    <td>
                                        Овердрафт
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Валюта кредита
                                    </td>
                                    <td>
                                        Рубли
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Срок кредита
                                    </td>
                                    <td>
                                        Срок действия договора – до 12 мес. <!--Срок транша – не более 2 мес.-->
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Максимальная сумма кредита
                                    </td>
                                    <td>
                                        150 000 000 руб.,<br>
                                        но не более 50% среднемесячных оборотов по расчетному счету
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Процентная ставка
                                    </td>
                                    <td>
                                        устанавливается индивидуально
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Порядок погашения кредита
                                    </td>
                                    <td>
                                        Списание со счета без распоряжения Заемщика на основании расчетных документов Банка
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Отсрочка погашения основного долга
                                    </td>
                                    <td>
                                        Не предусмотрена
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Досрочное погашение кредита
                                    </td>
                                    <td>
                                        Без ограничений
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Страхование
                                    </td>
                                    <td>
                                        Не требуется
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Срок рассмотрения заявки
                                    </td>
                                    <td>
                                        Не более 5 рабочих дней
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Наличие расчетного счета и поддержание оборотов
                                    </td>
                                    <td>
                                        Обязательно
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Проверка кредитной истории
                                    </td>
                                    <td>
                                        Согласие физических лиц – собственников (учредителей) бизнеса на получение
                                        информации из бюро кредитных историй
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--table-->
                            <div class="none_text">
                                Предоставление поручительства юридических лиц – связанных компаний (при наличии таких компаний)
                                и физических лиц – собственников (учредителей) рассматривается в индивидуальном порядке с
                                окончательным утверждением списка Поручителей коллегиальным органом Банка.
                            </div>
                            <div class="note_text">
                                В случае заключения договора поручительства / договора залога с физическим лицом
                                (собственником / учредителем бизнеса, залогодателем и/или третьим лицом) необходимо
                                нотариально заверенное согласие супруги (а) на совершение сделки или брачный контракт
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content_body" id="content-tabs-3">
                <div class="wr_block_type">
                    <div class="block_type to_column c-container">
                        <div class="block_type_right">

                        </div>
                        <div class="block_type_center">
                            <!--table-->
                            <table class="tb min_table">
                                <tbody>
                                <tr style="width: 1863px;">
                                    <td>
                                        Фактический срок существования бизнеса
                                    </td>
                                    <td>
                                        Не менее 12 месяцев
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Категория заемщиков&nbsp; / принципалов
                                    </td>
                                    <td>
                                        Юридические лица, индивидуальные предприниматели
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Место регистрации
                                    </td>
                                    <td>
                                        г. Москва, Московская область, г. Санкт-Петербург, Ленинградская область,
                                        регионы присутствия филиалов/операционных офисов Банка
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Состав учредителей / акционеров
                                    </td>
                                    <td>
                                        Доля участия в уставном капитале государственных, общественных и религиозных
                                        организаций (объединений), благотворительных и иных фондов – не более 25 %
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Прочие требования
                                    </td>
                                    <td>
                                        Руководители или акционеры данного предприятия ранее не были владельцами или
                                        руководили предприятиями, признанными банкротами или имевшими длительный период
                                        неплатежеспособности. Предприятие не имеет существенных налоговых нарушений или
                                        претензий со стороны государственных и правоохранительных органов.
                                    </td>
                                </tr>
                                <tr style="width: 1863px;">
                                    <td>
                                        Финансовые результаты
                                    </td>
                                    <td>
                                        Деятельность прибыльна (с учетом показателей бухгалтерской отчетности), либо
                                        Убыточная деятельность оценивается как несущественная (полученные убытки привели
                                        к снижению чистых активов менее чем на 25% от максимального значения за последние
                                        4 отчетные даты).
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!--table-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wr_block_type">
        <? $APPLICATION->IncludeComponent("bitrix:form.result.new", "universal", array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "N",
            "CHAIN_ITEM_LINK" => "",
            "CHAIN_ITEM_TEXT" => "",
            "EDIT_URL" => "",
            "IGNORE_CUSTOM_TEMPLATE" => "N",
            "LIST_URL" => "",
            "SEF_MODE" => "N",
            "SUCCESS_URL" => "",
            "USE_EXTENDED_ERRORS" => "Y",
            "WEB_FORM_ID" => "1",
            "COMPONENT_TEMPLATE" => "universal",
            "SOURCE_TREATMENT" => "Заявка на Овердрафт: " . $arResult["NAME"],
            "RIGHT_TEXT" => "Заполните заявку.<br/>Это займет не более 10 минут.",
            "VARIABLE_ALIASES" => array(
                "WEB_FORM_ID" => "WEB_FORM_ID",
                "RESULT_ID" => "RESULT_ID",
            )
        ), false); ?>

    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>