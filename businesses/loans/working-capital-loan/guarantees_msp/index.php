<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Гарантийная поддержка АО «МСП Банк»/АО «Корпорация «МСП»");
$APPLICATION->SetTitle("Гарантийная поддержка АО «МСП Банк»/АО «Корпорация «МСП»");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/credits.png');
?>
<div class="wr_block_type">
	<div class="block_type to_column c-container">
		<div class="block_type_right">
		</div>
		<div class="block_type_center">
<p>АО &laquo;МСП Банк&raquo;/АО &laquo;Корпорация &laquo;МСП&raquo; способствуют развитию малого и среднего бизнеса, предоставляя субъектам малого и среднего предпринимательства (субъекты МСП) гарантийную поддержку для получения банковского кредита/банковской гарантии в ПАО Банк ЗЕНИТ, когда единственным препятствием является отсутствие у клиента достаточного залогового обеспечения.</p>
<p>Кроме того, АО &laquo;МСП Банк&raquo;/АО &laquo;Корпорация &laquo;МСП&raquo; реализует программу гарантийной поддержки субъектов МСП предоставляя комбинированные гарантийные продукты, основанные на взаимодействии с <a href="/businesses/loans/state-support-of-smes/warranty-support-of-jsc-corporation-smes-jsc-sme-bank/">Региональными гарантийными организациями (РГО)</a></p>
<p><strong>Условия гарантии</strong></p>
<table class="numsV" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="th">
<p>Размер гарантии АО &laquo;МСП Банк&raquo;</p>
</td>
<td valign="top">
<ul style="list-style-type: square;">
<li>до 50% от суммы предоставляемого кредита / банковской гарантии</li>
<li>до 70% по продукту &laquo;Согарантия&raquo; (совместно с поручительством РГО)</li>
</ul>
<p>сумма гарантии до 100 млн. рублей (включительно)</p>
</td>
</tr>
<tr>
<td>
<p>Размер гарантии АО &laquo;Корпорация &laquo;МСП&raquo;</p>
</td>
<td valign="top">
<ul style="list-style-type: square;">
<li>до 50% от суммы предоставляемого кредита / банковской гарантии</li>
<li>до 70% по продукту &laquo;Согарантия&raquo; (совместно с поручительством РГО)</li>
</ul>
<p>сумма гарантии от 100 млн. рублей</p>
</td>
</tr>
<tr>
<td class="th">
<p>Срок гарантии</p>
</td>
<td valign="top">
<p>от 1 до 10 лет (соответствует сроку кредита + 120 дней)</p>
</td>
</tr>
<tr>
<td class="th">
<p>Комиссия за предоставление гарантии</p>
</td>
<td valign="top">
<p>0,75% годовых от суммы гарантии</p>
</td>
</tr>
<tr>
<td class="th">
<p>Порядок оплаты комиссии</p>
</td>
<td valign="top">
<p>Единовременно/ежегодно/1 раз в полгода/ежеквартально</p>
</td>
</tr>
</tbody>
</table>
<p><strong>Условия получения гарантийной поддержки</strong></p>
<p>Соответствие требованиям Федерального закона &laquo;О развитии малого и среднего предпринимательства в Российской Федерации&raquo; от 24.07.2007 № 209-ФЗ.</p>
<p>Порядок получения поддержки:</p>
<ol>
<li>Обратиться в ПАО Банк ЗЕНИТ за предоставлением кредита или банковской гарантией с использованием в качестве части обеспечения гарантии АО &laquo;МСП Банк&raquo; или АО &laquo;Корпорация &laquo;МСП&raquo;;</li>
<li>Получить предварительное одобрение в Банке с условием предоставления гарантии АО &laquo;МСП Банк&raquo; или АО &laquo;Корпорация &laquo;МСП&raquo; в зависимости от суммы гарантии;</li>
<li>Получить одобрение АО &laquo;МСП Банк&raquo; или АО &laquo;Корпорация &laquo;МСП&raquo;<sup>1</sup>;</li>
<li>Получить кредит или банковскую гарантию в ПАО Банк ЗЕНИТ <sup>2</sup>.</li>
</ol>
<p><sup>1</sup> Работу по взаимодействию с АО &laquo;МСП Банк&raquo; или АО &laquo;Корпорация &laquo;МСП&raquo; в части подготовки документов для принятия решения и последующей выдачи гарантии осуществляет ПАО Банк ЗЕНИТ (на основании документов, предоставленных клиентом).</p>
<p><sup>2</sup> Предоставление кредита возможно при получении гарантии АО &laquo;МСП Банк&raquo; или АО &laquo;Корпорация &laquo;МСП&raquo;.</p>
					</div>
				</div>
			</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>