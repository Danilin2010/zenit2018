<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Гарантийные механизмы");
BufferContent::SetTitle('top_breadcrumbs_images', SITE_TEMPLATE_PATH . '/img/top_banner/smes/credits.png');
?><div class="wr_block_type">
	 <!--content_tab-->
	<div class="content_rates_tabs">
		<ul class="content_tab c-container">
			<li><a href="#content-tabs-1">Условия</a></li>
			<li><a href="#content-tabs-2">Как получить</a></li>
			<li><a href="#content-tabs-3">Партнеры</a></li>
			<li><a href="#content-tabs-4">Контакты</a></li>
		</ul>
		<div class="content_body" id="content-tabs-1">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Размер гарантии АО «МСП Банк»
							</td>
							<td>
								<ul>
									<li>до 50% от суммы предоставляемого кредита / банковской гарантии</li>
									<li>до 70% по продукту «Согарантия» (совместно с поручительством РГО) сумма гарантии до 100 млн. рублей (включительно) </li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Размер гарантии АО «Корпорация «МСП»
							</td>
							<td>
								<ul>
									<li>до 50% от суммы предоставляемого кредита / банковской гарантии</li>
									<li>до 70% по продукту «Согарантия» (совместно с поручительством РГО) сумма гарантии до 100 млн. рублей (включительно) </li>
								</ul>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Срок гарантии
							</td>
							<td>
								 от 1 до 10 лет (соответствует сроку кредита + 120 дней)
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Комиссия за предоставление гарантии
							</td>
							<td>
								 0,75% годовых от суммы гарантии
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Порядок оплаты комиссии
							</td>
							<td>
								 Единовременно/ежегодно/1 раз в полгода/ежеквартально
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Условия получения поддержки
							</td>
							<td>
								 Соответствие требованиям Федерального закона «О развитии малого и среднего предпринимательства в Российской Федерации» от 24.07.2007 № 209-ФЗ.
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-2">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Список шагов (Как оформить и пр.)</h1>
						 <!--step_block-->
						<div class="step_block">
							<div class="step_item">
								<div class="step_num">
									1
								</div>
								<div class="step_body">
									<div class="step_title">
										 Обратиться в ПАО Банк ЗЕНИТ за предоставлением кредита или банковской гарантией с использованием в качестве части обеспечения гарантии АО «МСП Банк» или АО «Корпорация «МСП»;
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									2
								</div>
								<div class="step_body">
									<div class="step_title">
										 Получить предварительное одобрение в Банке с условием предоставления гарантии АО «МСП Банк» или АО «Корпорация «МСП» в зависимости от суммы гарантии;
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									3
								</div>
								<div class="step_body">
									<div class="step_title">
										 Получить одобрение АО «МСП Банк» или АО «Корпорация «МСП»<a href="#s1"><sup>1</sup></a> ;
									</div>
								</div>
							</div>
							<div class="step_item">
								<div class="step_num">
									4
								</div>
								<div class="step_body">
									<div class="step_title">
										 Получить кредит или банковскую гарантию в ПАО Банк ЗЕНИТ 2. <a href="#s2"><sup>2</sup></a> ;
									</div>
								</div>
							</div>
						</div>
						 <!--step_block-->
						<div class="note_text">
							<p id="s1">
 <a href="#s1"><sup>1</sup></a>Работу по взаимодействию с АО «МСП Банк» или АО «Корпорация «МСП» в части подготовки документов для принятия решения и последующей выдачи гарантии осуществляет ПАО Банк ЗЕНИТ (на основании документов, предоставленных клиентом).
							</p>
							<p id="s2">
 <a href="#s2"><sup>2</sup></a>
								Предоставление кредита возможно при получении гарантии АО «МСП Банк» или АО «Корпорация «МСП».
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-3">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<ul class="big_list">
							<li>
							АО «МСП Банк»/ АО «Корпорация «МСП» </li>
							<li>
							Региональные гарантийные организации (РГО):
							<ul>
								<li>
								Москва - <a href="http://www.mosgarantfund.ru/" target="_blank">Фонд содействия кредитованию малого бизнеса Москвы</a>, </li>
								<li>
								Республика Татарстан - <a href="http://garfondrt.ru/" target="_blank">Гарантийный фонд Республики Татарстан</a>, </li>
								<li>
								Воронежская область - <a href="http://www.fundsbs.ru/page/main/" target="_blank">Государственный фонд поддержки малого предпринимательства Воронежской области</a>, </li>
								<li>
								Кемеровская область - <a href="http://gfppko.ru/pages/98.html" target="_blank">Государственный фонд поддержки предпринимательства Кемеровской области</a>, </li>
								<li>
								Калининградская область - <a href="http://fpmp39.ru/" target="_blank">Государственное автономное учреждение Калининградской области «Фонд поддержки предпринимательства»,</a> </li>
								<li>
								Новосибирская область - <a href="http://fondmsp.ru/" target="_blank">Фонд развития малого и среднего предпринимательства Новосибирской области</a>, </li>
								<li>
								Нижегородская область - <a href="http://www.garantnn.ru/" target="_blank">Агентство по развитию системы гарантий для субъектов малого предпринимательства Нижегородской области</a>, </li>
								<li>
								Омская область - <a href="http://www.fond-omsk.ru/" target="_blank">Омский региональный фонд поддержки и развития малого предпринимательства</a>, </li>
								<li>
								Пермский край - <a href="http://www.pgf-perm.ru/" target="_blank">АО "Пермский гарантийный фонд"</a>, </li>
								<li>
								Ростовская область - <a href="http://www.dongarant.ru/" target="_blank">Гарантийный фонд Ростовской области</a>, </li>
								<li>
								Республика Алтай - <a href="http://aijk-ra.ru/index.php/garantijnyj-fond-respubliki-altaj" target="_blank">Агентство по ипотечному жилищному кредитованию Республики Алтай</a>, </li>
								<li>
								Санкт – Петербург - <a href="http://credit-fond.ru/" target="_blank">Фонд содействия кредитованию малого и среднего бизнеса Санкт-Петербурга</a>, </li>
								<li>
								Самарская область - <a href="http://www.gfso.ru/" target="_blank">Государственное унитарное предприятие Самарской области «Гарантийный фонд поддержки предпринимательства Самарской области»</a>, </li>
								<li>
								Свердловская область - <a href="http://www.sofp.ru/" target="_blank">Свердловский областной фонд поддержки малого предпринимательства</a>, </li>
								<li>
								Саратовская область - <a href="http://saratovgarantfond.ru/" target="_blank">АО "Гарантийный фонд для субъектов малого предпринимательства Саратовской области"</a>, </li>
								<li>
								Удмуртская Республика - <a href="http://www.gfskur.ru/" target="_blank">Гарантийный фонд содействия кредитованию малого и среднего предпринимательства Удмуртской Республики</a>, </li>
								<li>
								Челябинская область - <a href="http://www.fond74.ru/" target="_blank">Фонд содействия кредитованию малого предпринимательства Челябинской области</a>. </li>
							</ul>
 </li>
						</ul>
						<p>
							 Подробную информацию об условиях предоставления поручительства РГО в своем регионе уточняйте в филиалах ПАО Банк ЗЕНИТ и на сайтах РГО.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="content_body" id="content-tabs-4">
			<div class="wr_block_type">
				<div class="block_type to_column c-container">
					<div class="block_type_right">
					</div>
					<div class="block_type_center">
						<h1>Контактная информация</h1>
						 <!--table-->
						<table class="tb min_table">
						<tbody>
						<tr style="width: 1863px;">
							<td>
								 Роман Тимофеев<br>
 <span class="note_text">Управление продаж (Москва и Московская область)</span>
							</td>
							<td>
								 Тел. +7 (495) 937-07-37 доб.2687<br>
								 e-mail: <a href="mailto:r.timofeev@zenit.ru">r.timofeev@zenit.ru&nbsp;</a>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Гостева Инна <br>
 <span class="note_text">Управление регионального развития <br>
								 (Регионы, кроме Москвы и Московской области)</span>
							</td>
							<td>
								 Тел. +7 (495) 937-07-37 доб.2539<br>
								 e-mail: <a href="mailto:i.gosteva@zenit.ru">i.gosteva@zenit.ru</a>
							</td>
						</tr>
						<tr style="width: 1863px;">
							<td>
								 Шаймуратов Эльдар<br>
 <span class="note_text">Управление регионального развития <br>
								 (Регионы, кроме Москвы и Московской области)</span>
							</td>
							<td>
								 Тел. +7 (495) 937-07-37 доб.3717<br>
								 e-mail: <a href="mailto:e.shaymuratov@zenit.ru">e.shaymuratov@zenit.ru</a>
							</td>
						</tr>
						</tbody>
						</table>
						 <!--table-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>