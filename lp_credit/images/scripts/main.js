$(document).ready(function() {

    "use strict";

    /**
     * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
     * Она сработает только один раз через N миллисекунд после последнего вызова.
     * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
     * первого запуска.
     */
    function debounce(func, wait, immediate) {

        var timeout;

        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };

            var callNow = immediate && !timeout;

            clearTimeout(timeout);
            timeout = setTimeout(later, wait);

            if (callNow) func.apply(context, args);
        };
    };

    /**
     * определение версии IE 10 или 11
     *
     * @returns {Number} 10, 11, или 0 если это не IE
     */
    function GetIEVersion() {

        var sAgent = window.navigator.userAgent;
        var Idx = sAgent.indexOf("MSIE");

        // If IE, return version number.
        if (Idx > 0) {
            return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
        }

        // If IE 11 then look for Updated user agent string.
        else if ( !! navigator.userAgent.match(/Trident\/7\./)) {
            return 11;
        } else {
            return 0; //It is not IE
        }
    }


    // Test via a getter in the options object to see if the passive property is accessed
    window.supportsPassive = false;

    try {

        var opts = Object.defineProperty({}, 'passive', {
            get: function() {
                window.supportsPassive = true;
            }
        });

        window.addEventListener("test", null, opts);
    } catch (e) {}

    /**
     * определение существования элемента на странице
     */
    $.exists = function(selector) {
        return ($(selector).length > 0);
    }

    $(function() {
        $('.navigation-btn').on('click', function(e) {
            e.preventDefault();
            if ($('html, body').is(':animated')) return false;

            var href = $(this).attr('href');
            if ($(href).length) {
                var indent = $(window).width > 940 ? 70 : 90;
                var k = false;

                $('html, body').animate({
                    scrollTop: ($(href).offset().top - indent)
                }, 600, function() {
                    setTimeout(function() {
                        k = true
                    }, 100);
                    $('.app-header__navigation-wr').removeClass('opened');
                });
            }
            return false;
        });
        $('.app-header__mobile-burger, .svg-icon--close').on('click', function(e) {
            e.preventDefault();
            $('.app-header__navigation-wr').toggleClass('opened');
        });
    });

    $(function() {
        $('.calculator-block input[type="radio"], .calculator-block input[type="number"]').styler();


        $('.svg-icon--info-ico').on('click', function() {
            $('.info-popup').removeClass('active');
            $(this).closest('span.info').find('.info-popup').addClass('active');
        });
        $(document).on('click', function(event) {
            if ($(event.target).closest('span.info, .svg-icon--info-ico').length) return;
            $('.info-popup').removeClass('active');
        });

        function numbFormat() {
            $('.calculator-block input[type="text"]').each(function() {
                var $this = $(this),
                    val = $this.val(),
                    newVal = String(val).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');

                $this.val(newVal);
            });
        }

        $('.input_range_slider').each(function(index, el) {

            var $this = $(this),
                $lower = $this.closest('.kredit-summa, .credit-term-left').find('input[type="text"]'),
                min = parseInt($lower.attr('min')),
                max = parseInt($lower.attr('max')),
                start = parseInt($this.data('start')),
                step = parseInt($this.data('step')),
                arr = [min, max];

            var slider = $this.noUiSlider({
                start: [start],
                step: step,
                behaviour: 'drag-tap',
                format: wNumb({
                    decimals: 0
                }),
                range: {
                    'min': [min],
                    'max': [max]
                }
            });

            slider.Link('lower').to($lower);

            slider.on('slide', function() {
                calculatorFunction();
            });
            slider.on('set', function() {
                calculatorFunction();
            });
        });

        function calculatorFunction() {
            var creditSumm = $('.kredit-summa input[type="text"]').val().replace(/\D+/g, ''),
                procent = ($('.interest-rate-body input[type="radio"]:checked').val() - 0.4) / 100,
                creditTime = $('input[type="text"].credit-term').val();

            //year-rasschet
            var yearRasschet = parseInt((creditSumm / (1 - creditTime * 0.02)).toFixed(0));

            console.log(yearRasschet)
            if (yearRasschet >= 2000000) {
                procent = ((procent * 100).toFixed(1) - 3) / 100;
            } else if (yearRasschet >= 500000) {
                procent = ((procent * 100).toFixed(1) - 1) / 100;
            }

            var procentMonath = (1 + procent / 12),
                totalMonath = -creditTime * 12,
                stepenCredit = 1 - Math.pow(procentMonath, totalMonath),
                resultInMonath = (yearRasschet * procent / 12) / stepenCredit,
                totalResult = (resultInMonath * creditTime * 12).toFixed(0),
                newTotal = String(totalResult).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 ');


            $('.calculator-block input.monthly-payment').val(resultInMonath.toFixed(0) + ' руб.');
            $('.calculator-block .result-body > span').text(newTotal);

            var minYear = -$('.calculator-block input[type="text"].credit-term').attr('min') * 12,
                maxYear = -$('.calculator-block input[type="text"].credit-term').attr('max') * 12,
                stepenCreditMax = 1 - Math.pow(procentMonath, minYear),
                stepenCreditMin = 1 - Math.pow(procentMonath, maxYear),
                minPaymentMonath = (creditSumm * procent / 12) / stepenCreditMin,
                maxPaymentMonath = (creditSumm * procent / 12) / stepenCreditMax;

            $('.calculator-block input[type="text"].monthly-payment').attr('min', minPaymentMonath.toFixed(0));
            $('.calculator-block input[type="text"].monthly-payment').attr('max', maxPaymentMonath.toFixed(0));
            numbFormat()

        }
        calculatorFunction();

        $('.calculator-block input[type="radio"], .calculator-block input[type="number"].credit-term').change(function() {
            if ($(this).hasClass('credit-term')) {
                var min = $(this).attr('min'),
                    max = $(this).attr('max');

                if ($(this).val() > max) {
                    $(this).val(max);
                } else if ($(this).val() < min) {
                    $(this).val(min);
                }
                calculatorFunction();
            } else {
                calculatorFunction();
            }
        });


        function calculatorFunction2() {
            var creditSumm = $('.kredit-summa input[type="text"]').val().replace(/\D+/g, ''),
                procent = $('.interest-rate-body input[type="radio"]:checked').val() / 100,
                procentMonath = (1 + procent / 12),
                resultInMonath = $('.calculator-block input.monthly-payment').val().replace(/\D+/g, '');

            var rightX = 1 - (creditSumm * procent / 12) / resultInMonath,
                resultTime = ((Math.log(rightX) / Math.log(procentMonath)) / -12).toFixed(0),
                totalResult = resultInMonath * resultTime * 12;

            if (totalResult == 0 || isNaN(totalResult)) {
                calculatorFunction();
            } else {
                $('input[type="number"].credit-term').val(resultTime);
                $('.calculator-block .result-body > span').text(totalResult.toFixed(0));
            }
        }
        // calculatorFunction2();
        $('.calculator-block input[type="number"].monthly-payment').change(function() {
            var min = parseInt($(this).attr('min')),
                max = parseInt($(this).attr('max')),
                thisVal = parseInt($(this).val());

            if (thisVal > max) {
                $(this).val(max);
            } else if (thisVal < min) {
                $(this).val(min);
            }
            calculatorFunction2();
        });

    });
    $(function() {
        $('.phone').mask('+7(000) 000 00 00');

        $('.popup-message-wr .svg-icon--close-popup').on('click', function(e) {
            $('.popup-message-wr').removeClass('opened');
        });
        $(document).on('click', function(event) {
            if ($(event.target).closest('.popup-message-in').length) return;
            $('.popup-message-wr').removeClass('opened');
        });
        if (window.location.href.match(/.*\?.*/)) {
           // $('.popup-message-wr').addClass('opened');
        }

        $('.checkbox-wr input[type="checkbox"]').on('change', function() {
            console.log($(this).attr("checked"))
            if ($(this).prop('checked')) {
                $(this).closest('form').find('button').attr('disabled', false);
            } else {
                $(this).closest('form').find('button').attr('disabled', 'disabled');
            }
        });
    });
    $(function() {

        // offices
        var office_defaults = {
            time: 'Пн - Чт 9:00 - 21:00<br>Пт 9:00 - 20:00<br>Сб 10:00 - 18:00<br>Вс - Выходной',
            time2: 'Пн - Чт 9:00 - 21:00<br>Пт 9:00 - 20:00<br>Сб 10:00 - 18:00'
        };

        var offices_marks = [
            // Московские офисы
            {
                name: 'Головной офис',
                metro: 'Проспект мира',
                address: 'Москва, Банный пер., д.9',
                phone: '+7 (495) 937-07-37',
                time: office_defaults.time,
                city: 'Москва',
                weekend: true,
            }, {
                name: 'Клиентский центр "Садовая Слобода"',
                metro: 'Павелецкая',
                address: 'Москва, ул. Садовническая, 69',
                phone: '+7 (495) 980-75-78',
                time: office_defaults.time,
                city: 'Москва',
                weekend: true,
            },
            /*        {
            name    : 'Шереметьево',
            metro   : '',
            address : 'Московская область, г. Химки, аэропорт "Шереметьево", галерея "Аэроэкспресс", 3 этаж Аэропорт Шереметьево',
            phone   : '8-800-500-66-77',
            time    : office_defaults.time,
            city    : 'Москва',
            weekend : true,
        },*/
            {
                name: 'Дополнительный офис "Одинцово"',
                metro: 'Кунцевская',
                address: 'Московская область, Одинцовский р-н, рабочий поселок Новоивановское, ул. Луговая, д. 1 ТК "ТРИ КИТА", 2 этаж',
                phone: '+7 (495) 723-82-82 (доб. 22-60, 22-61, 22-62)',
                time: office_defaults.time,
                city: 'Москва',
                weekend: false,
            },
            /* {
            name    : '"Остоженка"',
            metro   : ['Кропоткинская' , 'Парк Культуры'],
            address : 'Москва, ул. Остоженка, д. 28',
            phone   : '+7 (495) 933-29-56',
            time    : office_defaults.time,
            city    : 'Москва',
            weekend : false,
        },*/
            {
                name: 'Дополнительный офис "Европейский"',
                metro: 'Киевская',
                address: 'Москва, площадь Киевского вокзала, д. 2, ТРЦ "Европейский", первый этаж',
                phone: '+7 (495) 229-19-70',
                time: office_defaults.time,
                city: 'Москва',
                weekend: true,
            }, {
                name: 'Дополнительный офис "Семеновский"',
                metro: 'Семеновская',
                address: 'Москва, ул. Щербаковская, д. 40',
                phone: '+7 (499) 166-71-74, 166-71-63, 166-71-76',
                time: office_defaults.time,
                city: 'Москва',
                weekend: false,
            }, {
                name: 'Дополнительный офис "Химки-2"',
                metro: ['Планерная', 'Речной вокзал'],
                address: 'Московская область, г. Химки, ул. Бутакова, д. 4, Мебельный центр "Гранд-2"',
                phone: '+7 (495) 723-80-01 (доб. 42-24, 42-23)',
                time: office_defaults.time,
                city: 'Москва',
                weekend: true,
            }, {
                name: 'Дополнителный офис "Пресненский"',
                metro: 'Улица 1905 года',
                address: 'Москва, ул. Пресненский Вал, д. 16, стр. 3',
                phone: '+7 (495) 937-36-64',
                time: office_defaults.time,
                city: 'Москва',
                weekend: false,
            }, {
                name: 'Дополнительный офис "Мытищи"',
                metro: 'Медведково',
                address: 'Московская область, г. Мытищи, ул. Мира, д. 24/5',
                phone: '+7 (499) 940-91-14',
                time: office_defaults.time,
                city: 'Москва',
                weekend: false,
            }, {
                name: 'Дополнительный офис "Водный"',
                metro: 'Водный стадион',
                address: 'Москва, Головинское шоссе, д. 5, корп. 1, МФК "Водный", первый этаж',
                phone: '+7 (495) 937-07-37, доб. 42-10',
                time: office_defaults.time,
                city: 'Москва',
                weekend: true,
            },
            // СПб
            {
                name: "Ф-л «Банковский центр БАЛТИКА»",
                metro: 'Спортивная',
                address: 'СПб, ул. Яблочкова, 20, Лит. Я',
                phone: '(812) 448-22-48',
                time: office_defaults.time,
                city: 'Санкт-Петербург',
                weekend: false,
            }, {
                name: "Дополнительный офис «Суворовский»",
                metro: 'Чернышевская',
                address: 'СПб, Суворовский пр-т, д. 32',
                phone: '(812) 324-69-24',
                time: office_defaults.time,
                city: 'Санкт-Петербург',
                weekend: false,
            },
            // Калининград
            {
                name: "Операционный офис «Калининградский»",
                metro: '',
                address: 'Калининград, Ленинский проспект, 155-А',
                phone: '(4012) 92-00-01',
                time: office_defaults.time,
                city: 'Калининград',
                weekend: false,
            },
            // Новосибирск
            {
                name: "Ф-л «Банковский центр СИБИРЬ»",
                metro: "",
                address: "Новосибирск, ул. Челюскинцев, 13",
                phone: "(383) 298-94-30",
                time: office_defaults.time,
                city: "Новосибирск",
                weekend: false,
            },
            // Горно-Алтайск
            {
                name: "Операционный офис «Горно-Алтайский»",
                metro: "",
                address: "Горно-Алтайск, ул. Чорос-Гуркина, 53/1",
                phone: "(38822) 2-50-18, 2-43-17",
                time: office_defaults.time,
                city: "Горно-Алтайск",
                weekend: false,
            },
            // Омск
            {
                name: "Операционный офис «Омский»",
                metro: "",
                address: "Омск, угол ул. Фрунзе и ул. Орджоникидзе, д. 49/46",
                phone: "(3812) 35-64-40",
                time: office_defaults.time,
                city: "Омск",
                weekend: false,
            },
            // Кемерово
            {
                name: "ОО «Кемеровский»",
                metro: "",
                address: "Кемерово, пр-т Советский, 74/1",
                phone: "(3842) 58-21-99, 58-33-93",
                time: office_defaults.time,
                city: "Кемерово",
                weekend: false,
            },
            // Новокузнецк
            {
                name: 'ОО "Новокузнецкий"',
                metro: "",
                address: "Новокузнецк, пр. Металлургов, 48",
                phone: "(3843) 46-60-11, 46-60-12",
                time: office_defaults.time,
                city: "Новокузнецк",
                weekend: false,
            },
            // Екатеринбург
            {
                name: 'Ф-л «Банковский центр УРАЛ»',
                metro: "",
                address: "Екатеринбург, ул. Попова, д.33-а",
                phone: "(343) 310-32-70",
                time: office_defaults.time,
                city: "Екатеринбург",
                weekend: false,
            },
            // Челябинск
            {
                name: 'Операционный офис «Челябинский»',
                metro: "",
                address: "Челябинск, ул. Советская, 17",
                phone: "(351) 247-91-94, (351) 265-13-79",
                time: office_defaults.time,
                city: "Челябинск",
                weekend: false,
            },
            // Самара
            {
                name: 'Ф-л «Банковский центр ПОВОЛЖЬЕ»',
                metro: "",
                address: "Самара, ул. Алексея Толстого/ Льва Толстого, 139/3",
                phone: "(846) 310-28-60",
                time: office_defaults.time,
                city: "Самара",
                weekend: false,
            }, {
                name: 'Дополнительный офис «Октябрьский»',
                metro: "",
                address: "Самара, ул. Скляренко, д. 26",
                phone: "(846) 200-11-00",
                time: office_defaults.time,
                city: "Самара",
                weekend: false,
            },
            // Тольятти
            {
                name: 'Дополнительный офис «Тольяттинский»',
                metro: "",
                address: "Тольятти, бульвар Ленина, 15 А",
                phone: "(8482) 31-99-50",
                time: office_defaults.time,
                city: "Тольятти",
                weekend: false,
            },
            // Саратов
            {
                name: 'Операционный офис «Саратовский»',
                metro: "",
                address: "Саратов, ул. Московская, 66",
                phone: "(8452) 66-92-11, 48-97-17",
                time: office_defaults.time,
                city: "Саратов",
                weekend: false,
            },
            // Казань
            {
                name: 'Ф-л «Банковский центр «ТАТАРСТАН» ',
                metro: "",
                address: "Казань, ул. Тельмана, 21-1",
                phone: "(843) 230-40-05",
                time: office_defaults.time,
                city: "Казань",
                weekend: false,
            },
            // Альметьевск
            {
                name: 'Дополнительный офис "На Белоглазова"',
                metro: "",
                address: "Альметьевск, ул. Белоглазова, 26а",
                phone: "(8553) 37-75-90",
                time: office_defaults.time,
                city: "Альметьевск",
                weekend: false,
            },
            // Нижнекамск
            {
                name: 'Дополнительный офис "Нижнекамский"',
                metro: "",
                address: "Нижнекамск, пр-т. Шинников, 53А",
                phone: "(8555) 42-03-51, 42-21-84, 42-23-98",
                time: office_defaults.time,
                city: "Нижнекамск",
                weekend: false,
            },
            // Курск
            {
                name: '«Курский» филиал',
                metro: "",
                address: "Курск, ул. Кати Зеленко, 9",
                phone: "(4712) 51-26-56, 52-02-84",
                time: office_defaults.time,
                city: "Курск",
                weekend: false,
            }, {
                name: 'Дополнительный офис "На Ленина"',
                metro: "",
                address: "Курск, ул. Ленина, д. 31",
                phone: "(4712) 39-90-44",
                time: office_defaults.time,
                city: "Курск",
                weekend: true,
            },
            // Воронеж
            {
                name: 'Операционный офис "Воронежский"',
                metro: "",
                address: "Воронеж, Московский проспект, д. 11",
                phone: "(4732) 76-58-68",
                time: office_defaults.time,
                city: "Воронеж",
                weekend: false,
            },
            // Ростов-на-Дону
            {
                name: '«Ростовский» филиал',
                metro: "",
                address: "Ростов-на-Дону, ул. Красноармейская, д. 154",
                phone: "(863) 201-80-28 , 201-78-10",
                time: office_defaults.time,
                city: "Ростов-на-Дону",
                weekend: false,
            },
            // Таганрог
            {
                name: 'Дополнительный офис "Таганрогский"',
                metro: "",
                address: "Таганрог, ул. Социалистическая, д. 7-1а",
                phone: "(8634) 611-996, 611-460",
                time: office_defaults.time,
                city: "Таганрог",
                weekend: false,
            },
            // Нижний Новгород
            {
                name: 'Филиал "Банковский центр "ВОЛГА"',
                metro: "",
                address: "Нижний Новгород, ул. Максима Горького, 148",
                phone: "(831) 278-97-77, 434-30-59",
                time: office_defaults.time,
                city: "Нижний Новгород",
                weekend: false,
            },
            // Арзамас
            {
                name: 'Дополнительный офис "Арзамасский"',
                metro: "",
                address: "Арзамас, ул. Куликова, 26",
                phone: "(83147) 2-37-57",
                time: office_defaults.time,
                city: "Арзамас",
                weekend: false,
            },
            // Саров
            {
                name: 'Дополнительный офис "Саровский"',
                metro: "",
                address: "Саров, проспект Ленина, д. 21, помещение П2",
                phone: "(83130) 6-99-77",
                time: office_defaults.time,
                city: "Саров",
                weekend: false,
            },
            // Пермь
            {
                name: 'Операционный офис "Пермский"',
                metro: "",
                address: "Пермь, ул. Советская, д. 40",
                phone: "(342) 211-12-20",
                time: office_defaults.time,
                city: "Пермь",
                weekend: false,
            },
            // Ижевск
            {
                name: 'Операционный офис "Ижевский"',
                metro: "",
                address: "Ижевск, ул. Ленина, д. 46",
                phone: "(3412) 799-755, 799-393",
                time: office_defaults.time,
                city: "Ижевск",
                weekend: false,
            },
        ];
        var myMap;
        var global_config = {};

        var selector = {
            size: 135,
            scrl_size: 360,
            append: 45,
            // jq
            $select: $("#metros").parents('.select'),
            $grey: $('#map .select_block, #map .grey'),
            $holder: $("#metros"),
            $scroll: $(".scrollable"),
            // tpls
            tpl_opt: _.template('<option value="<%= name %>"><%= name %></option>')
        }
        // supported functions
        var metroSelector = {
            show: function(metros) {
                $("#metros").parents('.select').show();
                selector.$grey.css('height', selector.size + selector.append);
                selector.$scroll.css('height', selector.scrl_size - selector.append);
                selector.$holder.html('').append('<option value="0" disabled selected>Ближайшее метро</option>');
                _.each(metros, function(item) {
                    selector.$holder.append(selector.tpl_opt({
                        name: item
                    }));
                })
            },
            hide: function() {
                $("#metros").parents('.select').hide();
                selector.$grey.css('height', selector.size);
                selector.$scroll.css('height', selector.scrl_size);
            }
        }

        $(selector.$holder).on("change", function() {
            var self = this;
            var geo = ymaps.geocode(global_config.city + ", метро " + $(this).val());
            geo.then(
                function res(res) {
                    myMap.setCenter(res.geoObjects.get(0).geometry.getCoordinates());
                    myMap.setZoom(13);

                    var id = null;
                    _.each(global_config.offices, function(item, key) {
                        if (typeof item.metro == 'string') {
                            if ($(self).val() == item.metro) {
                                id = 'office' + key;
                                return;
                            }
                        } else {
                            if (item.metro.indexOf($(self).val()) != -1) {
                                id = 'office' + key;
                                return;
                            }
                        }
                    });
                    $(".scrollable [data-item]").removeClass('active');
                    if (id) {
                        $(".scrollable [data-item='" + id + "']").addClass('active');
                        $(".scrollable").mCustomScrollbar("scrollTo", "#" + id);
                    }
                }
            )
        });

        var centers = {
            'Москва': [55.807909, 37.613638],
            'Санкт-Петербург': [59.9391, 30.3159],
            'Калининград': [54.7074, 20.5073],
            'Новосибирск': [55.0302, 82.9204],
            'Горно-Алтайск': [51.9582, 85.9604],
            'Омск': [54.9893, 73.3682],
            'Кемерово': [55.3550, 86.0873],
            'Новокузнецк': [53.7575, 87.1360],
            'Екатеринбург': [56.8386, 60.6055],
            'Челябинск': [55.1603, 61.4009],
            'Самара': [53.1955, 50.1018],
            'Тольятти': [53.5088, 49.4192],
            'Саратов': [51.5331, 46.0342],
            'Казань': [55.7986, 49.1063],
            'Альметьевск': [54.9014, 52.2971],
            'Нижнекамск': [55.6340, 51.8091],
            'Курск': [51.7304, 36.1926],
            'Воронеж': [51.6615, 39.2003],
            'Ростов-на-Дону': [47.2225, 39.7187],
            'Таганрог': [47.2096, 38.9352],
            'Нижний Новгород': [56.287654, 43.85763],
            'Арзамас': [55.386080, 43.816374],
            'Саров': [54.941787, 43.320621],
            'Пермь': [58.022652, 56.228604],
            'Ижевск': [56.85577, 53.201286],
        }

            function map(config) {
                global_config = config;
                var city = config.city;
                /****YaMaps****/
                var ymapsObjects = new Array();
                var allowZoomEvent = false; //разрешает событие "изменение масштаба"
                var zoomFired = false; //флаг, что "изменение масштаба" уже отработало
                var altmap = false;

                var offices = _.filter(offices_marks, function(item) {
                    return item['city'] == city
                });

                if (config.weekend) {
                    offices = _.filter(offices, function(item) {
                        return item['weekend'];
                    })
                }

                if (config.metro) {
                    offices = _.filter(offices, function(item) {
                        return item['metro'] == config.metro;
                    })
                }

                // collect metro stations
                var metro = [];
                _.each(offices, function(item) {
                    if (item.metro) {
                        if (typeof item.metro == 'string') {
                            metro.push(item.metro);
                        } else {
                            metro = metro.concat(item.metro);
                        }
                    }
                });

                // sorted uniq
                if (!config.stop_render) {
                    if (metro.length != 0) {
                        metro = _.uniq(metro, true);
                        metroSelector.show(metro);
                    } else {
                        metroSelector.hide();
                    }
                }

                $(document).on("click", ".scrollable [data-item]", function() {
                    var id = $(this).data('item');
                    $(".scrollable [data-item]").removeClass('active');
                    $(".scrollable [data-item='" + id + "']").addClass('active');
                    $(".scrollable").mCustomScrollbar("scrollTo", "#" + id);

                    ymaps.geoQuery(myMap.geoObjects).each(function(pm) {
                        pm.options.set({
                            iconImageHref: 'images/img/baloon-map.png'
                        });
                    });
                    ymapsObjects[id].options.set({
                        iconImageHref: 'images/img/baloon-map-active.png'
                    });
                    myMap.panTo(ymapsObjects[id].geometry.getCoordinates());
                })

                var scrollable_item = _.template('\
        <div class="item" data-item="<%= id %>" id="<%= id %>">\
            <div class="title"><%= name %></div>\
            <p>\
                <%= address %><br>\
                <%= time %>\
            </p>\
        </div>');

                global_config['offices'] = offices;

                // YAMAPS
                function ymapsInit() {

                    myMap = new ymaps.Map("yandex_map", {
                        // Центр карты
                        center: centers[city],
                        // Коэффициент масштабирования
                        zoom: 10,
                        // Тип карты
                        type: "yandex#map"
                    });

                    myMap.controls.add(
                        new ymaps.control.ZoomControl()
                    );

                    var myGeocoder = new Array();
                    var count = offices_marks.length;
                    $.each(offices, function(j, e) {
                        var addrblock = e.address;
                        var id = "office" + j;
                        myGeocoder[j] = ymaps.geocode(
                            // Строка с адресом, который нужно геокодировать
                            addrblock, {
                                // - требуемое количество результатов
                                results: 1,
                                // strictBounds:true,
                                boundedBy: [
                                    [56.062770, 37.132899],
                                    [55.404228, 38.299135]
                                ]
                            }
                        );
                        myGeocoder[j].then(
                            function(res) {
                                // @@ballon data
                                var data = offices[j];
                                res.geoObjects.get(0).properties.set({
                                    // balloonContentBody : "<div class='balloon_data'>\
                                    //         <div class='bl bl-name'>"+data.name+"</div>\
                                    //         <div class='bl bl-metro'>"+data.metro+"</div>\
                                    //         <div class='bl bl-address'>"+data.address+"</div>\
                                    //         <div class='bl bl-phone'>"+data.phone+"</div>\
                                    //     </div>"
                                });
                                // <div class='bl bl-time'>"+data.time+"</div>\
                                res.geoObjects.get(0).options.set({
                                    iconImageHref: 'images/img/baloon-map.png',
                                    iconImageSize: [37, 50],
                                    iconImageOffset: [-20, -52]
                                })
                                res.geoObjects.get(0).events.add('click', function(e) {
                                    var obj = e.get('target');
                                    e.preventDefault();
                                    myMap.panTo(obj.geometry.getCoordinates(), {
                                        delay: 0,
                                        duration: 500,
                                        flying: false
                                    });
                                    myMap.panTo(obj.geometry.getCoordinates());
                                    $(".scrollable [data-item]").removeClass('active');
                                    $(".scrollable [data-item='" + id + "']").addClass('active');
                                    $(".scrollable").mCustomScrollbar("scrollTo", "#" + id)
                                });
                                data['id'] = id;
                                $(".scrollable").append(scrollable_item(data));
                                ymapsObjects[id] = res.geoObjects.get(0);
                                myMap.geoObjects.add(res.geoObjects);
                            }
                        );
                    });
                    myMap.geoObjects.events.add('click', function(e) {
                        ymaps.geoQuery(myMap.geoObjects).each(function(pm) {
                            pm.options.set({
                                iconImageHref: 'images/img/baloon-map.png'
                            });
                        });
                        var obj = e.get('target');
                        obj.options.set({
                            iconImageHref: 'images/img/baloon-map-active.png'
                        });
                    });

                }

                ymaps.ready(function() {
                    ymapsInit();
                });

                // return myMap;
            }

        var map_config = {
            city: 'Москва',
            metro: false,
            weekend: false,
        };

        $(window).load(function() {
            initMap(map_config);
        })

        var cities = [
            'Калининград',
            'Новосибирск',
            'Горно-Алтайск',
            'Омск',
            'Кемерово',
            'Новокузнецк',
            'Екатеринбург',
            'Челябинск',
            'Самара',
            'Тольятти',
            'Саратов',
            'Курск',
            'Воронеж',
            'Ростов-на-Дону',
            'Таганрог',
            'Нижний Новгород',
            'Саров',
            'Арзамас',
            'Пермь',
            'Ижевск'
        ];

        cities.sort();
        cities.unshift('Москва', 'Санкт-Петербург');
        ymaps.ready(function(){
        _.each(cities, function(value) {
            var template;
            console.log(value);
            console.log("YA - "+ymaps.geolocation.city);
            if (value == "Москва") {
              /*  if(value == ymaps.geolocation.city){
                    template = _.template('<option selected value="<%= name %>">Москва и МО</option>');
                }
                else {*/
                    template = _.template('<option value="<%= name %>">Москва и МО</option>');
                //}
            } else {
               /* if(value == ymaps.geolocation.city){
                    console.log('11');
                    template = _.template('<option selected value="<%= name %>"><%= name %></option>');
                }
                else {
               */     template = _.template('<option value="<%= name %>"><%= name %></option>');
                //}
            }
            $("#cities").append(template({
                name: value
            }));
        });

        });

        // city trigger
        $("#cities").on("change", function() {
            // change settings
            map_config.city = $(this).val();

            $("#yandex_map").html('');
            initMap(map_config);
        });

        // weekend trigger
        $('[name="weekend"]').on("change", function() {
            map_config.weekend = $(this).prop('checked');
            $("#yandex_map").html('');
            initMap(map_config);
        });

        function initMap(city) {
            $(".scrollable .item").remove();
            $(".scrollable").mCustomScrollbar("destroy");

            var xmap = new map(city);

            setTimeout(function() {
                $(".scrollable").mCustomScrollbar({
                    axis: "y",
                    mouseWheel: {
                        enable: true,
                        scrollAmount: 10,
                        axis: "y"
                    },
                    snapOffset: 10,
                    snapAmount: 10,
                    alwaysShowScrollbar: true,
                    updateOnContentResize: true
                });
            }, 2000);
        }
        // return map;
        // };
    })
});

ymaps.ready(function(){
    $('input[name="REGION"]').val(ymaps.geolocation.city);

});